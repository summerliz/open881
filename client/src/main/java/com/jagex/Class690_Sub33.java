/* Class690_Sub33 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class690_Sub33 extends Class690
{
    public static final int anInt10942 = 0;
    public static final int anInt10943 = 1;
    
    int method14017(int i) {
	return 1;
    }
    
    public int method14027(int i) {
	if (!Class200_Sub23.method15660(aClass534_Sub35_8752
					    .aClass690_Sub7_10733
					    .method16935(-1807368365),
					507669992))
	    return 3;
	return 1;
    }
    
    public void method17164() {
	if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 1)
	    anInt8753 = method14017(2116854778) * 1823770475;
    }
    
    int method14022() {
	return 1;
    }
    
    public boolean method17165(int i) {
	if (!Class200_Sub23.method15660(aClass534_Sub35_8752
					    .aClass690_Sub7_10733
					    .method16935(-1807368365),
					660279492))
	    return false;
	return true;
    }
    
    public int method14026(int i, int i_0_) {
	if (!Class200_Sub23.method15660(aClass534_Sub35_8752
					    .aClass690_Sub7_10733
					    .method16935(-1807368365),
					668537201))
	    return 3;
	return 1;
    }
    
    void method14020(int i, int i_1_) {
	anInt8753 = i * 1823770475;
    }
    
    public int method17166(int i) {
	return anInt8753 * 189295939;
    }
    
    public int method17167() {
	return anInt8753 * 189295939;
    }
    
    public void method17168(int i) {
	if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 1)
	    anInt8753 = method14017(2090170331) * 1823770475;
    }
    
    int method14018() {
	return 1;
    }
    
    public void method17169() {
	if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 1)
	    anInt8753 = method14017(2146029508) * 1823770475;
    }
    
    void method14025(int i) {
	anInt8753 = i * 1823770475;
    }
    
    void method14023(int i) {
	anInt8753 = i * 1823770475;
    }
    
    public Class690_Sub33(int i, Class534_Sub35 class534_sub35) {
	super(i, class534_sub35);
    }
    
    public int method14028(int i) {
	if (!Class200_Sub23.method15660(aClass534_Sub35_8752
					    .aClass690_Sub7_10733
					    .method16935(-1807368365),
					-2008412052))
	    return 3;
	return 1;
    }
    
    public int method14029(int i) {
	if (!Class200_Sub23.method15660(aClass534_Sub35_8752
					    .aClass690_Sub7_10733
					    .method16935(-1807368365),
					-1031051597))
	    return 3;
	return 1;
    }
    
    public int method14030(int i) {
	if (!Class200_Sub23.method15660(aClass534_Sub35_8752
					    .aClass690_Sub7_10733
					    .method16935(-1807368365),
					1463828338))
	    return 3;
	return 1;
    }
    
    void method14024(int i) {
	anInt8753 = i * 1823770475;
    }
    
    public Class690_Sub33(Class534_Sub35 class534_sub35) {
	super(class534_sub35);
    }
    
    int method14021() {
	return 1;
    }
    
    public void method17170() {
	if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 1)
	    anInt8753 = method14017(2097202760) * 1823770475;
    }
    
    public boolean method17171() {
	if (!Class200_Sub23.method15660(aClass534_Sub35_8752
					    .aClass690_Sub7_10733
					    .method16935(-1807368365),
					296767400))
	    return false;
	return true;
    }
    
    public void method17172() {
	if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 1)
	    anInt8753 = method14017(2147230041) * 1823770475;
    }
    
    public int method17173() {
	return anInt8753 * 189295939;
    }
    
    public static void method17174(boolean bool, int i) {
	if (null == Class277.aClass300_3049)
	    Class502.method8302(898758840);
	if (bool)
	    Class277.aClass300_3049.method5520(-1394425898);
    }
}
