/* Class173 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class173
{
    public int anInt1825;
    public static Class173 aClass173_1826;
    static Class173 aClass173_1827;
    public static Class173 aClass173_1828;
    public static Class173 aClass173_1829;
    public static Class173 aClass173_1830;
    public static Class173 aClass173_1831;
    public static Class173 aClass173_1832;
    static Class173 aClass173_1833 = new Class173(1, 1);
    public int anInt1834;
    static Class173 aClass173_1835 = new Class173(8, 2);
    
    Class173(int i, int i_0_) {
	anInt1834 = i * 221097379;
	anInt1825 = i_0_ * -245646037;
    }
    
    static {
	aClass173_1827 = new Class173(6, 4);
	aClass173_1830 = new Class173(4, 1);
	aClass173_1829 = new Class173(5, 2);
	aClass173_1828 = new Class173(7, 3);
	aClass173_1831 = new Class173(3, 4);
	aClass173_1832 = new Class173(0, 2);
	aClass173_1826 = new Class173(2, 4);
    }
    
    static final void method2904(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 1415088283 * Class65.anInt682;
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = -1015338259 * Class65.anInt704;
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class65.anInt669 * -273159789;
	Class65.anInt682 = -2112294;
	Class65.anInt704 = 1793604891;
	Class65.anInt669 = 1552900965;
    }
    
    static final void method2905(Class669 class669, short i) {
	int i_1_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class530.method8851(i_1_, (byte) -13);
    }
    
    static final void method2906(Class669 class669, int i) {
	int i_2_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = client.aClass219_11338.method4120(i_2_, (byte) 55)
		  .method4346(1324454545);
    }
    
    static final void method2907(Class669 class669, int i) {
	class669.anInt8600 -= 617999126;
	int i_3_ = class669.anIntArray8595[class669.anInt8600 * 2088438307];
	int i_4_
	    = class669.anIntArray8595[1 + 2088438307 * class669.anInt8600];
	Class90 class90 = (Class90) Class534_Sub11_Sub13
					.aClass44_Sub22_11730
					.method91(i_4_, 642695840);
	if (class90.method1718(614523121))
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= ((Class205)
		   Class200_Sub12.aClass44_Sub1_9934.method91(i_3_,
							      -711433030))
		      .method3921(i_4_, class90.aString889, -1027177270);
	else
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= (((Class205)
		    Class200_Sub12.aClass44_Sub1_9934.method91(i_3_,
							       -1503906921))
		       .method3914
		   (i_4_, 263946597 * class90.anInt888, (byte) -13));
    }
    
    static final void method2908(Class669 class669, byte i) {
	Class44_Sub6.aClass534_Sub35_10989.method16438
	    (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub24_10756,
	     (class669.anIntArray8595
	      [(class669.anInt8600 -= 308999563) * 2088438307]),
	     -599499345);
	Class672.method11096((byte) 1);
    }
}
