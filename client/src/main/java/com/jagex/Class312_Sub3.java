/* Class312_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class312_Sub3 extends Class312
{
    void method5677(boolean bool, int i, int i_0_) {
	int i_1_ = (method5674((byte) 72)
		    * (aClass394_3366.anInt4097 * -1607607997) / 10000);
	Class254.aClass185_2683.method3298
	    (i, 2 + i_0_, i_1_, -228886179 * aClass394_3366.anInt4098 - 2,
	     ((Class394_Sub3) aClass394_3366).anInt10245 * 107098481, 0);
	Class254.aClass185_2683.method3298
	    (i + i_1_, 2 + i_0_, -1607607997 * aClass394_3366.anInt4097 - i_1_,
	     -228886179 * aClass394_3366.anInt4098 - 2, 0, 0);
    }
    
    void method5673(boolean bool, int i, int i_2_, byte i_3_) {
	int i_4_ = (method5674((byte) 18)
		    * (aClass394_3366.anInt4097 * -1607607997) / 10000);
	Class254.aClass185_2683.method3298
	    (i, 2 + i_2_, i_4_, -228886179 * aClass394_3366.anInt4098 - 2,
	     ((Class394_Sub3) aClass394_3366).anInt10245 * 107098481, 0);
	Class254.aClass185_2683.method3298
	    (i + i_4_, 2 + i_2_, -1607607997 * aClass394_3366.anInt4097 - i_4_,
	     -228886179 * aClass394_3366.anInt4098 - 2, 0, 0);
    }
    
    void method5672(boolean bool, int i, int i_5_, short i_6_) {
	Class254.aClass185_2683.method3297
	    (i - 2, i_5_, 4 + -1607607997 * aClass394_3366.anInt4097,
	     aClass394_3366.anInt4098 * -228886179 + 2,
	     ((Class394_Sub3) aClass394_3366).anInt10246 * -834269879, 0);
	Class254.aClass185_2683.method3297
	    (i - 1, i_5_ + 1, -1607607997 * aClass394_3366.anInt4097 + 2,
	     -228886179 * aClass394_3366.anInt4098, 0, 0);
    }
    
    void method5676(boolean bool, int i, int i_7_) {
	Class254.aClass185_2683.method3297
	    (i - 2, i_7_, 4 + -1607607997 * aClass394_3366.anInt4097,
	     aClass394_3366.anInt4098 * -228886179 + 2,
	     ((Class394_Sub3) aClass394_3366).anInt10246 * -834269879, 0);
	Class254.aClass185_2683.method3297
	    (i - 1, i_7_ + 1, -1607607997 * aClass394_3366.anInt4097 + 2,
	     -228886179 * aClass394_3366.anInt4098, 0, 0);
    }
    
    void method5671(boolean bool, int i, int i_8_) {
	Class254.aClass185_2683.method3297
	    (i - 2, i_8_, 4 + -1607607997 * aClass394_3366.anInt4097,
	     aClass394_3366.anInt4098 * -228886179 + 2,
	     ((Class394_Sub3) aClass394_3366).anInt10246 * -834269879, 0);
	Class254.aClass185_2683.method3297
	    (i - 1, i_8_ + 1, -1607607997 * aClass394_3366.anInt4097 + 2,
	     -228886179 * aClass394_3366.anInt4098, 0, 0);
    }
    
    void method5675(boolean bool, int i, int i_9_) {
	int i_10_ = (method5674((byte) 119)
		     * (aClass394_3366.anInt4097 * -1607607997) / 10000);
	Class254.aClass185_2683.method3298
	    (i, 2 + i_9_, i_10_, -228886179 * aClass394_3366.anInt4098 - 2,
	     ((Class394_Sub3) aClass394_3366).anInt10245 * 107098481, 0);
	Class254.aClass185_2683.method3298
	    (i + i_10_, 2 + i_9_,
	     -1607607997 * aClass394_3366.anInt4097 - i_10_,
	     -228886179 * aClass394_3366.anInt4098 - 2, 0, 0);
    }
    
    void method5678(boolean bool, int i, int i_11_) {
	int i_12_ = (method5674((byte) 93)
		     * (aClass394_3366.anInt4097 * -1607607997) / 10000);
	Class254.aClass185_2683.method3298
	    (i, 2 + i_11_, i_12_, -228886179 * aClass394_3366.anInt4098 - 2,
	     ((Class394_Sub3) aClass394_3366).anInt10245 * 107098481, 0);
	Class254.aClass185_2683.method3298
	    (i + i_12_, 2 + i_11_,
	     -1607607997 * aClass394_3366.anInt4097 - i_12_,
	     -228886179 * aClass394_3366.anInt4098 - 2, 0, 0);
    }
    
    Class312_Sub3(Class472 class472, Class472 class472_13_,
		  Class394_Sub3 class394_sub3) {
	super(class472, class472_13_, (Class394) class394_sub3);
    }
    
    static void method15675(Class669 class669, int i) {
	class669.anIntArray8595[class669.anInt8600 * 2088438307 - 2]
	    = (((Class273)
		Class223.aClass53_Sub2_2316.method91((class669.anIntArray8595
						      [(class669.anInt8600
							* 2088438307) - 2]),
						     -1606482431))
		   .method5080
	       (Class78.aClass103_825,
		class669.anIntArray8595[class669.anInt8600 * 2088438307 - 1],
		(byte) 37)) ? 1 : 0;
	class669.anInt8600 -= 308999563;
    }
    
    static final void method15676(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 0;
    }
    
    static final void method15677(Class669 class669, int i) {
	class669.anInt8600 -= 617999126;
	int i_14_ = class669.anIntArray8595[class669.anInt8600 * 2088438307];
	int i_15_
	    = class669.anIntArray8595[class669.anInt8600 * 2088438307 + 1];
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class2.method511(i_14_, i_15_, false, -817076929);
    }
}
