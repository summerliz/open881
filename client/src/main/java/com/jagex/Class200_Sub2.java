/* Class200_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class200_Sub2 extends Class200
{
    int anInt9882;
    String aString9883;
    int anInt9884;
    int anInt9885;
    
    public void method3847() {
	Class65.aClass192Array712[2073328707 * anInt9884].method3775
	    (2088438307).method18563
	    (aString9883, -2077324837 * anInt9882, 0, anInt9885 * -1908135543,
	     (short) 353);
    }
    
    Class200_Sub2(Class534_Sub40 class534_sub40) {
	super(class534_sub40);
	anInt9884 = class534_sub40.method16529((byte) 1) * -1755926421;
	aString9883 = class534_sub40.method16541((byte) -40);
	anInt9882 = class534_sub40.method16533(-258848859) * 137382483;
	anInt9885 = class534_sub40.method16529((byte) 1) * -1797579591;
    }
    
    public void method3846() {
	Class65.aClass192Array712[2073328707 * anInt9884].method3775
	    (2088438307).method18563
	    (aString9883, -2077324837 * anInt9882, 0, anInt9885 * -1908135543,
	     (short) 353);
    }
    
    public void method3845(int i) {
	Class65.aClass192Array712[2073328707 * anInt9884].method3775
	    (2088438307).method18563
	    (aString9883, -2077324837 * anInt9882, 0, anInt9885 * -1908135543,
	     (short) 353);
    }
    
    public static void method15568(int i, int i_0_, int i_1_, int i_2_,
				   byte i_3_) {
	for (Class534_Sub16 class534_sub16
		 = ((Class534_Sub16)
		    Class534_Sub16.aClass700_10468.method14135((byte) -1));
	     class534_sub16 != null;
	     class534_sub16 = (Class534_Sub16) Class534_Sub16
						   .aClass700_10468
						   .method14139(1568905241))
	    Class243.method4483(class534_sub16, i, i_0_, i_1_, i_2_,
				-1788895012);
	for (Class534_Sub16 class534_sub16
		 = ((Class534_Sub16)
		    Class534_Sub16.aClass700_10469.method14135((byte) -1));
	     null != class534_sub16;
	     class534_sub16 = (Class534_Sub16) Class534_Sub16
						   .aClass700_10469
						   .method14139(2004582438)) {
	    int i_4_ = 1;
	    Class570 class570
		= class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482
		      .method18531((byte) -31);
	    int i_5_ = class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482
			   .aClass711_Sub1_11965.method14329(1458672862);
	    if (i_5_ == -1
		|| (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482
		    .aClass711_Sub1_11965.aBool10971))
		i_4_ = 0;
	    else if (class570.anInt7642 * 1074876801 == i_5_
		     || i_5_ == 421310407 * class570.anInt7631
		     || i_5_ == class570.anInt7633 * 541177679
		     || i_5_ == class570.anInt7632 * -921167219)
		i_4_ = 2;
	    else if (i_5_ == 1846476627 * class570.anInt7634
		     || i_5_ == class570.anInt7638 * -63558043
		     || i_5_ == class570.anInt7647 * -1045334803
		     || 630970333 * class570.anInt7636 == i_5_)
		i_4_ = 3;
	    if (i_4_ != class534_sub16.anInt10497 * 1445799511) {
		int i_6_
		    = (Class308.method5656
		       (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482,
			-707649940));
		Class307 class307
		    = (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482
		       .aClass307_12204);
		if (null != class307.anIntArray3284)
		    class307 = class307.method5615(Class78.aClass103_825,
						   Class78.aClass103_825,
						   -1466068515);
		if (null == class307 || -1 == i_6_) {
		    class534_sub16.anInt10489 = -1086873753;
		    class534_sub16.anInt10497 = i_4_ * 1882006887;
		} else if (i_6_ != -241175639 * class534_sub16.anInt10489) {
		    boolean bool = false;
		    if (class534_sub16.aClass491_10492 != null) {
			class534_sub16.anInt10480 -= 1072686592;
			if (class534_sub16.anInt10480 * 63994171 <= 0) {
			    class534_sub16.aClass491_10492
				.method8014(100, 1962988118);
			    Class171_Sub4.aClass232_9944.method4234
				(class534_sub16.aClass491_10492, -1526886757);
			    class534_sub16.aClass491_10492 = null;
			    bool = true;
			}
		    } else
			bool = true;
		    if (bool) {
			class534_sub16.anInt10480
			    = class307.anInt3334 * 1689126761;
			class534_sub16.anInt10489 = i_6_ * 1086873753;
			class534_sub16.anInt10497 = i_4_ * 1882006887;
		    }
		} else {
		    class534_sub16.anInt10497 = i_4_ * 1882006887;
		    class534_sub16.anInt10480
			= class307.anInt3334 * 1689126761;
		}
	    }
	    Class438 class438
		= (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482
		       .method10807
		   ().aClass438_4885);
	    class534_sub16.anInt10472 = -973592293 * (int) class438.aFloat4864;
	    class534_sub16.anInt10474
		= ((int) class438.aFloat4864
		   + (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482
			  .method18545((byte) 1)
		      << 8)) * -800010841;
	    class534_sub16.anInt10473
		= (int) class438.aFloat4865 * -1604946927;
	    class534_sub16.anInt10475
		= ((int) class438.aFloat4865
		   + (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482
			  .method18545((byte) 1)
		      << 8)) * 1156683381;
	    class534_sub16.anInt10471
		= (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub1_10482
		   .aByte10854) * -1178810913;
	    Class243.method4483(class534_sub16, i, i_0_, i_1_, i_2_,
				-1788895012);
	}
	for (Class534_Sub16 class534_sub16
		 = ((Class534_Sub16)
		    Class534_Sub16.aClass9_10470.method583(-1725261976));
	     class534_sub16 != null;
	     class534_sub16 = (Class534_Sub16) Class534_Sub16.aClass9_10470
						   .method584((byte) -76)) {
	    int i_7_ = 1;
	    Class570 class570
		= class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub2_10490
		      .method18531((byte) -57);
	    int i_8_ = class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub2_10490
			   .aClass711_Sub1_11965.method14329(1041205336);
	    if (-1 == i_8_
		|| (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub2_10490
		    .aClass711_Sub1_11965.aBool10971))
		i_7_ = 0;
	    else if (i_8_ == 1074876801 * class570.anInt7642
		     || i_8_ == class570.anInt7631 * 421310407
		     || class570.anInt7633 * 541177679 == i_8_
		     || i_8_ == class570.anInt7632 * -921167219)
		i_7_ = 2;
	    else if (i_8_ == 1846476627 * class570.anInt7634
		     || -63558043 * class570.anInt7638 == i_8_
		     || -1045334803 * class570.anInt7647 == i_8_
		     || class570.anInt7636 * 630970333 == i_8_)
		i_7_ = 3;
	    if (i_7_ != 1445799511 * class534_sub16.anInt10497) {
		int i_9_
		    = (Class206.method3937
		       (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub2_10490,
			1928578023));
		if (class534_sub16.anInt10489 * -241175639 != i_9_) {
		    boolean bool = false;
		    if (class534_sub16.aClass491_10492 != null) {
			class534_sub16.anInt10480 -= 1072686592;
			if (class534_sub16.anInt10480 * 63994171 <= 0) {
			    class534_sub16.aClass491_10492
				.method8014(100, 1962988118);
			    Class171_Sub4.aClass232_9944.method4234
				(class534_sub16.aClass491_10492, -1447463720);
			    class534_sub16.aClass491_10492 = null;
			    bool = true;
			}
		    } else
			bool = true;
		    if (bool) {
			class534_sub16.anInt10480
			    = (-908420807
			       * (class534_sub16
				  .aClass654_Sub1_Sub5_Sub1_Sub2_10490
				  .anInt12215));
			class534_sub16.anInt10489 = i_9_ * 1086873753;
			class534_sub16.anInt10497 = 1882006887 * i_7_;
		    }
		} else {
		    class534_sub16.anInt10480
			= -908420807 * (class534_sub16
					.aClass654_Sub1_Sub5_Sub1_Sub2_10490
					.anInt12215);
		    class534_sub16.anInt10497 = i_7_ * 1882006887;
		}
	    }
	    Class438 class438
		= (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub2_10490
		       .method10807
		   ().aClass438_4885);
	    class534_sub16.anInt10472 = (int) class438.aFloat4864 * -973592293;
	    class534_sub16.anInt10474
		= ((int) class438.aFloat4864
		   + (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub2_10490
			  .method18545((byte) 1)
		      << 8)) * -800010841;
	    class534_sub16.anInt10473
		= -1604946927 * (int) class438.aFloat4865;
	    class534_sub16.anInt10475
		= ((int) class438.aFloat4865
		   + (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub2_10490
			  .method18545((byte) 1)
		      << 8)) * 1156683381;
	    class534_sub16.anInt10471
		= (class534_sub16.aClass654_Sub1_Sub5_Sub1_Sub2_10490
		   .aByte10854) * -1178810913;
	    Class243.method4483(class534_sub16, i, i_0_, i_1_, i_2_,
				-1788895012);
	}
    }
    
    static final void method15569(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 956397893 * client.anInt11228;
    }
}
