/* Class185_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.awt.Canvas;
import java.lang.reflect.Field;

import jaclib.memory.Buffer;
import jaclib.memory.Stream;
import jaclib.memory.heap.NativeHeap;

import jaggl.OpenGL;

import sun.misc.Unsafe;

public class Class185_Sub3 extends Class185
{
    float[] aFloatArray9515;
    static final float aFloat9516 = 0.35F;
    static final int anInt9517 = 0;
    static final int anInt9518 = 1;
    Class141[] aClass141Array9519;
    static final int anInt9520 = 4;
    static final int anInt9521 = 8;
    static final int anInt9522 = 16;
    static final int anInt9523 = 32;
    static final int anInt9524 = 32993;
    static final int anInt9525 = 5121;
    static final int anInt9526 = 2;
    static final int anInt9527 = 5126;
    Class141_Sub2 aClass141_Sub2_9528;
    static final int anInt9529 = 7;
    Class433 aClass433_9530;
    static final int anInt9531 = -1;
    static final int anInt9532 = -2;
    Class433 aClass433_9533;
    boolean aBool9534;
    static final int anInt9535 = 260;
    static final int anInt9536 = 34023;
    static final int anInt9537 = 34165;
    static final int anInt9538 = 34479;
    Interface15 anInterface15_9539;
    static final int anInt9540 = 7681;
    static final int anInt9541 = 34167;
    float aFloat9542;
    static final int anInt9543 = 770;
    static final int anInt9544 = 768;
    static final int anInt9545 = 0;
    static final int anInt9546 = 1;
    static final int anInt9547 = 2;
    static final int anInt9548 = 0;
    static final int anInt9549 = 1;
    static final int anInt9550 = 5890;
    static final int anInt9551 = 3;
    Class433 aClass433_9552;
    boolean aBool9553;
    static final int anInt9554 = 4;
    static final int anInt9555 = 8;
    static final int anInt9556 = 16;
    Class433 aClass433_9557;
    static final int anInt9558 = 7;
    OpenGL anOpenGL9559;
    boolean aBool9560;
    boolean aBool9561;
    Class143 aClass143_9562;
    static int anInt9563;
    Class534_Sub9_Sub2 aClass534_Sub9_Sub2_9564;
    Class534_Sub9_Sub3 aClass534_Sub9_Sub3_9565;
    boolean aBool9566;
    Class148 aClass148_9567 = new Class148();
    static final int anInt9568 = 2;
    float[] aFloatArray9569;
    Class446 aClass446_9570;
    Class156 aClass156_9571;
    boolean aBool9572;
    int anInt9573;
    int anInt9574;
    boolean aBool9575;
    float aFloat9576;
    Class700 aClass700_9577;
    Unsafe anUnsafe9578;
    int anInt9579;
    int anInt9580;
    Class534_Sub9_Sub1 aClass534_Sub9_Sub1_9581;
    boolean aBool9582;
    static final int anInt9583 = 4;
    Class700 aClass700_9584;
    Class700 aClass700_9585;
    Class700 aClass700_9586;
    float aFloat9587;
    Class700 aClass700_9588;
    long aLong9589;
    static int[] anIntArray9590 = new int[1000];
    int anInt9591;
    Class153 aClass153_9592;
    Interface15 anInterface15_9593;
    boolean aBool9594;
    Class700 aClass700_9595;
    int anInt9596;
    boolean aBool9597;
    boolean aBool9598;
    boolean aBool9599;
    boolean aBool9600;
    int anInt9601;
    Class446 aClass446_9602;
    boolean aBool9603;
    Class433 aClass433_9604;
    Class433 aClass433_9605;
    Class433 aClass433_9606;
    Class433 aClass433_9607;
    float[][] aFloatArrayArray9608;
    static final int anInt9609 = 2;
    float aFloat9610;
    float[] aFloatArray9611;
    float aFloat9612;
    float aFloat9613;
    float aFloat9614;
    float aFloat9615;
    float aFloat9616;
    float aFloat9617;
    Class446 aClass446_9618;
    String aString9619;
    int anInt9620;
    static final int anInt9621 = 100663296;
    int anInt9622;
    boolean aBool9623;
    boolean aBool9624;
    int anInt9625;
    int anInt9626;
    boolean aBool9627;
    int anInt9628;
    int anInt9629;
    int anInt9630;
    boolean aBool9631;
    int anInt9632;
    int anInt9633;
    int anInt9634;
    static final int anInt9635 = 55;
    int anInt9636;
    static final int anInt9637 = 34166;
    int anInt9638;
    NativeHeap aNativeHeap9639;
    float[] aFloatArray9640;
    float[] aFloatArray9641;
    int anInt9642;
    float aFloat9643;
    float aFloat9644;
    float aFloat9645;
    float aFloat9646;
    int anInt9647;
    int anInt9648;
    int anInt9649;
    static final int anInt9650 = 128;
    int anInt9651;
    int anInt9652;
    Class700 aClass700_9653;
    boolean aBool9654;
    Class433 aClass433_9655 = new Class433();
    boolean aBool9656;
    int anInt9657;
    int anInt9658;
    Class161 aClass161_9659;
    float aFloat9660;
    float aFloat9661;
    static final int anInt9662 = 1;
    float aFloat9663;
    int anInt9664;
    Class166 aClass166_9665;
    Interface16 anInterface16_9666;
    Class700 aClass700_9667;
    int anInt9668;
    int anInt9669;
    int anInt9670;
    boolean aBool9671;
    int anInt9672;
    byte aByte9673;
    Class165_Sub1 aClass165_Sub1_9674;
    int anInt9675;
    static final int anInt9676 = 34168;
    float[] aFloatArray9677;
    String aString9678;
    float aFloat9679;
    boolean aBool9680;
    static final int anInt9681 = 8448;
    int anInt9682;
    int anInt9683;
    int anInt9684;
    boolean aBool9685;
    float aFloat9686;
    int anInt9687;
    boolean aBool9688;
    boolean aBool9689;
    int anInt9690;
    boolean aBool9691;
    boolean aBool9692;
    boolean aBool9693;
    boolean aBool9694;
    boolean aBool9695;
    int anInt9696;
    boolean aBool9697;
    boolean aBool9698;
    int anInt9699;
    boolean aBool9700;
    boolean aBool9701;
    Class534_Sub21[] aClass534_Sub21Array9702;
    boolean aBool9703;
    boolean aBool9704;
    boolean aBool9705;
    float aFloat9706;
    Class183_Sub1[] aClass183_Sub1Array9707;
    int anInt9708;
    Class183_Sub1[] aClass183_Sub1Array9709;
    Class175_Sub1_Sub2 aClass175_Sub1_Sub2_9710;
    Class129 aClass129_9711;
    Class129 aClass129_9712;
    static final int anInt9713 = 5123;
    static float[] aFloatArray9714;
    static float[] aFloatArray9715;
    Class534_Sub40_Sub2 aClass534_Sub40_Sub2_9716;
    int[] anIntArray9717;
    int[] anIntArray9718;
    int[] anIntArray9719;
    byte[] aByteArray9720;
    static final int anInt9721 = 3;
    Class163 aClass163_9722;
    Class163 aClass163_9723;
    Class175_Sub1 aClass175_Sub1_9724;
    int[] anIntArray9725;
    long[] aLongArray9726;
    int[] anIntArray9727;
    int anInt9728;
    int anInt9729;
    
    public final void method3552() {
	if (aClass534_Sub9_Sub2_9564 != null
	    && aClass534_Sub9_Sub2_9564.method16109()) {
	    aClass153_9592.method2555(aClass534_Sub9_Sub2_9564);
	    aClass161_9659.method2626();
	}
    }
    
    final void method15177(float f, float f_0_) {
	aFloat9660 = f;
	aFloat9661 = f_0_;
	method15305();
    }
    
    int method15178() {
	int i = 0;
	aString9678 = OpenGL.glGetString(7936).toLowerCase();
	aString9619 = OpenGL.glGetString(7937).toLowerCase();
	if (aString9678.indexOf("microsoft") != -1)
	    i |= 0x1;
	if (aString9678.indexOf("brian paul") != -1
	    || aString9678.indexOf("mesa") != -1)
	    i |= 0x1;
	String string = OpenGL.glGetString(7938);
	String[] strings
	    = Class387.method6504(string.replace('.', ' '), ' ', -255127846);
	if (strings.length >= 2) {
	    try {
		int i_1_ = Class684.method13949(strings[0], (byte) 38);
		int i_2_ = Class684.method13949(strings[1], (byte) 47);
		anInt9579 = i_1_ * 10 + i_2_;
	    } catch (NumberFormatException numberformatexception) {
		i |= 0x4;
	    }
	} else
	    i |= 0x4;
	if (anInt9579 < 12)
	    i |= 0x2;
	if (!anOpenGL9559.method1766("GL_ARB_multitexture"))
	    i |= 0x8;
	if (!anOpenGL9559.method1766("GL_ARB_texture_env_combine"))
	    i |= 0x20;
	int[] is = new int[1];
	OpenGL.glGetIntegerv(34018, is, 0);
	anInt9682 = is[0];
	OpenGL.glGetIntegerv(34929, is, 0);
	anInt9683 = is[0];
	OpenGL.glGetIntegerv(34930, is, 0);
	anInt9684 = is[0];
	if (anInt9682 < 2 || anInt9683 < 2 || anInt9684 < 2)
	    i |= 0x10;
	aBool9566 = Stream.method1807();
	aBool9705 = anOpenGL9559.method1766("GL_ARB_vertex_buffer_object");
	aBool9688 = anOpenGL9559.method1766("GL_ARB_multisample");
	aBool9656 = anOpenGL9559.method1766("GL_ARB_vertex_program");
	aBool9701 = anOpenGL9559.method1766("GL_ARB_fragment_program");
	aBool9700 = anOpenGL9559.method1766("GL_ARB_vertex_shader");
	aBool9680 = anOpenGL9559.method1766("GL_ARB_fragment_shader");
	aBool9693 = anOpenGL9559.method1766("GL_EXT_texture3D");
	aBool9624 = anOpenGL9559.method1766("GL_ARB_texture_rectangle");
	aBool9694 = anOpenGL9559.method1766("GL_ARB_texture_cube_map");
	aBool9534 = anOpenGL9559.method1766("GL_ARB_seamless_cube_map");
	aBool9697 = anOpenGL9559.method1766("GL_ARB_texture_float");
	aBool9698 = anOpenGL9559.method1766("GL_ARB_texture_non_power_of_two");
	aBool9685 = anOpenGL9559.method1766("GL_EXT_framebuffer_object");
	aBool9603 = anOpenGL9559.method1766("GL_EXT_framebuffer_blit");
	aBool9572 = anOpenGL9559.method1766("GL_EXT_framebuffer_multisample");
	aBool9689 = aBool9603 & aBool9572;
	aBool9704 = anOpenGL9559.method1766("GL_EXT_blend_func_separate");
	aBool9692
	    = (anUnsafe9578 != null
	       && (anInt9579 >= 32 || anOpenGL9559.method1766("GL_ARB_sync")));
	aBool9582 = Class503.aString5626.startsWith("mac");
	OpenGL.glGetFloatv(2834, aFloatArray9714, 0);
	aFloat9542 = aFloatArray9714[0];
	aFloat9706 = aFloatArray9714[1];
	return i != 0 ? i : 0;
    }
    
    void method15179() {
	aClass141Array9519 = new Class141[anInt9682];
	aClass141_Sub2_9528
	    = new Class141_Sub2(this, 3553, Class181.aClass181_1952,
				Class173.aClass173_1830, 1, 1);
	new Class141_Sub2(this, 3553, Class181.aClass181_1952,
			  Class173.aClass173_1830, 1, 1);
	new Class141_Sub2(this, 3553, Class181.aClass181_1952,
			  Class173.aClass173_1830, 1, 1);
	for (int i = 0; i < 8; i++) {
	    aClass183_Sub1Array9707[i] = new Class183_Sub1(this);
	    aClass183_Sub1Array9709[i] = new Class183_Sub1(this);
	}
	if (aBool9685) {
	    aClass175_Sub1_Sub2_9710 = new Class175_Sub1_Sub2(this);
	    new Class175_Sub1_Sub2(this);
	}
    }
    
    void method15180() {
	method15227(-2);
	for (int i = anInt9682 - 1; i >= 0; i--) {
	    method15230(i);
	    method15231(null);
	    OpenGL.glTexEnvi(8960, 8704, 34160);
	}
	method15232(8448, 8448);
	method15325(2, 34168, 770);
	method15238();
	anInt9708 = 1;
	anInt9596 = 0;
	aBool9553 = true;
	if (aBool9704)
	    OpenGL.glBlendFuncSeparate(770, 771, 0, 0);
	else
	    OpenGL.glBlendFunc(770, 771);
	anInt9636 = 1;
	aByte9673 = (byte) -1;
	method15251((byte) 0);
	aBool9597 = true;
	OpenGL.glEnable(3008);
	OpenGL.glAlphaFunc(516, (float) aByte9673);
	if (aBool9688) {
	    if (anInt9668 == 0)
		OpenGL.glDisable(32925);
	    else
		OpenGL.glEnable(32925);
	    OpenGL.glDisable(32926);
	}
	OpenGL.glColorMask(true, true, true, true);
	aBool9594 = true;
	method15344(true);
	method15432(true);
	method15242(true);
	method15193(true);
	method3284(0.0F, 1.0F);
	method15203();
	anOpenGL9559.setSwapInterval(0);
	OpenGL.glShadeModel(7425);
	OpenGL.glClearDepth(1.0F);
	OpenGL.glDepthFunc(515);
	OpenGL.glPolygonMode(1028, 6914);
	method15255(anInt9658);
	OpenGL.glMatrixMode(5888);
	OpenGL.glLoadIdentity();
	OpenGL.glColorMaterial(1028, 5634);
	OpenGL.glEnable(2903);
	float[] fs = { 0.0F, 0.0F, 0.0F, 1.0F };
	for (int i = 0; i < 8; i++) {
	    int i_3_ = 16384 + i;
	    OpenGL.glLightfv(i_3_, 4608, fs, 0);
	    OpenGL.glLightf(i_3_, 4615, 0.0F);
	    OpenGL.glLightf(i_3_, 4616, 0.0F);
	}
	OpenGL.glEnable(16384);
	OpenGL.glEnable(16385);
	OpenGL.glFogf(2914, 0.95F);
	OpenGL.glFogi(2917, 9729);
	OpenGL.glHint(3156, 4353);
	if (aBool9534)
	    OpenGL.glEnable(34895);
	anInt9687 = -1;
	anInt9642 = -1;
	method3281();
	method3537();
    }
    
    public Class176 method3232() {
	int i = -1;
	if (aString9678.indexOf("nvidia") != -1)
	    i = 4318;
	else if (aString9678.indexOf("intel") != -1)
	    i = 32902;
	else if (aString9678.indexOf("ati") != -1)
	    i = 4098;
	return new Class176(i, "OpenGL", anInt9579, aString9619, 0L, false);
    }
    
    void method3234(int i, int i_4_) throws Exception_Sub7 {
	try {
	    aClass175_Sub2_2013.method15475();
	} catch (Exception exception) {
	    /* empty */
	}
	if (anInterface25_1997 != null)
	    anInterface25_1997.method33(1526967316);
    }
    
    public void method3575() {
	OpenGL.glFinish();
    }
    
    void method3327() {
	for (Class534 class534 = aClass700_9577.method14135((byte) -1);
	     class534 != null;
	     class534 = aClass700_9577.method14139(822446261))
	    ((Class534_Sub2_Sub2) class534).method18232();
	if (aClass153_9592 != null)
	    aClass153_9592.method2547();
	if (aBool9575) {
	    Class54.method1212(false, true, -1585647018);
	    aBool9575 = false;
	}
    }
    
    public boolean method3396() {
	return true;
    }
    
    void method3300(int i, int i_5_, int i_6_, int i_7_, int i_8_) {
	if (i_6_ < 0)
	    i_6_ = -i_6_;
	if (i + i_6_ >= anInt9690 && i - i_6_ <= anInt9628
	    && i_5_ + i_6_ >= anInt9625 && i_5_ - i_6_ <= anInt9626) {
	    method15223();
	    method15246(i_8_);
	    OpenGL.glColor4ub((byte) (i_7_ >> 16), (byte) (i_7_ >> 8),
			      (byte) i_7_, (byte) (i_7_ >> 24));
	    float f = (float) i + 0.35F;
	    float f_9_ = (float) i_5_ + 0.35F;
	    int i_10_ = i_6_ << 1;
	    if ((float) i_10_ < aFloat9542) {
		OpenGL.glBegin(7);
		OpenGL.glVertex2f(f + 1.0F, f_9_ + 1.0F);
		OpenGL.glVertex2f(f + 1.0F, f_9_ - 1.0F);
		OpenGL.glVertex2f(f - 1.0F, f_9_ - 1.0F);
		OpenGL.glVertex2f(f - 1.0F, f_9_ + 1.0F);
		OpenGL.glEnd();
	    } else if ((float) i_10_ <= aFloat9706) {
		OpenGL.glEnable(2832);
		OpenGL.glPointSize((float) i_10_);
		OpenGL.glBegin(0);
		OpenGL.glVertex2f(f, f_9_);
		OpenGL.glEnd();
		OpenGL.glDisable(2832);
	    } else {
		OpenGL.glBegin(6);
		OpenGL.glVertex2f(f, f_9_);
		int i_11_ = 262144 / (6 * i_6_);
		if (i_11_ <= 64)
		    i_11_ = 64;
		else if (i_11_ > 512)
		    i_11_ = 512;
		i_11_ = Class455.method7422(i_11_, -370167111);
		OpenGL.glVertex2f(f + (float) i_6_, f_9_);
		for (int i_12_ = 16384 - i_11_; i_12_ > 0; i_12_ -= i_11_)
		    OpenGL.glVertex2f(f + (Class147.aFloatArray1665[i_12_]
					   * (float) i_6_),
				      f_9_ + (Class147.aFloatArray1664[i_12_]
					      * (float) i_6_));
		OpenGL.glVertex2f(f + (float) i_6_, f_9_);
		OpenGL.glEnd();
	    }
	}
    }
    
    final synchronized void method15181(int i, int i_13_) {
	Class534_Sub39 class534_sub39 = new Class534_Sub39(i_13_);
	class534_sub39.aLong7158 = (long) i * 936217890172187787L;
	aClass700_9595.method14131(class534_sub39, (short) 789);
    }
    
    public boolean method3534() {
	return (aClass534_Sub9_Sub2_9564 != null
		&& (anInt9668 <= 1 || aBool9689));
    }
    
    public boolean method3404() {
	return true;
    }
    
    public void method3558(int i, Class166 class166) {
	if (!aBool9695)
	    throw new RuntimeException("");
	anInt9664 = i;
	aClass166_9665 = class166;
	if (aBool9598) {
	    aClass143_9562.aClass136_Sub4_1653.method14478();
	    aClass143_9562.aClass136_Sub4_1653.method14477();
	}
    }
    
    public final void method3453(int[] is) {
	is[0] = anInt9690;
	is[1] = anInt9625;
	is[2] = anInt9628;
	is[3] = anInt9626;
    }
    
    public boolean method3247() {
	return true;
    }
    
    final void method15182() {
	if (aBool9704) {
	    int i = 0;
	    int i_14_ = 0;
	    if (anInt9596 == 0) {
		i = 0;
		i_14_ = 0;
	    } else if (anInt9596 == 1) {
		i = 1;
		i_14_ = 0;
	    } else if (anInt9596 == 2) {
		i = 1;
		i_14_ = 1;
	    } else if (anInt9596 == 3) {
		i = 0;
		i_14_ = 1;
	    }
	    if (anInt9636 == 1)
		OpenGL.glBlendFuncSeparate(770, 771, i, i_14_);
	    else if (anInt9636 == 2)
		OpenGL.glBlendFuncSeparate(1, 1, i, i_14_);
	    else if (anInt9636 == 3)
		OpenGL.glBlendFuncSeparate(774, 1, i, i_14_);
	    else if (anInt9636 == 0)
		OpenGL.glBlendFuncSeparate(1, 0, i, i_14_);
	} else if (anInt9636 == 1) {
	    OpenGL.glEnable(3042);
	    OpenGL.glBlendFunc(770, 771);
	} else if (anInt9636 == 2) {
	    OpenGL.glEnable(3042);
	    OpenGL.glBlendFunc(1, 1);
	} else if (anInt9636 == 3) {
	    OpenGL.glEnable(3042);
	    OpenGL.glBlendFunc(774, 1);
	} else
	    OpenGL.glDisable(3042);
    }
    
    public boolean method3333() {
	return true;
    }
    
    public boolean method3249() {
	return false;
    }
    
    public String method3250() {
	String string = "Caps: 2:";
	String string_15_ = ":";
	string = new StringBuilder().append(string).append(anInt9668).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append(anInt9620).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append(anInt9682).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append(anInt9683).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append(anInt9684).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append(aFloat9542).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append(aFloat9706).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9566 ? 1 : 0).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9631 ? 1 : 0).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9582 ? 1 : 0).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9705 ? 1 : 0).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9691 ? 1 : 0).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9688 ? 1 : 0).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9656 ? 1 : 0).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9701 ? 1 : 0).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9700 ? 1 : 0).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9680 ? 1 : 0).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9693 ? 1 : 0).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9624 ? 1 : 0).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9694 ? 1 : 0).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9534 ? 1 : 0).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9698 ? 1 : 0).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9685 ? 1 : 0).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9603 ? 1 : 0).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9572 ? 1 : 0).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9697 ? 1 : 0).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9703 ? 1 : 0).append
		     (string_15_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9704 ? 1 : 0).toString();
	return string;
    }
    
    public int[] method3251() {
	int[] is = new int[1];
	OpenGL.glGetIntegerv(34466, is, 0);
	int i = is[0];
	if (i == 0)
	    return null;
	int[] is_16_ = new int[i];
	OpenGL.glGetIntegerv(34467, is_16_, 0);
	return is_16_;
    }
    
    final void method15183(float[] fs) {
	float[] fs_17_ = new float[16];
	System.arraycopy(fs, 0, fs_17_, 0, 16);
	fs_17_[1] = -fs_17_[1];
	fs_17_[5] = -fs_17_[5];
	fs_17_[9] = -fs_17_[9];
	fs_17_[13] = -fs_17_[13];
	OpenGL.glMatrixMode(5889);
	OpenGL.glLoadMatrixf(fs_17_, 0);
	OpenGL.glMatrixMode(5888);
    }
    
    void method15184() {
	OpenGL.glLightfv(16384, 4611, aFloatArray9640, 0);
	OpenGL.glLightfv(16385, 4611, aFloatArray9641, 0);
    }
    
    public Class151 method3332(int i, int i_18_, int[][] is, int[][] is_19_,
			       int i_20_, int i_21_, int i_22_) {
	return new Class151_Sub1(this, i_21_, i_22_, i, i_18_, is, is_19_,
				 i_20_);
    }
    
    public int[] method3267(int i, int i_23_, int i_24_, int i_25_) {
	if (aClass175_1989 != null) {
	    int[] is = new int[i_24_ * i_25_];
	    int i_26_ = aClass175_1989.method2911();
	    for (int i_27_ = 0; i_27_ < i_25_; i_27_++)
		OpenGL.glReadPixelsi(i, i_26_ - i_23_ - i_27_ - 1, i_24_, 1,
				     32993, anInt9620, is, i_27_ * i_24_);
	    return is;
	}
	return null;
    }
    
    public void method3268() {
	if (aBool9703 && aClass175_1989 != null) {
	    int i = anInt9690;
	    int i_28_ = anInt9628;
	    int i_29_ = anInt9625;
	    int i_30_ = anInt9626;
	    method3537();
	    int i_31_ = anInt9648;
	    int i_32_ = anInt9632;
	    int i_33_ = anInt9633;
	    int i_34_ = anInt9634;
	    method3281();
	    OpenGL.glReadBuffer(1028);
	    OpenGL.glDrawBuffer(1029);
	    method15203();
	    method15344(false);
	    method15432(false);
	    method15242(false);
	    method15193(false);
	    method15231(null);
	    method15227(-2);
	    method15214(1);
	    method15246(0);
	    OpenGL.glMatrixMode(5889);
	    OpenGL.glLoadIdentity();
	    OpenGL.glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
	    OpenGL.glMatrixMode(5888);
	    OpenGL.glLoadIdentity();
	    OpenGL.glRasterPos2i(0, 0);
	    OpenGL.glCopyPixels(0, 0, aClass175_1989.method2910(),
				aClass175_1989.method2911(), 6144);
	    OpenGL.glFlush();
	    OpenGL.glReadBuffer(1029);
	    OpenGL.glDrawBuffer(1029);
	    method3373(i, i_29_, i_28_, i_30_);
	    method3318(i_31_, i_32_, i_33_, i_34_);
	}
    }
    
    public boolean method3269() {
	return aBool9692;
    }
    
    public boolean method3270() {
	if (aBool9692 && aLongArray9726[anInt9729] != 0L)
	    return false;
	return true;
    }
    
    public Interface22 method3571(int i, int i_35_, Class181 class181,
				  Class173 class173, int i_36_) {
	return new Class534_Sub18_Sub1(this, class181, class173, i, i_35_,
				       i_36_);
    }
    
    public boolean method3666() {
	return true;
    }
    
    public int method3273() {
	if (aBool9692) {
	    if (aLongArray9726[anInt9728] == 0L)
		return -1;
	    int i = OpenGL.glClientWaitSync(aLongArray9726[anInt9728], 0, 0);
	    if (i == 37149) {
		method3656();
		return -1;
	    }
	    return i != 37147 ? anIntArray9727[anInt9728] : -1;
	}
	return -1;
    }
    
    public void method3275(int i, int i_37_, int i_38_) {
	method3559();
	if (aClass175_Sub1_9724 == null)
	    method15399(i_37_, i_38_);
	if (aClass163_9722 == null)
	    aClass163_9722 = method3319(0, 0, aClass175_1989.method2910(),
					aClass175_1989.method2911(), true);
	else
	    ((Class163_Sub3) aClass163_9722).method15485
		(0, 0, aClass175_1989.method2910(),
		 aClass175_1989.method2911(), 0, 0, true);
	method3260(aClass175_Sub1_9724, -1807502252);
	method3340(1, -16777216);
	aClass163_9722.method2659(anInt2018 * -1555714981,
				  anInt2019 * -385311879,
				  anInt2020 * 1769547157,
				  anInt2021 * -740019615);
	OpenGL.glBindBufferARB(35051, anIntArray9725[anInt9729]);
	OpenGL.glReadPixelsub(0, 0, anInt2016 * 1104963955,
			      anInt2017 * 1827315157, 32993, 5121, null, 0);
	OpenGL.glBindBufferARB(35051, 0);
	method3261(aClass175_Sub1_9724, -11578496);
	aLongArray9726[anInt9729] = OpenGL.glFenceSync(37143, 0);
	anIntArray9727[anInt9729] = i;
	if (++anInt9729 >= 3)
	    anInt9729 = 0;
	method3446();
    }
    
    public boolean method3421() {
	return true;
    }
    
    public final void method3507(Class446 class446) {
	aClass446_9602.method7236(class446);
	aClass433_9533.method6916(aClass446_9602);
	aClass446_9618.method7236(class446);
	aClass446_9618.method7243();
	aClass433_9604.method6916(aClass446_9618);
	method15207();
	if (anInt9622 != 1)
	    method15273();
    }
    
    public void method3427() {
	for (int i = 0; i < 3; i++) {
	    if (aLongArray9726[i] != 0L) {
		OpenGL.glDeleteSync(aLongArray9726[i]);
		aLongArray9726[i] = 0L;
	    }
	}
	anInt9729 = 0;
	anInt9728 = 0;
    }
    
    long method15185(int i, int i_39_, int[] is, int[] is_40_) {
	if (aBool9692) {
	    if (aLongArray9726[anInt9728] != 0L) {
		OpenGL.glDeleteSync(aLongArray9726[anInt9728]);
		aLongArray9726[anInt9728] = 0L;
	    }
	    OpenGL.glBindBufferARB(35051, anIntArray9725[anInt9728]);
	    long l = OpenGL.glMapBufferARB(35051, 35000);
	    if (is != null) {
		int i_41_ = 0;
		for (int i_42_ = i_39_ - 1; i_42_ >= 0; i_42_--) {
		    for (int i_43_ = 0; i_43_ < i; i_43_++)
			is[i_41_++]
			    = anUnsafe9578.getInt(l + (long) ((i_42_ * i
							       + i_43_)
							      * 4));
		}
		if (OpenGL.glUnmapBufferARB(35051)) {
		    /* empty */
		}
		OpenGL.glBindBufferARB(35051, 0);
		l = 0L;
	    }
	    if (++anInt9728 >= 3)
		anInt9728 = 0;
	    return l;
	}
	if (aClass163_9723 == null)
	    method15399(i, i_39_);
	if (aClass163_9722 == null)
	    aClass163_9722 = method3319(0, 0, aClass175_1989.method2910(),
					aClass175_1989.method2911(), true);
	else
	    ((Class163_Sub3) aClass163_9722).method15485
		(0, 0, aClass175_1989.method2910(),
		 aClass175_1989.method2911(), 0, 0, true);
	method3260(aClass175_Sub1_9724, 1128410437);
	method3340(1, -16777216);
	aClass163_9722.method2659(anInt2018 * -1555714981,
				  anInt2019 * -385311879,
				  anInt2020 * 1769547157,
				  anInt2021 * -740019615);
	aClass163_9723.method2653(0, 0, i, i_39_, is, is_40_, 0, i);
	method3261(aClass175_Sub1_9724, -11578496);
	return 0L;
    }
    
    Class185_Sub3(Canvas canvas, Class177 class177, Interface25 interface25,
		  Interface45 interface45, Interface48 interface48,
		  Interface46 interface46, int i) {
	super(class177, interface25, interface45, interface48, interface46);
	aClass433_9557 = new Class433();
	aClass446_9570 = new Class446();
	anInt9638 = 8;
	anInt9574 = 3;
	aBool9575 = false;
	aClass700_9577 = new Class700();
	anUnsafe9578 = null;
	aClass700_9653 = new Class700();
	aClass700_9595 = new Class700();
	aClass700_9584 = new Class700();
	aClass700_9585 = new Class700();
	aClass700_9586 = new Class700();
	aClass700_9667 = new Class700();
	aClass700_9588 = new Class700();
	anInt9658 = 2;
	aClass446_9602 = new Class446();
	aClass433_9533 = new Class433();
	aClass433_9604 = new Class433();
	aClass433_9605 = new Class433();
	aClass433_9606 = new Class433();
	aClass433_9607 = new Class433();
	aFloatArrayArray9608 = new float[6][4];
	aFloatArray9569 = new float[4];
	aFloat9614 = 0.0F;
	aFloat9615 = 1.0F;
	aFloat9616 = 0.0F;
	aFloat9617 = -1.0F;
	aClass446_9618 = new Class446();
	aClass433_9530 = new Class433();
	aClass433_9552 = new Class433();
	aFloatArray9611 = new float[16];
	aBool9623 = true;
	aBool9561 = true;
	anInt9625 = 0;
	anInt9626 = 0;
	anInt9690 = 0;
	anInt9628 = 0;
	anInt9629 = 0;
	anInt9630 = 0;
	aFloatArray9677 = new float[4];
	aFloatArray9515 = new float[4];
	aFloatArray9640 = new float[4];
	aFloatArray9641 = new float[4];
	anInt9642 = -1;
	aFloat9643 = 1.0F;
	aFloat9644 = 1.0F;
	aFloat9645 = 1.0F;
	aFloat9686 = -1.0F;
	aFloat9587 = -1.0F;
	aClass534_Sub21Array9702 = new Class534_Sub21[anInt9563];
	anInt9687 = -1;
	anInt9675 = -1;
	anInt9657 = 0;
	aFloat9660 = 1.0F;
	aFloat9661 = 0.0F;
	aBool9598 = false;
	anInt9573 = 8448;
	anInt9670 = 8448;
	aFloat9706 = -1.0F;
	aFloat9542 = -1.0F;
	aClass183_Sub1Array9707 = new Class183_Sub1[8];
	aClass183_Sub1Array9709 = new Class183_Sub1[8];
	aClass534_Sub40_Sub2_9716 = new Class534_Sub40_Sub2(8192);
	anIntArray9717 = new int[1];
	anIntArray9718 = new int[1];
	anIntArray9719 = new int[1];
	aByteArray9720 = new byte[16384];
	aClass163_9722 = null;
	aClass163_9723 = null;
	aClass175_Sub1_9724 = null;
	anIntArray9725 = new int[3];
	aLongArray9726 = new long[3];
	anIntArray9727 = new int[3];
	anInt9728 = 0;
	anInt9729 = 0;
	try {
	    try {
		Field field
		    = sun.misc.Unsafe.class.getDeclaredField("theUnsafe");
		field.setAccessible(true);
		anUnsafe9578 = (Unsafe) field.get(null);
	    } catch (Exception exception) {
		/* empty */
	    }
	    anInt9668 = i;
	    Class112.method2018(-1327590673).method400("jaclib", -847504289);
	    Class112.method2018(-1327590673).method400("jaggl", -1663578590);
	    anOpenGL9559 = new OpenGL();
	    long l = anOpenGL9559.init(canvas, 8, 8, 8, 24, 0, anInt9668);
	    if (l == 0L)
		throw new RuntimeException("");
	    method15287();
	    int i_44_ = method15178();
	    if (i_44_ != 0)
		throw new RuntimeException("");
	    if (aBool9582 && aBool9685) {
		String string = System.getProperty("java.version");
		int i_45_ = string.indexOf('_');
		if (i_45_ > -1)
		    string = string.substring(0, i_45_);
		i_45_ = string.indexOf('.');
		if (i_45_ > -1) {
		    i_45_ = string.indexOf('.', i_45_ + 1);
		    if (i_45_ > -1)
			string = string.substring(0, i_45_);
		}
		try {
		    int i_46_ = (int) (Float.parseFloat(string) * 100.0F);
		    if (i_46_ >= 170) {
			aBool9685 = false;
			aBool9688 = false;
			aBool9689 = false;
		    }
		} catch (NumberFormatException numberformatexception) {
		    aBool9685 = false;
		    aBool9688 = false;
		    aBool9689 = false;
		}
	    }
	    anInt9620 = aBool9566 ? 33639 : 5121;
	    aBool9631 = aString9619.indexOf("radeon") != -1;
	    boolean bool = aString9678.indexOf("intel") != -1;
	    boolean bool_47_ = false;
	    boolean bool_48_ = false;
	    int i_49_ = 0;
	    if (aBool9631 || bool) {
		String[] strings
		    = Class387.method6504(aString9619.replace('/', ' '), ' ',
					  -1649828927);
		int i_50_ = 0;
		for (/**/; i_50_ < strings.length; i_50_++) {
		    String string = strings[i_50_];
		    try {
			if (string.length() <= 0)
			    continue;
			if (string.charAt(0) == 'x' && string.length() >= 3
			    && Class545.method8965(string.substring(1, 3),
						   507490991)) {
			    string = string.substring(1);
			    bool_48_ = true;
			}
			if (string.equals("hd")) {
			    bool_47_ = true;
			    continue;
			}
			if (string.startsWith("hd")) {
			    string = string.substring(2);
			    bool_47_ = true;
			}
			if (string.length() < 4
			    || !Class545.method8965(string.substring(0, 4),
						    -2018559498))
			    continue;
			i_49_ = Class684.method13949(string.substring(0, 4),
						     (byte) 8);
		    } catch (Exception exception) {
			continue;
		    }
		    break;
		}
	    }
	    if (anInt9668 != 0 && bool && !bool_47_)
		throw new RuntimeException_Sub4("");
	    if (aBool9631 || bool) {
		if (bool) {
		    if (!bool_47_) {
			aBool9685 = false;
			aBool9688 = false;
			aBool9689 = false;
		    }
		} else {
		    if (!bool_48_ && !bool_47_) {
			if (i_49_ >= 7000 && i_49_ <= 7999)
			    aBool9705 = false;
			if (i_49_ >= 7000 && i_49_ <= 9250)
			    aBool9693 = false;
		    }
		    if (!bool_47_ || i_49_ < 4000)
			aBool9697 = false;
		    aBool9624
			&= anOpenGL9559.method1766("GL_ARB_half_float_pixel");
		    aBool9691 = aBool9705;
		}
	    }
	    aBool9703 = !aString9678.equals("s3 graphics");
	    if (aBool9705) {
		try {
		    int[] is = new int[1];
		    OpenGL.glGenBuffersARB(1, is, 0);
		} catch (Throwable throwable) {
		    throw new RuntimeException("");
		}
	    }
	    Class456.method7426(false, true, 1739452007);
	    aBool9575 = true;
	    aClass161_9659 = new Class161(this, anInterface25_1997);
	    method15179();
	    aClass156_9571 = new Class156(this);
	    aClass153_9592 = new Class153(this);
	    if (!aClass153_9592.method2548()) {
		aClass153_9592.method2547();
		aClass153_9592 = null;
	    } else {
		aClass534_Sub9_Sub2_9564 = new Class534_Sub9_Sub2(this);
		if (!aClass534_Sub9_Sub2_9564.method18103()) {
		    aClass534_Sub9_Sub2_9564.method16104();
		    aClass534_Sub9_Sub2_9564 = null;
		}
		aClass534_Sub9_Sub3_9565 = new Class534_Sub9_Sub3(this);
		if (!aClass534_Sub9_Sub3_9565.method18111()) {
		    aClass534_Sub9_Sub3_9565.method16104();
		    aClass534_Sub9_Sub3_9565 = null;
		}
		aClass534_Sub9_Sub1_9581 = new Class534_Sub9_Sub1(this);
		if (!aClass534_Sub9_Sub1_9581.method18061()) {
		    aClass534_Sub9_Sub1_9581.method16104();
		    aClass534_Sub9_Sub1_9581 = null;
		}
	    }
	    method3256(canvas, new Class175_Sub2_Sub2(this, canvas, l),
		       (byte) 4);
	    method3258(canvas, -776174490);
	    if (aBool9582) {
		int i_51_ = canvas.getLocation().x;
		int i_52_ = canvas.getLocation().y;
		canvas.setLocation(i_51_ + 1, i_52_);
		canvas.setLocation(i_51_, i_52_);
	    }
	    aClass143_9562 = new Class143(this);
	    method15180();
	    method3268();
	    if (aClass153_9592 != null) {
		method15216();
		method15291();
	    }
	} catch (Throwable throwable) {
	    throwable.printStackTrace();
	    method3236(-568376843);
	    if (throwable instanceof OutOfMemoryError)
		throw (OutOfMemoryError) throwable;
	    if (throwable instanceof RuntimeException_Sub4)
		throw (RuntimeException_Sub4) throwable;
	    throw new RuntimeException("");
	}
    }
    
    public Class165 method3535(int i) {
	return aBool9694 ? new Class165_Sub1_Sub2(this, i) : null;
    }
    
    final void method15186(int i, int i_53_) {
	anInt9629 = i;
	anInt9630 = i_53_;
	method15197();
	method15198();
    }
    
    public void method3340(int i, int i_54_) {
	int i_55_ = 0;
	if ((i & 0x1) != 0) {
	    OpenGL.glClearColor((float) (i_54_ & 0xff0000) / 1.671168E7F,
				(float) (i_54_ & 0xff00) / 65280.0F,
				(float) (i_54_ & 0xff) / 255.0F,
				(float) (i_54_ >>> 24) / 255.0F);
	    i_55_ = 16384;
	}
	if ((i & 0x2) != 0) {
	    method15193(true);
	    i_55_ |= 0x100;
	}
	if ((i & 0x4) != 0)
	    i_55_ |= 0x400;
	OpenGL.glClear(i_55_);
    }
    
    public void method3297(int i, int i_56_, int i_57_, int i_58_, int i_59_,
			   int i_60_) {
	float f = (float) i + 0.35F;
	float f_61_ = (float) i_56_ + 0.35F;
	float f_62_ = f + (float) i_57_ - 1.0F;
	float f_63_ = f_61_ + (float) i_58_ - 1.0F;
	method15223();
	method15246(i_60_);
	OpenGL.glColor4ub((byte) (i_59_ >> 16), (byte) (i_59_ >> 8),
			  (byte) i_59_, (byte) (i_59_ >> 24));
	if (aBool9688)
	    OpenGL.glDisable(32925);
	OpenGL.glBegin(2);
	OpenGL.glVertex2f(f, f_61_);
	OpenGL.glVertex2f(f, f_63_);
	OpenGL.glVertex2f(f_62_, f_63_);
	OpenGL.glVertex2f(f_62_, f_61_);
	OpenGL.glEnd();
	if (aBool9688)
	    OpenGL.glEnable(32925);
    }
    
    public void method3298(int i, int i_64_, int i_65_, int i_66_, int i_67_,
			   int i_68_) {
	float f = (float) i + 0.35F;
	float f_69_ = (float) i_64_ + 0.35F;
	float f_70_ = f + (float) i_65_;
	float f_71_ = f_69_ + (float) i_66_;
	method15223();
	method15246(i_68_);
	OpenGL.glColor4ub((byte) (i_67_ >> 16), (byte) (i_67_ >> 8),
			  (byte) i_67_, (byte) (i_67_ >> 24));
	if (aBool9688)
	    OpenGL.glDisable(32925);
	OpenGL.glBegin(7);
	OpenGL.glVertex2f(f, f_69_);
	OpenGL.glVertex2f(f, f_71_);
	OpenGL.glVertex2f(f_70_, f_71_);
	OpenGL.glVertex2f(f_70_, f_69_);
	OpenGL.glEnd();
	if (aBool9688)
	    OpenGL.glEnable(32925);
    }
    
    public void method3299(int i, int i_72_, float f, int i_73_, int i_74_,
			   float f_75_, int i_76_, int i_77_, float f_78_,
			   int i_79_, int i_80_, int i_81_, int i_82_) {
	method15223();
	method15246(i_82_);
	OpenGL.glBegin(4);
	OpenGL.glColor4ub((byte) (i_79_ >> 16), (byte) (i_79_ >> 8),
			  (byte) i_79_, (byte) (i_79_ >> 24));
	OpenGL.glVertex3f((float) i + 0.35F, (float) i_72_ + 0.35F, f);
	OpenGL.glColor4ub((byte) (i_80_ >> 16), (byte) (i_80_ >> 8),
			  (byte) i_80_, (byte) (i_80_ >> 24));
	OpenGL.glVertex3f((float) i_73_ + 0.35F, (float) i_74_ + 0.35F, f_75_);
	OpenGL.glColor4ub((byte) (i_81_ >> 16), (byte) (i_81_ >> 8),
			  (byte) i_81_, (byte) (i_81_ >> 24));
	OpenGL.glVertex3f((float) i_76_ + 0.35F, (float) i_77_ + 0.35F, f_78_);
	OpenGL.glEnd();
    }
    
    final Interface15 method15187(int i, byte[] is, int i_83_, boolean bool) {
	if (aBool9705 && (!bool || aBool9691))
	    return new Class134_Sub1(this, i, is, i_83_, bool);
	return new Class126_Sub1(this, i, is, i_83_);
    }
    
    void method15188() {
	aFloatArray9714[0] = aFloat9646 * aFloat9643;
	aFloatArray9714[1] = aFloat9646 * aFloat9644;
	aFloatArray9714[2] = aFloat9646 * aFloat9645;
	aFloatArray9714[3] = 1.0F;
	OpenGL.glLightModelfv(2899, aFloatArray9714, 0);
    }
    
    void method3680(int i, int i_84_, int i_85_, int i_86_, int i_87_) {
	method15223();
	method15246(i_87_);
	float f = (float) i + 0.35F;
	float f_88_ = (float) i_84_ + 0.35F;
	OpenGL.glColor4ub((byte) (i_86_ >> 16), (byte) (i_86_ >> 8),
			  (byte) i_86_, (byte) (i_86_ >> 24));
	OpenGL.glBegin(1);
	OpenGL.glVertex2f(f, f_88_);
	OpenGL.glVertex2f(f, f_88_ + (float) i_85_);
	OpenGL.glEnd();
    }
    
    public void method3303(int i, int i_89_, int i_90_, int i_91_, int i_92_,
			   int i_93_) {
	method15223();
	method15246(i_93_);
	float f = (float) i_90_ - (float) i;
	float f_94_ = (float) i_91_ - (float) i_89_;
	if (f == 0.0F && f_94_ == 0.0F)
	    f = 1.0F;
	else {
	    float f_95_
		= (float) (1.0 / Math.sqrt((double) (f * f + f_94_ * f_94_)));
	    f *= f_95_;
	    f_94_ *= f_95_;
	}
	OpenGL.glColor4ub((byte) (i_92_ >> 16), (byte) (i_92_ >> 8),
			  (byte) i_92_, (byte) (i_92_ >> 24));
	OpenGL.glBegin(1);
	OpenGL.glVertex2f((float) i + 0.35F, (float) i_89_ + 0.35F);
	OpenGL.glVertex2f((float) i_90_ + f + 0.35F,
			  (float) i_91_ + f_94_ + 0.35F);
	OpenGL.glEnd();
    }
    
    public Class163 method3407(int i, int i_96_, boolean bool,
			       boolean bool_97_) {
	return new Class163_Sub3(this, i, i_96_, bool);
    }
    
    boolean method15189() {
	if (aClass534_Sub9_Sub1_9581 != null) {
	    if (!aClass534_Sub9_Sub1_9581.method16109()) {
		if (aClass153_9592.method2554(aClass534_Sub9_Sub1_9581))
		    aClass161_9659.method2626();
		else
		    return false;
	    }
	    return true;
	}
	return false;
    }
    
    public final int method3411() {
	return anInt9580 + anInt9699 + anInt9669;
    }
    
    public void method3307(int i, int i_98_, int i_99_, int i_100_, int i_101_,
			   int i_102_, int i_103_) {
	OpenGL.glLineWidth((float) i_102_);
	method3303(i, i_98_, i_99_, i_100_, i_101_, i_103_);
	OpenGL.glLineWidth(1.0F);
    }
    
    public void method3367(float f, float f_104_, float f_105_, float[] fs) {
	float f_106_ = (aClass433_9607.aFloatArray4853[15]
			+ aClass433_9607.aFloatArray4853[3] * f
			+ aClass433_9607.aFloatArray4853[7] * f_104_
			+ aClass433_9607.aFloatArray4853[11] * f_105_);
	float f_107_ = (aClass433_9607.aFloatArray4853[12]
			+ aClass433_9607.aFloatArray4853[0] * f
			+ aClass433_9607.aFloatArray4853[4] * f_104_
			+ aClass433_9607.aFloatArray4853[8] * f_105_);
	float f_108_ = (aClass433_9607.aFloatArray4853[13]
			+ aClass433_9607.aFloatArray4853[1] * f
			+ aClass433_9607.aFloatArray4853[5] * f_104_
			+ aClass433_9607.aFloatArray4853[9] * f_105_);
	float f_109_ = (aClass433_9533.aFloatArray4853[14]
			+ aClass433_9533.aFloatArray4853[2] * f
			+ aClass433_9533.aFloatArray4853[6] * f_104_
			+ aClass433_9533.aFloatArray4853[10] * f_105_);
	fs[0] = aFloat9610 + aFloat9663 * f_107_ / f_106_;
	fs[1] = aFloat9612 + aFloat9613 * f_108_ / f_106_;
	fs[2] = f_109_;
    }
    
    public void method3366(float f, float f_110_, float f_111_, float[] fs) {
	float f_112_ = (aClass433_9607.aFloatArray4853[14]
			+ aClass433_9607.aFloatArray4853[2] * f
			+ aClass433_9607.aFloatArray4853[6] * f_110_
			+ aClass433_9607.aFloatArray4853[10] * f_111_);
	float f_113_ = (aClass433_9607.aFloatArray4853[15]
			+ aClass433_9607.aFloatArray4853[3] * f
			+ aClass433_9607.aFloatArray4853[7] * f_110_
			+ aClass433_9607.aFloatArray4853[11] * f_111_);
	if (f_112_ < -f_113_ || f_112_ > f_113_) {
	    float[] fs_114_ = fs;
	    float[] fs_115_ = fs;
	    fs[2] = Float.NaN;
	    fs_115_[1] = Float.NaN;
	    fs_114_[0] = Float.NaN;
	} else {
	    float f_116_ = (aClass433_9607.aFloatArray4853[12]
			    + aClass433_9607.aFloatArray4853[0] * f
			    + aClass433_9607.aFloatArray4853[4] * f_110_
			    + aClass433_9607.aFloatArray4853[8] * f_111_);
	    if (f_116_ < -f_113_ || f_116_ > f_113_) {
		float[] fs_117_ = fs;
		float[] fs_118_ = fs;
		fs[2] = Float.NaN;
		fs_118_[1] = Float.NaN;
		fs_117_[0] = Float.NaN;
	    } else {
		float f_119_ = (aClass433_9607.aFloatArray4853[13]
				+ aClass433_9607.aFloatArray4853[1] * f
				+ aClass433_9607.aFloatArray4853[5] * f_110_
				+ aClass433_9607.aFloatArray4853[9] * f_111_);
		if (f_119_ < -f_113_ || f_119_ > f_113_) {
		    float[] fs_120_ = fs;
		    float[] fs_121_ = fs;
		    fs[2] = Float.NaN;
		    fs_121_[1] = Float.NaN;
		    fs_120_[0] = Float.NaN;
		} else {
		    float f_122_
			= (aClass433_9533.aFloatArray4853[14]
			   + aClass433_9533.aFloatArray4853[2] * f
			   + aClass433_9533.aFloatArray4853[6] * f_110_
			   + aClass433_9533.aFloatArray4853[10] * f_111_);
		    fs[0] = aFloat9610 + aFloat9663 * f_116_ / f_113_;
		    fs[1] = aFloat9612 + aFloat9613 * f_119_ / f_113_;
		    fs[2] = f_122_;
		}
	    }
	}
    }
    
    public int method3308(int i, int i_123_, int i_124_, int i_125_,
			  int i_126_, int i_127_) {
	int i_128_ = 0;
	float f = (aClass433_9607.aFloatArray4853[14]
		   + aClass433_9607.aFloatArray4853[2] * (float) i
		   + aClass433_9607.aFloatArray4853[6] * (float) i_123_
		   + aClass433_9607.aFloatArray4853[10] * (float) i_124_);
	float f_129_ = (aClass433_9607.aFloatArray4853[14]
			+ aClass433_9607.aFloatArray4853[2] * (float) i_125_
			+ aClass433_9607.aFloatArray4853[6] * (float) i_126_
			+ aClass433_9607.aFloatArray4853[10] * (float) i_127_);
	float f_130_ = (aClass433_9607.aFloatArray4853[15]
			+ aClass433_9607.aFloatArray4853[3] * (float) i
			+ aClass433_9607.aFloatArray4853[7] * (float) i_123_
			+ aClass433_9607.aFloatArray4853[11] * (float) i_124_);
	float f_131_ = (aClass433_9607.aFloatArray4853[15]
			+ aClass433_9607.aFloatArray4853[3] * (float) i_125_
			+ aClass433_9607.aFloatArray4853[7] * (float) i_126_
			+ aClass433_9607.aFloatArray4853[11] * (float) i_127_);
	if (f < -f_130_ && f_129_ < -f_131_)
	    i_128_ |= 0x10;
	else if (f > f_130_ && f_129_ > f_131_)
	    i_128_ |= 0x20;
	float f_132_ = (aClass433_9607.aFloatArray4853[12]
			+ aClass433_9607.aFloatArray4853[0] * (float) i
			+ aClass433_9607.aFloatArray4853[4] * (float) i_123_
			+ aClass433_9607.aFloatArray4853[8] * (float) i_124_);
	float f_133_ = (aClass433_9607.aFloatArray4853[12]
			+ aClass433_9607.aFloatArray4853[0] * (float) i_125_
			+ aClass433_9607.aFloatArray4853[4] * (float) i_126_
			+ aClass433_9607.aFloatArray4853[8] * (float) i_127_);
	if (f_132_ < -f_130_ && f_133_ < -f_131_)
	    i_128_ |= 0x1;
	if (f_132_ > f_130_ && f_133_ > f_131_)
	    i_128_ |= 0x2;
	float f_134_ = (aClass433_9607.aFloatArray4853[13]
			+ aClass433_9607.aFloatArray4853[1] * (float) i
			+ aClass433_9607.aFloatArray4853[5] * (float) i_123_
			+ aClass433_9607.aFloatArray4853[9] * (float) i_124_);
	float f_135_ = (aClass433_9607.aFloatArray4853[13]
			+ aClass433_9607.aFloatArray4853[1] * (float) i_125_
			+ aClass433_9607.aFloatArray4853[5] * (float) i_126_
			+ aClass433_9607.aFloatArray4853[9] * (float) i_127_);
	if (f_134_ < -f_130_ && f_135_ < -f_131_)
	    i_128_ |= 0x4;
	if (f_134_ > f_130_ && f_135_ > f_131_)
	    i_128_ |= 0x8;
	return i_128_;
    }
    
    boolean method3310(int i, int i_136_, int i_137_, int i_138_,
		       Class446 class446, Class432 class432) {
	Class433 class433 = aClass433_9530;
	class433.method6916(class446);
	class433.method6839(aClass433_9607);
	return class432.method6838(i, i_136_, i_137_, i_138_, class433,
				   aFloat9610, aFloat9612, aFloat9663,
				   aFloat9613);
    }
    
    public void method3311(Class446 class446, Class194 class194,
			   Class432 class432) {
	Class433 class433 = aClass433_9530;
	class433.method6916(class446);
	class433.method6839(aClass433_9607);
	class194.method3800(class432, aClass433_9605, class433, aFloat9610,
			    aFloat9612, aFloat9663, aFloat9613);
    }
    
    public Class534_Sub2 method3312(int i) {
	Class534_Sub2_Sub2 class534_sub2_sub2 = new Class534_Sub2_Sub2(i);
	aClass700_9577.method14131(class534_sub2_sub2, (short) 789);
	return class534_sub2_sub2;
    }
    
    public void method3538(Class534_Sub2 class534_sub2) {
	aNativeHeap9639
	    = ((Class534_Sub2_Sub2) class534_sub2).aNativeHeap11743;
	if (anInterface15_9539 == null) {
	    Class534_Sub40_Sub2 class534_sub40_sub2
		= new Class534_Sub40_Sub2(80);
	    if (aBool9566) {
		class534_sub40_sub2.method18397(-1.0F);
		class534_sub40_sub2.method18397(-1.0F);
		class534_sub40_sub2.method18397(0.0F);
		class534_sub40_sub2.method18397(0.0F);
		class534_sub40_sub2.method18397(0.0F);
		class534_sub40_sub2.method18397(-1.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(0.0F);
		class534_sub40_sub2.method18397(0.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(0.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(-1.0F);
		class534_sub40_sub2.method18397(0.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(0.0F);
	    } else {
		class534_sub40_sub2.method18400(-1.0F);
		class534_sub40_sub2.method18400(-1.0F);
		class534_sub40_sub2.method18400(0.0F);
		class534_sub40_sub2.method18400(0.0F);
		class534_sub40_sub2.method18400(0.0F);
		class534_sub40_sub2.method18400(-1.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(0.0F);
		class534_sub40_sub2.method18400(0.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(0.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(-1.0F);
		class534_sub40_sub2.method18400(0.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(0.0F);
	    }
	    anInterface15_9539
		= method15218(20, class534_sub40_sub2.aByteArray10810,
			      class534_sub40_sub2.anInt10811 * 31645619,
			      false);
	    aClass129_9711 = new Class129(anInterface15_9539, 5126, 3, 0);
	    aClass129_9712 = new Class129(anInterface15_9539, 5126, 2, 12);
	    aClass148_9567.method2457(this);
	}
    }
    
    public Class163 method3315(int i, int i_139_, boolean bool,
			       boolean bool_140_) {
	return new Class163_Sub3(this, i, i_139_, bool);
    }
    
    final synchronized void method15190(int i, int i_141_) {
	Class534_Sub39 class534_sub39 = new Class534_Sub39(i_141_);
	class534_sub39.aLong7158 = (long) i * 936217890172187787L;
	aClass700_9584.method14131(class534_sub39, (short) 789);
    }
    
    public final void method3445(int i, int i_142_, int i_143_, int i_144_) {
	if (aClass175_1989 != null) {
	    if (i < 0)
		i = 0;
	    if (i_143_ > aClass175_1989.method2910())
		i_143_ = aClass175_1989.method2910();
	    if (i_142_ < 0)
		i_142_ = 0;
	    if (i_144_ > aClass175_1989.method2911())
		i_144_ = aClass175_1989.method2911();
	    anInt9690 = i;
	    anInt9625 = i_142_;
	    anInt9628 = i_143_;
	    anInt9626 = i_144_;
	    OpenGL.glEnable(3089);
	    method15198();
	}
    }
    
    public void method3384(int i, int i_145_, int i_146_, int i_147_,
			   int i_148_, int i_149_, Class145 class145,
			   int i_150_, int i_151_) {
	Class145_Sub3 class145_sub3 = (Class145_Sub3) class145;
	Class141_Sub2_Sub1 class141_sub2_sub1
	    = class145_sub3.aClass141_Sub2_Sub1_9876;
	method15224();
	method15231(class145_sub3.aClass141_Sub2_Sub1_9876);
	method15246(i_149_);
	method15232(7681, 8448);
	method15325(0, 34167, 768);
	float f = (class141_sub2_sub1.aFloat11378
		   / (float) class141_sub2_sub1.anInt11379);
	float f_152_ = (class141_sub2_sub1.aFloat11380
			/ (float) class141_sub2_sub1.anInt11381);
	float f_153_ = (float) i_146_ - (float) i;
	float f_154_ = (float) i_147_ - (float) i_145_;
	float f_155_ = (float) (1.0 / Math.sqrt((double) (f_153_ * f_153_
							  + f_154_ * f_154_)));
	f_153_ *= f_155_;
	f_154_ *= f_155_;
	OpenGL.glColor4ub((byte) (i_148_ >> 16), (byte) (i_148_ >> 8),
			  (byte) i_148_, (byte) (i_148_ >> 24));
	OpenGL.glBegin(1);
	OpenGL.glTexCoord2f(f * (float) (i - i_150_),
			    f_152_ * (float) (i_145_ - i_151_));
	OpenGL.glVertex2f((float) i + 0.35F, (float) i_145_ + 0.35F);
	OpenGL.glTexCoord2f(f * (float) (i_146_ - i_150_),
			    f_152_ * (float) (i_147_ - i_151_));
	OpenGL.glVertex2f((float) i_146_ + f_153_ + 0.35F,
			  (float) i_147_ + f_154_ + 0.35F);
	OpenGL.glEnd();
	method15325(0, 5890, 768);
    }
    
    public Class145 method3643(int i, int i_156_, int[] is, int[] is_157_) {
	return Class145_Sub3.method15559(this, i, i_156_, is, is_157_);
    }
    
    void method15191() {
	method15227(-2);
	for (int i = anInt9682 - 1; i >= 0; i--) {
	    method15230(i);
	    method15231(null);
	    OpenGL.glTexEnvi(8960, 8704, 34160);
	}
	method15232(8448, 8448);
	method15325(2, 34168, 770);
	method15238();
	anInt9708 = 1;
	anInt9596 = 0;
	aBool9553 = true;
	if (aBool9704)
	    OpenGL.glBlendFuncSeparate(770, 771, 0, 0);
	else
	    OpenGL.glBlendFunc(770, 771);
	anInt9636 = 1;
	aByte9673 = (byte) -1;
	method15251((byte) 0);
	aBool9597 = true;
	OpenGL.glEnable(3008);
	OpenGL.glAlphaFunc(516, (float) aByte9673);
	if (aBool9688) {
	    if (anInt9668 == 0)
		OpenGL.glDisable(32925);
	    else
		OpenGL.glEnable(32925);
	    OpenGL.glDisable(32926);
	}
	OpenGL.glColorMask(true, true, true, true);
	aBool9594 = true;
	method15344(true);
	method15432(true);
	method15242(true);
	method15193(true);
	method3284(0.0F, 1.0F);
	method15203();
	anOpenGL9559.setSwapInterval(0);
	OpenGL.glShadeModel(7425);
	OpenGL.glClearDepth(1.0F);
	OpenGL.glDepthFunc(515);
	OpenGL.glPolygonMode(1028, 6914);
	method15255(anInt9658);
	OpenGL.glMatrixMode(5888);
	OpenGL.glLoadIdentity();
	OpenGL.glColorMaterial(1028, 5634);
	OpenGL.glEnable(2903);
	float[] fs = { 0.0F, 0.0F, 0.0F, 1.0F };
	for (int i = 0; i < 8; i++) {
	    int i_158_ = 16384 + i;
	    OpenGL.glLightfv(i_158_, 4608, fs, 0);
	    OpenGL.glLightf(i_158_, 4615, 0.0F);
	    OpenGL.glLightf(i_158_, 4616, 0.0F);
	}
	OpenGL.glEnable(16384);
	OpenGL.glEnable(16385);
	OpenGL.glFogf(2914, 0.95F);
	OpenGL.glFogi(2917, 9729);
	OpenGL.glHint(3156, 4353);
	if (aBool9534)
	    OpenGL.glEnable(34895);
	anInt9687 = -1;
	anInt9642 = -1;
	method3281();
	method3537();
    }
    
    final void method15192(int i) {
	if (anInt9708 != i) {
	    int i_159_;
	    boolean bool;
	    boolean bool_160_;
	    boolean bool_161_;
	    if (i == 1) {
		i_159_ = 1;
		bool = true;
		bool_160_ = true;
		bool_161_ = true;
	    } else if (i == 2) {
		i_159_ = 2;
		bool = true;
		bool_160_ = false;
		bool_161_ = true;
	    } else if (i == 128) {
		i_159_ = 3;
		bool = true;
		bool_160_ = true;
		bool_161_ = true;
	    } else if (i == 3) {
		i_159_ = 0;
		bool = true;
		bool_160_ = true;
		bool_161_ = false;
	    } else {
		i_159_ = 0;
		bool = true;
		bool_160_ = false;
		bool_161_ = false;
	    }
	    if (bool != aBool9594) {
		OpenGL.glColorMask(bool, bool, bool, true);
		aBool9594 = bool;
	    }
	    if (bool_160_ != aBool9597) {
		aBool9597 = bool_160_;
		method15250();
	    }
	    if (bool_161_ != aBool9553) {
		aBool9553 = bool_161_;
		method15247();
	    }
	    if (i_159_ != anInt9636) {
		anInt9636 = i_159_;
		method15248();
	    }
	    anInt9708 = i;
	    anInt9591 &= ~0x1c;
	}
    }
    
    public void method3328(int i) {
	/* empty */
    }
    
    public Class183 method3329(Class187 class187, int i, int i_162_,
			       int i_163_, int i_164_) {
	return new Class183_Sub1(this, class187, i, i_163_, i_164_, i_162_);
    }
    
    public int method3330(int i, int i_165_) {
	return i & i_165_ ^ i_165_;
    }
    
    public final void method3531(int i, int i_166_, int i_167_, int i_168_) {
	if (aClass153_9592 != null)
	    aClass153_9592.method2552(i, i_166_, i_167_, i_168_);
    }
    
    final void method15193(boolean bool) {
	if (bool != aBool9600) {
	    aBool9600 = bool;
	    method15244();
	    anInt9591 &= ~0x1f;
	}
    }
    
    public Class433 method3518() {
	return aClass433_9655;
    }
    
    public Class446 method3665() {
	return aClass446_9570;
    }
    
    public Class151 method3509(int i, int i_169_, int[][] is, int[][] is_170_,
			       int i_171_, int i_172_, int i_173_) {
	return new Class151_Sub1(this, i_172_, i_173_, i, i_169_, is, is_170_,
				 i_171_);
    }
    
    public int method3500() {
	return 4;
    }
    
    public void method3364(int i, Class166 class166) {
	if (!aBool9695)
	    throw new RuntimeException("");
	anInt9664 = i;
	aClass166_9665 = class166;
	if (aBool9598) {
	    aClass143_9562.aClass136_Sub4_1653.method14478();
	    aClass143_9562.aClass136_Sub4_1653.method14477();
	}
    }
    
    public void method3529() {
	aBool9695 = false;
    }
    
    public void method3281() {
	anInt9648 = 0;
	anInt9632 = 0;
	anInt9633 = aClass175_1989.method2910();
	anInt9634 = aClass175_1989.method2911();
	method15197();
    }
    
    public void method3318(int i, int i_174_, int i_175_, int i_176_) {
	anInt9648 = i;
	anInt9632 = i_174_;
	anInt9633 = i_175_;
	anInt9634 = i_176_;
	method15197();
    }
    
    public void method3283(int[] is) {
	is[0] = anInt9648;
	is[1] = anInt9632;
	is[2] = anInt9633;
	is[3] = anInt9634;
    }
    
    public void method3426() {
	for (int i = 0; i < 3; i++) {
	    if (aLongArray9726[i] != 0L) {
		OpenGL.glDeleteSync(aLongArray9726[i]);
		aLongArray9726[i] = 0L;
	    }
	}
	anInt9729 = 0;
	anInt9728 = 0;
    }
    
    final void method15194() {
	if (anInt9622 != 2) {
	    anInt9622 = 2;
	    method15183(aClass433_9605.aFloatArray4853);
	    method15273();
	    method15197();
	    method15354();
	    anInt9591 &= ~0x7;
	}
    }
    
    public final void method3537() {
	if (aClass175_1989 != null) {
	    anInt9690 = 0;
	    anInt9625 = 0;
	    anInt9628 = aClass175_1989.method2910();
	    anInt9626 = aClass175_1989.method2911();
	    OpenGL.glDisable(3089);
	}
    }
    
    final synchronized void method15195(int i) {
	Class534_Sub39 class534_sub39 = new Class534_Sub39(i);
	aClass700_9585.method14131(class534_sub39, (short) 789);
    }
    
    public final void method3282(int i, int i_177_, int i_178_, int i_179_) {
	if (anInt9690 < i)
	    anInt9690 = i;
	if (anInt9628 > i_178_)
	    anInt9628 = i_178_;
	if (anInt9625 < i_177_)
	    anInt9625 = i_177_;
	if (anInt9626 > i_179_)
	    anInt9626 = i_179_;
	OpenGL.glEnable(3089);
	method15198();
    }
    
    final void method15196(int i, int i_180_) {
	anInt9629 = i;
	anInt9630 = i_180_;
	method15197();
	method15198();
    }
    
    final void method15197() {
	if (aClass175_1989 != null) {
	    int i;
	    int i_181_;
	    int i_182_;
	    int i_183_;
	    if (anInt9622 == 2) {
		i = anInt9648;
		i_181_ = anInt9632;
		i_182_ = anInt9633;
		i_183_ = anInt9634;
	    } else {
		i = 0;
		i_181_ = 0;
		i_182_ = aClass175_1989.method2910();
		i_183_ = aClass175_1989.method2911();
	    }
	    if (i_182_ < 1)
		i_182_ = 1;
	    if (i_183_ < 1)
		i_183_ = 1;
	    OpenGL.glViewport(anInt9629 + i,
			      (anInt9630 + aClass175_1989.method2911() - i_181_
			       - i_183_),
			      i_182_, i_183_);
	    aFloat9663 = (float) anInt9633 / 2.0F;
	    aFloat9613 = (float) anInt9634 / 2.0F;
	    aFloat9610 = (float) anInt9648 + aFloat9663;
	    aFloat9612 = (float) anInt9632 + aFloat9613;
	}
    }
    
    public void method3504(int i, Class534_Sub21[] class534_sub21s) {
	for (int i_184_ = 0; i_184_ < i; i_184_++)
	    aClass534_Sub21Array9702[i_184_] = class534_sub21s[i_184_];
	anInt9651 = i;
	if (anInt9622 != 1)
	    method15421();
    }
    
    final void method15198() {
	if (aClass175_1989 != null && anInt9690 < anInt9628
	    && anInt9625 < anInt9626)
	    OpenGL.glScissor(anInt9629 + anInt9690,
			     (anInt9630 + aClass175_1989.method2911()
			      - anInt9626),
			     anInt9628 - anInt9690, anInt9626 - anInt9625);
	else
	    OpenGL.glScissor(0, 0, 0, 0);
    }
    
    public void method3377() {
	OpenGL.glFinish();
    }
    
    final void method15199(Class433 class433) {
	OpenGL.glPushMatrix();
	OpenGL.glMultMatrixf(class433.aFloatArray4853, 0);
    }
    
    final void method15200(Class433 class433) {
	OpenGL.glLoadMatrixf(class433.aFloatArray4853, 0);
    }
    
    final void method15201() {
	OpenGL.glPopMatrix();
    }
    
    public final void method3335(Class446 class446) {
	aClass446_9602.method7236(class446);
	aClass433_9533.method6916(aClass446_9602);
	aClass446_9618.method7236(class446);
	aClass446_9618.method7243();
	aClass433_9604.method6916(aClass446_9618);
	method15207();
	if (anInt9622 != 1)
	    method15273();
    }
    
    void method3378(int i, int i_185_, int i_186_, int i_187_, int i_188_) {
	method15223();
	method15246(i_188_);
	float f = (float) i + 0.35F;
	float f_189_ = (float) i_185_ + 0.35F;
	OpenGL.glColor4ub((byte) (i_187_ >> 16), (byte) (i_187_ >> 8),
			  (byte) i_187_, (byte) (i_187_ >> 24));
	OpenGL.glBegin(1);
	OpenGL.glVertex2f(f, f_189_);
	OpenGL.glVertex2f(f, f_189_ + (float) i_186_);
	OpenGL.glEnd();
    }
    
    final void method15202(boolean bool) {
	if (bool != aBool9627) {
	    aBool9627 = bool;
	    method15241();
	    anInt9591 &= ~0x7;
	}
    }
    
    public final void method3338(Class433 class433) {
	aClass433_9605.method6842(class433);
	method15207();
	method15206();
    }
    
    public Class170 method3548(int[] is) {
	return new Class170_Sub2(this, is);
    }
    
    final void method15203() {
	if (anInt9622 != 0) {
	    anInt9622 = 0;
	    method15197();
	    method15354();
	    anInt9591 &= ~0x1f;
	}
    }
    
    final void method15204() {
	if (anInt9622 != 1) {
	    anInt9622 = 1;
	    method15206();
	    method15197();
	    method15354();
	    OpenGL.glMatrixMode(5888);
	    OpenGL.glLoadIdentity();
	    anInt9591 &= ~0x18;
	}
    }
    
    final void method15205(Interface15 interface15) {
	if (anInterface15_9593 != interface15) {
	    if (aBool9705)
		OpenGL.glBindBufferARB(34962, interface15.method1());
	    anInterface15_9593 = interface15;
	}
    }
    
    final void method15206() {
	aFloat9617 = aClass433_9605.method6907();
	aFloat9616 = aClass433_9605.method6859();
	method15305();
	if (anInt9622 == 2)
	    method15183(aClass433_9605.aFloatArray4853);
	else if (anInt9622 == 1)
	    method15183(aClass433_9606.aFloatArray4853);
    }
    
    final void method15207() {
	aClass433_9607.method6842(aClass433_9533);
	aClass433_9607.method6839(aClass433_9605);
	aClass433_9607.method6862(aFloatArrayArray9608[0]);
	aClass433_9607.method6858(aFloatArrayArray9608[1]);
	aClass433_9607.method6861(aFloatArrayArray9608[2]);
	aClass433_9607.method6949(aFloatArrayArray9608[3]);
	aClass433_9607.method6879(aFloatArrayArray9608[4]);
	aClass433_9607.method6945(aFloatArrayArray9608[5]);
    }
    
    final void method15208(int i, int i_190_) {
	if (anInt9672 == 0) {
	    boolean bool = false;
	    if (anInt9573 != i) {
		OpenGL.glTexEnvi(8960, 34161, i);
		anInt9573 = i;
		bool = true;
	    }
	    if (anInt9670 != i_190_) {
		OpenGL.glTexEnvi(8960, 34162, i_190_);
		anInt9670 = i_190_;
		bool = true;
	    }
	    if (bool)
		anInt9591 &= ~0x1d;
	} else {
	    OpenGL.glTexEnvi(8960, 34161, i);
	    OpenGL.glTexEnvi(8960, 34162, i_190_);
	}
    }
    
    public void method3461(boolean bool) {
	aBool9623 = bool;
	method15244();
    }
    
    public int method3344() {
	return 4;
    }
    
    public void method3654(int i, Class534_Sub21[] class534_sub21s) {
	for (int i_191_ = 0; i_191_ < i; i_191_++)
	    aClass534_Sub21Array9702[i_191_] = class534_sub21s[i_191_];
	anInt9651 = i;
	if (anInt9622 != 1)
	    method15421();
    }
    
    public void method3277(long l) {
	if (OpenGL.glUnmapBufferARB(35051)) {
	    /* empty */
	}
	OpenGL.glBindBufferARB(35051, 0);
    }
    
    void method15209() {
	method15422();
    }
    
    public boolean method3240() {
	return true;
    }
    
    public final void method3342(int i) {
	anInt9574 = 0;
	for (/**/; i > 1; i >>= 1)
	    anInt9574++;
	anInt9638 = 1 << anInt9574;
    }
    
    public final void method3463(int i, int i_192_, int i_193_) {
	if (anInt9687 != i || anInt9675 != i_192_ || anInt9657 != i_193_) {
	    anInt9687 = i;
	    anInt9675 = i_192_;
	    anInt9657 = i_193_;
	    method15305();
	    method15212();
	}
    }
    
    public void method3415() {
	if (aBool9703 && aClass175_1989 != null) {
	    int i = anInt9690;
	    int i_194_ = anInt9628;
	    int i_195_ = anInt9625;
	    int i_196_ = anInt9626;
	    method3537();
	    int i_197_ = anInt9648;
	    int i_198_ = anInt9632;
	    int i_199_ = anInt9633;
	    int i_200_ = anInt9634;
	    method3281();
	    OpenGL.glReadBuffer(1028);
	    OpenGL.glDrawBuffer(1029);
	    method15203();
	    method15344(false);
	    method15432(false);
	    method15242(false);
	    method15193(false);
	    method15231(null);
	    method15227(-2);
	    method15214(1);
	    method15246(0);
	    OpenGL.glMatrixMode(5889);
	    OpenGL.glLoadIdentity();
	    OpenGL.glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
	    OpenGL.glMatrixMode(5888);
	    OpenGL.glLoadIdentity();
	    OpenGL.glRasterPos2i(0, 0);
	    OpenGL.glCopyPixels(0, 0, aClass175_1989.method2910(),
				aClass175_1989.method2911(), 6144);
	    OpenGL.glFlush();
	    OpenGL.glReadBuffer(1029);
	    OpenGL.glDrawBuffer(1029);
	    method3373(i, i_195_, i_194_, i_196_);
	    method3318(i_197_, i_198_, i_199_, i_200_);
	}
    }
    
    void method15210() {
	aFloatArray9714[0] = aFloat9646 * aFloat9643;
	aFloatArray9714[1] = aFloat9646 * aFloat9644;
	aFloatArray9714[2] = aFloat9646 * aFloat9645;
	aFloatArray9714[3] = 1.0F;
	OpenGL.glLightModelfv(2899, aFloatArray9714, 0);
    }
    
    public void method3371(boolean bool) {
	/* empty */
    }
    
    final void method15211() {
	OpenGL.glPushMatrix();
    }
    
    void method15212() {
	if (aBool9654 && anInt9675 >= 0)
	    OpenGL.glEnable(2912);
	else
	    OpenGL.glDisable(2912);
    }
    
    final void method15213(int i) {
	if (anInt9596 != i) {
	    anInt9596 = i;
	    method15248();
	}
    }
    
    public Class165 method3485(int i) {
	return aBool9694 ? new Class165_Sub1_Sub2(this, i) : null;
    }
    
    final void method15214(int i) {
	if (i == 1)
	    method15232(7681, 7681);
	else if (i == 0)
	    method15232(8448, 8448);
	else if (i == 2)
	    method15232(34165, 7681);
	else if (i == 3)
	    method15232(260, 8448);
	else if (i == 4)
	    method15232(34023, 34023);
    }
    
    public final void method3349(Class165 class165) {
	aClass165_Sub1_9674 = (Class165_Sub1) class165;
    }
    
    final synchronized void method15215(int i, int i_201_) {
	Class534_Sub39 class534_sub39 = new Class534_Sub39(i_201_);
	class534_sub39.aLong7158 = (long) i * 936217890172187787L;
	aClass700_9586.method14131(class534_sub39, (short) 789);
    }
    
    public final void method3350(int i, int i_202_, int i_203_, int i_204_) {
	if (aClass153_9592 != null)
	    aClass153_9592.method2552(i, i_202_, i_203_, i_204_);
    }
    
    public final void method3351(int i, int i_205_) {
	if (aClass153_9592 != null)
	    aClass153_9592.method2553(i, i_205_);
    }
    
    public void method3278(int i, int i_206_, int[] is, int[] is_207_) {
	method15185(i, i_206_, is, is_207_);
    }
    
    boolean method15216() {
	if (aClass534_Sub9_Sub3_9565 != null) {
	    if (!aClass534_Sub9_Sub3_9565.method16109()) {
		if (aClass153_9592.method2554(aClass534_Sub9_Sub3_9565))
		    aClass161_9659.method2626();
		else
		    return false;
	    }
	    return true;
	}
	return false;
    }
    
    public boolean method3443() {
	return (aClass534_Sub9_Sub3_9565 != null
		&& aClass534_Sub9_Sub3_9565.method16109());
    }
    
    public void method3590(float f, float f_208_, float f_209_, float f_210_,
			   float f_211_) {
	Class534_Sub9_Sub3.aFloat11642 = f;
	Class534_Sub9_Sub3.aFloat11645 = f_208_;
	Class534_Sub9_Sub3.aFloat11641 = f_209_;
	Class534_Sub9_Sub3.aFloat11640 = f_210_;
	Class534_Sub9_Sub3.aFloat11643 = f_211_;
    }
    
    public Class170 method3605(int[] is) {
	return new Class170_Sub2(this, is);
    }
    
    public final int method3323() {
	return anInt9580 + anInt9699 + anInt9669;
    }
    
    public boolean method3304() {
	return (aClass534_Sub9_Sub1_9581 != null
		&& aClass534_Sub9_Sub1_9581.method16109());
    }
    
    public void method3357(Class170 class170, float f, Class170 class170_212_,
			   float f_213_, Class170 class170_214_,
			   float f_215_) {
	int i = 0;
	if (class170_214_ == null && f_215_ > 0.0F)
	    f_215_ = 0.0F;
	if (class170_212_ == null && f_213_ > 0.0F) {
	    class170_212_ = class170_214_;
	    class170_214_ = null;
	    f_213_ = f_215_;
	    f_215_ = 0.0F;
	}
	if (class170 == null && f > 0.0F) {
	    class170 = class170_212_;
	    class170_212_ = class170_214_;
	    class170_214_ = null;
	    f = f_213_;
	    f_213_ = f_215_;
	    f_215_ = 0.0F;
	}
	Class534_Sub9_Sub1.aClass170_Sub2Array11561[0]
	    = (Class170_Sub2) class170;
	Class534_Sub9_Sub1.aFloatArray11560[0] = f;
	if (f > 0.0F)
	    i++;
	Class534_Sub9_Sub1.aClass170_Sub2Array11561[1]
	    = (Class170_Sub2) class170_212_;
	Class534_Sub9_Sub1.aFloatArray11560[1] = f_213_;
	if (f_213_ > 0.0F)
	    i++;
	Class534_Sub9_Sub1.aClass170_Sub2Array11561[2]
	    = (Class170_Sub2) class170_214_;
	Class534_Sub9_Sub1.aFloatArray11560[2] = f_215_;
	if (f_215_ > 0.0F)
	    i++;
	Class534_Sub9_Sub1.anInt11555 = i;
	Class534_Sub9_Sub1.aFloat11554 = 1.0F - (f + f_213_ + f_215_);
    }
    
    public final boolean method3358() {
	if (aClass534_Sub9_Sub2_9564 != null) {
	    if (!aClass534_Sub9_Sub2_9564.method16109()) {
		if (aClass153_9592.method2554(aClass534_Sub9_Sub2_9564))
		    aClass161_9659.method2626();
		else
		    return false;
	    }
	    return true;
	}
	return false;
    }
    
    public final void method3359() {
	if (aClass534_Sub9_Sub2_9564 != null
	    && aClass534_Sub9_Sub2_9564.method16109()) {
	    aClass153_9592.method2555(aClass534_Sub9_Sub2_9564);
	    aClass161_9659.method2626();
	}
    }
    
    public final boolean method3409() {
	return (aClass534_Sub9_Sub2_9564 != null
		&& aClass534_Sub9_Sub2_9564.method16109());
    }
    
    final void method3362(float f, float f_216_, float f_217_, float f_218_,
			  float f_219_, float f_220_) {
	Class534_Sub9_Sub2.aFloat11615 = f;
	Class534_Sub9_Sub2.aFloat11614 = f_216_;
	Class534_Sub9_Sub2.aFloat11613 = f_217_;
    }
    
    public Class175_Sub1 method3263() {
	return new Class175_Sub1_Sub2(this);
    }
    
    public Interface21 method3265(int i, int i_221_) {
	return new Class534_Sub18_Sub1(this, Class181.aClass181_1956,
				       Class173.aClass173_1828, i, i_221_);
    }
    
    public Interface21 method3266(int i, int i_222_, int i_223_) {
	return new Class534_Sub18_Sub1(this, Class181.aClass181_1956,
				       Class173.aClass173_1828, i, i_222_,
				       i_223_);
    }
    
    public void method3588(int i, Class145 class145, int i_224_, int i_225_) {
	Class145_Sub3 class145_sub3 = (Class145_Sub3) class145;
	Class141_Sub2_Sub1 class141_sub2_sub1
	    = class145_sub3.aClass141_Sub2_Sub1_9876;
	method15224();
	method15231(class145_sub3.aClass141_Sub2_Sub1_9876);
	method15246(1);
	method15232(7681, 8448);
	method15325(0, 34167, 768);
	float f = (class141_sub2_sub1.aFloat11378
		   / (float) class141_sub2_sub1.anInt11379);
	float f_226_ = (class141_sub2_sub1.aFloat11380
			/ (float) class141_sub2_sub1.anInt11381);
	OpenGL.glColor4ub((byte) (i >> 16), (byte) (i >> 8), (byte) i,
			  (byte) (i >> 24));
	OpenGL.glBegin(7);
	OpenGL.glTexCoord2f(f * (float) (anInt9690 - i_224_),
			    f_226_ * (float) (anInt9625 - i_225_));
	OpenGL.glVertex2i(anInt9690, anInt9625);
	OpenGL.glTexCoord2f(f * (float) (anInt9690 - i_224_),
			    f_226_ * (float) (anInt9626 - i_225_));
	OpenGL.glVertex2i(anInt9690, anInt9626);
	OpenGL.glTexCoord2f(f * (float) (anInt9628 - i_224_),
			    f_226_ * (float) (anInt9626 - i_225_));
	OpenGL.glVertex2i(anInt9628, anInt9626);
	OpenGL.glTexCoord2f(f * (float) (anInt9628 - i_224_),
			    f_226_ * (float) (anInt9625 - i_225_));
	OpenGL.glVertex2i(anInt9628, anInt9625);
	OpenGL.glEnd();
	method15325(0, 5890, 768);
    }
    
    final Interface16 method15217(int i, byte[] is, int i_227_, boolean bool) {
	if (aBool9705 && (!bool || aBool9691))
	    return new Class134_Sub2(this, i, is, i_227_, bool);
	return new Class126_Sub2(this, i, is, i_227_);
    }
    
    final Interface15 method15218(int i, byte[] is, int i_228_, boolean bool) {
	if (aBool9705 && (!bool || aBool9691))
	    return new Class134_Sub1(this, i, is, i_228_, bool);
	return new Class126_Sub1(this, i, is, i_228_);
    }
    
    final Interface15 method15219(int i, Buffer buffer, int i_229_,
				  boolean bool) {
	if (aBool9705 && (!bool || aBool9691))
	    return new Class134_Sub1(this, i, buffer, i_229_, bool);
	return new Class126_Sub1(this, i, buffer);
    }
    
    final void method15220(Interface15 interface15) {
	if (anInterface15_9593 != interface15) {
	    if (aBool9705)
		OpenGL.glBindBufferARB(34962, interface15.method1());
	    anInterface15_9593 = interface15;
	}
    }
    
    public final void method3449(int i, int i_230_, int i_231_, int i_232_) {
	if (anInt9690 < i)
	    anInt9690 = i;
	if (anInt9628 > i_231_)
	    anInt9628 = i_231_;
	if (anInt9625 < i_230_)
	    anInt9625 = i_230_;
	if (anInt9626 > i_232_)
	    anInt9626 = i_232_;
	OpenGL.glEnable(3089);
	method15198();
    }
    
    final void method15221(Class129 class129, Class129 class129_233_,
			   Class129 class129_234_, Class129 class129_235_) {
	if (class129 != null) {
	    method15220(class129.anInterface15_1519);
	    OpenGL.glVertexPointer(class129.aByte1517, class129.aShort1516,
				   anInterface15_9593.method93(),
				   (anInterface15_9593.method94()
				    + (long) class129.aByte1518));
	    OpenGL.glEnableClientState(32884);
	} else
	    OpenGL.glDisableClientState(32884);
	if (class129_233_ != null) {
	    method15220(class129_233_.anInterface15_1519);
	    OpenGL.glNormalPointer(class129_233_.aShort1516,
				   anInterface15_9593.method93(),
				   (anInterface15_9593.method94()
				    + (long) class129_233_.aByte1518));
	    OpenGL.glEnableClientState(32885);
	} else
	    OpenGL.glDisableClientState(32885);
	if (class129_234_ != null) {
	    method15220(class129_234_.anInterface15_1519);
	    OpenGL.glColorPointer(class129_234_.aByte1517,
				  class129_234_.aShort1516,
				  anInterface15_9593.method93(),
				  (anInterface15_9593.method94()
				   + (long) class129_234_.aByte1518));
	    OpenGL.glEnableClientState(32886);
	} else
	    OpenGL.glDisableClientState(32886);
	if (class129_235_ != null) {
	    method15220(class129_235_.anInterface15_1519);
	    OpenGL.glTexCoordPointer(class129_235_.aByte1517,
				     class129_235_.aShort1516,
				     anInterface15_9593.method93(),
				     (anInterface15_9593.method94()
				      + (long) class129_235_.aByte1518));
	    OpenGL.glEnableClientState(32888);
	} else
	    OpenGL.glDisableClientState(32888);
    }
    
    public void method3284(float f, float f_236_) {
	aFloat9614 = f;
	aFloat9615 = f_236_;
	method15354();
    }
    
    final void method15222(Interface16 interface16, int i, int i_237_,
			   int i_238_) {
	method15290(interface16);
	OpenGL.glDrawElements(i, i_238_, 5123,
			      interface16.method2() + (long) (i_237_ * 2));
    }
    
    final void method15223() {
	if (anInt9591 != 1) {
	    method15204();
	    method15344(false);
	    method15432(false);
	    method15242(false);
	    method15193(false);
	    method15231(null);
	    method15227(-2);
	    method15214(1);
	    method15251((byte) 0);
	    anInt9591 = 1;
	}
    }
    
    final void method15224() {
	if (anInt9591 != 2) {
	    method15204();
	    method15344(false);
	    method15432(false);
	    method15242(false);
	    method15193(false);
	    method15227(-2);
	    method15251((byte) 0);
	    anInt9591 = 2;
	}
    }
    
    final void method15225() {
	if (anInt9591 != 4) {
	    method15204();
	    method15344(false);
	    method15432(false);
	    method15242(false);
	    method15193(false);
	    method15227(-2);
	    method15246(1);
	    method15251((byte) 0);
	    anInt9591 = 4;
	}
    }
    
    final void method15226() {
	if (anInt9591 != 8) {
	    method15194();
	    method15344(true);
	    method15242(true);
	    method15193(true);
	    method15246(1);
	    method15251((byte) 0);
	    anInt9591 = 8;
	}
    }
    
    final void method15227(int i) {
	method15228(i, true);
    }
    
    final void method15228(int i, boolean bool) {
	method15296(i, bool, true);
    }
    
    static int method15229(Class181 class181, Class173 class173) {
	if (class173 == Class173.aClass173_1830) {
	    switch (class181.anInt1953 * -939549997) {
	    default:
		throw new IllegalArgumentException();
	    case 0:
		return 6408;
	    case 4:
		return 6407;
	    case 1:
		return 6410;
	    case 6:
		return 6409;
	    case 8:
		return 6406;
	    }
	}
	if (class173 == Class173.aClass173_1829) {
	    switch (class181.anInt1953 * -939549997) {
	    case 3:
		return 33189;
	    case 8:
		return 32830;
	    case 1:
		return 36219;
	    case 0:
		return 32859;
	    case 4:
		return 32852;
	    case 6:
		return 32834;
	    default:
		throw new IllegalArgumentException();
	    }
	}
	if (class173 == Class173.aClass173_1828) {
	    switch (class181.anInt1953 * -939549997) {
	    default:
		throw new IllegalArgumentException();
	    case 3:
		return 33190;
	    }
	}
	if (class173 == Class173.aClass173_1832) {
	    switch (class181.anInt1953 * -939549997) {
	    case 0:
		return 34842;
	    case 4:
		return 34843;
	    case 8:
		return 34844;
	    default:
		throw new IllegalArgumentException();
	    case 6:
		return 34846;
	    case 1:
		return 34847;
	    }
	}
	if (class173 == Class173.aClass173_1826) {
	    switch (class181.anInt1953 * -939549997) {
	    case 1:
		return 34841;
	    default:
		throw new IllegalArgumentException();
	    case 6:
		return 34840;
	    case 4:
		return 34837;
	    case 0:
		return 34836;
	    case 8:
		return 34838;
	    }
	}
	throw new IllegalArgumentException();
    }
    
    final void method15230(int i) {
	if (anInt9672 != i) {
	    OpenGL.glActiveTexture(33984 + i);
	    anInt9672 = i;
	}
    }
    
    final void method15231(Class141 class141) {
	Class141 class141_239_ = aClass141Array9519[anInt9672];
	if (class141_239_ != class141) {
	    if (class141 != null) {
		if (class141_239_ != null) {
		    if (class141.anInt1628 != class141_239_.anInt1628) {
			OpenGL.glDisable(class141_239_.anInt1628);
			OpenGL.glEnable(class141.anInt1628);
		    }
		} else
		    OpenGL.glEnable(class141.anInt1628);
		OpenGL.glBindTexture(class141.anInt1628, class141.anInt1633);
	    } else
		OpenGL.glDisable(class141_239_.anInt1628);
	    aClass141Array9519[anInt9672] = class141;
	}
	anInt9591 &= ~0x11;
    }
    
    public final void method3416(int i, int i_240_, int i_241_, int i_242_) {
	if (aClass175_1989 != null) {
	    if (i < 0)
		i = 0;
	    if (i_241_ > aClass175_1989.method2910())
		i_241_ = aClass175_1989.method2910();
	    if (i_240_ < 0)
		i_240_ = 0;
	    if (i_242_ > aClass175_1989.method2911())
		i_242_ = aClass175_1989.method2911();
	    anInt9690 = i;
	    anInt9625 = i_240_;
	    anInt9628 = i_241_;
	    anInt9626 = i_242_;
	    OpenGL.glEnable(3089);
	    method15198();
	}
    }
    
    public boolean method3436() {
	return true;
    }
    
    final void method15232(int i, int i_243_) {
	if (anInt9672 == 0) {
	    boolean bool = false;
	    if (anInt9573 != i) {
		OpenGL.glTexEnvi(8960, 34161, i);
		anInt9573 = i;
		bool = true;
	    }
	    if (anInt9670 != i_243_) {
		OpenGL.glTexEnvi(8960, 34162, i_243_);
		anInt9670 = i_243_;
		bool = true;
	    }
	    if (bool)
		anInt9591 &= ~0x1d;
	} else {
	    OpenGL.glTexEnvi(8960, 34161, i);
	    OpenGL.glTexEnvi(8960, 34162, i_243_);
	}
    }
    
    final void method15233(float f, float f_244_) {
	aFloat9660 = f;
	aFloat9661 = f_244_;
	method15305();
    }
    
    final void method15234(boolean bool) {
	if (bool != aBool9654) {
	    aBool9654 = bool;
	    method15212();
	    anInt9591 &= ~0x1f;
	}
    }
    
    final void method15235(int i) {
	aFloatArray9714[0] = (float) (i & 0xff0000) / 1.671168E7F;
	aFloatArray9714[1] = (float) (i & 0xff00) / 65280.0F;
	aFloatArray9714[2] = (float) (i & 0xff) / 255.0F;
	aFloatArray9714[3] = (float) (i >>> 24) / 255.0F;
	OpenGL.glTexEnvfv(8960, 8705, aFloatArray9714, 0);
    }
    
    final void method15236(float f, float f_245_, float f_246_, float f_247_) {
	aFloatArray9714[0] = f;
	aFloatArray9714[1] = f_245_;
	aFloatArray9714[2] = f_246_;
	aFloatArray9714[3] = f_247_;
	OpenGL.glTexEnvfv(8960, 8705, aFloatArray9714, 0);
    }
    
    final void method15237(float f, float f_248_, float f_249_) {
	OpenGL.glMatrixMode(5890);
	if (aBool9671)
	    OpenGL.glLoadIdentity();
	OpenGL.glTranslatef(f, f_248_, f_249_);
	OpenGL.glMatrixMode(5888);
	aBool9671 = true;
    }
    
    final void method15238() {
	if (aBool9671) {
	    OpenGL.glMatrixMode(5890);
	    OpenGL.glLoadIdentity();
	    OpenGL.glMatrixMode(5888);
	    aBool9671 = false;
	}
    }
    
    final void method15239(int i, int i_250_, int i_251_) {
	OpenGL.glTexEnvi(8960, 34184 + i, i_250_);
	OpenGL.glTexEnvi(8960, 34200 + i, i_251_);
    }
    
    public void method3235(boolean bool) {
	aBool9623 = bool;
	method15244();
    }
    
    final void method15240(boolean bool) {
	if (bool != aBool9560) {
	    aBool9560 = bool;
	    method15241();
	}
    }
    
    void method15241() {
	if (aBool9627 && !aBool9560)
	    OpenGL.glEnable(2896);
	else
	    OpenGL.glDisable(2896);
    }
    
    final void method15242(boolean bool) {
	if (bool != aBool9599) {
	    aBool9599 = bool;
	    method15245();
	    anInt9591 &= ~0x1f;
	}
    }
    
    final void method15243(int i) {
	if (anInt9708 != i) {
	    int i_252_;
	    boolean bool;
	    boolean bool_253_;
	    boolean bool_254_;
	    if (i == 1) {
		i_252_ = 1;
		bool = true;
		bool_253_ = true;
		bool_254_ = true;
	    } else if (i == 2) {
		i_252_ = 2;
		bool = true;
		bool_253_ = false;
		bool_254_ = true;
	    } else if (i == 128) {
		i_252_ = 3;
		bool = true;
		bool_253_ = true;
		bool_254_ = true;
	    } else if (i == 3) {
		i_252_ = 0;
		bool = true;
		bool_253_ = true;
		bool_254_ = false;
	    } else {
		i_252_ = 0;
		bool = true;
		bool_253_ = false;
		bool_254_ = false;
	    }
	    if (bool != aBool9594) {
		OpenGL.glColorMask(bool, bool, bool, true);
		aBool9594 = bool;
	    }
	    if (bool_253_ != aBool9597) {
		aBool9597 = bool_253_;
		method15250();
	    }
	    if (bool_254_ != aBool9553) {
		aBool9553 = bool_254_;
		method15247();
	    }
	    if (i_252_ != anInt9636) {
		anInt9636 = i_252_;
		method15248();
	    }
	    anInt9708 = i;
	    anInt9591 &= ~0x1c;
	}
    }
    
    final void method15244() {
	OpenGL.glDepthMask(aBool9600 && aBool9623);
    }
    
    final void method15245() {
	if (aBool9599 && aBool9561)
	    OpenGL.glEnable(2929);
	else
	    OpenGL.glDisable(2929);
    }
    
    final void method15246(int i) {
	if (anInt9708 != i) {
	    int i_255_;
	    boolean bool;
	    boolean bool_256_;
	    boolean bool_257_;
	    if (i == 1) {
		i_255_ = 1;
		bool = true;
		bool_256_ = true;
		bool_257_ = true;
	    } else if (i == 2) {
		i_255_ = 2;
		bool = true;
		bool_256_ = false;
		bool_257_ = true;
	    } else if (i == 128) {
		i_255_ = 3;
		bool = true;
		bool_256_ = true;
		bool_257_ = true;
	    } else if (i == 3) {
		i_255_ = 0;
		bool = true;
		bool_256_ = true;
		bool_257_ = false;
	    } else {
		i_255_ = 0;
		bool = true;
		bool_256_ = false;
		bool_257_ = false;
	    }
	    if (bool != aBool9594) {
		OpenGL.glColorMask(bool, bool, bool, true);
		aBool9594 = bool;
	    }
	    if (bool_256_ != aBool9597) {
		aBool9597 = bool_256_;
		method15250();
	    }
	    if (bool_257_ != aBool9553) {
		aBool9553 = bool_257_;
		method15247();
	    }
	    if (i_255_ != anInt9636) {
		anInt9636 = i_255_;
		method15248();
	    }
	    anInt9708 = i;
	    anInt9591 &= ~0x1c;
	}
    }
    
    final void method15247() {
	if (aBool9553)
	    OpenGL.glEnable(3042);
	else
	    OpenGL.glDisable(3042);
    }
    
    final void method15248() {
	if (aBool9704) {
	    int i = 0;
	    int i_258_ = 0;
	    if (anInt9596 == 0) {
		i = 0;
		i_258_ = 0;
	    } else if (anInt9596 == 1) {
		i = 1;
		i_258_ = 0;
	    } else if (anInt9596 == 2) {
		i = 1;
		i_258_ = 1;
	    } else if (anInt9596 == 3) {
		i = 0;
		i_258_ = 1;
	    }
	    if (anInt9636 == 1)
		OpenGL.glBlendFuncSeparate(770, 771, i, i_258_);
	    else if (anInt9636 == 2)
		OpenGL.glBlendFuncSeparate(1, 1, i, i_258_);
	    else if (anInt9636 == 3)
		OpenGL.glBlendFuncSeparate(774, 1, i, i_258_);
	    else if (anInt9636 == 0)
		OpenGL.glBlendFuncSeparate(1, 0, i, i_258_);
	} else if (anInt9636 == 1) {
	    OpenGL.glEnable(3042);
	    OpenGL.glBlendFunc(770, 771);
	} else if (anInt9636 == 2) {
	    OpenGL.glEnable(3042);
	    OpenGL.glBlendFunc(1, 1);
	} else if (anInt9636 == 3) {
	    OpenGL.glEnable(3042);
	    OpenGL.glBlendFunc(774, 1);
	} else
	    OpenGL.glDisable(3042);
    }
    
    final void method15249(int i) {
	if (anInt9596 != i) {
	    anInt9596 = i;
	    method15248();
	}
    }
    
    final void method15250() {
	if (aBool9597)
	    OpenGL.glEnable(3008);
	else
	    OpenGL.glDisable(3008);
	OpenGL.glAlphaFunc(516, (float) (aByte9673 & 0xff) / 255.0F);
	if (anInt9668 > 0) {
	    if (aByte9673 == 0)
		OpenGL.glDisable(32926);
	    else
		OpenGL.glEnable(32926);
	}
    }
    
    final void method15251(byte i) {
	if (aByte9673 != i) {
	    aByte9673 = i;
	    if (i == 0) {
		method15249(2);
		method15246(1);
	    } else {
		method15249(3);
		method15246(3);
	    }
	    method15250();
	}
    }
    
    public boolean method3398() {
	return true;
    }
    
    public final synchronized void method3661(int i) {
	int i_259_ = 0;
	i &= 0x7fffffff;
	while (!aClass700_9595.method14142(1540456922)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9595.method14132((byte) -89);
	    anIntArray9590[i_259_++]
		= (int) (class534_sub39.aLong7158 * 8258869577519436579L);
	    anInt9580 -= class534_sub39.anInt10807 * -705967177;
	    if (i_259_ == 1000) {
		OpenGL.glDeleteBuffersARB(i_259_, anIntArray9590, 0);
		i_259_ = 0;
	    }
	}
	if (i_259_ > 0) {
	    OpenGL.glDeleteBuffersARB(i_259_, anIntArray9590, 0);
	    i_259_ = 0;
	}
	while (!aClass700_9584.method14142(-374795530)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9584.method14132((byte) -26);
	    anIntArray9590[i_259_++]
		= (int) (class534_sub39.aLong7158 * 8258869577519436579L);
	    anInt9699 -= class534_sub39.anInt10807 * -705967177;
	    if (i_259_ == 1000) {
		OpenGL.glDeleteTextures(i_259_, anIntArray9590, 0);
		i_259_ = 0;
	    }
	}
	if (i_259_ > 0) {
	    OpenGL.glDeleteTextures(i_259_, anIntArray9590, 0);
	    i_259_ = 0;
	}
	while (!aClass700_9585.method14142(-383188384)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9585.method14132((byte) -58);
	    anIntArray9590[i_259_++] = class534_sub39.anInt10807 * -705967177;
	    if (i_259_ == 1000) {
		OpenGL.glDeleteFramebuffersEXT(i_259_, anIntArray9590, 0);
		i_259_ = 0;
	    }
	}
	if (i_259_ > 0) {
	    OpenGL.glDeleteFramebuffersEXT(i_259_, anIntArray9590, 0);
	    i_259_ = 0;
	}
	while (!aClass700_9586.method14142(-340444806)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9586.method14132((byte) -2);
	    anIntArray9590[i_259_++]
		= (int) (class534_sub39.aLong7158 * 8258869577519436579L);
	    anInt9669 -= class534_sub39.anInt10807 * -705967177;
	    if (i_259_ == 1000) {
		OpenGL.glDeleteRenderbuffersEXT(i_259_, anIntArray9590, 0);
		i_259_ = 0;
	    }
	}
	if (i_259_ > 0) {
	    OpenGL.glDeleteRenderbuffersEXT(i_259_, anIntArray9590, 0);
	    boolean bool = false;
	}
	while (!aClass700_9653.method14142(-1573390521)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9653.method14132((byte) -76);
	    OpenGL.glDeleteLists((int) (class534_sub39.aLong7158
					* 8258869577519436579L),
				 class534_sub39.anInt10807 * -705967177);
	}
	while (!aClass700_9667.method14142(-840399190)) {
	    Class534 class534 = aClass700_9667.method14132((byte) -57);
	    OpenGL.glDeleteProgramARB((int) (class534.aLong7158
					     * 8258869577519436579L));
	}
	while (!aClass700_9588.method14142(-1134748498)) {
	    Class534 class534 = aClass700_9588.method14132((byte) -14);
	    OpenGL.glDeleteShader((int) (class534.aLong7158
					 * 8258869577519436579L));
	}
	while (!aClass700_9653.method14142(333910256)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9653.method14132((byte) -21);
	    OpenGL.glDeleteLists((int) (class534_sub39.aLong7158
					* 8258869577519436579L),
				 class534_sub39.anInt10807 * -705967177);
	}
	aClass161_9659.method2630();
	if (method3239() > 100663296
	    && Class250.method4604((byte) -68) > aLong9589 + 60000L) {
	    System.gc();
	    aLong9589 = Class250.method4604((byte) -119);
	}
	anInt9696 = i;
    }
    
    final synchronized void method15252(int i, int i_260_) {
	Class534_Sub39 class534_sub39 = new Class534_Sub39(i_260_);
	class534_sub39.aLong7158 = (long) i * 936217890172187787L;
	aClass700_9595.method14131(class534_sub39, (short) 789);
    }
    
    public int method3684() {
	if (aBool9692) {
	    if (aLongArray9726[anInt9728] == 0L)
		return -1;
	    int i = OpenGL.glClientWaitSync(aLongArray9726[anInt9728], 0, 0);
	    if (i == 37149) {
		method3656();
		return -1;
	    }
	    return i != 37147 ? anIntArray9727[anInt9728] : -1;
	}
	return -1;
    }
    
    final synchronized void method15253(int i) {
	Class534_Sub39 class534_sub39 = new Class534_Sub39(i);
	aClass700_9585.method14131(class534_sub39, (short) 789);
    }
    
    public void method3321(float f, float f_261_, float f_262_, float f_263_,
			   float f_264_) {
	Class534_Sub9_Sub3.aFloat11642 = f;
	Class534_Sub9_Sub3.aFloat11645 = f_261_;
	Class534_Sub9_Sub3.aFloat11641 = f_262_;
	Class534_Sub9_Sub3.aFloat11640 = f_263_;
	Class534_Sub9_Sub3.aFloat11643 = f_264_;
    }
    
    public final void method3607(int i, int i_265_, int i_266_) {
	if (anInt9687 != i || anInt9675 != i_265_ || anInt9657 != i_266_) {
	    anInt9687 = i;
	    anInt9675 = i_265_;
	    anInt9657 = i_266_;
	    method15305();
	    method15212();
	}
    }
    
    final synchronized void method15254(int i) {
	Class534 class534 = new Class534();
	class534.aLong7158 = (long) i * 936217890172187787L;
	aClass700_9667.method14131(class534, (short) 789);
    }
    
    void method15255(int i) {
	anInt9658 = i;
	if (anInt9658 == 1)
	    OpenGL.glDisable(2884);
	else {
	    OpenGL.glEnable(2884);
	    if (anInt9658 == 2)
		OpenGL.glCullFace(1029);
	    else if (anInt9658 == 3)
		OpenGL.glCullFace(1028);
	}
    }
    
    static int method15256(Class181 class181) {
	switch (class181.anInt1953 * -939549997) {
	case 4:
	    return 6407;
	case 8:
	    return 6406;
	case 1:
	    return 6410;
	case 0:
	    return 6408;
	case 3:
	    return 6402;
	default:
	    throw new IllegalStateException();
	case 6:
	    return 6409;
	}
    }
    
    static int method15257(Class181 class181, Class173 class173) {
	if (class173 == Class173.aClass173_1830) {
	    switch (class181.anInt1953 * -939549997) {
	    default:
		throw new IllegalArgumentException();
	    case 0:
		return 6408;
	    case 4:
		return 6407;
	    case 1:
		return 6410;
	    case 6:
		return 6409;
	    case 8:
		return 6406;
	    }
	}
	if (class173 == Class173.aClass173_1829) {
	    switch (class181.anInt1953 * -939549997) {
	    case 3:
		return 33189;
	    case 8:
		return 32830;
	    case 1:
		return 36219;
	    case 0:
		return 32859;
	    case 4:
		return 32852;
	    case 6:
		return 32834;
	    default:
		throw new IllegalArgumentException();
	    }
	}
	if (class173 == Class173.aClass173_1828) {
	    switch (class181.anInt1953 * -939549997) {
	    default:
		throw new IllegalArgumentException();
	    case 3:
		return 33190;
	    }
	}
	if (class173 == Class173.aClass173_1832) {
	    switch (class181.anInt1953 * -939549997) {
	    case 0:
		return 34842;
	    case 4:
		return 34843;
	    case 8:
		return 34844;
	    default:
		throw new IllegalArgumentException();
	    case 6:
		return 34846;
	    case 1:
		return 34847;
	    }
	}
	if (class173 == Class173.aClass173_1826) {
	    switch (class181.anInt1953 * -939549997) {
	    case 1:
		return 34841;
	    default:
		throw new IllegalArgumentException();
	    case 6:
		return 34840;
	    case 4:
		return 34837;
	    case 0:
		return 34836;
	    case 8:
		return 34838;
	    }
	}
	throw new IllegalArgumentException();
    }
    
    public Class163 method3602(int i, int i_267_, boolean bool,
			       boolean bool_268_) {
	return new Class163_Sub3(this, i, i_267_, bool);
    }
    
    final synchronized void method15258(int i) {
	Class534 class534 = new Class534();
	class534.aLong7158 = (long) i * 936217890172187787L;
	aClass700_9667.method14131(class534, (short) 789);
    }
    
    public Class176 method3288() {
	int i = -1;
	if (aString9678.indexOf("nvidia") != -1)
	    i = 4318;
	else if (aString9678.indexOf("intel") != -1)
	    i = 32902;
	else if (aString9678.indexOf("ati") != -1)
	    i = 4098;
	return new Class176(i, "OpenGL", anInt9579, aString9619, 0L, false);
    }
    
    public Class176 method3670() {
	int i = -1;
	if (aString9678.indexOf("nvidia") != -1)
	    i = 4318;
	else if (aString9678.indexOf("intel") != -1)
	    i = 32902;
	else if (aString9678.indexOf("ati") != -1)
	    i = 4098;
	return new Class176(i, "OpenGL", anInt9579, aString9619, 0L, false);
    }
    
    void method3375(int i, int i_269_) throws Exception_Sub7 {
	try {
	    aClass175_Sub2_2013.method15475();
	} catch (Exception exception) {
	    /* empty */
	}
	if (anInterface25_1997 != null)
	    anInterface25_1997.method33(-1981769356);
    }
    
    public void method3523() {
	if (aBool9692) {
	    aClass163_9722 = null;
	    if (aClass175_Sub1_9724 != null) {
		aClass175_Sub1_9724.method142();
		aClass175_Sub1_9724 = null;
	    }
	    OpenGL.glDeleteBuffersARB(3, anIntArray9725, 0);
	    for (int i = 0; i < 3; i++) {
		anIntArray9725[i] = 0;
		if (aLongArray9726[i] != 0L) {
		    OpenGL.glDeleteSync(aLongArray9726[i]);
		    aLongArray9726[i] = 0L;
		}
	    }
	} else {
	    aClass175_Sub1_9724 = null;
	    aClass163_9723 = null;
	    aClass163_9722 = null;
	}
    }
    
    long method15259(int i, int i_270_, int[] is, int[] is_271_) {
	if (aBool9692) {
	    if (aLongArray9726[anInt9728] != 0L) {
		OpenGL.glDeleteSync(aLongArray9726[anInt9728]);
		aLongArray9726[anInt9728] = 0L;
	    }
	    OpenGL.glBindBufferARB(35051, anIntArray9725[anInt9728]);
	    long l = OpenGL.glMapBufferARB(35051, 35000);
	    if (is != null) {
		int i_272_ = 0;
		for (int i_273_ = i_270_ - 1; i_273_ >= 0; i_273_--) {
		    for (int i_274_ = 0; i_274_ < i; i_274_++)
			is[i_272_++]
			    = anUnsafe9578.getInt(l + (long) ((i_273_ * i
							       + i_274_)
							      * 4));
		}
		if (OpenGL.glUnmapBufferARB(35051)) {
		    /* empty */
		}
		OpenGL.glBindBufferARB(35051, 0);
		l = 0L;
	    }
	    if (++anInt9728 >= 3)
		anInt9728 = 0;
	    return l;
	}
	if (aClass163_9723 == null)
	    method15399(i, i_270_);
	if (aClass163_9722 == null)
	    aClass163_9722 = method3319(0, 0, aClass175_1989.method2910(),
					aClass175_1989.method2911(), true);
	else
	    ((Class163_Sub3) aClass163_9722).method15485
		(0, 0, aClass175_1989.method2910(),
		 aClass175_1989.method2911(), 0, 0, true);
	method3260(aClass175_Sub1_9724, 716588085);
	method3340(1, -16777216);
	aClass163_9722.method2659(anInt2018 * -1555714981,
				  anInt2019 * -385311879,
				  anInt2020 * 1769547157,
				  anInt2021 * -740019615);
	aClass163_9723.method2653(0, 0, i, i_270_, is, is_271_, 0, i);
	method3261(aClass175_Sub1_9724, -11578496);
	return 0L;
    }
    
    public final synchronized void method3322(int i) {
	int i_275_ = 0;
	i &= 0x7fffffff;
	while (!aClass700_9595.method14142(157050896)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9595.method14132((byte) -9);
	    anIntArray9590[i_275_++]
		= (int) (class534_sub39.aLong7158 * 8258869577519436579L);
	    anInt9580 -= class534_sub39.anInt10807 * -705967177;
	    if (i_275_ == 1000) {
		OpenGL.glDeleteBuffersARB(i_275_, anIntArray9590, 0);
		i_275_ = 0;
	    }
	}
	if (i_275_ > 0) {
	    OpenGL.glDeleteBuffersARB(i_275_, anIntArray9590, 0);
	    i_275_ = 0;
	}
	while (!aClass700_9584.method14142(784904803)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9584.method14132((byte) -51);
	    anIntArray9590[i_275_++]
		= (int) (class534_sub39.aLong7158 * 8258869577519436579L);
	    anInt9699 -= class534_sub39.anInt10807 * -705967177;
	    if (i_275_ == 1000) {
		OpenGL.glDeleteTextures(i_275_, anIntArray9590, 0);
		i_275_ = 0;
	    }
	}
	if (i_275_ > 0) {
	    OpenGL.glDeleteTextures(i_275_, anIntArray9590, 0);
	    i_275_ = 0;
	}
	while (!aClass700_9585.method14142(-688959137)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9585.method14132((byte) -21);
	    anIntArray9590[i_275_++] = class534_sub39.anInt10807 * -705967177;
	    if (i_275_ == 1000) {
		OpenGL.glDeleteFramebuffersEXT(i_275_, anIntArray9590, 0);
		i_275_ = 0;
	    }
	}
	if (i_275_ > 0) {
	    OpenGL.glDeleteFramebuffersEXT(i_275_, anIntArray9590, 0);
	    i_275_ = 0;
	}
	while (!aClass700_9586.method14142(-624711119)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9586.method14132((byte) -74);
	    anIntArray9590[i_275_++]
		= (int) (class534_sub39.aLong7158 * 8258869577519436579L);
	    anInt9669 -= class534_sub39.anInt10807 * -705967177;
	    if (i_275_ == 1000) {
		OpenGL.glDeleteRenderbuffersEXT(i_275_, anIntArray9590, 0);
		i_275_ = 0;
	    }
	}
	if (i_275_ > 0) {
	    OpenGL.glDeleteRenderbuffersEXT(i_275_, anIntArray9590, 0);
	    boolean bool = false;
	}
	while (!aClass700_9653.method14142(1509703040)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9653.method14132((byte) -92);
	    OpenGL.glDeleteLists((int) (class534_sub39.aLong7158
					* 8258869577519436579L),
				 class534_sub39.anInt10807 * -705967177);
	}
	while (!aClass700_9667.method14142(961839039)) {
	    Class534 class534 = aClass700_9667.method14132((byte) -6);
	    OpenGL.glDeleteProgramARB((int) (class534.aLong7158
					     * 8258869577519436579L));
	}
	while (!aClass700_9588.method14142(911267749)) {
	    Class534 class534 = aClass700_9588.method14132((byte) -120);
	    OpenGL.glDeleteShader((int) (class534.aLong7158
					 * 8258869577519436579L));
	}
	while (!aClass700_9653.method14142(1437217647)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9653.method14132((byte) -28);
	    OpenGL.glDeleteLists((int) (class534_sub39.aLong7158
					* 8258869577519436579L),
				 class534_sub39.anInt10807 * -705967177);
	}
	aClass161_9659.method2630();
	if (method3239() > 100663296
	    && Class250.method4604((byte) -61) > aLong9589 + 60000L) {
	    System.gc();
	    aLong9589 = Class250.method4604((byte) -9);
	}
	anInt9696 = i;
    }
    
    public final synchronized void method3686(int i) {
	int i_276_ = 0;
	i &= 0x7fffffff;
	while (!aClass700_9595.method14142(-1376566595)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9595.method14132((byte) -105);
	    anIntArray9590[i_276_++]
		= (int) (class534_sub39.aLong7158 * 8258869577519436579L);
	    anInt9580 -= class534_sub39.anInt10807 * -705967177;
	    if (i_276_ == 1000) {
		OpenGL.glDeleteBuffersARB(i_276_, anIntArray9590, 0);
		i_276_ = 0;
	    }
	}
	if (i_276_ > 0) {
	    OpenGL.glDeleteBuffersARB(i_276_, anIntArray9590, 0);
	    i_276_ = 0;
	}
	while (!aClass700_9584.method14142(-1179153862)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9584.method14132((byte) -81);
	    anIntArray9590[i_276_++]
		= (int) (class534_sub39.aLong7158 * 8258869577519436579L);
	    anInt9699 -= class534_sub39.anInt10807 * -705967177;
	    if (i_276_ == 1000) {
		OpenGL.glDeleteTextures(i_276_, anIntArray9590, 0);
		i_276_ = 0;
	    }
	}
	if (i_276_ > 0) {
	    OpenGL.glDeleteTextures(i_276_, anIntArray9590, 0);
	    i_276_ = 0;
	}
	while (!aClass700_9585.method14142(-20528715)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9585.method14132((byte) -60);
	    anIntArray9590[i_276_++] = class534_sub39.anInt10807 * -705967177;
	    if (i_276_ == 1000) {
		OpenGL.glDeleteFramebuffersEXT(i_276_, anIntArray9590, 0);
		i_276_ = 0;
	    }
	}
	if (i_276_ > 0) {
	    OpenGL.glDeleteFramebuffersEXT(i_276_, anIntArray9590, 0);
	    i_276_ = 0;
	}
	while (!aClass700_9586.method14142(222740325)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9586.method14132((byte) -14);
	    anIntArray9590[i_276_++]
		= (int) (class534_sub39.aLong7158 * 8258869577519436579L);
	    anInt9669 -= class534_sub39.anInt10807 * -705967177;
	    if (i_276_ == 1000) {
		OpenGL.glDeleteRenderbuffersEXT(i_276_, anIntArray9590, 0);
		i_276_ = 0;
	    }
	}
	if (i_276_ > 0) {
	    OpenGL.glDeleteRenderbuffersEXT(i_276_, anIntArray9590, 0);
	    boolean bool = false;
	}
	while (!aClass700_9653.method14142(-322048012)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9653.method14132((byte) -100);
	    OpenGL.glDeleteLists((int) (class534_sub39.aLong7158
					* 8258869577519436579L),
				 class534_sub39.anInt10807 * -705967177);
	}
	while (!aClass700_9667.method14142(125191518)) {
	    Class534 class534 = aClass700_9667.method14132((byte) -111);
	    OpenGL.glDeleteProgramARB((int) (class534.aLong7158
					     * 8258869577519436579L));
	}
	while (!aClass700_9588.method14142(-1372515408)) {
	    Class534 class534 = aClass700_9588.method14132((byte) -50);
	    OpenGL.glDeleteShader((int) (class534.aLong7158
					 * 8258869577519436579L));
	}
	while (!aClass700_9653.method14142(-1163366387)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9653.method14132((byte) -57);
	    OpenGL.glDeleteLists((int) (class534_sub39.aLong7158
					* 8258869577519436579L),
				 class534_sub39.anInt10807 * -705967177);
	}
	aClass161_9659.method2630();
	if (method3239() > 100663296
	    && Class250.method4604((byte) -119) > aLong9589 + 60000L) {
	    System.gc();
	    aLong9589 = Class250.method4604((byte) -61);
	}
	anInt9696 = i;
    }
    
    public final synchronized void method3389(int i) {
	int i_277_ = 0;
	i &= 0x7fffffff;
	while (!aClass700_9595.method14142(-1646776421)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9595.method14132((byte) -65);
	    anIntArray9590[i_277_++]
		= (int) (class534_sub39.aLong7158 * 8258869577519436579L);
	    anInt9580 -= class534_sub39.anInt10807 * -705967177;
	    if (i_277_ == 1000) {
		OpenGL.glDeleteBuffersARB(i_277_, anIntArray9590, 0);
		i_277_ = 0;
	    }
	}
	if (i_277_ > 0) {
	    OpenGL.glDeleteBuffersARB(i_277_, anIntArray9590, 0);
	    i_277_ = 0;
	}
	while (!aClass700_9584.method14142(990739902)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9584.method14132((byte) -20);
	    anIntArray9590[i_277_++]
		= (int) (class534_sub39.aLong7158 * 8258869577519436579L);
	    anInt9699 -= class534_sub39.anInt10807 * -705967177;
	    if (i_277_ == 1000) {
		OpenGL.glDeleteTextures(i_277_, anIntArray9590, 0);
		i_277_ = 0;
	    }
	}
	if (i_277_ > 0) {
	    OpenGL.glDeleteTextures(i_277_, anIntArray9590, 0);
	    i_277_ = 0;
	}
	while (!aClass700_9585.method14142(484537995)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9585.method14132((byte) -25);
	    anIntArray9590[i_277_++] = class534_sub39.anInt10807 * -705967177;
	    if (i_277_ == 1000) {
		OpenGL.glDeleteFramebuffersEXT(i_277_, anIntArray9590, 0);
		i_277_ = 0;
	    }
	}
	if (i_277_ > 0) {
	    OpenGL.glDeleteFramebuffersEXT(i_277_, anIntArray9590, 0);
	    i_277_ = 0;
	}
	while (!aClass700_9586.method14142(-479208733)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9586.method14132((byte) -17);
	    anIntArray9590[i_277_++]
		= (int) (class534_sub39.aLong7158 * 8258869577519436579L);
	    anInt9669 -= class534_sub39.anInt10807 * -705967177;
	    if (i_277_ == 1000) {
		OpenGL.glDeleteRenderbuffersEXT(i_277_, anIntArray9590, 0);
		i_277_ = 0;
	    }
	}
	if (i_277_ > 0) {
	    OpenGL.glDeleteRenderbuffersEXT(i_277_, anIntArray9590, 0);
	    boolean bool = false;
	}
	while (!aClass700_9653.method14142(-1727964886)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9653.method14132((byte) -31);
	    OpenGL.glDeleteLists((int) (class534_sub39.aLong7158
					* 8258869577519436579L),
				 class534_sub39.anInt10807 * -705967177);
	}
	while (!aClass700_9667.method14142(-457479750)) {
	    Class534 class534 = aClass700_9667.method14132((byte) -86);
	    OpenGL.glDeleteProgramARB((int) (class534.aLong7158
					     * 8258869577519436579L));
	}
	while (!aClass700_9588.method14142(1286549608)) {
	    Class534 class534 = aClass700_9588.method14132((byte) -36);
	    OpenGL.glDeleteShader((int) (class534.aLong7158
					 * 8258869577519436579L));
	}
	while (!aClass700_9653.method14142(1110400680)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9653.method14132((byte) -109);
	    OpenGL.glDeleteLists((int) (class534_sub39.aLong7158
					* 8258869577519436579L),
				 class534_sub39.anInt10807 * -705967177);
	}
	aClass161_9659.method2630();
	if (method3239() > 100663296
	    && Class250.method4604((byte) -103) > aLong9589 + 60000L) {
	    System.gc();
	    aLong9589 = Class250.method4604((byte) -69);
	}
	anInt9696 = i;
    }
    
    public void method3245() {
	for (int i = 0; i < 3; i++) {
	    if (aLongArray9726[i] != 0L) {
		OpenGL.glDeleteSync(aLongArray9726[i]);
		aLongArray9726[i] = 0L;
	    }
	}
	anInt9729 = 0;
	anInt9728 = 0;
    }
    
    final void method15260() {
	if (aBool9671) {
	    OpenGL.glMatrixMode(5890);
	    OpenGL.glLoadIdentity();
	    OpenGL.glMatrixMode(5888);
	    aBool9671 = false;
	}
    }
    
    final void method15261() {
	if (anInt9591 != 1) {
	    method15204();
	    method15344(false);
	    method15432(false);
	    method15242(false);
	    method15193(false);
	    method15231(null);
	    method15227(-2);
	    method15214(1);
	    method15251((byte) 0);
	    anInt9591 = 1;
	}
    }
    
    public final void method3510(Class433 class433) {
	aClass433_9605.method6842(class433);
	method15207();
	method15206();
    }
    
    public boolean method3385() {
	return true;
    }
    
    public boolean method3386() {
	return (aClass534_Sub9_Sub2_9564 != null
		&& (anInt9668 <= 1 || aBool9689));
    }
    
    public boolean method3592() {
	return true;
    }
    
    public boolean method3388() {
	return true;
    }
    
    public boolean method3474() {
	return true;
    }
    
    final void method15262() {
	aClass433_9607.method6842(aClass433_9533);
	aClass433_9607.method6839(aClass433_9605);
	aClass433_9607.method6862(aFloatArrayArray9608[0]);
	aClass433_9607.method6858(aFloatArrayArray9608[1]);
	aClass433_9607.method6861(aFloatArrayArray9608[2]);
	aClass433_9607.method6949(aFloatArrayArray9608[3]);
	aClass433_9607.method6879(aFloatArrayArray9608[4]);
	aClass433_9607.method6945(aFloatArrayArray9608[5]);
    }
    
    final void method15263() {
	if (anInt9622 != 0) {
	    anInt9622 = 0;
	    method15197();
	    method15354();
	    anInt9591 &= ~0x1f;
	}
    }
    
    final void method15264() {
	OpenGL.glPushMatrix();
    }
    
    public boolean method3393() {
	return aBool9688 && (!method3409() || aBool9689);
    }
    
    final synchronized void method15265(long l) {
	Class534 class534 = new Class534();
	class534.aLong7158 = l * 936217890172187787L;
	aClass700_9588.method14131(class534, (short) 789);
    }
    
    public boolean method3540() {
	return (aClass534_Sub9_Sub3_9565 != null
		&& aClass534_Sub9_Sub3_9565.method16109());
    }
    
    public boolean method3326() {
	return true;
    }
    
    public boolean method3397() {
	return true;
    }
    
    public final boolean method3352() {
	if (aClass153_9592 == null)
	    return false;
	return aClass153_9592.method2564();
    }
    
    public boolean method3608() {
	return false;
    }
    
    final void method15266() {
	if (anInt9591 != 8) {
	    method15194();
	    method15344(true);
	    method15242(true);
	    method15193(true);
	    method15246(1);
	    method15251((byte) 0);
	    anInt9591 = 8;
	}
    }
    
    boolean method15267() {
	if (aClass534_Sub9_Sub3_9565 != null) {
	    if (!aClass534_Sub9_Sub3_9565.method16109()) {
		if (aClass153_9592.method2554(aClass534_Sub9_Sub3_9565))
		    aClass161_9659.method2626();
		else
		    return false;
	    }
	    return true;
	}
	return false;
    }
    
    public boolean method3241() {
	return true;
    }
    
    public Class163 method3319(int i, int i_278_, int i_279_, int i_280_,
			       boolean bool) {
	return new Class163_Sub3(this, i, i_278_, i_279_, i_280_);
    }
    
    final void method15268() {
	if (aBool9597)
	    OpenGL.glEnable(3008);
	else
	    OpenGL.glDisable(3008);
	OpenGL.glAlphaFunc(516, (float) (aByte9673 & 0xff) / 255.0F);
	if (anInt9668 > 0) {
	    if (aByte9673 == 0)
		OpenGL.glDisable(32926);
	    else
		OpenGL.glEnable(32926);
	}
    }
    
    final void method15269(int i) {
	method15228(i, true);
    }
    
    public void method3406(boolean bool) {
	/* empty */
    }
    
    public void method3334(Class174 class174) {
	aClass148_9567.method2458(this, class174);
    }
    
    Class175_Sub2 method3408(Canvas canvas, int i, int i_281_) {
	return new Class175_Sub2_Sub2(this, canvas);
    }
    
    public int[] method3597(int i, int i_282_, int i_283_, int i_284_) {
	if (aClass175_1989 != null) {
	    int[] is = new int[i_283_ * i_284_];
	    int i_285_ = aClass175_1989.method2911();
	    for (int i_286_ = 0; i_286_ < i_284_; i_286_++)
		OpenGL.glReadPixelsi(i, i_285_ - i_282_ - i_286_ - 1, i_283_,
				     1, 32993, anInt9620, is, i_286_ * i_283_);
	    return is;
	}
	return null;
    }
    
    final void method15270(int i) {
	if (anInt9672 != i) {
	    OpenGL.glActiveTexture(33984 + i);
	    anInt9672 = i;
	}
    }
    
    static {
	anInt9563 = 4;
	aFloatArray9714 = new float[4];
	aFloatArray9715 = new float[4];
    }
    
    public void method3412() {
	if (aBool9703 && aClass175_1989 != null) {
	    int i = anInt9690;
	    int i_287_ = anInt9628;
	    int i_288_ = anInt9625;
	    int i_289_ = anInt9626;
	    method3537();
	    int i_290_ = anInt9648;
	    int i_291_ = anInt9632;
	    int i_292_ = anInt9633;
	    int i_293_ = anInt9634;
	    method3281();
	    OpenGL.glReadBuffer(1028);
	    OpenGL.glDrawBuffer(1029);
	    method15203();
	    method15344(false);
	    method15432(false);
	    method15242(false);
	    method15193(false);
	    method15231(null);
	    method15227(-2);
	    method15214(1);
	    method15246(0);
	    OpenGL.glMatrixMode(5889);
	    OpenGL.glLoadIdentity();
	    OpenGL.glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
	    OpenGL.glMatrixMode(5888);
	    OpenGL.glLoadIdentity();
	    OpenGL.glRasterPos2i(0, 0);
	    OpenGL.glCopyPixels(0, 0, aClass175_1989.method2910(),
				aClass175_1989.method2911(), 6144);
	    OpenGL.glFlush();
	    OpenGL.glReadBuffer(1029);
	    OpenGL.glDrawBuffer(1029);
	    method3373(i, i_288_, i_287_, i_289_);
	    method3318(i_290_, i_291_, i_292_, i_293_);
	}
    }
    
    public void method3413() {
	if (aBool9703 && aClass175_1989 != null) {
	    int i = anInt9690;
	    int i_294_ = anInt9628;
	    int i_295_ = anInt9625;
	    int i_296_ = anInt9626;
	    method3537();
	    int i_297_ = anInt9648;
	    int i_298_ = anInt9632;
	    int i_299_ = anInt9633;
	    int i_300_ = anInt9634;
	    method3281();
	    OpenGL.glReadBuffer(1028);
	    OpenGL.glDrawBuffer(1029);
	    method15203();
	    method15344(false);
	    method15432(false);
	    method15242(false);
	    method15193(false);
	    method15231(null);
	    method15227(-2);
	    method15214(1);
	    method15246(0);
	    OpenGL.glMatrixMode(5889);
	    OpenGL.glLoadIdentity();
	    OpenGL.glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
	    OpenGL.glMatrixMode(5888);
	    OpenGL.glLoadIdentity();
	    OpenGL.glRasterPos2i(0, 0);
	    OpenGL.glCopyPixels(0, 0, aClass175_1989.method2910(),
				aClass175_1989.method2911(), 6144);
	    OpenGL.glFlush();
	    OpenGL.glReadBuffer(1029);
	    OpenGL.glDrawBuffer(1029);
	    method3373(i, i_295_, i_294_, i_296_);
	    method3318(i_297_, i_298_, i_299_, i_300_);
	}
    }
    
    public void method3414() {
	if (aBool9703 && aClass175_1989 != null) {
	    int i = anInt9690;
	    int i_301_ = anInt9628;
	    int i_302_ = anInt9625;
	    int i_303_ = anInt9626;
	    method3537();
	    int i_304_ = anInt9648;
	    int i_305_ = anInt9632;
	    int i_306_ = anInt9633;
	    int i_307_ = anInt9634;
	    method3281();
	    OpenGL.glReadBuffer(1028);
	    OpenGL.glDrawBuffer(1029);
	    method15203();
	    method15344(false);
	    method15432(false);
	    method15242(false);
	    method15193(false);
	    method15231(null);
	    method15227(-2);
	    method15214(1);
	    method15246(0);
	    OpenGL.glMatrixMode(5889);
	    OpenGL.glLoadIdentity();
	    OpenGL.glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
	    OpenGL.glMatrixMode(5888);
	    OpenGL.glLoadIdentity();
	    OpenGL.glRasterPos2i(0, 0);
	    OpenGL.glCopyPixels(0, 0, aClass175_1989.method2910(),
				aClass175_1989.method2911(), 6144);
	    OpenGL.glFlush();
	    OpenGL.glReadBuffer(1029);
	    OpenGL.glDrawBuffer(1029);
	    method3373(i, i_302_, i_301_, i_303_);
	    method3318(i_304_, i_305_, i_306_, i_307_);
	}
    }
    
    public boolean method3585() {
	return true;
    }
    
    final void method15271(int i) {
	if (i == 1)
	    method15232(7681, 7681);
	else if (i == 0)
	    method15232(8448, 8448);
	else if (i == 2)
	    method15232(34165, 7681);
	else if (i == 3)
	    method15232(260, 8448);
	else if (i == 4)
	    method15232(34023, 34023);
    }
    
    public boolean method3418() {
	if (aBool9692 && aLongArray9726[anInt9729] != 0L)
	    return false;
	return true;
    }
    
    public boolean method3419() {
	return true;
    }
    
    public Class163 method3316(int[] is, int i, int i_308_, int i_309_,
			       int i_310_, boolean bool) {
	return new Class163_Sub3(this, i_309_, i_310_, is, i, i_308_);
    }
    
    public Class446 method3676() {
	return new Class446(aClass446_9602);
    }
    
    public boolean method3392() {
	return aBool9688 && (!method3409() || aBool9689);
    }
    
    public int method3524() {
	if (aBool9692) {
	    if (aLongArray9726[anInt9728] == 0L)
		return -1;
	    int i = OpenGL.glClientWaitSync(aLongArray9726[anInt9728], 0, 0);
	    if (i == 37149) {
		method3656();
		return -1;
	    }
	    return i != 37147 ? anIntArray9727[anInt9728] : -1;
	}
	return -1;
    }
    
    public void method3424(int i, int i_311_, int i_312_) {
	method3559();
	if (aClass175_Sub1_9724 == null)
	    method15399(i_311_, i_312_);
	if (aClass163_9722 == null)
	    aClass163_9722 = method3319(0, 0, aClass175_1989.method2910(),
					aClass175_1989.method2911(), true);
	else
	    ((Class163_Sub3) aClass163_9722).method15485
		(0, 0, aClass175_1989.method2910(),
		 aClass175_1989.method2911(), 0, 0, true);
	method3260(aClass175_Sub1_9724, -1152085975);
	method3340(1, -16777216);
	aClass163_9722.method2659(anInt2018 * -1555714981,
				  anInt2019 * -385311879,
				  anInt2020 * 1769547157,
				  anInt2021 * -740019615);
	OpenGL.glBindBufferARB(35051, anIntArray9725[anInt9729]);
	OpenGL.glReadPixelsub(0, 0, anInt2016 * 1104963955,
			      anInt2017 * 1827315157, 32993, 5121, null, 0);
	OpenGL.glBindBufferARB(35051, 0);
	method3261(aClass175_Sub1_9724, -11578496);
	aLongArray9726[anInt9729] = OpenGL.glFenceSync(37143, 0);
	anIntArray9727[anInt9729] = i;
	if (++anInt9729 >= 3)
	    anInt9729 = 0;
	method3446();
    }
    
    public Class433 method3336() {
	return aClass433_9655;
    }
    
    final synchronized void method15272(int i) {
	Class534_Sub39 class534_sub39 = new Class534_Sub39(i);
	aClass700_9585.method14131(class534_sub39, (short) 789);
    }
    
    final void method15273() {
	OpenGL.glLoadIdentity();
	OpenGL.glMultMatrixf(aClass433_9533.aFloatArray4853, 0);
	if (aBool9598)
	    aClass143_9562.aClass136_Sub4_1653.method14478();
	method15184();
	method15421();
    }
    
    public void method3428() {
	for (int i = 0; i < 3; i++) {
	    if (aLongArray9726[i] != 0L) {
		OpenGL.glDeleteSync(aLongArray9726[i]);
		aLongArray9726[i] = 0L;
	    }
	}
	anInt9729 = 0;
	anInt9728 = 0;
    }
    
    public void method3230() {
	for (int i = 0; i < 3; i++) {
	    if (aLongArray9726[i] != 0L) {
		OpenGL.glDeleteSync(aLongArray9726[i]);
		aLongArray9726[i] = 0L;
	    }
	}
	anInt9729 = 0;
	anInt9728 = 0;
    }
    
    public void method3430(long l) {
	if (OpenGL.glUnmapBufferARB(35051)) {
	    /* empty */
	}
	OpenGL.glBindBufferARB(35051, 0);
    }
    
    public void method3431(int i, int i_313_, int[] is, int[] is_314_) {
	method15185(i, i_313_, is, is_314_);
    }
    
    public void method3432(int i, int i_315_, int[] is, int[] is_316_) {
	method15185(i, i_315_, is, is_316_);
    }
    
    public void method3584(int i, Class166 class166) {
	if (!aBool9695)
	    throw new RuntimeException("");
	anInt9664 = i;
	aClass166_9665 = class166;
	if (aBool9598) {
	    aClass143_9562.aClass136_Sub4_1653.method14478();
	    aClass143_9562.aClass136_Sub4_1653.method14477();
	}
    }
    
    public int[] method3405() {
	int[] is = new int[1];
	OpenGL.glGetIntegerv(34466, is, 0);
	int i = is[0];
	if (i == 0)
	    return null;
	int[] is_317_ = new int[i];
	OpenGL.glGetIntegerv(34467, is_317_, 0);
	return is_317_;
    }
    
    public void method3346() {
	if (aBool9692) {
	    aClass163_9722 = null;
	    if (aClass175_Sub1_9724 != null) {
		aClass175_Sub1_9724.method142();
		aClass175_Sub1_9724 = null;
	    }
	    OpenGL.glDeleteBuffersARB(3, anIntArray9725, 0);
	    for (int i = 0; i < 3; i++) {
		anIntArray9725[i] = 0;
		if (aLongArray9726[i] != 0L) {
		    OpenGL.glDeleteSync(aLongArray9726[i]);
		    aLongArray9726[i] = 0L;
		}
	    }
	} else {
	    aClass175_Sub1_9724 = null;
	    aClass163_9723 = null;
	    aClass163_9722 = null;
	}
    }
    
    public void method3306(int i, int i_318_, int i_319_, int i_320_,
			   int i_321_, int i_322_, Class145 class145,
			   int i_323_, int i_324_, int i_325_, int i_326_,
			   int i_327_) {
	if (i != i_319_ || i_318_ != i_320_) {
	    Class145_Sub3 class145_sub3 = (Class145_Sub3) class145;
	    Class141_Sub2_Sub1 class141_sub2_sub1
		= class145_sub3.aClass141_Sub2_Sub1_9876;
	    method15224();
	    method15231(class145_sub3.aClass141_Sub2_Sub1_9876);
	    method15246(i_322_);
	    method15232(7681, 8448);
	    method15325(0, 34167, 768);
	    float f = (class141_sub2_sub1.aFloat11378
		       / (float) class141_sub2_sub1.anInt11379);
	    float f_328_ = (class141_sub2_sub1.aFloat11380
			    / (float) class141_sub2_sub1.anInt11381);
	    float f_329_ = (float) i_319_ - (float) i;
	    float f_330_ = (float) i_320_ - (float) i_318_;
	    float f_331_
		= (float) (1.0 / Math.sqrt((double) (f_329_ * f_329_
						     + f_330_ * f_330_)));
	    f_329_ *= f_331_;
	    f_330_ *= f_331_;
	    OpenGL.glColor4ub((byte) (i_321_ >> 16), (byte) (i_321_ >> 8),
			      (byte) i_321_, (byte) (i_321_ >> 24));
	    i_327_ %= i_326_ + i_325_;
	    float f_332_ = f_329_ * (float) i_325_;
	    float f_333_ = f_330_ * (float) i_325_;
	    float f_334_ = 0.0F;
	    float f_335_ = 0.0F;
	    float f_336_ = f_332_;
	    float f_337_ = f_333_;
	    if (i_327_ > i_325_) {
		f_334_ = f_329_ * (float) (i_325_ + i_326_ - i_327_);
		f_335_ = f_330_ * (float) (i_325_ + i_326_ - i_327_);
	    } else {
		f_336_ = f_329_ * (float) (i_325_ - i_327_);
		f_337_ = f_330_ * (float) (i_325_ - i_327_);
	    }
	    float f_338_ = (float) i + 0.35F + f_334_;
	    float f_339_ = (float) i_318_ + 0.35F + f_335_;
	    float f_340_ = f_329_ * (float) i_326_;
	    float f_341_ = f_330_ * (float) i_326_;
	    for (;;) {
		if (i_319_ > i) {
		    if (f_338_ > (float) i_319_ + 0.35F)
			break;
		    if (f_338_ + f_336_ > (float) i_319_)
			f_336_ = (float) i_319_ - f_338_;
		} else {
		    if (f_338_ < (float) i_319_ + 0.35F)
			break;
		    if (f_338_ + f_336_ < (float) i_319_)
			f_336_ = (float) i_319_ - f_338_;
		}
		if (i_320_ > i_318_) {
		    if (f_339_ > (float) i_320_ + 0.35F)
			break;
		    if (f_339_ + f_337_ > (float) i_320_)
			f_337_ = (float) i_320_ - f_339_;
		} else {
		    if (f_339_ < (float) i_320_ + 0.35F)
			break;
		    if (f_339_ + f_337_ < (float) i_320_)
			f_337_ = (float) i_320_ - f_339_;
		}
		OpenGL.glBegin(1);
		OpenGL.glTexCoord2f(f * (f_338_ - (float) i_323_),
				    f_328_ * (f_339_ - (float) i_324_));
		OpenGL.glVertex2f(f_338_, f_339_);
		OpenGL.glTexCoord2f(f * (f_338_ + f_336_ - (float) i_323_),
				    f_328_ * (f_339_ + f_337_
					      - (float) i_324_));
		OpenGL.glVertex2f(f_338_ + f_336_, f_339_ + f_337_);
		OpenGL.glEnd();
		f_338_ += f_340_ + f_336_;
		f_339_ += f_341_ + f_337_;
		f_336_ = f_332_;
		f_337_ = f_333_;
	    }
	    method15325(0, 5890, 768);
	}
    }
    
    public void method3437() {
	anInt9648 = 0;
	anInt9632 = 0;
	anInt9633 = aClass175_1989.method2910();
	anInt9634 = aClass175_1989.method2911();
	method15197();
    }
    
    public final void method3353(int i, int i_342_, int i_343_) {
	if (anInt9687 != i || anInt9675 != i_342_ || anInt9657 != i_343_) {
	    anInt9687 = i;
	    anInt9675 = i_342_;
	    anInt9657 = i_343_;
	    method15305();
	    method15212();
	}
    }
    
    public void method3439() {
	anInt9648 = 0;
	anInt9632 = 0;
	anInt9633 = aClass175_1989.method2910();
	anInt9634 = aClass175_1989.method2911();
	method15197();
    }
    
    public void method3440(int i, int i_344_, int i_345_, int i_346_) {
	anInt9648 = i;
	anInt9632 = i_344_;
	anInt9633 = i_345_;
	anInt9634 = i_346_;
	method15197();
    }
    
    public void method3441(float f, float f_347_) {
	aFloat9614 = f;
	aFloat9615 = f_347_;
	method15354();
    }
    
    public final void method3442() {
	if (aClass175_1989 != null) {
	    anInt9690 = 0;
	    anInt9625 = 0;
	    anInt9628 = aClass175_1989.method2910();
	    anInt9626 = aClass175_1989.method2911();
	    OpenGL.glDisable(3089);
	}
    }
    
    public final void method3522() {
	if (aClass175_1989 != null) {
	    anInt9690 = 0;
	    anInt9625 = 0;
	    anInt9628 = aClass175_1989.method2910();
	    anInt9626 = aClass175_1989.method2911();
	    OpenGL.glDisable(3089);
	}
    }
    
    public final void method3444() {
	if (aClass175_1989 != null) {
	    anInt9690 = 0;
	    anInt9625 = 0;
	    anInt9628 = aClass175_1989.method2910();
	    anInt9626 = aClass175_1989.method2911();
	    OpenGL.glDisable(3089);
	}
    }
    
    final void method15274(int i, int i_348_, int i_349_) {
	OpenGL.glDrawArrays(i, i_348_, i_349_);
    }
    
    final synchronized void method15275(int i) {
	Class534_Sub39 class534_sub39 = new Class534_Sub39(i);
	aClass700_9585.method14131(class534_sub39, (short) 789);
    }
    
    public final void method3447(int i, int i_350_, int i_351_, int i_352_) {
	if (aClass175_1989 != null) {
	    if (i < 0)
		i = 0;
	    if (i_351_ > aClass175_1989.method2910())
		i_351_ = aClass175_1989.method2910();
	    if (i_350_ < 0)
		i_350_ = 0;
	    if (i_352_ > aClass175_1989.method2911())
		i_352_ = aClass175_1989.method2911();
	    anInt9690 = i;
	    anInt9625 = i_350_;
	    anInt9628 = i_351_;
	    anInt9626 = i_352_;
	    OpenGL.glEnable(3089);
	    method15198();
	}
    }
    
    public final int method3671() {
	return anInt9580 + anInt9699 + anInt9669;
    }
    
    public void method3656() {
	for (int i = 0; i < 3; i++) {
	    if (aLongArray9726[i] != 0L) {
		OpenGL.glDeleteSync(aLongArray9726[i]);
		aLongArray9726[i] = 0L;
	    }
	}
	anInt9729 = 0;
	anInt9728 = 0;
    }
    
    public final void method3450(int i, int i_353_, int i_354_, int i_355_) {
	if (anInt9690 < i)
	    anInt9690 = i;
	if (anInt9628 > i_354_)
	    anInt9628 = i_354_;
	if (anInt9625 < i_353_)
	    anInt9625 = i_353_;
	if (anInt9626 > i_355_)
	    anInt9626 = i_355_;
	OpenGL.glEnable(3089);
	method15198();
    }
    
    public final void method3290(int i, int i_356_, int i_357_, int i_358_) {
	if (anInt9690 < i)
	    anInt9690 = i;
	if (anInt9628 > i_357_)
	    anInt9628 = i_357_;
	if (anInt9625 < i_356_)
	    anInt9625 = i_356_;
	if (anInt9626 > i_358_)
	    anInt9626 = i_358_;
	OpenGL.glEnable(3089);
	method15198();
    }
    
    public final void method3562(int[] is) {
	is[0] = anInt9690;
	is[1] = anInt9625;
	is[2] = anInt9628;
	is[3] = anInt9626;
    }
    
    public final void method3452(int[] is) {
	is[0] = anInt9690;
	is[1] = anInt9625;
	is[2] = anInt9628;
	is[3] = anInt9626;
    }
    
    public boolean method3242() {
	return true;
    }
    
    public int[] method3484() {
	int[] is = new int[1];
	OpenGL.glGetIntegerv(34466, is, 0);
	int i = is[0];
	if (i == 0)
	    return null;
	int[] is_359_ = new int[i];
	OpenGL.glGetIntegerv(34467, is_359_, 0);
	return is_359_;
    }
    
    public final void method3382(int[] is) {
	is[0] = anInt9690;
	is[1] = anInt9625;
	is[2] = anInt9628;
	is[3] = anInt9626;
    }
    
    public Class446 method3532() {
	return new Class446(aClass446_9602);
    }
    
    public void method3457(int i, int i_360_) {
	int i_361_ = 0;
	if ((i & 0x1) != 0) {
	    OpenGL.glClearColor((float) (i_360_ & 0xff0000) / 1.671168E7F,
				(float) (i_360_ & 0xff00) / 65280.0F,
				(float) (i_360_ & 0xff) / 255.0F,
				(float) (i_360_ >>> 24) / 255.0F);
	    i_361_ = 16384;
	}
	if ((i & 0x2) != 0) {
	    method15193(true);
	    i_361_ |= 0x100;
	}
	if ((i & 0x4) != 0)
	    i_361_ |= 0x400;
	OpenGL.glClear(i_361_);
    }
    
    public void method3458(int i, int i_362_) {
	int i_363_ = 0;
	if ((i & 0x1) != 0) {
	    OpenGL.glClearColor((float) (i_362_ & 0xff0000) / 1.671168E7F,
				(float) (i_362_ & 0xff00) / 65280.0F,
				(float) (i_362_ & 0xff) / 255.0F,
				(float) (i_362_ >>> 24) / 255.0F);
	    i_363_ = 16384;
	}
	if ((i & 0x2) != 0) {
	    method15193(true);
	    i_363_ |= 0x100;
	}
	if ((i & 0x4) != 0)
	    i_363_ |= 0x400;
	OpenGL.glClear(i_363_);
    }
    
    public void method3459(int i, int i_364_) {
	int i_365_ = 0;
	if ((i & 0x1) != 0) {
	    OpenGL.glClearColor((float) (i_364_ & 0xff0000) / 1.671168E7F,
				(float) (i_364_ & 0xff00) / 65280.0F,
				(float) (i_364_ & 0xff) / 255.0F,
				(float) (i_364_ >>> 24) / 255.0F);
	    i_365_ = 16384;
	}
	if ((i & 0x2) != 0) {
	    method15193(true);
	    i_365_ |= 0x100;
	}
	if ((i & 0x4) != 0)
	    i_365_ |= 0x400;
	OpenGL.glClear(i_365_);
    }
    
    public void method3460(int i, int i_366_, int i_367_, int i_368_,
			   int i_369_, int i_370_) {
	float f = (float) i + 0.35F;
	float f_371_ = (float) i_366_ + 0.35F;
	float f_372_ = f + (float) i_367_ - 1.0F;
	float f_373_ = f_371_ + (float) i_368_ - 1.0F;
	method15223();
	method15246(i_370_);
	OpenGL.glColor4ub((byte) (i_369_ >> 16), (byte) (i_369_ >> 8),
			  (byte) i_369_, (byte) (i_369_ >> 24));
	if (aBool9688)
	    OpenGL.glDisable(32925);
	OpenGL.glBegin(2);
	OpenGL.glVertex2f(f, f_371_);
	OpenGL.glVertex2f(f, f_373_);
	OpenGL.glVertex2f(f_372_, f_373_);
	OpenGL.glVertex2f(f_372_, f_371_);
	OpenGL.glEnd();
	if (aBool9688)
	    OpenGL.glEnable(32925);
    }
    
    public void method3291(int i, int i_374_, int i_375_, int i_376_,
			   int i_377_, int i_378_) {
	float f = (float) i + 0.35F;
	float f_379_ = (float) i_374_ + 0.35F;
	float f_380_ = f + (float) i_375_ - 1.0F;
	float f_381_ = f_379_ + (float) i_376_ - 1.0F;
	method15223();
	method15246(i_378_);
	OpenGL.glColor4ub((byte) (i_377_ >> 16), (byte) (i_377_ >> 8),
			  (byte) i_377_, (byte) (i_377_ >> 24));
	if (aBool9688)
	    OpenGL.glDisable(32925);
	OpenGL.glBegin(2);
	OpenGL.glVertex2f(f, f_379_);
	OpenGL.glVertex2f(f, f_381_);
	OpenGL.glVertex2f(f_380_, f_381_);
	OpenGL.glVertex2f(f_380_, f_379_);
	OpenGL.glEnd();
	if (aBool9688)
	    OpenGL.glEnable(32925);
    }
    
    public void method3462(int i, int i_382_, int i_383_, int i_384_,
			   int i_385_, int i_386_) {
	float f = (float) i + 0.35F;
	float f_387_ = (float) i_382_ + 0.35F;
	float f_388_ = f + (float) i_383_;
	float f_389_ = f_387_ + (float) i_384_;
	method15223();
	method15246(i_386_);
	OpenGL.glColor4ub((byte) (i_385_ >> 16), (byte) (i_385_ >> 8),
			  (byte) i_385_, (byte) (i_385_ >> 24));
	if (aBool9688)
	    OpenGL.glDisable(32925);
	OpenGL.glBegin(7);
	OpenGL.glVertex2f(f, f_387_);
	OpenGL.glVertex2f(f, f_389_);
	OpenGL.glVertex2f(f_388_, f_389_);
	OpenGL.glVertex2f(f_388_, f_387_);
	OpenGL.glEnd();
	if (aBool9688)
	    OpenGL.glEnable(32925);
    }
    
    final void method15276() {
	aFloat9617 = aClass433_9605.method6907();
	aFloat9616 = aClass433_9605.method6859();
	method15305();
	if (anInt9622 == 2)
	    method15183(aClass433_9605.aFloatArray4853);
	else if (anInt9622 == 1)
	    method15183(aClass433_9606.aFloatArray4853);
    }
    
    final Interface15 method15277(int i, Buffer buffer, int i_390_,
				  boolean bool) {
	if (aBool9705 && (!bool || aBool9691))
	    return new Class134_Sub1(this, i, buffer, i_390_, bool);
	return new Class126_Sub1(this, i, buffer);
    }
    
    final void method15278(Class129 class129, Class129 class129_391_,
			   Class129 class129_392_, Class129 class129_393_) {
	if (class129 != null) {
	    method15220(class129.anInterface15_1519);
	    OpenGL.glVertexPointer(class129.aByte1517, class129.aShort1516,
				   anInterface15_9593.method93(),
				   (anInterface15_9593.method94()
				    + (long) class129.aByte1518));
	    OpenGL.glEnableClientState(32884);
	} else
	    OpenGL.glDisableClientState(32884);
	if (class129_391_ != null) {
	    method15220(class129_391_.anInterface15_1519);
	    OpenGL.glNormalPointer(class129_391_.aShort1516,
				   anInterface15_9593.method93(),
				   (anInterface15_9593.method94()
				    + (long) class129_391_.aByte1518));
	    OpenGL.glEnableClientState(32885);
	} else
	    OpenGL.glDisableClientState(32885);
	if (class129_392_ != null) {
	    method15220(class129_392_.anInterface15_1519);
	    OpenGL.glColorPointer(class129_392_.aByte1517,
				  class129_392_.aShort1516,
				  anInterface15_9593.method93(),
				  (anInterface15_9593.method94()
				   + (long) class129_392_.aByte1518));
	    OpenGL.glEnableClientState(32886);
	} else
	    OpenGL.glDisableClientState(32886);
	if (class129_393_ != null) {
	    method15220(class129_393_.anInterface15_1519);
	    OpenGL.glTexCoordPointer(class129_393_.aByte1517,
				     class129_393_.aShort1516,
				     anInterface15_9593.method93(),
				     (anInterface15_9593.method94()
				      + (long) class129_393_.aByte1518));
	    OpenGL.glEnableClientState(32888);
	} else
	    OpenGL.glDisableClientState(32888);
    }
    
    void method3595(int i, int i_394_, int i_395_, int i_396_, int i_397_) {
	method15223();
	method15246(i_397_);
	float f = (float) i + 0.35F;
	float f_398_ = (float) i_394_ + 0.35F;
	OpenGL.glColor4ub((byte) (i_396_ >> 16), (byte) (i_396_ >> 8),
			  (byte) i_396_, (byte) (i_396_ >> 24));
	OpenGL.glBegin(1);
	OpenGL.glVertex2f(f, f_398_);
	OpenGL.glVertex2f(f + (float) i_395_, f_398_);
	OpenGL.glEnd();
    }
    
    void method3582() {
	for (Class534 class534 = aClass700_9577.method14135((byte) -1);
	     class534 != null;
	     class534 = aClass700_9577.method14139(592597086))
	    ((Class534_Sub2_Sub2) class534).method18232();
	if (aClass153_9592 != null)
	    aClass153_9592.method2547();
	if (aBool9575) {
	    Class54.method1212(false, true, -1176350707);
	    aBool9575 = false;
	}
    }
    
    public void method3468(int i, int i_399_, int i_400_, int i_401_,
			   int i_402_, int i_403_) {
	method15223();
	method15246(i_403_);
	float f = (float) i_400_ - (float) i;
	float f_404_ = (float) i_401_ - (float) i_399_;
	if (f == 0.0F && f_404_ == 0.0F)
	    f = 1.0F;
	else {
	    float f_405_
		= (float) (1.0
			   / Math.sqrt((double) (f * f + f_404_ * f_404_)));
	    f *= f_405_;
	    f_404_ *= f_405_;
	}
	OpenGL.glColor4ub((byte) (i_402_ >> 16), (byte) (i_402_ >> 8),
			  (byte) i_402_, (byte) (i_402_ >> 24));
	OpenGL.glBegin(1);
	OpenGL.glVertex2f((float) i + 0.35F, (float) i_399_ + 0.35F);
	OpenGL.glVertex2f((float) i_400_ + f + 0.35F,
			  (float) i_401_ + f_404_ + 0.35F);
	OpenGL.glEnd();
    }
    
    public void method3469(int i, int i_406_, int i_407_, int i_408_,
			   int i_409_, int i_410_, int i_411_, int i_412_,
			   int i_413_) {
	if (i != i_407_ || i_406_ != i_408_) {
	    method15223();
	    method15246(i_410_);
	    float f = (float) i_407_ - (float) i;
	    float f_414_ = (float) i_408_ - (float) i_406_;
	    float f_415_
		= (float) (1.0
			   / Math.sqrt((double) (f * f + f_414_ * f_414_)));
	    f *= f_415_;
	    f_414_ *= f_415_;
	    OpenGL.glColor4ub((byte) (i_409_ >> 16), (byte) (i_409_ >> 8),
			      (byte) i_409_, (byte) (i_409_ >> 24));
	    i_413_ %= i_412_ + i_411_;
	    float f_416_ = f * (float) i_411_;
	    float f_417_ = f_414_ * (float) i_411_;
	    float f_418_ = 0.0F;
	    float f_419_ = 0.0F;
	    float f_420_ = f_416_;
	    float f_421_ = f_417_;
	    if (i_413_ > i_411_) {
		f_418_ = f * (float) (i_411_ + i_412_ - i_413_);
		f_419_ = f_414_ * (float) (i_411_ + i_412_ - i_413_);
	    } else {
		f_420_ = f * (float) (i_411_ - i_413_);
		f_421_ = f_414_ * (float) (i_411_ - i_413_);
	    }
	    float f_422_ = (float) i + 0.35F + f_418_;
	    float f_423_ = (float) i_406_ + 0.35F + f_419_;
	    float f_424_ = f * (float) i_412_;
	    float f_425_ = f_414_ * (float) i_412_;
	    for (;;) {
		if (i_407_ > i) {
		    if (f_422_ > (float) i_407_ + 0.35F)
			break;
		    if (f_422_ + f_420_ > (float) i_407_)
			f_420_ = (float) i_407_ - f_422_;
		} else {
		    if (f_422_ < (float) i_407_ + 0.35F)
			break;
		    if (f_422_ + f_420_ < (float) i_407_)
			f_420_ = (float) i_407_ - f_422_;
		}
		if (i_408_ > i_406_) {
		    if (f_423_ > (float) i_408_ + 0.35F)
			break;
		    if (f_423_ + f_421_ > (float) i_408_)
			f_421_ = (float) i_408_ - f_423_;
		} else {
		    if (f_423_ < (float) i_408_ + 0.35F)
			break;
		    if (f_423_ + f_421_ < (float) i_408_)
			f_421_ = (float) i_408_ - f_423_;
		}
		OpenGL.glBegin(1);
		OpenGL.glVertex2f(f_422_, f_423_);
		OpenGL.glVertex2f(f_422_ + f_420_, f_423_ + f_421_);
		OpenGL.glEnd();
		f_422_ += f_424_ + f_420_;
		f_423_ += f_425_ + f_421_;
		f_420_ = f_416_;
		f_421_ = f_417_;
	    }
	}
    }
    
    final void method15279() {
	if (anInt9591 != 4) {
	    method15204();
	    method15344(false);
	    method15432(false);
	    method15242(false);
	    method15193(false);
	    method15227(-2);
	    method15246(1);
	    method15251((byte) 0);
	    anInt9591 = 4;
	}
    }
    
    public void method3471(int i, int i_426_, int i_427_, int i_428_,
			   int i_429_, int i_430_, Class145 class145,
			   int i_431_, int i_432_, int i_433_, int i_434_,
			   int i_435_) {
	if (i != i_427_ || i_426_ != i_428_) {
	    Class145_Sub3 class145_sub3 = (Class145_Sub3) class145;
	    Class141_Sub2_Sub1 class141_sub2_sub1
		= class145_sub3.aClass141_Sub2_Sub1_9876;
	    method15224();
	    method15231(class145_sub3.aClass141_Sub2_Sub1_9876);
	    method15246(i_430_);
	    method15232(7681, 8448);
	    method15325(0, 34167, 768);
	    float f = (class141_sub2_sub1.aFloat11378
		       / (float) class141_sub2_sub1.anInt11379);
	    float f_436_ = (class141_sub2_sub1.aFloat11380
			    / (float) class141_sub2_sub1.anInt11381);
	    float f_437_ = (float) i_427_ - (float) i;
	    float f_438_ = (float) i_428_ - (float) i_426_;
	    float f_439_
		= (float) (1.0 / Math.sqrt((double) (f_437_ * f_437_
						     + f_438_ * f_438_)));
	    f_437_ *= f_439_;
	    f_438_ *= f_439_;
	    OpenGL.glColor4ub((byte) (i_429_ >> 16), (byte) (i_429_ >> 8),
			      (byte) i_429_, (byte) (i_429_ >> 24));
	    i_435_ %= i_434_ + i_433_;
	    float f_440_ = f_437_ * (float) i_433_;
	    float f_441_ = f_438_ * (float) i_433_;
	    float f_442_ = 0.0F;
	    float f_443_ = 0.0F;
	    float f_444_ = f_440_;
	    float f_445_ = f_441_;
	    if (i_435_ > i_433_) {
		f_442_ = f_437_ * (float) (i_433_ + i_434_ - i_435_);
		f_443_ = f_438_ * (float) (i_433_ + i_434_ - i_435_);
	    } else {
		f_444_ = f_437_ * (float) (i_433_ - i_435_);
		f_445_ = f_438_ * (float) (i_433_ - i_435_);
	    }
	    float f_446_ = (float) i + 0.35F + f_442_;
	    float f_447_ = (float) i_426_ + 0.35F + f_443_;
	    float f_448_ = f_437_ * (float) i_434_;
	    float f_449_ = f_438_ * (float) i_434_;
	    for (;;) {
		if (i_427_ > i) {
		    if (f_446_ > (float) i_427_ + 0.35F)
			break;
		    if (f_446_ + f_444_ > (float) i_427_)
			f_444_ = (float) i_427_ - f_446_;
		} else {
		    if (f_446_ < (float) i_427_ + 0.35F)
			break;
		    if (f_446_ + f_444_ < (float) i_427_)
			f_444_ = (float) i_427_ - f_446_;
		}
		if (i_428_ > i_426_) {
		    if (f_447_ > (float) i_428_ + 0.35F)
			break;
		    if (f_447_ + f_445_ > (float) i_428_)
			f_445_ = (float) i_428_ - f_447_;
		} else {
		    if (f_447_ < (float) i_428_ + 0.35F)
			break;
		    if (f_447_ + f_445_ < (float) i_428_)
			f_445_ = (float) i_428_ - f_447_;
		}
		OpenGL.glBegin(1);
		OpenGL.glTexCoord2f(f * (f_446_ - (float) i_431_),
				    f_436_ * (f_447_ - (float) i_432_));
		OpenGL.glVertex2f(f_446_, f_447_);
		OpenGL.glTexCoord2f(f * (f_446_ + f_444_ - (float) i_431_),
				    f_436_ * (f_447_ + f_445_
					      - (float) i_432_));
		OpenGL.glVertex2f(f_446_ + f_444_, f_447_ + f_445_);
		OpenGL.glEnd();
		f_446_ += f_448_ + f_444_;
		f_447_ += f_449_ + f_445_;
		f_444_ = f_440_;
		f_445_ = f_441_;
	    }
	    method15325(0, 5890, 768);
	}
    }
    
    public void method3472(int i, int i_450_, int i_451_, int i_452_,
			   int i_453_, int i_454_, int i_455_) {
	OpenGL.glLineWidth((float) i_454_);
	method3303(i, i_450_, i_451_, i_452_, i_453_, i_455_);
	OpenGL.glLineWidth(1.0F);
    }
    
    public void method3598(int i, int i_456_, int i_457_, int i_458_,
			   int i_459_, int i_460_, int i_461_) {
	OpenGL.glLineWidth((float) i_460_);
	method3303(i, i_456_, i_457_, i_458_, i_459_, i_461_);
	OpenGL.glLineWidth(1.0F);
    }
    
    public void method3348(int i, int i_462_, int i_463_, int i_464_,
			   int i_465_, int i_466_, int i_467_) {
	OpenGL.glLineWidth((float) i_466_);
	method3303(i, i_462_, i_463_, i_464_, i_465_, i_467_);
	OpenGL.glLineWidth(1.0F);
    }
    
    public int method3475(int i, int i_468_, int i_469_, int i_470_,
			  int i_471_, int i_472_) {
	int i_473_ = 0;
	float f = (aClass433_9607.aFloatArray4853[14]
		   + aClass433_9607.aFloatArray4853[2] * (float) i
		   + aClass433_9607.aFloatArray4853[6] * (float) i_468_
		   + aClass433_9607.aFloatArray4853[10] * (float) i_469_);
	float f_474_ = (aClass433_9607.aFloatArray4853[14]
			+ aClass433_9607.aFloatArray4853[2] * (float) i_470_
			+ aClass433_9607.aFloatArray4853[6] * (float) i_471_
			+ aClass433_9607.aFloatArray4853[10] * (float) i_472_);
	float f_475_ = (aClass433_9607.aFloatArray4853[15]
			+ aClass433_9607.aFloatArray4853[3] * (float) i
			+ aClass433_9607.aFloatArray4853[7] * (float) i_468_
			+ aClass433_9607.aFloatArray4853[11] * (float) i_469_);
	float f_476_ = (aClass433_9607.aFloatArray4853[15]
			+ aClass433_9607.aFloatArray4853[3] * (float) i_470_
			+ aClass433_9607.aFloatArray4853[7] * (float) i_471_
			+ aClass433_9607.aFloatArray4853[11] * (float) i_472_);
	if (f < -f_475_ && f_474_ < -f_476_)
	    i_473_ |= 0x10;
	else if (f > f_475_ && f_474_ > f_476_)
	    i_473_ |= 0x20;
	float f_477_ = (aClass433_9607.aFloatArray4853[12]
			+ aClass433_9607.aFloatArray4853[0] * (float) i
			+ aClass433_9607.aFloatArray4853[4] * (float) i_468_
			+ aClass433_9607.aFloatArray4853[8] * (float) i_469_);
	float f_478_ = (aClass433_9607.aFloatArray4853[12]
			+ aClass433_9607.aFloatArray4853[0] * (float) i_470_
			+ aClass433_9607.aFloatArray4853[4] * (float) i_471_
			+ aClass433_9607.aFloatArray4853[8] * (float) i_472_);
	if (f_477_ < -f_475_ && f_478_ < -f_476_)
	    i_473_ |= 0x1;
	if (f_477_ > f_475_ && f_478_ > f_476_)
	    i_473_ |= 0x2;
	float f_479_ = (aClass433_9607.aFloatArray4853[13]
			+ aClass433_9607.aFloatArray4853[1] * (float) i
			+ aClass433_9607.aFloatArray4853[5] * (float) i_468_
			+ aClass433_9607.aFloatArray4853[9] * (float) i_469_);
	float f_480_ = (aClass433_9607.aFloatArray4853[13]
			+ aClass433_9607.aFloatArray4853[1] * (float) i_470_
			+ aClass433_9607.aFloatArray4853[5] * (float) i_471_
			+ aClass433_9607.aFloatArray4853[9] * (float) i_472_);
	if (f_479_ < -f_475_ && f_480_ < -f_476_)
	    i_473_ |= 0x4;
	if (f_479_ > f_475_ && f_480_ > f_476_)
	    i_473_ |= 0x8;
	return i_473_;
    }
    
    final void method15280() {
	OpenGL.glLoadIdentity();
	OpenGL.glMultMatrixf(aClass433_9533.aFloatArray4853, 0);
	if (aBool9598)
	    aClass143_9562.aClass136_Sub4_1653.method14478();
	method15184();
	method15421();
    }
    
    boolean method3477(int i, int i_481_, int i_482_, int i_483_,
		       Class446 class446, Class432 class432) {
	Class433 class433 = aClass433_9530;
	class433.method6916(class446);
	class433.method6839(aClass433_9607);
	return class432.method6838(i, i_481_, i_482_, i_483_, class433,
				   aFloat9610, aFloat9612, aFloat9663,
				   aFloat9613);
    }
    
    public boolean method3246() {
	return aBool9688 && (!method3409() || aBool9689);
    }
    
    public void method3479(Class446 class446, Class194 class194,
			   Class432 class432) {
	Class433 class433 = aClass433_9530;
	class433.method6916(class446);
	class433.method6839(aClass433_9607);
	class194.method3800(class432, aClass433_9605, class433, aFloat9610,
			    aFloat9612, aFloat9663, aFloat9613);
    }
    
    final void method15281(float f, float f_484_, float f_485_) {
	OpenGL.glMatrixMode(5890);
	if (aBool9671)
	    OpenGL.glLoadIdentity();
	OpenGL.glTranslatef(f, f_484_, f_485_);
	OpenGL.glMatrixMode(5888);
	aBool9671 = true;
    }
    
    void method3634(int i, int i_486_, int i_487_, int i_488_, int i_489_) {
	if (i_487_ < 0)
	    i_487_ = -i_487_;
	if (i + i_487_ >= anInt9690 && i - i_487_ <= anInt9628
	    && i_486_ + i_487_ >= anInt9625 && i_486_ - i_487_ <= anInt9626) {
	    method15223();
	    method15246(i_489_);
	    OpenGL.glColor4ub((byte) (i_488_ >> 16), (byte) (i_488_ >> 8),
			      (byte) i_488_, (byte) (i_488_ >> 24));
	    float f = (float) i + 0.35F;
	    float f_490_ = (float) i_486_ + 0.35F;
	    int i_491_ = i_487_ << 1;
	    if ((float) i_491_ < aFloat9542) {
		OpenGL.glBegin(7);
		OpenGL.glVertex2f(f + 1.0F, f_490_ + 1.0F);
		OpenGL.glVertex2f(f + 1.0F, f_490_ - 1.0F);
		OpenGL.glVertex2f(f - 1.0F, f_490_ - 1.0F);
		OpenGL.glVertex2f(f - 1.0F, f_490_ + 1.0F);
		OpenGL.glEnd();
	    } else if ((float) i_491_ <= aFloat9706) {
		OpenGL.glEnable(2832);
		OpenGL.glPointSize((float) i_491_);
		OpenGL.glBegin(0);
		OpenGL.glVertex2f(f, f_490_);
		OpenGL.glEnd();
		OpenGL.glDisable(2832);
	    } else {
		OpenGL.glBegin(6);
		OpenGL.glVertex2f(f, f_490_);
		int i_492_ = 262144 / (6 * i_487_);
		if (i_492_ <= 64)
		    i_492_ = 64;
		else if (i_492_ > 512)
		    i_492_ = 512;
		i_492_ = Class455.method7422(i_492_, 5391525);
		OpenGL.glVertex2f(f + (float) i_487_, f_490_);
		for (int i_493_ = 16384 - i_492_; i_493_ > 0; i_493_ -= i_492_)
		    OpenGL.glVertex2f(f + (Class147.aFloatArray1665[i_493_]
					   * (float) i_487_),
				      f_490_ + (Class147.aFloatArray1664
						[i_493_]) * (float) i_487_);
		OpenGL.glVertex2f(f + (float) i_487_, f_490_);
		OpenGL.glEnd();
	    }
	}
    }
    
    public void method3482(Class534_Sub2 class534_sub2) {
	aNativeHeap9639
	    = ((Class534_Sub2_Sub2) class534_sub2).aNativeHeap11743;
	if (anInterface15_9539 == null) {
	    Class534_Sub40_Sub2 class534_sub40_sub2
		= new Class534_Sub40_Sub2(80);
	    if (aBool9566) {
		class534_sub40_sub2.method18397(-1.0F);
		class534_sub40_sub2.method18397(-1.0F);
		class534_sub40_sub2.method18397(0.0F);
		class534_sub40_sub2.method18397(0.0F);
		class534_sub40_sub2.method18397(0.0F);
		class534_sub40_sub2.method18397(-1.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(0.0F);
		class534_sub40_sub2.method18397(0.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(0.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(-1.0F);
		class534_sub40_sub2.method18397(0.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(0.0F);
	    } else {
		class534_sub40_sub2.method18400(-1.0F);
		class534_sub40_sub2.method18400(-1.0F);
		class534_sub40_sub2.method18400(0.0F);
		class534_sub40_sub2.method18400(0.0F);
		class534_sub40_sub2.method18400(0.0F);
		class534_sub40_sub2.method18400(-1.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(0.0F);
		class534_sub40_sub2.method18400(0.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(0.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(-1.0F);
		class534_sub40_sub2.method18400(0.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(0.0F);
	    }
	    anInterface15_9539
		= method15218(20, class534_sub40_sub2.aByteArray10810,
			      class534_sub40_sub2.anInt10811 * 31645619,
			      false);
	    aClass129_9711 = new Class129(anInterface15_9539, 5126, 3, 0);
	    aClass129_9712 = new Class129(anInterface15_9539, 5126, 2, 12);
	    aClass148_9567.method2457(this);
	}
    }
    
    public void method3483(Class534_Sub2 class534_sub2) {
	aNativeHeap9639
	    = ((Class534_Sub2_Sub2) class534_sub2).aNativeHeap11743;
	if (anInterface15_9539 == null) {
	    Class534_Sub40_Sub2 class534_sub40_sub2
		= new Class534_Sub40_Sub2(80);
	    if (aBool9566) {
		class534_sub40_sub2.method18397(-1.0F);
		class534_sub40_sub2.method18397(-1.0F);
		class534_sub40_sub2.method18397(0.0F);
		class534_sub40_sub2.method18397(0.0F);
		class534_sub40_sub2.method18397(0.0F);
		class534_sub40_sub2.method18397(-1.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(0.0F);
		class534_sub40_sub2.method18397(0.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(0.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(-1.0F);
		class534_sub40_sub2.method18397(0.0F);
		class534_sub40_sub2.method18397(1.0F);
		class534_sub40_sub2.method18397(0.0F);
	    } else {
		class534_sub40_sub2.method18400(-1.0F);
		class534_sub40_sub2.method18400(-1.0F);
		class534_sub40_sub2.method18400(0.0F);
		class534_sub40_sub2.method18400(0.0F);
		class534_sub40_sub2.method18400(0.0F);
		class534_sub40_sub2.method18400(-1.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(0.0F);
		class534_sub40_sub2.method18400(0.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(0.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(-1.0F);
		class534_sub40_sub2.method18400(0.0F);
		class534_sub40_sub2.method18400(1.0F);
		class534_sub40_sub2.method18400(0.0F);
	    }
	    anInterface15_9539
		= method15218(20, class534_sub40_sub2.aByteArray10810,
			      class534_sub40_sub2.anInt10811 * 31645619,
			      false);
	    aClass129_9711 = new Class129(anInterface15_9539, 5126, 3, 0);
	    aClass129_9712 = new Class129(anInterface15_9539, 5126, 2, 12);
	    aClass148_9567.method2457(this);
	}
    }
    
    final void method15282() {
	if (aBool9704) {
	    int i = 0;
	    int i_494_ = 0;
	    if (anInt9596 == 0) {
		i = 0;
		i_494_ = 0;
	    } else if (anInt9596 == 1) {
		i = 1;
		i_494_ = 0;
	    } else if (anInt9596 == 2) {
		i = 1;
		i_494_ = 1;
	    } else if (anInt9596 == 3) {
		i = 0;
		i_494_ = 1;
	    }
	    if (anInt9636 == 1)
		OpenGL.glBlendFuncSeparate(770, 771, i, i_494_);
	    else if (anInt9636 == 2)
		OpenGL.glBlendFuncSeparate(1, 1, i, i_494_);
	    else if (anInt9636 == 3)
		OpenGL.glBlendFuncSeparate(774, 1, i, i_494_);
	    else if (anInt9636 == 0)
		OpenGL.glBlendFuncSeparate(1, 0, i, i_494_);
	} else if (anInt9636 == 1) {
	    OpenGL.glEnable(3042);
	    OpenGL.glBlendFunc(770, 771);
	} else if (anInt9636 == 2) {
	    OpenGL.glEnable(3042);
	    OpenGL.glBlendFunc(1, 1);
	} else if (anInt9636 == 3) {
	    OpenGL.glEnable(3042);
	    OpenGL.glBlendFunc(774, 1);
	} else
	    OpenGL.glDisable(3042);
    }
    
    public final void method3489(float f) {
	if (aFloat9646 != f) {
	    aFloat9646 = f;
	    method15210();
	}
    }
    
    public Class163 method3343(int[] is, int i, int i_495_, int i_496_,
			       int i_497_, boolean bool) {
	return new Class163_Sub3(this, i_496_, i_497_, is, i, i_495_);
    }
    
    public Class163 method3487(int i, int i_498_, int i_499_, int i_500_,
			       boolean bool) {
	return new Class163_Sub3(this, i, i_498_, i_499_, i_500_);
    }
    
    public Class163 method3488(int i, int i_501_, int i_502_, int i_503_,
			       boolean bool) {
	return new Class163_Sub3(this, i, i_501_, i_502_, i_503_);
    }
    
    final void method15283(Interface16 interface16, int i, int i_504_,
			   int i_505_) {
	method15290(interface16);
	OpenGL.glDrawElements(i, i_505_, 5123,
			      interface16.method2() + (long) (i_504_ * 2));
    }
    
    public boolean method3391() {
	return true;
    }
    
    final void method15284(Interface15 interface15) {
	if (anInterface15_9593 != interface15) {
	    if (aBool9705)
		OpenGL.glBindBufferARB(34962, interface15.method1());
	    anInterface15_9593 = interface15;
	}
    }
    
    public void method3492(int i) {
	/* empty */
    }
    
    public Class183 method3493(Class187 class187, int i, int i_506_,
			       int i_507_, int i_508_) {
	return new Class183_Sub1(this, class187, i, i_507_, i_508_, i_506_);
    }
    
    public int method3467(int i, int i_509_) {
	return i & i_509_ ^ i_509_;
    }
    
    public void method3324(int i, Class145 class145, int i_510_, int i_511_) {
	Class145_Sub3 class145_sub3 = (Class145_Sub3) class145;
	Class141_Sub2_Sub1 class141_sub2_sub1
	    = class145_sub3.aClass141_Sub2_Sub1_9876;
	method15224();
	method15231(class145_sub3.aClass141_Sub2_Sub1_9876);
	method15246(1);
	method15232(7681, 8448);
	method15325(0, 34167, 768);
	float f = (class141_sub2_sub1.aFloat11378
		   / (float) class141_sub2_sub1.anInt11379);
	float f_512_ = (class141_sub2_sub1.aFloat11380
			/ (float) class141_sub2_sub1.anInt11381);
	OpenGL.glColor4ub((byte) (i >> 16), (byte) (i >> 8), (byte) i,
			  (byte) (i >> 24));
	OpenGL.glBegin(7);
	OpenGL.glTexCoord2f(f * (float) (anInt9690 - i_510_),
			    f_512_ * (float) (anInt9625 - i_511_));
	OpenGL.glVertex2i(anInt9690, anInt9625);
	OpenGL.glTexCoord2f(f * (float) (anInt9690 - i_510_),
			    f_512_ * (float) (anInt9626 - i_511_));
	OpenGL.glVertex2i(anInt9690, anInt9626);
	OpenGL.glTexCoord2f(f * (float) (anInt9628 - i_510_),
			    f_512_ * (float) (anInt9626 - i_511_));
	OpenGL.glVertex2i(anInt9628, anInt9626);
	OpenGL.glTexCoord2f(f * (float) (anInt9628 - i_510_),
			    f_512_ * (float) (anInt9625 - i_511_));
	OpenGL.glVertex2i(anInt9628, anInt9625);
	OpenGL.glEnd();
	method15325(0, 5890, 768);
    }
    
    public void method3543(float f, float f_513_, float f_514_, float f_515_,
			   float f_516_) {
	Class534_Sub9_Sub3.aFloat11642 = f;
	Class534_Sub9_Sub3.aFloat11645 = f_513_;
	Class534_Sub9_Sub3.aFloat11641 = f_514_;
	Class534_Sub9_Sub3.aFloat11640 = f_515_;
	Class534_Sub9_Sub3.aFloat11643 = f_516_;
    }
    
    public final void method3341(int i, float f, float f_517_, float f_518_,
				 float f_519_, float f_520_) {
	boolean bool = anInt9642 != i;
	if (bool || aFloat9686 != f || aFloat9587 != f_517_) {
	    anInt9642 = i;
	    aFloat9686 = f;
	    aFloat9587 = f_517_;
	    if (bool) {
		aFloat9643 = (float) (anInt9642 & 0xff0000) / 1.671168E7F;
		aFloat9644 = (float) (anInt9642 & 0xff00) / 65280.0F;
		aFloat9645 = (float) (anInt9642 & 0xff) / 255.0F;
		method15210();
	    }
	    method15313();
	}
	if (aFloatArray9677[0] != f_518_ || aFloatArray9677[1] != f_519_
	    || aFloatArray9677[2] != f_520_) {
	    aFloatArray9677[0] = f_518_;
	    aFloatArray9677[1] = f_519_;
	    aFloatArray9677[2] = f_520_;
	    aFloatArray9515[0] = -f_518_;
	    aFloatArray9515[1] = -f_519_;
	    aFloatArray9515[2] = -f_520_;
	    float f_521_
		= (float) (1.0 / Math.sqrt((double) (f_518_ * f_518_
						     + f_519_ * f_519_
						     + f_520_ * f_520_)));
	    aFloatArray9640[0] = f_518_ * f_521_;
	    aFloatArray9640[1] = f_519_ * f_521_;
	    aFloatArray9640[2] = f_520_ * f_521_;
	    aFloatArray9641[0] = -aFloatArray9640[0];
	    aFloatArray9641[1] = -aFloatArray9640[1];
	    aFloatArray9641[2] = -aFloatArray9640[2];
	    method15184();
	    anInt9652 = (int) (f_518_ * 256.0F / f_519_);
	    anInt9647 = (int) (f_520_ * 256.0F / f_519_);
	}
    }
    
    public int method3498() {
	return 4;
    }
    
    public int method3499() {
	return 4;
    }
    
    final void method15285(Interface16 interface16, int i, int i_522_,
			   int i_523_) {
	method15290(interface16);
	OpenGL.glDrawElements(i, i_523_, 5123,
			      interface16.method2() + (long) (i_522_ * 2));
    }
    
    public int method3501() {
	return 4;
    }
    
    public int method3502() {
	return 4;
    }
    
    public void method3659(int i, Class534_Sub21[] class534_sub21s) {
	for (int i_524_ = 0; i_524_ < i; i_524_++)
	    aClass534_Sub21Array9702[i_524_] = class534_sub21s[i_524_];
	anInt9651 = i;
	if (anInt9622 != 1)
	    method15421();
    }
    
    final void method15286(Class141 class141) {
	Class141 class141_525_ = aClass141Array9519[anInt9672];
	if (class141_525_ != class141) {
	    if (class141 != null) {
		if (class141_525_ != null) {
		    if (class141.anInt1628 != class141_525_.anInt1628) {
			OpenGL.glDisable(class141_525_.anInt1628);
			OpenGL.glEnable(class141.anInt1628);
		    }
		} else
		    OpenGL.glEnable(class141.anInt1628);
		OpenGL.glBindTexture(class141.anInt1628, class141.anInt1633);
	    } else
		OpenGL.glDisable(class141_525_.anInt1628);
	    aClass141Array9519[anInt9672] = class141;
	}
	anInt9591 &= ~0x11;
    }
    
    public void method3505(Class174 class174) {
	aClass148_9567.method2458(this, class174);
    }
    
    public final void method3506(Class446 class446) {
	aClass446_9602.method7236(class446);
	aClass433_9533.method6916(aClass446_9602);
	aClass446_9618.method7236(class446);
	aClass446_9618.method7243();
	aClass433_9604.method6916(aClass446_9618);
	method15207();
	if (anInt9622 != 1)
	    method15273();
    }
    
    void method15287() {
	int i = 0;
	while (!anOpenGL9559.method1767()) {
	    if (i++ > 5)
		throw new RuntimeException("");
	    Class251.method4622(1000L);
	}
    }
    
    boolean method15288() {
	return aClass143_9562.method2416(3);
    }
    
    void method15289() {
	aFloatArray9714[0] = aFloat9646 * aFloat9643;
	aFloatArray9714[1] = aFloat9646 * aFloat9644;
	aFloatArray9714[2] = aFloat9646 * aFloat9645;
	aFloatArray9714[3] = 1.0F;
	OpenGL.glLightModelfv(2899, aFloatArray9714, 0);
    }
    
    final void method15290(Interface16 interface16) {
	if (anInterface16_9666 != interface16) {
	    if (aBool9705)
		OpenGL.glBindBufferARB(34963, interface16.method1());
	    anInterface16_9666 = interface16;
	}
    }
    
    public final Class433 method3511() {
	return new Class433(aClass433_9605);
    }
    
    boolean method3478(int i, int i_526_, int i_527_, int i_528_,
		       Class446 class446, Class432 class432) {
	Class433 class433 = aClass433_9530;
	class433.method6916(class446);
	class433.method6839(aClass433_9607);
	return class432.method6838(i, i_526_, i_527_, i_528_, class433,
				   aFloat9610, aFloat9612, aFloat9663,
				   aFloat9613);
    }
    
    public final Class433 method3513() {
	return new Class433(aClass433_9605);
    }
    
    public final Class433 method3514() {
	return new Class433(aClass433_9605);
    }
    
    public final Class433 method3515() {
	return new Class433(aClass433_9605);
    }
    
    public final void method3516(float f) {
	if (aFloat9646 != f) {
	    aFloat9646 = f;
	    method15210();
	}
    }
    
    public boolean method3395() {
	return aBool9688 && (!method3409() || aBool9689);
    }
    
    public final void method3681(float f) {
	if (aFloat9646 != f) {
	    aFloat9646 = f;
	    method15210();
	}
    }
    
    public final void method3519(int i) {
	anInt9574 = 0;
	for (/**/; i > 1; i >>= 1)
	    anInt9574++;
	anInt9638 = 1 << anInt9574;
    }
    
    public Class170 method3546(int[] is) {
	return new Class170_Sub2(this, is);
    }
    
    public final void method3521(int i, int i_529_, int i_530_) {
	if (anInt9687 != i || anInt9675 != i_529_ || anInt9657 != i_530_) {
	    anInt9687 = i;
	    anInt9675 = i_529_;
	    anInt9657 = i_530_;
	    method15305();
	    method15212();
	}
    }
    
    public final void method3517(float f) {
	if (aFloat9646 != f) {
	    aFloat9646 = f;
	    method15210();
	}
    }
    
    public final Class433 method3512() {
	return new Class433(aClass433_9605);
    }
    
    public int method3495(int i, int i_531_) {
	return i & i_531_ ^ i_531_;
    }
    
    public Interface22 method3678(int i, int i_532_, Class181 class181,
				  Class173 class173, int i_533_) {
	return new Class534_Sub18_Sub1(this, class181, class173, i, i_532_,
				       i_533_);
    }
    
    boolean method15291() {
	if (aClass534_Sub9_Sub1_9581 != null) {
	    if (!aClass534_Sub9_Sub1_9581.method16109()) {
		if (aClass153_9592.method2554(aClass534_Sub9_Sub1_9581))
		    aClass161_9659.method2626();
		else
		    return false;
	    }
	    return true;
	}
	return false;
    }
    
    public final void method3527(Class165 class165) {
	aClass165_Sub1_9674 = (Class165_Sub1) class165;
    }
    
    public final void method3528(int i, int i_534_, int i_535_, int i_536_) {
	if (aClass153_9592 != null)
	    aClass153_9592.method2552(i, i_534_, i_535_, i_536_);
    }
    
    public final void method3320(int i, int i_537_, int i_538_, int i_539_) {
	if (aClass153_9592 != null)
	    aClass153_9592.method2552(i, i_537_, i_538_, i_539_);
    }
    
    public final void method3373(int i, int i_540_, int i_541_, int i_542_) {
	if (aClass175_1989 != null) {
	    if (i < 0)
		i = 0;
	    if (i_541_ > aClass175_1989.method2910())
		i_541_ = aClass175_1989.method2910();
	    if (i_540_ < 0)
		i_540_ = 0;
	    if (i_542_ > aClass175_1989.method2911())
		i_542_ = aClass175_1989.method2911();
	    anInt9690 = i;
	    anInt9625 = i_540_;
	    anInt9628 = i_541_;
	    anInt9626 = i_542_;
	    OpenGL.glEnable(3089);
	    method15198();
	}
    }
    
    public void method3433(int i, int i_543_, int[] is, int[] is_544_) {
	method15185(i, i_543_, is, is_544_);
    }
    
    public final void method3674(int i, int i_545_) {
	if (aClass153_9592 != null)
	    aClass153_9592.method2553(i, i_545_);
    }
    
    public final void method3533(int i, int i_546_) {
	if (aClass153_9592 != null)
	    aClass153_9592.method2553(i, i_546_);
    }
    
    public boolean method3390() {
	return true;
    }
    
    public final void method3285(int i, int i_547_) {
	if (aClass153_9592 != null)
	    aClass153_9592.method2553(i, i_547_);
    }
    
    public final boolean method3536() {
	if (aClass153_9592 == null)
	    return false;
	return aClass153_9592.method2564();
    }
    
    public final boolean method3455() {
	if (aClass153_9592 == null)
	    return false;
	return aClass153_9592.method2564();
    }
    
    public final boolean method3630() {
	if (aClass153_9592 == null)
	    return false;
	return aClass153_9592.method2564();
    }
    
    public final boolean method3473() {
	if (aClass153_9592 == null)
	    return false;
	return aClass153_9592.method2564();
    }
    
    final void method15292(boolean bool) {
	if (bool != aBool9627) {
	    aBool9627 = bool;
	    method15241();
	    anInt9591 &= ~0x7;
	}
    }
    
    final void method15293(boolean bool) {
	if (bool != aBool9599) {
	    aBool9599 = bool;
	    method15245();
	    anInt9591 &= ~0x1f;
	}
    }
    
    public int method3331(int i, int i_548_) {
	return i | i_548_;
    }
    
    void method15294() {
	aFloatArray9714[0] = aFloat9646 * aFloat9643;
	aFloatArray9714[1] = aFloat9646 * aFloat9644;
	aFloatArray9714[2] = aFloat9646 * aFloat9645;
	aFloatArray9714[3] = 1.0F;
	OpenGL.glLightModelfv(2899, aFloatArray9714, 0);
    }
    
    public void method3544(float f, float f_549_, float f_550_, float f_551_,
			   float f_552_) {
	Class534_Sub9_Sub3.aFloat11642 = f;
	Class534_Sub9_Sub3.aFloat11645 = f_549_;
	Class534_Sub9_Sub3.aFloat11641 = f_550_;
	Class534_Sub9_Sub3.aFloat11640 = f_551_;
	Class534_Sub9_Sub3.aFloat11643 = f_552_;
    }
    
    public void method3545(float f, float f_553_, float f_554_, float f_555_,
			   float f_556_) {
	Class534_Sub9_Sub3.aFloat11642 = f;
	Class534_Sub9_Sub3.aFloat11645 = f_553_;
	Class534_Sub9_Sub3.aFloat11641 = f_554_;
	Class534_Sub9_Sub3.aFloat11640 = f_555_;
	Class534_Sub9_Sub3.aFloat11643 = f_556_;
    }
    
    void method15295() {
	aFloatArray9714[0] = aFloat9686 * aFloat9643;
	aFloatArray9714[1] = aFloat9686 * aFloat9644;
	aFloatArray9714[2] = aFloat9686 * aFloat9645;
	aFloatArray9714[3] = 1.0F;
	OpenGL.glLightfv(16384, 4609, aFloatArray9714, 0);
	aFloatArray9714[0] = -aFloat9587 * aFloat9643;
	aFloatArray9714[1] = -aFloat9587 * aFloat9644;
	aFloatArray9714[2] = -aFloat9587 * aFloat9645;
	aFloatArray9714[3] = 1.0F;
	OpenGL.glLightfv(16385, 4609, aFloatArray9714, 0);
    }
    
    public Class170 method3422(int[] is) {
	return new Class170_Sub2(this, is);
    }
    
    public void method3435(int i, int i_557_, int i_558_, int i_559_,
			   int i_560_, int i_561_, Class145 class145,
			   int i_562_, int i_563_) {
	Class145_Sub3 class145_sub3 = (Class145_Sub3) class145;
	Class141_Sub2_Sub1 class141_sub2_sub1
	    = class145_sub3.aClass141_Sub2_Sub1_9876;
	method15224();
	method15231(class145_sub3.aClass141_Sub2_Sub1_9876);
	method15246(i_561_);
	method15232(7681, 8448);
	method15325(0, 34167, 768);
	float f = (class141_sub2_sub1.aFloat11378
		   / (float) class141_sub2_sub1.anInt11379);
	float f_564_ = (class141_sub2_sub1.aFloat11380
			/ (float) class141_sub2_sub1.anInt11381);
	float f_565_ = (float) i_558_ - (float) i;
	float f_566_ = (float) i_559_ - (float) i_557_;
	float f_567_ = (float) (1.0 / Math.sqrt((double) (f_565_ * f_565_
							  + f_566_ * f_566_)));
	f_565_ *= f_567_;
	f_566_ *= f_567_;
	OpenGL.glColor4ub((byte) (i_560_ >> 16), (byte) (i_560_ >> 8),
			  (byte) i_560_, (byte) (i_560_ >> 24));
	OpenGL.glBegin(1);
	OpenGL.glTexCoord2f(f * (float) (i - i_562_),
			    f_564_ * (float) (i_557_ - i_563_));
	OpenGL.glVertex2f((float) i + 0.35F, (float) i_557_ + 0.35F);
	OpenGL.glTexCoord2f(f * (float) (i_558_ - i_562_),
			    f_564_ * (float) (i_559_ - i_563_));
	OpenGL.glVertex2f((float) i_558_ + f_565_ + 0.35F,
			  (float) i_559_ + f_566_ + 0.35F);
	OpenGL.glEnd();
	method15325(0, 5890, 768);
    }
    
    public Class446 method3497() {
	return aClass446_9570;
    }
    
    public final void method3550() {
	if (aClass534_Sub9_Sub2_9564 != null
	    && aClass534_Sub9_Sub2_9564.method16109()) {
	    aClass153_9592.method2555(aClass534_Sub9_Sub2_9564);
	    aClass161_9659.method2626();
	}
    }
    
    public final void method3648() {
	if (aClass534_Sub9_Sub2_9564 != null
	    && aClass534_Sub9_Sub2_9564.method16109()) {
	    aClass153_9592.method2555(aClass534_Sub9_Sub2_9564);
	    aClass161_9659.method2626();
	}
    }
    
    final void method15296(int i, boolean bool, boolean bool_568_) {
	if (i != anInt9649 || aBool9598 != aBool9695) {
	    Class141_Sub2 class141_sub2 = null;
	    byte i_569_ = 0;
	    byte i_570_ = 0;
	    int i_571_ = 0;
	    byte i_572_ = aBool9695 ? (byte) 3 : (byte) 0;
	    byte i_573_ = 0;
	    if (i >= 0) {
		Class186 class186 = aClass177_2012.method2931(i, (byte) 1);
		if (class186.aBool2043) {
		    class141_sub2 = aClass161_9659.method2622(class186);
		    if (class186.aByte2047 != 0 || class186.aByte2048 != 0)
			method15237(((float) (anInt9696 % 128000) / 1000.0F
				     * (float) class186.aByte2047 / 64.0F
				     % 1.0F),
				    ((float) (anInt9696 % 128000) / 1000.0F
				     * (float) class186.aByte2048 / 64.0F
				     % 1.0F),
				    0.0F);
		    else
			method15238();
		    if (!aBool9695) {
			i_570_ = class186.aByte2068;
			i_571_ = class186.anInt2069 * 1880963453;
			i_572_ = class186.aByte2067;
		    }
		    i_569_ = class186.aByte2075;
		} else
		    method15238();
		if (class186.aClass599_2064 == Class599.aClass599_7868)
		    i_573_ = class186.aByte2051;
	    } else
		method15238();
	    method15251(i_573_);
	    aClass143_9562.method2417(i_572_, i_570_, i_571_, bool, bool_568_);
	    if (!aClass143_9562.method2419(class141_sub2, i_569_)) {
		method15231(class141_sub2);
		method15214(i_569_);
	    }
	    aBool9598 = aBool9695;
	    anInt9649 = i;
	}
	anInt9591 &= ~0x7;
    }
    
    public final void method3542() {
	if (aClass534_Sub9_Sub2_9564 != null
	    && aClass534_Sub9_Sub2_9564.method16109()) {
	    aClass153_9592.method2555(aClass534_Sub9_Sub2_9564);
	    aClass161_9659.method2626();
	}
    }
    
    public final boolean method3554() {
	return (aClass534_Sub9_Sub2_9564 != null
		&& aClass534_Sub9_Sub2_9564.method16109());
    }
    
    final void method3555(float f, float f_574_, float f_575_, float f_576_,
			  float f_577_, float f_578_) {
	Class534_Sub9_Sub2.aFloat11615 = f;
	Class534_Sub9_Sub2.aFloat11614 = f_574_;
	Class534_Sub9_Sub2.aFloat11613 = f_575_;
    }
    
    public void method3556(int i, Class166 class166) {
	anInt9664 = i;
	aClass166_9665 = class166;
	aBool9695 = true;
    }
    
    public void method3231(int i, Class166 class166) {
	anInt9664 = i;
	aClass166_9665 = class166;
	aBool9695 = true;
    }
    
    public final void method3530(int i, int i_579_, int i_580_, int i_581_) {
	if (aClass153_9592 != null)
	    aClass153_9592.method2552(i, i_579_, i_580_, i_581_);
    }
    
    final void method15297(int i, int i_582_) {
	if (anInt9672 == 0) {
	    boolean bool = false;
	    if (anInt9573 != i) {
		OpenGL.glTexEnvi(8960, 34161, i);
		anInt9573 = i;
		bool = true;
	    }
	    if (anInt9670 != i_582_) {
		OpenGL.glTexEnvi(8960, 34162, i_582_);
		anInt9670 = i_582_;
		bool = true;
	    }
	    if (bool)
		anInt9591 &= ~0x1d;
	} else {
	    OpenGL.glTexEnvi(8960, 34161, i);
	    OpenGL.glTexEnvi(8960, 34162, i_582_);
	}
    }
    
    public void method3560(int i, Class166 class166) {
	if (!aBool9695)
	    throw new RuntimeException("");
	anInt9664 = i;
	aClass166_9665 = class166;
	if (aBool9598) {
	    aClass143_9562.aClass136_Sub4_1653.method14478();
	    aClass143_9562.aClass136_Sub4_1653.method14477();
	}
    }
    
    public void method3561(int i, Class166 class166) {
	if (!aBool9695)
	    throw new RuntimeException("");
	anInt9664 = i;
	aClass166_9665 = class166;
	if (aBool9598) {
	    aClass143_9562.aClass136_Sub4_1653.method14478();
	    aClass143_9562.aClass136_Sub4_1653.method14477();
	}
    }
    
    final int method15298(int i) {
	if (i == 1)
	    return 7681;
	if (i == 0)
	    return 8448;
	if (i == 2)
	    return 34165;
	if (i == 3)
	    return 260;
	if (i == 4)
	    return 34023;
	throw new IllegalArgumentException();
    }
    
    public void method3563() {
	aBool9695 = false;
    }
    
    public void method3564() {
	aBool9695 = false;
    }
    
    public void method3305(int i, int i_583_, int i_584_, int i_585_,
			   int i_586_, int i_587_, int i_588_, int i_589_,
			   int i_590_) {
	if (i != i_584_ || i_583_ != i_585_) {
	    method15223();
	    method15246(i_587_);
	    float f = (float) i_584_ - (float) i;
	    float f_591_ = (float) i_585_ - (float) i_583_;
	    float f_592_
		= (float) (1.0
			   / Math.sqrt((double) (f * f + f_591_ * f_591_)));
	    f *= f_592_;
	    f_591_ *= f_592_;
	    OpenGL.glColor4ub((byte) (i_586_ >> 16), (byte) (i_586_ >> 8),
			      (byte) i_586_, (byte) (i_586_ >> 24));
	    i_590_ %= i_589_ + i_588_;
	    float f_593_ = f * (float) i_588_;
	    float f_594_ = f_591_ * (float) i_588_;
	    float f_595_ = 0.0F;
	    float f_596_ = 0.0F;
	    float f_597_ = f_593_;
	    float f_598_ = f_594_;
	    if (i_590_ > i_588_) {
		f_595_ = f * (float) (i_588_ + i_589_ - i_590_);
		f_596_ = f_591_ * (float) (i_588_ + i_589_ - i_590_);
	    } else {
		f_597_ = f * (float) (i_588_ - i_590_);
		f_598_ = f_591_ * (float) (i_588_ - i_590_);
	    }
	    float f_599_ = (float) i + 0.35F + f_595_;
	    float f_600_ = (float) i_583_ + 0.35F + f_596_;
	    float f_601_ = f * (float) i_589_;
	    float f_602_ = f_591_ * (float) i_589_;
	    for (;;) {
		if (i_584_ > i) {
		    if (f_599_ > (float) i_584_ + 0.35F)
			break;
		    if (f_599_ + f_597_ > (float) i_584_)
			f_597_ = (float) i_584_ - f_599_;
		} else {
		    if (f_599_ < (float) i_584_ + 0.35F)
			break;
		    if (f_599_ + f_597_ < (float) i_584_)
			f_597_ = (float) i_584_ - f_599_;
		}
		if (i_585_ > i_583_) {
		    if (f_600_ > (float) i_585_ + 0.35F)
			break;
		    if (f_600_ + f_598_ > (float) i_585_)
			f_598_ = (float) i_585_ - f_600_;
		} else {
		    if (f_600_ < (float) i_585_ + 0.35F)
			break;
		    if (f_600_ + f_598_ < (float) i_585_)
			f_598_ = (float) i_585_ - f_600_;
		}
		OpenGL.glBegin(1);
		OpenGL.glVertex2f(f_599_, f_600_);
		OpenGL.glVertex2f(f_599_ + f_597_, f_600_ + f_598_);
		OpenGL.glEnd();
		f_599_ += f_601_ + f_597_;
		f_600_ += f_602_ + f_598_;
		f_597_ = f_593_;
		f_598_ = f_594_;
	    }
	}
    }
    
    public final void method3448(int i, int i_603_, int i_604_, int i_605_) {
	if (anInt9690 < i)
	    anInt9690 = i;
	if (anInt9628 > i_604_)
	    anInt9628 = i_604_;
	if (anInt9625 < i_603_)
	    anInt9625 = i_603_;
	if (anInt9626 > i_605_)
	    anInt9626 = i_605_;
	OpenGL.glEnable(3089);
	method15198();
    }
    
    public void method3363(int i, Class166 class166) {
	anInt9664 = i;
	aClass166_9665 = class166;
	aBool9695 = true;
    }
    
    public void method3568(float f, float f_606_, float f_607_, float[] fs) {
	float f_608_ = (aClass433_9607.aFloatArray4853[14]
			+ aClass433_9607.aFloatArray4853[2] * f
			+ aClass433_9607.aFloatArray4853[6] * f_606_
			+ aClass433_9607.aFloatArray4853[10] * f_607_);
	float f_609_ = (aClass433_9607.aFloatArray4853[15]
			+ aClass433_9607.aFloatArray4853[3] * f
			+ aClass433_9607.aFloatArray4853[7] * f_606_
			+ aClass433_9607.aFloatArray4853[11] * f_607_);
	if (f_608_ < -f_609_ || f_608_ > f_609_) {
	    float[] fs_610_ = fs;
	    float[] fs_611_ = fs;
	    fs[2] = Float.NaN;
	    fs_611_[1] = Float.NaN;
	    fs_610_[0] = Float.NaN;
	} else {
	    float f_612_ = (aClass433_9607.aFloatArray4853[12]
			    + aClass433_9607.aFloatArray4853[0] * f
			    + aClass433_9607.aFloatArray4853[4] * f_606_
			    + aClass433_9607.aFloatArray4853[8] * f_607_);
	    if (f_612_ < -f_609_ || f_612_ > f_609_) {
		float[] fs_613_ = fs;
		float[] fs_614_ = fs;
		fs[2] = Float.NaN;
		fs_614_[1] = Float.NaN;
		fs_613_[0] = Float.NaN;
	    } else {
		float f_615_ = (aClass433_9607.aFloatArray4853[13]
				+ aClass433_9607.aFloatArray4853[1] * f
				+ aClass433_9607.aFloatArray4853[5] * f_606_
				+ aClass433_9607.aFloatArray4853[9] * f_607_);
		if (f_615_ < -f_609_ || f_615_ > f_609_) {
		    float[] fs_616_ = fs;
		    float[] fs_617_ = fs;
		    fs[2] = Float.NaN;
		    fs_617_[1] = Float.NaN;
		    fs_616_[0] = Float.NaN;
		} else {
		    float f_618_
			= (aClass433_9533.aFloatArray4853[14]
			   + aClass433_9533.aFloatArray4853[2] * f
			   + aClass433_9533.aFloatArray4853[6] * f_606_
			   + aClass433_9533.aFloatArray4853[10] * f_607_);
		    fs[0] = aFloat9610 + aFloat9663 * f_612_ / f_609_;
		    fs[1] = aFloat9612 + aFloat9613 * f_615_ / f_609_;
		    fs[2] = f_618_;
		}
	    }
	}
    }
    
    public Class175_Sub1 method3569() {
	return new Class175_Sub1_Sub2(this);
    }
    
    public Class175_Sub1 method3570() {
	return new Class175_Sub1_Sub2(this);
    }
    
    public void method3565(int i, int i_619_, float f, int i_620_, int i_621_,
			   float f_622_, int i_623_, int i_624_, float f_625_,
			   int i_626_, int i_627_, int i_628_, int i_629_) {
	method15223();
	method15246(i_629_);
	OpenGL.glBegin(4);
	OpenGL.glColor4ub((byte) (i_626_ >> 16), (byte) (i_626_ >> 8),
			  (byte) i_626_, (byte) (i_626_ >> 24));
	OpenGL.glVertex3f((float) i + 0.35F, (float) i_619_ + 0.35F, f);
	OpenGL.glColor4ub((byte) (i_627_ >> 16), (byte) (i_627_ >> 8),
			  (byte) i_627_, (byte) (i_627_ >> 24));
	OpenGL.glVertex3f((float) i_620_ + 0.35F, (float) i_621_ + 0.35F,
			  f_622_);
	OpenGL.glColor4ub((byte) (i_628_ >> 16), (byte) (i_628_ >> 8),
			  (byte) i_628_, (byte) (i_628_ >> 24));
	OpenGL.glVertex3f((float) i_623_ + 0.35F, (float) i_624_ + 0.35F,
			  f_625_);
	OpenGL.glEnd();
    }
    
    public Interface22 method3276(int i, int i_630_, Class181 class181,
				  Class173 class173, int i_631_) {
	return new Class534_Sub18_Sub1(this, class181, class173, i, i_630_,
				       i_631_);
    }
    
    public Interface21 method3572(int i, int i_632_) {
	return new Class534_Sub18_Sub1(this, Class181.aClass181_1956,
				       Class173.aClass173_1828, i, i_632_);
    }
    
    public Interface21 method3573(int i, int i_633_) {
	return new Class534_Sub18_Sub1(this, Class181.aClass181_1956,
				       Class173.aClass173_1828, i, i_633_);
    }
    
    public boolean method3541() {
	return (aClass534_Sub9_Sub3_9565 != null
		&& aClass534_Sub9_Sub3_9565.method16109());
    }
    
    public Interface21 method3627(int i, int i_634_, int i_635_) {
	return new Class534_Sub18_Sub1(this, Class181.aClass181_1956,
				       Class173.aClass173_1828, i, i_634_,
				       i_635_);
    }
    
    public Interface21 method3645(int i, int i_636_, int i_637_) {
	return new Class534_Sub18_Sub1(this, Class181.aClass181_1956,
				       Class173.aClass173_1828, i, i_636_,
				       i_637_);
    }
    
    void method15299() {
	int i = 0;
	while (!anOpenGL9559.method1767()) {
	    if (i++ > 5)
		throw new RuntimeException("");
	    Class251.method4622(1000L);
	}
    }
    
    void method15300() {
	aClass141Array9519 = new Class141[anInt9682];
	aClass141_Sub2_9528
	    = new Class141_Sub2(this, 3553, Class181.aClass181_1952,
				Class173.aClass173_1830, 1, 1);
	new Class141_Sub2(this, 3553, Class181.aClass181_1952,
			  Class173.aClass173_1830, 1, 1);
	new Class141_Sub2(this, 3553, Class181.aClass181_1952,
			  Class173.aClass173_1830, 1, 1);
	for (int i = 0; i < 8; i++) {
	    aClass183_Sub1Array9707[i] = new Class183_Sub1(this);
	    aClass183_Sub1Array9709[i] = new Class183_Sub1(this);
	}
	if (aBool9685) {
	    aClass175_Sub1_Sub2_9710 = new Class175_Sub1_Sub2(this);
	    new Class175_Sub1_Sub2(this);
	}
    }
    
    void method15301() {
	aClass141Array9519 = new Class141[anInt9682];
	aClass141_Sub2_9528
	    = new Class141_Sub2(this, 3553, Class181.aClass181_1952,
				Class173.aClass173_1830, 1, 1);
	new Class141_Sub2(this, 3553, Class181.aClass181_1952,
			  Class173.aClass173_1830, 1, 1);
	new Class141_Sub2(this, 3553, Class181.aClass181_1952,
			  Class173.aClass173_1830, 1, 1);
	for (int i = 0; i < 8; i++) {
	    aClass183_Sub1Array9707[i] = new Class183_Sub1(this);
	    aClass183_Sub1Array9709[i] = new Class183_Sub1(this);
	}
	if (aBool9685) {
	    aClass175_Sub1_Sub2_9710 = new Class175_Sub1_Sub2(this);
	    new Class175_Sub1_Sub2(this);
	}
    }
    
    void method15302() {
	aClass141Array9519 = new Class141[anInt9682];
	aClass141_Sub2_9528
	    = new Class141_Sub2(this, 3553, Class181.aClass181_1952,
				Class173.aClass173_1830, 1, 1);
	new Class141_Sub2(this, 3553, Class181.aClass181_1952,
			  Class173.aClass173_1830, 1, 1);
	new Class141_Sub2(this, 3553, Class181.aClass181_1952,
			  Class173.aClass173_1830, 1, 1);
	for (int i = 0; i < 8; i++) {
	    aClass183_Sub1Array9707[i] = new Class183_Sub1(this);
	    aClass183_Sub1Array9709[i] = new Class183_Sub1(this);
	}
	if (aBool9685) {
	    aClass175_Sub1_Sub2_9710 = new Class175_Sub1_Sub2(this);
	    new Class175_Sub1_Sub2(this);
	}
    }
    
    void method15303() {
	method15227(-2);
	for (int i = anInt9682 - 1; i >= 0; i--) {
	    method15230(i);
	    method15231(null);
	    OpenGL.glTexEnvi(8960, 8704, 34160);
	}
	method15232(8448, 8448);
	method15325(2, 34168, 770);
	method15238();
	anInt9708 = 1;
	anInt9596 = 0;
	aBool9553 = true;
	if (aBool9704)
	    OpenGL.glBlendFuncSeparate(770, 771, 0, 0);
	else
	    OpenGL.glBlendFunc(770, 771);
	anInt9636 = 1;
	aByte9673 = (byte) -1;
	method15251((byte) 0);
	aBool9597 = true;
	OpenGL.glEnable(3008);
	OpenGL.glAlphaFunc(516, (float) aByte9673);
	if (aBool9688) {
	    if (anInt9668 == 0)
		OpenGL.glDisable(32925);
	    else
		OpenGL.glEnable(32925);
	    OpenGL.glDisable(32926);
	}
	OpenGL.glColorMask(true, true, true, true);
	aBool9594 = true;
	method15344(true);
	method15432(true);
	method15242(true);
	method15193(true);
	method3284(0.0F, 1.0F);
	method15203();
	anOpenGL9559.setSwapInterval(0);
	OpenGL.glShadeModel(7425);
	OpenGL.glClearDepth(1.0F);
	OpenGL.glDepthFunc(515);
	OpenGL.glPolygonMode(1028, 6914);
	method15255(anInt9658);
	OpenGL.glMatrixMode(5888);
	OpenGL.glLoadIdentity();
	OpenGL.glColorMaterial(1028, 5634);
	OpenGL.glEnable(2903);
	float[] fs = { 0.0F, 0.0F, 0.0F, 1.0F };
	for (int i = 0; i < 8; i++) {
	    int i_638_ = 16384 + i;
	    OpenGL.glLightfv(i_638_, 4608, fs, 0);
	    OpenGL.glLightf(i_638_, 4615, 0.0F);
	    OpenGL.glLightf(i_638_, 4616, 0.0F);
	}
	OpenGL.glEnable(16384);
	OpenGL.glEnable(16385);
	OpenGL.glFogf(2914, 0.95F);
	OpenGL.glFogi(2917, 9729);
	OpenGL.glHint(3156, 4353);
	if (aBool9534)
	    OpenGL.glEnable(34895);
	anInt9687 = -1;
	anInt9642 = -1;
	method3281();
	method3537();
    }
    
    void method15304() {
	method15227(-2);
	for (int i = anInt9682 - 1; i >= 0; i--) {
	    method15230(i);
	    method15231(null);
	    OpenGL.glTexEnvi(8960, 8704, 34160);
	}
	method15232(8448, 8448);
	method15325(2, 34168, 770);
	method15238();
	anInt9708 = 1;
	anInt9596 = 0;
	aBool9553 = true;
	if (aBool9704)
	    OpenGL.glBlendFuncSeparate(770, 771, 0, 0);
	else
	    OpenGL.glBlendFunc(770, 771);
	anInt9636 = 1;
	aByte9673 = (byte) -1;
	method15251((byte) 0);
	aBool9597 = true;
	OpenGL.glEnable(3008);
	OpenGL.glAlphaFunc(516, (float) aByte9673);
	if (aBool9688) {
	    if (anInt9668 == 0)
		OpenGL.glDisable(32925);
	    else
		OpenGL.glEnable(32925);
	    OpenGL.glDisable(32926);
	}
	OpenGL.glColorMask(true, true, true, true);
	aBool9594 = true;
	method15344(true);
	method15432(true);
	method15242(true);
	method15193(true);
	method3284(0.0F, 1.0F);
	method15203();
	anOpenGL9559.setSwapInterval(0);
	OpenGL.glShadeModel(7425);
	OpenGL.glClearDepth(1.0F);
	OpenGL.glDepthFunc(515);
	OpenGL.glPolygonMode(1028, 6914);
	method15255(anInt9658);
	OpenGL.glMatrixMode(5888);
	OpenGL.glLoadIdentity();
	OpenGL.glColorMaterial(1028, 5634);
	OpenGL.glEnable(2903);
	float[] fs = { 0.0F, 0.0F, 0.0F, 1.0F };
	for (int i = 0; i < 8; i++) {
	    int i_639_ = 16384 + i;
	    OpenGL.glLightfv(i_639_, 4608, fs, 0);
	    OpenGL.glLightf(i_639_, 4615, 0.0F);
	    OpenGL.glLightf(i_639_, 4616, 0.0F);
	}
	OpenGL.glEnable(16384);
	OpenGL.glEnable(16385);
	OpenGL.glFogf(2914, 0.95F);
	OpenGL.glFogi(2917, 9729);
	OpenGL.glHint(3156, 4353);
	if (aBool9534)
	    OpenGL.glEnable(34895);
	anInt9687 = -1;
	anInt9642 = -1;
	method3281();
	method3537();
    }
    
    public Class165 method3451(Class165 class165, Class165 class165_640_,
			       float f, Class165 class165_641_) {
	if (class165 != null && class165_640_ != null && aBool9694
	    && aBool9685) {
	    Class165_Sub1_Sub1 class165_sub1_sub1 = null;
	    Class165_Sub1 class165_sub1 = (Class165_Sub1) class165;
	    Class165_Sub1 class165_sub1_642_ = (Class165_Sub1) class165_640_;
	    Class141_Sub1 class141_sub1 = class165_sub1.method15068();
	    Class141_Sub1 class141_sub1_643_
		= class165_sub1_642_.method15068();
	    if (class141_sub1 != null && class141_sub1_643_ != null) {
		int i = (class141_sub1.anInt8917 > class141_sub1_643_.anInt8917
			 ? class141_sub1.anInt8917
			 : class141_sub1_643_.anInt8917);
		if (class165 != class165_641_ && class165_640_ != class165_641_
		    && class165_641_ instanceof Class165_Sub1_Sub1) {
		    Class165_Sub1_Sub1 class165_sub1_sub1_644_
			= (Class165_Sub1_Sub1) class165_641_;
		    if (class165_sub1_sub1_644_.method17964() == i)
			class165_sub1_sub1 = class165_sub1_sub1_644_;
		}
		if (class165_sub1_sub1 == null)
		    class165_sub1_sub1 = new Class165_Sub1_Sub1(this, i);
		if (class165_sub1_sub1.method17962(class141_sub1,
						   class141_sub1_643_, f))
		    return class165_sub1_sub1;
	    }
	}
	return f < 0.5F ? class165 : class165_640_;
    }
    
    boolean method3420(int i, int i_645_, int i_646_, int i_647_,
		       Class446 class446, Class432 class432) {
	Class433 class433 = aClass433_9530;
	class433.method6916(class446);
	class433.method6839(aClass433_9607);
	return class432.method6838(i, i_645_, i_646_, i_647_, class433,
				   aFloat9610, aFloat9612, aFloat9663,
				   aFloat9613);
    }
    
    void method3557() {
	for (Class534 class534 = aClass700_9577.method14135((byte) -1);
	     class534 != null;
	     class534 = aClass700_9577.method14139(1678728551))
	    ((Class534_Sub2_Sub2) class534).method18232();
	if (aClass153_9592 != null)
	    aClass153_9592.method2547();
	if (aBool9575) {
	    Class54.method1212(false, true, -1394408799);
	    aBool9575 = false;
	}
    }
    
    void method15305() {
	aFloat9679 = aFloat9617 - (float) anInt9657 - aFloat9661;
	aFloat9576 = aFloat9679 - (float) anInt9675 * aFloat9660;
	if (aFloat9576 < aFloat9616)
	    aFloat9576 = aFloat9616;
	OpenGL.glFogf(2915, aFloat9576);
	OpenGL.glFogf(2916, aFloat9679);
	aFloatArray9714[0] = (float) (anInt9687 & 0xff0000) / 1.671168E7F;
	aFloatArray9714[1] = (float) (anInt9687 & 0xff00) / 65280.0F;
	aFloatArray9714[2] = (float) (anInt9687 & 0xff) / 255.0F;
	OpenGL.glFogfv(2918, aFloatArray9714, 0);
    }
    
    public boolean method3583() {
	return true;
    }
    
    public boolean method3496() {
	return true;
    }
    
    public final void method3380(int i, int i_648_) {
	if (aClass153_9592 != null)
	    aClass153_9592.method2553(i, i_648_);
    }
    
    boolean method15306() {
	return aClass143_9562.method2416(3);
    }
    
    boolean method15307() {
	return aClass143_9562.method2416(3);
    }
    
    void method15308() {
	method15422();
    }
    
    void method15309() {
	method15422();
    }
    
    void method15310() {
	method15422();
    }
    
    void method15311() {
	int i = aClass175_1989.method2910();
	int i_649_ = aClass175_1989.method2911();
	aClass433_9606.method6853(0.0F, (float) i, 0.0F, (float) i_649_, -1.0F,
				  1.0F);
	method3281();
	method15203();
	method3537();
    }
    
    public long method3586(int i, int i_650_) {
	return method15185(i, i_650_, null, null);
    }
    
    long method15312(int i, int i_651_, int[] is, int[] is_652_) {
	if (aBool9692) {
	    if (aLongArray9726[anInt9728] != 0L) {
		OpenGL.glDeleteSync(aLongArray9726[anInt9728]);
		aLongArray9726[anInt9728] = 0L;
	    }
	    OpenGL.glBindBufferARB(35051, anIntArray9725[anInt9728]);
	    long l = OpenGL.glMapBufferARB(35051, 35000);
	    if (is != null) {
		int i_653_ = 0;
		for (int i_654_ = i_651_ - 1; i_654_ >= 0; i_654_--) {
		    for (int i_655_ = 0; i_655_ < i; i_655_++)
			is[i_653_++]
			    = anUnsafe9578.getInt(l + (long) ((i_654_ * i
							       + i_655_)
							      * 4));
		}
		if (OpenGL.glUnmapBufferARB(35051)) {
		    /* empty */
		}
		OpenGL.glBindBufferARB(35051, 0);
		l = 0L;
	    }
	    if (++anInt9728 >= 3)
		anInt9728 = 0;
	    return l;
	}
	if (aClass163_9723 == null)
	    method15399(i, i_651_);
	if (aClass163_9722 == null)
	    aClass163_9722 = method3319(0, 0, aClass175_1989.method2910(),
					aClass175_1989.method2911(), true);
	else
	    ((Class163_Sub3) aClass163_9722).method15485
		(0, 0, aClass175_1989.method2910(),
		 aClass175_1989.method2911(), 0, 0, true);
	method3260(aClass175_Sub1_9724, -1758547872);
	method3340(1, -16777216);
	aClass163_9722.method2659(anInt2018 * -1555714981,
				  anInt2019 * -385311879,
				  anInt2020 * 1769547157,
				  anInt2021 * -740019615);
	aClass163_9723.method2653(0, 0, i, i_651_, is, is_652_, 0, i);
	method3261(aClass175_Sub1_9724, -11578496);
	return 0L;
    }
    
    void method15313() {
	aFloatArray9714[0] = aFloat9686 * aFloat9643;
	aFloatArray9714[1] = aFloat9686 * aFloat9644;
	aFloatArray9714[2] = aFloat9686 * aFloat9645;
	aFloatArray9714[3] = 1.0F;
	OpenGL.glLightfv(16384, 4609, aFloatArray9714, 0);
	aFloatArray9714[0] = -aFloat9587 * aFloat9643;
	aFloatArray9714[1] = -aFloat9587 * aFloat9644;
	aFloatArray9714[2] = -aFloat9587 * aFloat9645;
	aFloatArray9714[3] = 1.0F;
	OpenGL.glLightfv(16385, 4609, aFloatArray9714, 0);
    }
    
    final Class141_Sub1 method15314() {
	return (aClass165_Sub1_9674 != null ? aClass165_Sub1_9674.method15068()
		: null);
    }
    
    long method15315(int i, int i_656_, int[] is, int[] is_657_) {
	if (aBool9692) {
	    if (aLongArray9726[anInt9728] != 0L) {
		OpenGL.glDeleteSync(aLongArray9726[anInt9728]);
		aLongArray9726[anInt9728] = 0L;
	    }
	    OpenGL.glBindBufferARB(35051, anIntArray9725[anInt9728]);
	    long l = OpenGL.glMapBufferARB(35051, 35000);
	    if (is != null) {
		int i_658_ = 0;
		for (int i_659_ = i_656_ - 1; i_659_ >= 0; i_659_--) {
		    for (int i_660_ = 0; i_660_ < i; i_660_++)
			is[i_658_++]
			    = anUnsafe9578.getInt(l + (long) ((i_659_ * i
							       + i_660_)
							      * 4));
		}
		if (OpenGL.glUnmapBufferARB(35051)) {
		    /* empty */
		}
		OpenGL.glBindBufferARB(35051, 0);
		l = 0L;
	    }
	    if (++anInt9728 >= 3)
		anInt9728 = 0;
	    return l;
	}
	if (aClass163_9723 == null)
	    method15399(i, i_656_);
	if (aClass163_9722 == null)
	    aClass163_9722 = method3319(0, 0, aClass175_1989.method2910(),
					aClass175_1989.method2911(), true);
	else
	    ((Class163_Sub3) aClass163_9722).method15485
		(0, 0, aClass175_1989.method2910(),
		 aClass175_1989.method2911(), 0, 0, true);
	method3260(aClass175_Sub1_9724, 532617796);
	method3340(1, -16777216);
	aClass163_9722.method2659(anInt2018 * -1555714981,
				  anInt2019 * -385311879,
				  anInt2020 * 1769547157,
				  anInt2021 * -740019615);
	aClass163_9723.method2653(0, 0, i, i_656_, is, is_657_, 0, i);
	method3261(aClass175_Sub1_9724, -11578496);
	return 0L;
    }
    
    void method15316(int i, int i_661_) {
	method3280();
	method3616(i, i_661_, 198186650);
	if (aBool9692) {
	    aClass163_9723 = method3315(i, i_661_, true, true);
	    aClass175_Sub1_9724 = method3263();
	    aClass175_Sub1_9724.method15075(0, aClass163_9723.method2651());
	    OpenGL.glGenBuffersARB(3, anIntArray9725, 0);
	    for (int i_662_ = 0; i_662_ < 3; i_662_++) {
		OpenGL.glBindBufferARB(35051, anIntArray9725[i_662_]);
		OpenGL.glBufferDataARBa(35051, i * i_661_ * 4, 0L, 35041);
		OpenGL.glBindBufferARB(35051, 0);
	    }
	} else {
	    aClass163_9723 = method3315(i, i_661_, true, true);
	    aClass175_Sub1_9724 = method3263();
	    aClass175_Sub1_9724.method15075(0, aClass163_9723.method2651());
	}
    }
    
    public Class163 method3274(Class169 class169, boolean bool) {
	int[] is = class169.method2766(false);
	Class163 class163
	    = method3317(is, 0, class169.method2762(), class169.method2762(),
			 class169.method2763(), (byte) -64);
	class163.method2645(class169.method2764(), class169.method2781(),
			    class169.method2765(), class169.method2767());
	return class163;
    }
    
    public Class165 method3486(Class165 class165, Class165 class165_663_,
			       float f, Class165 class165_664_) {
	if (class165 != null && class165_663_ != null && aBool9694
	    && aBool9685) {
	    Class165_Sub1_Sub1 class165_sub1_sub1 = null;
	    Class165_Sub1 class165_sub1 = (Class165_Sub1) class165;
	    Class165_Sub1 class165_sub1_665_ = (Class165_Sub1) class165_663_;
	    Class141_Sub1 class141_sub1 = class165_sub1.method15068();
	    Class141_Sub1 class141_sub1_666_
		= class165_sub1_665_.method15068();
	    if (class141_sub1 != null && class141_sub1_666_ != null) {
		int i = (class141_sub1.anInt8917 > class141_sub1_666_.anInt8917
			 ? class141_sub1.anInt8917
			 : class141_sub1_666_.anInt8917);
		if (class165 != class165_664_ && class165_663_ != class165_664_
		    && class165_664_ instanceof Class165_Sub1_Sub1) {
		    Class165_Sub1_Sub1 class165_sub1_sub1_667_
			= (Class165_Sub1_Sub1) class165_664_;
		    if (class165_sub1_sub1_667_.method17964() == i)
			class165_sub1_sub1 = class165_sub1_sub1_667_;
		}
		if (class165_sub1_sub1 == null)
		    class165_sub1_sub1 = new Class165_Sub1_Sub1(this, i);
		if (class165_sub1_sub1.method17962(class141_sub1,
						   class141_sub1_666_, f))
		    return class165_sub1_sub1;
	    }
	}
	return f < 0.5F ? class165 : class165_663_;
    }
    
    public void method3589(int i, Class145 class145, int i_668_, int i_669_) {
	Class145_Sub3 class145_sub3 = (Class145_Sub3) class145;
	Class141_Sub2_Sub1 class141_sub2_sub1
	    = class145_sub3.aClass141_Sub2_Sub1_9876;
	method15224();
	method15231(class145_sub3.aClass141_Sub2_Sub1_9876);
	method15246(1);
	method15232(7681, 8448);
	method15325(0, 34167, 768);
	float f = (class141_sub2_sub1.aFloat11378
		   / (float) class141_sub2_sub1.anInt11379);
	float f_670_ = (class141_sub2_sub1.aFloat11380
			/ (float) class141_sub2_sub1.anInt11381);
	OpenGL.glColor4ub((byte) (i >> 16), (byte) (i >> 8), (byte) i,
			  (byte) (i >> 24));
	OpenGL.glBegin(7);
	OpenGL.glTexCoord2f(f * (float) (anInt9690 - i_668_),
			    f_670_ * (float) (anInt9625 - i_669_));
	OpenGL.glVertex2i(anInt9690, anInt9625);
	OpenGL.glTexCoord2f(f * (float) (anInt9690 - i_668_),
			    f_670_ * (float) (anInt9626 - i_669_));
	OpenGL.glVertex2i(anInt9690, anInt9626);
	OpenGL.glTexCoord2f(f * (float) (anInt9628 - i_668_),
			    f_670_ * (float) (anInt9626 - i_669_));
	OpenGL.glVertex2i(anInt9628, anInt9626);
	OpenGL.glTexCoord2f(f * (float) (anInt9628 - i_668_),
			    f_670_ * (float) (anInt9625 - i_669_));
	OpenGL.glVertex2i(anInt9628, anInt9625);
	OpenGL.glEnd();
	method15325(0, 5890, 768);
    }
    
    public int method3379(int i, int i_671_) {
	return i | i_671_;
    }
    
    public Class433 method3591() {
	return aClass433_9655;
    }
    
    public String method3401() {
	String string = "Caps: 2:";
	String string_672_ = ":";
	string = new StringBuilder().append(string).append(anInt9668).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append(anInt9620).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append(anInt9682).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append(anInt9683).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append(anInt9684).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append(aFloat9542).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append(aFloat9706).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9566 ? 1 : 0).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9631 ? 1 : 0).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9582 ? 1 : 0).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9705 ? 1 : 0).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9691 ? 1 : 0).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9688 ? 1 : 0).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9656 ? 1 : 0).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9701 ? 1 : 0).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9700 ? 1 : 0).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9680 ? 1 : 0).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9693 ? 1 : 0).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9624 ? 1 : 0).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9694 ? 1 : 0).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9534 ? 1 : 0).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9698 ? 1 : 0).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9685 ? 1 : 0).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9603 ? 1 : 0).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9572 ? 1 : 0).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9697 ? 1 : 0).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9703 ? 1 : 0).append
		     (string_672_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9704 ? 1 : 0).toString();
	return string;
    }
    
    public void method3593(int[] is) {
	is[0] = anInt9648;
	is[1] = anInt9632;
	is[2] = anInt9633;
	is[3] = anInt9634;
    }
    
    public void method3594(int[] is) {
	is[0] = anInt9648;
	is[1] = anInt9632;
	is[2] = anInt9633;
	is[3] = anInt9634;
    }
    
    final void method15317(int i, int i_673_) {
	anInt9629 = i;
	anInt9630 = i_673_;
	method15197();
	method15198();
    }
    
    final void method15318(int i, int i_674_) {
	anInt9629 = i;
	anInt9630 = i_674_;
	method15197();
	method15198();
    }
    
    final void method15319() {
	if (anInt9591 != 1) {
	    method15204();
	    method15344(false);
	    method15432(false);
	    method15242(false);
	    method15193(false);
	    method15231(null);
	    method15227(-2);
	    method15214(1);
	    method15251((byte) 0);
	    anInt9591 = 1;
	}
    }
    
    final void method15320() {
	if (aClass175_1989 != null) {
	    int i;
	    int i_675_;
	    int i_676_;
	    int i_677_;
	    if (anInt9622 == 2) {
		i = anInt9648;
		i_675_ = anInt9632;
		i_676_ = anInt9633;
		i_677_ = anInt9634;
	    } else {
		i = 0;
		i_675_ = 0;
		i_676_ = aClass175_1989.method2910();
		i_677_ = aClass175_1989.method2911();
	    }
	    if (i_676_ < 1)
		i_676_ = 1;
	    if (i_677_ < 1)
		i_677_ = 1;
	    OpenGL.glViewport(anInt9629 + i,
			      (anInt9630 + aClass175_1989.method2911() - i_675_
			       - i_677_),
			      i_676_, i_677_);
	    aFloat9663 = (float) anInt9633 / 2.0F;
	    aFloat9613 = (float) anInt9634 / 2.0F;
	    aFloat9610 = (float) anInt9648 + aFloat9663;
	    aFloat9612 = (float) anInt9632 + aFloat9613;
	}
    }
    
    public Class171 method3503(Class16 class16, Class169[] class169s,
			       boolean bool) {
	return new Class171_Sub3(this, class16, class169s, bool);
    }
    
    final void method15321() {
	if (aClass175_1989 != null && anInt9690 < anInt9628
	    && anInt9625 < anInt9626)
	    OpenGL.glScissor(anInt9629 + anInt9690,
			     (anInt9630 + aClass175_1989.method2911()
			      - anInt9626),
			     anInt9628 - anInt9690, anInt9626 - anInt9625);
	else
	    OpenGL.glScissor(0, 0, 0, 0);
    }
    
    final void method15322() {
	OpenGL.glPushMatrix();
    }
    
    final void method15323() {
	OpenGL.glPushMatrix();
    }
    
    final void method15324() {
	OpenGL.glPushMatrix();
    }
    
    final void method15325(int i, int i_678_, int i_679_) {
	OpenGL.glTexEnvi(8960, 34176 + i, i_678_);
	OpenGL.glTexEnvi(8960, 34192 + i, i_679_);
    }
    
    final void method15326(Class433 class433) {
	OpenGL.glPushMatrix();
	OpenGL.glMultMatrixf(class433.aFloatArray4853, 0);
    }
    
    final void method15327() {
	if (aBool9671) {
	    OpenGL.glMatrixMode(5890);
	    OpenGL.glLoadIdentity();
	    OpenGL.glMatrixMode(5888);
	    aBool9671 = false;
	}
    }
    
    final void method15328(Class433 class433) {
	OpenGL.glLoadMatrixf(class433.aFloatArray4853, 0);
    }
    
    public final void method3360(int[] is) {
	is[0] = anInt9690;
	is[1] = anInt9625;
	is[2] = anInt9628;
	is[3] = anInt9626;
    }
    
    final void method15329() {
	OpenGL.glLoadIdentity();
	OpenGL.glMultMatrixf(aClass433_9533.aFloatArray4853, 0);
	if (aBool9598)
	    aClass143_9562.aClass136_Sub4_1653.method14478();
	method15184();
	method15421();
    }
    
    public void method3632(int i, int i_680_, int i_681_, int i_682_,
			   int i_683_, int i_684_) {
	float f = (float) i + 0.35F;
	float f_685_ = (float) i_680_ + 0.35F;
	float f_686_ = f + (float) i_681_;
	float f_687_ = f_685_ + (float) i_682_;
	method15223();
	method15246(i_684_);
	OpenGL.glColor4ub((byte) (i_683_ >> 16), (byte) (i_683_ >> 8),
			  (byte) i_683_, (byte) (i_683_ >> 24));
	if (aBool9688)
	    OpenGL.glDisable(32925);
	OpenGL.glBegin(7);
	OpenGL.glVertex2f(f, f_685_);
	OpenGL.glVertex2f(f, f_687_);
	OpenGL.glVertex2f(f_686_, f_687_);
	OpenGL.glVertex2f(f_686_, f_685_);
	OpenGL.glEnd();
	if (aBool9688)
	    OpenGL.glEnable(32925);
    }
    
    public void method3361(float f, float f_688_, float f_689_, float[] fs) {
	float f_690_ = (aClass433_9607.aFloatArray4853[14]
			+ aClass433_9607.aFloatArray4853[2] * f
			+ aClass433_9607.aFloatArray4853[6] * f_688_
			+ aClass433_9607.aFloatArray4853[10] * f_689_);
	float f_691_ = (aClass433_9607.aFloatArray4853[15]
			+ aClass433_9607.aFloatArray4853[3] * f
			+ aClass433_9607.aFloatArray4853[7] * f_688_
			+ aClass433_9607.aFloatArray4853[11] * f_689_);
	if (f_690_ < -f_691_ || f_690_ > f_691_) {
	    float[] fs_692_ = fs;
	    float[] fs_693_ = fs;
	    fs[2] = Float.NaN;
	    fs_693_[1] = Float.NaN;
	    fs_692_[0] = Float.NaN;
	} else {
	    float f_694_ = (aClass433_9607.aFloatArray4853[12]
			    + aClass433_9607.aFloatArray4853[0] * f
			    + aClass433_9607.aFloatArray4853[4] * f_688_
			    + aClass433_9607.aFloatArray4853[8] * f_689_);
	    if (f_694_ < -f_691_ || f_694_ > f_691_) {
		float[] fs_695_ = fs;
		float[] fs_696_ = fs;
		fs[2] = Float.NaN;
		fs_696_[1] = Float.NaN;
		fs_695_[0] = Float.NaN;
	    } else {
		float f_697_ = (aClass433_9607.aFloatArray4853[13]
				+ aClass433_9607.aFloatArray4853[1] * f
				+ aClass433_9607.aFloatArray4853[5] * f_688_
				+ aClass433_9607.aFloatArray4853[9] * f_689_);
		if (f_697_ < -f_691_ || f_697_ > f_691_) {
		    float[] fs_698_ = fs;
		    float[] fs_699_ = fs;
		    fs[2] = Float.NaN;
		    fs_699_[1] = Float.NaN;
		    fs_698_[0] = Float.NaN;
		} else {
		    float f_700_
			= (aClass433_9533.aFloatArray4853[14]
			   + aClass433_9533.aFloatArray4853[2] * f
			   + aClass433_9533.aFloatArray4853[6] * f_688_
			   + aClass433_9533.aFloatArray4853[10] * f_689_);
		    fs[0] = aFloat9610 + aFloat9663 * f_694_ / f_691_;
		    fs[1] = aFloat9612 + aFloat9613 * f_697_ / f_691_;
		    fs[2] = f_700_;
		}
	    }
	}
    }
    
    final void method15330() {
	if (anInt9622 != 0) {
	    anInt9622 = 0;
	    method15197();
	    method15354();
	    anInt9591 &= ~0x1f;
	}
    }
    
    final void method15331(int i, int i_701_) {
	if (anInt9672 == 0) {
	    boolean bool = false;
	    if (anInt9573 != i) {
		OpenGL.glTexEnvi(8960, 34161, i);
		anInt9573 = i;
		bool = true;
	    }
	    if (anInt9670 != i_701_) {
		OpenGL.glTexEnvi(8960, 34162, i_701_);
		anInt9670 = i_701_;
		bool = true;
	    }
	    if (bool)
		anInt9591 &= ~0x1d;
	} else {
	    OpenGL.glTexEnvi(8960, 34161, i);
	    OpenGL.glTexEnvi(8960, 34162, i_701_);
	}
    }
    
    final void method15332() {
	if (anInt9622 != 0) {
	    anInt9622 = 0;
	    method15197();
	    method15354();
	    anInt9591 &= ~0x1f;
	}
    }
    
    final void method15333() {
	if (anInt9622 != 1) {
	    anInt9622 = 1;
	    method15206();
	    method15197();
	    method15354();
	    OpenGL.glMatrixMode(5888);
	    OpenGL.glLoadIdentity();
	    anInt9591 &= ~0x18;
	}
    }
    
    final void method15334() {
	if (anInt9622 != 1) {
	    anInt9622 = 1;
	    method15206();
	    method15197();
	    method15354();
	    OpenGL.glMatrixMode(5888);
	    OpenGL.glLoadIdentity();
	    anInt9591 &= ~0x18;
	}
    }
    
    final void method15335() {
	if (anInt9622 != 1) {
	    anInt9622 = 1;
	    method15206();
	    method15197();
	    method15354();
	    OpenGL.glMatrixMode(5888);
	    OpenGL.glLoadIdentity();
	    anInt9591 &= ~0x18;
	}
    }
    
    public String method3402() {
	String string = "Caps: 2:";
	String string_702_ = ":";
	string = new StringBuilder().append(string).append(anInt9668).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append(anInt9620).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append(anInt9682).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append(anInt9683).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append(anInt9684).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append(aFloat9542).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append(aFloat9706).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9566 ? 1 : 0).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9631 ? 1 : 0).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9582 ? 1 : 0).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9705 ? 1 : 0).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9691 ? 1 : 0).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9688 ? 1 : 0).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9656 ? 1 : 0).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9701 ? 1 : 0).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9700 ? 1 : 0).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9680 ? 1 : 0).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9693 ? 1 : 0).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9624 ? 1 : 0).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9694 ? 1 : 0).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9534 ? 1 : 0).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9698 ? 1 : 0).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9685 ? 1 : 0).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9603 ? 1 : 0).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9572 ? 1 : 0).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9697 ? 1 : 0).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9703 ? 1 : 0).append
		     (string_702_).toString();
	string = new StringBuilder().append(string).append
		     (aBool9704 ? 1 : 0).toString();
	return string;
    }
    
    final void method15336() {
	if (anInt9622 != 2) {
	    anInt9622 = 2;
	    method15183(aClass433_9605.aFloatArray4853);
	    method15273();
	    method15197();
	    method15354();
	    anInt9591 &= ~0x7;
	}
    }
    
    final int method15337(int i) {
	if (i == 1)
	    return 7681;
	if (i == 0)
	    return 8448;
	if (i == 2)
	    return 34165;
	if (i == 3)
	    return 260;
	if (i == 4)
	    return 34023;
	throw new IllegalArgumentException();
    }
    
    final void method15338() {
	aFloat9617 = aClass433_9605.method6907();
	aFloat9616 = aClass433_9605.method6859();
	method15305();
	if (anInt9622 == 2)
	    method15183(aClass433_9605.aFloatArray4853);
	else if (anInt9622 == 1)
	    method15183(aClass433_9606.aFloatArray4853);
    }
    
    final void method15339() {
	aClass433_9607.method6842(aClass433_9533);
	aClass433_9607.method6839(aClass433_9605);
	aClass433_9607.method6862(aFloatArrayArray9608[0]);
	aClass433_9607.method6858(aFloatArrayArray9608[1]);
	aClass433_9607.method6861(aFloatArrayArray9608[2]);
	aClass433_9607.method6949(aFloatArrayArray9608[3]);
	aClass433_9607.method6879(aFloatArrayArray9608[4]);
	aClass433_9607.method6945(aFloatArrayArray9608[5]);
    }
    
    final void method15340() {
	aClass433_9607.method6842(aClass433_9533);
	aClass433_9607.method6839(aClass433_9605);
	aClass433_9607.method6862(aFloatArrayArray9608[0]);
	aClass433_9607.method6858(aFloatArrayArray9608[1]);
	aClass433_9607.method6861(aFloatArrayArray9608[2]);
	aClass433_9607.method6949(aFloatArrayArray9608[3]);
	aClass433_9607.method6879(aFloatArrayArray9608[4]);
	aClass433_9607.method6945(aFloatArrayArray9608[5]);
    }
    
    public Class534_Sub2 method3614(int i) {
	Class534_Sub2_Sub2 class534_sub2_sub2 = new Class534_Sub2_Sub2(i);
	aClass700_9577.method14131(class534_sub2_sub2, (short) 789);
	return class534_sub2_sub2;
    }
    
    public Class171 method3491(Class16 class16, Class169[] class169s,
			       boolean bool) {
	return new Class171_Sub3(this, class16, class169s, bool);
    }
    
    public void method3596(boolean bool) {
	aBool9623 = bool;
	method15244();
    }
    
    void method15341() {
	int i;
	for (i = 0; i < anInt9651; i++) {
	    Class534_Sub21 class534_sub21 = aClass534_Sub21Array9702[i];
	    int i_703_ = 16386 + i;
	    aFloatArray9715[0]
		= (float) class534_sub21.method16197(-1001175773);
	    aFloatArray9715[1]
		= (float) class534_sub21.method16226(-1835424129);
	    aFloatArray9715[2]
		= (float) class534_sub21.method16199((byte) -26);
	    aFloatArray9715[3] = 1.0F;
	    OpenGL.glLightfv(i_703_, 4611, aFloatArray9715, 0);
	    int i_704_ = class534_sub21.method16201((byte) 20);
	    float f = class534_sub21.method16202(-693604944) / 255.0F;
	    aFloatArray9715[0] = (float) (i_704_ >> 16 & 0xff) * f;
	    aFloatArray9715[1] = (float) (i_704_ >> 8 & 0xff) * f;
	    aFloatArray9715[2] = (float) (i_704_ & 0xff) * f;
	    OpenGL.glLightfv(i_703_, 4609, aFloatArray9715, 0);
	    OpenGL.glLightf(i_703_, 4617,
			    (1.0F
			     / (float) (class534_sub21.method16233(-593105027)
					* class534_sub21
					      .method16233(59536364))));
	    OpenGL.glEnable(i_703_);
	}
	for (/**/; i < anInt9601; i++)
	    OpenGL.glDisable(16386 + i);
	anInt9601 = anInt9651;
    }
    
    void method15342() {
	int i;
	for (i = 0; i < anInt9651; i++) {
	    Class534_Sub21 class534_sub21 = aClass534_Sub21Array9702[i];
	    int i_705_ = 16386 + i;
	    aFloatArray9715[0] = (float) class534_sub21.method16197(940897071);
	    aFloatArray9715[1]
		= (float) class534_sub21.method16226(-742492284);
	    aFloatArray9715[2]
		= (float) class534_sub21.method16199((byte) -20);
	    aFloatArray9715[3] = 1.0F;
	    OpenGL.glLightfv(i_705_, 4611, aFloatArray9715, 0);
	    int i_706_ = class534_sub21.method16201((byte) 57);
	    float f = class534_sub21.method16202(-693604944) / 255.0F;
	    aFloatArray9715[0] = (float) (i_706_ >> 16 & 0xff) * f;
	    aFloatArray9715[1] = (float) (i_706_ >> 8 & 0xff) * f;
	    aFloatArray9715[2] = (float) (i_706_ & 0xff) * f;
	    OpenGL.glLightfv(i_705_, 4609, aFloatArray9715, 0);
	    OpenGL.glLightf(i_705_, 4617,
			    (1.0F
			     / (float) (class534_sub21.method16233(-1689917217)
					* class534_sub21
					      .method16233(-785169034))));
	    OpenGL.glEnable(i_705_);
	}
	for (/**/; i < anInt9601; i++)
	    OpenGL.glDisable(16386 + i);
	anInt9601 = anInt9651;
    }
    
    public final void method3574(int i, float f, float f_707_, float f_708_,
				 float f_709_, float f_710_) {
	boolean bool = anInt9642 != i;
	if (bool || aFloat9686 != f || aFloat9587 != f_707_) {
	    anInt9642 = i;
	    aFloat9686 = f;
	    aFloat9587 = f_707_;
	    if (bool) {
		aFloat9643 = (float) (anInt9642 & 0xff0000) / 1.671168E7F;
		aFloat9644 = (float) (anInt9642 & 0xff00) / 65280.0F;
		aFloat9645 = (float) (anInt9642 & 0xff) / 255.0F;
		method15210();
	    }
	    method15313();
	}
	if (aFloatArray9677[0] != f_708_ || aFloatArray9677[1] != f_709_
	    || aFloatArray9677[2] != f_710_) {
	    aFloatArray9677[0] = f_708_;
	    aFloatArray9677[1] = f_709_;
	    aFloatArray9677[2] = f_710_;
	    aFloatArray9515[0] = -f_708_;
	    aFloatArray9515[1] = -f_709_;
	    aFloatArray9515[2] = -f_710_;
	    float f_711_
		= (float) (1.0 / Math.sqrt((double) (f_708_ * f_708_
						     + f_709_ * f_709_
						     + f_710_ * f_710_)));
	    aFloatArray9640[0] = f_708_ * f_711_;
	    aFloatArray9640[1] = f_709_ * f_711_;
	    aFloatArray9640[2] = f_710_ * f_711_;
	    aFloatArray9641[0] = -aFloatArray9640[0];
	    aFloatArray9641[1] = -aFloatArray9640[1];
	    aFloatArray9641[2] = -aFloatArray9640[2];
	    method15184();
	    anInt9652 = (int) (f_708_ * 256.0F / f_709_);
	    anInt9647 = (int) (f_710_ * 256.0F / f_709_);
	}
    }
    
    public final void method3347(int i, float f, float f_712_, float f_713_,
				 float f_714_, float f_715_) {
	boolean bool = anInt9642 != i;
	if (bool || aFloat9686 != f || aFloat9587 != f_712_) {
	    anInt9642 = i;
	    aFloat9686 = f;
	    aFloat9587 = f_712_;
	    if (bool) {
		aFloat9643 = (float) (anInt9642 & 0xff0000) / 1.671168E7F;
		aFloat9644 = (float) (anInt9642 & 0xff00) / 65280.0F;
		aFloat9645 = (float) (anInt9642 & 0xff) / 255.0F;
		method15210();
	    }
	    method15313();
	}
	if (aFloatArray9677[0] != f_713_ || aFloatArray9677[1] != f_714_
	    || aFloatArray9677[2] != f_715_) {
	    aFloatArray9677[0] = f_713_;
	    aFloatArray9677[1] = f_714_;
	    aFloatArray9677[2] = f_715_;
	    aFloatArray9515[0] = -f_713_;
	    aFloatArray9515[1] = -f_714_;
	    aFloatArray9515[2] = -f_715_;
	    float f_716_
		= (float) (1.0 / Math.sqrt((double) (f_713_ * f_713_
						     + f_714_ * f_714_
						     + f_715_ * f_715_)));
	    aFloatArray9640[0] = f_713_ * f_716_;
	    aFloatArray9640[1] = f_714_ * f_716_;
	    aFloatArray9640[2] = f_715_ * f_716_;
	    aFloatArray9641[0] = -aFloatArray9640[0];
	    aFloatArray9641[1] = -aFloatArray9640[1];
	    aFloatArray9641[2] = -aFloatArray9640[2];
	    method15184();
	    anInt9652 = (int) (f_713_ * 256.0F / f_714_);
	    anInt9647 = (int) (f_715_ * 256.0F / f_714_);
	}
    }
    
    long method15343(int i, int i_717_, int[] is, int[] is_718_) {
	if (aBool9692) {
	    if (aLongArray9726[anInt9728] != 0L) {
		OpenGL.glDeleteSync(aLongArray9726[anInt9728]);
		aLongArray9726[anInt9728] = 0L;
	    }
	    OpenGL.glBindBufferARB(35051, anIntArray9725[anInt9728]);
	    long l = OpenGL.glMapBufferARB(35051, 35000);
	    if (is != null) {
		int i_719_ = 0;
		for (int i_720_ = i_717_ - 1; i_720_ >= 0; i_720_--) {
		    for (int i_721_ = 0; i_721_ < i; i_721_++)
			is[i_719_++]
			    = anUnsafe9578.getInt(l + (long) ((i_720_ * i
							       + i_721_)
							      * 4));
		}
		if (OpenGL.glUnmapBufferARB(35051)) {
		    /* empty */
		}
		OpenGL.glBindBufferARB(35051, 0);
		l = 0L;
	    }
	    if (++anInt9728 >= 3)
		anInt9728 = 0;
	    return l;
	}
	if (aClass163_9723 == null)
	    method15399(i, i_717_);
	if (aClass163_9722 == null)
	    aClass163_9722 = method3319(0, 0, aClass175_1989.method2910(),
					aClass175_1989.method2911(), true);
	else
	    ((Class163_Sub3) aClass163_9722).method15485
		(0, 0, aClass175_1989.method2910(),
		 aClass175_1989.method2911(), 0, 0, true);
	method3260(aClass175_Sub1_9724, -641762740);
	method3340(1, -16777216);
	aClass163_9722.method2659(anInt2018 * -1555714981,
				  anInt2019 * -385311879,
				  anInt2020 * 1769547157,
				  anInt2021 * -740019615);
	aClass163_9723.method2653(0, 0, i, i_717_, is, is_718_, 0, i);
	method3261(aClass175_Sub1_9724, -11578496);
	return 0L;
    }
    
    public void method3454() {
	if (aBool9703 && aClass175_1989 != null) {
	    int i = anInt9690;
	    int i_722_ = anInt9628;
	    int i_723_ = anInt9625;
	    int i_724_ = anInt9626;
	    method3537();
	    int i_725_ = anInt9648;
	    int i_726_ = anInt9632;
	    int i_727_ = anInt9633;
	    int i_728_ = anInt9634;
	    method3281();
	    OpenGL.glReadBuffer(1028);
	    OpenGL.glDrawBuffer(1029);
	    method15203();
	    method15344(false);
	    method15432(false);
	    method15242(false);
	    method15193(false);
	    method15231(null);
	    method15227(-2);
	    method15214(1);
	    method15246(0);
	    OpenGL.glMatrixMode(5889);
	    OpenGL.glLoadIdentity();
	    OpenGL.glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
	    OpenGL.glMatrixMode(5888);
	    OpenGL.glLoadIdentity();
	    OpenGL.glRasterPos2i(0, 0);
	    OpenGL.glCopyPixels(0, 0, aClass175_1989.method2910(),
				aClass175_1989.method2911(), 6144);
	    OpenGL.glFlush();
	    OpenGL.glReadBuffer(1029);
	    OpenGL.glDrawBuffer(1029);
	    method3373(i, i_723_, i_722_, i_724_);
	    method3318(i_725_, i_726_, i_727_, i_728_);
	}
    }
    
    public final int method3239() {
	return anInt9580 + anInt9699 + anInt9669;
    }
    
    final void method15344(boolean bool) {
	if (bool != aBool9654) {
	    aBool9654 = bool;
	    method15212();
	    anInt9591 &= ~0x1f;
	}
    }
    
    public void method3372(int i, int i_729_, int[] is, int[] is_730_) {
	method15185(i, i_729_, is, is_730_);
    }
    
    public void method3566(float f, float f_731_, float f_732_, float[] fs) {
	float f_733_ = (aClass433_9607.aFloatArray4853[15]
			+ aClass433_9607.aFloatArray4853[3] * f
			+ aClass433_9607.aFloatArray4853[7] * f_731_
			+ aClass433_9607.aFloatArray4853[11] * f_732_);
	float f_734_ = (aClass433_9607.aFloatArray4853[12]
			+ aClass433_9607.aFloatArray4853[0] * f
			+ aClass433_9607.aFloatArray4853[4] * f_731_
			+ aClass433_9607.aFloatArray4853[8] * f_732_);
	float f_735_ = (aClass433_9607.aFloatArray4853[13]
			+ aClass433_9607.aFloatArray4853[1] * f
			+ aClass433_9607.aFloatArray4853[5] * f_731_
			+ aClass433_9607.aFloatArray4853[9] * f_732_);
	float f_736_ = (aClass433_9533.aFloatArray4853[14]
			+ aClass433_9533.aFloatArray4853[2] * f
			+ aClass433_9533.aFloatArray4853[6] * f_731_
			+ aClass433_9533.aFloatArray4853[10] * f_732_);
	fs[0] = aFloat9610 + aFloat9663 * f_734_ / f_733_;
	fs[1] = aFloat9612 + aFloat9613 * f_735_ / f_733_;
	fs[2] = f_736_;
    }
    
    void method15345() {
	OpenGL.glLightfv(16384, 4611, aFloatArray9640, 0);
	OpenGL.glLightfv(16385, 4611, aFloatArray9641, 0);
    }
    
    void method15346() {
	OpenGL.glLightfv(16384, 4611, aFloatArray9640, 0);
	OpenGL.glLightfv(16385, 4611, aFloatArray9641, 0);
    }
    
    void method15347() {
	if (aBool9654 && anInt9675 >= 0)
	    OpenGL.glEnable(2912);
	else
	    OpenGL.glDisable(2912);
    }
    
    void method15348() {
	if (aBool9654 && anInt9675 >= 0)
	    OpenGL.glEnable(2912);
	else
	    OpenGL.glDisable(2912);
    }
    
    void method15349() {
	aFloat9679 = aFloat9617 - (float) anInt9657 - aFloat9661;
	aFloat9576 = aFloat9679 - (float) anInt9675 * aFloat9660;
	if (aFloat9576 < aFloat9616)
	    aFloat9576 = aFloat9616;
	OpenGL.glFogf(2915, aFloat9576);
	OpenGL.glFogf(2916, aFloat9679);
	aFloatArray9714[0] = (float) (anInt9687 & 0xff0000) / 1.671168E7F;
	aFloatArray9714[1] = (float) (anInt9687 & 0xff00) / 65280.0F;
	aFloatArray9714[2] = (float) (anInt9687 & 0xff) / 255.0F;
	OpenGL.glFogfv(2918, aFloatArray9714, 0);
    }
    
    void method15350() {
	aFloat9679 = aFloat9617 - (float) anInt9657 - aFloat9661;
	aFloat9576 = aFloat9679 - (float) anInt9675 * aFloat9660;
	if (aFloat9576 < aFloat9616)
	    aFloat9576 = aFloat9616;
	OpenGL.glFogf(2915, aFloat9576);
	OpenGL.glFogf(2916, aFloat9679);
	aFloatArray9714[0] = (float) (anInt9687 & 0xff0000) / 1.671168E7F;
	aFloatArray9714[1] = (float) (anInt9687 & 0xff00) / 65280.0F;
	aFloatArray9714[2] = (float) (anInt9687 & 0xff) / 255.0F;
	OpenGL.glFogfv(2918, aFloatArray9714, 0);
    }
    
    void method15351() {
	aFloat9679 = aFloat9617 - (float) anInt9657 - aFloat9661;
	aFloat9576 = aFloat9679 - (float) anInt9675 * aFloat9660;
	if (aFloat9576 < aFloat9616)
	    aFloat9576 = aFloat9616;
	OpenGL.glFogf(2915, aFloat9576);
	OpenGL.glFogf(2916, aFloat9679);
	aFloatArray9714[0] = (float) (anInt9687 & 0xff0000) / 1.671168E7F;
	aFloatArray9714[1] = (float) (anInt9687 & 0xff00) / 65280.0F;
	aFloatArray9714[2] = (float) (anInt9687 & 0xff) / 255.0F;
	OpenGL.glFogfv(2918, aFloatArray9714, 0);
    }
    
    boolean method15352() {
	if (aClass534_Sub9_Sub3_9565 != null) {
	    if (!aClass534_Sub9_Sub3_9565.method16109()) {
		if (aClass153_9592.method2554(aClass534_Sub9_Sub3_9565))
		    aClass161_9659.method2626();
		else
		    return false;
	    }
	    return true;
	}
	return false;
    }
    
    public void method3456(int i, Class166 class166) {
	if (!aBool9695)
	    throw new RuntimeException("");
	anInt9664 = i;
	aClass166_9665 = class166;
	if (aBool9598) {
	    aClass143_9562.aClass136_Sub4_1653.method14478();
	    aClass143_9562.aClass136_Sub4_1653.method14477();
	}
    }
    
    boolean method15353() {
	if (aClass534_Sub9_Sub3_9565 != null) {
	    if (!aClass534_Sub9_Sub3_9565.method16109()) {
		if (aClass153_9592.method2554(aClass534_Sub9_Sub3_9565))
		    aClass161_9659.method2626();
		else
		    return false;
	    }
	    return true;
	}
	return false;
    }
    
    final void method15354() {
	if (anInt9622 == 2)
	    OpenGL.glDepthRange(aFloat9614, aFloat9615);
	else
	    OpenGL.glDepthRange(0.0F, 1.0F);
    }
    
    void method3580() {
	for (Class534 class534 = aClass700_9577.method14135((byte) -1);
	     class534 != null;
	     class534 = aClass700_9577.method14139(678169150))
	    ((Class534_Sub2_Sub2) class534).method18232();
	if (aClass153_9592 != null)
	    aClass153_9592.method2547();
	if (aBool9575) {
	    Class54.method1212(false, true, -1572468133);
	    aBool9575 = false;
	}
    }
    
    boolean method15355() {
	if (aClass534_Sub9_Sub1_9581 != null) {
	    if (!aClass534_Sub9_Sub1_9581.method16109()) {
		if (aClass153_9592.method2554(aClass534_Sub9_Sub1_9581))
		    aClass161_9659.method2626();
		else
		    return false;
	    }
	    return true;
	}
	return false;
    }
    
    boolean method15356() {
	if (aClass534_Sub9_Sub1_9581 != null) {
	    if (!aClass534_Sub9_Sub1_9581.method16109()) {
		if (aClass153_9592.method2554(aClass534_Sub9_Sub1_9581))
		    aClass161_9659.method2626();
		else
		    return false;
	    }
	    return true;
	}
	return false;
    }
    
    public final boolean method3599() {
	if (aClass534_Sub9_Sub2_9564 != null) {
	    if (!aClass534_Sub9_Sub2_9564.method16109()) {
		if (aClass153_9592.method2554(aClass534_Sub9_Sub2_9564))
		    aClass161_9659.method2626();
		else
		    return false;
	    }
	    return true;
	}
	return false;
    }
    
    final Interface16 method15357(int i, byte[] is, int i_737_, boolean bool) {
	if (aBool9705 && (!bool || aBool9691))
	    return new Class134_Sub2(this, i, is, i_737_, bool);
	return new Class126_Sub2(this, i, is, i_737_);
    }
    
    final void method15358(int i, boolean bool) {
	method15296(i, bool, true);
    }
    
    public void method3549(Class170 class170, float f, Class170 class170_738_,
			   float f_739_, Class170 class170_740_,
			   float f_741_) {
	int i = 0;
	if (class170_740_ == null && f_741_ > 0.0F)
	    f_741_ = 0.0F;
	if (class170_738_ == null && f_739_ > 0.0F) {
	    class170_738_ = class170_740_;
	    class170_740_ = null;
	    f_739_ = f_741_;
	    f_741_ = 0.0F;
	}
	if (class170 == null && f > 0.0F) {
	    class170 = class170_738_;
	    class170_738_ = class170_740_;
	    class170_740_ = null;
	    f = f_739_;
	    f_739_ = f_741_;
	    f_741_ = 0.0F;
	}
	Class534_Sub9_Sub1.aClass170_Sub2Array11561[0]
	    = (Class170_Sub2) class170;
	Class534_Sub9_Sub1.aFloatArray11560[0] = f;
	if (f > 0.0F)
	    i++;
	Class534_Sub9_Sub1.aClass170_Sub2Array11561[1]
	    = (Class170_Sub2) class170_738_;
	Class534_Sub9_Sub1.aFloatArray11560[1] = f_739_;
	if (f_739_ > 0.0F)
	    i++;
	Class534_Sub9_Sub1.aClass170_Sub2Array11561[2]
	    = (Class170_Sub2) class170_740_;
	Class534_Sub9_Sub1.aFloatArray11560[2] = f_741_;
	if (f_741_ > 0.0F)
	    i++;
	Class534_Sub9_Sub1.anInt11555 = i;
	Class534_Sub9_Sub1.aFloat11554 = 1.0F - (f + f_739_ + f_741_);
    }
    
    final Interface15 method15359(int i, Buffer buffer, int i_742_,
				  boolean bool) {
	if (aBool9705 && (!bool || aBool9691))
	    return new Class134_Sub1(this, i, buffer, i_742_, bool);
	return new Class126_Sub1(this, i, buffer);
    }
    
    final synchronized void method15360(int i, int i_743_) {
	Class534_Sub39 class534_sub39 = new Class534_Sub39(i_743_);
	class534_sub39.aLong7158 = (long) i * 936217890172187787L;
	aClass700_9584.method14131(class534_sub39, (short) 789);
    }
    
    final Interface15 method15361(int i, Buffer buffer, int i_744_,
				  boolean bool) {
	if (aBool9705 && (!bool || aBool9691))
	    return new Class134_Sub1(this, i, buffer, i_744_, bool);
	return new Class126_Sub1(this, i, buffer);
    }
    
    public final void method3387(Class433 class433) {
	aClass433_9605.method6842(class433);
	method15207();
	method15206();
    }
    
    final void method15362(Interface15 interface15) {
	if (anInterface15_9593 != interface15) {
	    if (aBool9705)
		OpenGL.glBindBufferARB(34962, interface15.method1());
	    anInterface15_9593 = interface15;
	}
    }
    
    final void method15363(Interface15 interface15) {
	if (anInterface15_9593 != interface15) {
	    if (aBool9705)
		OpenGL.glBindBufferARB(34962, interface15.method1());
	    anInterface15_9593 = interface15;
	}
    }
    
    final void method15364(Class433 class433) {
	OpenGL.glPushMatrix();
	OpenGL.glMultMatrixf(class433.aFloatArray4853, 0);
    }
    
    final void method15365(Interface16 interface16) {
	if (anInterface16_9666 != interface16) {
	    if (aBool9705)
		OpenGL.glBindBufferARB(34963, interface16.method1());
	    anInterface16_9666 = interface16;
	}
    }
    
    final void method15366(Interface16 interface16) {
	if (anInterface16_9666 != interface16) {
	    if (aBool9705)
		OpenGL.glBindBufferARB(34963, interface16.method1());
	    anInterface16_9666 = interface16;
	}
    }
    
    public final void method3508(Class446 class446) {
	aClass446_9602.method7236(class446);
	aClass433_9533.method6916(aClass446_9602);
	aClass446_9618.method7236(class446);
	aClass446_9618.method7243();
	aClass433_9604.method6916(aClass446_9618);
	method15207();
	if (anInt9622 != 1)
	    method15273();
    }
    
    final void method15367(Class129 class129, Class129 class129_745_,
			   Class129 class129_746_, Class129 class129_747_) {
	if (class129 != null) {
	    method15220(class129.anInterface15_1519);
	    OpenGL.glVertexPointer(class129.aByte1517, class129.aShort1516,
				   anInterface15_9593.method93(),
				   (anInterface15_9593.method94()
				    + (long) class129.aByte1518));
	    OpenGL.glEnableClientState(32884);
	} else
	    OpenGL.glDisableClientState(32884);
	if (class129_745_ != null) {
	    method15220(class129_745_.anInterface15_1519);
	    OpenGL.glNormalPointer(class129_745_.aShort1516,
				   anInterface15_9593.method93(),
				   (anInterface15_9593.method94()
				    + (long) class129_745_.aByte1518));
	    OpenGL.glEnableClientState(32885);
	} else
	    OpenGL.glDisableClientState(32885);
	if (class129_746_ != null) {
	    method15220(class129_746_.anInterface15_1519);
	    OpenGL.glColorPointer(class129_746_.aByte1517,
				  class129_746_.aShort1516,
				  anInterface15_9593.method93(),
				  (anInterface15_9593.method94()
				   + (long) class129_746_.aByte1518));
	    OpenGL.glEnableClientState(32886);
	} else
	    OpenGL.glDisableClientState(32886);
	if (class129_747_ != null) {
	    method15220(class129_747_.anInterface15_1519);
	    OpenGL.glTexCoordPointer(class129_747_.aByte1517,
				     class129_747_.aShort1516,
				     anInterface15_9593.method93(),
				     (anInterface15_9593.method94()
				      + (long) class129_747_.aByte1518));
	    OpenGL.glEnableClientState(32888);
	} else
	    OpenGL.glDisableClientState(32888);
    }
    
    final void method15368(Class129 class129, Class129 class129_748_,
			   Class129 class129_749_, Class129 class129_750_) {
	if (class129 != null) {
	    method15220(class129.anInterface15_1519);
	    OpenGL.glVertexPointer(class129.aByte1517, class129.aShort1516,
				   anInterface15_9593.method93(),
				   (anInterface15_9593.method94()
				    + (long) class129.aByte1518));
	    OpenGL.glEnableClientState(32884);
	} else
	    OpenGL.glDisableClientState(32884);
	if (class129_748_ != null) {
	    method15220(class129_748_.anInterface15_1519);
	    OpenGL.glNormalPointer(class129_748_.aShort1516,
				   anInterface15_9593.method93(),
				   (anInterface15_9593.method94()
				    + (long) class129_748_.aByte1518));
	    OpenGL.glEnableClientState(32885);
	} else
	    OpenGL.glDisableClientState(32885);
	if (class129_749_ != null) {
	    method15220(class129_749_.anInterface15_1519);
	    OpenGL.glColorPointer(class129_749_.aByte1517,
				  class129_749_.aShort1516,
				  anInterface15_9593.method93(),
				  (anInterface15_9593.method94()
				   + (long) class129_749_.aByte1518));
	    OpenGL.glEnableClientState(32886);
	} else
	    OpenGL.glDisableClientState(32886);
	if (class129_750_ != null) {
	    method15220(class129_750_.anInterface15_1519);
	    OpenGL.glTexCoordPointer(class129_750_.aByte1517,
				     class129_750_.aShort1516,
				     anInterface15_9593.method93(),
				     (anInterface15_9593.method94()
				      + (long) class129_750_.aByte1518));
	    OpenGL.glEnableClientState(32888);
	} else
	    OpenGL.glDisableClientState(32888);
    }
    
    final void method15369(int i, int i_751_, int i_752_) {
	OpenGL.glDrawArrays(i, i_751_, i_752_);
    }
    
    final void method15370(int i, int i_753_, int i_754_) {
	OpenGL.glDrawArrays(i, i_753_, i_754_);
    }
    
    public boolean method3394() {
	return aBool9688 && (!method3409() || aBool9689);
    }
    
    Class175_Sub2 method3262(Canvas canvas, int i, int i_755_) {
	return new Class175_Sub2_Sub2(this, canvas);
    }
    
    final Interface15 method15371(int i, byte[] is, int i_756_, boolean bool) {
	if (aBool9705 && (!bool || aBool9691))
	    return new Class134_Sub1(this, i, is, i_756_, bool);
	return new Class126_Sub1(this, i, is, i_756_);
    }
    
    final void method15372(Interface16 interface16, int i, int i_757_,
			   int i_758_) {
	method15290(interface16);
	OpenGL.glDrawElements(i, i_758_, 5123,
			      interface16.method2() + (long) (i_757_ * 2));
    }
    
    public Class534_Sub2 method3481(int i) {
	Class534_Sub2_Sub2 class534_sub2_sub2 = new Class534_Sub2_Sub2(i);
	aClass700_9577.method14131(class534_sub2_sub2, (short) 789);
	return class534_sub2_sub2;
    }
    
    final void method15373() {
	if (anInt9591 != 1) {
	    method15204();
	    method15344(false);
	    method15432(false);
	    method15242(false);
	    method15193(false);
	    method15231(null);
	    method15227(-2);
	    method15214(1);
	    method15251((byte) 0);
	    anInt9591 = 1;
	}
    }
    
    public Class163 method3279(Class169 class169, boolean bool) {
	int[] is = class169.method2766(false);
	Class163 class163
	    = method3317(is, 0, class169.method2762(), class169.method2762(),
			 class169.method2763(), (byte) -20);
	class163.method2645(class169.method2764(), class169.method2781(),
			    class169.method2765(), class169.method2767());
	return class163;
    }
    
    final void method15374() {
	if (anInt9591 != 2) {
	    method15204();
	    method15344(false);
	    method15432(false);
	    method15242(false);
	    method15193(false);
	    method15227(-2);
	    method15251((byte) 0);
	    anInt9591 = 2;
	}
    }
    
    final void method15375() {
	if (anInt9591 != 2) {
	    method15204();
	    method15344(false);
	    method15432(false);
	    method15242(false);
	    method15193(false);
	    method15227(-2);
	    method15251((byte) 0);
	    anInt9591 = 2;
	}
    }
    
    void method3248(int i, int i_759_, int i_760_, int i_761_, int i_762_) {
	method15223();
	method15246(i_762_);
	float f = (float) i + 0.35F;
	float f_763_ = (float) i_759_ + 0.35F;
	OpenGL.glColor4ub((byte) (i_761_ >> 16), (byte) (i_761_ >> 8),
			  (byte) i_761_, (byte) (i_761_ >> 24));
	OpenGL.glBegin(1);
	OpenGL.glVertex2f(f, f_763_);
	OpenGL.glVertex2f(f + (float) i_760_, f_763_);
	OpenGL.glEnd();
    }
    
    final void method15376() {
	if (anInt9591 != 4) {
	    method15204();
	    method15344(false);
	    method15432(false);
	    method15242(false);
	    method15193(false);
	    method15227(-2);
	    method15246(1);
	    method15251((byte) 0);
	    anInt9591 = 4;
	}
    }
    
    final void method15377() {
	if (anInt9591 != 4) {
	    method15204();
	    method15344(false);
	    method15432(false);
	    method15242(false);
	    method15193(false);
	    method15227(-2);
	    method15246(1);
	    method15251((byte) 0);
	    anInt9591 = 4;
	}
    }
    
    final void method15378() {
	OpenGL.glPopMatrix();
    }
    
    public void method3438() {
	anInt9648 = 0;
	anInt9632 = 0;
	anInt9633 = aClass175_1989.method2910();
	anInt9634 = aClass175_1989.method2911();
	method15197();
    }
    
    final void method15379(int i) {
	method15228(i, true);
    }
    
    final void method15380(int i, boolean bool) {
	method15296(i, bool, true);
    }
    
    public final synchronized void method3381(int i) {
	int i_764_ = 0;
	i &= 0x7fffffff;
	while (!aClass700_9595.method14142(1271059535)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9595.method14132((byte) -87);
	    anIntArray9590[i_764_++]
		= (int) (class534_sub39.aLong7158 * 8258869577519436579L);
	    anInt9580 -= class534_sub39.anInt10807 * -705967177;
	    if (i_764_ == 1000) {
		OpenGL.glDeleteBuffersARB(i_764_, anIntArray9590, 0);
		i_764_ = 0;
	    }
	}
	if (i_764_ > 0) {
	    OpenGL.glDeleteBuffersARB(i_764_, anIntArray9590, 0);
	    i_764_ = 0;
	}
	while (!aClass700_9584.method14142(98759564)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9584.method14132((byte) -92);
	    anIntArray9590[i_764_++]
		= (int) (class534_sub39.aLong7158 * 8258869577519436579L);
	    anInt9699 -= class534_sub39.anInt10807 * -705967177;
	    if (i_764_ == 1000) {
		OpenGL.glDeleteTextures(i_764_, anIntArray9590, 0);
		i_764_ = 0;
	    }
	}
	if (i_764_ > 0) {
	    OpenGL.glDeleteTextures(i_764_, anIntArray9590, 0);
	    i_764_ = 0;
	}
	while (!aClass700_9585.method14142(-808297784)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9585.method14132((byte) -81);
	    anIntArray9590[i_764_++] = class534_sub39.anInt10807 * -705967177;
	    if (i_764_ == 1000) {
		OpenGL.glDeleteFramebuffersEXT(i_764_, anIntArray9590, 0);
		i_764_ = 0;
	    }
	}
	if (i_764_ > 0) {
	    OpenGL.glDeleteFramebuffersEXT(i_764_, anIntArray9590, 0);
	    i_764_ = 0;
	}
	while (!aClass700_9586.method14142(173940072)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9586.method14132((byte) -89);
	    anIntArray9590[i_764_++]
		= (int) (class534_sub39.aLong7158 * 8258869577519436579L);
	    anInt9669 -= class534_sub39.anInt10807 * -705967177;
	    if (i_764_ == 1000) {
		OpenGL.glDeleteRenderbuffersEXT(i_764_, anIntArray9590, 0);
		i_764_ = 0;
	    }
	}
	if (i_764_ > 0) {
	    OpenGL.glDeleteRenderbuffersEXT(i_764_, anIntArray9590, 0);
	    boolean bool = false;
	}
	while (!aClass700_9653.method14142(-1096952169)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9653.method14132((byte) -120);
	    OpenGL.glDeleteLists((int) (class534_sub39.aLong7158
					* 8258869577519436579L),
				 class534_sub39.anInt10807 * -705967177);
	}
	while (!aClass700_9667.method14142(1728908237)) {
	    Class534 class534 = aClass700_9667.method14132((byte) -56);
	    OpenGL.glDeleteProgramARB((int) (class534.aLong7158
					     * 8258869577519436579L));
	}
	while (!aClass700_9588.method14142(1394114759)) {
	    Class534 class534 = aClass700_9588.method14132((byte) -98);
	    OpenGL.glDeleteShader((int) (class534.aLong7158
					 * 8258869577519436579L));
	}
	while (!aClass700_9653.method14142(-479841068)) {
	    Class534_Sub39 class534_sub39
		= (Class534_Sub39) aClass700_9653.method14132((byte) -88);
	    OpenGL.glDeleteLists((int) (class534_sub39.aLong7158
					* 8258869577519436579L),
				 class534_sub39.anInt10807 * -705967177);
	}
	aClass161_9659.method2630();
	if (method3239() > 100663296
	    && Class250.method4604((byte) -20) > aLong9589 + 60000L) {
	    System.gc();
	    aLong9589 = Class250.method4604((byte) -8);
	}
	anInt9696 = i;
    }
    
    final void method15381(int i, boolean bool) {
	method15296(i, bool, true);
    }
    
    final void method15382(int i, boolean bool, boolean bool_765_) {
	if (i != anInt9649 || aBool9598 != aBool9695) {
	    Class141_Sub2 class141_sub2 = null;
	    byte i_766_ = 0;
	    byte i_767_ = 0;
	    int i_768_ = 0;
	    byte i_769_ = aBool9695 ? (byte) 3 : (byte) 0;
	    byte i_770_ = 0;
	    if (i >= 0) {
		Class186 class186 = aClass177_2012.method2931(i, (byte) 1);
		if (class186.aBool2043) {
		    class141_sub2 = aClass161_9659.method2622(class186);
		    if (class186.aByte2047 != 0 || class186.aByte2048 != 0)
			method15237(((float) (anInt9696 % 128000) / 1000.0F
				     * (float) class186.aByte2047 / 64.0F
				     % 1.0F),
				    ((float) (anInt9696 % 128000) / 1000.0F
				     * (float) class186.aByte2048 / 64.0F
				     % 1.0F),
				    0.0F);
		    else
			method15238();
		    if (!aBool9695) {
			i_767_ = class186.aByte2068;
			i_768_ = class186.anInt2069 * 1880963453;
			i_769_ = class186.aByte2067;
		    }
		    i_766_ = class186.aByte2075;
		} else
		    method15238();
		if (class186.aClass599_2064 == Class599.aClass599_7868)
		    i_770_ = class186.aByte2051;
	    } else
		method15238();
	    method15251(i_770_);
	    aClass143_9562.method2417(i_769_, i_767_, i_768_, bool, bool_765_);
	    if (!aClass143_9562.method2419(class141_sub2, i_766_)) {
		method15231(class141_sub2);
		method15214(i_766_);
	    }
	    aBool9598 = aBool9695;
	    anInt9649 = i;
	}
	anInt9591 &= ~0x7;
    }
    
    void method15383() {
	if (aBool9627 && !aBool9560)
	    OpenGL.glEnable(2896);
	else
	    OpenGL.glDisable(2896);
    }
    
    final void method15384(int i) {
	if (anInt9672 != i) {
	    OpenGL.glActiveTexture(33984 + i);
	    anInt9672 = i;
	}
    }
    
    final void method15385(int i, int i_771_, int i_772_) {
	OpenGL.glDrawArrays(i, i_771_, i_772_);
    }
    
    final void method15386(int i) {
	if (anInt9672 != i) {
	    OpenGL.glActiveTexture(33984 + i);
	    anInt9672 = i;
	}
    }
    
    final void method15387(int i) {
	if (anInt9672 != i) {
	    OpenGL.glActiveTexture(33984 + i);
	    anInt9672 = i;
	}
    }
    
    final void method15388(Class141 class141) {
	Class141 class141_773_ = aClass141Array9519[anInt9672];
	if (class141_773_ != class141) {
	    if (class141 != null) {
		if (class141_773_ != null) {
		    if (class141.anInt1628 != class141_773_.anInt1628) {
			OpenGL.glDisable(class141_773_.anInt1628);
			OpenGL.glEnable(class141.anInt1628);
		    }
		} else
		    OpenGL.glEnable(class141.anInt1628);
		OpenGL.glBindTexture(class141.anInt1628, class141.anInt1633);
	    } else
		OpenGL.glDisable(class141_773_.anInt1628);
	    aClass141Array9519[anInt9672] = class141;
	}
	anInt9591 &= ~0x11;
    }
    
    public boolean method3400() {
	return false;
    }
    
    final void method15389(int i) {
	if (i == 1)
	    method15232(7681, 7681);
	else if (i == 0)
	    method15232(8448, 8448);
	else if (i == 2)
	    method15232(34165, 7681);
	else if (i == 3)
	    method15232(260, 8448);
	else if (i == 4)
	    method15232(34023, 34023);
    }
    
    final void method15390(int i) {
	if (i == 1)
	    method15232(7681, 7681);
	else if (i == 0)
	    method15232(8448, 8448);
	else if (i == 2)
	    method15232(34165, 7681);
	else if (i == 3)
	    method15232(260, 8448);
	else if (i == 4)
	    method15232(34023, 34023);
    }
    
    final void method15391() {
	OpenGL.glLoadIdentity();
	OpenGL.glMultMatrixf(aClass433_9533.aFloatArray4853, 0);
	if (aBool9598)
	    aClass143_9562.aClass136_Sub4_1653.method14478();
	method15184();
	method15421();
    }
    
    final void method15392(int i) {
	if (i == 1)
	    method15232(7681, 7681);
	else if (i == 0)
	    method15232(8448, 8448);
	else if (i == 2)
	    method15232(34165, 7681);
	else if (i == 3)
	    method15232(260, 8448);
	else if (i == 4)
	    method15232(34023, 34023);
    }
    
    public final Class433 method3339() {
	return new Class433(aClass433_9605);
    }
    
    final int method15393(int i) {
	if (i == 1)
	    return 7681;
	if (i == 0)
	    return 8448;
	if (i == 2)
	    return 34165;
	if (i == 3)
	    return 260;
	if (i == 4)
	    return 34023;
	throw new IllegalArgumentException();
    }
    
    final void method15394(int i, boolean bool, boolean bool_774_) {
	if (i != anInt9649 || aBool9598 != aBool9695) {
	    Class141_Sub2 class141_sub2 = null;
	    byte i_775_ = 0;
	    byte i_776_ = 0;
	    int i_777_ = 0;
	    byte i_778_ = aBool9695 ? (byte) 3 : (byte) 0;
	    byte i_779_ = 0;
	    if (i >= 0) {
		Class186 class186 = aClass177_2012.method2931(i, (byte) 1);
		if (class186.aBool2043) {
		    class141_sub2 = aClass161_9659.method2622(class186);
		    if (class186.aByte2047 != 0 || class186.aByte2048 != 0)
			method15237(((float) (anInt9696 % 128000) / 1000.0F
				     * (float) class186.aByte2047 / 64.0F
				     % 1.0F),
				    ((float) (anInt9696 % 128000) / 1000.0F
				     * (float) class186.aByte2048 / 64.0F
				     % 1.0F),
				    0.0F);
		    else
			method15238();
		    if (!aBool9695) {
			i_776_ = class186.aByte2068;
			i_777_ = class186.anInt2069 * 1880963453;
			i_778_ = class186.aByte2067;
		    }
		    i_775_ = class186.aByte2075;
		} else
		    method15238();
		if (class186.aClass599_2064 == Class599.aClass599_7868)
		    i_779_ = class186.aByte2051;
	    } else
		method15238();
	    method15251(i_779_);
	    aClass143_9562.method2417(i_778_, i_776_, i_777_, bool, bool_774_);
	    if (!aClass143_9562.method2419(class141_sub2, i_775_)) {
		method15231(class141_sub2);
		method15214(i_775_);
	    }
	    aBool9598 = aBool9695;
	    anInt9649 = i;
	}
	anInt9591 &= ~0x7;
    }
    
    final void method15395(int i, int i_780_) {
	if (anInt9672 == 0) {
	    boolean bool = false;
	    if (anInt9573 != i) {
		OpenGL.glTexEnvi(8960, 34161, i);
		anInt9573 = i;
		bool = true;
	    }
	    if (anInt9670 != i_780_) {
		OpenGL.glTexEnvi(8960, 34162, i_780_);
		anInt9670 = i_780_;
		bool = true;
	    }
	    if (bool)
		anInt9591 &= ~0x1d;
	} else {
	    OpenGL.glTexEnvi(8960, 34161, i);
	    OpenGL.glTexEnvi(8960, 34162, i_780_);
	}
    }
    
    final void method15396() {
	if (aClass175_1989 != null) {
	    int i;
	    int i_781_;
	    int i_782_;
	    int i_783_;
	    if (anInt9622 == 2) {
		i = anInt9648;
		i_781_ = anInt9632;
		i_782_ = anInt9633;
		i_783_ = anInt9634;
	    } else {
		i = 0;
		i_781_ = 0;
		i_782_ = aClass175_1989.method2910();
		i_783_ = aClass175_1989.method2911();
	    }
	    if (i_782_ < 1)
		i_782_ = 1;
	    if (i_783_ < 1)
		i_783_ = 1;
	    OpenGL.glViewport(anInt9629 + i,
			      (anInt9630 + aClass175_1989.method2911() - i_781_
			       - i_783_),
			      i_782_, i_783_);
	    aFloat9663 = (float) anInt9633 / 2.0F;
	    aFloat9613 = (float) anInt9634 / 2.0F;
	    aFloat9610 = (float) anInt9648 + aFloat9663;
	    aFloat9612 = (float) anInt9632 + aFloat9613;
	}
    }
    
    final void method15397(int i, int i_784_) {
	if (anInt9672 == 0) {
	    boolean bool = false;
	    if (anInt9573 != i) {
		OpenGL.glTexEnvi(8960, 34161, i);
		anInt9573 = i;
		bool = true;
	    }
	    if (anInt9670 != i_784_) {
		OpenGL.glTexEnvi(8960, 34162, i_784_);
		anInt9670 = i_784_;
		bool = true;
	    }
	    if (bool)
		anInt9591 &= ~0x1d;
	} else {
	    OpenGL.glTexEnvi(8960, 34161, i);
	    OpenGL.glTexEnvi(8960, 34162, i_784_);
	}
    }
    
    final void method15398(int i, int i_785_) {
	if (anInt9672 == 0) {
	    boolean bool = false;
	    if (anInt9573 != i) {
		OpenGL.glTexEnvi(8960, 34161, i);
		anInt9573 = i;
		bool = true;
	    }
	    if (anInt9670 != i_785_) {
		OpenGL.glTexEnvi(8960, 34162, i_785_);
		anInt9670 = i_785_;
		bool = true;
	    }
	    if (bool)
		anInt9591 &= ~0x1d;
	} else {
	    OpenGL.glTexEnvi(8960, 34161, i);
	    OpenGL.glTexEnvi(8960, 34162, i_785_);
	}
    }
    
    void method15399(int i, int i_786_) {
	method3280();
	method3616(i, i_786_, -1203594173);
	if (aBool9692) {
	    aClass163_9723 = method3315(i, i_786_, true, true);
	    aClass175_Sub1_9724 = method3263();
	    aClass175_Sub1_9724.method15075(0, aClass163_9723.method2651());
	    OpenGL.glGenBuffersARB(3, anIntArray9725, 0);
	    for (int i_787_ = 0; i_787_ < 3; i_787_++) {
		OpenGL.glBindBufferARB(35051, anIntArray9725[i_787_]);
		OpenGL.glBufferDataARBa(35051, i * i_786_ * 4, 0L, 35041);
		OpenGL.glBindBufferARB(35051, 0);
	    }
	} else {
	    aClass163_9723 = method3315(i, i_786_, true, true);
	    aClass175_Sub1_9724 = method3263();
	    aClass175_Sub1_9724.method15075(0, aClass163_9723.method2651());
	}
    }
    
    final void method15400(int i, int i_788_, int i_789_) {
	OpenGL.glTexEnvi(8960, 34184 + i, i_788_);
	OpenGL.glTexEnvi(8960, 34200 + i, i_789_);
    }
    
    final void method15401(int i) {
	aFloatArray9714[0] = (float) (i & 0xff0000) / 1.671168E7F;
	aFloatArray9714[1] = (float) (i & 0xff00) / 65280.0F;
	aFloatArray9714[2] = (float) (i & 0xff) / 255.0F;
	aFloatArray9714[3] = (float) (i >>> 24) / 255.0F;
	OpenGL.glTexEnvfv(8960, 8705, aFloatArray9714, 0);
    }
    
    final void method15402(int i) {
	aFloatArray9714[0] = (float) (i & 0xff0000) / 1.671168E7F;
	aFloatArray9714[1] = (float) (i & 0xff00) / 65280.0F;
	aFloatArray9714[2] = (float) (i & 0xff) / 255.0F;
	aFloatArray9714[3] = (float) (i >>> 24) / 255.0F;
	OpenGL.glTexEnvfv(8960, 8705, aFloatArray9714, 0);
    }
    
    final void method15403(int i) {
	aFloatArray9714[0] = (float) (i & 0xff0000) / 1.671168E7F;
	aFloatArray9714[1] = (float) (i & 0xff00) / 65280.0F;
	aFloatArray9714[2] = (float) (i & 0xff) / 255.0F;
	aFloatArray9714[3] = (float) (i >>> 24) / 255.0F;
	OpenGL.glTexEnvfv(8960, 8705, aFloatArray9714, 0);
    }
    
    void method3465(int i, int i_790_, int i_791_, int i_792_, int i_793_) {
	if (i_791_ < 0)
	    i_791_ = -i_791_;
	if (i + i_791_ >= anInt9690 && i - i_791_ <= anInt9628
	    && i_790_ + i_791_ >= anInt9625 && i_790_ - i_791_ <= anInt9626) {
	    method15223();
	    method15246(i_793_);
	    OpenGL.glColor4ub((byte) (i_792_ >> 16), (byte) (i_792_ >> 8),
			      (byte) i_792_, (byte) (i_792_ >> 24));
	    float f = (float) i + 0.35F;
	    float f_794_ = (float) i_790_ + 0.35F;
	    int i_795_ = i_791_ << 1;
	    if ((float) i_795_ < aFloat9542) {
		OpenGL.glBegin(7);
		OpenGL.glVertex2f(f + 1.0F, f_794_ + 1.0F);
		OpenGL.glVertex2f(f + 1.0F, f_794_ - 1.0F);
		OpenGL.glVertex2f(f - 1.0F, f_794_ - 1.0F);
		OpenGL.glVertex2f(f - 1.0F, f_794_ + 1.0F);
		OpenGL.glEnd();
	    } else if ((float) i_795_ <= aFloat9706) {
		OpenGL.glEnable(2832);
		OpenGL.glPointSize((float) i_795_);
		OpenGL.glBegin(0);
		OpenGL.glVertex2f(f, f_794_);
		OpenGL.glEnd();
		OpenGL.glDisable(2832);
	    } else {
		OpenGL.glBegin(6);
		OpenGL.glVertex2f(f, f_794_);
		int i_796_ = 262144 / (6 * i_791_);
		if (i_796_ <= 64)
		    i_796_ = 64;
		else if (i_796_ > 512)
		    i_796_ = 512;
		i_796_ = Class455.method7422(i_796_, -1408575131);
		OpenGL.glVertex2f(f + (float) i_791_, f_794_);
		for (int i_797_ = 16384 - i_796_; i_797_ > 0; i_797_ -= i_796_)
		    OpenGL.glVertex2f(f + (Class147.aFloatArray1665[i_797_]
					   * (float) i_791_),
				      f_794_ + (Class147.aFloatArray1664
						[i_797_]) * (float) i_791_);
		OpenGL.glVertex2f(f + (float) i_791_, f_794_);
		OpenGL.glEnd();
	    }
	}
    }
    
    final void method15404(float f, float f_798_, float f_799_) {
	OpenGL.glMatrixMode(5890);
	if (aBool9671)
	    OpenGL.glLoadIdentity();
	OpenGL.glTranslatef(f, f_798_, f_799_);
	OpenGL.glMatrixMode(5888);
	aBool9671 = true;
    }
    
    final void method15405(float f, float f_800_, float f_801_) {
	OpenGL.glMatrixMode(5890);
	if (aBool9671)
	    OpenGL.glLoadIdentity();
	OpenGL.glTranslatef(f, f_800_, f_801_);
	OpenGL.glMatrixMode(5888);
	aBool9671 = true;
    }
    
    final synchronized void method15406(int i, int i_802_) {
	Class534_Sub39 class534_sub39 = new Class534_Sub39(i_802_);
	class534_sub39.aLong7158 = (long) i * 936217890172187787L;
	aClass700_9586.method14131(class534_sub39, (short) 789);
    }
    
    Class175_Sub2 method3244(Canvas canvas, int i, int i_803_) {
	return new Class175_Sub2_Sub2(this, canvas);
    }
    
    public final void method3520(int i) {
	anInt9574 = 0;
	for (/**/; i > 1; i >>= 1)
	    anInt9574++;
	anInt9638 = 1 << anInt9574;
    }
    
    final void method15407(boolean bool) {
	if (bool != aBool9627) {
	    aBool9627 = bool;
	    method15241();
	    anInt9591 &= ~0x7;
	}
    }
    
    public long method3355(int i, int i_804_) {
	return method15185(i, i_804_, null, null);
    }
    
    public int[] method3410(int i, int i_805_, int i_806_, int i_807_) {
	if (aClass175_1989 != null) {
	    int[] is = new int[i_806_ * i_807_];
	    int i_808_ = aClass175_1989.method2911();
	    for (int i_809_ = 0; i_809_ < i_807_; i_809_++)
		OpenGL.glReadPixelsi(i, i_808_ - i_805_ - i_809_ - 1, i_806_,
				     1, 32993, anInt9620, is, i_809_ * i_806_);
	    return is;
	}
	return null;
    }
    
    final void method15408(boolean bool) {
	if (bool != aBool9560) {
	    aBool9560 = bool;
	    method15241();
	}
    }
    
    public final void method3466(int[] is) {
	is[0] = anInt9690;
	is[1] = anInt9625;
	is[2] = anInt9628;
	is[3] = anInt9626;
    }
    
    void method15409() {
	if (aBool9627 && !aBool9560)
	    OpenGL.glEnable(2896);
	else
	    OpenGL.glDisable(2896);
    }
    
    public Class145 method3480(int i, int i_810_, int[] is, int[] is_811_) {
	return Class145_Sub3.method15559(this, i, i_810_, is, is_811_);
    }
    
    final void method15410(boolean bool) {
	if (bool != aBool9600) {
	    aBool9600 = bool;
	    method15244();
	    anInt9591 &= ~0x1f;
	}
    }
    
    final void method15411() {
	OpenGL.glDepthMask(aBool9600 && aBool9623);
    }
    
    final void method15412() {
	OpenGL.glDepthMask(aBool9600 && aBool9623);
    }
    
    final void method15413() {
	if (aBool9599 && aBool9561)
	    OpenGL.glEnable(2929);
	else
	    OpenGL.glDisable(2929);
    }
    
    public final void method3313(int[] is) {
	is[0] = anInt9690;
	is[1] = anInt9625;
	is[2] = anInt9628;
	is[3] = anInt9626;
    }
    
    final void method15414(int i) {
	if (anInt9708 != i) {
	    int i_812_;
	    boolean bool;
	    boolean bool_813_;
	    boolean bool_814_;
	    if (i == 1) {
		i_812_ = 1;
		bool = true;
		bool_813_ = true;
		bool_814_ = true;
	    } else if (i == 2) {
		i_812_ = 2;
		bool = true;
		bool_813_ = false;
		bool_814_ = true;
	    } else if (i == 128) {
		i_812_ = 3;
		bool = true;
		bool_813_ = true;
		bool_814_ = true;
	    } else if (i == 3) {
		i_812_ = 0;
		bool = true;
		bool_813_ = true;
		bool_814_ = false;
	    } else {
		i_812_ = 0;
		bool = true;
		bool_813_ = false;
		bool_814_ = false;
	    }
	    if (bool != aBool9594) {
		OpenGL.glColorMask(bool, bool, bool, true);
		aBool9594 = bool;
	    }
	    if (bool_813_ != aBool9597) {
		aBool9597 = bool_813_;
		method15250();
	    }
	    if (bool_814_ != aBool9553) {
		aBool9553 = bool_814_;
		method15247();
	    }
	    if (i_812_ != anInt9636) {
		anInt9636 = i_812_;
		method15248();
	    }
	    anInt9708 = i;
	    anInt9591 &= ~0x1c;
	}
    }
    
    final void method15415(int i) {
	if (anInt9708 != i) {
	    int i_815_;
	    boolean bool;
	    boolean bool_816_;
	    boolean bool_817_;
	    if (i == 1) {
		i_815_ = 1;
		bool = true;
		bool_816_ = true;
		bool_817_ = true;
	    } else if (i == 2) {
		i_815_ = 2;
		bool = true;
		bool_816_ = false;
		bool_817_ = true;
	    } else if (i == 128) {
		i_815_ = 3;
		bool = true;
		bool_816_ = true;
		bool_817_ = true;
	    } else if (i == 3) {
		i_815_ = 0;
		bool = true;
		bool_816_ = true;
		bool_817_ = false;
	    } else {
		i_815_ = 0;
		bool = true;
		bool_816_ = false;
		bool_817_ = false;
	    }
	    if (bool != aBool9594) {
		OpenGL.glColorMask(bool, bool, bool, true);
		aBool9594 = bool;
	    }
	    if (bool_816_ != aBool9597) {
		aBool9597 = bool_816_;
		method15250();
	    }
	    if (bool_817_ != aBool9553) {
		aBool9553 = bool_817_;
		method15247();
	    }
	    if (i_815_ != anInt9636) {
		anInt9636 = i_815_;
		method15248();
	    }
	    anInt9708 = i;
	    anInt9591 &= ~0x1c;
	}
    }
    
    boolean method15416() {
	if (aClass534_Sub9_Sub1_9581 != null) {
	    if (!aClass534_Sub9_Sub1_9581.method16109()) {
		if (aClass153_9592.method2554(aClass534_Sub9_Sub1_9581))
		    aClass161_9659.method2626();
		else
		    return false;
	    }
	    return true;
	}
	return false;
    }
    
    final void method15417(int i) {
	if (anInt9708 != i) {
	    int i_818_;
	    boolean bool;
	    boolean bool_819_;
	    boolean bool_820_;
	    if (i == 1) {
		i_818_ = 1;
		bool = true;
		bool_819_ = true;
		bool_820_ = true;
	    } else if (i == 2) {
		i_818_ = 2;
		bool = true;
		bool_819_ = false;
		bool_820_ = true;
	    } else if (i == 128) {
		i_818_ = 3;
		bool = true;
		bool_819_ = true;
		bool_820_ = true;
	    } else if (i == 3) {
		i_818_ = 0;
		bool = true;
		bool_819_ = true;
		bool_820_ = false;
	    } else {
		i_818_ = 0;
		bool = true;
		bool_819_ = false;
		bool_820_ = false;
	    }
	    if (bool != aBool9594) {
		OpenGL.glColorMask(bool, bool, bool, true);
		aBool9594 = bool;
	    }
	    if (bool_819_ != aBool9597) {
		aBool9597 = bool_819_;
		method15250();
	    }
	    if (bool_820_ != aBool9553) {
		aBool9553 = bool_820_;
		method15247();
	    }
	    if (i_818_ != anInt9636) {
		anInt9636 = i_818_;
		method15248();
	    }
	    anInt9708 = i;
	    anInt9591 &= ~0x1c;
	}
    }
    
    final void method15418() {
	if (aBool9553)
	    OpenGL.glEnable(3042);
	else
	    OpenGL.glDisable(3042);
    }
    
    final void method15419() {
	if (aBool9553)
	    OpenGL.glEnable(3042);
	else
	    OpenGL.glDisable(3042);
    }
    
    final void method15420() {
	if (aBool9704) {
	    int i = 0;
	    int i_821_ = 0;
	    if (anInt9596 == 0) {
		i = 0;
		i_821_ = 0;
	    } else if (anInt9596 == 1) {
		i = 1;
		i_821_ = 0;
	    } else if (anInt9596 == 2) {
		i = 1;
		i_821_ = 1;
	    } else if (anInt9596 == 3) {
		i = 0;
		i_821_ = 1;
	    }
	    if (anInt9636 == 1)
		OpenGL.glBlendFuncSeparate(770, 771, i, i_821_);
	    else if (anInt9636 == 2)
		OpenGL.glBlendFuncSeparate(1, 1, i, i_821_);
	    else if (anInt9636 == 3)
		OpenGL.glBlendFuncSeparate(774, 1, i, i_821_);
	    else if (anInt9636 == 0)
		OpenGL.glBlendFuncSeparate(1, 0, i, i_821_);
	} else if (anInt9636 == 1) {
	    OpenGL.glEnable(3042);
	    OpenGL.glBlendFunc(770, 771);
	} else if (anInt9636 == 2) {
	    OpenGL.glEnable(3042);
	    OpenGL.glBlendFunc(1, 1);
	} else if (anInt9636 == 3) {
	    OpenGL.glEnable(3042);
	    OpenGL.glBlendFunc(774, 1);
	} else
	    OpenGL.glDisable(3042);
    }
    
    void method15421() {
	int i;
	for (i = 0; i < anInt9651; i++) {
	    Class534_Sub21 class534_sub21 = aClass534_Sub21Array9702[i];
	    int i_822_ = 16386 + i;
	    aFloatArray9715[0]
		= (float) class534_sub21.method16197(-452294101);
	    aFloatArray9715[1]
		= (float) class534_sub21.method16226(-1780570162);
	    aFloatArray9715[2]
		= (float) class534_sub21.method16199((byte) -51);
	    aFloatArray9715[3] = 1.0F;
	    OpenGL.glLightfv(i_822_, 4611, aFloatArray9715, 0);
	    int i_823_ = class534_sub21.method16201((byte) -12);
	    float f = class534_sub21.method16202(-693604944) / 255.0F;
	    aFloatArray9715[0] = (float) (i_823_ >> 16 & 0xff) * f;
	    aFloatArray9715[1] = (float) (i_823_ >> 8 & 0xff) * f;
	    aFloatArray9715[2] = (float) (i_823_ & 0xff) * f;
	    OpenGL.glLightfv(i_822_, 4609, aFloatArray9715, 0);
	    OpenGL.glLightf(i_822_, 4617,
			    (1.0F
			     / (float) (class534_sub21.method16233(-2097794812)
					* class534_sub21
					      .method16233(-868761879))));
	    OpenGL.glEnable(i_822_);
	}
	for (/**/; i < anInt9601; i++)
	    OpenGL.glDisable(16386 + i);
	anInt9601 = anInt9651;
    }
    
    void method15422() {
	int i = aClass175_1989.method2910();
	int i_824_ = aClass175_1989.method2911();
	aClass433_9606.method6853(0.0F, (float) i, 0.0F, (float) i_824_, -1.0F,
				  1.0F);
	method3281();
	method15203();
	method3537();
    }
    
    final void method15423() {
	if (aBool9704) {
	    int i = 0;
	    int i_825_ = 0;
	    if (anInt9596 == 0) {
		i = 0;
		i_825_ = 0;
	    } else if (anInt9596 == 1) {
		i = 1;
		i_825_ = 0;
	    } else if (anInt9596 == 2) {
		i = 1;
		i_825_ = 1;
	    } else if (anInt9596 == 3) {
		i = 0;
		i_825_ = 1;
	    }
	    if (anInt9636 == 1)
		OpenGL.glBlendFuncSeparate(770, 771, i, i_825_);
	    else if (anInt9636 == 2)
		OpenGL.glBlendFuncSeparate(1, 1, i, i_825_);
	    else if (anInt9636 == 3)
		OpenGL.glBlendFuncSeparate(774, 1, i, i_825_);
	    else if (anInt9636 == 0)
		OpenGL.glBlendFuncSeparate(1, 0, i, i_825_);
	} else if (anInt9636 == 1) {
	    OpenGL.glEnable(3042);
	    OpenGL.glBlendFunc(770, 771);
	} else if (anInt9636 == 2) {
	    OpenGL.glEnable(3042);
	    OpenGL.glBlendFunc(1, 1);
	} else if (anInt9636 == 3) {
	    OpenGL.glEnable(3042);
	    OpenGL.glBlendFunc(774, 1);
	} else
	    OpenGL.glDisable(3042);
    }
    
    public Class171 method3325(Class16 class16, Class169[] class169s,
			       boolean bool) {
	return new Class171_Sub3(this, class16, class169s, bool);
    }
    
    final void method15424() {
	if (aBool9597)
	    OpenGL.glEnable(3008);
	else
	    OpenGL.glDisable(3008);
	OpenGL.glAlphaFunc(516, (float) (aByte9673 & 0xff) / 255.0F);
	if (anInt9668 > 0) {
	    if (aByte9673 == 0)
		OpenGL.glDisable(32926);
	    else
		OpenGL.glEnable(32926);
	}
    }
    
    public void method3280() {
	if (aBool9692) {
	    aClass163_9722 = null;
	    if (aClass175_Sub1_9724 != null) {
		aClass175_Sub1_9724.method142();
		aClass175_Sub1_9724 = null;
	    }
	    OpenGL.glDeleteBuffersARB(3, anIntArray9725, 0);
	    for (int i = 0; i < 3; i++) {
		anIntArray9725[i] = 0;
		if (aLongArray9726[i] != 0L) {
		    OpenGL.glDeleteSync(aLongArray9726[i]);
		    aLongArray9726[i] = 0L;
		}
	    }
	} else {
	    aClass175_Sub1_9724 = null;
	    aClass163_9723 = null;
	    aClass163_9722 = null;
	}
    }
    
    final void method15425() {
	if (aBool9597)
	    OpenGL.glEnable(3008);
	else
	    OpenGL.glDisable(3008);
	OpenGL.glAlphaFunc(516, (float) (aByte9673 & 0xff) / 255.0F);
	if (anInt9668 > 0) {
	    if (aByte9673 == 0)
		OpenGL.glDisable(32926);
	    else
		OpenGL.glEnable(32926);
	}
    }
    
    public Interface21 method3624(int i, int i_826_) {
	return new Class534_Sub18_Sub1(this, Class181.aClass181_1956,
				       Class173.aClass173_1828, i, i_826_);
    }
    
    final synchronized void method15426(int i, int i_827_) {
	Class534_Sub39 class534_sub39 = new Class534_Sub39(i_827_);
	class534_sub39.aLong7158 = (long) i * 936217890172187787L;
	aClass700_9584.method14131(class534_sub39, (short) 789);
    }
    
    public final void method3301(int i, int i_828_, int i_829_) {
	if (anInt9687 != i || anInt9675 != i_828_ || anInt9657 != i_829_) {
	    anInt9687 = i;
	    anInt9675 = i_828_;
	    anInt9657 = i_829_;
	    method15305();
	    method15212();
	}
    }
    
    void method15427() {
	aFloatArray9714[0] = aFloat9686 * aFloat9643;
	aFloatArray9714[1] = aFloat9686 * aFloat9644;
	aFloatArray9714[2] = aFloat9686 * aFloat9645;
	aFloatArray9714[3] = 1.0F;
	OpenGL.glLightfv(16384, 4609, aFloatArray9714, 0);
	aFloatArray9714[0] = -aFloat9587 * aFloat9643;
	aFloatArray9714[1] = -aFloat9587 * aFloat9644;
	aFloatArray9714[2] = -aFloat9587 * aFloat9645;
	aFloatArray9714[3] = 1.0F;
	OpenGL.glLightfv(16385, 4609, aFloatArray9714, 0);
    }
    
    public boolean method3417() {
	return aBool9692;
    }
    
    final void method15428() {
	if (anInt9622 != 1) {
	    anInt9622 = 1;
	    method15206();
	    method15197();
	    method15354();
	    OpenGL.glMatrixMode(5888);
	    OpenGL.glLoadIdentity();
	    anInt9591 &= ~0x18;
	}
    }
    
    final synchronized void method15429(int i, int i_830_) {
	Class534_Sub39 class534_sub39 = new Class534_Sub39(i_830_);
	class534_sub39.aLong7158 = (long) i * 936217890172187787L;
	aClass700_9586.method14131(class534_sub39, (short) 789);
    }
    
    void method3376(int i, int i_831_) throws Exception_Sub7 {
	try {
	    aClass175_Sub2_2013.method15475();
	} catch (Exception exception) {
	    /* empty */
	}
	if (anInterface25_1997 != null)
	    anInterface25_1997.method33(-1183923057);
    }
    
    final synchronized void method15430(long l) {
	Class534 class534 = new Class534();
	class534.aLong7158 = l * 936217890172187787L;
	aClass700_9588.method14131(class534, (short) 789);
    }
    
    final synchronized void method15431(long l) {
	Class534 class534 = new Class534();
	class534.aLong7158 = l * 936217890172187787L;
	aClass700_9588.method14131(class534, (short) 789);
    }
    
    final void method15432(boolean bool) {
	if (bool != aBool9627) {
	    aBool9627 = bool;
	    method15241();
	    anInt9591 &= ~0x7;
	}
    }
    
    final synchronized void method15433(int i) {
	Class534 class534 = new Class534();
	class534.aLong7158 = (long) i * 936217890172187787L;
	aClass700_9667.method14131(class534, (short) 789);
    }
    
    void method15434(int i) {
	anInt9658 = i;
	if (anInt9658 == 1)
	    OpenGL.glDisable(2884);
	else {
	    OpenGL.glEnable(2884);
	    if (anInt9658 == 2)
		OpenGL.glCullFace(1029);
	    else if (anInt9658 == 3)
		OpenGL.glCullFace(1028);
	}
    }
    
    static int method15435(Class181 class181) {
	switch (class181.anInt1953 * -939549997) {
	case 4:
	    return 6407;
	case 8:
	    return 6406;
	case 1:
	    return 6410;
	case 0:
	    return 6408;
	case 3:
	    return 6402;
	default:
	    throw new IllegalStateException();
	case 6:
	    return 6409;
	}
    }
    
    public boolean method3628() {
	return true;
    }
    
    static int method15436(Class181 class181, Class173 class173) {
	if (class173 == Class173.aClass173_1830) {
	    switch (class181.anInt1953 * -939549997) {
	    default:
		throw new IllegalArgumentException();
	    case 0:
		return 6408;
	    case 4:
		return 6407;
	    case 1:
		return 6410;
	    case 6:
		return 6409;
	    case 8:
		return 6406;
	    }
	}
	if (class173 == Class173.aClass173_1829) {
	    switch (class181.anInt1953 * -939549997) {
	    case 3:
		return 33189;
	    case 8:
		return 32830;
	    case 1:
		return 36219;
	    case 0:
		return 32859;
	    case 4:
		return 32852;
	    case 6:
		return 32834;
	    default:
		throw new IllegalArgumentException();
	    }
	}
	if (class173 == Class173.aClass173_1828) {
	    switch (class181.anInt1953 * -939549997) {
	    default:
		throw new IllegalArgumentException();
	    case 3:
		return 33190;
	    }
	}
	if (class173 == Class173.aClass173_1832) {
	    switch (class181.anInt1953 * -939549997) {
	    case 0:
		return 34842;
	    case 4:
		return 34843;
	    case 8:
		return 34844;
	    default:
		throw new IllegalArgumentException();
	    case 6:
		return 34846;
	    case 1:
		return 34847;
	    }
	}
	if (class173 == Class173.aClass173_1826) {
	    switch (class181.anInt1953 * -939549997) {
	    case 1:
		return 34841;
	    default:
		throw new IllegalArgumentException();
	    case 6:
		return 34840;
	    case 4:
		return 34837;
	    case 0:
		return 34836;
	    case 8:
		return 34838;
	    }
	}
	throw new IllegalArgumentException();
    }
    
    public boolean method3611() {
	return true;
    }
    
    public int[] method3663() {
	int[] is = new int[1];
	OpenGL.glGetIntegerv(34466, is, 0);
	int i = is[0];
	if (i == 0)
	    return null;
	int[] is_832_ = new int[i];
	OpenGL.glGetIntegerv(34467, is_832_, 0);
	return is_832_;
    }
    
    public Class446 method3257() {
	return new Class446(aClass446_9602);
    }
    
    public boolean method3286() {
	return (aClass534_Sub9_Sub1_9581 != null
		&& aClass534_Sub9_Sub1_9581.method16109());
    }
    
    public boolean method3682() {
	return (aClass534_Sub9_Sub1_9581 != null
		&& aClass534_Sub9_Sub1_9581.method16109());
    }
}
