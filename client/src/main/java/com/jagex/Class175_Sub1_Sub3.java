/* Class175_Sub1_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class175_Sub1_Sub3 extends Class175_Sub1
{
    int anInt11409;
    Class130 aClass130_11410;
    Class115 aClass115_11411;
    int anInt11412 = 0;
    Class185_Sub2 aClass185_Sub2_11413;
    
    public int method2910() {
	return -706875759 * anInt11412;
    }
    
    Class175_Sub1_Sub3(Class185_Sub2 class185_sub2) {
	anInt11409 = 0;
	aClass185_Sub2_11413 = class185_sub2;
    }
    
    public int method2911() {
	return -200708425 * anInt11409;
    }
    
    public void method15075(int i, Interface22 interface22) {
	if (i != 0)
	    throw new RuntimeException();
	Class115 class115 = (Class115) interface22;
	if (aClass130_11410 != null && class115 != null
	    && ((1306524683 * aClass130_11410.anInt1521
		 != 351836835 * class115.anInt1399)
		|| (-357499107 * class115.anInt1398
		    != aClass130_11410.anInt1522 * 339609175)))
	    throw new RuntimeException();
	aClass115_11411 = class115;
	if (class115 != null) {
	    anInt11412 = -1758578701 * class115.anInt1399;
	    anInt11409 = class115.anInt1398 * 1183280331;
	} else if (null == aClass130_11410) {
	    anInt11412 = 0;
	    anInt11409 = 0;
	}
	if (this == aClass185_Sub2_11413.method3253(-1912205789))
	    method358();
    }
    
    public int method2914() {
	return -200708425 * anInt11409;
    }
    
    public boolean method15076() {
	return true;
    }
    
    public void method361() {
	/* empty */
    }
    
    public void method15077(int i, int i_0_, int i_1_, int i_2_, int i_3_,
			    int i_4_, boolean bool, boolean bool_5_) {
	int[] is = null;
	int[] is_6_ = null;
	float[] fs = null;
	float[] fs_7_ = null;
	if (bool && null != aClass115_11411) {
	    is = aClass115_11411.anIntArray1400;
	    is_6_ = aClass185_Sub2_11413.anIntArray9338;
	}
	if (bool_5_ && aClass130_11410 != null) {
	    fs = aClass130_11410.aFloatArray1520;
	    fs_7_ = aClass185_Sub2_11413.aFloatArray9341;
	}
	Class690_Sub9.method16964(anInt11412 * -706875759,
				  356365251 * aClass185_Sub2_11413.anInt9356,
				  is, is_6_, fs, fs_7_, i, i_0_, i_3_, i_4_,
				  i_1_, i_2_, 59416246);
    }
    
    boolean method2912() {
	return true;
    }
    
    public void method15074(Interface21 interface21) {
	Class130 class130 = (Class130) interface21;
	if (null != aClass115_11411 && class130 != null
	    && ((aClass115_11411.anInt1399 * 351836835
		 != 1306524683 * class130.anInt1521)
		|| (-357499107 * aClass115_11411.anInt1398
		    != class130.anInt1522 * 339609175)))
	    throw new RuntimeException();
	aClass130_11410 = class130;
	if (class130 != null) {
	    anInt11412 = class130.anInt1521 * 2018871003;
	    anInt11409 = class130.anInt1522 * -1257601695;
	} else if (aClass115_11411 == null) {
	    anInt11412 = 0;
	    anInt11409 = 0;
	}
	if (this == aClass185_Sub2_11413.method3253(-1134497428))
	    method358();
    }
    
    public void method142() {
	/* empty */
    }
    
    public int method2913() {
	return -706875759 * anInt11412;
    }
    
    public int method2915() {
	return -200708425 * anInt11409;
    }
    
    public void method15078(int i, Interface22 interface22) {
	if (i != 0)
	    throw new RuntimeException();
	Class115 class115 = (Class115) interface22;
	if (aClass130_11410 != null && class115 != null
	    && ((1306524683 * aClass130_11410.anInt1521
		 != 351836835 * class115.anInt1399)
		|| (-357499107 * class115.anInt1398
		    != aClass130_11410.anInt1522 * 339609175)))
	    throw new RuntimeException();
	aClass115_11411 = class115;
	if (class115 != null) {
	    anInt11412 = -1758578701 * class115.anInt1399;
	    anInt11409 = class115.anInt1398 * 1183280331;
	} else if (null == aClass130_11410) {
	    anInt11412 = 0;
	    anInt11409 = 0;
	}
	if (this == aClass185_Sub2_11413.method3253(-1845410338))
	    method358();
    }
    
    public void method15079(int i, Interface22 interface22) {
	if (i != 0)
	    throw new RuntimeException();
	Class115 class115 = (Class115) interface22;
	if (aClass130_11410 != null && class115 != null
	    && ((1306524683 * aClass130_11410.anInt1521
		 != 351836835 * class115.anInt1399)
		|| (-357499107 * class115.anInt1398
		    != aClass130_11410.anInt1522 * 339609175)))
	    throw new RuntimeException();
	aClass115_11411 = class115;
	if (class115 != null) {
	    anInt11412 = -1758578701 * class115.anInt1399;
	    anInt11409 = class115.anInt1398 * 1183280331;
	} else if (null == aClass130_11410) {
	    anInt11412 = 0;
	    anInt11409 = 0;
	}
	if (this == aClass185_Sub2_11413.method3253(-1025206006))
	    method358();
    }
    
    public void method15080(int i, Interface22 interface22) {
	if (i != 0)
	    throw new RuntimeException();
	Class115 class115 = (Class115) interface22;
	if (aClass130_11410 != null && class115 != null
	    && ((1306524683 * aClass130_11410.anInt1521
		 != 351836835 * class115.anInt1399)
		|| (-357499107 * class115.anInt1398
		    != aClass130_11410.anInt1522 * 339609175)))
	    throw new RuntimeException();
	aClass115_11411 = class115;
	if (class115 != null) {
	    anInt11412 = -1758578701 * class115.anInt1399;
	    anInt11409 = class115.anInt1398 * 1183280331;
	} else if (null == aClass130_11410) {
	    anInt11412 = 0;
	    anInt11409 = 0;
	}
	if (this == aClass185_Sub2_11413.method3253(-1615385140))
	    method358();
    }
    
    public void method15081(Interface21 interface21) {
	Class130 class130 = (Class130) interface21;
	if (null != aClass115_11411 && class130 != null
	    && ((aClass115_11411.anInt1399 * 351836835
		 != 1306524683 * class130.anInt1521)
		|| (-357499107 * aClass115_11411.anInt1398
		    != class130.anInt1522 * 339609175)))
	    throw new RuntimeException();
	aClass130_11410 = class130;
	if (class130 != null) {
	    anInt11412 = class130.anInt1521 * 2018871003;
	    anInt11409 = class130.anInt1522 * -1257601695;
	} else if (aClass115_11411 == null) {
	    anInt11412 = 0;
	    anInt11409 = 0;
	}
	if (this == aClass185_Sub2_11413.method3253(-1366167355))
	    method358();
    }
    
    public void method15073(Interface21 interface21) {
	Class130 class130 = (Class130) interface21;
	if (null != aClass115_11411 && class130 != null
	    && ((aClass115_11411.anInt1399 * 351836835
		 != 1306524683 * class130.anInt1521)
		|| (-357499107 * aClass115_11411.anInt1398
		    != class130.anInt1522 * 339609175)))
	    throw new RuntimeException();
	aClass130_11410 = class130;
	if (class130 != null) {
	    anInt11412 = class130.anInt1521 * 2018871003;
	    anInt11409 = class130.anInt1522 * -1257601695;
	} else if (aClass115_11411 == null) {
	    anInt11412 = 0;
	    anInt11409 = 0;
	}
	if (this == aClass185_Sub2_11413.method3253(-1397671547))
	    method358();
    }
    
    public void method15082(Interface21 interface21) {
	Class130 class130 = (Class130) interface21;
	if (null != aClass115_11411 && class130 != null
	    && ((aClass115_11411.anInt1399 * 351836835
		 != 1306524683 * class130.anInt1521)
		|| (-357499107 * aClass115_11411.anInt1398
		    != class130.anInt1522 * 339609175)))
	    throw new RuntimeException();
	aClass130_11410 = class130;
	if (class130 != null) {
	    anInt11412 = class130.anInt1521 * 2018871003;
	    anInt11409 = class130.anInt1522 * -1257601695;
	} else if (aClass115_11411 == null) {
	    anInt11412 = 0;
	    anInt11409 = 0;
	}
	if (this == aClass185_Sub2_11413.method3253(-1458143874))
	    method358();
    }
    
    public void method15083(Interface21 interface21) {
	Class130 class130 = (Class130) interface21;
	if (null != aClass115_11411 && class130 != null
	    && ((aClass115_11411.anInt1399 * 351836835
		 != 1306524683 * class130.anInt1521)
		|| (-357499107 * aClass115_11411.anInt1398
		    != class130.anInt1522 * 339609175)))
	    throw new RuntimeException();
	aClass130_11410 = class130;
	if (class130 != null) {
	    anInt11412 = class130.anInt1521 * 2018871003;
	    anInt11409 = class130.anInt1522 * -1257601695;
	} else if (aClass115_11411 == null) {
	    anInt11412 = 0;
	    anInt11409 = 0;
	}
	if (this == aClass185_Sub2_11413.method3253(-1478065704))
	    method358();
    }
    
    public boolean method15084() {
	return true;
    }
    
    boolean method358() {
	aClass185_Sub2_11413.method15029(anInt11412 * -706875759,
					 -200708425 * anInt11409,
					 (aClass115_11411 == null ? null
					  : aClass115_11411.anIntArray1400),
					 (null == aClass130_11410 ? null
					  : aClass130_11410.aFloatArray1520));
	return true;
    }
    
    static void method17947(int i, int i_8_, int[] is, int[] is_9_, float[] fs,
			    float[] fs_10_, int i_11_, int i_12_, int i_13_,
			    int i_14_, int i_15_, int i_16_) {
	int i_17_ = i_12_ * i + i_11_;
	int i_18_ = i_13_ + i_14_ * i_8_;
	int i_19_ = i - i_15_;
	int i_20_ = i_8_ - i_15_;
	if (is == null) {
	    for (int i_21_ = 0; i_21_ < i_16_; i_21_++) {
		int i_22_ = i_17_ + i_15_;
		while (i_17_ < i_22_)
		    fs_10_[i_18_++] = fs[i_17_++];
		i_17_ += i_19_;
		i_18_ += i_20_;
	    }
	} else if (fs == null) {
	    for (int i_23_ = 0; i_23_ < i_16_; i_23_++) {
		int i_24_ = i_17_ + i_15_;
		while (i_17_ < i_24_)
		    is_9_[i_18_++] = is[i_17_++];
		i_17_ += i_19_;
		i_18_ += i_20_;
	    }
	} else {
	    for (int i_25_ = 0; i_25_ < i_16_; i_25_++) {
		int i_26_ = i_17_ + i_15_;
		while (i_17_ < i_26_) {
		    is_9_[i_18_] = is[i_17_];
		    fs_10_[i_18_++] = fs[i_17_++];
		}
		i_17_ += i_19_;
		i_18_ += i_20_;
	    }
	}
    }
    
    boolean method206() {
	aClass185_Sub2_11413.method15029(anInt11412 * -706875759,
					 -200708425 * anInt11409,
					 (aClass115_11411 == null ? null
					  : aClass115_11411.anIntArray1400),
					 (null == aClass130_11410 ? null
					  : aClass130_11410.aFloatArray1520));
	return true;
    }
    
    boolean method360() {
	aClass185_Sub2_11413.method15029(anInt11412 * -706875759,
					 -200708425 * anInt11409,
					 (aClass115_11411 == null ? null
					  : aClass115_11411.anIntArray1400),
					 (null == aClass130_11410 ? null
					  : aClass130_11410.aFloatArray1520));
	return true;
    }
    
    boolean method357() {
	aClass185_Sub2_11413.method15029(anInt11412 * -706875759,
					 -200708425 * anInt11409,
					 (aClass115_11411 == null ? null
					  : aClass115_11411.anIntArray1400),
					 (null == aClass130_11410 ? null
					  : aClass130_11410.aFloatArray1520));
	return true;
    }
    
    boolean method2916() {
	return true;
    }
    
    boolean method2917() {
	return true;
    }
    
    public boolean method15085() {
	return true;
    }
    
    boolean method2918() {
	return true;
    }
    
    boolean method207() {
	aClass185_Sub2_11413.method15029(anInt11412 * -706875759,
					 -200708425 * anInt11409,
					 (aClass115_11411 == null ? null
					  : aClass115_11411.anIntArray1400),
					 (null == aClass130_11410 ? null
					  : aClass130_11410.aFloatArray1520));
	return true;
    }
    
    public void method362() {
	/* empty */
    }
    
    public void method364() {
	/* empty */
    }
    
    public void method15086(int i, int i_27_, int i_28_, int i_29_, int i_30_,
			    int i_31_, boolean bool, boolean bool_32_) {
	int[] is = null;
	int[] is_33_ = null;
	float[] fs = null;
	float[] fs_34_ = null;
	if (bool && null != aClass115_11411) {
	    is = aClass115_11411.anIntArray1400;
	    is_33_ = aClass185_Sub2_11413.anIntArray9338;
	}
	if (bool_32_ && aClass130_11410 != null) {
	    fs = aClass130_11410.aFloatArray1520;
	    fs_34_ = aClass185_Sub2_11413.aFloatArray9341;
	}
	Class690_Sub9.method16964(anInt11412 * -706875759,
				  356365251 * aClass185_Sub2_11413.anInt9356,
				  is, is_33_, fs, fs_34_, i, i_27_, i_30_,
				  i_31_, i_28_, i_29_, 529284718);
    }
    
    public void method15087(int i, int i_35_, int i_36_, int i_37_, int i_38_,
			    int i_39_, boolean bool, boolean bool_40_) {
	int[] is = null;
	int[] is_41_ = null;
	float[] fs = null;
	float[] fs_42_ = null;
	if (bool && null != aClass115_11411) {
	    is = aClass115_11411.anIntArray1400;
	    is_41_ = aClass185_Sub2_11413.anIntArray9338;
	}
	if (bool_40_ && aClass130_11410 != null) {
	    fs = aClass130_11410.aFloatArray1520;
	    fs_42_ = aClass185_Sub2_11413.aFloatArray9341;
	}
	Class690_Sub9.method16964(anInt11412 * -706875759,
				  356365251 * aClass185_Sub2_11413.anInt9356,
				  is, is_41_, fs, fs_42_, i, i_35_, i_38_,
				  i_39_, i_36_, i_37_, -46790235);
    }
}
