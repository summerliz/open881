/* Class222 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

class Class222 implements Interface42
{
    Class232 this$0;
    public static Class44_Sub9 aClass44_Sub9_2313;
    
    Class222(Class232 class232) {
	this$0 = class232;
    }
    
    public float method330(short i) {
	return ((float) Class44_Sub6.aClass534_Sub35_10989
			    .aClass690_Sub28_10788.method17131(-1059994694)
		/ 255.0F);
    }
    
    public float method331() {
	return ((float) Class44_Sub6.aClass534_Sub35_10989
			    .aClass690_Sub28_10788.method17131(2115725716)
		/ 255.0F);
    }
    
    static final void method4150(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = (1423663301
	       * Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419.anInt12210);
    }
    
    static final void method4151(Class669 class669, int i) {
	int i_0_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = RuntimeException_Sub4.aClass56_12127.method1218
		  (i_0_, -1581069180).method18354((byte) 77);
    }
    
    static final void method4152(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = client.aBool11015 ? 1 : 0;
    }
    
    static final void method4153(Class669 class669, int i) {
	int i_1_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_1_, 202743137);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_1_ >> 16];
	Class175.method2921(class247, class243, class669, (byte) 54);
    }
    
    static void method4154(int i, int i_2_, int i_3_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(15, (long) i);
	class534_sub18_sub6.method18121(-322703804);
	class534_sub18_sub6.anInt11666 = i_2_ * 517206857;
    }
    
    static void method4155(int i, boolean bool, int i_4_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(22, (long) i);
	class534_sub18_sub6.method18121(129931733);
	class534_sub18_sub6.anInt11666 = (bool ? 1 : 0) * 517206857;
    }
}
