/* Class690_Sub5 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class690_Sub5 extends Class690
{
    public static final int anInt10862 = 2;
    public static final int anInt10863 = 1;
    public static final int anInt10864 = 0;
    
    void method14020(int i, int i_0_) {
	anInt8753 = 1823770475 * i;
	Class590.method9876(189295939 * anInt8753, 213050033);
    }
    
    public int method14029(int i) {
	if (aClass534_Sub35_8752.method16442(-531493946).method14115((byte) 8)
	    < 245)
	    return 3;
	return 1;
    }
    
    public void method16911(int i) {
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14115((byte) 106)
	    < 245)
	    anInt8753 = 0;
	if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 2)
	    anInt8753 = method14017(2136886561) * 1823770475;
    }
    
    int method14017(int i) {
	if (aClass534_Sub35_8752.method16442(-531493946).method14115((byte) 93)
	    < 245)
	    return 0;
	return 2;
    }
    
    public boolean method16912(short i) {
	if (aClass534_Sub35_8752.method16442(-531493946).method14115((byte) 22)
	    < 245)
	    return false;
	return true;
    }
    
    public int method14026(int i, int i_1_) {
	if (aClass534_Sub35_8752.method16442(-531493946).method14115((byte) 85)
	    < 245)
	    return 3;
	return 1;
    }
    
    public Class690_Sub5(Class534_Sub35 class534_sub35) {
	super(class534_sub35);
	Class590.method9876(189295939 * anInt8753, -2058904358);
    }
    
    void method14025(int i) {
	anInt8753 = 1823770475 * i;
	Class590.method9876(189295939 * anInt8753, -816436865);
    }
    
    int method14021() {
	if (aClass534_Sub35_8752.method16442(-531493946).method14115((byte) 67)
	    < 245)
	    return 0;
	return 2;
    }
    
    int method14022() {
	if (aClass534_Sub35_8752.method16442(-531493946).method14115((byte) 86)
	    < 245)
	    return 0;
	return 2;
    }
    
    int method14018() {
	if (aClass534_Sub35_8752.method16442(-531493946).method14115((byte) 19)
	    < 245)
	    return 0;
	return 2;
    }
    
    void method14024(int i) {
	anInt8753 = 1823770475 * i;
	Class590.method9876(189295939 * anInt8753, 149554676);
    }
    
    public int method16913(byte i) {
	return anInt8753 * 189295939;
    }
    
    void method14023(int i) {
	anInt8753 = 1823770475 * i;
	Class590.method9876(189295939 * anInt8753, 279224671);
    }
    
    public int method14027(int i) {
	if (aClass534_Sub35_8752.method16442(-531493946).method14115((byte) 93)
	    < 245)
	    return 3;
	return 1;
    }
    
    public int method14028(int i) {
	if (aClass534_Sub35_8752.method16442(-531493946).method14115((byte) 35)
	    < 245)
	    return 3;
	return 1;
    }
    
    public int method14030(int i) {
	if (aClass534_Sub35_8752.method16442(-531493946).method14115((byte) 42)
	    < 245)
	    return 3;
	return 1;
    }
    
    public void method16914() {
	if (aClass534_Sub35_8752.method16442(-531493946).method14115((byte) 14)
	    < 245)
	    anInt8753 = 0;
	if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 2)
	    anInt8753 = method14017(2126605518) * 1823770475;
    }
    
    public void method16915() {
	if (aClass534_Sub35_8752.method16442(-531493946).method14115((byte) 67)
	    < 245)
	    anInt8753 = 0;
	if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 2)
	    anInt8753 = method14017(2101403189) * 1823770475;
    }
    
    public Class690_Sub5(int i, Class534_Sub35 class534_sub35) {
	super(i, class534_sub35);
	Class590.method9876(anInt8753 * 189295939, 132007285);
    }
    
    public int method16916() {
	return anInt8753 * 189295939;
    }
    
    public int method16917() {
	return anInt8753 * 189295939;
    }
}
