/* Class654_Sub1_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public abstract class Class654_Sub1_Sub3 extends Class654_Sub1
{
    protected short aShort11867;
    int anInt11868 = 0;
    protected short aShort11869;
    Class534_Sub21[] aClass534_Sub21Array11870 = new Class534_Sub21[4];
    
    boolean method16885() {
	Class438 class438 = method10807().aClass438_4885;
	return (aClass556_10855.aBoolArrayArray7481
		[(-1213435377 * aClass556_10855.anInt7461
		  + (((int) class438.aFloat4864
		      >> 941710601 * aClass556_10855.anInt7422)
		     - aClass556_10855.anInt7455 * -380604831))]
		[(((int) class438.aFloat4865
		   >> 941710601 * aClass556_10855.anInt7422)
		  - aClass556_10855.anInt7459 * -1709472547
		  + aClass556_10855.anInt7461 * -1213435377)]);
    }
    
    int method16888(Class534_Sub21[] class534_sub21s) {
	if (aBool8514) {
	    Class438 class438 = method10807().aClass438_4885;
	    anInt11868
		= (method16860(((int) class438.aFloat4864
				>> aClass556_10855.anInt7422 * 941710601),
			       ((int) class438.aFloat4865
				>> aClass556_10855.anInt7422 * 941710601),
			       aClass534_Sub21Array11870, -509022242)
		   * -152576849);
	    aBool8514 = false;
	}
	for (int i = 0; i < 1242626639 * anInt11868; i++)
	    class534_sub21s[i] = aClass534_Sub21Array11870[i];
	return 1242626639 * anInt11868;
    }
    
    boolean method16857(Class185 class185, int i) {
	Class438 class438 = method10807().aClass438_4885;
	return (aClass556_10855.aClass552_7427.method9064
		(aByte10853,
		 ((int) class438.aFloat4864
		  >> 941710601 * aClass556_10855.anInt7422),
		 ((int) class438.aFloat4865
		  >> 941710601 * aClass556_10855.anInt7422),
		 method16876(-1147619823)));
    }
    
    int method16856(Class534_Sub21[] class534_sub21s, int i) {
	if (aBool8514) {
	    Class438 class438 = method10807().aClass438_4885;
	    anInt11868
		= (method16860(((int) class438.aFloat4864
				>> aClass556_10855.anInt7422 * 941710601),
			       ((int) class438.aFloat4865
				>> aClass556_10855.anInt7422 * 941710601),
			       aClass534_Sub21Array11870, -1463437719)
		   * -152576849);
	    aBool8514 = false;
	}
	for (int i_0_ = 0; i_0_ < 1242626639 * anInt11868; i_0_++)
	    class534_sub21s[i_0_] = aClass534_Sub21Array11870[i_0_];
	return 1242626639 * anInt11868;
    }
    
    final void method16877(Class185 class185, Class654_Sub1 class654_sub1,
			   int i, int i_1_, int i_2_, boolean bool) {
	throw new IllegalStateException();
    }
    
    final void method16851(Class185 class185, Class654_Sub1 class654_sub1,
			   int i, int i_3_, int i_4_, boolean bool, int i_5_) {
	throw new IllegalStateException();
    }
    
    final void method16852(int i) {
	throw new IllegalStateException();
    }
    
    final boolean method16861() {
	return false;
    }
    
    final boolean method16848(byte i) {
	return false;
    }
    
    final void method16883(Class185 class185, Class654_Sub1 class654_sub1,
			   int i, int i_6_, int i_7_, boolean bool) {
	throw new IllegalStateException();
    }
    
    final void method16845() {
	throw new IllegalStateException();
    }
    
    final void method16865() {
	throw new IllegalStateException();
    }
    
    final void method16881() {
	throw new IllegalStateException();
    }
    
    Class654_Sub1_Sub3(Class556 class556, int i, int i_8_, int i_9_, int i_10_,
		       int i_11_, int i_12_, int i_13_) {
	super(class556);
	aByte10854 = (byte) i_10_;
	aByte10853 = (byte) i_11_;
	aShort11869 = (short) i_12_;
	aShort11867 = (short) i_13_;
	method10809(new Class438((float) i, (float) i_8_, (float) i_9_));
	for (int i_14_ = 0; i_14_ < 4; i_14_++)
	    aClass534_Sub21Array11870[i_14_] = null;
    }
    
    boolean method16858(byte i) {
	Class438 class438 = method10807().aClass438_4885;
	return (aClass556_10855.aBoolArrayArray7481
		[(-1213435377 * aClass556_10855.anInt7461
		  + (((int) class438.aFloat4864
		      >> 941710601 * aClass556_10855.anInt7422)
		     - aClass556_10855.anInt7455 * -380604831))]
		[(((int) class438.aFloat4865
		   >> 941710601 * aClass556_10855.anInt7422)
		  - aClass556_10855.anInt7459 * -1709472547
		  + aClass556_10855.anInt7461 * -1213435377)]);
    }
    
    boolean method16890(Class185 class185) {
	Class438 class438 = method10807().aClass438_4885;
	return (aClass556_10855.aClass552_7427.method9064
		(aByte10853,
		 ((int) class438.aFloat4864
		  >> 941710601 * aClass556_10855.anInt7422),
		 ((int) class438.aFloat4865
		  >> 941710601 * aClass556_10855.anInt7422),
		 method16876(-764500510)));
    }
    
    boolean method16891(Class185 class185) {
	Class438 class438 = method10807().aClass438_4885;
	return (aClass556_10855.aClass552_7427.method9064
		(aByte10853,
		 ((int) class438.aFloat4864
		  >> 941710601 * aClass556_10855.anInt7422),
		 ((int) class438.aFloat4865
		  >> 941710601 * aClass556_10855.anInt7422),
		 method16876(-1962200510)));
    }
    
    boolean method16889(Class185 class185) {
	Class438 class438 = method10807().aClass438_4885;
	return (aClass556_10855.aClass552_7427.method9064
		(aByte10853,
		 ((int) class438.aFloat4864
		  >> 941710601 * aClass556_10855.anInt7422),
		 ((int) class438.aFloat4865
		  >> 941710601 * aClass556_10855.anInt7422),
		 method16876(-1664634569)));
    }
    
    boolean method16893() {
	Class438 class438 = method10807().aClass438_4885;
	return (aClass556_10855.aBoolArrayArray7481
		[(-1213435377 * aClass556_10855.anInt7461
		  + (((int) class438.aFloat4864
		      >> 941710601 * aClass556_10855.anInt7422)
		     - aClass556_10855.anInt7455 * -380604831))]
		[(((int) class438.aFloat4865
		   >> 941710601 * aClass556_10855.anInt7422)
		  - aClass556_10855.anInt7459 * -1709472547
		  + aClass556_10855.anInt7461 * -1213435377)]);
    }
}
