/* Class621 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class621
{
    int anInt8114;
    int anInt8115;
    int anInt8116;
    int anInt8117;
    int anInt8118;
    int anInt8119;
    int anInt8120;
    int anInt8121;
    int anInt8122;
    int anInt8123;
    int anInt8124;
    int anInt8125;
    static int anInt8126;
    
    Class621() {
	/* empty */
    }
    
    boolean method10276(Class621 class621_0_) {
	if (class621_0_.anInt8123 * -1481682457 == anInt8123 * -1481682457
	    && 379767745 * class621_0_.anInt8125 == 379767745 * anInt8125
	    && class621_0_.anInt8121 * 702655677 == anInt8121 * 702655677)
	    return true;
	return false;
    }
    
    boolean method10277(Class621 class621_1_, byte i) {
	if (class621_1_.anInt8123 * -1481682457 == anInt8123 * -1481682457
	    && 379767745 * class621_1_.anInt8125 == 379767745 * anInt8125
	    && class621_1_.anInt8121 * 702655677 == anInt8121 * 702655677)
	    return true;
	return false;
    }
    
    boolean method10278(Class621 class621_2_) {
	if (class621_2_.anInt8123 * -1481682457 == anInt8123 * -1481682457
	    && 379767745 * class621_2_.anInt8125 == 379767745 * anInt8125
	    && class621_2_.anInt8121 * 702655677 == anInt8121 * 702655677)
	    return true;
	return false;
    }
    
    static final void method10279(Class669 class669, byte i) {
	int i_3_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_3_, -71067672);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_3_ >> 16];
	Class277.method5161(class247, class243, class669, -919297823);
    }
    
    static final void method10280(Class669 class669, short i) {
	Class28.method865(-1103002300);
    }
    
    static final void method10281(Class669 class669, int i) {
	Class265.method4856(969278507);
	Class72.aBool784 = false;
    }
    
    static final void method10282
	(Class534_Sub37 class534_sub37, int i, int i_4_, int i_5_, int i_6_,
	 int i_7_, int i_8_, int i_9_, int i_10_, int i_11_, byte i_12_) {
	if (Class180.method2978(i, null, 681759147)) {
	    if (class534_sub37 != null
		&& !class534_sub37.method16499((byte) 109))
		Class534_Sub41.method16766(class534_sub37, true, false,
					   -501970604);
	    else {
		Class243 class243 = Class44_Sub11.aClass243Array11006[i];
		client.method17391(class243, class243.method4473((byte) 0), -1,
				   i_4_, i_5_, i_6_, i_7_, i_8_, i_9_, i_10_,
				   i_11_);
	    }
	}
    }
    
    static final void method10283(Class669 class669, int i) {
	int i_13_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class613.method10111((class669.aClass534_Sub26_8606
			      == Class574.aClass534_Sub26_7710),
			     i_13_, 131580982);
    }
}
