/* Class654_Sub1_Sub5_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class654_Sub1_Sub5_Sub3 extends Class654_Sub1_Sub5
    implements Interface62
{
    boolean aBool12037;
    public Class528 aClass528_12038;
    Class564 aClass564_12039;
    boolean aBool12040 = true;
    boolean aBool12041;
    
    final void method16852(int i) {
	throw new IllegalStateException();
    }
    
    boolean method16895() {
	return false;
    }
    
    boolean method16850(int i) {
	return aBool12040;
    }
    
    public int method16876(int i) {
	return aClass528_12038.method8805((byte) 20);
    }
    
    static byte method18646(int i, int i_0_) {
	if (i != Class595.aClass595_7833.anInt7852 * 847393323)
	    return (byte) 0;
	if (0 == (i_0_ & 0x1))
	    return (byte) 1;
	return (byte) 2;
    }
    
    public int method16897(int i) {
	return aClass528_12038.method8789((byte) 1);
    }
    
    boolean method16849(int i) {
	return false;
    }
    
    public int method254() {
	return aClass528_12038.anInt7103 * 1626333597;
    }
    
    public void method18647(Class596 class596, byte i) {
	aClass528_12038.method8783(class596, (byte) -127);
    }
    
    boolean method16846(Class185 class185, int i, int i_1_, byte i_2_) {
	Class602 class602 = aClass528_12038.method8786(1460193483);
	if (null != class602.aClass432_7956)
	    return class185.method3309(i, i_1_, method10834(),
				       class602.aClass432_7956, 1554530302);
	Class183 class183 = aClass528_12038.method8787(class185, 131072, false,
						       false, 1405470617);
	if (class183 == null)
	    return false;
	return class183.method3039(i, i_1_, method10834(), false, 0);
    }
    
    public Class564 method16872(Class185 class185) {
	return aClass564_12039;
    }
    
    final void method16851(Class185 class185, Class654_Sub1 class654_sub1,
			   int i, int i_3_, int i_4_, boolean bool, int i_5_) {
	throw new IllegalStateException();
    }
    
    public Class654_Sub1_Sub5_Sub3
	(Class556 class556, Class185 class185, Class44_Sub13 class44_sub13,
	 Class602 class602, int i, int i_6_, int i_7_, int i_8_, int i_9_,
	 boolean bool, int i_10_, int i_11_, int i_12_, int i_13_, int i_14_,
	 int i_15_, int i_16_, int i_17_, boolean bool_18_) {
	super(class556, i, i_6_, i_7_, i_8_, i_9_, i_10_, i_11_, i_12_, i_13_,
	      class602.anInt7940 * 1288889595 == 1,
	      Class150_Sub1.method14441(i_14_, i_15_, (byte) -14));
	aClass528_12038
	    = new Class528(class185, class44_sub13, class602, i_14_, i_15_,
			   i_6_, this, bool, i_16_, i_17_);
	aBool12037 = class602.anInt7907 * -2134171963 != 0 && !bool;
	aBool12041 = bool_18_;
	method16862(1, -354675075);
    }
    
    final void method16883(Class185 class185, Class654_Sub1 class654_sub1,
			   int i, int i_19_, int i_20_, boolean bool) {
	throw new IllegalStateException();
    }
    
    public int method56(int i) {
	return aClass528_12038.anInt7103 * 1626333597;
    }
    
    boolean method16879() {
	return aBool12040;
    }
    
    public void method411(int i) {
	/* empty */
    }
    
    public boolean method413(byte i) {
	return aBool12041;
    }
    
    public boolean method419(byte i) {
	return aClass528_12038.method8809((byte) 77);
    }
    
    public void method408(Class185 class185, byte i) {
	aClass528_12038.method8790(class185, -1934676099);
    }
    
    public void method414(Class185 class185, int i) {
	aClass528_12038.method8791(class185, 198005613);
    }
    
    public int method9() {
	return -1932952217 * aClass528_12038.anInt7104;
    }
    
    public int method145() {
	return aClass528_12038.anInt7111 * -2129482149;
    }
    
    public int method181() {
	return aClass528_12038.anInt7111 * -2129482149;
    }
    
    public void method141() {
	/* empty */
    }
    
    public int method252() {
	return aClass528_12038.anInt7103 * 1626333597;
    }
    
    void method16871(Class185 class185) {
	Class183 class183 = aClass528_12038.method8787(class185, 262144, true,
						       true, 1874948169);
	if (null != class183)
	    aClass528_12038.method8788(class185, class183, method10834(),
				       aShort11900, aShort11896, aShort11901,
				       aShort11898, false, (byte) -30);
    }
    
    public void method417(Class185 class185) {
	aClass528_12038.method8790(class185, -1829375871);
    }
    
    public void method422(Class185 class185) {
	aClass528_12038.method8790(class185, -940510442);
    }
    
    public void method418(Class185 class185) {
	aClass528_12038.method8790(class185, -1329253107);
    }
    
    public void method421(Class185 class185) {
	aClass528_12038.method8791(class185, 198005613);
    }
    
    public void method420(Class185 class185) {
	aClass528_12038.method8791(class185, 198005613);
    }
    
    public boolean method260() {
	return aBool12041;
    }
    
    boolean method16864() {
	return false;
    }
    
    public boolean method416() {
	return aClass528_12038.method8809((byte) 77);
    }
    
    boolean method16869() {
	return aBool12040;
    }
    
    static byte method18648(int i, int i_21_) {
	if (i != Class595.aClass595_7833.anInt7852 * 847393323)
	    return (byte) 0;
	if (0 == (i_21_ & 0x1))
	    return (byte) 1;
	return (byte) 2;
    }
    
    static byte method18649(int i, int i_22_) {
	if (i != Class595.aClass595_7833.anInt7852 * 847393323)
	    return (byte) 0;
	if (0 == (i_22_ & 0x1))
	    return (byte) 1;
	return (byte) 2;
    }
    
    public void method18650(Class596 class596) {
	aClass528_12038.method8783(class596, (byte) -48);
    }
    
    public int method410(int i) {
	return aClass528_12038.anInt7111 * -2129482149;
    }
    
    public boolean method423() {
	return aBool12041;
    }
    
    public int method16866() {
	return aClass528_12038.method8805((byte) 101);
    }
    
    public void method144() {
	/* empty */
    }
    
    public Class564 method16870(Class185 class185) {
	return aClass564_12039;
    }
    
    Class550 method16853(Class185 class185, int i) {
	Class183 class183 = aClass528_12038.method8787(class185, 2048, false,
						       true, 1356165030);
	if (class183 == null)
	    return null;
	Class446 class446 = method10834();
	Class550 class550 = Class322.method5779(aBool12037, -132655830);
	aClass528_12038.method8788(class185, class183, class446, aShort11900,
				   aShort11896, aShort11901, aShort11898, true,
				   (byte) -46);
	Class602 class602 = aClass528_12038.method8786(1460193483);
	if (class602.aClass432_7956 != null) {
	    class183.method3034(class446, null, 0);
	    class185.method3311(class446, aClass194Array10852[0],
				class602.aClass432_7956);
	} else
	    class183.method3034(class446, aClass194Array10852[0], 0);
	if (null != aClass528_12038.aClass629_7105) {
	    Class174 class174 = aClass528_12038.aClass629_7105.method10405();
	    class185.method3334(class174);
	}
	aBool12040
	    = class183.method3027() || aClass528_12038.aClass629_7105 != null;
	Class444 class444 = method10807();
	if (null == aClass564_12039)
	    aClass564_12039
		= Class528.method8820((int) class444.aClass438_4885.aFloat4864,
				      (int) class444.aClass438_4885.aFloat4863,
				      (int) class444.aClass438_4885.aFloat4865,
				      class183, (byte) 11);
	else
	    Class274.method5144(aClass564_12039,
				(int) class444.aClass438_4885.aFloat4864,
				(int) class444.aClass438_4885.aFloat4863,
				(int) class444.aClass438_4885.aFloat4865,
				class183, 1076655535);
	return class550;
    }
    
    final boolean method16861() {
	return false;
    }
    
    void method16868(Class185 class185, int i) {
	Class183 class183 = aClass528_12038.method8787(class185, 262144, true,
						       true, 1410336662);
	if (null != class183)
	    aClass528_12038.method8788(class185, class183, method10834(),
				       aShort11900, aShort11896, aShort11901,
				       aShort11898, false, (byte) -84);
    }
    
    boolean method16880(Class185 class185, int i, int i_23_) {
	Class602 class602 = aClass528_12038.method8786(1460193483);
	if (null != class602.aClass432_7956)
	    return class185.method3309(i, i_23_, method10834(),
				       class602.aClass432_7956, 1554530302);
	Class183 class183 = aClass528_12038.method8787(class185, 131072, false,
						       false, 1991961059);
	if (class183 == null)
	    return false;
	return class183.method3039(i, i_23_, method10834(), false, 0);
    }
    
    public Class564 method16855(Class185 class185, short i) {
	return aClass564_12039;
    }
    
    boolean method16874(Class185 class185, int i, int i_24_) {
	Class602 class602 = aClass528_12038.method8786(1460193483);
	if (null != class602.aClass432_7956)
	    return class185.method3309(i, i_24_, method10834(),
				       class602.aClass432_7956, 1554530302);
	Class183 class183 = aClass528_12038.method8787(class185, 131072, false,
						       false, 1318487056);
	if (class183 == null)
	    return false;
	return class183.method3039(i, i_24_, method10834(), false, 0);
    }
    
    boolean method16882(Class185 class185, int i, int i_25_) {
	Class602 class602 = aClass528_12038.method8786(1460193483);
	if (null != class602.aClass432_7956)
	    return class185.method3309(i, i_25_, method10834(),
				       class602.aClass432_7956, 1554530302);
	Class183 class183 = aClass528_12038.method8787(class185, 131072, false,
						       false, 2092930706);
	if (class183 == null)
	    return false;
	return class183.method3039(i, i_25_, method10834(), false, 0);
    }
    
    public int method409(int i) {
	return -1932952217 * aClass528_12038.anInt7104;
    }
    
    final void method16877(Class185 class185, Class654_Sub1 class654_sub1,
			   int i, int i_26_, int i_27_, boolean bool) {
	throw new IllegalStateException();
    }
    
    boolean method16873(Class185 class185, int i, int i_28_) {
	Class602 class602 = aClass528_12038.method8786(1460193483);
	if (null != class602.aClass432_7956)
	    return class185.method3309(i, i_28_, method10834(),
				       class602.aClass432_7956, 1554530302);
	Class183 class183 = aClass528_12038.method8787(class185, 131072, false,
						       false, 1717307276);
	if (class183 == null)
	    return false;
	return class183.method3039(i, i_28_, method10834(), false, 0);
    }
    
    final void method16845() {
	throw new IllegalStateException();
    }
    
    final void method16865() {
	throw new IllegalStateException();
    }
    
    final void method16881() {
	throw new IllegalStateException();
    }
    
    final boolean method16848(byte i) {
	return false;
    }
    
    public int method253() {
	return aClass528_12038.anInt7103 * 1626333597;
    }
    
    public int method16867() {
	return aClass528_12038.method8805((byte) 93);
    }
    
    public boolean method412() {
	return aBool12041;
    }
    
    public boolean method415() {
	return aBool12041;
    }
    
    static byte method18651(int i, int i_29_) {
	if (i != Class595.aClass595_7833.anInt7852 * 847393323)
	    return (byte) 0;
	if (0 == (i_29_ & 0x1))
	    return (byte) 1;
	return (byte) 2;
    }
    
    public void method18652(Class596 class596) {
	aClass528_12038.method8783(class596, (byte) -59);
    }
    
    public int method16854() {
	return aClass528_12038.method8789((byte) 1);
    }
    
    public int method16875() {
	return aClass528_12038.method8789((byte) 1);
    }
    
    Class550 method16884(Class185 class185) {
	Class183 class183 = aClass528_12038.method8787(class185, 2048, false,
						       true, 2075120598);
	if (class183 == null)
	    return null;
	Class446 class446 = method10834();
	Class550 class550 = Class322.method5779(aBool12037, 691798688);
	aClass528_12038.method8788(class185, class183, class446, aShort11900,
				   aShort11896, aShort11901, aShort11898, true,
				   (byte) -60);
	Class602 class602 = aClass528_12038.method8786(1460193483);
	if (class602.aClass432_7956 != null) {
	    class183.method3034(class446, null, 0);
	    class185.method3311(class446, aClass194Array10852[0],
				class602.aClass432_7956);
	} else
	    class183.method3034(class446, aClass194Array10852[0], 0);
	if (null != aClass528_12038.aClass629_7105) {
	    Class174 class174 = aClass528_12038.aClass629_7105.method10405();
	    class185.method3334(class174);
	}
	aBool12040
	    = class183.method3027() || aClass528_12038.aClass629_7105 != null;
	Class444 class444 = method10807();
	if (null == aClass564_12039)
	    aClass564_12039
		= Class528.method8820((int) class444.aClass438_4885.aFloat4864,
				      (int) class444.aClass438_4885.aFloat4863,
				      (int) class444.aClass438_4885.aFloat4865,
				      class183, (byte) 52);
	else
	    Class274.method5144(aClass564_12039,
				(int) class444.aClass438_4885.aFloat4864,
				(int) class444.aClass438_4885.aFloat4863,
				(int) class444.aClass438_4885.aFloat4865,
				class183, -1361279585);
	return class550;
    }
}
