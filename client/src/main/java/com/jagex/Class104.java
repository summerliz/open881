/* Class104 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.awt.Canvas;

public class Class104
{
    static Class185 aClass185_1297;
    static Class700 aClass700_1298 = new Class700();
    
    static void method1921() {
	if (null != aClass185_1297) {
	    aClass185_1297.method3236(-568376843);
	    aClass185_1297 = null;
	    Class662.aClass171_8551 = null;
	}
    }
    
    Class104() throws Throwable {
	throw new Error();
    }
    
    static void method1922(Class185 class185, Class247 class247) {
	boolean bool
	    = ((Class531.aClass44_Sub7_7135.method17306
		(class185, class247.anInt2606 * 403396513,
		 class247.anInt2602 * -148110895,
		 973326593 * class247.anInt2490,
		 ~0xffffff | class247.anInt2587 * 2071487835,
		 class247.anInt2513 * 1245616333,
		 (class247.aBool2610
		  ? Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419.aClass631_12226
		  : null),
		 -878424575))
	       == null);
	if (bool) {
	    aClass700_1298.method14131
		(new Class534_Sub8(class247.anInt2606 * 403396513,
				   class247.anInt2602 * -148110895,
				   class247.anInt2490 * 973326593,
				   ~0xffffff | class247.anInt2587 * 2071487835,
				   class247.anInt2513 * 1245616333,
				   class247.aBool2610),
		 (short) 789);
	    Class454.method7416(class247, -1083544954);
	}
    }
    
    static void method1923(Class185 class185, Class247 class247) {
	boolean bool
	    = ((Class531.aClass44_Sub7_7135.method17306
		(class185, class247.anInt2606 * 403396513,
		 class247.anInt2602 * -148110895,
		 973326593 * class247.anInt2490,
		 ~0xffffff | class247.anInt2587 * 2071487835,
		 class247.anInt2513 * 1245616333,
		 (class247.aBool2610
		  ? Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419.aClass631_12226
		  : null),
		 -878424575))
	       == null);
	if (bool) {
	    aClass700_1298.method14131
		(new Class534_Sub8(class247.anInt2606 * 403396513,
				   class247.anInt2602 * -148110895,
				   class247.anInt2490 * 973326593,
				   ~0xffffff | class247.anInt2587 * 2071487835,
				   class247.anInt2513 * 1245616333,
				   class247.aBool2610),
		 (short) 789);
	    Class454.method7416(class247, -545072566);
	}
    }
    
    static void method1924(Class185 class185, Class247 class247) {
	boolean bool
	    = ((Class531.aClass44_Sub7_7135.method17306
		(class185, class247.anInt2606 * 403396513,
		 class247.anInt2602 * -148110895,
		 973326593 * class247.anInt2490,
		 ~0xffffff | class247.anInt2587 * 2071487835,
		 class247.anInt2513 * 1245616333,
		 (class247.aBool2610
		  ? Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419.aClass631_12226
		  : null),
		 -878424575))
	       == null);
	if (bool) {
	    aClass700_1298.method14131
		(new Class534_Sub8(class247.anInt2606 * 403396513,
				   class247.anInt2602 * -148110895,
				   class247.anInt2490 * 973326593,
				   ~0xffffff | class247.anInt2587 * 2071487835,
				   class247.anInt2513 * 1245616333,
				   class247.aBool2610),
		 (short) 789);
	    Class454.method7416(class247, -338392557);
	}
    }
    
    static void method1925(Class185 class185, Class247 class247) {
	boolean bool
	    = ((Class531.aClass44_Sub7_7135.method17306
		(class185, class247.anInt2606 * 403396513,
		 class247.anInt2602 * -148110895,
		 973326593 * class247.anInt2490,
		 ~0xffffff | class247.anInt2587 * 2071487835,
		 class247.anInt2513 * 1245616333,
		 (class247.aBool2610
		  ? Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419.aClass631_12226
		  : null),
		 -878424575))
	       == null);
	if (bool) {
	    aClass700_1298.method14131
		(new Class534_Sub8(class247.anInt2606 * 403396513,
				   class247.anInt2602 * -148110895,
				   class247.anInt2490 * 973326593,
				   ~0xffffff | class247.anInt2587 * 2071487835,
				   class247.anInt2513 * 1245616333,
				   class247.aBool2610),
		 (short) 789);
	    Class454.method7416(class247, -1543780909);
	}
    }
    
    static void method1926(Class185 class185) {
	if (aClass700_1298.method14141((byte) 6) != 0) {
	    if (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub7_10733
		    .method16935(-1807368365)
		== 0) {
		for (Class534_Sub8 class534_sub8
			 = ((Class534_Sub8)
			    aClass700_1298.method14135((byte) -1));
		     class534_sub8 != null;
		     class534_sub8
			 = ((Class534_Sub8)
			    aClass700_1298.method14139(1067855245))) {
		    Class531.aClass44_Sub7_7135.method17307
			(class185, class185,
			 class534_sub8.anInt10420 * -2069726317,
			 -863343329 * class534_sub8.anInt10419,
			 1038549693 * class534_sub8.anInt10423,
			 304308761 * class534_sub8.anInt10422, false, false,
			 class534_sub8.anInt10421 * 1740682347,
			 Class219.aClass171_2307,
			 (class534_sub8.aBool10424
			  ? (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
			     .aClass631_12226)
			  : null),
			 Class620.aClass632_8113, -2074326341);
		    class534_sub8.method8892((byte) 1);
		}
		Class422.method6785((byte) -90);
	    } else {
		if (null == aClass185_1297) {
		    Canvas canvas = new Canvas();
		    canvas.setSize(36, 32);
		    aClass185_1297
			= Class321.method5777(0, canvas,
					      Class656.aClass177_8524,
					      Class534.anInterface25_7160,
					      Class334.aClass402_3513,
					      Class269.aClass396_2956,
					      Class48.aClass387_363,
					      Class295.aClass472_3161, 0,
					      2138160553);
		    Class662.aClass171_8551
			= (aClass185_1297.method3325
			   (Class711.method14414(Class606.aClass472_7988,
						 (Class67.anInt715
						  * -1643399711),
						 0, 1243174761),
			    Class178.method2939(Class464.aClass472_5113,
						Class67.anInt715 * -1643399711,
						0),
			    true));
		}
		for (Class534_Sub8 class534_sub8
			 = ((Class534_Sub8)
			    aClass700_1298.method14135((byte) -1));
		     class534_sub8 != null;
		     class534_sub8
			 = ((Class534_Sub8)
			    aClass700_1298.method14139(1424480839))) {
		    Class531.aClass44_Sub7_7135.method17307
			(aClass185_1297, class185,
			 -2069726317 * class534_sub8.anInt10420,
			 class534_sub8.anInt10419 * -863343329,
			 1038549693 * class534_sub8.anInt10423,
			 304308761 * class534_sub8.anInt10422, false, false,
			 1740682347 * class534_sub8.anInt10421,
			 Class662.aClass171_8551,
			 (class534_sub8.aBool10424
			  ? (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
			     .aClass631_12226)
			  : null),
			 Class620.aClass632_8113, -2062820747);
		    class534_sub8.method8892((byte) 1);
		}
	    }
	}
    }
    
    static void method1927(Class185 class185) {
	if (aClass700_1298.method14141((byte) 6) != 0) {
	    if (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub7_10733
		    .method16935(-1807368365)
		== 0) {
		for (Class534_Sub8 class534_sub8
			 = ((Class534_Sub8)
			    aClass700_1298.method14135((byte) -1));
		     class534_sub8 != null;
		     class534_sub8 = ((Class534_Sub8)
				      aClass700_1298.method14139(942030570))) {
		    Class531.aClass44_Sub7_7135.method17307
			(class185, class185,
			 class534_sub8.anInt10420 * -2069726317,
			 -863343329 * class534_sub8.anInt10419,
			 1038549693 * class534_sub8.anInt10423,
			 304308761 * class534_sub8.anInt10422, false, false,
			 class534_sub8.anInt10421 * 1740682347,
			 Class219.aClass171_2307,
			 (class534_sub8.aBool10424
			  ? (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
			     .aClass631_12226)
			  : null),
			 Class620.aClass632_8113, -1957849728);
		    class534_sub8.method8892((byte) 1);
		}
		Class422.method6785((byte) -83);
	    } else {
		if (null == aClass185_1297) {
		    Canvas canvas = new Canvas();
		    canvas.setSize(36, 32);
		    aClass185_1297
			= Class321.method5777(0, canvas,
					      Class656.aClass177_8524,
					      Class534.anInterface25_7160,
					      Class334.aClass402_3513,
					      Class269.aClass396_2956,
					      Class48.aClass387_363,
					      Class295.aClass472_3161, 0,
					      2143455538);
		    Class662.aClass171_8551
			= (aClass185_1297.method3325
			   (Class711.method14414(Class606.aClass472_7988,
						 (Class67.anInt715
						  * -1643399711),
						 0, -221465200),
			    Class178.method2939(Class464.aClass472_5113,
						Class67.anInt715 * -1643399711,
						0),
			    true));
		}
		for (Class534_Sub8 class534_sub8
			 = ((Class534_Sub8)
			    aClass700_1298.method14135((byte) -1));
		     class534_sub8 != null;
		     class534_sub8
			 = ((Class534_Sub8)
			    aClass700_1298.method14139(1335005116))) {
		    Class531.aClass44_Sub7_7135.method17307
			(aClass185_1297, class185,
			 -2069726317 * class534_sub8.anInt10420,
			 class534_sub8.anInt10419 * -863343329,
			 1038549693 * class534_sub8.anInt10423,
			 304308761 * class534_sub8.anInt10422, false, false,
			 1740682347 * class534_sub8.anInt10421,
			 Class662.aClass171_8551,
			 (class534_sub8.aBool10424
			  ? (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
			     .aClass631_12226)
			  : null),
			 Class620.aClass632_8113, -1967308205);
		    class534_sub8.method8892((byte) 1);
		}
	    }
	}
    }
    
    static void method1928(Class185 class185, Class247 class247) {
	boolean bool
	    = ((Class531.aClass44_Sub7_7135.method17306
		(class185, class247.anInt2606 * 403396513,
		 class247.anInt2602 * -148110895,
		 973326593 * class247.anInt2490,
		 ~0xffffff | class247.anInt2587 * 2071487835,
		 class247.anInt2513 * 1245616333,
		 (class247.aBool2610
		  ? Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419.aClass631_12226
		  : null),
		 -878424575))
	       == null);
	if (bool) {
	    aClass700_1298.method14131
		(new Class534_Sub8(class247.anInt2606 * 403396513,
				   class247.anInt2602 * -148110895,
				   class247.anInt2490 * 973326593,
				   ~0xffffff | class247.anInt2587 * 2071487835,
				   class247.anInt2513 * 1245616333,
				   class247.aBool2610),
		 (short) 789);
	    Class454.method7416(class247, -1699173700);
	}
    }
    
    public static Class408[] method1929(int i) {
	return (new Class408[]
		{ Class408.aClass408_4338, Class408.aClass408_4322,
		  Class408.aClass408_4337, Class408.aClass408_4327,
		  Class408.aClass408_4362, Class408.aClass408_4363,
		  Class408.aClass408_4340, Class408.aClass408_4359,
		  Class408.aClass408_4364, Class408.aClass408_4326,
		  Class408.aClass408_4325, Class408.aClass408_4361,
		  Class408.aClass408_4351, Class408.aClass408_4344,
		  Class408.aClass408_4352, Class408.aClass408_4323,
		  Class408.aClass408_4324, Class408.aClass408_4331,
		  Class408.aClass408_4319, Class408.aClass408_4333,
		  Class408.aClass408_4341, Class408.aClass408_4353,
		  Class408.aClass408_4355, Class408.aClass408_4317,
		  Class408.aClass408_4357, Class408.aClass408_4336,
		  Class408.aClass408_4321, Class408.aClass408_4349,
		  Class408.aClass408_4356, Class408.aClass408_4350,
		  Class408.aClass408_4348, Class408.aClass408_4318,
		  Class408.aClass408_4315, Class408.aClass408_4342,
		  Class408.aClass408_4334, Class408.aClass408_4343,
		  Class408.aClass408_4335, Class408.aClass408_4316,
		  Class408.aClass408_4345, Class408.aClass408_4332,
		  Class408.aClass408_4330, Class408.aClass408_4346,
		  Class408.aClass408_4328, Class408.aClass408_4314,
		  Class408.aClass408_4339, Class408.aClass408_4354,
		  Class408.aClass408_4329, Class408.aClass408_4360,
		  Class408.aClass408_4320, Class408.aClass408_4347,
		  Class408.aClass408_4313, Class408.aClass408_4358 });
    }
    
    static final void method1930(Class669 class669, int i) {
	int i_0_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub33_10765
		  .method14026(i_0_, -2024064741);
    }
    
    static final void method1931(Class669 class669, short i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = class669.aClass654_Sub1_Sub5_Sub1_8604.method16897(-1527522767);
    }
}
