/* Class298_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.util.Iterator;

public class Class298_Sub1 extends Class298
{
    int anInt10062 = 0;
    public static boolean aBool10063;
    public static Class472 aClass472_10064;
    
    public int method15678(int i) {
	anInt10062 += -1538640795;
	if (anInt10062 * 1987272045 == 126)
	    anInt10062 = 0;
	return anInt10062 * 1987272045;
    }
    
    public void method15679(Class534_Sub40 class534_sub40, int i) {
	int i_0_ = class534_sub40.anInt10811 * 31645619;
	int i_1_ = class534_sub40.method16527(-334807806);
	method5353(Class247.method4593(i_1_ & 0x1, 1853923000), (byte) 8);
	if ((i_1_ & 0x8) != 0) {
	    Class293 class293
		= Class255.method4645(class534_sub40.method16527(2005649909),
				      2115718220);
	    if (class293 != aClass293_3191) {
		try {
		    method5455(class293, true, (byte) -70);
		} catch (Exception_Sub2 exception_sub2) {
		    exception_sub2.printStackTrace();
		}
	    }
	}
	if ((i_1_ & 0x10) != 0) {
	    Class305 class305
		= Class217.method4114(class534_sub40.method16527(2055495371),
				      608978046);
	    if (aClass305_3196 != class305) {
		try {
		    method5355(class305, true, 1286284930);
		} catch (Exception_Sub2 exception_sub2) {
		    exception_sub2.printStackTrace();
		}
	    }
	}
	if ((i_1_ >> 7 & 0x1) == 1) {
	    int i_2_ = class534_sub40.method16529((byte) 1);
	    if (1 == (i_2_ >> 269920571 * Class289.aClass289_3101.anInt3105
		      & 0x1))
		aClass438_3184.method7080(class534_sub40);
	    if (1 == (i_2_ >> Class289.aClass289_3091.anInt3105 * 269920571
		      & 0x1))
		aClass438_3195.method7080(class534_sub40);
	    if ((i_2_ >> 269920571 * Class289.aClass289_3092.anInt3105 & 0x1)
		== 1)
		aClass438_3200.method7080(class534_sub40);
	    if ((i_2_ >> Class289.aClass289_3093.anInt3105 * 269920571 & 0x1)
		== 1)
		aClass438_3188.method7080(class534_sub40);
	    if ((i_2_ >> 269920571 * Class289.aClass289_3094.anInt3105 & 0x1)
		== 1) {
		aFloat3199 = class534_sub40.method16539(-1912586817);
		aFloat3173 = class534_sub40.method16539(-1933774550);
	    }
	    if ((i_2_ >> Class289.aClass289_3090.anInt3105 * 269920571 & 0x1)
		== 1) {
		aFloat3180 = class534_sub40.method16539(-1349812019);
		aFloat3204 = class534_sub40.method16539(-1325840456);
	    }
	    if (1 == (i_2_ >> Class289.aClass289_3096.anInt3105 * 269920571
		      & 0x1))
		aClass310_3175
		    = Class175.method2920(class534_sub40
					      .method16527(131828562),
					  -2088509352);
	    if (1 == (i_2_ >> 269920571 * Class289.aClass289_3097.anInt3105
		      & 0x1)) {
		anInt3182 = class534_sub40.method16531(518806085) * 2095297221;
		class534_sub40.method16527(-1396636446);
	    }
	    if (1 == (i_2_ >> 269920571 * Class289.aClass289_3102.anInt3105
		      & 0x1)) {
		int i_3_ = class534_sub40.method16527(2041216140);
		aBool3176 = 1 == (i_3_ & 0x1);
		aBool3185 = (i_3_ & 0x2) == 2;
	    }
	    if ((i_2_ >> Class289.aClass289_3099.anInt3105 * 269920571 & 0x1)
		== 1) {
		int i_4_ = class534_sub40.method16527(-1978343158);
		for (int i_5_ = 0; i_5_ < i_4_; i_5_++) {
		    int i_6_ = class534_sub40.method16527(-1232330066);
		    int i_7_ = class534_sub40.method16527(-1439576990);
		    if (0 == i_6_)
			method5374(i_7_, (byte) -49);
		    else {
			Class271 class271
			    = (Class482.method7932
			       (class534_sub40.method16527(-1290024479),
				-1890792124));
			boolean bool = true;
			Iterator iterator = aClass9_3189.iterator();
			while (iterator.hasNext()) {
			    Class534_Sub18_Sub13 class534_sub18_sub13
				= (Class534_Sub18_Sub13) iterator.next();
			    if (i_7_ == (class534_sub18_sub13.anInt11802
					 * -997430305)) {
				class534_sub18_sub13
				    .method18383(class534_sub40, 1345190522);
				bool = false;
				break;
			    }
			}
			if (bool)
			    method5373(Class283.method5251(i_7_, class271,
							   class534_sub40,
							   1510000011),
				       356365251);
		    }
		}
	    }
	    if (1 == (i_2_ >> 269920571 * Class289.aClass289_3098.anInt3105
		      & 0x1)) {
		anInt3207 = class534_sub40.method16529((byte) 1) * 695781827;
		aFloat3192 = class534_sub40.method16539(-1533579763);
	    }
	    if ((i_2_ >> Class289.aClass289_3100.anInt3105 * 269920571 & 0x1)
		== 1)
		aClass303_3181
		    = Class108.method1964(class534_sub40
					      .method16527(715243575),
					  (byte) 1);
	    if ((i_2_ >> Class289.aClass289_3103.anInt3105 * 269920571 & 0x1)
		== 1) {
		aClass438_3206.method7080(class534_sub40);
		aClass438_3208.method7080(class534_sub40);
		aFloat3197 = class534_sub40.method16539(-1761332604);
		aFloat3198 = class534_sub40.method16539(-1594103868);
	    }
	    if ((i_2_ >> 269920571 * Class289.aClass289_3095.anInt3105 & 0x1)
		== 1)
		class534_sub40.method16539(-1063424328);
	    if (1 == (i_2_ >> 269920571 * Class289.aClass289_3104.anInt3105
		      & 0x1))
		aFloat3183 = class534_sub40.method16539(-1300494411);
	}
	if (aClass706_3177 != null && (i_1_ >> 5 & 0x1) == 1)
	    aClass706_3177.method14235(class534_sub40, -1252640479);
	if (aClass347_3179 != null && (i_1_ >> 6 & 0x1) == 1)
	    aClass347_3179.method6148(class534_sub40, (byte) 85);
	if (i != 31645619 * class534_sub40.anInt10811 - i_0_)
	    throw new RuntimeException(new StringBuilder().append
					   ((class534_sub40.anInt10811
					     * 31645619) - i_0_)
					   .append
					   (",").append
					   (i).toString());
    }
    
    public void method15680(Class534_Sub40 class534_sub40, int i, int i_8_) {
	int i_9_ = class534_sub40.anInt10811 * 31645619;
	int i_10_ = class534_sub40.method16527(-1648884497);
	method5353(Class247.method4593(i_10_ & 0x1, 1958101896), (byte) -61);
	if ((i_10_ & 0x8) != 0) {
	    Class293 class293
		= Class255.method4645(class534_sub40.method16527(821378580),
				      2034032214);
	    if (class293 != aClass293_3191) {
		try {
		    method5455(class293, true, (byte) 77);
		} catch (Exception_Sub2 exception_sub2) {
		    exception_sub2.printStackTrace();
		}
	    }
	}
	if ((i_10_ & 0x10) != 0) {
	    Class305 class305
		= Class217.method4114(class534_sub40.method16527(765903348),
				      608978046);
	    if (aClass305_3196 != class305) {
		try {
		    method5355(class305, true, 1919203434);
		} catch (Exception_Sub2 exception_sub2) {
		    exception_sub2.printStackTrace();
		}
	    }
	}
	if ((i_10_ >> 7 & 0x1) == 1) {
	    int i_11_ = class534_sub40.method16529((byte) 1);
	    if (1 == (i_11_ >> 269920571 * Class289.aClass289_3101.anInt3105
		      & 0x1))
		aClass438_3184.method7080(class534_sub40);
	    if (1 == (i_11_ >> Class289.aClass289_3091.anInt3105 * 269920571
		      & 0x1))
		aClass438_3195.method7080(class534_sub40);
	    if ((i_11_ >> 269920571 * Class289.aClass289_3092.anInt3105 & 0x1)
		== 1)
		aClass438_3200.method7080(class534_sub40);
	    if ((i_11_ >> Class289.aClass289_3093.anInt3105 * 269920571 & 0x1)
		== 1)
		aClass438_3188.method7080(class534_sub40);
	    if ((i_11_ >> 269920571 * Class289.aClass289_3094.anInt3105 & 0x1)
		== 1) {
		aFloat3199 = class534_sub40.method16539(-1525805800);
		aFloat3173 = class534_sub40.method16539(-1407649979);
	    }
	    if ((i_11_ >> Class289.aClass289_3090.anInt3105 * 269920571 & 0x1)
		== 1) {
		aFloat3180 = class534_sub40.method16539(-1549870074);
		aFloat3204 = class534_sub40.method16539(-1119363833);
	    }
	    if (1 == (i_11_ >> Class289.aClass289_3096.anInt3105 * 269920571
		      & 0x1))
		aClass310_3175
		    = Class175.method2920(class534_sub40
					      .method16527(-625052182),
					  973900965);
	    if (1 == (i_11_ >> 269920571 * Class289.aClass289_3097.anInt3105
		      & 0x1)) {
		anInt3182 = class534_sub40.method16531(930369190) * 2095297221;
		class534_sub40.method16527(-2014334155);
	    }
	    if (1 == (i_11_ >> 269920571 * Class289.aClass289_3102.anInt3105
		      & 0x1)) {
		int i_12_ = class534_sub40.method16527(1561022787);
		aBool3176 = 1 == (i_12_ & 0x1);
		aBool3185 = (i_12_ & 0x2) == 2;
	    }
	    if ((i_11_ >> Class289.aClass289_3099.anInt3105 * 269920571 & 0x1)
		== 1) {
		int i_13_ = class534_sub40.method16527(-732515732);
		for (int i_14_ = 0; i_14_ < i_13_; i_14_++) {
		    int i_15_ = class534_sub40.method16527(-1620092352);
		    int i_16_ = class534_sub40.method16527(1689434517);
		    if (0 == i_15_)
			method5374(i_16_, (byte) 97);
		    else {
			Class271 class271
			    = Class482.method7932(class534_sub40
						      .method16527(679020882),
						  152452385);
			boolean bool = true;
			Iterator iterator = aClass9_3189.iterator();
			while (iterator.hasNext()) {
			    Class534_Sub18_Sub13 class534_sub18_sub13
				= (Class534_Sub18_Sub13) iterator.next();
			    if (i_16_ == (class534_sub18_sub13.anInt11802
					  * -997430305)) {
				class534_sub18_sub13
				    .method18383(class534_sub40, 1505981781);
				bool = false;
				break;
			    }
			}
			if (bool)
			    method5373(Class283.method5251(i_16_, class271,
							   class534_sub40,
							   1445841994),
				       356365251);
		    }
		}
	    }
	    if (1 == (i_11_ >> 269920571 * Class289.aClass289_3098.anInt3105
		      & 0x1)) {
		anInt3207 = class534_sub40.method16529((byte) 1) * 695781827;
		aFloat3192 = class534_sub40.method16539(-1413875088);
	    }
	    if ((i_11_ >> Class289.aClass289_3100.anInt3105 * 269920571 & 0x1)
		== 1)
		aClass303_3181
		    = Class108.method1964(class534_sub40
					      .method16527(234193477),
					  (byte) 1);
	    if ((i_11_ >> Class289.aClass289_3103.anInt3105 * 269920571 & 0x1)
		== 1) {
		aClass438_3206.method7080(class534_sub40);
		aClass438_3208.method7080(class534_sub40);
		aFloat3197 = class534_sub40.method16539(-1963869560);
		aFloat3198 = class534_sub40.method16539(-2057584884);
	    }
	    if ((i_11_ >> 269920571 * Class289.aClass289_3095.anInt3105 & 0x1)
		== 1)
		class534_sub40.method16539(-980431804);
	    if (1 == (i_11_ >> 269920571 * Class289.aClass289_3104.anInt3105
		      & 0x1))
		aFloat3183 = class534_sub40.method16539(-1401635544);
	}
	if (aClass706_3177 != null && (i_10_ >> 5 & 0x1) == 1)
	    aClass706_3177.method14235(class534_sub40, -1433251422);
	if (aClass347_3179 != null && (i_10_ >> 6 & 0x1) == 1)
	    aClass347_3179.method6148(class534_sub40, (byte) 57);
	if (i != 31645619 * class534_sub40.anInt10811 - i_9_)
	    throw new RuntimeException(new StringBuilder().append
					   ((class534_sub40.anInt10811
					     * 31645619) - i_9_)
					   .append
					   (",").append
					   (i).toString());
    }
    
    public int method15681() {
	anInt10062 += -1538640795;
	if (anInt10062 * 1987272045 == 126)
	    anInt10062 = 0;
	return anInt10062 * 1987272045;
    }
    
    public int method15682() {
	anInt10062 += -1538640795;
	if (anInt10062 * 1987272045 == 126)
	    anInt10062 = 0;
	return anInt10062 * 1987272045;
    }
    
    public void method15683(Class534_Sub40 class534_sub40, int i) {
	int i_17_ = class534_sub40.anInt10811 * 31645619;
	int i_18_ = class534_sub40.method16527(1874571976);
	method5353(Class247.method4593(i_18_ & 0x1, 2029139796), (byte) 44);
	if ((i_18_ & 0x8) != 0) {
	    Class293 class293
		= Class255.method4645(class534_sub40.method16527(-1292530731),
				      2093414224);
	    if (class293 != aClass293_3191) {
		try {
		    method5455(class293, true, (byte) -60);
		} catch (Exception_Sub2 exception_sub2) {
		    exception_sub2.printStackTrace();
		}
	    }
	}
	if ((i_18_ & 0x10) != 0) {
	    Class305 class305
		= Class217.method4114(class534_sub40.method16527(-254294839),
				      608978046);
	    if (aClass305_3196 != class305) {
		try {
		    method5355(class305, true, 1907138206);
		} catch (Exception_Sub2 exception_sub2) {
		    exception_sub2.printStackTrace();
		}
	    }
	}
	if ((i_18_ >> 7 & 0x1) == 1) {
	    int i_19_ = class534_sub40.method16529((byte) 1);
	    if (1 == (i_19_ >> 269920571 * Class289.aClass289_3101.anInt3105
		      & 0x1))
		aClass438_3184.method7080(class534_sub40);
	    if (1 == (i_19_ >> Class289.aClass289_3091.anInt3105 * 269920571
		      & 0x1))
		aClass438_3195.method7080(class534_sub40);
	    if ((i_19_ >> 269920571 * Class289.aClass289_3092.anInt3105 & 0x1)
		== 1)
		aClass438_3200.method7080(class534_sub40);
	    if ((i_19_ >> Class289.aClass289_3093.anInt3105 * 269920571 & 0x1)
		== 1)
		aClass438_3188.method7080(class534_sub40);
	    if ((i_19_ >> 269920571 * Class289.aClass289_3094.anInt3105 & 0x1)
		== 1) {
		aFloat3199 = class534_sub40.method16539(-1262568931);
		aFloat3173 = class534_sub40.method16539(-1846591229);
	    }
	    if ((i_19_ >> Class289.aClass289_3090.anInt3105 * 269920571 & 0x1)
		== 1) {
		aFloat3180 = class534_sub40.method16539(-1610221027);
		aFloat3204 = class534_sub40.method16539(-1686368394);
	    }
	    if (1 == (i_19_ >> Class289.aClass289_3096.anInt3105 * 269920571
		      & 0x1))
		aClass310_3175
		    = Class175.method2920(class534_sub40.method16527(18290512),
					  -479302478);
	    if (1 == (i_19_ >> 269920571 * Class289.aClass289_3097.anInt3105
		      & 0x1)) {
		anInt3182
		    = class534_sub40.method16531(1712368153) * 2095297221;
		class534_sub40.method16527(1363088735);
	    }
	    if (1 == (i_19_ >> 269920571 * Class289.aClass289_3102.anInt3105
		      & 0x1)) {
		int i_20_ = class534_sub40.method16527(-313851044);
		aBool3176 = 1 == (i_20_ & 0x1);
		aBool3185 = (i_20_ & 0x2) == 2;
	    }
	    if ((i_19_ >> Class289.aClass289_3099.anInt3105 * 269920571 & 0x1)
		== 1) {
		int i_21_ = class534_sub40.method16527(-1525902228);
		for (int i_22_ = 0; i_22_ < i_21_; i_22_++) {
		    int i_23_ = class534_sub40.method16527(1884976667);
		    int i_24_ = class534_sub40.method16527(818687363);
		    if (0 == i_23_)
			method5374(i_24_, (byte) 8);
		    else {
			Class271 class271
			    = Class482.method7932(class534_sub40
						      .method16527(1931142379),
						  -564285551);
			boolean bool = true;
			Iterator iterator = aClass9_3189.iterator();
			while (iterator.hasNext()) {
			    Class534_Sub18_Sub13 class534_sub18_sub13
				= (Class534_Sub18_Sub13) iterator.next();
			    if (i_24_ == (class534_sub18_sub13.anInt11802
					  * -997430305)) {
				class534_sub18_sub13
				    .method18383(class534_sub40, 1881972370);
				bool = false;
				break;
			    }
			}
			if (bool)
			    method5373(Class283.method5251(i_24_, class271,
							   class534_sub40,
							   1244555532),
				       356365251);
		    }
		}
	    }
	    if (1 == (i_19_ >> 269920571 * Class289.aClass289_3098.anInt3105
		      & 0x1)) {
		anInt3207 = class534_sub40.method16529((byte) 1) * 695781827;
		aFloat3192 = class534_sub40.method16539(-1850796142);
	    }
	    if ((i_19_ >> Class289.aClass289_3100.anInt3105 * 269920571 & 0x1)
		== 1)
		aClass303_3181
		    = Class108.method1964(class534_sub40
					      .method16527(-1005670849),
					  (byte) 1);
	    if ((i_19_ >> Class289.aClass289_3103.anInt3105 * 269920571 & 0x1)
		== 1) {
		aClass438_3206.method7080(class534_sub40);
		aClass438_3208.method7080(class534_sub40);
		aFloat3197 = class534_sub40.method16539(-2024799181);
		aFloat3198 = class534_sub40.method16539(-1322226097);
	    }
	    if ((i_19_ >> 269920571 * Class289.aClass289_3095.anInt3105 & 0x1)
		== 1)
		class534_sub40.method16539(-1220426288);
	    if (1 == (i_19_ >> 269920571 * Class289.aClass289_3104.anInt3105
		      & 0x1))
		aFloat3183 = class534_sub40.method16539(-2097402687);
	}
	if (aClass706_3177 != null && (i_18_ >> 5 & 0x1) == 1)
	    aClass706_3177.method14235(class534_sub40, -2040001237);
	if (aClass347_3179 != null && (i_18_ >> 6 & 0x1) == 1)
	    aClass347_3179.method6148(class534_sub40, (byte) -59);
	if (i != 31645619 * class534_sub40.anInt10811 - i_17_)
	    throw new RuntimeException(new StringBuilder().append
					   ((class534_sub40.anInt10811
					     * 31645619) - i_17_)
					   .append
					   (",").append
					   (i).toString());
    }
    
    public int method15684() {
	anInt10062 += -1538640795;
	if (anInt10062 * 1987272045 == 126)
	    anInt10062 = 0;
	return anInt10062 * 1987272045;
    }
    
    public Class298_Sub1(Interface30 interface30) {
	super(Class309.aClass309_3353, interface30);
    }
    
    public int method15685() {
	anInt10062 += -1538640795;
	if (anInt10062 * 1987272045 == 126)
	    anInt10062 = 0;
	return anInt10062 * 1987272045;
    }
}
