/* Class346 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class346
{
    static final int anInt3572 = 9;
    static final int anInt3573 = 3;
    Class700 aClass700_3574;
    static final int anInt3575 = 1;
    static final int anInt3576 = 10;
    static final int anInt3577 = 2;
    static final int anInt3578 = 4;
    long aLong3579;
    static final int anInt3580 = 6;
    static final int anInt3581 = 7;
    static final int anInt3582 = 8;
    static final int anInt3583 = 12;
    static final int anInt3584 = 5;
    static final int anInt3585 = 11;
    static final int anInt3586 = 13;
    int anInt3587 = 828518333;
    static final int anInt3588 = 14;
    
    void method6122(Class534_Sub40 class534_sub40) {
	aLong3579
	    = class534_sub40.method16537(1359621443) * -5749192068976987273L;
	anInt3587 = class534_sub40.method16533(-258848859) * -828518333;
	for (int i = class534_sub40.method16527(536502261); 0 != i;
	     i = class534_sub40.method16527(2141476151)) {
	    Class534_Sub11 class534_sub11;
	    if (3 == i)
		class534_sub11 = new Class534_Sub11_Sub2(this);
	    else if (i == 1)
		class534_sub11 = new Class534_Sub11_Sub5(this);
	    else if (i == 13)
		class534_sub11 = new Class534_Sub11_Sub4(this);
	    else if (4 == i)
		class534_sub11 = new Class534_Sub11_Sub14(this);
	    else if (6 == i)
		class534_sub11 = new Class534_Sub11_Sub7(this);
	    else if (5 == i)
		class534_sub11 = new Class534_Sub11_Sub3(this);
	    else if (2 == i)
		class534_sub11 = new Class534_Sub11_Sub1(this);
	    else if (7 == i)
		class534_sub11 = new Class534_Sub11_Sub6(this);
	    else if (i == 14)
		class534_sub11 = new Class534_Sub11_Sub12(this);
	    else if (i == 8)
		class534_sub11 = new Class534_Sub11_Sub13(this);
	    else if (9 == i)
		class534_sub11 = new Class534_Sub11_Sub10(this);
	    else if (10 == i)
		class534_sub11 = new Class534_Sub11_Sub8(this);
	    else if (i == 11)
		class534_sub11 = new Class534_Sub11_Sub9(this);
	    else if (12 == i)
		class534_sub11 = new Class534_Sub11_Sub11(this);
	    else
		throw new RuntimeException("");
	    class534_sub11.method16128(class534_sub40, 955150514);
	    aClass700_3574.method14131(class534_sub11, (short) 789);
	}
    }
    
    void method6123(Class534_Sub40 class534_sub40, byte i) {
	aLong3579
	    = class534_sub40.method16537(1359621443) * -5749192068976987273L;
	anInt3587 = class534_sub40.method16533(-258848859) * -828518333;
	for (int i_0_ = class534_sub40.method16527(-1471709421); 0 != i_0_;
	     i_0_ = class534_sub40.method16527(2077420156)) {
	    Class534_Sub11 class534_sub11;
	    if (3 == i_0_)
		class534_sub11 = new Class534_Sub11_Sub2(this);
	    else if (i_0_ == 1)
		class534_sub11 = new Class534_Sub11_Sub5(this);
	    else if (i_0_ == 13)
		class534_sub11 = new Class534_Sub11_Sub4(this);
	    else if (4 == i_0_)
		class534_sub11 = new Class534_Sub11_Sub14(this);
	    else if (6 == i_0_)
		class534_sub11 = new Class534_Sub11_Sub7(this);
	    else if (5 == i_0_)
		class534_sub11 = new Class534_Sub11_Sub3(this);
	    else if (2 == i_0_)
		class534_sub11 = new Class534_Sub11_Sub1(this);
	    else if (7 == i_0_)
		class534_sub11 = new Class534_Sub11_Sub6(this);
	    else if (i_0_ == 14)
		class534_sub11 = new Class534_Sub11_Sub12(this);
	    else if (i_0_ == 8)
		class534_sub11 = new Class534_Sub11_Sub13(this);
	    else if (9 == i_0_)
		class534_sub11 = new Class534_Sub11_Sub10(this);
	    else if (10 == i_0_)
		class534_sub11 = new Class534_Sub11_Sub8(this);
	    else if (i_0_ == 11)
		class534_sub11 = new Class534_Sub11_Sub9(this);
	    else if (12 == i_0_)
		class534_sub11 = new Class534_Sub11_Sub11(this);
	    else
		throw new RuntimeException("");
	    class534_sub11.method16128(class534_sub40, 955150514);
	    aClass700_3574.method14131(class534_sub11, (short) 789);
	}
    }
    
    public void method6124(Class352 class352, int i) {
	if ((-1240730156808410261L * class352.aLong3640
	     != aLong3579 * -1903610295825951161L)
	    || anInt3587 * 1012464747 != -406614527 * class352.anInt3626)
	    throw new RuntimeException("");
	for (Class534_Sub11 class534_sub11
		 = (Class534_Sub11) aClass700_3574.method14135((byte) -1);
	     class534_sub11 != null;
	     class534_sub11
		 = (Class534_Sub11) aClass700_3574.method14139(1510104654))
	    class534_sub11.method16129(class352, (byte) 86);
	class352.anInt3626 += 50360833;
    }
    
    void method6125(Class534_Sub40 class534_sub40) {
	aLong3579
	    = class534_sub40.method16537(1359621443) * -5749192068976987273L;
	anInt3587 = class534_sub40.method16533(-258848859) * -828518333;
	for (int i = class534_sub40.method16527(-269512564); 0 != i;
	     i = class534_sub40.method16527(1932726588)) {
	    Class534_Sub11 class534_sub11;
	    if (3 == i)
		class534_sub11 = new Class534_Sub11_Sub2(this);
	    else if (i == 1)
		class534_sub11 = new Class534_Sub11_Sub5(this);
	    else if (i == 13)
		class534_sub11 = new Class534_Sub11_Sub4(this);
	    else if (4 == i)
		class534_sub11 = new Class534_Sub11_Sub14(this);
	    else if (6 == i)
		class534_sub11 = new Class534_Sub11_Sub7(this);
	    else if (5 == i)
		class534_sub11 = new Class534_Sub11_Sub3(this);
	    else if (2 == i)
		class534_sub11 = new Class534_Sub11_Sub1(this);
	    else if (7 == i)
		class534_sub11 = new Class534_Sub11_Sub6(this);
	    else if (i == 14)
		class534_sub11 = new Class534_Sub11_Sub12(this);
	    else if (i == 8)
		class534_sub11 = new Class534_Sub11_Sub13(this);
	    else if (9 == i)
		class534_sub11 = new Class534_Sub11_Sub10(this);
	    else if (10 == i)
		class534_sub11 = new Class534_Sub11_Sub8(this);
	    else if (i == 11)
		class534_sub11 = new Class534_Sub11_Sub9(this);
	    else if (12 == i)
		class534_sub11 = new Class534_Sub11_Sub11(this);
	    else
		throw new RuntimeException("");
	    class534_sub11.method16128(class534_sub40, 955150514);
	    aClass700_3574.method14131(class534_sub11, (short) 789);
	}
    }
    
    public Class346(Class534_Sub40 class534_sub40) {
	aClass700_3574 = new Class700();
	method6123(class534_sub40, (byte) 98);
    }
    
    public void method6126(Class352 class352) {
	if ((-1240730156808410261L * class352.aLong3640
	     != aLong3579 * -1903610295825951161L)
	    || anInt3587 * 1012464747 != -406614527 * class352.anInt3626)
	    throw new RuntimeException("");
	for (Class534_Sub11 class534_sub11
		 = (Class534_Sub11) aClass700_3574.method14135((byte) -1);
	     class534_sub11 != null;
	     class534_sub11
		 = (Class534_Sub11) aClass700_3574.method14139(1030424316))
	    class534_sub11.method16129(class352, (byte) 40);
	class352.anInt3626 += 50360833;
    }
    
    public void method6127(Class352 class352) {
	if ((-1240730156808410261L * class352.aLong3640
	     != aLong3579 * -1903610295825951161L)
	    || anInt3587 * 1012464747 != -406614527 * class352.anInt3626)
	    throw new RuntimeException("");
	for (Class534_Sub11 class534_sub11
		 = (Class534_Sub11) aClass700_3574.method14135((byte) -1);
	     class534_sub11 != null;
	     class534_sub11
		 = (Class534_Sub11) aClass700_3574.method14139(1580474970))
	    class534_sub11.method16129(class352, (byte) 75);
	class352.anInt3626 += 50360833;
    }
    
    public static Class534_Sub19 method6128(Class404 class404, IsaacCipher class13,
					    int i) {
	Class534_Sub19 class534_sub19 = Class670.method11042(994484310);
	class534_sub19.aClass404_10512 = class404;
	class534_sub19.anInt10516 = 182467999 * class404.anInt4277;
	if (-1 == 393990279 * class534_sub19.anInt10516)
	    class534_sub19.aClass534_Sub40_Sub1_10513
		= new Class534_Sub40_Sub1(260);
	else if (class534_sub19.anInt10516 * 393990279 == -2)
	    class534_sub19.aClass534_Sub40_Sub1_10513
		= new Class534_Sub40_Sub1(10000);
	else if (393990279 * class534_sub19.anInt10516 <= 18)
	    class534_sub19.aClass534_Sub40_Sub1_10513
		= new Class534_Sub40_Sub1(20);
	else if (class534_sub19.anInt10516 * 393990279 <= 98)
	    class534_sub19.aClass534_Sub40_Sub1_10513
		= new Class534_Sub40_Sub1(100);
	else
	    class534_sub19.aClass534_Sub40_Sub1_10513
		= new Class534_Sub40_Sub1(260);
	class534_sub19.aClass534_Sub40_Sub1_10513.method18316(class13,
							      163694688);
	class534_sub19.aClass534_Sub40_Sub1_10513.method18306
	    (145532973 * class534_sub19.aClass404_10512.anInt4276, 78957319);
	class534_sub19.anInt10511 = 0;
	return class534_sub19;
    }
}
