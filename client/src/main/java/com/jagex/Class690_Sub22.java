/* Class690_Sub22 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class690_Sub22 extends Class690
{
    static Class169 aClass169_10914;
    
    public void method17076(int i) {
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14108(-1233550485))
	    anInt8753 = 1931346361 * Class302.aClass302_3245.anInt3244;
	else {
	    int i_0_ = aClass534_Sub35_8752.method16442(-531493946)
			   .method14115((byte) 57);
	    if (i_0_ < 245)
		anInt8753 = 1931346361 * Class302.aClass302_3246.anInt3244;
	    if ((anInt8753 * 189295939
		 == Class302.aClass302_3242.anInt3244 * 1453209707)
		&& i_0_ < 500)
		anInt8753 = Class302.aClass302_3239.anInt3244 * 1931346361;
	    if ((anInt8753 * 189295939
		 < Class302.aClass302_3246.anInt3244 * 1453209707)
		|| (anInt8753 * 189295939
		    > 1453209707 * Class302.aClass302_3243.anInt3244))
		anInt8753 = method14017(2143837928) * 1823770475;
	}
    }
    
    public Class690_Sub22(int i, Class534_Sub35 class534_sub35) {
	super(i, class534_sub35);
    }
    
    public Class690_Sub22(Class534_Sub35 class534_sub35) {
	super(class534_sub35);
    }
    
    int method14017(int i) {
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14108(-1233550485))
	    return 1453209707 * Class302.aClass302_3245.anInt3244;
	return 1453209707 * Class302.aClass302_3246.anInt3244;
    }
    
    public boolean method17077(int i) {
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14108(-1233550485))
	    return false;
	int i_1_ = aClass534_Sub35_8752.method16442(-531493946)
		       .method14115((byte) 13);
	if (i_1_ < 245)
	    return false;
	return true;
    }
    
    public int method14026(int i, int i_2_) {
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14108(-1233550485))
	    return 3;
	int i_3_ = aClass534_Sub35_8752.method16442(-531493946)
		       .method14115((byte) 19);
	if (i_3_ < 245)
	    return 3;
	if (i == Class302.aClass302_3242.anInt3244 * 1453209707 && i_3_ < 500)
	    return 3;
	return 1;
    }
    
    void method14020(int i, int i_4_) {
	anInt8753 = i * 1823770475;
    }
    
    public int method17078(int i) {
	return 189295939 * anInt8753;
    }
    
    int method14021() {
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14108(-1233550485))
	    return 1453209707 * Class302.aClass302_3245.anInt3244;
	return 1453209707 * Class302.aClass302_3246.anInt3244;
    }
    
    public void method17079() {
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14108(-1233550485))
	    anInt8753 = 1931346361 * Class302.aClass302_3245.anInt3244;
	else {
	    int i = aClass534_Sub35_8752.method16442(-531493946)
			.method14115((byte) 57);
	    if (i < 245)
		anInt8753 = 1931346361 * Class302.aClass302_3246.anInt3244;
	    if ((anInt8753 * 189295939
		 == Class302.aClass302_3242.anInt3244 * 1453209707)
		&& i < 500)
		anInt8753 = Class302.aClass302_3239.anInt3244 * 1931346361;
	    if ((anInt8753 * 189295939
		 < Class302.aClass302_3246.anInt3244 * 1453209707)
		|| (anInt8753 * 189295939
		    > 1453209707 * Class302.aClass302_3243.anInt3244))
		anInt8753 = method14017(2118253130) * 1823770475;
	}
    }
    
    int method14018() {
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14108(-1233550485))
	    return 1453209707 * Class302.aClass302_3245.anInt3244;
	return 1453209707 * Class302.aClass302_3246.anInt3244;
    }
    
    public int method14029(int i) {
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14108(-1233550485))
	    return 3;
	int i_5_ = aClass534_Sub35_8752.method16442(-531493946)
		       .method14115((byte) 77);
	if (i_5_ < 245)
	    return 3;
	if (i == Class302.aClass302_3242.anInt3244 * 1453209707 && i_5_ < 500)
	    return 3;
	return 1;
    }
    
    void method14024(int i) {
	anInt8753 = i * 1823770475;
    }
    
    public boolean method17080() {
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14108(-1233550485))
	    return false;
	int i = aClass534_Sub35_8752.method16442(-531493946)
		    .method14115((byte) 89);
	if (i < 245)
	    return false;
	return true;
    }
    
    public int method14027(int i) {
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14108(-1233550485))
	    return 3;
	int i_6_ = aClass534_Sub35_8752.method16442(-531493946)
		       .method14115((byte) 33);
	if (i_6_ < 245)
	    return 3;
	if (i == Class302.aClass302_3242.anInt3244 * 1453209707 && i_6_ < 500)
	    return 3;
	return 1;
    }
    
    public int method14028(int i) {
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14108(-1233550485))
	    return 3;
	int i_7_ = aClass534_Sub35_8752.method16442(-531493946)
		       .method14115((byte) 32);
	if (i_7_ < 245)
	    return 3;
	if (i == Class302.aClass302_3242.anInt3244 * 1453209707 && i_7_ < 500)
	    return 3;
	return 1;
    }
    
    void method14023(int i) {
	anInt8753 = i * 1823770475;
    }
    
    void method14025(int i) {
	anInt8753 = i * 1823770475;
    }
    
    public boolean method17081() {
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14108(-1233550485))
	    return false;
	int i = aClass534_Sub35_8752.method16442(-531493946)
		    .method14115((byte) 77);
	if (i < 245)
	    return false;
	return true;
    }
    
    int method14022() {
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14108(-1233550485))
	    return 1453209707 * Class302.aClass302_3245.anInt3244;
	return 1453209707 * Class302.aClass302_3246.anInt3244;
    }
    
    public int method14030(int i) {
	if (aClass534_Sub35_8752.method16442(-531493946)
		.method14108(-1233550485))
	    return 3;
	int i_8_ = aClass534_Sub35_8752.method16442(-531493946)
		       .method14115((byte) 78);
	if (i_8_ < 245)
	    return 3;
	if (i == Class302.aClass302_3242.anInt3244 * 1453209707 && i_8_ < 500)
	    return 3;
	return 1;
    }
    
    public int method17082() {
	return 189295939 * anInt8753;
    }
}
