/* Class534_Sub34 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class534_Sub34 extends Class534
{
    int anInt10723;
    int anInt10724;
    int anInt10725;
    int anInt10726;
    int anInt10727;
    int anInt10728;
    int anInt10729;
    int anInt10730;
    int anInt10731;
    public static int anInt10732;
    
    void method16419(int i, int i_0_, int[] is) {
	is[0] = -1866465315 * anInt10729;
	is[1] = i + (anInt10724 * -1052720587 - -1934072337 * anInt10728);
	is[2] = i_0_ + (2124392283 * anInt10725 - 1816020231 * anInt10723);
    }
    
    void method16420(int i, int i_1_, int[] is, int i_2_) {
	is[0] = 0;
	is[1] = -1934072337 * anInt10728 - anInt10724 * -1052720587 + i;
	is[2] = i_1_ + (1816020231 * anInt10723 - anInt10725 * 2124392283);
    }
    
    boolean method16421(int i, int i_3_, int i_4_, int i_5_) {
	if (i >= anInt10729 * -1866465315 && i_3_ >= -1052720587 * anInt10724
	    && i_3_ <= anInt10726 * -990133129
	    && i_4_ >= 2124392283 * anInt10725
	    && i_4_ <= 1512492649 * anInt10727)
	    return true;
	return false;
    }
    
    boolean method16422(int i, int i_6_, int i_7_) {
	if (i >= anInt10728 * -1934072337 && i <= anInt10730 * 105883891
	    && i_6_ >= anInt10723 * 1816020231
	    && i_6_ <= -1577710271 * anInt10731)
	    return true;
	return false;
    }
    
    void method16423(int i, int i_8_, int[] is, byte i_9_) {
	is[0] = -1866465315 * anInt10729;
	is[1] = i + (anInt10724 * -1052720587 - -1934072337 * anInt10728);
	is[2] = i_8_ + (2124392283 * anInt10725 - 1816020231 * anInt10723);
    }
    
    boolean method16424(int i, int i_10_, int i_11_) {
	if (i >= anInt10729 * -1866465315 && i_10_ >= -1052720587 * anInt10724
	    && i_10_ <= anInt10726 * -990133129
	    && i_11_ >= 2124392283 * anInt10725
	    && i_11_ <= 1512492649 * anInt10727)
	    return true;
	return false;
    }
    
    Class534_Sub34(int i, int i_12_, int i_13_, int i_14_, int i_15_,
		   int i_16_, int i_17_, int i_18_, int i_19_) {
	anInt10729 = 1246187637 * i;
	anInt10724 = 574750749 * i_12_;
	anInt10725 = 261632211 * i_13_;
	anInt10726 = i_14_ * -354986681;
	anInt10727 = -1886150183 * i_15_;
	anInt10728 = i_16_ * 1909287183;
	anInt10723 = i_17_ * 299636919;
	anInt10730 = -372264901 * i_18_;
	anInt10731 = 277185217 * i_19_;
    }
    
    boolean method16425(int i, int i_20_) {
	if (i >= -1052720587 * anInt10724 && i <= anInt10726 * -990133129
	    && i_20_ >= anInt10725 * 2124392283
	    && i_20_ <= anInt10727 * 1512492649)
	    return true;
	return false;
    }
    
    boolean method16426(int i, int i_21_) {
	if (i >= -1052720587 * anInt10724 && i <= anInt10726 * -990133129
	    && i_21_ >= anInt10725 * 2124392283
	    && i_21_ <= anInt10727 * 1512492649)
	    return true;
	return false;
    }
    
    void method16427(int i, int i_22_, int[] is) {
	is[0] = -1866465315 * anInt10729;
	is[1] = i + (anInt10724 * -1052720587 - -1934072337 * anInt10728);
	is[2] = i_22_ + (2124392283 * anInt10725 - 1816020231 * anInt10723);
    }
    
    boolean method16428(int i, int i_23_) {
	if (i >= anInt10728 * -1934072337 && i <= anInt10730 * 105883891
	    && i_23_ >= anInt10723 * 1816020231
	    && i_23_ <= -1577710271 * anInt10731)
	    return true;
	return false;
    }
    
    void method16429(int i, int i_24_, int[] is) {
	is[0] = -1866465315 * anInt10729;
	is[1] = i + (anInt10724 * -1052720587 - -1934072337 * anInt10728);
	is[2] = i_24_ + (2124392283 * anInt10725 - 1816020231 * anInt10723);
    }
    
    boolean method16430(int i, int i_25_) {
	if (i >= -1052720587 * anInt10724 && i <= anInt10726 * -990133129
	    && i_25_ >= anInt10725 * 2124392283
	    && i_25_ <= anInt10727 * 1512492649)
	    return true;
	return false;
    }
    
    boolean method16431(int i, int i_26_, int i_27_) {
	if (i >= -1052720587 * anInt10724 && i <= anInt10726 * -990133129
	    && i_26_ >= anInt10725 * 2124392283
	    && i_26_ <= anInt10727 * 1512492649)
	    return true;
	return false;
    }
}
