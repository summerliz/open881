/* Class690_Sub10 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class690_Sub10 extends Class690
{
    public static final int anInt10878 = 0;
    public static final int anInt10879 = 1;
    
    void method14020(int i, int i_0_) {
	anInt8753 = i * 1823770475;
    }
    
    public int method14027(int i) {
	if (aClass534_Sub35_8752.method16441(929555429)
	    == Class675.aClass675_8634)
	    return 1;
	return 3;
    }
    
    public int method14030(int i) {
	if (aClass534_Sub35_8752.method16441(2095686895)
	    == Class675.aClass675_8634)
	    return 1;
	return 3;
    }
    
    int method14017(int i) {
	return 1;
    }
    
    public boolean method16966(int i) {
	if (aClass534_Sub35_8752.method16441(-1930820925)
	    == Class675.aClass675_8634)
	    return true;
	return false;
    }
    
    public int method14026(int i, int i_1_) {
	if (aClass534_Sub35_8752.method16441(41399715)
	    == Class675.aClass675_8634)
	    return 1;
	return 3;
    }
    
    public void method16967(int i) {
	if (aClass534_Sub35_8752.method16441(-1367871731)
	    != Class675.aClass675_8634)
	    anInt8753 = 1823770475;
	if (0 != 189295939 * anInt8753 && 189295939 * anInt8753 != 1)
	    anInt8753 = method14017(2140144235) * 1823770475;
    }
    
    public boolean method16968() {
	if (aClass534_Sub35_8752.method16441(-1277782914)
	    == Class675.aClass675_8634)
	    return true;
	return false;
    }
    
    int method14021() {
	return 1;
    }
    
    public boolean method16969() {
	if (aClass534_Sub35_8752.method16441(1978552088)
	    == Class675.aClass675_8634)
	    return true;
	return false;
    }
    
    int method14022() {
	return 1;
    }
    
    void method14024(int i) {
	anInt8753 = i * 1823770475;
    }
    
    void method14025(int i) {
	anInt8753 = i * 1823770475;
    }
    
    void method14023(int i) {
	anInt8753 = i * 1823770475;
    }
    
    public int method16970(byte i) {
	return anInt8753 * 189295939;
    }
    
    public int method14028(int i) {
	if (aClass534_Sub35_8752.method16441(-1388993451)
	    == Class675.aClass675_8634)
	    return 1;
	return 3;
    }
    
    public int method14029(int i) {
	if (aClass534_Sub35_8752.method16441(1516249953)
	    == Class675.aClass675_8634)
	    return 1;
	return 3;
    }
    
    int method14018() {
	return 1;
    }
    
    public void method16971() {
	if (aClass534_Sub35_8752.method16441(-776881366)
	    != Class675.aClass675_8634)
	    anInt8753 = 1823770475;
	if (0 != 189295939 * anInt8753 && 189295939 * anInt8753 != 1)
	    anInt8753 = method14017(2142924788) * 1823770475;
    }
    
    public boolean method16972() {
	if (aClass534_Sub35_8752.method16441(625589599)
	    == Class675.aClass675_8634)
	    return true;
	return false;
    }
    
    public Class690_Sub10(Class534_Sub35 class534_sub35) {
	super(class534_sub35);
    }
    
    public Class690_Sub10(int i, Class534_Sub35 class534_sub35) {
	super(i, class534_sub35);
    }
    
    public int method16973() {
	return anInt8753 * 189295939;
    }
}
