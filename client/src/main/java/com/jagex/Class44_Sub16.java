/* Class44_Sub16 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class44_Sub16 extends Class44
{
    public Class44_Sub16(Class675 class675, Class672 class672,
			 Class472 class472) {
	super(class675, class672, class472, Class649.aClass649_8406, 64,
	      new Class61(com.jagex.Class651.class));
    }
    
    public static void method17357(byte i) {
	Class582.method9834((byte) 31);
	Class72.aBool758 = false;
	Class316.method5724(Class627.anInt8175 * -620506573,
			    Class72.anInt782 * -260575397,
			    -1739196959 * Class641.anInt8340,
			    -577412881 * Class327_Sub1.anInt9991, (byte) 1);
	Class251.aClass534_Sub18_Sub7_2653 = null;
	Class271.aClass534_Sub18_Sub7_2963 = null;
    }
    
    static Class580 method17358(Class534_Sub40 class534_sub40, int i) {
	int i_0_ = class534_sub40.method16527(-1633461010);
	int i_1_ = class534_sub40.method16527(-1256668241);
	int i_2_ = class534_sub40.method16527(1951622147);
	int[] is = new int[i_2_];
	for (int i_3_ = 0; i_3_ < i_2_; i_3_++)
	    is[i_3_] = class534_sub40.method16527(-1627494288);
	return new Class580(i_0_, i_1_, is);
    }
}
