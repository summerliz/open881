/* Class534_Sub37_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class534_Sub37_Sub1 extends Class534_Sub37
{
    int anInt11883;
    
    public boolean method16500() {
	Class534_Sub6 class534_sub6
	    = ((Class534_Sub6)
	       client.aClass9_11331.method579((long) (-1285007727
						      * anInt11883)));
	if (class534_sub6 != null) {
	    Class184.method3228(Class583.aClass583_7786,
				1225863589 * anInt10803, -1,
				((Class654_Sub1_Sub5_Sub1)
				 class534_sub6.anObject10417),
				-1285007727 * anInt11883, 1781521033);
	    return true;
	}
	return false;
    }
    
    public boolean method16499(byte i) {
	Class534_Sub6 class534_sub6
	    = ((Class534_Sub6)
	       client.aClass9_11331.method579((long) (-1285007727
						      * anInt11883)));
	if (class534_sub6 != null) {
	    Class184.method3228(Class583.aClass583_7786,
				1225863589 * anInt10803, -1,
				((Class654_Sub1_Sub5_Sub1)
				 class534_sub6.anObject10417),
				-1285007727 * anInt11883, 1781521033);
	    return true;
	}
	return false;
    }
    
    public Class534_Sub37_Sub1(int i, int i_0_, int i_1_) {
	super(i, i_0_);
	anInt11883 = i_1_ * 803089009;
    }
    
    public static int method18458(int i, int i_2_) {
	Class534_Sub18_Sub14 class534_sub18_sub14
	    = (Class534_Sub18_Sub14) Class274.aClass9_3036.method579((long) i);
	if (null == class534_sub18_sub14)
	    return -1;
	if (class534_sub18_sub14.aClass534_Sub18_10510
	    == Class274.aClass696_3038.aClass534_Sub18_8785)
	    return -1;
	return (((Class534_Sub18_Sub14)
		 class534_sub18_sub14.aClass534_Sub18_10510).anInt11808
		* -759944081);
    }
}
