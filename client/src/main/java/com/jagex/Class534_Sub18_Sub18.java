/* Class534_Sub18_Sub18 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public abstract class Class534_Sub18_Sub18 extends Class534_Sub18
{
    boolean aBool11886;
    boolean aBool11887;
    volatile boolean aBool11888 = true;
    
    abstract byte[] method18459();
    
    abstract byte[] method18460(short i);
    
    abstract int method18461(int i);
    
    abstract int method18462();
    
    abstract byte[] method18463();
    
    abstract byte[] method18464();
    
    Class534_Sub18_Sub18() {
	/* empty */
    }
    
    abstract int method18465();
    
    abstract int method18466();
}
