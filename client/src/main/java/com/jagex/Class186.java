/* Class186 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class186
{
    public boolean aBool2024;
    static final int anInt2025 = 0;
    static final int anInt2026 = 2;
    static final int anInt2027 = 3;
    public boolean aBool2028;
    static final int anInt2029 = 512;
    static final int anInt2030 = 2;
    static final int anInt2031 = 1;
    static final int anInt2032 = 16;
    static final int anInt2033 = 32;
    static final int anInt2034 = 64;
    static final int anInt2035 = 128;
    static final int anInt2036 = 256;
    public static final int anInt2037 = 1;
    public static final byte aByte2038 = 0;
    public static final byte aByte2039 = 1;
    static final int anInt2040 = 1;
    static final int anInt2041 = 2;
    public int anInt2042;
    public boolean aBool2043 = false;
    boolean aBool2044 = false;
    public boolean aBool2045 = false;
    public int anInt2046;
    public byte aByte2047 = 0;
    public byte aByte2048 = 0;
    public static final int anInt2049 = 5;
    public static final int anInt2050 = 0;
    public byte aByte2051;
    public static final int anInt2052 = 6;
    public byte aByte2053;
    public static final int anInt2054 = 9;
    static final int anInt2055 = 16;
    public static final int anInt2056 = 2;
    public static final int anInt2057 = 3;
    public static final int anInt2058 = 4;
    public byte aByte2059;
    public short aShort2060;
    public static final int anInt2061 = 7;
    public static final int anInt2062 = 8;
    static final int anInt2063 = 1;
    public Class599 aClass599_2064;
    public static final int anInt2065 = 11;
    public static final int anInt2066 = 16;
    public byte aByte2067;
    public byte aByte2068;
    public int anInt2069;
    public byte aByte2070;
    static final int anInt2071 = 4;
    public boolean aBool2072;
    public byte aByte2073;
    public byte aByte2074;
    public byte aByte2075;
    public static final int anInt2076 = 13;
    
    public static boolean method3696(int i) {
	return i != 1 && i != 7;
    }
    
    void method3697(int i, Class534_Sub40 class534_sub40, short i_0_) {
	anInt2042 = i * -1380370465;
	class534_sub40.method16527(-445040664);
	int i_1_ = class534_sub40.method16527(-726142729);
	if (0 == i_1_)
	    anInt2046 = -1182452288;
	else if (i_1_ == 1)
	    anInt2046 = 1930062720;
	else if (2 == i_1_)
	    anInt2046 = -434841856;
	else if (3 == i_1_)
	    anInt2046 = -869683712;
	else if (4 == i_1_)
	    anInt2046 = -1739367424;
	int i_2_ = class534_sub40.method16533(-258848859);
	aBool2043 = (i_2_ & 0x1) != 0;
	aBool2044 = 0 != (i_2_ & 0x2);
	aBool2045 = 0 != (i_2_ & 0x10);
	int i_3_ = (byte) class534_sub40.method16527(-1570416012);
	aByte2073 = (byte) (i_3_ & 0x7);
	aByte2053 = (byte) (i_3_ >> 3 & 0x7);
	int i_4_ = class534_sub40.method16533(-258848859);
	if ((i_4_ & 0x10) != 0) {
	    class534_sub40.method16539(-1574986781);
	    class534_sub40.method16539(-1740555032);
	}
	if (aBool2044)
	    class534_sub40.method16539(-1280543206);
	if (0 != (i_4_ & 0x20))
	    class534_sub40.method16533(-258848859);
	if (0 != (i_4_ & 0x40))
	    class534_sub40.method16533(-258848859);
	if ((i_4_ & 0x80) != 0)
	    class534_sub40.method16533(-258848859);
	if ((i_4_ & 0x100) != 0)
	    class534_sub40.method16533(-258848859);
	if ((i_4_ & 0x200) != 0)
	    class534_sub40.method16533(-258848859);
	aBool2024 = class534_sub40.method16527(-894772225) == 1;
	Class448.method7319(Class281.method5238(868321672),
			    class534_sub40.method16527(-558298335),
			    2088438307);
	Class448.method7319(Class454.method7414(-1351168887),
			    class534_sub40.method16527(1871396952),
			    2088438307);
	aClass599_2064
	    = ((Class599)
	       Class448.method7319(Class384.method6477(1846941648),
				   class534_sub40.method16527(1302497671),
				   2088438307));
	if (Class599.aClass599_7868 == aClass599_2064)
	    aByte2051 = (byte) class534_sub40.method16527(1532701647);
	int i_5_ = class534_sub40.method16527(-440302594);
	if (0 != (i_5_ & 0x1))
	    aByte2047 = (byte) class534_sub40.method16527(-1462339145);
	if (0 != (i_5_ & 0x2))
	    aByte2048 = (byte) class534_sub40.method16527(332539497);
	if (class534_sub40.method16527(976225848) == 1) {
	    aByte2067 = (byte) class534_sub40.method16527(-834124283);
	    aByte2068 = (byte) class534_sub40.method16527(-2123577700);
	    anInt2069 = class534_sub40.method16533(-258848859) * 942393301;
	    aByte2075 = (byte) class534_sub40.method16527(-1348502976);
	    class534_sub40.method16527(-97118553);
	    aByte2070 = (byte) class534_sub40.method16527(1654780550);
	    aBool2028 = class534_sub40.method16527(-985560356) == 1;
	    aBool2072 = class534_sub40.method16527(-1942693621) == 1;
	    aByte2059 = (byte) class534_sub40.method16527(-687761446);
	    aByte2074 = (byte) class534_sub40.method16527(-699002204);
	    aShort2060 = (short) class534_sub40.method16529((byte) 1);
	}
    }
    
    Class186() {
	aBool2024 = false;
	aClass599_2064 = Class599.aClass599_7869;
	aByte2051 = (byte) -1;
	aByte2073 = (byte) 0;
	aByte2053 = (byte) 0;
	aByte2067 = (byte) 0;
	aByte2068 = (byte) 0;
	anInt2069 = 0;
	aByte2070 = (byte) 0;
	aBool2028 = false;
	aBool2072 = false;
	aByte2059 = (byte) 0;
	aByte2074 = (byte) -1;
	aByte2075 = (byte) 0;
	aShort2060 = (short) 0;
    }
    
    public static boolean method3698(int i) {
	return i != 1 && i != 7;
    }
    
    void method3699(int i, Class534_Sub40 class534_sub40) {
	anInt2042 = i * -1380370465;
	class534_sub40.method16527(-1858642273);
	int i_6_ = class534_sub40.method16527(2095328145);
	if (0 == i_6_)
	    anInt2046 = -1182452288;
	else if (i_6_ == 1)
	    anInt2046 = 1930062720;
	else if (2 == i_6_)
	    anInt2046 = -434841856;
	else if (3 == i_6_)
	    anInt2046 = -869683712;
	else if (4 == i_6_)
	    anInt2046 = -1739367424;
	int i_7_ = class534_sub40.method16533(-258848859);
	aBool2043 = (i_7_ & 0x1) != 0;
	aBool2044 = 0 != (i_7_ & 0x2);
	aBool2045 = 0 != (i_7_ & 0x10);
	int i_8_ = (byte) class534_sub40.method16527(1580396002);
	aByte2073 = (byte) (i_8_ & 0x7);
	aByte2053 = (byte) (i_8_ >> 3 & 0x7);
	int i_9_ = class534_sub40.method16533(-258848859);
	if ((i_9_ & 0x10) != 0) {
	    class534_sub40.method16539(-1260642819);
	    class534_sub40.method16539(-1022075342);
	}
	if (aBool2044)
	    class534_sub40.method16539(-1777484726);
	if (0 != (i_9_ & 0x20))
	    class534_sub40.method16533(-258848859);
	if (0 != (i_9_ & 0x40))
	    class534_sub40.method16533(-258848859);
	if ((i_9_ & 0x80) != 0)
	    class534_sub40.method16533(-258848859);
	if ((i_9_ & 0x100) != 0)
	    class534_sub40.method16533(-258848859);
	if ((i_9_ & 0x200) != 0)
	    class534_sub40.method16533(-258848859);
	aBool2024 = class534_sub40.method16527(998654289) == 1;
	Class448.method7319(Class281.method5238(348609397),
			    class534_sub40.method16527(1108625499),
			    2088438307);
	Class448.method7319(Class454.method7414(-1480181527),
			    class534_sub40.method16527(160872814), 2088438307);
	aClass599_2064
	    = ((Class599)
	       Class448.method7319(Class384.method6477(-845862710),
				   class534_sub40.method16527(-415989733),
				   2088438307));
	if (Class599.aClass599_7868 == aClass599_2064)
	    aByte2051 = (byte) class534_sub40.method16527(-900688559);
	int i_10_ = class534_sub40.method16527(1409496790);
	if (0 != (i_10_ & 0x1))
	    aByte2047 = (byte) class534_sub40.method16527(-374590707);
	if (0 != (i_10_ & 0x2))
	    aByte2048 = (byte) class534_sub40.method16527(1863963445);
	if (class534_sub40.method16527(-2041230539) == 1) {
	    aByte2067 = (byte) class534_sub40.method16527(1057667879);
	    aByte2068 = (byte) class534_sub40.method16527(-739181656);
	    anInt2069 = class534_sub40.method16533(-258848859) * 942393301;
	    aByte2075 = (byte) class534_sub40.method16527(1132978990);
	    class534_sub40.method16527(730312545);
	    aByte2070 = (byte) class534_sub40.method16527(-2115960141);
	    aBool2028 = class534_sub40.method16527(-145509558) == 1;
	    aBool2072 = class534_sub40.method16527(1552858453) == 1;
	    aByte2059 = (byte) class534_sub40.method16527(1215449095);
	    aByte2074 = (byte) class534_sub40.method16527(-779808461);
	    aShort2060 = (short) class534_sub40.method16529((byte) 1);
	}
    }
    
    public static void method3700(String string, int i) {
	Class272.method5067(0, 0, "", "", "", string, null, (byte) 5);
    }
    
    static final void method3701(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class641.method10600(class247, class243, class669, (byte) 100);
    }
    
    static final void method3702(Class669 class669, byte i)
	throws Exception_Sub2 {
	int i_11_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class599.aClass298_Sub1_7871.method5455
	    (Class255.method4645(i_11_, 2115120767), true, (byte) 15);
	client.aBool11147 = true;
    }
    
    static final void method3703(Class669 class669, byte i) {
	int i_12_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	if (2 == -1050164879 * client.anInt11171
	    && i_12_ < -1979292205 * client.anInt11324)
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= client.aClass28Array11327[i_12_].aBool255 ? 1 : 0;
	else
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= 0;
    }
    
    public static final void method3704(int i) {
	if (!client.aBool11132) {
	    client.aFloat11141 += (-24.0F - client.aFloat11141) / 2.0F;
	    client.aBool11147 = true;
	    client.aBool11132 = true;
	}
    }
    
    static final void method3705(Class247 class247, Class243 class243,
				 Class669 class669, int i) {
	class669.anInt8600 -= 1235998252;
	class247.anInt2456 = -1429993067 * (class669.anIntArray8595
					    [class669.anInt8600 * 2088438307]);
	class247.anInt2465
	    = (class669.anIntArray8595[1 + class669.anInt8600 * 2088438307]
	       * 2088825989);
	class247.anInt2594 = 0;
	class247.anInt2510 = 0;
	int i_13_
	    = class669.anIntArray8595[2 + 2088438307 * class669.anInt8600];
	if (i_13_ < 0)
	    i_13_ = 0;
	else if (i_13_ > 4)
	    i_13_ = 4;
	int i_14_
	    = class669.anIntArray8595[2088438307 * class669.anInt8600 + 3];
	if (i_14_ < 0)
	    i_14_ = 0;
	else if (i_14_ > 4)
	    i_14_ = 4;
	class247.aByte2460 = (byte) i_13_;
	class247.aByte2448 = (byte) i_14_;
	Class454.method7416(class247, -963428534);
	Class519.method8649(class243, class247, -264598033);
	if (class247.anInt2438 * -1960530827 == 0)
	    Class606.method10054(class243, class247, false, -1190959835);
    }
}
