/* Class399 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class399 implements Interface72
{
    public String method490(Class640 class640, int[] is, long l) {
	if (class640 == Class640.aClass640_8320) {
	    Class41 class41
		= ((Class41)
		   Class667.aClass44_Sub21_8582.method91(is[0], -351694328));
	    return class41.method1040((int) l, (byte) -88);
	}
	if (class640 == Class640.aClass640_8323
	    || Class640.aClass640_8326 == class640) {
	    Class15 class15
		= ((Class15)
		   Class531.aClass44_Sub7_7135.method91((int) l, 389034017));
	    return class15.aString122;
	}
	if (Class640.aClass640_8318 == class640
	    || Class640.aClass640_8332 == class640
	    || Class640.aClass640_8327 == class640
	    || Class640.aClass640_8325 == class640)
	    return ((Class41)
		    Class667.aClass44_Sub21_8582.method91(is[0], 921693612))
		       .method1040((int) l, (byte) 8);
	return null;
    }
    
    public String method488(Class640 class640, int[] is, long l) {
	if (class640 == Class640.aClass640_8320) {
	    Class41 class41
		= ((Class41)
		   Class667.aClass44_Sub21_8582.method91(is[0], 953858341));
	    return class41.method1040((int) l, (byte) 17);
	}
	if (class640 == Class640.aClass640_8323
	    || Class640.aClass640_8326 == class640) {
	    Class15 class15
		= ((Class15)
		   Class531.aClass44_Sub7_7135.method91((int) l, 1039792740));
	    return class15.aString122;
	}
	if (Class640.aClass640_8318 == class640
	    || Class640.aClass640_8332 == class640
	    || Class640.aClass640_8327 == class640
	    || Class640.aClass640_8325 == class640)
	    return ((Class41)
		    Class667.aClass44_Sub21_8582.method91(is[0], -1703593675))
		       .method1040((int) l, (byte) 27);
	return null;
    }
    
    public String method489(Class640 class640, int[] is, long l) {
	if (class640 == Class640.aClass640_8320) {
	    Class41 class41
		= ((Class41)
		   Class667.aClass44_Sub21_8582.method91(is[0], -1297299879));
	    return class41.method1040((int) l, (byte) -34);
	}
	if (class640 == Class640.aClass640_8323
	    || Class640.aClass640_8326 == class640) {
	    Class15 class15
		= ((Class15)
		   Class531.aClass44_Sub7_7135.method91((int) l, -1054495750));
	    return class15.aString122;
	}
	if (Class640.aClass640_8318 == class640
	    || Class640.aClass640_8332 == class640
	    || Class640.aClass640_8327 == class640
	    || Class640.aClass640_8325 == class640)
	    return ((Class41)
		    Class667.aClass44_Sub21_8582.method91(is[0], -1763867056))
		       .method1040((int) l, (byte) -6);
	return null;
    }
    
    public String method487(Class640 class640, int[] is, long l) {
	if (class640 == Class640.aClass640_8320) {
	    Class41 class41
		= ((Class41)
		   Class667.aClass44_Sub21_8582.method91(is[0], -1032047108));
	    return class41.method1040((int) l, (byte) 19);
	}
	if (class640 == Class640.aClass640_8323
	    || Class640.aClass640_8326 == class640) {
	    Class15 class15
		= ((Class15)
		   Class531.aClass44_Sub7_7135.method91((int) l, -1608596862));
	    return class15.aString122;
	}
	if (Class640.aClass640_8318 == class640
	    || Class640.aClass640_8332 == class640
	    || Class640.aClass640_8327 == class640
	    || Class640.aClass640_8325 == class640)
	    return ((Class41)
		    Class667.aClass44_Sub21_8582.method91(is[0], -1097326669))
		       .method1040((int) l, (byte) -73);
	return null;
    }
    
    static int method6581(CharSequence charsequence, char c, int i) {
	int i_0_ = 0;
	int i_1_ = charsequence.length();
	for (int i_2_ = 0; i_2_ < i_1_; i_2_++) {
	    if (charsequence.charAt(i_2_) == c)
		i_0_++;
	}
	return i_0_;
    }
}
