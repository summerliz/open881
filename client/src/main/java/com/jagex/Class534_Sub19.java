/* Class534_Sub19 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class534_Sub19 extends Class534
{
    public int anInt10511;
    Class404 aClass404_10512;
    public Class534_Sub40_Sub1 aClass534_Sub40_Sub1_10513;
    static Class534_Sub19[] aClass534_Sub19Array10514
	= new Class534_Sub19[300];
    static int anInt10515 = 0;
    int anInt10516;
    
    public static Class534_Sub19 method16182(Class404 class404,
					     IsaacCipher class13) {
	Class534_Sub19 class534_sub19 = Class670.method11042(-386371654);
	class534_sub19.aClass404_10512 = class404;
	class534_sub19.anInt10516 = 182467999 * class404.anInt4277;
	if (-1 == 393990279 * class534_sub19.anInt10516)
	    class534_sub19.aClass534_Sub40_Sub1_10513
		= new Class534_Sub40_Sub1(260);
	else if (class534_sub19.anInt10516 * 393990279 == -2)
	    class534_sub19.aClass534_Sub40_Sub1_10513
		= new Class534_Sub40_Sub1(10000);
	else if (393990279 * class534_sub19.anInt10516 <= 18)
	    class534_sub19.aClass534_Sub40_Sub1_10513
		= new Class534_Sub40_Sub1(20);
	else if (class534_sub19.anInt10516 * 393990279 <= 98)
	    class534_sub19.aClass534_Sub40_Sub1_10513
		= new Class534_Sub40_Sub1(100);
	else
	    class534_sub19.aClass534_Sub40_Sub1_10513
		= new Class534_Sub40_Sub1(260);
	class534_sub19.aClass534_Sub40_Sub1_10513.method18316(class13,
							      163694688);
	class534_sub19.aClass534_Sub40_Sub1_10513.method18306
	    (145532973 * class534_sub19.aClass404_10512.anInt4276, -326307091);
	class534_sub19.anInt10511 = 0;
	return class534_sub19;
    }
    
    public static Class534_Sub19 method16183() {
	Class534_Sub19 class534_sub19 = Class670.method11042(-1681099239);
	class534_sub19.aClass404_10512 = null;
	class534_sub19.anInt10516 = 0;
	class534_sub19.aClass534_Sub40_Sub1_10513
	    = new Class534_Sub40_Sub1(5000);
	return class534_sub19;
    }
    
    Class534_Sub19() {
	/* empty */
    }
    
    public void method16184(int i) {
	if (-1174729025 * anInt10515 < aClass534_Sub19Array10514.length)
	    aClass534_Sub19Array10514
		[(anInt10515 += -1902157505) * -1174729025 - 1]
		= this;
    }
    
    public static Class534_Sub19 method16185(Class404 class404,
					     IsaacCipher class13) {
	Class534_Sub19 class534_sub19 = Class670.method11042(1553925808);
	class534_sub19.aClass404_10512 = class404;
	class534_sub19.anInt10516 = 182467999 * class404.anInt4277;
	if (-1 == 393990279 * class534_sub19.anInt10516)
	    class534_sub19.aClass534_Sub40_Sub1_10513
		= new Class534_Sub40_Sub1(260);
	else if (class534_sub19.anInt10516 * 393990279 == -2)
	    class534_sub19.aClass534_Sub40_Sub1_10513
		= new Class534_Sub40_Sub1(10000);
	else if (393990279 * class534_sub19.anInt10516 <= 18)
	    class534_sub19.aClass534_Sub40_Sub1_10513
		= new Class534_Sub40_Sub1(20);
	else if (class534_sub19.anInt10516 * 393990279 <= 98)
	    class534_sub19.aClass534_Sub40_Sub1_10513
		= new Class534_Sub40_Sub1(100);
	else
	    class534_sub19.aClass534_Sub40_Sub1_10513
		= new Class534_Sub40_Sub1(260);
	class534_sub19.aClass534_Sub40_Sub1_10513.method18316(class13,
							      163694688);
	class534_sub19.aClass534_Sub40_Sub1_10513.method18306
	    (145532973 * class534_sub19.aClass404_10512.anInt4276,
	     -1477274239);
	class534_sub19.anInt10511 = 0;
	return class534_sub19;
    }
    
    public static Class534_Sub19 method16186(Class404 class404,
					     IsaacCipher class13) {
	Class534_Sub19 class534_sub19 = Class670.method11042(94768967);
	class534_sub19.aClass404_10512 = class404;
	class534_sub19.anInt10516 = 182467999 * class404.anInt4277;
	if (-1 == 393990279 * class534_sub19.anInt10516)
	    class534_sub19.aClass534_Sub40_Sub1_10513
		= new Class534_Sub40_Sub1(260);
	else if (class534_sub19.anInt10516 * 393990279 == -2)
	    class534_sub19.aClass534_Sub40_Sub1_10513
		= new Class534_Sub40_Sub1(10000);
	else if (393990279 * class534_sub19.anInt10516 <= 18)
	    class534_sub19.aClass534_Sub40_Sub1_10513
		= new Class534_Sub40_Sub1(20);
	else if (class534_sub19.anInt10516 * 393990279 <= 98)
	    class534_sub19.aClass534_Sub40_Sub1_10513
		= new Class534_Sub40_Sub1(100);
	else
	    class534_sub19.aClass534_Sub40_Sub1_10513
		= new Class534_Sub40_Sub1(260);
	class534_sub19.aClass534_Sub40_Sub1_10513.method18316(class13,
							      163694688);
	class534_sub19.aClass534_Sub40_Sub1_10513.method18306
	    (145532973 * class534_sub19.aClass404_10512.anInt4276, -902037687);
	class534_sub19.anInt10511 = 0;
	return class534_sub19;
    }
    
    public static Class534_Sub19 method16187() {
	Class534_Sub19 class534_sub19 = Class670.method11042(-346456522);
	class534_sub19.aClass404_10512 = null;
	class534_sub19.anInt10516 = 0;
	class534_sub19.aClass534_Sub40_Sub1_10513
	    = new Class534_Sub40_Sub1(5000);
	return class534_sub19;
    }
    
    public static Class534_Sub19 method16188() {
	Class534_Sub19 class534_sub19 = Class670.method11042(771672282);
	class534_sub19.aClass404_10512 = null;
	class534_sub19.anInt10516 = 0;
	class534_sub19.aClass534_Sub40_Sub1_10513
	    = new Class534_Sub40_Sub1(5000);
	return class534_sub19;
    }
    
    static Class534_Sub19 method16189() {
	if (0 == anInt10515 * -1174729025)
	    return new Class534_Sub19();
	return (aClass534_Sub19Array10514
		[(anInt10515 -= -1902157505) * -1174729025]);
    }
    
    public void method16190() {
	if (-1174729025 * anInt10515 < aClass534_Sub19Array10514.length)
	    aClass534_Sub19Array10514
		[(anInt10515 += -1902157505) * -1174729025 - 1]
		= this;
    }
    
    static final void method16191(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = -87431073 * Class550.anInt7307;
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class567.anInt7595 * 114918783;
    }
}
