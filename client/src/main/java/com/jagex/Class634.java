/* Class634 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class634
{
    float aFloat8279;
    float aFloat8280;
    float aFloat8281;
    float aFloat8282;
    int[] anIntArray8283;
    int anInt8284;
    Class438 aClass438_8285;
    Class165 aClass165_8286;
    float aFloat8287;
    float aFloat8288;
    int anInt8289;
    Class505 aClass505_8290;
    float aFloat8291 = 1.0F;
    int anInt8292;
    float aFloat8293;
    float aFloat8294;
    float aFloat8295;
    float aFloat8296;
    float[] aFloatArray8297;
    
    public int method10500() {
	return 1102104287 * anInt8284;
    }
    
    public Class634(Class534_Sub40 class534_sub40, Class616 class616) {
	aFloat8279 = 0.0F;
	aFloat8293 = 1.0F;
	aFloat8294 = 0.0F;
	aFloat8295 = 1.0F;
	anIntArray8283 = new int[] { -1, -1, -1 };
	aFloatArray8297 = new float[] { 0.0F, 0.0F, 0.0F };
	method10521(class534_sub40, class616, 1424011373);
    }
    
    void method10501(byte i) {
	anInt8289 = -1879224663;
	aClass438_8285 = Class438.method6996(-50.0F, -60.0F, -50.0F);
	aFloat8280 = 1.1523438F;
	aFloat8281 = 0.69921875F;
	aFloat8282 = 1.2F;
	anInt8284 = -969139112;
	anInt8292 = 0;
	aClass165_8286 = Class616.aClass165_8057;
	aFloat8287 = 1.0F;
	aFloat8288 = 0.25F;
	aFloat8296 = 1.0F;
	aClass505_8290 = Class616.aClass505_8058;
	aFloat8291 = 1.0F;
	aFloat8279 = 0.0F;
	aFloat8293 = 1.0F;
	aFloat8294 = 0.0F;
	aFloat8295 = 1.0F;
	int[] is = anIntArray8283;
	int[] is_0_ = anIntArray8283;
	anIntArray8283[2] = -1;
	is_0_[1] = -1;
	is[0] = -1;
	float[] fs = aFloatArray8297;
	float[] fs_1_ = aFloatArray8297;
	aFloatArray8297[2] = 0.0F;
	fs_1_[1] = 0.0F;
	fs[0] = 0.0F;
    }
    
    void method10502(Class634 class634_2_, byte i) {
	anInt8289 = class634_2_.anInt8289 * 1;
	aFloat8280 = class634_2_.aFloat8280;
	aFloat8281 = class634_2_.aFloat8281;
	aFloat8282 = class634_2_.aFloat8282;
	aClass438_8285.method6992(class634_2_.aClass438_8285);
	anInt8284 = 1 * class634_2_.anInt8284;
	anInt8292 = 1 * class634_2_.anInt8292;
	aClass165_8286 = class634_2_.aClass165_8286;
	aFloat8287 = class634_2_.aFloat8287;
	aFloat8288 = class634_2_.aFloat8288;
	aFloat8296 = class634_2_.aFloat8296;
	aClass505_8290 = class634_2_.aClass505_8290;
	aFloat8291 = class634_2_.aFloat8291;
	aFloat8279 = class634_2_.aFloat8279;
	aFloat8293 = class634_2_.aFloat8293;
	aFloat8294 = class634_2_.aFloat8294;
	aFloat8295 = class634_2_.aFloat8295;
	anIntArray8283[0] = class634_2_.anIntArray8283[0];
	anIntArray8283[1] = class634_2_.anIntArray8283[1];
	anIntArray8283[2] = class634_2_.anIntArray8283[2];
	aFloatArray8297[0] = class634_2_.aFloatArray8297[0];
	aFloatArray8297[1] = class634_2_.aFloatArray8297[1];
	aFloatArray8297[2] = class634_2_.aFloatArray8297[2];
    }
    
    void method10503(Class616 class616, Class653 class653) {
	if (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub12_10753
		.method16985(16711680) == 1
	    && Class254.aClass185_2683.method3344() > 0) {
	    if (class653.method10742(-922632465) != -1)
		anInt8289 = class653.method10742(-1230716968) * -956124841;
	    if (class653.method10788(-599223845) != -1.0F)
		aFloat8280 = class653.method10788(-209668529);
	    if (class653.method10744((short) 18748) != -1.0F)
		aFloat8281 = class653.method10744((short) 32733);
	    if (class653.method10795((byte) 30) != -1.0F)
		aFloat8282 = class653.method10795((byte) 63);
	}
	if (class653.method10745((byte) 5) != null)
	    aClass438_8285.method6992(class653.method10745((byte) 5));
	if (class653.method10747((byte) -71) != -1)
	    anInt8284 = class653.method10747((byte) -90) * 432829727;
	if (class653.method10748((byte) -78) != -1)
	    anInt8292 = class653.method10748((byte) -20) * 2005399217;
	if (class653.method10781(1059273714) != -1.0F)
	    aFloat8288 = class653.method10781(720797935);
	if (class653.method10750(563822280) != -1.0F)
	    aFloat8296 = class653.method10750(-157575629);
	if (class653.method10770(-1424495220) != -1.0F)
	    aFloat8287 = class653.method10770(-1317976514);
	if (class653.method10752((byte) 2) != -1)
	    aClass165_8286
		= class616.method10148(class653.method10752((byte) 2),
				       -967325485);
	if (class653.method10753((byte) 0) != -1) {
	    int i = class653.method10753((byte) 0);
	    int i_3_ = class653.method10754(-1243052412);
	    int i_4_ = class653.method10759(-938602392);
	    int i_5_ = class653.method10756((byte) -99);
	    int i_6_ = class653.method10757(1009289266);
	    Class12.anInt92 = -1037919377 * i_6_;
	    aClass505_8290
		= class616.method10149(i, i_3_, i_4_, i_5_, (byte) 41);
	}
	if (class653.method10758(0, -578167900) != -1) {
	    anIntArray8283[0] = class653.method10758(0, -1052341316);
	    aFloatArray8297[0] = class653.method10755(0, 1750067212);
	}
	if (class653.method10758(1, -2008631076) != -1) {
	    anIntArray8283[1] = class653.method10758(1, -807453154);
	    aFloatArray8297[1] = class653.method10755(1, 1915781415);
	}
	if (class653.method10758(2, -416558485) != -1) {
	    anIntArray8283[2] = class653.method10758(2, -247586115);
	    aFloatArray8297[2] = class653.method10755(2, 1735742086);
	}
    }
    
    void method10504(Class185 class185, Class634 class634_7_,
		     Class634 class634_8_, float f, int i) {
	anInt8289 = Class69.method1396(-1201354137 * class634_7_.anInt8289,
				       class634_8_.anInt8289 * -1201354137,
				       255.0F * f, (byte) -66) * -956124841;
	aFloat8281 = ((class634_8_.aFloat8281 - class634_7_.aFloat8281) * f
		      + class634_7_.aFloat8281);
	aFloat8282 = ((class634_8_.aFloat8282 - class634_7_.aFloat8282) * f
		      + class634_7_.aFloat8282);
	aFloat8280 = (class634_7_.aFloat8280
		      + (class634_8_.aFloat8280 - class634_7_.aFloat8280) * f);
	aFloat8296 = (class634_7_.aFloat8296
		      + (class634_8_.aFloat8296 - class634_7_.aFloat8296) * f);
	aFloat8287 = (class634_7_.aFloat8287
		      + (class634_8_.aFloat8287 - class634_7_.aFloat8287) * f);
	aFloat8288 = class634_7_.aFloat8288 + f * (class634_8_.aFloat8288
						   - class634_7_.aFloat8288);
	anInt8284 = Class69.method1396(class634_7_.anInt8284 * 1102104287,
				       1102104287 * class634_8_.anInt8284,
				       f * 255.0F, (byte) 3) * 432829727;
	anInt8292 = ((int) ((float) (class634_8_.anInt8292 * -563441071
				     - -563441071 * class634_7_.anInt8292) * f
			    + (float) (class634_7_.anInt8292 * -563441071))
		     * 2005399217);
	if (class634_8_.aClass165_8286 != class634_7_.aClass165_8286)
	    aClass165_8286 = class185.method3451(class634_7_.aClass165_8286,
						 class634_8_.aClass165_8286, f,
						 aClass165_8286);
	if (class634_7_.aClass505_8290 != class634_8_.aClass505_8290) {
	    if (class634_7_.aClass505_8290 == null) {
		aClass505_8290 = class634_8_.aClass505_8290;
		if (aClass505_8290 != null)
		    aClass505_8290.method8328((int) (f * 255.0F), 0,
					      -541311282);
	    } else {
		aClass505_8290 = class634_7_.aClass505_8290;
		if (null != aClass505_8290)
		    aClass505_8290.method8328((int) (255.0F * f), 255,
					      -541311282);
	    }
	}
	aFloat8291 = class634_7_.aFloat8291 + f * (class634_8_.aFloat8291
						   - class634_7_.aFloat8291);
	aFloat8279 = ((class634_8_.aFloat8279 - class634_7_.aFloat8279) * f
		      + class634_7_.aFloat8279);
	aFloat8293 = ((class634_8_.aFloat8293 - class634_7_.aFloat8293) * f
		      + class634_7_.aFloat8293);
	aFloat8294 = ((class634_8_.aFloat8294 - class634_7_.aFloat8294) * f
		      + class634_7_.aFloat8294);
	aFloat8295 = ((class634_8_.aFloat8295 - class634_7_.aFloat8295) * f
		      + class634_7_.aFloat8295);
	float f_9_
	    = (class634_7_.aFloatArray8297[0] + class634_7_.aFloatArray8297[1]
	       + class634_7_.aFloatArray8297[2]);
	float f_10_ = (class634_8_.aFloatArray8297[2]
		       + (class634_8_.aFloatArray8297[1]
			  + class634_8_.aFloatArray8297[0]));
	float f_11_ = (f_10_ - f_9_) * f + f_9_;
	if (0.0F == f_11_) {
	    int[] is = anIntArray8283;
	    int[] is_12_ = anIntArray8283;
	    anIntArray8283[2] = -1;
	    is_12_[1] = -1;
	    is[0] = -1;
	    float[] fs = aFloatArray8297;
	    float[] fs_13_ = aFloatArray8297;
	    aFloatArray8297[2] = 0.0F;
	    fs_13_[1] = 0.0F;
	    fs[0] = 0.0F;
	} else if (class634_7_.anIntArray8283[0] == -1
		   && class634_7_.anIntArray8283[1] == -1
		   && -1 == class634_7_.anIntArray8283[2]) {
	    for (int i_14_ = 0; i_14_ < 3; i_14_++) {
		anIntArray8283[i_14_] = class634_8_.anIntArray8283[i_14_];
		if (-1 == anIntArray8283[i_14_])
		    aFloatArray8297[i_14_] = 0.0F;
		else
		    aFloatArray8297[i_14_]
			= class634_8_.aFloatArray8297[i_14_] * f;
	    }
	} else if (-1 == class634_8_.anIntArray8283[0]
		   && class634_8_.anIntArray8283[1] == -1
		   && -1 == class634_8_.anIntArray8283[2]) {
	    for (int i_15_ = 0; i_15_ < 3; i_15_++) {
		anIntArray8283[i_15_] = class634_7_.anIntArray8283[i_15_];
		if (anIntArray8283[i_15_] == -1)
		    aFloatArray8297[i_15_] = 0.0F;
		else
		    aFloatArray8297[i_15_]
			= class634_7_.aFloatArray8297[i_15_] * (1.0F - f);
	    }
	} else {
	    float f_16_ = 1.0F - f;
	    int i_17_ = 0;
	    int[] is = { -1, -1, -1, -1, -1, -1 };
	    float[] fs = { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F };
	    for (int i_18_ = 0; i_18_ < 3; i_18_++) {
		if (class634_7_.anIntArray8283[i_18_] > -1) {
		    is[i_17_] = class634_7_.anIntArray8283[i_18_];
		    fs[i_17_++] = class634_7_.aFloatArray8297[i_18_] * f_16_;
		}
	    }
	    int i_19_ = i_17_;
	    for (int i_20_ = 0; i_20_ < 3; i_20_++) {
		if (class634_8_.anIntArray8283[i_20_] > -1) {
		    float f_21_ = class634_8_.aFloatArray8297[i_20_] * f;
		    for (int i_22_ = 0; i_22_ < i_19_; i_22_++) {
			if (is[i_22_] == class634_8_.anIntArray8283[i_20_]) {
			    fs[i_22_] += f_21_;
			    break;
			}
			if (i_22_ == i_19_ - 1) {
			    is[i_17_] = class634_8_.anIntArray8283[i_20_];
			    fs[i_17_++] = f_21_;
			}
		    }
		}
	    }
	    if (i_17_ > 3) {
		float f_23_ = 0.0F;
		float f_24_ = 0.0F;
		for (int i_25_ = 0; i_25_ < i_17_; i_25_++)
		    f_23_ += fs[i_25_];
		Class592.method9884(fs, is, 0, i_17_ - 1, 2066800357);
		for (int i_26_ = 0; i_26_ < 3; i_26_++)
		    f_24_ += fs[i_26_];
		float f_27_ = f_23_ / f_24_;
		for (int i_28_ = 0; i_28_ < 3; i_28_++)
		    fs[i_28_] *= f_27_;
	    }
	    for (int i_29_ = 0; i_29_ < 3; i_29_++) {
		anIntArray8283[i_29_] = is[i_29_];
		aFloatArray8297[i_29_] = fs[i_29_];
	    }
	}
    }
    
    public void method10505(Class534_Sub40 class534_sub40, int i) {
	aFloat8287 = class534_sub40.method16539(-2118037799);
	aFloat8288 = class534_sub40.method16539(-1648270754);
	aFloat8296 = class534_sub40.method16539(-1554801199);
    }
    
    void method10506(Class616 class616, Class653 class653) {
	if (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub12_10753
		.method16985(16711680) == 1
	    && Class254.aClass185_2683.method3344() > 0) {
	    if (class653.method10742(-1625350703) != -1)
		anInt8289 = class653.method10742(-1173956380) * -956124841;
	    if (class653.method10788(-746452044) != -1.0F)
		aFloat8280 = class653.method10788(-65049728);
	    if (class653.method10744((short) 30073) != -1.0F)
		aFloat8281 = class653.method10744((short) 20116);
	    if (class653.method10795((byte) 70) != -1.0F)
		aFloat8282 = class653.method10795((byte) 90);
	}
	if (class653.method10745((byte) 5) != null)
	    aClass438_8285.method6992(class653.method10745((byte) 5));
	if (class653.method10747((byte) -67) != -1)
	    anInt8284 = class653.method10747((byte) -1) * 432829727;
	if (class653.method10748((byte) -53) != -1)
	    anInt8292 = class653.method10748((byte) -84) * 2005399217;
	if (class653.method10781(1502096354) != -1.0F)
	    aFloat8288 = class653.method10781(1742372772);
	if (class653.method10750(-687556928) != -1.0F)
	    aFloat8296 = class653.method10750(-1190340962);
	if (class653.method10770(-1768292984) != -1.0F)
	    aFloat8287 = class653.method10770(-1863990896);
	if (class653.method10752((byte) 2) != -1)
	    aClass165_8286
		= class616.method10148(class653.method10752((byte) 2),
				       -1097130377);
	if (class653.method10753((byte) 0) != -1) {
	    int i = class653.method10753((byte) 0);
	    int i_30_ = class653.method10754(-1243052412);
	    int i_31_ = class653.method10759(1663357694);
	    int i_32_ = class653.method10756((byte) -115);
	    int i_33_ = class653.method10757(53743915);
	    Class12.anInt92 = -1037919377 * i_33_;
	    aClass505_8290
		= class616.method10149(i, i_30_, i_31_, i_32_, (byte) 13);
	}
	if (class653.method10758(0, -1448210190) != -1) {
	    anIntArray8283[0] = class653.method10758(0, 229812974);
	    aFloatArray8297[0] = class653.method10755(0, 2093208053);
	}
	if (class653.method10758(1, -1513871379) != -1) {
	    anIntArray8283[1] = class653.method10758(1, -1528593068);
	    aFloatArray8297[1] = class653.method10755(1, 1849411137);
	}
	if (class653.method10758(2, -1867988912) != -1) {
	    anIntArray8283[2] = class653.method10758(2, -2080218181);
	    aFloatArray8297[2] = class653.method10755(2, 1802926373);
	}
    }
    
    public void method10507(Class534_Sub40 class534_sub40, Class616 class616) {
	int i = class534_sub40.method16527(-486194636);
	if (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub12_10753
		.method16985(16711680) == 1
	    && Class254.aClass185_2683.method3344() > 0) {
	    if (0 != (i & 522594405 * Class644.aClass644_8353.anInt8354))
		anInt8289
		    = class534_sub40.method16533(-258848859) * -956124841;
	    else
		anInt8289 = -1879224663;
	    if ((i & 522594405 * Class644.aClass644_8349.anInt8354) != 0)
		aFloat8280
		    = (float) class534_sub40.method16529((byte) 1) / 256.0F;
	    else
		aFloat8280 = 1.1523438F;
	    if (0 != (i & 522594405 * Class644.aClass644_8350.anInt8354))
		aFloat8281
		    = (float) class534_sub40.method16529((byte) 1) / 256.0F;
	    else
		aFloat8281 = 0.69921875F;
	    if (0 != (i & Class644.aClass644_8351.anInt8354 * 522594405))
		aFloat8282
		    = (float) class534_sub40.method16529((byte) 1) / 256.0F;
	    else
		aFloat8282 = 1.2F;
	} else {
	    if ((i & 522594405 * Class644.aClass644_8353.anInt8354) != 0)
		class534_sub40.method16533(-258848859);
	    if (0 != (i & Class644.aClass644_8349.anInt8354 * 522594405))
		class534_sub40.method16529((byte) 1);
	    if (0 != (i & 522594405 * Class644.aClass644_8350.anInt8354))
		class534_sub40.method16529((byte) 1);
	    if ((i & 522594405 * Class644.aClass644_8351.anInt8354) != 0)
		class534_sub40.method16529((byte) 1);
	    anInt8289 = -1879224663;
	    aFloat8282 = 1.2F;
	    aFloat8281 = 0.69921875F;
	    aFloat8280 = 1.1523438F;
	}
	if (0 != (i & 522594405 * Class644.aClass644_8352.anInt8354))
	    aClass438_8285
		= Class438.method6996((float) class534_sub40
						  .method16530((byte) -85),
				      (float) class534_sub40
						  .method16530((byte) -99),
				      (float) class534_sub40
						  .method16530((byte) -26));
	else
	    aClass438_8285 = Class438.method6996(-50.0F, -60.0F, -50.0F);
	if (0 != (i & 522594405 * Class644.aClass644_8360.anInt8354))
	    anInt8284 = class534_sub40.method16533(-258848859) * 432829727;
	else
	    anInt8284 = -969139112;
	if ((i & 522594405 * Class644.aClass644_8359.anInt8354) != 0)
	    anInt8292 = class534_sub40.method16529((byte) 1) * 2005399217;
	else
	    anInt8292 = 0;
	if (0 != (i & Class644.aClass644_8355.anInt8354 * 522594405)) {
	    int i_34_ = class534_sub40.method16529((byte) 1);
	    aClass165_8286 = class616.method10148(i_34_, 1778242587);
	} else
	    aClass165_8286 = Class616.aClass165_8057;
    }
    
    public void method10508(Class534_Sub40 class534_sub40, Class616 class616,
			    byte i) {
	int i_35_ = class534_sub40.method16529((byte) 1);
	int i_36_ = class534_sub40.method16530((byte) -82);
	int i_37_ = class534_sub40.method16530((byte) -105);
	int i_38_ = class534_sub40.method16530((byte) -112);
	int i_39_ = class534_sub40.method16529((byte) 1);
	Class12.anInt92 = i_39_ * -1037919377;
	aClass505_8290
	    = class616.method10149(i_35_, i_36_, i_37_, i_38_, (byte) 32);
    }
    
    boolean method10509(Class634 class634_40_, byte i) {
	return (class634_40_.anInt8289 * -1201354137 == -1201354137 * anInt8289
		&& aFloat8280 == class634_40_.aFloat8280
		&& aFloat8281 == class634_40_.aFloat8281
		&& class634_40_.aFloat8282 == aFloat8282
		&& class634_40_.aFloat8288 == aFloat8288
		&& class634_40_.aFloat8287 == aFloat8287
		&& aFloat8296 == class634_40_.aFloat8296
		&& (1102104287 * class634_40_.anInt8284
		    == anInt8284 * 1102104287)
		&& (class634_40_.anInt8292 * -563441071
		    == -563441071 * anInt8292)
		&& aClass165_8286 == class634_40_.aClass165_8286
		&& class634_40_.aClass505_8290 == aClass505_8290
		&& aFloat8291 == class634_40_.aFloat8291
		&& class634_40_.aFloat8279 == aFloat8279
		&& class634_40_.aFloat8293 == aFloat8293
		&& class634_40_.aFloat8294 == aFloat8294
		&& class634_40_.aFloat8295 == aFloat8295
		&& class634_40_.anIntArray8283[0] == anIntArray8283[0]
		&& anIntArray8283[1] == class634_40_.anIntArray8283[1]
		&& class634_40_.anIntArray8283[2] == anIntArray8283[2]
		&& aFloatArray8297[0] == class634_40_.aFloatArray8297[0]
		&& aFloatArray8297[1] == class634_40_.aFloatArray8297[1]
		&& aFloatArray8297[2] == class634_40_.aFloatArray8297[2]);
    }
    
    void method10510(Class634 class634_41_) {
	anInt8289 = class634_41_.anInt8289 * 1;
	aFloat8280 = class634_41_.aFloat8280;
	aFloat8281 = class634_41_.aFloat8281;
	aFloat8282 = class634_41_.aFloat8282;
	aClass438_8285.method6992(class634_41_.aClass438_8285);
	anInt8284 = 1 * class634_41_.anInt8284;
	anInt8292 = 1 * class634_41_.anInt8292;
	aClass165_8286 = class634_41_.aClass165_8286;
	aFloat8287 = class634_41_.aFloat8287;
	aFloat8288 = class634_41_.aFloat8288;
	aFloat8296 = class634_41_.aFloat8296;
	aClass505_8290 = class634_41_.aClass505_8290;
	aFloat8291 = class634_41_.aFloat8291;
	aFloat8279 = class634_41_.aFloat8279;
	aFloat8293 = class634_41_.aFloat8293;
	aFloat8294 = class634_41_.aFloat8294;
	aFloat8295 = class634_41_.aFloat8295;
	anIntArray8283[0] = class634_41_.anIntArray8283[0];
	anIntArray8283[1] = class634_41_.anIntArray8283[1];
	anIntArray8283[2] = class634_41_.anIntArray8283[2];
	aFloatArray8297[0] = class634_41_.aFloatArray8297[0];
	aFloatArray8297[1] = class634_41_.aFloatArray8297[1];
	aFloatArray8297[2] = class634_41_.aFloatArray8297[2];
    }
    
    public int method10511(int i) {
	return 1102104287 * anInt8284;
    }
    
    public Class505 method10512(int i) {
	return aClass505_8290;
    }
    
    void method10513() {
	anInt8289 = -1879224663;
	aClass438_8285 = Class438.method6996(-50.0F, -60.0F, -50.0F);
	aFloat8280 = 1.1523438F;
	aFloat8281 = 0.69921875F;
	aFloat8282 = 1.2F;
	anInt8284 = -969139112;
	anInt8292 = 0;
	aClass165_8286 = Class616.aClass165_8057;
	aFloat8287 = 1.0F;
	aFloat8288 = 0.25F;
	aFloat8296 = 1.0F;
	aClass505_8290 = Class616.aClass505_8058;
	aFloat8291 = 1.0F;
	aFloat8279 = 0.0F;
	aFloat8293 = 1.0F;
	aFloat8294 = 0.0F;
	aFloat8295 = 1.0F;
	int[] is = anIntArray8283;
	int[] is_42_ = anIntArray8283;
	anIntArray8283[2] = -1;
	is_42_[1] = -1;
	is[0] = -1;
	float[] fs = aFloatArray8297;
	float[] fs_43_ = aFloatArray8297;
	aFloatArray8297[2] = 0.0F;
	fs_43_[1] = 0.0F;
	fs[0] = 0.0F;
    }
    
    public void method10514(Class534_Sub40 class534_sub40, Class616 class616) {
	int i = class534_sub40.method16529((byte) 1);
	int i_44_ = class534_sub40.method16530((byte) -9);
	int i_45_ = class534_sub40.method16530((byte) -59);
	int i_46_ = class534_sub40.method16530((byte) -86);
	int i_47_ = class534_sub40.method16529((byte) 1);
	Class12.anInt92 = i_47_ * -1037919377;
	aClass505_8290
	    = class616.method10149(i, i_44_, i_45_, i_46_, (byte) 80);
    }
    
    void method10515(Class185 class185, Class634 class634_48_,
		     Class634 class634_49_, float f) {
	anInt8289 = Class69.method1396(-1201354137 * class634_48_.anInt8289,
				       class634_49_.anInt8289 * -1201354137,
				       255.0F * f, (byte) 84) * -956124841;
	aFloat8281 = ((class634_49_.aFloat8281 - class634_48_.aFloat8281) * f
		      + class634_48_.aFloat8281);
	aFloat8282 = ((class634_49_.aFloat8282 - class634_48_.aFloat8282) * f
		      + class634_48_.aFloat8282);
	aFloat8280 = class634_48_.aFloat8280 + (class634_49_.aFloat8280
						- class634_48_.aFloat8280) * f;
	aFloat8296 = class634_48_.aFloat8296 + (class634_49_.aFloat8296
						- class634_48_.aFloat8296) * f;
	aFloat8287 = class634_48_.aFloat8287 + (class634_49_.aFloat8287
						- class634_48_.aFloat8287) * f;
	aFloat8288 = class634_48_.aFloat8288 + f * (class634_49_.aFloat8288
						    - class634_48_.aFloat8288);
	anInt8284 = Class69.method1396(class634_48_.anInt8284 * 1102104287,
				       1102104287 * class634_49_.anInt8284,
				       f * 255.0F, (byte) 51) * 432829727;
	anInt8292 = ((int) ((float) (class634_49_.anInt8292 * -563441071
				     - -563441071 * class634_48_.anInt8292) * f
			    + (float) (class634_48_.anInt8292 * -563441071))
		     * 2005399217);
	if (class634_49_.aClass165_8286 != class634_48_.aClass165_8286)
	    aClass165_8286 = class185.method3451(class634_48_.aClass165_8286,
						 class634_49_.aClass165_8286,
						 f, aClass165_8286);
	if (class634_48_.aClass505_8290 != class634_49_.aClass505_8290) {
	    if (class634_48_.aClass505_8290 == null) {
		aClass505_8290 = class634_49_.aClass505_8290;
		if (aClass505_8290 != null)
		    aClass505_8290.method8328((int) (f * 255.0F), 0,
					      -541311282);
	    } else {
		aClass505_8290 = class634_48_.aClass505_8290;
		if (null != aClass505_8290)
		    aClass505_8290.method8328((int) (255.0F * f), 255,
					      -541311282);
	    }
	}
	aFloat8291 = class634_48_.aFloat8291 + f * (class634_49_.aFloat8291
						    - class634_48_.aFloat8291);
	aFloat8279 = ((class634_49_.aFloat8279 - class634_48_.aFloat8279) * f
		      + class634_48_.aFloat8279);
	aFloat8293 = ((class634_49_.aFloat8293 - class634_48_.aFloat8293) * f
		      + class634_48_.aFloat8293);
	aFloat8294 = ((class634_49_.aFloat8294 - class634_48_.aFloat8294) * f
		      + class634_48_.aFloat8294);
	aFloat8295 = ((class634_49_.aFloat8295 - class634_48_.aFloat8295) * f
		      + class634_48_.aFloat8295);
	float f_50_ = (class634_48_.aFloatArray8297[0]
		       + class634_48_.aFloatArray8297[1]
		       + class634_48_.aFloatArray8297[2]);
	float f_51_ = (class634_49_.aFloatArray8297[2]
		       + (class634_49_.aFloatArray8297[1]
			  + class634_49_.aFloatArray8297[0]));
	float f_52_ = (f_51_ - f_50_) * f + f_50_;
	if (0.0F == f_52_) {
	    int[] is = anIntArray8283;
	    int[] is_53_ = anIntArray8283;
	    anIntArray8283[2] = -1;
	    is_53_[1] = -1;
	    is[0] = -1;
	    float[] fs = aFloatArray8297;
	    float[] fs_54_ = aFloatArray8297;
	    aFloatArray8297[2] = 0.0F;
	    fs_54_[1] = 0.0F;
	    fs[0] = 0.0F;
	} else if (class634_48_.anIntArray8283[0] == -1
		   && class634_48_.anIntArray8283[1] == -1
		   && -1 == class634_48_.anIntArray8283[2]) {
	    for (int i = 0; i < 3; i++) {
		anIntArray8283[i] = class634_49_.anIntArray8283[i];
		if (-1 == anIntArray8283[i])
		    aFloatArray8297[i] = 0.0F;
		else
		    aFloatArray8297[i] = class634_49_.aFloatArray8297[i] * f;
	    }
	} else if (-1 == class634_49_.anIntArray8283[0]
		   && class634_49_.anIntArray8283[1] == -1
		   && -1 == class634_49_.anIntArray8283[2]) {
	    for (int i = 0; i < 3; i++) {
		anIntArray8283[i] = class634_48_.anIntArray8283[i];
		if (anIntArray8283[i] == -1)
		    aFloatArray8297[i] = 0.0F;
		else
		    aFloatArray8297[i]
			= class634_48_.aFloatArray8297[i] * (1.0F - f);
	    }
	} else {
	    float f_55_ = 1.0F - f;
	    int i = 0;
	    int[] is = { -1, -1, -1, -1, -1, -1 };
	    float[] fs = { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F };
	    for (int i_56_ = 0; i_56_ < 3; i_56_++) {
		if (class634_48_.anIntArray8283[i_56_] > -1) {
		    is[i] = class634_48_.anIntArray8283[i_56_];
		    fs[i++] = class634_48_.aFloatArray8297[i_56_] * f_55_;
		}
	    }
	    int i_57_ = i;
	    for (int i_58_ = 0; i_58_ < 3; i_58_++) {
		if (class634_49_.anIntArray8283[i_58_] > -1) {
		    float f_59_ = class634_49_.aFloatArray8297[i_58_] * f;
		    for (int i_60_ = 0; i_60_ < i_57_; i_60_++) {
			if (is[i_60_] == class634_49_.anIntArray8283[i_58_]) {
			    fs[i_60_] += f_59_;
			    break;
			}
			if (i_60_ == i_57_ - 1) {
			    is[i] = class634_49_.anIntArray8283[i_58_];
			    fs[i++] = f_59_;
			}
		    }
		}
	    }
	    if (i > 3) {
		float f_61_ = 0.0F;
		float f_62_ = 0.0F;
		for (int i_63_ = 0; i_63_ < i; i_63_++)
		    f_61_ += fs[i_63_];
		Class592.method9884(fs, is, 0, i - 1, 1013486639);
		for (int i_64_ = 0; i_64_ < 3; i_64_++)
		    f_62_ += fs[i_64_];
		float f_65_ = f_61_ / f_62_;
		for (int i_66_ = 0; i_66_ < 3; i_66_++)
		    fs[i_66_] *= f_65_;
	    }
	    for (int i_67_ = 0; i_67_ < 3; i_67_++) {
		anIntArray8283[i_67_] = is[i_67_];
		aFloatArray8297[i_67_] = fs[i_67_];
	    }
	}
    }
    
    public void method10516(Class534_Sub40 class534_sub40, int i) {
	method10524(class534_sub40, 0, -775742687);
    }
    
    public void method10517(Class534_Sub40 class534_sub40) {
	method10524(class534_sub40, 0, -189258914);
    }
    
    public void method10518(Class534_Sub40 class534_sub40) {
	method10524(class534_sub40, 0, -1137627148);
    }
    
    void method10519(Class534_Sub40 class534_sub40, int i) {
	anIntArray8283[i] = class534_sub40.method16529((byte) 1);
	aFloatArray8297[i] = class534_sub40.method16539(-1065606664);
    }
    
    boolean method10520(Class634 class634_68_) {
	return (class634_68_.anInt8289 * -1201354137 == -1201354137 * anInt8289
		&& aFloat8280 == class634_68_.aFloat8280
		&& aFloat8281 == class634_68_.aFloat8281
		&& class634_68_.aFloat8282 == aFloat8282
		&& class634_68_.aFloat8288 == aFloat8288
		&& class634_68_.aFloat8287 == aFloat8287
		&& aFloat8296 == class634_68_.aFloat8296
		&& (1102104287 * class634_68_.anInt8284
		    == anInt8284 * 1102104287)
		&& (class634_68_.anInt8292 * -563441071
		    == -563441071 * anInt8292)
		&& aClass165_8286 == class634_68_.aClass165_8286
		&& class634_68_.aClass505_8290 == aClass505_8290
		&& aFloat8291 == class634_68_.aFloat8291
		&& class634_68_.aFloat8279 == aFloat8279
		&& class634_68_.aFloat8293 == aFloat8293
		&& class634_68_.aFloat8294 == aFloat8294
		&& class634_68_.aFloat8295 == aFloat8295
		&& class634_68_.anIntArray8283[0] == anIntArray8283[0]
		&& anIntArray8283[1] == class634_68_.anIntArray8283[1]
		&& class634_68_.anIntArray8283[2] == anIntArray8283[2]
		&& aFloatArray8297[0] == class634_68_.aFloatArray8297[0]
		&& aFloatArray8297[1] == class634_68_.aFloatArray8297[1]
		&& aFloatArray8297[2] == class634_68_.aFloatArray8297[2]);
    }
    
    public void method10521(Class534_Sub40 class534_sub40, Class616 class616,
			    int i) {
	int i_69_ = class534_sub40.method16527(427310168);
	if (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub12_10753
		.method16985(16711680) == 1
	    && Class254.aClass185_2683.method3344() > 0) {
	    if (0 != (i_69_ & 522594405 * Class644.aClass644_8353.anInt8354))
		anInt8289
		    = class534_sub40.method16533(-258848859) * -956124841;
	    else
		anInt8289 = -1879224663;
	    if ((i_69_ & 522594405 * Class644.aClass644_8349.anInt8354) != 0)
		aFloat8280
		    = (float) class534_sub40.method16529((byte) 1) / 256.0F;
	    else
		aFloat8280 = 1.1523438F;
	    if (0 != (i_69_ & 522594405 * Class644.aClass644_8350.anInt8354))
		aFloat8281
		    = (float) class534_sub40.method16529((byte) 1) / 256.0F;
	    else
		aFloat8281 = 0.69921875F;
	    if (0 != (i_69_ & Class644.aClass644_8351.anInt8354 * 522594405))
		aFloat8282
		    = (float) class534_sub40.method16529((byte) 1) / 256.0F;
	    else
		aFloat8282 = 1.2F;
	} else {
	    if ((i_69_ & 522594405 * Class644.aClass644_8353.anInt8354) != 0)
		class534_sub40.method16533(-258848859);
	    if (0 != (i_69_ & Class644.aClass644_8349.anInt8354 * 522594405))
		class534_sub40.method16529((byte) 1);
	    if (0 != (i_69_ & 522594405 * Class644.aClass644_8350.anInt8354))
		class534_sub40.method16529((byte) 1);
	    if ((i_69_ & 522594405 * Class644.aClass644_8351.anInt8354) != 0)
		class534_sub40.method16529((byte) 1);
	    anInt8289 = -1879224663;
	    aFloat8282 = 1.2F;
	    aFloat8281 = 0.69921875F;
	    aFloat8280 = 1.1523438F;
	}
	if (0 != (i_69_ & 522594405 * Class644.aClass644_8352.anInt8354))
	    aClass438_8285
		= Class438.method6996((float) class534_sub40
						  .method16530((byte) -99),
				      (float) class534_sub40
						  .method16530((byte) -114),
				      (float) class534_sub40
						  .method16530((byte) -71));
	else
	    aClass438_8285 = Class438.method6996(-50.0F, -60.0F, -50.0F);
	if (0 != (i_69_ & 522594405 * Class644.aClass644_8360.anInt8354))
	    anInt8284 = class534_sub40.method16533(-258848859) * 432829727;
	else
	    anInt8284 = -969139112;
	if ((i_69_ & 522594405 * Class644.aClass644_8359.anInt8354) != 0)
	    anInt8292 = class534_sub40.method16529((byte) 1) * 2005399217;
	else
	    anInt8292 = 0;
	if (0 != (i_69_ & Class644.aClass644_8355.anInt8354 * 522594405)) {
	    int i_70_ = class534_sub40.method16529((byte) 1);
	    aClass165_8286 = class616.method10148(i_70_, 2123352597);
	} else
	    aClass165_8286 = Class616.aClass165_8057;
    }
    
    boolean method10522(Class634 class634_71_) {
	return (class634_71_.anInt8289 * -1201354137 == -1201354137 * anInt8289
		&& aFloat8280 == class634_71_.aFloat8280
		&& aFloat8281 == class634_71_.aFloat8281
		&& class634_71_.aFloat8282 == aFloat8282
		&& class634_71_.aFloat8288 == aFloat8288
		&& class634_71_.aFloat8287 == aFloat8287
		&& aFloat8296 == class634_71_.aFloat8296
		&& (1102104287 * class634_71_.anInt8284
		    == anInt8284 * 1102104287)
		&& (class634_71_.anInt8292 * -563441071
		    == -563441071 * anInt8292)
		&& aClass165_8286 == class634_71_.aClass165_8286
		&& class634_71_.aClass505_8290 == aClass505_8290
		&& aFloat8291 == class634_71_.aFloat8291
		&& class634_71_.aFloat8279 == aFloat8279
		&& class634_71_.aFloat8293 == aFloat8293
		&& class634_71_.aFloat8294 == aFloat8294
		&& class634_71_.aFloat8295 == aFloat8295
		&& class634_71_.anIntArray8283[0] == anIntArray8283[0]
		&& anIntArray8283[1] == class634_71_.anIntArray8283[1]
		&& class634_71_.anIntArray8283[2] == anIntArray8283[2]
		&& aFloatArray8297[0] == class634_71_.aFloatArray8297[0]
		&& aFloatArray8297[1] == class634_71_.aFloatArray8297[1]
		&& aFloatArray8297[2] == class634_71_.aFloatArray8297[2]);
    }
    
    boolean method10523(Class634 class634_72_) {
	return (class634_72_.anInt8289 * -1201354137 == -1201354137 * anInt8289
		&& aFloat8280 == class634_72_.aFloat8280
		&& aFloat8281 == class634_72_.aFloat8281
		&& class634_72_.aFloat8282 == aFloat8282
		&& class634_72_.aFloat8288 == aFloat8288
		&& class634_72_.aFloat8287 == aFloat8287
		&& aFloat8296 == class634_72_.aFloat8296
		&& (1102104287 * class634_72_.anInt8284
		    == anInt8284 * 1102104287)
		&& (class634_72_.anInt8292 * -563441071
		    == -563441071 * anInt8292)
		&& aClass165_8286 == class634_72_.aClass165_8286
		&& class634_72_.aClass505_8290 == aClass505_8290
		&& aFloat8291 == class634_72_.aFloat8291
		&& class634_72_.aFloat8279 == aFloat8279
		&& class634_72_.aFloat8293 == aFloat8293
		&& class634_72_.aFloat8294 == aFloat8294
		&& class634_72_.aFloat8295 == aFloat8295
		&& class634_72_.anIntArray8283[0] == anIntArray8283[0]
		&& anIntArray8283[1] == class634_72_.anIntArray8283[1]
		&& class634_72_.anIntArray8283[2] == anIntArray8283[2]
		&& aFloatArray8297[0] == class634_72_.aFloatArray8297[0]
		&& aFloatArray8297[1] == class634_72_.aFloatArray8297[1]
		&& aFloatArray8297[2] == class634_72_.aFloatArray8297[2]);
    }
    
    public Class634() {
	aFloat8279 = 0.0F;
	aFloat8293 = 1.0F;
	aFloat8294 = 0.0F;
	aFloat8295 = 1.0F;
	anIntArray8283 = new int[] { -1, -1, -1 };
	aFloatArray8297 = new float[] { 0.0F, 0.0F, 0.0F };
	method10501((byte) -13);
    }
    
    void method10524(Class534_Sub40 class534_sub40, int i, int i_73_) {
	anIntArray8283[i] = class534_sub40.method16529((byte) 1);
	aFloatArray8297[i] = class534_sub40.method16539(-1907338706);
    }
    
    void method10525(Class616 class616, Class653 class653, int i) {
	if (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub12_10753
		.method16985(16711680) == 1
	    && Class254.aClass185_2683.method3344() > 0) {
	    if (class653.method10742(-1674285162) != -1)
		anInt8289 = class653.method10742(-2099991643) * -956124841;
	    if (class653.method10788(-774360275) != -1.0F)
		aFloat8280 = class653.method10788(-1588626643);
	    if (class653.method10744((short) 1295) != -1.0F)
		aFloat8281 = class653.method10744((short) 16471);
	    if (class653.method10795((byte) 8) != -1.0F)
		aFloat8282 = class653.method10795((byte) 4);
	}
	if (class653.method10745((byte) 5) != null)
	    aClass438_8285.method6992(class653.method10745((byte) 5));
	if (class653.method10747((byte) -20) != -1)
	    anInt8284 = class653.method10747((byte) -28) * 432829727;
	if (class653.method10748((byte) -98) != -1)
	    anInt8292 = class653.method10748((byte) -3) * 2005399217;
	if (class653.method10781(859309255) != -1.0F)
	    aFloat8288 = class653.method10781(1282198835);
	if (class653.method10750(-292270949) != -1.0F)
	    aFloat8296 = class653.method10750(-1085680475);
	if (class653.method10770(-1217860483) != -1.0F)
	    aFloat8287 = class653.method10770(-1244666271);
	if (class653.method10752((byte) 2) != -1)
	    aClass165_8286
		= class616.method10148(class653.method10752((byte) 2),
				       1147206401);
	if (class653.method10753((byte) 0) != -1) {
	    int i_74_ = class653.method10753((byte) 0);
	    int i_75_ = class653.method10754(-1243052412);
	    int i_76_ = class653.method10759(1210028727);
	    int i_77_ = class653.method10756((byte) -83);
	    int i_78_ = class653.method10757(340045908);
	    Class12.anInt92 = -1037919377 * i_78_;
	    aClass505_8290
		= class616.method10149(i_74_, i_75_, i_76_, i_77_, (byte) 90);
	}
	if (class653.method10758(0, -1831722842) != -1) {
	    anIntArray8283[0] = class653.method10758(0, 244183358);
	    aFloatArray8297[0] = class653.method10755(0, 1894254237);
	}
	if (class653.method10758(1, -259738176) != -1) {
	    anIntArray8283[1] = class653.method10758(1, -135469932);
	    aFloatArray8297[1] = class653.method10755(1, 1994848549);
	}
	if (class653.method10758(2, -41099133) != -1) {
	    anIntArray8283[2] = class653.method10758(2, 213468361);
	    aFloatArray8297[2] = class653.method10755(2, 1917439096);
	}
    }
    
    public void method10526(Class534_Sub40 class534_sub40, Class616 class616) {
	int i = class534_sub40.method16529((byte) 1);
	int i_79_ = class534_sub40.method16530((byte) -116);
	int i_80_ = class534_sub40.method16530((byte) -97);
	int i_81_ = class534_sub40.method16530((byte) -16);
	int i_82_ = class534_sub40.method16529((byte) 1);
	Class12.anInt92 = i_82_ * -1037919377;
	aClass505_8290
	    = class616.method10149(i, i_79_, i_80_, i_81_, (byte) 111);
    }
    
    public int method10527() {
	return 1102104287 * anInt8284;
    }
    
    public Class505 method10528() {
	return aClass505_8290;
    }
    
    static final void method10529(Class669 class669, short i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 0;
    }
    
    static final void method10530(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = -4020817 * (((Class654_Sub1_Sub5_Sub1_Sub1)
			   class669.aClass654_Sub1_Sub5_Sub1_8604)
			  .aClass307_12204.anInt3279);
    }
    
    static final void method10531(Class247 class247, byte[] is, byte[] is_83_,
				  Class669 class669, byte i) {
	int i_84_ = ((class669.anIntArray8595
		      [(class669.anInt8600 -= 308999563) * 2088438307])
		     - 1);
	if (i_84_ < 0 || i_84_ > 9)
	    throw new RuntimeException();
	Class337.method5903(class247, i_84_, is, is_83_, class669,
			    -1444386474);
    }
    
    static void method10532(String[] strings, byte i) {
	if (strings.length > 1) {
	    for (int i_85_ = 0; i_85_ < strings.length; i_85_++) {
		if (strings[i_85_].startsWith("pause")) {
		    int i_86_ = 5;
		    try {
			i_86_ = Integer.parseInt(strings[i_85_].substring(6));
		    } catch (Exception exception) {
			/* empty */
		    }
		    Class73.method1567(new StringBuilder().append
					   ("Pausing for ").append
					   (i_86_).append
					   (" seconds...").toString(),
				       -1870129239);
		    Class114.aStringArray1382 = strings;
		    Class114.anInt1387 = -556419873 * (1 + i_85_);
		    Class303.aLong3252
			= (Class250.method4604((byte) -23)
			   + (long) (1000 * i_86_)) * 6271713210732061629L;
		    break;
		}
		Class114.aString1396 = strings[i_85_];
		Class649.method10705(false, (byte) 63);
	    }
	} else {
	    Class114.aString1396
		= new StringBuilder().append(Class114.aString1396).append
		      (strings[0]).toString();
	    Class114.anInt1389 += strings[0].length() * 399107939;
	}
    }
}
