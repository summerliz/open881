/* Class136_Sub7 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import jaggl.OpenGL;

public class Class136_Sub7 extends Class136
{
    static final char aChar8969 = '\001';
    static final char aChar8970 = '\0';
    Class124 aClass124_8971;
    static float[] aFloatArray8972 = { 0.0F, 0.0F, 0.0F, 0.0F };
    Class141_Sub3 aClass141_Sub3_8973;
    Class125 aClass125_8974;
    
    void method2341() {
	aClass125_8974.method2184('\001');
	if (aClass185_Sub3_1600.anInt9675 > 0) {
	    aClass185_Sub3_1600.method15230(1);
	    aClass185_Sub3_1600.method15231(null);
	    aClass185_Sub3_1600.method15177(1.0F, 0.0F);
	    aClass185_Sub3_1600.method15230(0);
	}
	aClass185_Sub3_1600.method15232(8448, 8448);
	OpenGL.glMatrixMode(5890);
	OpenGL.glPopMatrix();
	OpenGL.glMatrixMode(5888);
    }
    
    boolean method2347() {
	return true;
    }
    
    boolean method2342() {
	return true;
    }
    
    void method2329(boolean bool) {
	aClass185_Sub3_1600.method15232(260, 8448);
    }
    
    void method2327() {
	aClass125_8974.method2184('\001');
	if (aClass185_Sub3_1600.anInt9675 > 0) {
	    aClass185_Sub3_1600.method15230(1);
	    aClass185_Sub3_1600.method15231(null);
	    aClass185_Sub3_1600.method15177(1.0F, 0.0F);
	    aClass185_Sub3_1600.method15230(0);
	}
	aClass185_Sub3_1600.method15232(8448, 8448);
	OpenGL.glMatrixMode(5890);
	OpenGL.glPopMatrix();
	OpenGL.glMatrixMode(5888);
    }
    
    void method2326() {
	aClass125_8974.method2184('\001');
	if (aClass185_Sub3_1600.anInt9675 > 0) {
	    aClass185_Sub3_1600.method15230(1);
	    aClass185_Sub3_1600.method15231(null);
	    aClass185_Sub3_1600.method15177(1.0F, 0.0F);
	    aClass185_Sub3_1600.method15230(0);
	}
	aClass185_Sub3_1600.method15232(8448, 8448);
	OpenGL.glMatrixMode(5890);
	OpenGL.glPopMatrix();
	OpenGL.glMatrixMode(5888);
    }
    
    void method2343(Class141 class141, int i) {
	/* empty */
    }
    
    void method14486() {
	aClass125_8974 = new Class125(aClass185_Sub3_1600, 2);
	aClass125_8974.method2185(0);
	aClass185_Sub3_1600.method15230(1);
	aClass185_Sub3_1600.method15232(7681, 260);
	aClass185_Sub3_1600.method15325(0, 34168, 768);
	OpenGL.glTexGeni(8192, 9472, 9216);
	OpenGL.glEnable(3168);
	aClass185_Sub3_1600.method15230(0);
	OpenGL.glTexEnvf(8960, 34163, 2.0F);
	if (aClass124_8971.aBool1496) {
	    OpenGL.glTexGeni(8194, 9472, 9217);
	    OpenGL.glTexGeni(8195, 9472, 9217);
	    OpenGL.glTexGenfv(8195, 9473,
			      new float[] { 0.0F, 0.0F, 0.0F, 1.0F }, 0);
	    OpenGL.glEnable(3170);
	    OpenGL.glEnable(3171);
	}
	aClass125_8974.method2186();
	aClass125_8974.method2185(1);
	aClass185_Sub3_1600.method15230(1);
	aClass185_Sub3_1600.method15232(8448, 8448);
	aClass185_Sub3_1600.method15325(0, 5890, 768);
	OpenGL.glDisable(3168);
	aClass185_Sub3_1600.method15230(0);
	OpenGL.glTexEnvf(8960, 34163, 1.0F);
	if (aClass124_8971.aBool1496) {
	    OpenGL.glDisable(3170);
	    OpenGL.glDisable(3171);
	}
	aClass125_8974.method2186();
    }
    
    void method14487() {
	aClass125_8974 = new Class125(aClass185_Sub3_1600, 2);
	aClass125_8974.method2185(0);
	aClass185_Sub3_1600.method15230(1);
	aClass185_Sub3_1600.method15232(7681, 260);
	aClass185_Sub3_1600.method15325(0, 34168, 768);
	OpenGL.glTexGeni(8192, 9472, 9216);
	OpenGL.glEnable(3168);
	aClass185_Sub3_1600.method15230(0);
	OpenGL.glTexEnvf(8960, 34163, 2.0F);
	if (aClass124_8971.aBool1496) {
	    OpenGL.glTexGeni(8194, 9472, 9217);
	    OpenGL.glTexGeni(8195, 9472, 9217);
	    OpenGL.glTexGenfv(8195, 9473,
			      new float[] { 0.0F, 0.0F, 0.0F, 1.0F }, 0);
	    OpenGL.glEnable(3170);
	    OpenGL.glEnable(3171);
	}
	aClass125_8974.method2186();
	aClass125_8974.method2185(1);
	aClass185_Sub3_1600.method15230(1);
	aClass185_Sub3_1600.method15232(8448, 8448);
	aClass185_Sub3_1600.method15325(0, 5890, 768);
	OpenGL.glDisable(3168);
	aClass185_Sub3_1600.method15230(0);
	OpenGL.glTexEnvf(8960, 34163, 1.0F);
	if (aClass124_8971.aBool1496) {
	    OpenGL.glDisable(3170);
	    OpenGL.glDisable(3171);
	}
	aClass125_8974.method2186();
    }
    
    void method2332(boolean bool) {
	aClass185_Sub3_1600.method15232(260, 8448);
    }
    
    boolean method2334() {
	return true;
    }
    
    void method2335(boolean bool) {
	if (aClass185_Sub3_1600.anInt9675 > 0) {
	    float f = -0.5F / (float) aClass185_Sub3_1600.anInt9675;
	    aClass185_Sub3_1600.method15230(1);
	    aFloatArray8972[0] = 0.0F;
	    aFloatArray8972[1] = 0.0F;
	    aFloatArray8972[2] = f;
	    aFloatArray8972[3] = aClass185_Sub3_1600.aFloat9576 * f + 0.25F;
	    OpenGL.glPushMatrix();
	    OpenGL.glLoadIdentity();
	    OpenGL.glTexGenfv(8192, 9474, aFloatArray8972, 0);
	    OpenGL.glPopMatrix();
	    aClass185_Sub3_1600
		.method15177(0.5F, (float) aClass185_Sub3_1600.anInt9675);
	    aClass185_Sub3_1600.method15231(aClass141_Sub3_8973);
	    aClass185_Sub3_1600.method15230(0);
	}
	aClass125_8974.method2184('\0');
	OpenGL.glMatrixMode(5890);
	OpenGL.glPushMatrix();
	OpenGL.glScalef(0.25F, 0.25F, 1.0F);
	OpenGL.glMatrixMode(5888);
    }
    
    void method2331(int i, int i_0_) {
	if ((i & 0x1) == 1) {
	    if (aClass124_8971.aBool1496) {
		aClass185_Sub3_1600
		    .method15231(aClass124_8971.aClass141_Sub4_1495);
		aFloatArray8972[0] = 0.0F;
		aFloatArray8972[1] = 0.0F;
		aFloatArray8972[2] = 0.0F;
		aFloatArray8972[3]
		    = (float) (aClass185_Sub3_1600.anInt9696 % 4000) / 4000.0F;
		OpenGL.glTexGenfv(8194, 9473, aFloatArray8972, 0);
	    } else {
		int i_1_ = aClass185_Sub3_1600.anInt9696 % 4000 * 16 / 4000;
		aClass185_Sub3_1600
		    .method15231(aClass124_8971.aClass141_Sub2Array1494[i_1_]);
	    }
	} else if (aClass124_8971.aBool1496) {
	    aClass185_Sub3_1600
		.method15231(aClass124_8971.aClass141_Sub4_1495);
	    aFloatArray8972[0] = 0.0F;
	    aFloatArray8972[1] = 0.0F;
	    aFloatArray8972[2] = 0.0F;
	    aFloatArray8972[3] = 0.0F;
	    OpenGL.glTexGenfv(8194, 9473, aFloatArray8972, 0);
	} else
	    aClass185_Sub3_1600
		.method15231(aClass124_8971.aClass141_Sub2Array1494[0]);
    }
    
    void method2337(boolean bool) {
	if (aClass185_Sub3_1600.anInt9675 > 0) {
	    float f = -0.5F / (float) aClass185_Sub3_1600.anInt9675;
	    aClass185_Sub3_1600.method15230(1);
	    aFloatArray8972[0] = 0.0F;
	    aFloatArray8972[1] = 0.0F;
	    aFloatArray8972[2] = f;
	    aFloatArray8972[3] = aClass185_Sub3_1600.aFloat9576 * f + 0.25F;
	    OpenGL.glPushMatrix();
	    OpenGL.glLoadIdentity();
	    OpenGL.glTexGenfv(8192, 9474, aFloatArray8972, 0);
	    OpenGL.glPopMatrix();
	    aClass185_Sub3_1600
		.method15177(0.5F, (float) aClass185_Sub3_1600.anInt9675);
	    aClass185_Sub3_1600.method15231(aClass141_Sub3_8973);
	    aClass185_Sub3_1600.method15230(0);
	}
	aClass125_8974.method2184('\0');
	OpenGL.glMatrixMode(5890);
	OpenGL.glPushMatrix();
	OpenGL.glScalef(0.25F, 0.25F, 1.0F);
	OpenGL.glMatrixMode(5888);
    }
    
    void method2338(boolean bool) {
	if (aClass185_Sub3_1600.anInt9675 > 0) {
	    float f = -0.5F / (float) aClass185_Sub3_1600.anInt9675;
	    aClass185_Sub3_1600.method15230(1);
	    aFloatArray8972[0] = 0.0F;
	    aFloatArray8972[1] = 0.0F;
	    aFloatArray8972[2] = f;
	    aFloatArray8972[3] = aClass185_Sub3_1600.aFloat9576 * f + 0.25F;
	    OpenGL.glPushMatrix();
	    OpenGL.glLoadIdentity();
	    OpenGL.glTexGenfv(8192, 9474, aFloatArray8972, 0);
	    OpenGL.glPopMatrix();
	    aClass185_Sub3_1600
		.method15177(0.5F, (float) aClass185_Sub3_1600.anInt9675);
	    aClass185_Sub3_1600.method15231(aClass141_Sub3_8973);
	    aClass185_Sub3_1600.method15230(0);
	}
	aClass125_8974.method2184('\0');
	OpenGL.glMatrixMode(5890);
	OpenGL.glPushMatrix();
	OpenGL.glScalef(0.25F, 0.25F, 1.0F);
	OpenGL.glMatrixMode(5888);
    }
    
    Class136_Sub7(Class185_Sub3 class185_sub3, Class124 class124) {
	super(class185_sub3);
	aClass124_8971 = class124;
	method14486();
	aClass141_Sub3_8973
	    = new Class141_Sub3(aClass185_Sub3_1600, Class181.aClass181_1951,
				Class173.aClass173_1830, 2,
				new byte[] { 0, -1 }, Class181.aClass181_1951);
	aClass141_Sub3_8973.method14483(false);
    }
    
    void method2340(boolean bool) {
	aClass185_Sub3_1600.method15232(260, 8448);
    }
    
    void method2336(boolean bool) {
	aClass185_Sub3_1600.method15232(260, 8448);
    }
    
    void method2328(boolean bool) {
	if (aClass185_Sub3_1600.anInt9675 > 0) {
	    float f = -0.5F / (float) aClass185_Sub3_1600.anInt9675;
	    aClass185_Sub3_1600.method15230(1);
	    aFloatArray8972[0] = 0.0F;
	    aFloatArray8972[1] = 0.0F;
	    aFloatArray8972[2] = f;
	    aFloatArray8972[3] = aClass185_Sub3_1600.aFloat9576 * f + 0.25F;
	    OpenGL.glPushMatrix();
	    OpenGL.glLoadIdentity();
	    OpenGL.glTexGenfv(8192, 9474, aFloatArray8972, 0);
	    OpenGL.glPopMatrix();
	    aClass185_Sub3_1600
		.method15177(0.5F, (float) aClass185_Sub3_1600.anInt9675);
	    aClass185_Sub3_1600.method15231(aClass141_Sub3_8973);
	    aClass185_Sub3_1600.method15230(0);
	}
	aClass125_8974.method2184('\0');
	OpenGL.glMatrixMode(5890);
	OpenGL.glPushMatrix();
	OpenGL.glScalef(0.25F, 0.25F, 1.0F);
	OpenGL.glMatrixMode(5888);
    }
    
    void method2330(int i, int i_2_) {
	if ((i & 0x1) == 1) {
	    if (aClass124_8971.aBool1496) {
		aClass185_Sub3_1600
		    .method15231(aClass124_8971.aClass141_Sub4_1495);
		aFloatArray8972[0] = 0.0F;
		aFloatArray8972[1] = 0.0F;
		aFloatArray8972[2] = 0.0F;
		aFloatArray8972[3]
		    = (float) (aClass185_Sub3_1600.anInt9696 % 4000) / 4000.0F;
		OpenGL.glTexGenfv(8194, 9473, aFloatArray8972, 0);
	    } else {
		int i_3_ = aClass185_Sub3_1600.anInt9696 % 4000 * 16 / 4000;
		aClass185_Sub3_1600
		    .method15231(aClass124_8971.aClass141_Sub2Array1494[i_3_]);
	    }
	} else if (aClass124_8971.aBool1496) {
	    aClass185_Sub3_1600
		.method15231(aClass124_8971.aClass141_Sub4_1495);
	    aFloatArray8972[0] = 0.0F;
	    aFloatArray8972[1] = 0.0F;
	    aFloatArray8972[2] = 0.0F;
	    aFloatArray8972[3] = 0.0F;
	    OpenGL.glTexGenfv(8194, 9473, aFloatArray8972, 0);
	} else
	    aClass185_Sub3_1600
		.method15231(aClass124_8971.aClass141_Sub2Array1494[0]);
    }
    
    boolean method2333() {
	return true;
    }
    
    void method2345() {
	aClass125_8974.method2184('\001');
	if (aClass185_Sub3_1600.anInt9675 > 0) {
	    aClass185_Sub3_1600.method15230(1);
	    aClass185_Sub3_1600.method15231(null);
	    aClass185_Sub3_1600.method15177(1.0F, 0.0F);
	    aClass185_Sub3_1600.method15230(0);
	}
	aClass185_Sub3_1600.method15232(8448, 8448);
	OpenGL.glMatrixMode(5890);
	OpenGL.glPopMatrix();
	OpenGL.glMatrixMode(5888);
    }
    
    void method2346(int i, int i_4_) {
	if ((i & 0x1) == 1) {
	    if (aClass124_8971.aBool1496) {
		aClass185_Sub3_1600
		    .method15231(aClass124_8971.aClass141_Sub4_1495);
		aFloatArray8972[0] = 0.0F;
		aFloatArray8972[1] = 0.0F;
		aFloatArray8972[2] = 0.0F;
		aFloatArray8972[3]
		    = (float) (aClass185_Sub3_1600.anInt9696 % 4000) / 4000.0F;
		OpenGL.glTexGenfv(8194, 9473, aFloatArray8972, 0);
	    } else {
		int i_5_ = aClass185_Sub3_1600.anInt9696 % 4000 * 16 / 4000;
		aClass185_Sub3_1600
		    .method15231(aClass124_8971.aClass141_Sub2Array1494[i_5_]);
	    }
	} else if (aClass124_8971.aBool1496) {
	    aClass185_Sub3_1600
		.method15231(aClass124_8971.aClass141_Sub4_1495);
	    aFloatArray8972[0] = 0.0F;
	    aFloatArray8972[1] = 0.0F;
	    aFloatArray8972[2] = 0.0F;
	    aFloatArray8972[3] = 0.0F;
	    OpenGL.glTexGenfv(8194, 9473, aFloatArray8972, 0);
	} else
	    aClass185_Sub3_1600
		.method15231(aClass124_8971.aClass141_Sub2Array1494[0]);
    }
    
    void method2344(boolean bool) {
	if (aClass185_Sub3_1600.anInt9675 > 0) {
	    float f = -0.5F / (float) aClass185_Sub3_1600.anInt9675;
	    aClass185_Sub3_1600.method15230(1);
	    aFloatArray8972[0] = 0.0F;
	    aFloatArray8972[1] = 0.0F;
	    aFloatArray8972[2] = f;
	    aFloatArray8972[3] = aClass185_Sub3_1600.aFloat9576 * f + 0.25F;
	    OpenGL.glPushMatrix();
	    OpenGL.glLoadIdentity();
	    OpenGL.glTexGenfv(8192, 9474, aFloatArray8972, 0);
	    OpenGL.glPopMatrix();
	    aClass185_Sub3_1600
		.method15177(0.5F, (float) aClass185_Sub3_1600.anInt9675);
	    aClass185_Sub3_1600.method15231(aClass141_Sub3_8973);
	    aClass185_Sub3_1600.method15230(0);
	}
	aClass125_8974.method2184('\0');
	OpenGL.glMatrixMode(5890);
	OpenGL.glPushMatrix();
	OpenGL.glScalef(0.25F, 0.25F, 1.0F);
	OpenGL.glMatrixMode(5888);
    }
    
    void method2348(Class141 class141, int i) {
	/* empty */
    }
    
    void method14488() {
	aClass125_8974 = new Class125(aClass185_Sub3_1600, 2);
	aClass125_8974.method2185(0);
	aClass185_Sub3_1600.method15230(1);
	aClass185_Sub3_1600.method15232(7681, 260);
	aClass185_Sub3_1600.method15325(0, 34168, 768);
	OpenGL.glTexGeni(8192, 9472, 9216);
	OpenGL.glEnable(3168);
	aClass185_Sub3_1600.method15230(0);
	OpenGL.glTexEnvf(8960, 34163, 2.0F);
	if (aClass124_8971.aBool1496) {
	    OpenGL.glTexGeni(8194, 9472, 9217);
	    OpenGL.glTexGeni(8195, 9472, 9217);
	    OpenGL.glTexGenfv(8195, 9473,
			      new float[] { 0.0F, 0.0F, 0.0F, 1.0F }, 0);
	    OpenGL.glEnable(3170);
	    OpenGL.glEnable(3171);
	}
	aClass125_8974.method2186();
	aClass125_8974.method2185(1);
	aClass185_Sub3_1600.method15230(1);
	aClass185_Sub3_1600.method15232(8448, 8448);
	aClass185_Sub3_1600.method15325(0, 5890, 768);
	OpenGL.glDisable(3168);
	aClass185_Sub3_1600.method15230(0);
	OpenGL.glTexEnvf(8960, 34163, 1.0F);
	if (aClass124_8971.aBool1496) {
	    OpenGL.glDisable(3170);
	    OpenGL.glDisable(3171);
	}
	aClass125_8974.method2186();
    }
    
    void method2339(boolean bool) {
	aClass185_Sub3_1600.method15232(260, 8448);
    }
}
