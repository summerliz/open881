/* Interface60 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public interface Interface60
{
    public boolean method397(String string, int i);
    
    public void method398(String string) throws Exception_Sub3;
    
    public void method399(String string) throws Exception_Sub3;
    
    public void method400(String string, int i) throws Exception_Sub3;
    
    public boolean method401(String string);
    
    public boolean method204();
    
    public boolean method208();
    
    public boolean method402(short i);
    
    public boolean method403(String string);
}
