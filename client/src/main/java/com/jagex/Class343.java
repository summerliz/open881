/* Class343 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public abstract class Class343
{
    boolean aBool3555;
    protected Class185_Sub1 aClass185_Sub1_3556;
    
    int method6058() {
	return 0;
    }
    
    int method6059() {
	return 1;
    }
    
    abstract boolean method6060();
    
    abstract boolean method6061();
    
    boolean method6062() {
	return method6061();
    }
    
    abstract boolean method6063();
    
    abstract void method6064(int i, int i_0_);
    
    abstract void method6065(int i, Class175_Sub1 class175_sub1,
			     Interface38 interface38, Interface21 interface21,
			     Interface38 interface38_1_, boolean bool);
    
    abstract void method6066(int i);
    
    abstract void method6067(int i, int i_2_);
    
    abstract int method6068();
    
    int method6069() {
	return 0;
    }
    
    boolean method6070() {
	return aBool3555;
    }
    
    void method6071() {
	/* empty */
    }
    
    abstract boolean method6072();
    
    abstract boolean method6073();
    
    int method6074() {
	return 1;
    }
    
    abstract void method6075(int i, int i_3_);
    
    abstract boolean method6076();
    
    abstract boolean method6077();
    
    boolean method6078() {
	return method6061();
    }
    
    abstract void method6079(int i, Class175_Sub1 class175_sub1,
			     Interface38 interface38, Interface21 interface21,
			     Interface38 interface38_4_, boolean bool);
    
    abstract void method6080(int i, Class175_Sub1 class175_sub1,
			     Interface38 interface38, Interface21 interface21,
			     Interface38 interface38_5_, boolean bool);
    
    abstract void method6081(int i, Class175_Sub1 class175_sub1,
			     Interface38 interface38, Interface21 interface21,
			     Interface38 interface38_6_, boolean bool);
    
    int method6082() {
	return 1;
    }
    
    int method6083() {
	return 1;
    }
    
    abstract boolean method6084();
    
    int method6085() {
	return 1;
    }
    
    abstract int method6086();
    
    abstract void method6087(int i);
    
    boolean method6088() {
	return aBool3555;
    }
    
    boolean method6089() {
	return aBool3555;
    }
    
    void method6090() {
	/* empty */
    }
    
    void method6091() {
	/* empty */
    }
    
    abstract void method6092(int i);
    
    abstract boolean method6093();
    
    abstract boolean method6094();
    
    abstract boolean method6095();
    
    abstract boolean method6096();
    
    abstract boolean method6097();
    
    abstract boolean method6098();
    
    abstract boolean method6099();
    
    abstract void method6100(int i, int i_7_);
    
    void method6101() {
	/* empty */
    }
    
    abstract void method6102(int i, int i_8_);
    
    abstract void method6103();
    
    abstract void method6104();
    
    abstract void method6105();
    
    abstract void method6106();
    
    Class343(Class185_Sub1 class185_sub1) {
	aClass185_Sub1_3556 = class185_sub1;
    }
    
    abstract void method6107(int i);
    
    abstract void method6108(int i);
}
