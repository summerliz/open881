/* Class701 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public final class Class701
{
    Class536_Sub2 aClass536_Sub2_8808;
    public Class536_Sub2 aClass536_Sub2_8809 = new Class536_Sub2();
    
    public Class536_Sub2 method14202() {
	Class536_Sub2 class536_sub2 = aClass536_Sub2_8808;
	if (class536_sub2 == aClass536_Sub2_8809) {
	    aClass536_Sub2_8808 = null;
	    return null;
	}
	aClass536_Sub2_8808 = class536_sub2.aClass536_Sub2_10349;
	return class536_sub2;
    }
    
    public void method14203(Class536_Sub2 class536_sub2, int i) {
	if (null != class536_sub2.aClass536_Sub2_10348)
	    class536_sub2.method15973(1214266234);
	class536_sub2.aClass536_Sub2_10348
	    = aClass536_Sub2_8809.aClass536_Sub2_10348;
	class536_sub2.aClass536_Sub2_10349 = aClass536_Sub2_8809;
	class536_sub2.aClass536_Sub2_10348.aClass536_Sub2_10349
	    = class536_sub2;
	class536_sub2.aClass536_Sub2_10349.aClass536_Sub2_10348
	    = class536_sub2;
    }
    
    public Class536_Sub2 method14204() {
	Class536_Sub2 class536_sub2 = aClass536_Sub2_8808;
	if (class536_sub2 == aClass536_Sub2_8809) {
	    aClass536_Sub2_8808 = null;
	    return null;
	}
	aClass536_Sub2_8808 = class536_sub2.aClass536_Sub2_10349;
	return class536_sub2;
    }
    
    public Class536_Sub2 method14205(int i) {
	Class536_Sub2 class536_sub2 = aClass536_Sub2_8808;
	if (class536_sub2 == aClass536_Sub2_8809) {
	    aClass536_Sub2_8808 = null;
	    return null;
	}
	aClass536_Sub2_8808 = class536_sub2.aClass536_Sub2_10349;
	return class536_sub2;
    }
    
    public void method14206(Class536_Sub2 class536_sub2) {
	if (null != class536_sub2.aClass536_Sub2_10348)
	    class536_sub2.method15973(1214266234);
	class536_sub2.aClass536_Sub2_10348
	    = aClass536_Sub2_8809.aClass536_Sub2_10348;
	class536_sub2.aClass536_Sub2_10349 = aClass536_Sub2_8809;
	class536_sub2.aClass536_Sub2_10348.aClass536_Sub2_10349
	    = class536_sub2;
	class536_sub2.aClass536_Sub2_10349.aClass536_Sub2_10348
	    = class536_sub2;
    }
    
    public void method14207(byte i) {
	for (;;) {
	    Class536_Sub2 class536_sub2
		= aClass536_Sub2_8809.aClass536_Sub2_10349;
	    if (class536_sub2 == aClass536_Sub2_8809)
		break;
	    class536_sub2.method15973(1214266234);
	}
	aClass536_Sub2_8808 = null;
    }
    
    public Class536_Sub2 method14208() {
	Class536_Sub2 class536_sub2 = aClass536_Sub2_8809.aClass536_Sub2_10349;
	if (aClass536_Sub2_8809 == class536_sub2) {
	    aClass536_Sub2_8808 = null;
	    return null;
	}
	aClass536_Sub2_8808 = class536_sub2.aClass536_Sub2_10349;
	return class536_sub2;
    }
    
    public Class701() {
	aClass536_Sub2_8809.aClass536_Sub2_10349 = aClass536_Sub2_8809;
	aClass536_Sub2_8809.aClass536_Sub2_10348 = aClass536_Sub2_8809;
    }
    
    public Class536_Sub2 method14209() {
	Class536_Sub2 class536_sub2 = aClass536_Sub2_8808;
	if (class536_sub2 == aClass536_Sub2_8809) {
	    aClass536_Sub2_8808 = null;
	    return null;
	}
	aClass536_Sub2_8808 = class536_sub2.aClass536_Sub2_10349;
	return class536_sub2;
    }
    
    public Class536_Sub2 method14210() {
	Class536_Sub2 class536_sub2 = aClass536_Sub2_8809.aClass536_Sub2_10349;
	if (aClass536_Sub2_8809 == class536_sub2) {
	    aClass536_Sub2_8808 = null;
	    return null;
	}
	aClass536_Sub2_8808 = class536_sub2.aClass536_Sub2_10349;
	return class536_sub2;
    }
    
    public Class536_Sub2 method14211(int i) {
	Class536_Sub2 class536_sub2 = aClass536_Sub2_8809.aClass536_Sub2_10349;
	if (aClass536_Sub2_8809 == class536_sub2) {
	    aClass536_Sub2_8808 = null;
	    return null;
	}
	aClass536_Sub2_8808 = class536_sub2.aClass536_Sub2_10349;
	return class536_sub2;
    }
    
    public Class536_Sub2 method14212() {
	Class536_Sub2 class536_sub2 = aClass536_Sub2_8808;
	if (class536_sub2 == aClass536_Sub2_8809) {
	    aClass536_Sub2_8808 = null;
	    return null;
	}
	aClass536_Sub2_8808 = class536_sub2.aClass536_Sub2_10349;
	return class536_sub2;
    }
    
    public static void method14213(int i) {
	Class44_Sub11.aClass243Array11006
	    = new Class243[Class247.aClass472_2446.method7679(1554434211)];
	Class250.aBoolArray2652
	    = new boolean[Class247.aClass472_2446.method7679(1554434211)];
    }
}
