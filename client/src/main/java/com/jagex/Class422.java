/* Class422 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class422
{
    public static final int anInt4712 = 4;
    public static final int anInt4713 = 8;
    public static final int anInt4714 = 1;
    public static final int anInt4715 = 1;
    public static final int anInt4716 = 16;
    public static final int anInt4717 = 524288;
    public static final int anInt4718 = 128;
    public static final int anInt4719 = 67108864;
    public static final int anInt4720 = 1024;
    public static final int anInt4721 = 256;
    public static final int anInt4722 = 512;
    public static final int anInt4723 = 32768;
    public static final int anInt4724 = 4096;
    public static final int anInt4725 = 8192;
    public static final int anInt4726 = 256;
    public static final int anInt4727 = 4194304;
    public static final int anInt4728 = 65536;
    public static final int anInt4729 = 64;
    public static final int anInt4730 = 262144;
    public static final int anInt4731 = 524288;
    public static final int anInt4732 = 2097152;
    public static final int anInt4733 = 131072;
    public static final int anInt4734 = 8388608;
    public static final int anInt4735 = 8;
    public static final int anInt4736 = 262144;
    public static final int anInt4737 = 131072;
    public static final int anInt4738 = 128;
    public static final int anInt4739 = 16;
    public static final int anInt4740 = 4;
    public static final int anInt4741 = 32;
    public static final int anInt4742 = 2;
    public static final int anInt4743 = 512;
    public static final int anInt4744 = 8192;
    public static final int anInt4745 = 1024;
    public static final int anInt4746 = 2048;
    public static final int anInt4747 = 32;
    public static final int anInt4748 = 32768;
    public static final int anInt4749 = 2048;
    public static final int anInt4750 = 2;
    public static final int anInt4751 = 1048576;
    public static final int anInt4752 = 4194304;
    public static final int anInt4753 = 8388608;
    public static final int anInt4754 = 33554432;
    public static final int anInt4755 = 2097152;
    public static final int anInt4756 = 65536;
    public static final int anInt4757 = 64;
    public static final int anInt4758 = 16777216;
    public static final int anInt4759 = 4096;
    public static final int anInt4760 = 134217728;
    public static final int anInt4761 = 1048576;
    public static final int anInt4762 = 268435456;
    public static final int anInt4763 = 536870912;
    public static int anInt4764;
    
    Class422() throws Throwable {
	throw new Error();
    }
    
    static final void method6784(Class669 class669, byte i) {
	int i_0_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class307.method5649(i_0_, 1911546269);
    }
    
    static void method6785(byte i) {
	for (int i_1_ = 0; i_1_ < 108; i_1_++)
	    client.aBoolArray11180[i_1_] = true;
    }
    
    public static int method6786(String string, int i) {
	return string.length() + 2;
    }
    
    static final void method6787(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 1245290027 * Class65.anInt701;
    }
    
    static final void method6788(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 0;
    }
}
