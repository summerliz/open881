/* Class324 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public abstract class Class324
{
    public int anInt3428;
    protected Class185_Sub1 aClass185_Sub1_3429;
    public Class433 aClass433_3430;
    public Class438 aClass438_3431;
    public Interface38 anInterface38_3432;
    public Class438 aClass438_3433;
    public Interface41 anInterface41_3434;
    public Class438 aClass438_3435;
    protected float aFloat3436;
    protected float aFloat3437;
    public Class441 aClass441_3438;
    public float[] aFloatArray3439 = new float[16];
    public Class441 aClass441_3440;
    static final int anInt3441 = 4;
    public Class438 aClass438_3442;
    public Class438 aClass438_3443;
    public Class438 aClass438_3444;
    public Class438 aClass438_3445;
    public Class433 aClass433_3446;
    public int anInt3447;
    public float[] aFloatArray3448 = new float[16];
    public int anInt3449;
    public int anInt3450;
    
    public abstract void method5792();
    
    public void method5793(byte i) {
	switch (i) {
	case 1:
	    aFloat3436 = 32.0F;
	    aFloat3437 = 0.0F;
	    break;
	case 3:
	    aFloat3436 = 1.0F;
	    aFloat3437 = 0.0F;
	    break;
	case 2:
	    aFloat3436 = 4.0F;
	    aFloat3437 = 0.0F;
	    break;
	}
    }
    
    public abstract void method5794(Class433 class433);
    
    public abstract void method5795();
    
    public abstract void method5796(int i);
    
    public abstract void method5797(int i);
    
    public void method5798(byte i) {
	switch (i) {
	case 1:
	    aFloat3436 = 32.0F;
	    aFloat3437 = 0.0F;
	    break;
	case 3:
	    aFloat3436 = 1.0F;
	    aFloat3437 = 0.0F;
	    break;
	case 2:
	    aFloat3436 = 4.0F;
	    aFloat3437 = 0.0F;
	    break;
	}
    }
    
    public abstract void method5799();
    
    public abstract void method5800();
    
    public abstract void method5801(Class433 class433);
    
    public abstract void method5802(Class433 class433);
    
    public abstract void method5803(boolean bool);
    
    public abstract void method5804(boolean bool);
    
    public abstract void method5805(int i);
    
    public abstract void method5806(int i);
    
    public abstract void method5807(int i);
    
    public abstract void method5808(int i);
    
    public abstract void method5809();
    
    public abstract void method5810(int i);
    
    public void method5811(byte i) {
	switch (i) {
	case 1:
	    aFloat3436 = 32.0F;
	    aFloat3437 = 0.0F;
	    break;
	case 3:
	    aFloat3436 = 1.0F;
	    aFloat3437 = 0.0F;
	    break;
	case 2:
	    aFloat3436 = 4.0F;
	    aFloat3437 = 0.0F;
	    break;
	}
    }
    
    public abstract void method5812(boolean bool);
    
    Class324(Class185_Sub1 class185_sub1) {
	aClass433_3430 = new Class433();
	aClass438_3433 = new Class438();
	aClass441_3438 = new Class441();
	aClass438_3431 = new Class438();
	aClass441_3440 = new Class441();
	aClass438_3435 = new Class438();
	aClass438_3442 = new Class438();
	aClass438_3443 = new Class438();
	aClass438_3444 = new Class438();
	aClass438_3445 = new Class438();
	aClass433_3446 = new Class433();
	aClass185_Sub1_3429 = class185_sub1;
    }
    
    public abstract void method5813(int i);
    
    public abstract void method5814(int i);
    
    public abstract void method5815(int i);
}
