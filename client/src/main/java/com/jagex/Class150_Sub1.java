/* Class150_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class150_Sub1 extends Class150
{
    public static Class44 aClass44_8902;
    
    void method2486(Class534_Sub40 class534_sub40, int i, byte i_0_) {
	/* empty */
    }
    
    void method2481(Class534_Sub40 class534_sub40, int i) {
	/* empty */
    }
    
    public void method82(int i) {
	/* empty */
    }
    
    void method2480(Class534_Sub40 class534_sub40, int i) {
	/* empty */
    }
    
    Class150_Sub1(Class453 class453, int i) {
	super(class453, i);
    }
    
    public void method83() {
	/* empty */
    }
    
    public void method84() {
	/* empty */
    }
    
    static byte method14441(int i, int i_1_, byte i_2_) {
	if (i != Class595.aClass595_7833.anInt7852 * 847393323)
	    return (byte) 0;
	if (0 == (i_1_ & 0x1))
	    return (byte) 1;
	return (byte) 2;
    }
    
    public static void method14442(Class632 class632, byte i) {
	Class202.anInt2192 = class632.anInt8246 * -103344533;
	Class67.anInt718 = class632.anInt8258 * 1731883691;
	Class68.anInt722 = class632.anInt8248 * -2011868781;
	Class623.anInt8149 = 1072432179 * class632.anInt8260;
	Class706.anInt8846 = -593735475 * class632.anInt8261;
	Class523.anInt7087 = class632.anInt8224 * -2033390451;
	Class216.anInt2299 = class632.anInt8232 * -687787901;
	Class67.anInt719 = class632.anInt8264 * -561352305;
	Class530.anInt7134 = -1866109191 * class632.anInt8265;
	Class215.anInt2297 = class632.anInt8266 * 488161695;
	Class610.anInt8010 = class632.anInt8223 * -1447697231;
	Class406.anInt4298 = 1286636607 * class632.anInt8268;
    }
    
    public static void method14443(Class472 class472, Class472 class472_3_,
				   Class472 class472_4_, Class472 class472_5_,
				   int i) {
	Class247.aClass472_2446 = class472;
	Class317.aClass472_3389 = class472_3_;
	Class406.aClass472_4297 = class472_4_;
	Class44_Sub11.aClass243Array11006
	    = new Class243[Class247.aClass472_2446.method7679(1554434211)];
	Class250.aBoolArray2652
	    = new boolean[Class247.aClass472_2446.method7679(1554434211)];
    }
}
