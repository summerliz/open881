/* Class110_Sub1_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.util.Iterator;

public class Class110_Sub1_Sub2 extends Class110_Sub1 implements Interface14
{
    Class203 aClass203_11376 = new Class203(64);
    Class472 aClass472_11377;
    
    public void method17879(byte i) {
	synchronized (aClass203_11376) {
	    aClass203_11376.method3884((byte) -89);
	}
    }
    
    public void method17880(int i, int i_0_) {
	synchronized (aClass203_11376) {
	    aClass203_11376.method3876(i, (byte) 0);
	}
    }
    
    Class150_Sub1 method17881(int i, int i_1_) {
	byte[] is
	    = aClass472_11377.method7743((aClass453_8978.method7394((byte) 64)
					  .anInt8385) * 1570156841,
					 i, -1331941876);
	Class150_Sub1 class150_sub1 = new Class150_Sub1(aClass453_8978, i);
	if (is != null)
	    class150_sub1.method79(new Class534_Sub40(is), (byte) 3);
	return class150_sub1;
    }
    
    public void method17882(short i) {
	synchronized (aClass203_11376) {
	    aClass203_11376.method3877(1717314495);
	}
    }
    
    public void method17883() {
	synchronized (aClass203_11376) {
	    aClass203_11376.method3884((byte) -32);
	}
    }
    
    public void method17884() {
	synchronized (aClass203_11376) {
	    aClass203_11376.method3884((byte) -16);
	}
    }
    
    public Interface13 method91(int i, int i_2_) {
	Class150_Sub1 class150_sub1;
	synchronized (aClass203_11376) {
	    class150_sub1
		= (Class150_Sub1) aClass203_11376.method3871((long) i);
	    if (class150_sub1 == null) {
		class150_sub1 = method17881(i, 1364461718);
		aClass203_11376.method3893(class150_sub1, (long) i);
	    }
	}
	return class150_sub1;
    }
    
    public Class110_Sub1_Sub2(Class675 class675, Class453 class453,
			      Class672 class672, Class472 class472) {
	super(class675, class453, class672,
	      (class472 != null
	       ? class472.method7726((class453.method7394((byte) 64).anInt8385
				      * 1570156841),
				     (byte) 35)
	       : 0));
	aClass472_11377 = class472;
    }
    
    public Interface13 method87(int i) {
	Class150_Sub1 class150_sub1;
	synchronized (aClass203_11376) {
	    class150_sub1
		= (Class150_Sub1) aClass203_11376.method3871((long) i);
	    if (class150_sub1 == null) {
		class150_sub1 = method17881(i, 1590343695);
		aClass203_11376.method3893(class150_sub1, (long) i);
	    }
	}
	return class150_sub1;
    }
    
    public Interface13 method86(int i) {
	Class150_Sub1 class150_sub1;
	synchronized (aClass203_11376) {
	    class150_sub1
		= (Class150_Sub1) aClass203_11376.method3871((long) i);
	    if (class150_sub1 == null) {
		class150_sub1 = method17881(i, -736525263);
		aClass203_11376.method3893(class150_sub1, (long) i);
	    }
	}
	return class150_sub1;
    }
    
    public Iterator iterator() {
	return new Class636(this);
    }
    
    public int method85() {
	return super.method90((byte) 1);
    }
    
    public void method17885() {
	synchronized (aClass203_11376) {
	    aClass203_11376.method3884((byte) -31);
	}
    }
    
    public Iterator method17886() {
	return new Class636(this);
    }
    
    public Iterator method17887() {
	return new Class636(this);
    }
    
    Class150_Sub1 method17888(int i) {
	byte[] is
	    = aClass472_11377.method7743((aClass453_8978.method7394((byte) 64)
					  .anInt8385) * 1570156841,
					 i, -1983730782);
	Class150_Sub1 class150_sub1 = new Class150_Sub1(aClass453_8978, i);
	if (is != null)
	    class150_sub1.method79(new Class534_Sub40(is), (byte) 3);
	return class150_sub1;
    }
    
    Class150_Sub1 method17889(int i) {
	byte[] is
	    = aClass472_11377.method7743((aClass453_8978.method7394((byte) 64)
					  .anInt8385) * 1570156841,
					 i, -1096143672);
	Class150_Sub1 class150_sub1 = new Class150_Sub1(aClass453_8978, i);
	if (is != null)
	    class150_sub1.method79(new Class534_Sub40(is), (byte) 3);
	return class150_sub1;
    }
    
    public Interface13 method89(int i) {
	Class150_Sub1 class150_sub1;
	synchronized (aClass203_11376) {
	    class150_sub1
		= (Class150_Sub1) aClass203_11376.method3871((long) i);
	    if (class150_sub1 == null) {
		class150_sub1 = method17881(i, -537530830);
		aClass203_11376.method3893(class150_sub1, (long) i);
	    }
	}
	return class150_sub1;
    }
    
    public int method88() {
	return super.method90((byte) 76);
    }
    
    public void method17890() {
	synchronized (aClass203_11376) {
	    aClass203_11376.method3884((byte) 1);
	}
    }
    
    Class150_Sub1 method17891(int i) {
	byte[] is
	    = aClass472_11377.method7743((aClass453_8978.method7394((byte) 64)
					  .anInt8385) * 1570156841,
					 i, -1624409284);
	Class150_Sub1 class150_sub1 = new Class150_Sub1(aClass453_8978, i);
	if (is != null)
	    class150_sub1.method79(new Class534_Sub40(is), (byte) 3);
	return class150_sub1;
    }
    
    public void method17892() {
	synchronized (aClass203_11376) {
	    aClass203_11376.method3884((byte) -50);
	}
    }
    
    public int method90(byte i) {
	return super.method90((byte) 6);
    }
    
    public void method17893() {
	synchronized (aClass203_11376) {
	    aClass203_11376.method3884((byte) -114);
	}
    }
}
