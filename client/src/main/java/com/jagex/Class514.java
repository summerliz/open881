/* Class514 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class514
{
    public int anInt5728;
    public int anInt5729;
    public int anInt5730;
    public int anInt5731;
    
    Class514() {
	anInt5728 = (32 + (int) (Math.random() * 4.0)) * -1207019925;
	anInt5729 = (3 + (int) (Math.random() * 2.0)) * -417960453;
	anInt5730 = (16 + (int) (Math.random() * 3.0)) * 1700028133;
	if (Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub16_10763
		.method17030((byte) -28)
	    == 1)
	    anInt5731 = (int) (Math.random() * 6.0) * 567148163;
	else
	    anInt5731 = (int) (Math.random() * 12.0) * 567148163;
    }
    
    Class514(int i, int i_0_, int i_1_, int i_2_) {
	anInt5728 = -1207019925 * i;
	anInt5729 = i_0_ * -417960453;
	anInt5730 = i_1_ * 1700028133;
	anInt5731 = 567148163 * i_2_;
    }
    
    static final void method8583(Class669 class669, short i) {
	Class606_Sub1 class606_sub1
	    = Class534_Sub18_Sub3.method17878(1675385260);
	if (class606_sub1 != null) {
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= class606_sub1.anInt10861 * 539448607;
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= -1395637313 * class606_sub1.anInt7985;
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= class606_sub1.aString10858;
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= class606_sub1.method16908((byte) 42);
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= class606_sub1.method16906(249979853);
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= 2098404975 * class606_sub1.anInt7981;
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= -663639827 * class606_sub1.anInt10856;
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= class606_sub1.aString10857;
	} else {
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= -1;
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= 0;
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= "";
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= 0;
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= "";
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= 0;
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= 0;
	    class669.anObjectArray8593
		[(class669.anInt8594 += 1460193483) * 1485266147 - 1]
		= "";
	}
    }
    
    static void method8584(int i, int i_3_, int i_4_, int i_5_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(11, (long) i);
	class534_sub18_sub6.method18121(-10715090);
	class534_sub18_sub6.anInt11666 = i_3_ * 517206857;
	class534_sub18_sub6.anInt11660 = -1621355885 * i_4_;
    }
}
