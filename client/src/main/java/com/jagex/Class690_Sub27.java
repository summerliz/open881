/* Class690_Sub27 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class690_Sub27 extends Class690
{
    public static final int anInt10927 = 0;
    static final int anInt10928 = 2;
    
    int method14021() {
	return 0;
    }
    
    public int method14027(int i) {
	if (!Class200_Sub23.method15660(aClass534_Sub35_8752
					    .aClass690_Sub7_10733
					    .method16935(-1807368365),
					-1619781311))
	    return 3;
	return 1;
    }
    
    public void method17118(byte i) {
	if (aClass534_Sub35_8752.aClass690_Sub7_10733.method16938(-881188269)
	    && !Class200_Sub23.method15660(aClass534_Sub35_8752
					       .aClass690_Sub7_10733
					       .method16935(-1807368365),
					   -953710915))
	    anInt8753 = 0;
	if (189295939 * anInt8753 < 0 || 189295939 * anInt8753 > 2)
	    anInt8753 = method14017(2101600379) * 1823770475;
    }
    
    int method14017(int i) {
	return 0;
    }
    
    public Class690_Sub27(Class534_Sub35 class534_sub35) {
	super(class534_sub35);
    }
    
    public int method14026(int i, int i_0_) {
	if (!Class200_Sub23.method15660(aClass534_Sub35_8752
					    .aClass690_Sub7_10733
					    .method16935(-1807368365),
					706301753))
	    return 3;
	return 1;
    }
    
    public Class690_Sub27(int i, Class534_Sub35 class534_sub35) {
	super(i, class534_sub35);
    }
    
    public void method14020(int i, int i_1_) {
	anInt8753 = 1823770475 * i;
    }
    
    public int method17119(byte i) {
	return 189295939 * anInt8753;
    }
    
    public void method17120() {
	if (aClass534_Sub35_8752.aClass690_Sub7_10733.method16938(-881188269)
	    && !Class200_Sub23.method15660(aClass534_Sub35_8752
					       .aClass690_Sub7_10733
					       .method16935(-1807368365),
					   246557439))
	    anInt8753 = 0;
	if (189295939 * anInt8753 < 0 || 189295939 * anInt8753 > 2)
	    anInt8753 = method14017(2141595279) * 1823770475;
    }
    
    int method14018() {
	return 0;
    }
    
    public boolean method17121(byte i) {
	if (!Class200_Sub23.method15660(aClass534_Sub35_8752
					    .aClass690_Sub7_10733
					    .method16935(-1807368365),
					-434473950))
	    return false;
	return true;
    }
    
    public void method14025(int i) {
	anInt8753 = 1823770475 * i;
    }
    
    public void method14023(int i) {
	anInt8753 = 1823770475 * i;
    }
    
    int method14022() {
	return 0;
    }
    
    public int method14028(int i) {
	if (!Class200_Sub23.method15660(aClass534_Sub35_8752
					    .aClass690_Sub7_10733
					    .method16935(-1807368365),
					-1768391135))
	    return 3;
	return 1;
    }
    
    public int method14029(int i) {
	if (!Class200_Sub23.method15660(aClass534_Sub35_8752
					    .aClass690_Sub7_10733
					    .method16935(-1807368365),
					-1827154445))
	    return 3;
	return 1;
    }
    
    public int method14030(int i) {
	if (!Class200_Sub23.method15660(aClass534_Sub35_8752
					    .aClass690_Sub7_10733
					    .method16935(-1807368365),
					1289548116))
	    return 3;
	return 1;
    }
    
    public void method17122() {
	if (aClass534_Sub35_8752.aClass690_Sub7_10733.method16938(-881188269)
	    && !Class200_Sub23.method15660(aClass534_Sub35_8752
					       .aClass690_Sub7_10733
					       .method16935(-1807368365),
					   610819770))
	    anInt8753 = 0;
	if (189295939 * anInt8753 < 0 || 189295939 * anInt8753 > 2)
	    anInt8753 = method14017(2092397633) * 1823770475;
    }
    
    public void method17123() {
	if (aClass534_Sub35_8752.aClass690_Sub7_10733.method16938(-881188269)
	    && !Class200_Sub23.method15660(aClass534_Sub35_8752
					       .aClass690_Sub7_10733
					       .method16935(-1807368365),
					   -252849203))
	    anInt8753 = 0;
	if (189295939 * anInt8753 < 0 || 189295939 * anInt8753 > 2)
	    anInt8753 = method14017(2130683362) * 1823770475;
    }
    
    public void method14024(int i) {
	anInt8753 = 1823770475 * i;
    }
    
    public boolean method17124() {
	if (!Class200_Sub23.method15660(aClass534_Sub35_8752
					    .aClass690_Sub7_10733
					    .method16935(-1807368365),
					1898984054))
	    return false;
	return true;
    }
    
    public boolean method17125() {
	if (!Class200_Sub23.method15660(aClass534_Sub35_8752
					    .aClass690_Sub7_10733
					    .method16935(-1807368365),
					-1787388319))
	    return false;
	return true;
    }
    
    public int method17126() {
	return 189295939 * anInt8753;
    }
    
    static final void method17127(Class669 class669, int i) {
	class669.anInt8600 -= 617999126;
	int i_2_ = class669.anIntArray8595[class669.anInt8600 * 2088438307];
	int i_3_
	    = class669.anIntArray8595[1 + 2088438307 * class669.anInt8600];
	if (i_2_ > 700 || i_3_ > 700)
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= 256;
	else {
	    double d = ((Math.random() * (double) (i_2_ + i_3_) - (double) i_2_
			 + 800.0)
			/ 100.0);
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= (int) (Math.pow(2.0, d) + 0.5);
	}
    }
    
    static void method17128(int i, int i_4_) {
	Class113.anInt1380 = 0;
	int i_5_ = client.aClass512_11100.method8417(375193701);
	int i_6_ = client.aClass512_11100.method8418(-1533611049);
	Class468 class468 = client.aClass512_11100.method8552((byte) 0);
	Class556 class556 = client.aClass512_11100.method8424((byte) 20);
	Class44_Sub13 class44_sub13
	    = client.aClass512_11100.method8428(-1486655428);
	int i_7_ = i;
	if (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419 != null) {
	    int i_8_ = ((Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
			 .anIntArray11977[0])
			>> 3);
	    int i_9_ = ((Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419
			 .anIntArray11978[0])
			>> 3);
	    if (i_8_ >= 0 && i_8_ < Class113.aBoolArrayArray1374.length
		&& i_9_ >= 0
		&& i_9_ < Class113.aBoolArrayArray1374[i_8_].length
		&& Class113.aBoolArrayArray1374[i_8_][i_9_])
		i_7_ = 0;
	}
	for (int i_10_ = 0; i_10_ < i_5_; i_10_++) {
	    for (int i_11_ = 0; i_11_ < i_6_; i_11_++) {
		for (int i_12_ = i_7_; i_12_ <= i + 1 && i_12_ <= 3; i_12_++) {
		    if ((i_12_ < i || class468.method7609(i, i_12_, i_10_,
							  i_11_, 2124754369))
			&& !(Class297.method5346
			     ((Interface62) class556.method9264(i_12_, i_10_,
								i_11_,
								(byte) 19),
			      class44_sub13, i_10_, i_11_, -2039946177))
			&& !(Class297.method5346
			     (((Interface62)
			       class556.method9262(i_12_, i_10_, i_11_,
						   client.anInterface64_11333,
						   (byte) 103)),
			      class44_sub13, i_10_, i_11_, 1928588763))
			&& !(Class297.method5346
			     ((Interface62) class556.method9258(i_12_, i_10_,
								i_11_,
								(byte) -69),
			      class44_sub13, i_10_, i_11_, -1442436883))
			&& !(Class297.method5346
			     ((Interface62) class556.method9357(i_12_, i_10_,
								i_11_,
								(byte) 36),
			      class44_sub13, i_10_, i_11_, 134546257))) {
			/* empty */
		    }
		}
	    }
	}
    }
}
