/* Class658 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public abstract class Class658
{
    Class203 aClass203_8539 = new Class203(2);
    Class472 aClass472_8540;
    public static Class163[] aClass163Array8541;
    
    void method10897() {
	synchronized (aClass203_8539) {
	    aClass203_8539.method3884((byte) -97);
	}
    }
    
    void method10898(byte i) {
	synchronized (aClass203_8539) {
	    aClass203_8539.method3877(438821330);
	}
    }
    
    void method10899(int i, int i_0_) {
	synchronized (aClass203_8539) {
	    aClass203_8539.method3876(i, (byte) 0);
	}
    }
    
    void method10900(int i) {
	synchronized (aClass203_8539) {
	    aClass203_8539.method3884((byte) -36);
	}
    }
    
    void method10901() {
	synchronized (aClass203_8539) {
	    aClass203_8539.method3877(604158350);
	}
    }
    
    void method10902() {
	synchronized (aClass203_8539) {
	    aClass203_8539.method3877(-233887939);
	}
    }
    
    void method10903(int i) {
	synchronized (aClass203_8539) {
	    aClass203_8539.method3876(i, (byte) 0);
	}
    }
    
    void method10904(int i) {
	synchronized (aClass203_8539) {
	    aClass203_8539.method3876(i, (byte) 0);
	}
    }
    
    void method10905(int i) {
	synchronized (aClass203_8539) {
	    aClass203_8539.method3876(i, (byte) 0);
	}
    }
    
    void method10906() {
	synchronized (aClass203_8539) {
	    aClass203_8539.method3884((byte) -57);
	}
    }
    
    Class658(Class472 class472) {
	aClass472_8540 = class472;
    }
    
    static final void method10907(Class669 class669, int i) {
	class669.anInt8600 -= 926998689;
	int i_1_ = class669.anIntArray8595[2088438307 * class669.anInt8600];
	int i_2_
	    = class669.anIntArray8595[1 + class669.anInt8600 * 2088438307];
	int i_3_
	    = class669.anIntArray8595[2 + class669.anInt8600 * 2088438307];
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class200_Sub9.method15582(i_1_, i_2_, i_3_, true, (short) 255);
    }
    
    static final void method10908(Class669 class669, int i) {
	int i_4_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class15 class15
	    = ((Class15)
	       Class531.aClass44_Sub7_7135.method91(i_4_, -1930645483));
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = class15.aBool181 ? 1 : 0;
    }
    
    static final void method10909(Class669 class669, int i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class690_Sub24.method17100(class247, class243, class669, 2039625223);
    }
    
    static final void method10910(Class669 class669, byte i) {
	Class666 class666 = (class669.aBool8615 ? class669.aClass666_8592
			     : class669.aClass666_8599);
	Class247 class247 = class666.aClass247_8574;
	Class243 class243 = class666.aClass243_8575;
	Class536.method8907(class247, class243, class669, (short) -256);
    }
    
    public static void method10911(Class247[] class247s, byte i) {
	for (int i_5_ = 0; i_5_ < class247s.length; i_5_++) {
	    Class247 class247 = class247s[i_5_];
	    if (null != class247.anObjectArray2623) {
		Class534_Sub41 class534_sub41 = new Class534_Sub41();
		class534_sub41.aClass247_10818 = class247;
		class534_sub41.anObjectArray10819 = class247.anObjectArray2623;
		Class690_Sub14.method17010(class534_sub41, 5000000,
					   1673233807);
	    }
	}
    }
    
    static final void method10912(Class534_Sub18_Sub7 class534_sub18_sub7,
				  Class669 class669, short i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class505.method8358(class534_sub18_sub7, (byte) 81);
	class669.anObjectArray8593
	    [(class669.anInt8594 += 1460193483) * 1485266147 - 1]
	    = Class58.method1248(class534_sub18_sub7, 1219122855);
	class669.anObjectArray8593
	    [(class669.anInt8594 += 1460193483) * 1485266147 - 1]
	    = Class473.method7753(class534_sub18_sub7, (byte) 0);
	class669.anObjectArray8593
	    [(class669.anInt8594 += 1460193483) * 1485266147 - 1]
	    = Exception_Sub7.method17949(class534_sub18_sub7, (short) 5888);
    }
    
    static void method10913(Class243 class243, Class247 class247, byte i) {
	if (null != class247) {
	    if (-1 != class247.anInt2580 * 1365669833) {
		Class247 class247_6_
		    = class243.method4476(class247.anInt2472 * -742015869,
					  2115680773);
		if (class247_6_ != null) {
		    if (class247_6_.aClass247Array2620
			== class247_6_.aClass247Array2621) {
			class247_6_.aClass247Array2621
			    = (new Class247
			       [class247_6_.aClass247Array2620.length]);
			class247_6_.aClass247Array2621
			    [class247_6_.aClass247Array2621.length - 1]
			    = class247;
			Class688.method13989(class247_6_.aClass247Array2620, 0,
					     class247_6_.aClass247Array2621, 0,
					     class247.anInt2580 * 1365669833);
			Class688.method13989(class247_6_.aClass247Array2620,
					     (1365669833 * class247.anInt2580
					      + 1),
					     class247_6_.aClass247Array2621,
					     1365669833 * class247.anInt2580,
					     ((class247_6_
					       .aClass247Array2620).length
					      - class247.anInt2580 * 1365669833
					      - 1));
		    } else {
			int i_7_ = 0;
			Class247[] class247s;
			for (class247s = class247_6_.aClass247Array2621;
			     (i_7_ < class247s.length
			      && class247s[i_7_] != class247);
			     i_7_++) {
			    /* empty */
			}
			if (i_7_ < class247s.length) {
			    Class688.method13989(class247s, i_7_ + 1,
						 class247s, i_7_,
						 class247s.length - i_7_ - 1);
			    class247s[(class247_6_.aClass247Array2621.length
				       - 1)]
				= class247;
			}
		    }
		}
	    } else {
		Class247[] class247s = class243.method4475((byte) 126);
		int i_8_;
		for (i_8_ = 0;
		     i_8_ < class247s.length && class247s[i_8_] != class247;
		     i_8_++) {
		    /* empty */
		}
		if (i_8_ < class247s.length) {
		    Class688.method13989(class247s, 1 + i_8_, class247s, i_8_,
					 class247s.length - i_8_ - 1);
		    class247s[class247s.length - 1] = class247;
		}
	    }
	}
    }
    
    static Class593[] method10914(byte i) {
	return (new Class593[]
		{ Class593.aClass593_7823, Class593.aClass593_7822,
		  Class593.aClass593_7821 });
    }
}
