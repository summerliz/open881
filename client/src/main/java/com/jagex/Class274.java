/* Class274 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.util.HashMap;
import java.util.Map;

public class Class274
{
    static int anInt3035;
    static Class9 aClass9_3036;
    static Map aMap3037 = new HashMap();
    static Class696 aClass696_3038;
    static String aString3039;
    public static Class163 aClass163_3040;
    
    public static void method5103() {
	aMap3037.clear();
	aClass9_3036.method578((byte) -29);
	aClass696_3038.method14075(958896847);
	anInt3035 = 0;
    }
    
    static int method5104() {
	return (anInt3035 += -1583824911) * 304107793 - 1;
    }
    
    static int method5105() {
	return (anInt3035 += -1583824911) * 304107793 - 1;
    }
    
    static int method5106() {
	return (anInt3035 += -1583824911) * 304107793 - 1;
    }
    
    public static void method5107(int i, int i_0_, String string,
				  String string_1_, String string_2_,
				  String string_3_, Class407 class407) {
	Class216.method4111(i, i_0_, string, string_1_, string_2_, string_3_,
			    null, -1, class407, (byte) -23);
    }
    
    public static void method5108(int i, String string) {
	Class272.method5067(i, 0, "", "", "", string, null, (byte) 5);
    }
    
    public static int method5109() {
	return anInt3035 * 304107793 - 1;
    }
    
    public static void method5110(String string) {
	Class272.method5067(0, 0, "", "", "", string, null, (byte) 5);
    }
    
    public static void method5111(String string) {
	Class272.method5067(0, 0, "", "", "", string, null, (byte) 5);
    }
    
    public static void method5112(String string) {
	Class272.method5067(0, 0, "", "", "", string, null, (byte) 5);
    }
    
    public static int method5113(int i) {
	Class534_Sub18_Sub14 class534_sub18_sub14
	    = (Class534_Sub18_Sub14) aClass9_3036.method579((long) i);
	if (null == class534_sub18_sub14)
	    return -1;
	if (aClass696_3038.aClass534_Sub18_8785
	    == class534_sub18_sub14.aClass534_Sub18_10508)
	    return -1;
	return (((Class534_Sub18_Sub14)
		 class534_sub18_sub14.aClass534_Sub18_10508).anInt11808
		* -759944081);
    }
    
    public static void method5114(int i, String string) {
	Class272.method5067(i, 0, "", "", "", string, null, (byte) 5);
    }
    
    public static int method5115(int i) {
	Class270 class270 = (Class270) aMap3037.get(Integer.valueOf(i));
	if (class270 == null)
	    return 0;
	return class270.method5026(-2142634232);
    }
    
    public static void method5116(String string) {
	Class272.method5067(0, 0, "", "", "", string, null, (byte) 5);
    }
    
    public static void method5117() {
	aMap3037.clear();
	aClass9_3036.method578((byte) 20);
	aClass696_3038.method14075(958896847);
	anInt3035 = 0;
    }
    
    public static int method5118() {
	return anInt3035 * 304107793 - 1;
    }
    
    public static void method5119(int i, int i_4_, String string,
				  String string_5_, String string_6_,
				  String string_7_, Class407 class407) {
	Class216.method4111(i, i_4_, string, string_5_, string_6_, string_7_,
			    null, -1, class407, (byte) 57);
    }
    
    public static void method5120(int i, int i_8_, String string,
				  String string_9_, String string_10_,
				  String string_11_, Class407 class407) {
	Class216.method4111(i, i_8_, string, string_9_, string_10_, string_11_,
			    null, -1, class407, (byte) -22);
    }
    
    public static Class534_Sub18_Sub14 method5121(int i, int i_12_) {
	Class270 class270 = (Class270) aMap3037.get(Integer.valueOf(i));
	return class270.method5029(i_12_, 1877299679);
    }
    
    public static void method5122
	(int i, int i_13_, String string, String string_14_, String string_15_,
	 String string_16_, String string_17_, int i_18_, Class407 class407) {
	Class270 class270 = (Class270) aMap3037.get(Integer.valueOf(i));
	if (class270 == null) {
	    class270 = new Class270();
	    aMap3037.put(Integer.valueOf(i), class270);
	}
	Class534_Sub18_Sub14 class534_sub18_sub14
	    = class270.method5025(i, i_13_, string, string_14_, string_15_,
				  string_16_, string_17_, i_18_, class407,
				  (byte) 47);
	aClass9_3036.method581(class534_sub18_sub14,
			       (long) (class534_sub18_sub14.anInt11808
				       * -759944081));
	aClass696_3038.method14076(class534_sub18_sub14, (byte) 94);
	client.anInt11045 = -910438199 * client.anInt11095;
    }
    
    public static void method5123
	(int i, int i_19_, String string, String string_20_, String string_21_,
	 String string_22_, String string_23_, int i_24_, Class407 class407) {
	Class270 class270 = (Class270) aMap3037.get(Integer.valueOf(i));
	if (class270 == null) {
	    class270 = new Class270();
	    aMap3037.put(Integer.valueOf(i), class270);
	}
	Class534_Sub18_Sub14 class534_sub18_sub14
	    = class270.method5025(i, i_19_, string, string_20_, string_21_,
				  string_22_, string_23_, i_24_, class407,
				  (byte) 26);
	aClass9_3036.method581(class534_sub18_sub14,
			       (long) (class534_sub18_sub14.anInt11808
				       * -759944081));
	aClass696_3038.method14076(class534_sub18_sub14, (byte) 70);
	client.anInt11045 = -910438199 * client.anInt11095;
    }
    
    public static Class534_Sub18_Sub14 method5124(int i, int i_25_) {
	Class270 class270 = (Class270) aMap3037.get(Integer.valueOf(i));
	return class270.method5029(i_25_, 646606627);
    }
    
    static {
	aClass9_3036 = new Class9(1024);
	aClass696_3038 = new Class696();
	anInt3035 = 0;
    }
    
    public static Class534_Sub18_Sub14 method5125(int i, int i_26_) {
	Class270 class270 = (Class270) aMap3037.get(Integer.valueOf(i));
	return class270.method5029(i_26_, -736167065);
    }
    
    public static Class534_Sub18_Sub14 method5126(int i) {
	return (Class534_Sub18_Sub14) aClass9_3036.method579((long) i);
    }
    
    public static Class534_Sub18_Sub14 method5127(int i) {
	return (Class534_Sub18_Sub14) aClass9_3036.method579((long) i);
    }
    
    public static void method5128() {
	aMap3037.clear();
	aClass9_3036.method578((byte) 42);
	aClass696_3038.method14075(958896847);
	anInt3035 = 0;
    }
    
    public static int method5129(int i) {
	Class270 class270 = (Class270) aMap3037.get(Integer.valueOf(i));
	if (class270 == null)
	    return 0;
	return class270.method5026(-2058993086);
    }
    
    public static int method5130(int i) {
	Class270 class270 = (Class270) aMap3037.get(Integer.valueOf(i));
	if (class270 == null)
	    return 0;
	return class270.method5026(-1996211418);
    }
    
    public static int method5131(int i) {
	Class270 class270 = (Class270) aMap3037.get(Integer.valueOf(i));
	if (class270 == null)
	    return 0;
	return class270.method5026(-1986385353);
    }
    
    public static int method5132(int i) {
	Class270 class270 = (Class270) aMap3037.get(Integer.valueOf(i));
	if (class270 == null)
	    return 0;
	return class270.method5026(-2047307784);
    }
    
    Class274() throws Throwable {
	throw new Error();
    }
    
    public static void method5133
	(int i, int i_27_, String string, String string_28_, String string_29_,
	 String string_30_, String string_31_, int i_32_, Class407 class407) {
	Class270 class270 = (Class270) aMap3037.get(Integer.valueOf(i));
	if (class270 == null) {
	    class270 = new Class270();
	    aMap3037.put(Integer.valueOf(i), class270);
	}
	Class534_Sub18_Sub14 class534_sub18_sub14
	    = class270.method5025(i, i_27_, string, string_28_, string_29_,
				  string_30_, string_31_, i_32_, class407,
				  (byte) 14);
	aClass9_3036.method581(class534_sub18_sub14,
			       (long) (class534_sub18_sub14.anInt11808
				       * -759944081));
	aClass696_3038.method14076(class534_sub18_sub14, (byte) 10);
	client.anInt11045 = -910438199 * client.anInt11095;
    }
    
    public static void method5134() {
	aMap3037.clear();
	aClass9_3036.method578((byte) -67);
	aClass696_3038.method14075(958896847);
	anInt3035 = 0;
    }
    
    public static void method5135(int i, String string) {
	Class272.method5067(i, 0, "", "", "", string, null, (byte) 5);
    }
    
    public static void method5136() {
	aMap3037.clear();
	aClass9_3036.method578((byte) -2);
	aClass696_3038.method14075(958896847);
	anInt3035 = 0;
    }
    
    public static int method5137(int i) {
	Class534_Sub18_Sub14 class534_sub18_sub14
	    = (Class534_Sub18_Sub14) aClass9_3036.method579((long) i);
	if (null == class534_sub18_sub14)
	    return -1;
	if (aClass696_3038.aClass534_Sub18_8785
	    == class534_sub18_sub14.aClass534_Sub18_10508)
	    return -1;
	return (((Class534_Sub18_Sub14)
		 class534_sub18_sub14.aClass534_Sub18_10508).anInt11808
		* -759944081);
    }
    
    public static void method5138(int i, String string) {
	Class272.method5067(i, 0, "", "", "", string, null, (byte) 5);
    }
    
    public static int method5139(int i) {
	Class534_Sub18_Sub14 class534_sub18_sub14
	    = (Class534_Sub18_Sub14) aClass9_3036.method579((long) i);
	if (null == class534_sub18_sub14)
	    return -1;
	if (aClass696_3038.aClass534_Sub18_8785
	    == class534_sub18_sub14.aClass534_Sub18_10508)
	    return -1;
	return (((Class534_Sub18_Sub14)
		 class534_sub18_sub14.aClass534_Sub18_10508).anInt11808
		* -759944081);
    }
    
    public static int method5140(int i) {
	Class534_Sub18_Sub14 class534_sub18_sub14
	    = (Class534_Sub18_Sub14) aClass9_3036.method579((long) i);
	if (null == class534_sub18_sub14)
	    return -1;
	if (class534_sub18_sub14.aClass534_Sub18_10510
	    == aClass696_3038.aClass534_Sub18_8785)
	    return -1;
	return (((Class534_Sub18_Sub14)
		 class534_sub18_sub14.aClass534_Sub18_10510).anInt11808
		* -759944081);
    }
    
    static final void method5141(Class669 class669, int i) {
	int i_33_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_33_, 652363219);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_33_ >> 16];
	Class60.method1255(class247, class243, class669, -124082767);
    }
    
    static final void method5142(Class669 class669, short i) {
	class669.anInt8600 -= 308999563;
    }
    
    public static void method5143(byte i) {
	if (client.anInt11039 * -1850530127 == 18) {
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4209,
				      client.aClass100_11094.aClass13_1183,
				      1341391005);
	    client.aClass100_11094.method1863(class534_sub19, (byte) 47);
	    Class331.aClass702_3495 = Class702.aClass702_8812;
	    Class534_Sub1_Sub2.aString11720 = null;
	}
    }
    
    static void method5144(Class564 class564, int i, int i_34_, int i_35_,
			   Class183 class183, int i_36_) {
	if (null != class183)
	    class564.method9514(i, i_34_, i_35_, class183.method3041(),
				class183.method3134(), class183.method3043(),
				class183.method3045(), class183.method3046(),
				class183.method3047(), class183.method3038());
    }
}
