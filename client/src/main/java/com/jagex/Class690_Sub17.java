/* Class690_Sub17 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class690_Sub17 extends Class690
{
    public static final int anInt10897 = 3;
    public static final int anInt10898 = 0;
    public static final int anInt10899 = 2;
    public static final int anInt10900 = 1;
    
    void method14024(int i) {
	anInt8753 = i * 1823770475;
    }
    
    public Class690_Sub17(int i, Class534_Sub35 class534_sub35) {
	super(i, class534_sub35);
    }
    
    public void method17034(int i) {
	if (aClass534_Sub35_8752.aClass690_Sub13_10761
		.method16993((byte) -114) == 2
	    && 189295939 * anInt8753 == 2)
	    anInt8753 = 1823770475;
	if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 3)
	    anInt8753 = method14017(2141508548) * 1823770475;
    }
    
    int method14022() {
	return 2;
    }
    
    public Class690_Sub17(Class534_Sub35 class534_sub35) {
	super(class534_sub35);
    }
    
    void method14020(int i, int i_0_) {
	anInt8753 = i * 1823770475;
    }
    
    public int method17035(byte i) {
	return anInt8753 * 189295939;
    }
    
    int method14021() {
	return 2;
    }
    
    int method14026(int i, int i_1_) {
	return 1;
    }
    
    int method14018() {
	return 2;
    }
    
    int method14027(int i) {
	return 1;
    }
    
    void method14025(int i) {
	anInt8753 = i * 1823770475;
    }
    
    void method14023(int i) {
	anInt8753 = i * 1823770475;
    }
    
    public int method17036() {
	return anInt8753 * 189295939;
    }
    
    int method14028(int i) {
	return 1;
    }
    
    int method14029(int i) {
	return 1;
    }
    
    int method14030(int i) {
	return 1;
    }
    
    public void method17037() {
	if ((aClass534_Sub35_8752.aClass690_Sub13_10761.method16993((byte) 64)
	     == 2)
	    && 189295939 * anInt8753 == 2)
	    anInt8753 = 1823770475;
	if (anInt8753 * 189295939 < 0 || anInt8753 * 189295939 > 3)
	    anInt8753 = method14017(2088595744) * 1823770475;
    }
    
    public int method17038() {
	return anInt8753 * 189295939;
    }
    
    int method14017(int i) {
	return 2;
    }
}
