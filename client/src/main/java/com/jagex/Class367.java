/* Class367 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public abstract class Class367
{
    static String aString3856;
    
    public abstract Object method6334(int i, int i_0_, Class382 class382,
				      Class386 class386, int i_1_, float f,
				      int i_2_);
    
    public abstract int method6335(Object object);
    
    public abstract Class369 method6336();
    
    abstract Object method6337(Class388 class388, int i);
    
    public abstract void method6338(Object object, byte[] is, int i, int i_3_,
				    byte i_4_);
    
    public abstract int method6339(Object object, int i);
    
    public abstract Class479 method6340(Class496 class496, int i);
    
    public abstract void method6341(int i);
    
    public abstract Class369 method6342(byte i);
    
    public abstract void method6343(int i);
    
    Class367() {
	/* empty */
    }
    
    public abstract Object method6344(int i, int i_5_, Class382 class382,
				      Class386 class386, int i_6_, float f);
    
    abstract Object method6345(Class388 class388);
    
    public abstract void method6346(Object object, int i);
    
    public abstract void method6347();
    
    public abstract Object method6348(int i, int i_7_, Class382 class382,
				      Class386 class386, int i_8_, float f);
    
    public abstract Class479 method6349(Class496 class496);
    
    public abstract Object method6350(int i, int i_9_, Class382 class382,
				      Class386 class386, int i_10_, float f);
    
    public abstract void method6351(Object object);
    
    public abstract void method6352(Object object, byte[] is, int i,
				    int i_11_);
    
    public abstract void method6353(Object object, byte[] is, int i,
				    int i_12_);
    
    public abstract Class479 method6354(Class496 class496);
    
    public abstract Class479 method6355(Class496 class496);
    
    public abstract int method6356(Object object);
    
    public abstract Class479 method6357(Class496 class496);
    
    public abstract int method6358(Object object);
    
    abstract Object method6359(Class388 class388);
    
    public abstract void method6360();
    
    public abstract void method6361();
    
    public abstract void method6362();
    
    public abstract void method6363();
    
    public abstract void method6364();
    
    static final void method6365(Class669 class669, int i) {
	int i_13_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_13_, -356036628);
	Class43.method1074(class247, class669, -861406211);
    }
    
    public static void method6366(boolean bool, byte[] is, int i) {
	if (Class534_Sub8.aClass534_Sub40_10425 == null)
	    Class534_Sub8.aClass534_Sub40_10425 = new Class534_Sub40(20000);
	Class534_Sub8.aClass534_Sub40_10425.method16519(is, 0, is.length,
							-40501768);
	if (bool) {
	    Class6.method566((Class534_Sub8.aClass534_Sub40_10425
			      .aByteArray10810),
			     (byte) 95);
	    Class5.aClass606_Sub1Array42
		= new Class606_Sub1[Class542.anInt7183 * 1699576543];
	    int i_14_ = 0;
	    for (int i_15_ = 206492647 * Class619.anInt8108;
		 i_15_ <= -1664594981 * Class619.anInt8109; i_15_++) {
		Class606_Sub1 class606_sub1
		    = Class521.method8688(i_15_, -349785923);
		if (class606_sub1 != null)
		    Class5.aClass606_Sub1Array42[i_14_++] = class606_sub1;
	    }
	    Class5.aBool45 = false;
	    Class5.aLong46
		= Class250.method4604((byte) -123) * -7774518681047063557L;
	    Class534_Sub8.aClass534_Sub40_10425 = null;
	}
    }
    
    static final void method6367(Class669 class669, int i) {
	int i_16_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_16_, -365325513);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_16_ >> 16];
	Class534_Sub20.method16195(class247, class243, false, 2, class669,
				   -497788361);
    }
}
