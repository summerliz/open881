/* Class654_Sub1_Sub3_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class654_Sub1_Sub3_Sub2 extends Class654_Sub1_Sub3
    implements Interface62
{
    boolean aBool12033 = true;
    boolean aBool12034;
    Class564 aClass564_12035;
    public Class528 aClass528_12036;
    
    boolean method16882(Class185 class185, int i, int i_0_) {
	Class602 class602 = aClass528_12036.method8786(1460193483);
	if (null != class602.aClass432_7956)
	    return class185.method3309(i, i_0_, method10834(),
				       class602.aClass432_7956, 1554530302);
	Class183 class183 = aClass528_12036.method8787(class185, 131072, false,
						       false, 1456702509);
	if (null == class183)
	    return false;
	return class183.method3039(i, i_0_, method10834(), false, 0);
    }
    
    boolean method16849(int i) {
	return false;
    }
    
    boolean method16850(int i) {
	return aBool12033;
    }
    
    public Class564 method16855(Class185 class185, short i) {
	return aClass564_12035;
    }
    
    public int method16876(int i) {
	return aClass528_12036.method8805((byte) 58);
    }
    
    public int method145() {
	return -2129482149 * aClass528_12036.anInt7111;
    }
    
    public void method18644(Class596 class596, byte i) {
	aClass528_12036.method8783(class596, (byte) -50);
    }
    
    Class550 method16853(Class185 class185, int i) {
	Class183 class183 = aClass528_12036.method8787(class185, 2048, false,
						       true, 1798239562);
	if (class183 == null)
	    return null;
	Class446 class446 = class185.method3665();
	class446.method7236(method10834());
	class446.method7287((float) aShort11869, 0.0F, (float) aShort11867);
	Class444 class444 = method10807();
	Class550 class550 = Class322.method5779(aBool12034, -885065281);
	int i_1_ = (int) class444.aClass438_4885.aFloat4864 >> 9;
	int i_2_ = (int) class444.aClass438_4885.aFloat4865 >> 9;
	aClass528_12036.method8788(class185, class183, class446, i_1_, i_1_,
				   i_2_, i_2_, true, (byte) -95);
	Class602 class602 = aClass528_12036.method8786(1460193483);
	if (null != class602.aClass432_7956) {
	    class183.method3034(class446, null, 0);
	    class185.method3311(class446, aClass194Array10852[0],
				class602.aClass432_7956);
	} else
	    class183.method3034(class446, aClass194Array10852[0], 0);
	if (aClass528_12036.aClass629_7105 != null) {
	    Class174 class174 = aClass528_12036.aClass629_7105.method10405();
	    class185.method3334(class174);
	}
	aBool12033
	    = class183.method3027() || null != aClass528_12036.aClass629_7105;
	if (aClass564_12035 == null)
	    aClass564_12035
		= Class528.method8820((int) class444.aClass438_4885.aFloat4864,
				      (int) class444.aClass438_4885.aFloat4863,
				      (int) class444.aClass438_4885.aFloat4865,
				      class183, (byte) 69);
	else
	    Class274.method5144(aClass564_12035,
				(int) class444.aClass438_4885.aFloat4864,
				(int) class444.aClass438_4885.aFloat4863,
				(int) class444.aClass438_4885.aFloat4865,
				class183, 1570709169);
	return class550;
    }
    
    void method16868(Class185 class185, int i) {
	Class183 class183 = aClass528_12036.method8787(class185, 262144, false,
						       true, 1717603895);
	if (class183 != null) {
	    Class446 class446 = method10834();
	    Class444 class444 = method10807();
	    int i_3_ = (int) class444.aClass438_4885.aFloat4864 >> 9;
	    int i_4_ = (int) class444.aClass438_4885.aFloat4865 >> 9;
	    aClass528_12036.method8788(class185, class183, class446, i_3_,
				       i_3_, i_4_, i_4_, false, (byte) -115);
	}
    }
    
    boolean method16846(Class185 class185, int i, int i_5_, byte i_6_) {
	Class602 class602 = aClass528_12036.method8786(1460193483);
	if (null != class602.aClass432_7956)
	    return class185.method3309(i, i_5_, method10834(),
				       class602.aClass432_7956, 1554530302);
	Class183 class183 = aClass528_12036.method8787(class185, 131072, false,
						       false, 1957383462);
	if (null == class183)
	    return false;
	return class183.method3039(i, i_5_, method10834(), false, 0);
    }
    
    public int method16866() {
	return aClass528_12036.method8805((byte) 86);
    }
    
    public void method408(Class185 class185, byte i) {
	aClass528_12036.method8790(class185, -1661660889);
    }
    
    public int method410(int i) {
	return -2129482149 * aClass528_12036.anInt7111;
    }
    
    public void method411(int i) {
	/* empty */
    }
    
    public boolean method413(byte i) {
	return true;
    }
    
    boolean method16864() {
	return false;
    }
    
    public int method56(int i) {
	return aClass528_12036.anInt7103 * 1626333597;
    }
    
    public void method414(Class185 class185, int i) {
	aClass528_12036.method8791(class185, 198005613);
    }
    
    boolean method16874(Class185 class185, int i, int i_7_) {
	Class602 class602 = aClass528_12036.method8786(1460193483);
	if (null != class602.aClass432_7956)
	    return class185.method3309(i, i_7_, method10834(),
				       class602.aClass432_7956, 1554530302);
	Class183 class183 = aClass528_12036.method8787(class185, 131072, false,
						       false, 1563201056);
	if (null == class183)
	    return false;
	return class183.method3039(i, i_7_, method10834(), false, 0);
    }
    
    public int method16875() {
	return aClass528_12036.method8789((byte) 1);
    }
    
    public int method181() {
	return -2129482149 * aClass528_12036.anInt7111;
    }
    
    public int method16867() {
	return aClass528_12036.method8805((byte) 52);
    }
    
    public void method144() {
	/* empty */
    }
    
    public int method409(int i) {
	return aClass528_12036.anInt7104 * -1932952217;
    }
    
    public void method417(Class185 class185) {
	aClass528_12036.method8790(class185, -377888320);
    }
    
    public void method422(Class185 class185) {
	aClass528_12036.method8790(class185, -764955922);
    }
    
    public void method418(Class185 class185) {
	aClass528_12036.method8790(class185, -1046085486);
    }
    
    public void method421(Class185 class185) {
	aClass528_12036.method8791(class185, 198005613);
    }
    
    public void method420(Class185 class185) {
	aClass528_12036.method8791(class185, 198005613);
    }
    
    boolean method16895() {
	return false;
    }
    
    boolean method16879() {
	return aBool12033;
    }
    
    public int method9() {
	return aClass528_12036.anInt7104 * -1932952217;
    }
    
    boolean method16869() {
	return aBool12033;
    }
    
    public Class654_Sub1_Sub3_Sub2
	(Class556 class556, Class185 class185, Class44_Sub13 class44_sub13,
	 Class602 class602, int i, int i_8_, int i_9_, int i_10_, int i_11_,
	 boolean bool, int i_12_, int i_13_, int i_14_, int i_15_, int i_16_,
	 int i_17_) {
	super(class556, i_9_, i_10_, i_11_, i, i_8_, i_12_, i_13_);
	aClass528_12036
	    = new Class528(class185, class44_sub13, class602, i_14_, i_15_,
			   i_8_, this, bool, i_16_, i_17_);
	aBool12034 = -2134171963 * class602.anInt7907 != 0 && !bool;
	method16862(1, -1506666972);
    }
    
    void method16871(Class185 class185) {
	Class183 class183 = aClass528_12036.method8787(class185, 262144, false,
						       true, 1635788967);
	if (class183 != null) {
	    Class446 class446 = method10834();
	    Class444 class444 = method10807();
	    int i = (int) class444.aClass438_4885.aFloat4864 >> 9;
	    int i_18_ = (int) class444.aClass438_4885.aFloat4865 >> 9;
	    aClass528_12036.method8788(class185, class183, class446, i, i,
				       i_18_, i_18_, false, (byte) -117);
	}
    }
    
    public Class564 method16872(Class185 class185) {
	return aClass564_12035;
    }
    
    public Class564 method16870(Class185 class185) {
	return aClass564_12035;
    }
    
    public int method16897(int i) {
	return aClass528_12036.method8789((byte) 1);
    }
    
    boolean method16880(Class185 class185, int i, int i_19_) {
	Class602 class602 = aClass528_12036.method8786(1460193483);
	if (null != class602.aClass432_7956)
	    return class185.method3309(i, i_19_, method10834(),
				       class602.aClass432_7956, 1554530302);
	Class183 class183 = aClass528_12036.method8787(class185, 131072, false,
						       false, 1360546001);
	if (null == class183)
	    return false;
	return class183.method3039(i, i_19_, method10834(), false, 0);
    }
    
    boolean method16873(Class185 class185, int i, int i_20_) {
	Class602 class602 = aClass528_12036.method8786(1460193483);
	if (null != class602.aClass432_7956)
	    return class185.method3309(i, i_20_, method10834(),
				       class602.aClass432_7956, 1554530302);
	Class183 class183 = aClass528_12036.method8787(class185, 131072, false,
						       false, 1939711919);
	if (null == class183)
	    return false;
	return class183.method3039(i, i_20_, method10834(), false, 0);
    }
    
    public int method253() {
	return aClass528_12036.anInt7103 * 1626333597;
    }
    
    public boolean method416() {
	return aClass528_12036.method8809((byte) 77);
    }
    
    public int method252() {
	return aClass528_12036.anInt7103 * 1626333597;
    }
    
    public int method16854() {
	return aClass528_12036.method8789((byte) 1);
    }
    
    public int method254() {
	return aClass528_12036.anInt7103 * 1626333597;
    }
    
    public boolean method412() {
	return true;
    }
    
    public boolean method415() {
	return true;
    }
    
    public void method141() {
	/* empty */
    }
    
    public boolean method423() {
	return true;
    }
    
    public void method18645(Class596 class596) {
	aClass528_12036.method8783(class596, (byte) 7);
    }
    
    public boolean method419(byte i) {
	return aClass528_12036.method8809((byte) 77);
    }
    
    Class550 method16884(Class185 class185) {
	Class183 class183 = aClass528_12036.method8787(class185, 2048, false,
						       true, 1416713512);
	if (class183 == null)
	    return null;
	Class446 class446 = class185.method3665();
	class446.method7236(method10834());
	class446.method7287((float) aShort11869, 0.0F, (float) aShort11867);
	Class444 class444 = method10807();
	Class550 class550 = Class322.method5779(aBool12034, 1661972238);
	int i = (int) class444.aClass438_4885.aFloat4864 >> 9;
	int i_21_ = (int) class444.aClass438_4885.aFloat4865 >> 9;
	aClass528_12036.method8788(class185, class183, class446, i, i, i_21_,
				   i_21_, true, (byte) -30);
	Class602 class602 = aClass528_12036.method8786(1460193483);
	if (null != class602.aClass432_7956) {
	    class183.method3034(class446, null, 0);
	    class185.method3311(class446, aClass194Array10852[0],
				class602.aClass432_7956);
	} else
	    class183.method3034(class446, aClass194Array10852[0], 0);
	if (aClass528_12036.aClass629_7105 != null) {
	    Class174 class174 = aClass528_12036.aClass629_7105.method10405();
	    class185.method3334(class174);
	}
	aBool12033
	    = class183.method3027() || null != aClass528_12036.aClass629_7105;
	if (aClass564_12035 == null)
	    aClass564_12035
		= Class528.method8820((int) class444.aClass438_4885.aFloat4864,
				      (int) class444.aClass438_4885.aFloat4863,
				      (int) class444.aClass438_4885.aFloat4865,
				      class183, (byte) 123);
	else
	    Class274.method5144(aClass564_12035,
				(int) class444.aClass438_4885.aFloat4864,
				(int) class444.aClass438_4885.aFloat4863,
				(int) class444.aClass438_4885.aFloat4865,
				class183, -1973815221);
	return class550;
    }
    
    public boolean method260() {
	return true;
    }
}
