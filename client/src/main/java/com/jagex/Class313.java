/* Class313 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class313
{
    Class472 aClass472_3372;
    Class472 aClass472_3373;
    Class315 aClass315_3374;
    
    public Class313(Class472 class472, Class472 class472_0_) {
	aClass472_3372 = class472;
	aClass472_3373 = class472_0_;
    }
    
    public Interface32 method5683(Interface47 interface47, int i) {
	if (interface47 == null)
	    return null;
	Class397 class397 = interface47.method348(1621519520);
	if (class397 == Class397.aClass397_4112)
	    return new Class329((Class398) interface47);
	if (Class397.aClass397_4108 == class397)
	    return new Class333(method5685(-680344217),
				(Class400) interface47);
	if (Class397.aClass397_4113 == class397)
	    return new Class331(aClass472_3372, (Class392) interface47);
	if (class397 == Class397.aClass397_4114)
	    return new Class331_Sub1(aClass472_3372,
				     (Class392_Sub1) interface47);
	if (class397 == Class397.aClass397_4109)
	    return new Class312_Sub3(aClass472_3372, aClass472_3373,
				     (Class394_Sub3) interface47);
	if (class397 == Class397.aClass397_4110)
	    return new Class312_Sub2(aClass472_3372, aClass472_3373,
				     (Class394_Sub2) interface47);
	if (class397 == Class397.aClass397_4111)
	    return new Class312_Sub1(aClass472_3372, aClass472_3373,
				     (Class394_Sub1) interface47);
	if (Class397.aClass397_4116 == class397)
	    return new Class319(aClass472_3372, aClass472_3373,
				(Class390) interface47);
	if (Class397.aClass397_4115 == class397)
	    return new Class326(aClass472_3372, (Class389) interface47);
	if (class397 == Class397.aClass397_4117)
	    return new Class312_Sub1_Sub1(aClass472_3372, aClass472_3373,
					  (Class394_Sub1_Sub1) interface47);
	if (Class397.aClass397_4118 == class397)
	    return new Class322(aClass472_3372, aClass472_3373,
				(Class393) interface47);
	return null;
    }
    
    Class315 method5684() {
	if (null == aClass315_3374)
	    aClass315_3374 = new Class315();
	return aClass315_3374;
    }
    
    Class315 method5685(int i) {
	if (null == aClass315_3374)
	    aClass315_3374 = new Class315();
	return aClass315_3374;
    }
    
    public Interface32 method5686(Interface47 interface47) {
	if (interface47 == null)
	    return null;
	Class397 class397 = interface47.method348(1010981009);
	if (class397 == Class397.aClass397_4112)
	    return new Class329((Class398) interface47);
	if (Class397.aClass397_4108 == class397)
	    return new Class333(method5685(-1045510151),
				(Class400) interface47);
	if (Class397.aClass397_4113 == class397)
	    return new Class331(aClass472_3372, (Class392) interface47);
	if (class397 == Class397.aClass397_4114)
	    return new Class331_Sub1(aClass472_3372,
				     (Class392_Sub1) interface47);
	if (class397 == Class397.aClass397_4109)
	    return new Class312_Sub3(aClass472_3372, aClass472_3373,
				     (Class394_Sub3) interface47);
	if (class397 == Class397.aClass397_4110)
	    return new Class312_Sub2(aClass472_3372, aClass472_3373,
				     (Class394_Sub2) interface47);
	if (class397 == Class397.aClass397_4111)
	    return new Class312_Sub1(aClass472_3372, aClass472_3373,
				     (Class394_Sub1) interface47);
	if (Class397.aClass397_4116 == class397)
	    return new Class319(aClass472_3372, aClass472_3373,
				(Class390) interface47);
	if (Class397.aClass397_4115 == class397)
	    return new Class326(aClass472_3372, (Class389) interface47);
	if (class397 == Class397.aClass397_4117)
	    return new Class312_Sub1_Sub1(aClass472_3372, aClass472_3373,
					  (Class394_Sub1_Sub1) interface47);
	if (Class397.aClass397_4118 == class397)
	    return new Class322(aClass472_3372, aClass472_3373,
				(Class393) interface47);
	return null;
    }
    
    public Interface32 method5687(Interface47 interface47) {
	if (interface47 == null)
	    return null;
	Class397 class397 = interface47.method348(1202788661);
	if (class397 == Class397.aClass397_4112)
	    return new Class329((Class398) interface47);
	if (Class397.aClass397_4108 == class397)
	    return new Class333(method5685(-766923228),
				(Class400) interface47);
	if (Class397.aClass397_4113 == class397)
	    return new Class331(aClass472_3372, (Class392) interface47);
	if (class397 == Class397.aClass397_4114)
	    return new Class331_Sub1(aClass472_3372,
				     (Class392_Sub1) interface47);
	if (class397 == Class397.aClass397_4109)
	    return new Class312_Sub3(aClass472_3372, aClass472_3373,
				     (Class394_Sub3) interface47);
	if (class397 == Class397.aClass397_4110)
	    return new Class312_Sub2(aClass472_3372, aClass472_3373,
				     (Class394_Sub2) interface47);
	if (class397 == Class397.aClass397_4111)
	    return new Class312_Sub1(aClass472_3372, aClass472_3373,
				     (Class394_Sub1) interface47);
	if (Class397.aClass397_4116 == class397)
	    return new Class319(aClass472_3372, aClass472_3373,
				(Class390) interface47);
	if (Class397.aClass397_4115 == class397)
	    return new Class326(aClass472_3372, (Class389) interface47);
	if (class397 == Class397.aClass397_4117)
	    return new Class312_Sub1_Sub1(aClass472_3372, aClass472_3373,
					  (Class394_Sub1_Sub1) interface47);
	if (Class397.aClass397_4118 == class397)
	    return new Class322(aClass472_3372, aClass472_3373,
				(Class393) interface47);
	return null;
    }
    
    static void method5688(int[] is, double d) {
	if (d != Class179.aDouble1854) {
	    for (int i = 0; i < 256; i++) {
		int i_1_ = (int) (Math.pow((double) i / 255.0, d) * 255.0);
		Class179.anIntArray1855[i] = i_1_ > 255 ? 255 : i_1_;
	    }
	    Class179.aDouble1854 = d;
	}
	for (int i = 0; i < is.length; i++) {
	    int i_2_ = Class179.anIntArray1855[is[i] >> 16 & 0xff];
	    int i_3_ = Class179.anIntArray1855[is[i] >> 8 & 0xff];
	    int i_4_ = Class179.anIntArray1855[is[i] >> 0 & 0xff];
	    is[i] = is[i] & ~0xffffff | i_2_ << 16 | i_3_ << 8 | i_4_;
	}
    }
    
    public static void method5689(int i) {
	if (Class393.method6554(683293173))
	    Class584.method9841(new Class646(), (byte) 112);
    }
    
    static final void method5690(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 0;
    }
    
    static boolean method5691(int i, int i_5_) {
	return 15 == i || 13 == i || 12 == i || i == 18 || i == 19 || i == 0;
    }
}
