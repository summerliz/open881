/* Class536_Sub5 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class536_Sub5 extends Class536
{
    Class709 aClass709_10367 = new Class709();
    public Class579 aClass579_10368;
    
    public Class536_Sub5(Class579 class579) {
	aClass579_10368 = class579;
    }
    
    public void method15996(int i, int i_0_, int i_1_, int i_2_, short i_3_) {
	Class536_Sub1 class536_sub1 = null;
	int i_4_ = 0;
	for (Class536_Sub1 class536_sub1_5_
		 = (Class536_Sub1) aClass709_10367.method14301(764908544);
	     null != class536_sub1_5_;
	     class536_sub1_5_
		 = (Class536_Sub1) aClass709_10367.method14282(-1434062773)) {
	    i_4_++;
	    if (i == 1770939769 * class536_sub1_5_.anInt10330) {
		class536_sub1_5_.method15931(i, i_0_, i_1_, i_2_, -242147765);
		return;
	    }
	    if (1770939769 * class536_sub1_5_.anInt10330 <= i)
		class536_sub1 = class536_sub1_5_;
	}
	if (null == class536_sub1) {
	    if (i_4_ < Class620.aClass632_8113.anInt8228 * -1207315025)
		aClass709_10367.method14285(new Class536_Sub1(i, i_0_, i_1_,
							      i_2_),
					    (byte) 46);
	} else {
	    Class485.method7963(new Class536_Sub1(i, i_0_, i_1_, i_2_),
				class536_sub1, (byte) 27);
	    if (i_4_ >= Class620.aClass632_8113.anInt8228 * -1207315025)
		aClass709_10367.method14301(764908544).method8900(2092473901);
	}
    }
    
    public Class536_Sub1 method15997(int i, int i_6_) {
	Class536_Sub1 class536_sub1
	    = (Class536_Sub1) aClass709_10367.method14301(764908544);
	if (class536_sub1 == null || class536_sub1.anInt10330 * 1770939769 > i)
	    return null;
	for (Class536_Sub1 class536_sub1_7_
		 = (Class536_Sub1) aClass709_10367.method14282(-503966430);
	     (class536_sub1_7_ != null
	      && 1770939769 * class536_sub1_7_.anInt10330 <= i);
	     class536_sub1_7_
		 = (Class536_Sub1) aClass709_10367.method14282(375969902)) {
	    class536_sub1.method8900(742573442);
	    class536_sub1 = class536_sub1_7_;
	}
	if ((aClass579_10368.anInt7751 * 1929647207
	     + (class536_sub1.anInt10330 * 1770939769
		+ class536_sub1.anInt10332 * -1162579465))
	    > i)
	    return class536_sub1;
	class536_sub1.method8900(598359351);
	return null;
    }
    
    public boolean method15998(byte i) {
	return aClass709_10367.method14288(1042580144);
    }
    
    public void method15999(int i, int i_8_, int i_9_, int i_10_) {
	Class536_Sub1 class536_sub1 = null;
	int i_11_ = 0;
	for (Class536_Sub1 class536_sub1_12_
		 = (Class536_Sub1) aClass709_10367.method14301(764908544);
	     null != class536_sub1_12_;
	     class536_sub1_12_
		 = (Class536_Sub1) aClass709_10367.method14282(-1571481193)) {
	    i_11_++;
	    if (i == 1770939769 * class536_sub1_12_.anInt10330) {
		class536_sub1_12_.method15931(i, i_8_, i_9_, i_10_,
					      1167561134);
		return;
	    }
	    if (1770939769 * class536_sub1_12_.anInt10330 <= i)
		class536_sub1 = class536_sub1_12_;
	}
	if (null == class536_sub1) {
	    if (i_11_ < Class620.aClass632_8113.anInt8228 * -1207315025)
		aClass709_10367.method14285(new Class536_Sub1(i, i_8_, i_9_,
							      i_10_),
					    (byte) 98);
	} else {
	    Class485.method7963(new Class536_Sub1(i, i_8_, i_9_, i_10_),
				class536_sub1, (byte) -44);
	    if (i_11_ >= Class620.aClass632_8113.anInt8228 * -1207315025)
		aClass709_10367.method14301(764908544).method8900(1197754860);
	}
    }
    
    public boolean method16000() {
	return aClass709_10367.method14288(1042580144);
    }
    
    public Class536_Sub1 method16001(int i) {
	Class536_Sub1 class536_sub1
	    = (Class536_Sub1) aClass709_10367.method14301(764908544);
	if (class536_sub1 == null || class536_sub1.anInt10330 * 1770939769 > i)
	    return null;
	for (Class536_Sub1 class536_sub1_13_
		 = (Class536_Sub1) aClass709_10367.method14282(281620113);
	     (class536_sub1_13_ != null
	      && 1770939769 * class536_sub1_13_.anInt10330 <= i);
	     class536_sub1_13_
		 = (Class536_Sub1) aClass709_10367.method14282(522365055)) {
	    class536_sub1.method8900(-641134311);
	    class536_sub1 = class536_sub1_13_;
	}
	if ((aClass579_10368.anInt7751 * 1929647207
	     + (class536_sub1.anInt10330 * 1770939769
		+ class536_sub1.anInt10332 * -1162579465))
	    > i)
	    return class536_sub1;
	class536_sub1.method8900(-1231606678);
	return null;
    }
    
    public boolean method16002() {
	return aClass709_10367.method14288(1042580144);
    }
    
    public boolean method16003() {
	return aClass709_10367.method14288(1042580144);
    }
    
    public boolean method16004() {
	return aClass709_10367.method14288(1042580144);
    }
    
    public void method16005(int i, int i_14_, int i_15_, int i_16_) {
	Class536_Sub1 class536_sub1 = null;
	int i_17_ = 0;
	for (Class536_Sub1 class536_sub1_18_
		 = (Class536_Sub1) aClass709_10367.method14301(764908544);
	     null != class536_sub1_18_;
	     class536_sub1_18_
		 = (Class536_Sub1) aClass709_10367.method14282(1197927570)) {
	    i_17_++;
	    if (i == 1770939769 * class536_sub1_18_.anInt10330) {
		class536_sub1_18_.method15931(i, i_14_, i_15_, i_16_,
					      1696265938);
		return;
	    }
	    if (1770939769 * class536_sub1_18_.anInt10330 <= i)
		class536_sub1 = class536_sub1_18_;
	}
	if (null == class536_sub1) {
	    if (i_17_ < Class620.aClass632_8113.anInt8228 * -1207315025)
		aClass709_10367.method14285(new Class536_Sub1(i, i_14_, i_15_,
							      i_16_),
					    (byte) 3);
	} else {
	    Class485.method7963(new Class536_Sub1(i, i_14_, i_15_, i_16_),
				class536_sub1, (byte) 76);
	    if (i_17_ >= Class620.aClass632_8113.anInt8228 * -1207315025)
		aClass709_10367.method14301(764908544).method8900(1799357271);
	}
    }
    
    static Class675[] method16006(byte i) {
	return (new Class675[]
		{ Class675.aClass675_8639, Class675.aClass675_8636,
		  Class675.aClass675_8635, Class675.aClass675_8638,
		  Class675.aClass675_8637, Class675.aClass675_8634 });
    }
}
