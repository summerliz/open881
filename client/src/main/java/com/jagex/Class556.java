/* Class556 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Class556
{
    public int anInt7418;
    int anInt7419;
    int anInt7420;
    int anInt7421;
    public int anInt7422;
    byte[][] aByteArrayArray7423;
    Class654_Sub1[] aClass654_Sub1Array7424;
    int anInt7425;
    int anInt7426 = 16;
    public Class552 aClass552_7427;
    int anInt7428;
    HashMap aHashMap7429;
    int anInt7430;
    public Class568[][][] aClass568ArrayArrayArray7431;
    public Class151[] aClass151Array7432;
    public Class568[][][] aClass568ArrayArrayArray7433;
    public Class151[] aClass151Array7434;
    public int anInt7435;
    public Class151[] aClass151Array7436;
    int[][] anIntArrayArray7437;
    short[][] aShortArrayArray7438;
    Class561[] aClass561Array7439;
    public static boolean aBool7440 = true;
    byte[][] aByteArrayArray7441;
    int[] anIntArray7442;
    boolean aBool7443;
    int anInt7444 = 10083;
    int anInt7445 = 5098;
    int anInt7446 = 5078;
    Class654_Sub1[] aClass654_Sub1Array7447;
    Class654_Sub1[] aClass654_Sub1Array7448;
    Class654_Sub1[] aClass654_Sub1Array7449;
    int anInt7450;
    int anInt7451;
    Class654_Sub1[] aClass654_Sub1Array7452;
    Class185 aClass185_7453;
    Class654_Sub1_Sub5[] aClass654_Sub1_Sub5Array7454;
    int anInt7455;
    int anInt7456;
    int anInt7457;
    int anInt7458;
    int anInt7459;
    public Class568[][][] aClass568ArrayArrayArray7460;
    int anInt7461;
    int anInt7462;
    int anInt7463;
    int anInt7464 = 65165;
    int anInt7465 = 0;
    Class27 aClass27_7466;
    int anInt7467;
    float[] aFloatArray7468;
    Class534_Sub21[] aClass534_Sub21Array7469;
    public int anInt7470;
    static final int anInt7471 = 4;
    int anInt7472;
    long[][][] aLongArrayArrayArray7473;
    byte[][] aByteArrayArray7474;
    boolean[] aBoolArray7475;
    boolean aBool7476;
    HashMap aHashMap7477;
    public Class557 aClass557_7478;
    boolean[][] aBoolArrayArray7479;
    byte[][] aByteArrayArray7480;
    boolean[][] aBoolArrayArray7481;
    boolean[][] aBoolArrayArray7482;
    
    public Class654_Sub1_Sub1 method9225(int i, int i_0_, int i_1_, int i_2_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_0_][i_1_];
	if (null != class568) {
	    method9257(class568.aClass654_Sub1_Sub1_7597, -1219190831);
	    if (null != class568.aClass654_Sub1_Sub1_7597) {
		Class654_Sub1_Sub1 class654_sub1_sub1
		    = class568.aClass654_Sub1_Sub1_7597;
		class568.aClass654_Sub1_Sub1_7597 = null;
		return class654_sub1_sub1;
	    }
	}
	return null;
    }
    
    public void method9226(int i, int i_3_, int i_4_, int i_5_, byte[][][] is,
			   int[] is_6_, int[] is_7_, int[] is_8_, int[] is_9_,
			   int[] is_10_, int i_11_, byte i_12_, int i_13_,
			   int i_14_, boolean bool, boolean bool_15_,
			   int i_16_, boolean bool_17_) {
	aClass552_7427.aBool7310 = true;
	aBool7476 = bool_15_;
	anInt7455 = (i_3_ >> anInt7422 * 941710601) * 1113634209;
	anInt7459 = 23163765 * (i_5_ >> anInt7422 * 941710601);
	anInt7457 = i_3_ * -1749494531;
	anInt7458 = i_5_ * -1680182669;
	anInt7462 = 973969553 * i_4_;
	anInt7419 = -273223625 * anInt7455 - 562864601 * anInt7461;
	if (anInt7419 * -474621401 < 0) {
	    anInt7425 = -(-1556834553 * anInt7419);
	    anInt7419 = 0;
	} else
	    anInt7425 = 0;
	anInt7430 = -1674151189 * anInt7459 - anInt7461 * 425788937;
	if (anInt7430 * 1375305047 < 0) {
	    anInt7463 = -(anInt7430 * 1723447665);
	    anInt7430 = 0;
	} else
	    anInt7463 = 0;
	anInt7456 = anInt7455 * -1718617707 + anInt7461 * 1700611483;
	if (anInt7456 * 2106025629 > 1183912005 * anInt7435)
	    anInt7456 = anInt7435 * -444609591;
	anInt7467 = anInt7461 * 1281599533 + anInt7459 * -1334631273;
	if (-860025685 * anInt7467 > anInt7470 * 60330541)
	    anInt7467 = anInt7470 * -1909674873;
	boolean[][] bools = aBoolArrayArray7481;
	boolean[][] bools_18_ = aBoolArrayArray7479;
	if (aBool7476) {
	    for (int i_19_ = 0;
		 (i_19_
		  < 2 + (-1213435377 * anInt7461 + anInt7461 * -1213435377));
		 i_19_++) {
		int i_20_ = 0;
		int i_21_ = 0;
		for (int i_22_ = 0;
		     i_22_ < 2 + (-1213435377 * anInt7461
				  + anInt7461 * -1213435377);
		     i_22_++) {
		    if (i_22_ > 1)
			anIntArray7442[i_22_ - 2] = i_20_;
		    i_20_ = i_21_;
		    int i_23_ = (-380604831 * anInt7455
				 - -1213435377 * anInt7461 + i_19_);
		    int i_24_ = i_22_ + (anInt7459 * -1709472547
					 - anInt7461 * -1213435377);
		    if (i_23_ >= 0 && i_24_ >= 0
			&& i_23_ < 1183912005 * anInt7435
			&& i_24_ < 60330541 * anInt7470) {
			int i_25_ = i_23_ << anInt7422 * 941710601;
			int i_26_ = i_24_ << anInt7422 * 941710601;
			int i_27_ = (aClass151Array7434
					 [aClass151Array7434.length - 1]
					 .method2491(i_23_, i_24_, -999555986)
				     - (1000 << 941710601 * anInt7422 - 7));
			int i_28_
			    = ((aClass151Array7436 != null
				? (aClass151Array7436[0]
				       .method2491(i_23_, i_24_, 1212675088)
				   + -1935734157 * anInt7420)
				: (aClass151Array7434[0]
				       .method2491(i_23_, i_24_, 1102508538)
				   + -1935734157 * anInt7420))
			       + (1000 << 941710601 * anInt7422 - 7));
			i_21_ = aClass185_7453.method3308(i_25_, i_27_, i_26_,
							  i_25_, i_28_, i_26_);
			aBoolArrayArray7479[i_19_][i_22_] = 0 == i_21_;
		    } else {
			i_21_ = -1;
			aBoolArrayArray7479[i_19_][i_22_] = false;
		    }
		    if (i_19_ > 0 && i_22_ > 0) {
			int i_29_ = (anIntArray7442[i_22_ - 1]
				     & anIntArray7442[i_22_] & i_20_ & i_21_);
			aBoolArrayArray7481[i_19_ - 1][i_22_ - 1] = i_29_ == 0;
		    }
		}
		anIntArray7442[(anInt7461 * -1213435377
				+ -1213435377 * anInt7461)]
		    = i_20_;
		anIntArray7442[(anInt7461 * -1213435377
				+ -1213435377 * anInt7461 + 1)]
		    = i_21_;
	    }
	    if (!bool_17_)
		aClass552_7427.aBool7310 = false;
	    else {
		aClass552_7427.anIntArray7328 = is_6_;
		aClass552_7427.anIntArray7329 = is_7_;
		aClass552_7427.anIntArray7330 = is_8_;
		aClass552_7427.anIntArray7331 = is_9_;
		aClass552_7427.anIntArray7336 = is_10_;
		aClass552_7427.method9058(aClass185_7453, i_11_);
	    }
	} else {
	    if (null == aBoolArrayArray7482)
		aBoolArrayArray7482
		    = (new boolean
		       [-1213435377 * anInt7461 + -1213435377 * anInt7461 + 2]
		       [(-1213435377 * anInt7461 + -1213435377 * anInt7461
			 + 2)]);
	    for (int i_30_ = 0; i_30_ < aBoolArrayArray7482.length; i_30_++) {
		for (int i_31_ = 0; i_31_ < aBoolArrayArray7482[0].length;
		     i_31_++)
		    aBoolArrayArray7482[i_30_][i_31_] = true;
	    }
	    aBoolArrayArray7479 = aBoolArrayArray7482;
	    aBoolArrayArray7481 = aBoolArrayArray7482;
	    anInt7419 = 0;
	    anInt7430 = 0;
	    anInt7456 = -444609591 * anInt7435;
	    anInt7467 = -1909674873 * anInt7470;
	    aClass552_7427.aBool7310 = false;
	}
	Class636.method10551(this, aClass185_7453, -441101126);
	if (!aClass557_7478.aBool7483) {
	    Iterator iterator = aClass557_7478.aList7484.iterator();
	    while (iterator.hasNext()) {
		Class550 class550 = (Class550) iterator.next();
		iterator.remove();
		Class42.method1063(class550, 1262399261);
	    }
	}
	if (aBool7443) {
	    for (int i_32_ = 0; i_32_ < anInt7472 * 1942714221; i_32_++)
		aClass561Array7439[i_32_].method9436(i, bool, -1536562927);
	}
	if (null != aClass568ArrayArrayArray7460) {
	    method9228(true, (byte) -68);
	    aClass185_7453.method3363(-1, new Class166(1583160, 40, 127, 63, 0,
						       0, 0));
	    method9269(true, is, i_11_, i_12_, i_16_);
	    aClass185_7453.method3529();
	    method9228(false, (byte) -59);
	}
	method9269(false, is, i_11_, i_12_, i_16_);
	if (!aBool7476) {
	    aBoolArrayArray7481 = bools;
	    aBoolArrayArray7479 = bools_18_;
	}
    }
    
    public void method9227(Class171 class171, byte i) {
	/* empty */
    }
    
    public void method9228(boolean bool, byte i) {
	if (bool) {
	    aClass568ArrayArrayArray7431 = aClass568ArrayArrayArray7460;
	    aClass151Array7432 = aClass151Array7436;
	} else {
	    aClass568ArrayArrayArray7431 = aClass568ArrayArrayArray7433;
	    aClass151Array7432 = aClass151Array7434;
	}
	anInt7418 = -1525290801 * aClass568ArrayArrayArray7431.length;
    }
    
    public void method9229(int i) {
	for (int i_33_ = 0; i_33_ < 1183912005 * anInt7435; i_33_++) {
	    for (int i_34_ = 0; i_34_ < anInt7470 * 60330541; i_34_++) {
		if (null == aClass568ArrayArrayArray7431[0][i_33_][i_34_])
		    aClass568ArrayArrayArray7431[0][i_33_][i_34_]
			= new Class568(0);
	    }
	}
    }
    
    public void method9230(int i, int i_35_, short i_36_) {
	Class568 class568 = aClass568ArrayArrayArray7431[0][i][i_35_];
	for (int i_37_ = 0; i_37_ < 3; i_37_++) {
	    Class568 class568_38_
		= (aClass568ArrayArrayArray7431[i_37_][i][i_35_]
		   = aClass568ArrayArrayArray7431[i_37_ + 1][i][i_35_]);
	    if (null != class568_38_) {
		for (Class559 class559 = class568_38_.aClass559_7604;
		     class559 != null; class559 = class559.aClass559_7497) {
		    Class654_Sub1_Sub5 class654_sub1_sub5
			= class559.aClass654_Sub1_Sub5_7500;
		    if (class654_sub1_sub5.aShort11900 == i
			&& class654_sub1_sub5.aShort11901 == i_35_)
			class654_sub1_sub5.aByte10854--;
		}
		if (null != class568_38_.aClass654_Sub1_Sub2_7607)
		    class568_38_.aClass654_Sub1_Sub2_7607.aByte10854--;
		if (null != class568_38_.aClass654_Sub1_Sub1_7598)
		    class568_38_.aClass654_Sub1_Sub1_7598.aByte10854--;
		if (null != class568_38_.aClass654_Sub1_Sub1_7597)
		    class568_38_.aClass654_Sub1_Sub1_7597.aByte10854--;
		if (null != class568_38_.aClass654_Sub1_Sub3_7600)
		    class568_38_.aClass654_Sub1_Sub3_7600.aByte10854--;
		if (class568_38_.aClass654_Sub1_Sub3_7601 != null)
		    class568_38_.aClass654_Sub1_Sub3_7601.aByte10854--;
	    }
	}
	if (aClass568ArrayArrayArray7431[0][i][i_35_] == null) {
	    aClass568ArrayArrayArray7431[0][i][i_35_] = new Class568(0);
	    aClass568ArrayArrayArray7431[0][i][i_35_].aByte7599 = (byte) 1;
	}
	aClass568ArrayArrayArray7431[0][i][i_35_].aClass568_7605 = class568;
	aClass568ArrayArrayArray7431[3][i][i_35_] = null;
    }
    
    public void method9231(int i, int i_39_, int i_40_,
			   Class654_Sub1_Sub1 class654_sub1_sub1,
			   Class654_Sub1_Sub1 class654_sub1_sub1_41_) {
	Class568 class568 = method9256(i, i_39_, i_40_, 768451766);
	if (class568 != null) {
	    class568.aClass654_Sub1_Sub1_7598 = class654_sub1_sub1;
	    class568.aClass654_Sub1_Sub1_7597 = class654_sub1_sub1_41_;
	    int i_42_ = aClass151Array7432 == aClass151Array7436 ? 1 : 0;
	    if (class654_sub1_sub1.method16849(859765625)) {
		if (class654_sub1_sub1.method16850(-1920390363)) {
		    class654_sub1_sub1.aClass654_Sub1_10850
			= aClass654_Sub1Array7448[i_42_];
		    aClass654_Sub1Array7448[i_42_] = class654_sub1_sub1;
		} else {
		    class654_sub1_sub1.aClass654_Sub1_10850
			= aClass654_Sub1Array7447[i_42_];
		    aClass654_Sub1Array7447[i_42_] = class654_sub1_sub1;
		}
	    } else {
		class654_sub1_sub1.aClass654_Sub1_10850
		    = aClass654_Sub1Array7449[i_42_];
		aClass654_Sub1Array7449[i_42_] = class654_sub1_sub1;
	    }
	    if (class654_sub1_sub1_41_ != null) {
		if (class654_sub1_sub1_41_.method16849(748239854)) {
		    if (class654_sub1_sub1_41_.method16850(-1920390363)) {
			class654_sub1_sub1_41_.aClass654_Sub1_10850
			    = aClass654_Sub1Array7448[i_42_];
			aClass654_Sub1Array7448[i_42_]
			    = class654_sub1_sub1_41_;
		    } else {
			class654_sub1_sub1_41_.aClass654_Sub1_10850
			    = aClass654_Sub1Array7447[i_42_];
			aClass654_Sub1Array7447[i_42_]
			    = class654_sub1_sub1_41_;
		    }
		} else {
		    class654_sub1_sub1_41_.aClass654_Sub1_10850
			= aClass654_Sub1Array7449[i_42_];
		    aClass654_Sub1Array7449[i_42_] = class654_sub1_sub1_41_;
		}
	    }
	}
    }
    
    Class568 method9232(int i, int i_43_, int i_44_, byte i_45_) {
	return method9256(i,
			  Math.min(1183912005 * anInt7435 - 1,
				   Math.max(0, i_43_)),
			  Math.min(60330541 * anInt7470 - 1,
				   Math.max(0, i_44_)),
			  768451766);
    }
    
    public void method9233(int i, int i_46_, int i_47_, int i_48_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_46_][i_47_];
	if (null != class568) {
	    Class654_Sub1_Sub3 class654_sub1_sub3
		= class568.aClass654_Sub1_Sub3_7600;
	    Class654_Sub1_Sub3 class654_sub1_sub3_49_
		= class568.aClass654_Sub1_Sub3_7601;
	    if (class654_sub1_sub3 != null) {
		class654_sub1_sub3.aShort11869
		    = (short) (class654_sub1_sub3.aShort11869 * i_48_
			       / (16 << anInt7422 * 941710601 - 7));
		class654_sub1_sub3.aShort11867
		    = (short) (i_48_ * class654_sub1_sub3.aShort11867
			       / (16 << anInt7422 * 941710601 - 7));
	    }
	    if (class654_sub1_sub3_49_ != null) {
		class654_sub1_sub3_49_.aShort11869
		    = (short) (i_48_ * class654_sub1_sub3_49_.aShort11869
			       / (16 << anInt7422 * 941710601 - 7));
		class654_sub1_sub3_49_.aShort11867
		    = (short) (class654_sub1_sub3_49_.aShort11867 * i_48_
			       / (16 << anInt7422 * 941710601 - 7));
	    }
	}
    }
    
    void method9234(Class654_Sub1 class654_sub1,
		    Class534_Sub21[] class534_sub21s) {
	if (aBool7443) {
	    int i = class654_sub1.method16856(class534_sub21s, 308999563);
	    aClass185_7453.method3654(i, class534_sub21s);
	}
	if (aClass151Array7436 == aClass151Array7432) {
	    boolean bool = false;
	    boolean bool_50_ = false;
	    Class438 class438 = class654_sub1.method10807().aClass438_4885;
	    int i;
	    int i_51_;
	    if (class654_sub1 instanceof Class654_Sub1_Sub5) {
		i = ((Class654_Sub1_Sub5) class654_sub1).aShort11900;
		i_51_ = ((Class654_Sub1_Sub5) class654_sub1).aShort11901;
	    } else {
		i = (int) class438.aFloat4864 >> anInt7422 * 941710601;
		i_51_ = (int) class438.aFloat4865 >> 941710601 * anInt7422;
	    }
	    i = Math.min(1183912005 * anInt7435 - 1, Math.max(0, i));
	    i_51_ = Math.min(anInt7470 * 60330541 - 1, Math.max(0, i_51_));
	    Class166 class166 = new Class166();
	    class166.anInt1763 = method9305(i, i_51_, -464896791) * 929497037;
	    class166.anInt1762 = method9283(i, i_51_, (byte) -30) * 1063224577;
	    class166.anInt1764
		= method9331(i, i_51_, 1896003368) * -1101271945;
	    class166.anInt1765
		= method9238(i, i_51_, (byte) -125) * -1961234339;
	    class166.anInt1766
		= method9239(i, i_51_, 1146482836) * -1338809385;
	    class166.anInt1767
		= method9310(i, i_51_, (short) -18830) * -2041662327;
	    aClass185_7453.method3364
		(aClass151Array7434[0].method2498((int) class438.aFloat4864,
						  (int) class438.aFloat4865,
						  1351174555),
		 class166);
	}
	Class550 class550
	    = class654_sub1.method16853(aClass185_7453, 308999563);
	if (class550 != null) {
	    if (class550.aBool7305) {
		class550.aClass654_Sub1_7303 = class654_sub1;
		aClass557_7478.method9402(class550, (byte) 6);
	    } else
		Class42.method1063(class550, 1262399261);
	}
    }
    
    void method9235(Class654_Sub1 class654_sub1, int i, int i_52_, int i_53_) {
	if (i_52_ < anInt7435 * 1183912005) {
	    Class568 class568
		= aClass568ArrayArrayArray7431[i][1 + i_52_][i_53_];
	    if (class568 != null && null != class568.aClass654_Sub1_Sub2_7607
		&& class568.aClass654_Sub1_Sub2_7607.method16848((byte) 93)) {
		int i_54_
		    = ((aClass151Array7432[i].method2491(i_52_ + 1, i_53_,
							 1397667985)
			+ aClass151Array7432[i].method2491(1 + i_52_ + 1,
							   i_53_, -1215586342)
			+ aClass151Array7432[i]
			      .method2491(1 + i_52_, i_53_ + 1, 815107766)
			+ aClass151Array7432[i].method2491(1 + (i_52_ + 1),
							   i_53_ + 1,
							   -1931912750)) / 4
		       - (aClass151Array7432[i].method2491(i_52_, i_53_,
							   702913963)
			  + aClass151Array7432[i].method2491(i_52_ + 1, i_53_,
							     455105644)
			  + aClass151Array7432[i].method2491(i_52_, i_53_ + 1,
							     421442492)
			  + aClass151Array7432[i].method2491(i_52_ + 1,
							     i_53_ + 1,
							     669596385)) / 4);
		class654_sub1.method16851(aClass185_7453,
					  class568.aClass654_Sub1_Sub2_7607,
					  -1935734157 * anInt7420, i_54_, 0,
					  true, 308999563);
	    }
	}
	if (i_53_ < anInt7435 * 1183912005) {
	    Class568 class568
		= aClass568ArrayArrayArray7431[i][i_52_][1 + i_53_];
	    if (null != class568 && null != class568.aClass654_Sub1_Sub2_7607
		&& class568.aClass654_Sub1_Sub2_7607.method16848((byte) 96)) {
		int i_55_
		    = ((aClass151Array7432[i].method2491(i_52_, i_53_,
							 644048417)
			+ aClass151Array7432[i]
			      .method2491(i_52_ + 1, 1 + i_53_, 1791320479)
			+ aClass151Array7432[i]
			      .method2491(i_52_, i_53_ + 1 + 1, -808226096)
			+ aClass151Array7432[i].method2491(i_52_ + 1,
							   1 + (1 + i_53_),
							   -292334441)) / 4
		       - ((aClass151Array7432[i].method2491(i_52_, i_53_,
							    385380170)
			   + aClass151Array7432[i].method2491(i_52_ + 1, i_53_,
							      -1075412333)
			   + aClass151Array7432[i].method2491(i_52_, 1 + i_53_,
							      -1784900494)
			   + aClass151Array7432[i].method2491(i_52_ + 1,
							      1 + i_53_,
							      -1961427589))
			  / 4));
		class654_sub1.method16851(aClass185_7453,
					  class568.aClass654_Sub1_Sub2_7607, 0,
					  i_55_, -1935734157 * anInt7420, true,
					  308999563);
	    }
	}
	if (i_52_ < anInt7435 * 1183912005 && i_53_ < 60330541 * anInt7470) {
	    Class568 class568
		= aClass568ArrayArrayArray7431[i][1 + i_52_][i_53_ + 1];
	    if (class568 != null && class568.aClass654_Sub1_Sub2_7607 != null
		&& class568.aClass654_Sub1_Sub2_7607.method16848((byte) 88)) {
		int i_56_
		    = ((aClass151Array7432[i].method2491(i_52_ + 1, i_53_ + 1,
							 1299216212)
			+ aClass151Array7432[i].method2491(1 + (1 + i_52_),
							   1 + i_53_,
							   -831363148)
			+ aClass151Array7432[i].method2491(i_52_ + 1,
							   1 + (1 + i_53_),
							   1700888312)
			+ aClass151Array7432[i].method2491(1 + (i_52_ + 1),
							   i_53_ + 1 + 1,
							   1363961443)) / 4
		       - ((aClass151Array7432[i].method2491(i_52_, i_53_,
							    -1043339465)
			   + aClass151Array7432[i].method2491(i_52_ + 1, i_53_,
							      -62789436)
			   + aClass151Array7432[i].method2491(i_52_, i_53_ + 1,
							      1458577540)
			   + aClass151Array7432[i].method2491(i_52_ + 1,
							      1 + i_53_,
							      -1628990347))
			  / 4));
		class654_sub1.method16851(aClass185_7453,
					  class568.aClass654_Sub1_Sub2_7607,
					  -1935734157 * anInt7420, i_56_,
					  anInt7420 * -1935734157, true,
					  308999563);
	    }
	}
	if (i_52_ < anInt7435 * 1183912005 && i_53_ > 0) {
	    Class568 class568
		= aClass568ArrayArrayArray7431[i][i_52_ + 1][i_53_ - 1];
	    if (class568 != null && class568.aClass654_Sub1_Sub2_7607 != null
		&& class568.aClass654_Sub1_Sub2_7607.method16848((byte) 99)) {
		int i_57_
		    = ((aClass151Array7432[i].method2491(i_52_ + 1, i_53_ - 1,
							 -342860609)
			+ aClass151Array7432[i].method2491(i_52_ + 1 + 1,
							   i_53_ - 1,
							   -1739544882)
			+ aClass151Array7432[i]
			      .method2491(1 + i_52_, i_53_ + 1 - 1, 993532167)
			+ aClass151Array7432[i].method2491(1 + (1 + i_52_),
							   i_53_ + 1 - 1,
							   -10446040)) / 4
		       - (aClass151Array7432[i].method2491(i_52_, i_53_,
							   1311011974)
			  + aClass151Array7432[i].method2491(1 + i_52_, i_53_,
							     1655629363)
			  + aClass151Array7432[i].method2491(i_52_, 1 + i_53_,
							     -1862196156)
			  + aClass151Array7432[i].method2491(i_52_ + 1,
							     i_53_ + 1,
							     1793660482)) / 4);
		class654_sub1.method16851(aClass185_7453,
					  class568.aClass654_Sub1_Sub2_7607,
					  -1935734157 * anInt7420, i_57_,
					  -(anInt7420 * -1935734157), true,
					  308999563);
	    }
	}
    }
    
    void method9236(Class654_Sub1[] class654_sub1s, int i, int i_58_) {
	if (i < i_58_) {
	    int i_59_ = (i_58_ + i) / 2;
	    int i_60_ = i;
	    Class654_Sub1 class654_sub1 = class654_sub1s[i_59_];
	    class654_sub1s[i_59_] = class654_sub1s[i_58_];
	    class654_sub1s[i_58_] = class654_sub1;
	    int i_61_ = class654_sub1.anInt10851 * 621186375;
	    for (int i_62_ = i; i_62_ < i_58_; i_62_++) {
		if (class654_sub1s[i_62_].anInt10851 * 621186375
		    > i_61_ + (i_62_ & 0x1)) {
		    Class654_Sub1 class654_sub1_63_ = class654_sub1s[i_62_];
		    class654_sub1s[i_62_] = class654_sub1s[i_60_];
		    class654_sub1s[i_60_++] = class654_sub1_63_;
		}
	    }
	    class654_sub1s[i_58_] = class654_sub1s[i_60_];
	    class654_sub1s[i_60_] = class654_sub1;
	    method9236(class654_sub1s, i, i_60_ - 1);
	    method9236(class654_sub1s, 1 + i_60_, i_58_);
	}
    }
    
    public void method9237(int i, int i_64_, int i_65_) {
	boolean bool
	    = (null != aClass568ArrayArrayArray7431[0][i_64_][i_65_]
	       && (aClass568ArrayArrayArray7431[0][i_64_][i_65_].aClass568_7605
		   != null));
	for (int i_66_ = i; i_66_ >= 0; i_66_--) {
	    if (aClass568ArrayArrayArray7431[i_66_][i_64_][i_65_] == null) {
		Class568 class568
		    = (aClass568ArrayArrayArray7431[i_66_][i_64_][i_65_]
		       = new Class568(i_66_));
		if (bool)
		    class568.aByte7599++;
	    }
	}
    }
    
    public int method9238(int i, int i_67_, byte i_68_) {
	return (null != aByteArrayArray7474
		? aByteArrayArray7474[i][i_67_] & 0xff : 0);
    }
    
    public int method9239(int i, int i_69_, int i_70_) {
	return (aByteArrayArray7441 != null
		? aByteArrayArray7441[i][i_69_] & 0xff : 0);
    }
    
    public void method9240(int i, int i_71_, int i_72_,
			   Class654_Sub1_Sub2 class654_sub1_sub2) {
	Class568 class568 = method9256(i, i_71_, i_72_, 768451766);
	if (null != class568) {
	    class568.aClass654_Sub1_Sub2_7607 = class654_sub1_sub2;
	    int i_73_ = aClass151Array7432 == aClass151Array7436 ? 1 : 0;
	    if (class654_sub1_sub2.method16849(662346614)) {
		if (class654_sub1_sub2.method16850(-1920390363)) {
		    class654_sub1_sub2.aClass654_Sub1_10850
			= aClass654_Sub1Array7448[i_73_];
		    aClass654_Sub1Array7448[i_73_] = class654_sub1_sub2;
		} else {
		    class654_sub1_sub2.aClass654_Sub1_10850
			= aClass654_Sub1Array7447[i_73_];
		    aClass654_Sub1Array7447[i_73_] = class654_sub1_sub2;
		}
	    } else {
		class654_sub1_sub2.aClass654_Sub1_10850
		    = aClass654_Sub1Array7449[i_73_];
		aClass654_Sub1Array7449[i_73_] = class654_sub1_sub2;
	    }
	}
    }
    
    void method9241(int i, int i_74_) {
	Class534_Sub18_Sub16 class534_sub18_sub16 = null;
	for (int i_75_ = i; i_75_ < i_74_; i_75_++) {
	    Class151 class151 = aClass151Array7434[i_75_];
	    if (null != class151) {
		for (int i_76_ = 0; i_76_ < anInt7470 * 60330541; i_76_++) {
		    for (int i_77_ = 0; i_77_ < 1183912005 * anInt7435;
			 i_77_++) {
			class534_sub18_sub16
			    = class151.method2526(i_77_, i_76_,
						  class534_sub18_sub16);
			if (null != class534_sub18_sub16) {
			    int i_78_ = i_77_ << anInt7422 * 941710601;
			    int i_79_ = i_76_ << anInt7422 * 941710601;
			    for (int i_80_ = i_75_ - 1; i_80_ >= 0; i_80_--) {
				Class151 class151_81_
				    = aClass151Array7434[i_80_];
				if (class151_81_ != null) {
				    int i_82_
					= (class151.method2491(i_77_, i_76_,
							       1466730905)
					   - (class151_81_.method2491
					      (i_77_, i_76_, 2060646339)));
				    int i_83_
					= (class151.method2491(1 + i_77_,
							       i_76_,
							       -834344885)
					   - (class151_81_.method2491
					      (1 + i_77_, i_76_, 1918583930)));
				    int i_84_
					= (class151.method2491(i_77_ + 1,
							       i_76_ + 1,
							       -1622064186)
					   - (class151_81_.method2491
					      (i_77_ + 1, i_76_ + 1,
					       -1019219113)));
				    int i_85_ = (class151.method2491(i_77_,
								     1 + i_76_,
								     151265802)
						 - (class151_81_.method2491
						    (i_77_, 1 + i_76_,
						     -1334573056)));
				    class151_81_.method2502
					(class534_sub18_sub16, i_78_,
					 (i_84_ + (i_83_ + i_82_) + i_85_) / 4,
					 i_79_, 0, false);
				}
			    }
			}
		    }
		}
	    }
	}
    }
    
    public void method9242(int i, int i_86_, int i_87_,
			   Class654_Sub1_Sub2 class654_sub1_sub2, int i_88_) {
	Class568 class568 = method9256(i, i_86_, i_87_, 768451766);
	if (null != class568) {
	    class568.aClass654_Sub1_Sub2_7607 = class654_sub1_sub2;
	    int i_89_ = aClass151Array7432 == aClass151Array7436 ? 1 : 0;
	    if (class654_sub1_sub2.method16849(1535331445)) {
		if (class654_sub1_sub2.method16850(-1920390363)) {
		    class654_sub1_sub2.aClass654_Sub1_10850
			= aClass654_Sub1Array7448[i_89_];
		    aClass654_Sub1Array7448[i_89_] = class654_sub1_sub2;
		} else {
		    class654_sub1_sub2.aClass654_Sub1_10850
			= aClass654_Sub1Array7447[i_89_];
		    aClass654_Sub1Array7447[i_89_] = class654_sub1_sub2;
		}
	    } else {
		class654_sub1_sub2.aClass654_Sub1_10850
		    = aClass654_Sub1Array7449[i_89_];
		aClass654_Sub1Array7449[i_89_] = class654_sub1_sub2;
	    }
	}
    }
    
    public void method9243(int i, int i_90_, int i_91_, int i_92_,
			   Class654_Sub1_Sub4 class654_sub1_sub4, int i_93_) {
	Class568 class568 = method9256(i, i_90_, i_91_, 768451766);
	if (null != class568) {
	    class654_sub1_sub4.method10809
		(new Class438((float) (-1978691487 * anInt7421
				       + (i_90_ << 941710601 * anInt7422)),
			      (float) i_92_,
			      (float) ((i_91_ << anInt7422 * 941710601)
				       + -1978691487 * anInt7421)));
	    class568.aClass654_Sub1_Sub4_7602 = class654_sub1_sub4;
	    int i_94_ = aClass151Array7432 == aClass151Array7436 ? 1 : 0;
	    if (class654_sub1_sub4.method16849(-131828239)) {
		if (class654_sub1_sub4.method16850(-1920390363)) {
		    class654_sub1_sub4.aClass654_Sub1_10850
			= aClass654_Sub1Array7448[i_94_];
		    aClass654_Sub1Array7448[i_94_] = class654_sub1_sub4;
		} else {
		    class654_sub1_sub4.aClass654_Sub1_10850
			= aClass654_Sub1Array7447[i_94_];
		    aClass654_Sub1Array7447[i_94_] = class654_sub1_sub4;
		}
	    } else {
		class654_sub1_sub4.aClass654_Sub1_10850
		    = aClass654_Sub1Array7449[i_94_];
		aClass654_Sub1Array7449[i_94_] = class654_sub1_sub4;
	    }
	}
    }
    
    public Class654_Sub1_Sub4 method9244(int i, int i_95_, int i_96_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_95_][i_96_];
	if (class568 == null)
	    return null;
	return class568.aClass654_Sub1_Sub4_7602;
    }
    
    public void method9245
	(int i, int i_97_, int i_98_, Class654_Sub1_Sub3 class654_sub1_sub3,
	 Class654_Sub1_Sub3 class654_sub1_sub3_99_, int i_100_) {
	Class568 class568 = method9256(i, i_97_, i_98_, 768451766);
	if (null != class568) {
	    class568.aClass654_Sub1_Sub3_7600 = class654_sub1_sub3;
	    class568.aClass654_Sub1_Sub3_7601 = class654_sub1_sub3_99_;
	    int i_101_ = aClass151Array7436 == aClass151Array7432 ? 1 : 0;
	    if (class654_sub1_sub3.method16849(-120662566)) {
		if (class654_sub1_sub3.method16850(-1920390363)) {
		    class654_sub1_sub3.aClass654_Sub1_10850
			= aClass654_Sub1Array7448[i_101_];
		    aClass654_Sub1Array7448[i_101_] = class654_sub1_sub3;
		} else {
		    class654_sub1_sub3.aClass654_Sub1_10850
			= aClass654_Sub1Array7447[i_101_];
		    aClass654_Sub1Array7447[i_101_] = class654_sub1_sub3;
		}
	    } else {
		class654_sub1_sub3.aClass654_Sub1_10850
		    = aClass654_Sub1Array7449[i_101_];
		aClass654_Sub1Array7449[i_101_] = class654_sub1_sub3;
	    }
	    if (null != class654_sub1_sub3_99_) {
		if (class654_sub1_sub3_99_.method16849(301952725)) {
		    if (class654_sub1_sub3_99_.method16850(-1920390363)) {
			class654_sub1_sub3_99_.aClass654_Sub1_10850
			    = aClass654_Sub1Array7448[i_101_];
			aClass654_Sub1Array7448[i_101_]
			    = class654_sub1_sub3_99_;
		    } else {
			class654_sub1_sub3_99_.aClass654_Sub1_10850
			    = aClass654_Sub1Array7447[i_101_];
			aClass654_Sub1Array7447[i_101_]
			    = class654_sub1_sub3_99_;
		    }
		} else {
		    class654_sub1_sub3_99_.aClass654_Sub1_10850
			= aClass654_Sub1Array7449[i_101_];
		    aClass654_Sub1Array7449[i_101_] = class654_sub1_sub3_99_;
		}
	    }
	}
    }
    
    public boolean method9246(Class654_Sub1_Sub5 class654_sub1_sub5,
			      boolean bool, byte i) {
	boolean bool_102_ = aClass151Array7436 == aClass151Array7432;
	int i_103_ = 0;
	short i_104_ = 0;
	byte i_105_ = 0;
	class654_sub1_sub5.method18487(-2046990039);
	short i_106_ = 0;
	int i_107_ = Math.min(1183912005 * anInt7435 - 1,
			      Math.max(0, class654_sub1_sub5.aShort11900));
	int i_108_ = Math.min(1183912005 * anInt7435 - 1,
			      Math.max(0, class654_sub1_sub5.aShort11896));
	int i_109_ = Math.min(anInt7470 * 60330541 - 1,
			      Math.max(0, class654_sub1_sub5.aShort11901));
	int i_110_ = Math.min(anInt7470 * 60330541 - 1,
			      Math.max(0, class654_sub1_sub5.aShort11898));
	for (int i_111_ = i_107_; i_111_ <= i_108_; i_111_++) {
	    for (int i_112_ = i_109_; i_112_ <= i_110_; i_112_++) {
		Class568 class568 = method9232(class654_sub1_sub5.aByte10854,
					       i_111_, i_112_, (byte) 0);
		if (null != class568) {
		    Class559 class559
			= Class197.method3822(class654_sub1_sub5, 1244811404);
		    Class559 class559_113_ = class568.aClass559_7604;
		    if (class559_113_ == null)
			class568.aClass559_7604 = class559;
		    else {
			for (/**/; null != class559_113_.aClass559_7497;
			     class559_113_ = class559_113_.aClass559_7497) {
			    /* empty */
			}
			class559_113_.aClass559_7497 = class559;
		    }
		    if (bool_102_ && (anIntArrayArray7437[i_111_][i_112_]
				      & ~0xffffff) != 0) {
			i_103_ = anIntArrayArray7437[i_111_][i_112_];
			i_104_ = aShortArrayArray7438[i_111_][i_112_];
			i_105_ = aByteArrayArray7480[i_111_][i_112_];
		    }
		    if (!bool && class568.aClass654_Sub1_Sub2_7607 != null
			&& (class568.aClass654_Sub1_Sub2_7607.aShort11864
			    > i_106_))
			i_106_ = class568.aClass654_Sub1_Sub2_7607.aShort11864;
		}
	    }
	}
	if (bool_102_ && 0 != (i_103_ & ~0xffffff)) {
	    for (int i_114_ = i_107_; i_114_ <= i_108_; i_114_++) {
		for (int i_115_ = i_109_; i_115_ <= i_110_; i_115_++) {
		    if ((anIntArrayArray7437[i_114_][i_115_] & ~0xffffff)
			== 0) {
			anIntArrayArray7437[i_114_][i_115_] = i_103_;
			aShortArrayArray7438[i_114_][i_115_] = i_104_;
			aByteArrayArray7480[i_114_][i_115_] = i_105_;
		    }
		}
	    }
	}
	if (bool) {
	    aClass654_Sub1_Sub5Array7454
		[(anInt7465 += -2053860005) * -67005741 - 1]
		= class654_sub1_sub5;
	    class654_sub1_sub5.aClass556_10855 = this;
	} else {
	    int i_116_ = aClass151Array7432 == aClass151Array7436 ? 1 : 0;
	    if (class654_sub1_sub5.method16849(-156097505)) {
		if (class654_sub1_sub5.method16850(-1920390363)) {
		    class654_sub1_sub5.aClass654_Sub1_10850
			= aClass654_Sub1Array7448[i_116_];
		    aClass654_Sub1Array7448[i_116_] = class654_sub1_sub5;
		} else {
		    class654_sub1_sub5.aClass654_Sub1_10850
			= aClass654_Sub1Array7447[i_116_];
		    aClass654_Sub1Array7447[i_116_] = class654_sub1_sub5;
		}
	    } else {
		class654_sub1_sub5.aClass654_Sub1_10850
		    = aClass654_Sub1Array7449[i_116_];
		aClass654_Sub1Array7449[i_116_] = class654_sub1_sub5;
	    }
	}
	if (bool) {
	    Class438 class438
		= Class438.method6994(class654_sub1_sub5.method10807()
				      .aClass438_4885);
	    class438.aFloat4863 -= (float) i_106_;
	    class654_sub1_sub5.method10809(class438);
	    class438.method7074();
	}
	return true;
    }
    
    public void method9247(int i, int i_117_, int i_118_, int i_119_,
			   byte i_120_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_117_][i_118_];
	if (null != class568) {
	    Class654_Sub1_Sub3 class654_sub1_sub3
		= class568.aClass654_Sub1_Sub3_7600;
	    Class654_Sub1_Sub3 class654_sub1_sub3_121_
		= class568.aClass654_Sub1_Sub3_7601;
	    if (class654_sub1_sub3 != null) {
		class654_sub1_sub3.aShort11869
		    = (short) (class654_sub1_sub3.aShort11869 * i_119_
			       / (16 << anInt7422 * 941710601 - 7));
		class654_sub1_sub3.aShort11867
		    = (short) (i_119_ * class654_sub1_sub3.aShort11867
			       / (16 << anInt7422 * 941710601 - 7));
	    }
	    if (class654_sub1_sub3_121_ != null) {
		class654_sub1_sub3_121_.aShort11869
		    = (short) (i_119_ * class654_sub1_sub3_121_.aShort11869
			       / (16 << anInt7422 * 941710601 - 7));
		class654_sub1_sub3_121_.aShort11867
		    = (short) (class654_sub1_sub3_121_.aShort11867 * i_119_
			       / (16 << anInt7422 * 941710601 - 7));
	    }
	}
    }
    
    public void method9248(int i) {
	for (int i_122_ = 0; i_122_ < anInt7465 * -67005741; i_122_++) {
	    Class654_Sub1_Sub5 class654_sub1_sub5
		= aClass654_Sub1_Sub5Array7454[i_122_];
	    method9337(class654_sub1_sub5, true, -2130129643);
	    aClass654_Sub1_Sub5Array7454[i_122_] = null;
	}
	anInt7465 = 0;
    }
    
    public Class654_Sub1_Sub4 method9249(int i, int i_123_, int i_124_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_123_][i_124_];
	if (null == class568)
	    return null;
	Class654_Sub1_Sub4 class654_sub1_sub4
	    = class568.aClass654_Sub1_Sub4_7602;
	class568.aClass654_Sub1_Sub4_7602 = null;
	method9257(class654_sub1_sub4, -1353164646);
	return class654_sub1_sub4;
    }
    
    public int method9250(int i, int i_125_) {
	return (null != aByteArrayArray7480
		? aByteArrayArray7480[i][i_125_] & 0xff : 0);
    }
    
    public void method9251(HashMap hashmap, byte i) {
	aHashMap7477 = hashmap;
    }
    
    public Class654_Sub1_Sub1 method9252(int i, int i_126_, int i_127_,
					 byte i_128_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_126_][i_127_];
	if (class568 != null) {
	    method9257(class568.aClass654_Sub1_Sub1_7598, -244818286);
	    if (null != class568.aClass654_Sub1_Sub1_7598) {
		Class654_Sub1_Sub1 class654_sub1_sub1
		    = class568.aClass654_Sub1_Sub1_7598;
		class568.aClass654_Sub1_Sub1_7598 = null;
		return class654_sub1_sub1;
	    }
	}
	return null;
    }
    
    public Class654_Sub1_Sub2 method9253(int i, int i_129_, int i_130_,
					 byte i_131_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_129_][i_130_];
	if (class568 == null)
	    return null;
	method9257(class568.aClass654_Sub1_Sub2_7607, -147984214);
	if (null != class568.aClass654_Sub1_Sub2_7607) {
	    Class654_Sub1_Sub2 class654_sub1_sub2
		= class568.aClass654_Sub1_Sub2_7607;
	    class568.aClass654_Sub1_Sub2_7607 = null;
	    return class654_sub1_sub2;
	}
	return null;
    }
    
    public Class654_Sub1_Sub4 method9254(int i, int i_132_, int i_133_,
					 int i_134_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_132_][i_133_];
	if (null == class568)
	    return null;
	Class654_Sub1_Sub4 class654_sub1_sub4
	    = class568.aClass654_Sub1_Sub4_7602;
	class568.aClass654_Sub1_Sub4_7602 = null;
	method9257(class654_sub1_sub4, -1009303634);
	return class654_sub1_sub4;
    }
    
    public Class654_Sub1_Sub5 method9255
	(int i, int i_135_, int i_136_, Interface64 interface64, byte i_137_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_135_][i_136_];
	if (class568 == null)
	    return null;
	for (Class559 class559 = class568.aClass559_7604; null != class559;
	     class559 = class559.aClass559_7497) {
	    Class654_Sub1_Sub5 class654_sub1_sub5
		= class559.aClass654_Sub1_Sub5_7500;
	    if ((null == interface64
		 || interface64.method432(class654_sub1_sub5, (byte) -58))
		&& i_135_ == class654_sub1_sub5.aShort11900
		&& class654_sub1_sub5.aShort11901 == i_136_) {
		method9337(class654_sub1_sub5, false, -2050094031);
		return class654_sub1_sub5;
	    }
	}
	return null;
    }
    
    Class568 method9256(int i, int i_138_, int i_139_, int i_140_) {
	if (aClass568ArrayArrayArray7431[i][i_138_][i_139_] == null) {
	    boolean bool
		= (null != aClass568ArrayArrayArray7431[0][i_138_][i_139_]
		   && (aClass568ArrayArrayArray7431[0][i_138_][i_139_]
		       .aClass568_7605) != null);
	    if (bool && i >= -1381097937 * anInt7418 - 1)
		return null;
	    method9367(i, i_138_, i_139_, -2111684435);
	}
	return aClass568ArrayArrayArray7431[i][i_138_][i_139_];
    }
    
    void method9257(Class654_Sub1 class654_sub1, int i) {
	if (class654_sub1 != null) {
	    class654_sub1.method10840();
	    for (int i_141_ = 0; i_141_ < 2; i_141_++) {
		Class654_Sub1 class654_sub1_142_ = null;
		for (Class654_Sub1 class654_sub1_143_
			 = aClass654_Sub1Array7447[i_141_];
		     null != class654_sub1_143_;
		     class654_sub1_143_
			 = class654_sub1_143_.aClass654_Sub1_10850) {
		    if (class654_sub1 == class654_sub1_143_) {
			if (null != class654_sub1_142_)
			    class654_sub1_142_.aClass654_Sub1_10850
				= class654_sub1_143_.aClass654_Sub1_10850;
			else
			    aClass654_Sub1Array7447[i_141_]
				= class654_sub1_143_.aClass654_Sub1_10850;
			return;
		    }
		    class654_sub1_142_ = class654_sub1_143_;
		}
		class654_sub1_142_ = null;
		for (Class654_Sub1 class654_sub1_144_
			 = aClass654_Sub1Array7448[i_141_];
		     null != class654_sub1_144_;
		     class654_sub1_144_
			 = class654_sub1_144_.aClass654_Sub1_10850) {
		    if (class654_sub1 == class654_sub1_144_) {
			if (null != class654_sub1_142_)
			    class654_sub1_142_.aClass654_Sub1_10850
				= class654_sub1_144_.aClass654_Sub1_10850;
			else
			    aClass654_Sub1Array7448[i_141_]
				= class654_sub1_144_.aClass654_Sub1_10850;
			return;
		    }
		    class654_sub1_142_ = class654_sub1_144_;
		}
		class654_sub1_142_ = null;
		for (Class654_Sub1 class654_sub1_145_
			 = aClass654_Sub1Array7449[i_141_];
		     class654_sub1_145_ != null;
		     class654_sub1_145_
			 = class654_sub1_145_.aClass654_Sub1_10850) {
		    if (class654_sub1_145_ == class654_sub1) {
			if (null != class654_sub1_142_)
			    class654_sub1_142_.aClass654_Sub1_10850
				= class654_sub1_145_.aClass654_Sub1_10850;
			else
			    aClass654_Sub1Array7449[i_141_]
				= class654_sub1_145_.aClass654_Sub1_10850;
			return;
		    }
		    class654_sub1_142_ = class654_sub1_145_;
		}
	    }
	}
    }
    
    public Class654_Sub1_Sub1 method9258(int i, int i_146_, int i_147_,
					 byte i_148_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_146_][i_147_];
	if (class568 == null)
	    return null;
	return class568.aClass654_Sub1_Sub1_7598;
    }
    
    public Class654_Sub1_Sub1 method9259(int i, int i_149_, int i_150_,
					 int i_151_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_149_][i_150_];
	if (class568 == null)
	    return null;
	return class568.aClass654_Sub1_Sub1_7597;
    }
    
    public void method9260(Class561 class561, int i) {
	if (1942714221 * anInt7472 < 65165) {
	    Class534_Sub21 class534_sub21 = class561.aClass534_Sub21_7541;
	    aClass561Array7439[1942714221 * anInt7472] = class561;
	    aBoolArray7475[anInt7472 * 1942714221] = false;
	    anInt7472 += -328586651;
	    int i_152_ = class561.anInt7544 * 551684827;
	    if (class561.aBool7543)
		i_152_ = 0;
	    int i_153_ = 551684827 * class561.anInt7544;
	    if (class561.aBool7542)
		i_153_ = anInt7418 * -1381097937 - 1;
	    for (int i_154_ = i_152_; i_154_ <= i_153_; i_154_++) {
		int i_155_ = 0;
		int i_156_ = ((class534_sub21.method16199((byte) -104)
			       - class534_sub21.method16233(-1939698717)
			       + anInt7421 * -1978691487)
			      >> 941710601 * anInt7422);
		if (i_156_ < 0) {
		    i_155_ -= i_156_;
		    i_156_ = 0;
		}
		int i_157_ = ((class534_sub21.method16199((byte) -122)
			       + class534_sub21.method16233(-1330144770)
			       - -1978691487 * anInt7421)
			      >> anInt7422 * 941710601);
		if (i_157_ >= 60330541 * anInt7470)
		    i_157_ = anInt7470 * 60330541 - 1;
		for (int i_158_ = i_156_; i_158_ <= i_157_; i_158_++) {
		    int i_159_ = class561.aShortArray7531[i_155_++];
		    int i_160_ = (((class534_sub21.method16197(435671096)
				    - class534_sub21.method16233(-883508390)
				    + -1978691487 * anInt7421)
				   >> 941710601 * anInt7422)
				  + (i_159_ >>> 8));
		    int i_161_ = i_160_ + (i_159_ & 0xff) - 1;
		    if (i_160_ < 0)
			i_160_ = 0;
		    if (i_161_ >= 1183912005 * anInt7435)
			i_161_ = anInt7435 * 1183912005 - 1;
		    for (int i_162_ = i_160_; i_162_ <= i_161_; i_162_++) {
			long l
			    = aLongArrayArrayArray7473[i_154_][i_162_][i_158_];
			if ((l & 0xffffL) == 0L)
			    aLongArrayArrayArray7473[i_154_][i_162_][i_158_]
				= l | (long) (anInt7472 * 1942714221);
			else if ((l & 0xffff0000L) == 0L)
			    aLongArrayArrayArray7473[i_154_][i_162_][i_158_]
				= l | (long) (1942714221 * anInt7472) << 16;
			else if (0L == (l & 0xffff00000000L))
			    aLongArrayArrayArray7473[i_154_][i_162_][i_158_]
				= l | (long) (anInt7472 * 1942714221) << 32;
			else if ((l & ~0xffffffffffffL) == 0L)
			    aLongArrayArrayArray7473[i_154_][i_162_][i_158_]
				= l | (long) (anInt7472 * 1942714221) << 48;
		    }
		}
	    }
	    if (-1 != class561.anInt7546 * -1446450775) {
		List list
		    = (List) aHashMap7429.get(Integer.valueOf(-1446450775
							      * (class561
								 .anInt7546)));
		if (list == null) {
		    list = new ArrayList();
		    aHashMap7429.put(Integer.valueOf(-1446450775
						     * class561.anInt7546),
				     list);
		}
		list.add(class561);
		Class534_Sub21 class534_sub21_163_
		    = ((Class534_Sub21)
		       aHashMap7477.get(Integer.valueOf(-1446450775
							* (class561
							   .anInt7546))));
		if (class534_sub21_163_ != null) {
		    class561.aClass534_Sub21_7541.method16207
			(class534_sub21_163_.method16201((byte) 26),
			 1665516725);
		    class561.aClass534_Sub21_7541.method16235
			(class534_sub21_163_.method16230((byte) -39),
			 class534_sub21_163_.method16206((byte) 52),
			 (byte) 117);
		}
	    }
	}
    }
    
    public void method9261(int i, int i_164_, int i_165_, int i_166_,
			   int i_167_, int i_168_, int i_169_, int i_170_,
			   int i_171_) {
	if (anIntArrayArray7437 != null)
	    anIntArrayArray7437[i][i_164_] = ~0xffffff | i_165_;
	if (aShortArrayArray7438 != null)
	    aShortArrayArray7438[i][i_164_] = (short) i_166_;
	if (null != aByteArrayArray7480)
	    aByteArrayArray7480[i][i_164_] = (byte) i_167_;
	if (aByteArrayArray7474 != null)
	    aByteArrayArray7474[i][i_164_] = (byte) i_168_;
	if (null != aByteArrayArray7441)
	    aByteArrayArray7441[i][i_164_] = (byte) i_169_;
	if (aByteArrayArray7423 != null)
	    aByteArrayArray7423[i][i_164_] = (byte) i_170_;
    }
    
    public Class654_Sub1_Sub5 method9262
	(int i, int i_172_, int i_173_, Interface64 interface64, byte i_174_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_172_][i_173_];
	if (class568 == null)
	    return null;
	for (Class559 class559 = class568.aClass559_7604; class559 != null;
	     class559 = class559.aClass559_7497) {
	    Class654_Sub1_Sub5 class654_sub1_sub5
		= class559.aClass654_Sub1_Sub5_7500;
	    if ((null == interface64
		 || interface64.method432(class654_sub1_sub5, (byte) -36))
		&& i_172_ == class654_sub1_sub5.aShort11900
		&& i_173_ == class654_sub1_sub5.aShort11901)
		return class654_sub1_sub5;
	}
	return null;
    }
    
    public Class559 method9263(int i, int i_175_, int i_176_, int i_177_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_175_][i_176_];
	if (null == class568)
	    return null;
	return class568.aClass559_7604;
    }
    
    public Class654_Sub1_Sub2 method9264(int i, int i_178_, int i_179_,
					 byte i_180_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_178_][i_179_];
	if (class568 == null || null == class568.aClass654_Sub1_Sub2_7607)
	    return null;
	return class568.aClass654_Sub1_Sub2_7607;
    }
    
    public void method9265(boolean bool) {
	if (bool) {
	    aClass568ArrayArrayArray7431 = aClass568ArrayArrayArray7460;
	    aClass151Array7432 = aClass151Array7436;
	} else {
	    aClass568ArrayArrayArray7431 = aClass568ArrayArrayArray7433;
	    aClass151Array7432 = aClass151Array7434;
	}
	anInt7418 = -1525290801 * aClass568ArrayArrayArray7431.length;
    }
    
    public void method9266(boolean bool) {
	if (bool) {
	    aClass568ArrayArrayArray7431 = aClass568ArrayArrayArray7460;
	    aClass151Array7432 = aClass151Array7436;
	} else {
	    aClass568ArrayArrayArray7431 = aClass568ArrayArrayArray7433;
	    aClass151Array7432 = aClass151Array7434;
	}
	anInt7418 = -1525290801 * aClass568ArrayArrayArray7431.length;
    }
    
    void method9267(Class654_Sub1 class654_sub1, int i, int i_181_, int i_182_,
		    int i_183_, int i_184_) {
	boolean bool = true;
	int i_185_ = i_181_;
	int i_186_ = i_181_ + i_183_;
	int i_187_ = i_182_ - 1;
	int i_188_ = i_182_ + i_184_;
	for (int i_189_ = i; i_189_ <= i + 1; i_189_++) {
	    if (i_189_ != -1381097937 * anInt7418) {
		for (int i_190_ = i_185_; i_190_ <= i_186_; i_190_++) {
		    if (i_190_ >= 0 && i_190_ < anInt7435 * 1183912005) {
			for (int i_191_ = i_187_; i_191_ <= i_188_; i_191_++) {
			    if (i_191_ >= 0 && i_191_ < 60330541 * anInt7470
				&& (!bool || i_190_ >= i_186_
				    || i_191_ >= i_188_
				    || i_191_ < i_182_ && i_190_ != i_181_)) {
				Class568 class568
				    = (aClass568ArrayArrayArray7431[i_189_]
				       [i_190_][i_191_]);
				if (null != class568) {
				    int i_192_
					= (((aClass151Array7432[i_189_]
						 .method2491
					     (i_190_, i_191_, -2057773779))
					    + (aClass151Array7432[i_189_]
						   .method2491
					       (1 + i_190_, i_191_,
						2010574194))
					    + (aClass151Array7432[i_189_]
						   .method2491
					       (i_190_, 1 + i_191_,
						-398616634))
					    + (aClass151Array7432[i_189_]
						   .method2491
					       (i_190_ + 1, i_191_ + 1,
						-2105030898))) / 4
					   - ((aClass151Array7432[i].method2491
					       (i_181_, i_182_, -1818263695))
					      + (aClass151Array7432[i]
						     .method2491
						 (1 + i_181_, i_182_,
						  -627394456))
					      + (aClass151Array7432[i]
						     .method2491
						 (i_181_, i_182_ + 1,
						  459633285))
					      + (aClass151Array7432[i]
						     .method2491
						 (1 + i_181_, 1 + i_182_,
						  1057903112))) / 4);
				    Class654_Sub1_Sub1 class654_sub1_sub1
					= class568.aClass654_Sub1_Sub1_7598;
				    Class654_Sub1_Sub1 class654_sub1_sub1_193_
					= class568.aClass654_Sub1_Sub1_7597;
				    if (class654_sub1_sub1 != null
					&& class654_sub1_sub1
					       .method16848((byte) 115))
					class654_sub1.method16851
					    (aClass185_7453,
					     class654_sub1_sub1,
					     (((i_190_ - i_181_)
					       * (anInt7420 * -1935734157))
					      + (-1978691487 * anInt7421
						 * (1 - i_183_))),
					     i_192_,
					     ((-1978691487 * anInt7421
					       * (1 - i_184_))
					      + ((i_191_ - i_182_)
						 * (anInt7420 * -1935734157))),
					     bool, 308999563);
				    if (null != class654_sub1_sub1_193_
					&& class654_sub1_sub1_193_
					       .method16848((byte) 111))
					class654_sub1.method16851
					    (aClass185_7453,
					     class654_sub1_sub1_193_,
					     ((i_190_ - i_181_) * (-1935734157
								   * anInt7420)
					      + (-1978691487 * anInt7421
						 * (1 - i_183_))),
					     i_192_,
					     ((anInt7421 * -1978691487
					       * (1 - i_184_))
					      + ((i_191_ - i_182_)
						 * (-1935734157 * anInt7420))),
					     bool, 308999563);
				    for (Class559 class559
					     = class568.aClass559_7604;
					 class559 != null;
					 class559 = class559.aClass559_7497) {
					Class654_Sub1_Sub5 class654_sub1_sub5
					    = (class559
					       .aClass654_Sub1_Sub5_7500);
					if (null != class654_sub1_sub5
					    && class654_sub1_sub5
						   .method16848((byte) 9)
					    && ((class654_sub1_sub5.aShort11900
						 == i_190_)
						|| i_190_ == i_185_)
					    && ((class654_sub1_sub5.aShort11901
						 == i_191_)
						|| i_187_ == i_191_)) {
					    int i_194_ = ((class654_sub1_sub5
							   .aShort11896)
							  - (class654_sub1_sub5
							     .aShort11900)
							  + 1);
					    int i_195_ = ((class654_sub1_sub5
							   .aShort11898)
							  - (class654_sub1_sub5
							     .aShort11901)
							  + 1);
					    class654_sub1.method16851
						(aClass185_7453,
						 class654_sub1_sub5,
						 ((((class654_sub1_sub5
						     .aShort11900)
						    - i_181_)
						   * (anInt7420 * -1935734157))
						  + ((i_194_ - i_183_)
						     * (anInt7421
							* -1978691487))),
						 i_192_,
						 ((((class654_sub1_sub5
						     .aShort11901)
						    - i_182_)
						   * (-1935734157 * anInt7420))
						  + (anInt7421 * -1978691487
						     * (i_195_ - i_184_))),
						 bool, 308999563);
					}
				    }
				}
			    }
			}
		    }
		}
		i_185_--;
		bool = false;
	    }
	}
    }
    
    public Class654_Sub1_Sub1 method9268(int i, int i_196_, int i_197_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_196_][i_197_];
	if (class568 == null)
	    return null;
	return class568.aClass654_Sub1_Sub1_7597;
    }
    
    void method9269(boolean bool, byte[][][] is, int i, byte i_198_,
		    int i_199_) {
	int i_200_ = bool ? 1 : 0;
	anInt7450 = 0;
	anInt7451 = 0;
	anInt7428 += 964746483;
	if ((i_199_ & 0x2) == 0) {
	    for (Class654_Sub1 class654_sub1 = aClass654_Sub1Array7447[i_200_];
		 class654_sub1 != null;
		 class654_sub1 = class654_sub1.aClass654_Sub1_10850) {
		method9287(class654_sub1, (byte) 12);
		if (class654_sub1.anInt10851 * 621186375 != -1
		    && !method9328(class654_sub1, bool, is, i, i_198_))
		    aClass654_Sub1Array7424
			[(anInt7450 += 744803731) * 1319065755 - 1]
			= class654_sub1;
	    }
	}
	if ((i_199_ & 0x1) == 0) {
	    for (Class654_Sub1 class654_sub1 = aClass654_Sub1Array7448[i_200_];
		 class654_sub1 != null;
		 class654_sub1 = class654_sub1.aClass654_Sub1_10850) {
		method9287(class654_sub1, (byte) 67);
		if (-1 != class654_sub1.anInt10851 * 621186375
		    && !method9328(class654_sub1, bool, is, i, i_198_))
		    aClass654_Sub1Array7452
			[(anInt7451 += -755338105) * -219410121 - 1]
			= class654_sub1;
	    }
	    for (Class654_Sub1 class654_sub1 = aClass654_Sub1Array7449[i_200_];
		 null != class654_sub1;
		 class654_sub1 = class654_sub1.aClass654_Sub1_10850) {
		method9287(class654_sub1, (byte) 52);
		if (-1 != class654_sub1.anInt10851 * 621186375
		    && !method9328(class654_sub1, bool, is, i, i_198_)) {
		    if (class654_sub1.method16850(-1920390363))
			aClass654_Sub1Array7452
			    [(anInt7451 += -755338105) * -219410121 - 1]
			    = class654_sub1;
		    else
			aClass654_Sub1Array7424
			    [(anInt7450 += 744803731) * 1319065755 - 1]
			    = class654_sub1;
		}
	    }
	    if (!bool) {
		for (int i_201_ = 0; i_201_ < anInt7465 * -67005741;
		     i_201_++) {
		    method9287(aClass654_Sub1_Sub5Array7454[i_201_],
			       (byte) 78);
		    if ((aClass654_Sub1_Sub5Array7454[i_201_].anInt10851
			 * 621186375) != -1
			&& !method9328(aClass654_Sub1_Sub5Array7454[i_201_],
				       bool, is, i, i_198_)) {
			if (aClass654_Sub1_Sub5Array7454[i_201_]
				.method16850(-1920390363))
			    aClass654_Sub1Array7452
				[(anInt7451 += -755338105) * -219410121 - 1]
				= aClass654_Sub1_Sub5Array7454[i_201_];
			else
			    aClass654_Sub1Array7424
				[(anInt7450 += 744803731) * 1319065755 - 1]
				= aClass654_Sub1_Sub5Array7454[i_201_];
		    }
		}
	    }
	}
	if (anInt7450 * 1319065755 > 0) {
	    method9312(aClass654_Sub1Array7424, 0, 1319065755 * anInt7450 - 1);
	    for (int i_202_ = 0; i_202_ < anInt7450 * 1319065755; i_202_++)
		method9234(aClass654_Sub1Array7424[i_202_],
			   aClass534_Sub21Array7469);
	}
	if (aBool7443)
	    aClass185_7453.method3654(0, null);
	if (0 == (i_199_ & 0x2)) {
	    for (int i_203_ = 0; i_203_ < -1381097937 * anInt7418; i_203_++) {
		if (i_203_ >= i && null != is) {
		    int i_204_ = aBoolArrayArray7481.length;
		    if (-474621401 * anInt7419 + aBoolArrayArray7481.length
			> anInt7435 * 1183912005)
			i_204_ -= (anInt7419 * -474621401
				   + aBoolArrayArray7481.length
				   - 1183912005 * anInt7435);
		    int i_205_ = aBoolArrayArray7481[0].length;
		    if (anInt7430 * 1375305047 + aBoolArrayArray7481[0].length
			> anInt7470 * 60330541)
			i_205_ -= (1375305047 * anInt7430
				   + aBoolArrayArray7481[0].length
				   - 60330541 * anInt7470);
		    boolean[][] bools = aBoolArrayArray7479;
		    if (aBool7476) {
			for (int i_206_ = anInt7425 * 951958497;
			     i_206_ < i_204_; i_206_++) {
			    int i_207_ = (i_206_ + anInt7419 * -474621401
					  - anInt7425 * 951958497);
			    for (int i_208_ = anInt7463 * -856129721;
				 i_208_ < i_205_; i_208_++) {
				bools[i_206_][i_208_] = false;
				if (aBoolArrayArray7481[i_206_][i_208_]) {
				    int i_209_
					= (1375305047 * anInt7430 + i_208_
					   - -856129721 * anInt7463);
				    for (int i_210_ = i_203_; i_210_ >= 0;
					 i_210_--) {
					if ((null
					     != (aClass568ArrayArrayArray7431
						 [i_210_][i_207_][i_209_]))
					    && (i_203_
						== (aClass568ArrayArrayArray7431
						    [i_210_][i_207_][i_209_]
						    .aByte7599))) {
					    if ((i_210_ >= i
						 && (is[i_210_][i_207_][i_209_]
						     == i_198_))
						|| (aClass552_7427.method9062
						    (i_203_, i_207_, i_209_)))
						bools[i_206_][i_208_] = false;
					    else
						bools[i_206_][i_208_] = true;
					    break;
					}
				    }
				}
			    }
			}
		    }
		    aClass151Array7432[i_203_].method2506
			(anInt7455 * -380604831, anInt7459 * -1709472547,
			 -1213435377 * anInt7461, aBoolArrayArray7479, false,
			 i_199_);
		} else {
		    int i_211_ = aBoolArrayArray7481.length;
		    if (anInt7419 * -474621401 + aBoolArrayArray7481.length
			> anInt7435 * 1183912005)
			i_211_ -= (anInt7419 * -474621401
				   + aBoolArrayArray7481.length
				   - anInt7435 * 1183912005);
		    int i_212_ = aBoolArrayArray7481[0].length;
		    if (anInt7430 * 1375305047 + aBoolArrayArray7481[0].length
			> 60330541 * anInt7470)
			i_212_ -= (aBoolArrayArray7481[0].length
				   + 1375305047 * anInt7430
				   - anInt7470 * 60330541);
		    boolean[][] bools = aBoolArrayArray7479;
		    if (aBool7476) {
			for (int i_213_ = anInt7425 * 951958497;
			     i_213_ < i_211_; i_213_++) {
			    int i_214_ = (i_213_ + -474621401 * anInt7419
					  - 951958497 * anInt7425);
			    for (int i_215_ = anInt7463 * -856129721;
				 i_215_ < i_212_; i_215_++) {
				if (aBoolArrayArray7481[i_213_][i_215_]
				    && !(aClass552_7427.method9062
					 (i_203_, i_214_,
					  (i_215_ + 1375305047 * anInt7430
					   - -856129721 * anInt7463))))
				    bools[i_213_][i_215_] = true;
				else
				    bools[i_213_][i_215_] = false;
			    }
			}
		    }
		    aClass151Array7432[i_203_].method2506
			(anInt7455 * -380604831, -1709472547 * anInt7459,
			 -1213435377 * anInt7461, aBoolArrayArray7479, true,
			 i_199_);
		}
	    }
	}
	if (-219410121 * anInt7451 > 0) {
	    method9236(aClass654_Sub1Array7452, 0, -219410121 * anInt7451 - 1);
	    for (int i_216_ = 0; i_216_ < -219410121 * anInt7451; i_216_++)
		method9234(aClass654_Sub1Array7452[i_216_],
			   aClass534_Sub21Array7469);
	}
    }
    
    Class568 method9270(int i, int i_217_, int i_218_) {
	return method9256(i,
			  Math.min(1183912005 * anInt7435 - 1,
				   Math.max(0, i_217_)),
			  Math.min(60330541 * anInt7470 - 1,
				   Math.max(0, i_218_)),
			  768451766);
    }
    
    public void method9271(int i, Class151 class151) {
	aClass151Array7432[i] = class151;
    }
    
    public int method9272(int i, int i_219_) {
	return (null != aShortArrayArray7438
		? aShortArrayArray7438[i][i_219_] & 0xffff : 0);
    }
    
    Class568 method9273(int i, int i_220_, int i_221_) {
	return method9256(i,
			  Math.min(1183912005 * anInt7435 - 1,
				   Math.max(0, i_220_)),
			  Math.min(60330541 * anInt7470 - 1,
				   Math.max(0, i_221_)),
			  768451766);
    }
    
    public void method9274() {
	for (int i = 0; i < anInt7465 * -67005741; i++) {
	    Class654_Sub1_Sub5 class654_sub1_sub5
		= aClass654_Sub1_Sub5Array7454[i];
	    method9337(class654_sub1_sub5, true, -2043298330);
	    aClass654_Sub1_Sub5Array7454[i] = null;
	}
	anInt7465 = 0;
    }
    
    public boolean method9275(Class534_Sub18_Sub16 class534_sub18_sub16, int i,
			      int i_222_, int i_223_, boolean[] bools,
			      byte i_224_) {
	boolean bool = false;
	if (aClass151Array7432 != aClass151Array7436) {
	    int i_225_
		= aClass151Array7434[i].method2498(i_222_, i_223_, 228801165);
	    int i_226_ = 0;
	    for (/**/; i_226_ <= i; i_226_++) {
		Class151 class151 = aClass151Array7434[i_226_];
		if (class151 != null) {
		    int i_227_ = i_225_ - class151.method2498(i_222_, i_223_,
							      -351702028);
		    if (null != bools) {
			bools[i_226_]
			    = class151.method2504(class534_sub18_sub16, i_222_,
						  i_227_, i_223_, 0, false);
			if (!bools[i_226_])
			    continue;
		    }
		    class151.method2502(class534_sub18_sub16, i_222_, i_227_,
					i_223_, 0, false);
		    bool = true;
		}
	    }
	}
	return bool;
    }
    
    public void method9276(Class534_Sub18_Sub16 class534_sub18_sub16, int i,
			   int i_228_, int i_229_, boolean[] bools,
			   int i_230_) {
	if (aClass151Array7432 != aClass151Array7436) {
	    int i_231_
		= aClass151Array7434[i].method2498(i_228_, i_229_, -796787316);
	    for (int i_232_ = 0; i_232_ <= i; i_232_++) {
		if (bools == null || bools[i_232_]) {
		    Class151 class151 = aClass151Array7434[i_232_];
		    if (null != class151)
			class151.method2503
			    (class534_sub18_sub16, i_228_,
			     i_231_ - class151.method2498(i_228_, i_229_,
							  -1117779301),
			     i_229_, 0, false);
		}
	    }
	}
    }
    
    void method9277(int i, int i_233_, int i_234_) {
	Class534_Sub18_Sub16 class534_sub18_sub16 = null;
	for (int i_235_ = i; i_235_ < i_233_; i_235_++) {
	    Class151 class151 = aClass151Array7434[i_235_];
	    if (null != class151) {
		for (int i_236_ = 0; i_236_ < anInt7470 * 60330541; i_236_++) {
		    for (int i_237_ = 0; i_237_ < 1183912005 * anInt7435;
			 i_237_++) {
			class534_sub18_sub16
			    = class151.method2526(i_237_, i_236_,
						  class534_sub18_sub16);
			if (null != class534_sub18_sub16) {
			    int i_238_ = i_237_ << anInt7422 * 941710601;
			    int i_239_ = i_236_ << anInt7422 * 941710601;
			    for (int i_240_ = i_235_ - 1; i_240_ >= 0;
				 i_240_--) {
				Class151 class151_241_
				    = aClass151Array7434[i_240_];
				if (class151_241_ != null) {
				    int i_242_
					= (class151.method2491(i_237_, i_236_,
							       -813167854)
					   - (class151_241_.method2491
					      (i_237_, i_236_, -1149336911)));
				    int i_243_
					= (class151.method2491(1 + i_237_,
							       i_236_,
							       334711259)
					   - (class151_241_.method2491
					      (1 + i_237_, i_236_,
					       1245318941)));
				    int i_244_
					= (class151.method2491(i_237_ + 1,
							       i_236_ + 1,
							       1013166151)
					   - (class151_241_.method2491
					      (i_237_ + 1, i_236_ + 1,
					       765465628)));
				    int i_245_
					= (class151.method2491(i_237_,
							       1 + i_236_,
							       1670459148)
					   - (class151_241_.method2491
					      (i_237_, 1 + i_236_,
					       230128434)));
				    class151_241_.method2502
					(class534_sub18_sub16, i_238_,
					 ((i_244_ + (i_243_ + i_242_) + i_245_)
					  / 4),
					 i_239_, 0, false);
				}
			    }
			}
		    }
		}
	    }
	}
    }
    
    public void method9278(int i) {
	method9277(1, -1381097937 * anInt7418, -1052626615);
    }
    
    Class568 method9279(int i, int i_246_, int i_247_) {
	return method9256(i,
			  Math.min(1183912005 * anInt7435 - 1,
				   Math.max(0, i_246_)),
			  Math.min(60330541 * anInt7470 - 1,
				   Math.max(0, i_247_)),
			  768451766);
    }
    
    public void method9280(byte i) {
	for (int i_248_ = 0; i_248_ < anInt7472 * 1942714221; i_248_++) {
	    if (!aBoolArray7475[i_248_]) {
		Class561 class561 = aClass561Array7439[i_248_];
		Class534_Sub21 class534_sub21 = class561.aClass534_Sub21_7541;
		int i_249_ = class561.anInt7544 * 551684827;
		int i_250_ = (class534_sub21.method16233(143571326)
			      - anInt7421 * -1978691487);
		int i_251_ = (2 * i_250_ >> 941710601 * anInt7422) + 1;
		int i_252_ = 0;
		int[] is = new int[i_251_ * i_251_];
		int i_253_ = (class534_sub21.method16197(572560079) - i_250_
			      >> 941710601 * anInt7422);
		int i_254_ = (class534_sub21.method16199((byte) -78) - i_250_
			      >> 941710601 * anInt7422);
		int i_255_ = (class534_sub21.method16199((byte) -126) + i_250_
			      >> 941710601 * anInt7422);
		if (i_254_ < 0) {
		    i_252_ -= i_254_;
		    i_254_ = 0;
		}
		if (i_255_ >= 60330541 * anInt7470)
		    i_255_ = 60330541 * anInt7470 - 1;
		for (int i_256_ = i_254_; i_256_ <= i_255_; i_256_++) {
		    int i_257_ = class561.aShortArray7531[i_252_];
		    int i_258_ = i_257_ >>> 8;
		    int i_259_ = i_258_ + i_251_ * i_252_;
		    int i_260_ = i_253_ + (i_257_ >>> 8);
		    int i_261_ = i_260_ + (i_257_ & 0xff) - 1;
		    if (i_260_ < 0) {
			i_259_ -= i_260_;
			i_260_ = 0;
		    }
		    if (i_261_ >= 1183912005 * anInt7435)
			i_261_ = anInt7435 * 1183912005 - 1;
		    for (int i_262_ = i_260_; i_262_ <= i_261_; i_262_++) {
			int i_263_ = 1;
			Class654_Sub1_Sub5 class654_sub1_sub5
			    = method9262(i_249_, i_262_, i_256_, null,
					 (byte) -83);
			if (null != class654_sub1_sub5
			    && 0 != class654_sub1_sub5.aByte11899) {
			    if (class654_sub1_sub5.aByte11899 == 1) {
				boolean bool = i_262_ - 1 >= i_260_;
				boolean bool_264_ = i_262_ + 1 <= i_261_;
				if (!bool && 1 + i_256_ <= i_255_) {
				    int i_265_
					= class561.aShortArray7531[1 + i_252_];
				    int i_266_ = i_253_ + (i_265_ >>> 8);
				    int i_267_ = i_266_ + (i_265_ & 0xff);
				    bool = i_262_ > i_266_ && i_262_ < i_267_;
				}
				if (!bool_264_ && i_256_ - 1 >= i_254_) {
				    int i_268_
					= class561.aShortArray7531[i_252_ - 1];
				    int i_269_ = i_253_ + (i_268_ >>> 8);
				    int i_270_ = i_269_ + (i_268_ & 0xff);
				    bool_264_
					= i_262_ > i_269_ && i_262_ < i_270_;
				}
				if (bool && !bool_264_)
				    i_263_ = 4;
				else if (bool_264_ && !bool)
				    i_263_ = 2;
			    } else {
				boolean bool = i_262_ - 1 >= i_260_;
				boolean bool_271_ = i_262_ + 1 <= i_261_;
				if (!bool && i_256_ - 1 >= i_254_) {
				    int i_272_
					= class561.aShortArray7531[i_252_ - 1];
				    int i_273_ = (i_272_ >>> 8) + i_253_;
				    int i_274_ = i_273_ + (i_272_ & 0xff);
				    bool = i_262_ > i_273_ && i_262_ < i_274_;
				}
				if (!bool_271_ && i_256_ + 1 <= i_255_) {
				    int i_275_
					= class561.aShortArray7531[1 + i_252_];
				    int i_276_ = i_253_ + (i_275_ >>> 8);
				    int i_277_ = i_276_ + (i_275_ & 0xff);
				    bool_271_
					= i_262_ > i_276_ && i_262_ < i_277_;
				}
				if (bool && !bool_271_)
				    i_263_ = 3;
				else if (bool_271_ && !bool)
				    i_263_ = 5;
			    }
			}
			is[i_259_++] = i_263_;
		    }
		    i_252_++;
		}
		aBoolArray7475[i_248_] = true;
		if (aBool7440)
		    aClass151Array7432[i_249_].method2530(class534_sub21, is);
	    }
	}
    }
    
    public Class654_Sub1_Sub3 method9281(int i, int i_278_, int i_279_,
					 int i_280_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_278_][i_279_];
	if (null != class568) {
	    method9257(class568.aClass654_Sub1_Sub3_7601, -562399744);
	    if (class568.aClass654_Sub1_Sub3_7601 != null) {
		Class654_Sub1_Sub3 class654_sub1_sub3
		    = class568.aClass654_Sub1_Sub3_7601;
		class568.aClass654_Sub1_Sub3_7601 = null;
		return class654_sub1_sub3;
	    }
	}
	return null;
    }
    
    public HashMap method9282(int i) {
	aHashMap7477.clear();
	Iterator iterator = aHashMap7429.entrySet().iterator();
	while (iterator.hasNext()) {
	    Map.Entry entry = (Map.Entry) iterator.next();
	    aHashMap7477.put(entry.getKey(),
			     (((Class561) ((List) entry.getValue()).get(0))
			      .aClass534_Sub21_7541));
	}
	return aHashMap7477;
    }
    
    public int method9283(int i, int i_281_, byte i_282_) {
	return (null != aShortArrayArray7438
		? aShortArrayArray7438[i][i_281_] & 0xffff : 0);
    }
    
    public void method9284(int i) {
	/* empty */
    }
    
    void method9285(Class654_Sub1[] class654_sub1s, int i, int i_283_) {
	if (i < i_283_) {
	    int i_284_ = (i_283_ + i) / 2;
	    int i_285_ = i;
	    Class654_Sub1 class654_sub1 = class654_sub1s[i_284_];
	    class654_sub1s[i_284_] = class654_sub1s[i_283_];
	    class654_sub1s[i_283_] = class654_sub1;
	    int i_286_ = class654_sub1.anInt10851 * 621186375;
	    for (int i_287_ = i; i_287_ < i_283_; i_287_++) {
		if (class654_sub1s[i_287_].anInt10851 * 621186375
		    > i_286_ + (i_287_ & 0x1)) {
		    Class654_Sub1 class654_sub1_288_ = class654_sub1s[i_287_];
		    class654_sub1s[i_287_] = class654_sub1s[i_285_];
		    class654_sub1s[i_285_++] = class654_sub1_288_;
		}
	    }
	    class654_sub1s[i_283_] = class654_sub1s[i_285_];
	    class654_sub1s[i_285_] = class654_sub1;
	    method9236(class654_sub1s, i, i_285_ - 1);
	    method9236(class654_sub1s, 1 + i_285_, i_283_);
	}
    }
    
    public void method9286(int i) {
	/* empty */
    }
    
    void method9287(Class654_Sub1 class654_sub1, byte i) {
	Class438 class438 = class654_sub1.method10807().aClass438_4885;
	aClass185_7453.method3367((float) (int) class438.aFloat4864,
				  (float) ((int) class438.aFloat4863
					   + (class654_sub1
						  .method16876(-740800334)
					      >> 1)),
				  (float) (int) class438.aFloat4865,
				  aFloatArray7468);
	class654_sub1.anInt10851 = 1350765687 * (int) aFloatArray7468[2];
    }
    
    public void method9288(boolean bool) {
	if (bool) {
	    aClass568ArrayArrayArray7431 = aClass568ArrayArrayArray7460;
	    aClass151Array7432 = aClass151Array7436;
	} else {
	    aClass568ArrayArrayArray7431 = aClass568ArrayArrayArray7433;
	    aClass151Array7432 = aClass151Array7434;
	}
	anInt7418 = -1525290801 * aClass568ArrayArrayArray7431.length;
    }
    
    public void method9289(int i, int i_289_, int i_290_, int i_291_) {
	List list = (List) aHashMap7429.get(Integer.valueOf(i));
	if (list != null) {
	    Iterator iterator = list.iterator();
	    while (iterator.hasNext()) {
		Class561 class561 = (Class561) iterator.next();
		class561.aClass534_Sub21_7541.method16235(i_289_, i_290_,
							  (byte) 93);
	    }
	}
    }
    
    public void method9290() {
	for (int i = 0; i < 1183912005 * anInt7435; i++) {
	    for (int i_292_ = 0; i_292_ < anInt7470 * 60330541; i_292_++) {
		if (null == aClass568ArrayArrayArray7431[0][i][i_292_])
		    aClass568ArrayArrayArray7431[0][i][i_292_]
			= new Class568(0);
	    }
	}
    }
    
    void method9291(boolean bool, byte[][][] is, int i, byte i_293_,
		    int i_294_) {
	int i_295_ = bool ? 1 : 0;
	anInt7450 = 0;
	anInt7451 = 0;
	anInt7428 += 964746483;
	if ((i_294_ & 0x2) == 0) {
	    for (Class654_Sub1 class654_sub1 = aClass654_Sub1Array7447[i_295_];
		 class654_sub1 != null;
		 class654_sub1 = class654_sub1.aClass654_Sub1_10850) {
		method9287(class654_sub1, (byte) 27);
		if (class654_sub1.anInt10851 * 621186375 != -1
		    && !method9328(class654_sub1, bool, is, i, i_293_))
		    aClass654_Sub1Array7424
			[(anInt7450 += 744803731) * 1319065755 - 1]
			= class654_sub1;
	    }
	}
	if ((i_294_ & 0x1) == 0) {
	    for (Class654_Sub1 class654_sub1 = aClass654_Sub1Array7448[i_295_];
		 class654_sub1 != null;
		 class654_sub1 = class654_sub1.aClass654_Sub1_10850) {
		method9287(class654_sub1, (byte) 63);
		if (-1 != class654_sub1.anInt10851 * 621186375
		    && !method9328(class654_sub1, bool, is, i, i_293_))
		    aClass654_Sub1Array7452
			[(anInt7451 += -755338105) * -219410121 - 1]
			= class654_sub1;
	    }
	    for (Class654_Sub1 class654_sub1 = aClass654_Sub1Array7449[i_295_];
		 null != class654_sub1;
		 class654_sub1 = class654_sub1.aClass654_Sub1_10850) {
		method9287(class654_sub1, (byte) 100);
		if (-1 != class654_sub1.anInt10851 * 621186375
		    && !method9328(class654_sub1, bool, is, i, i_293_)) {
		    if (class654_sub1.method16850(-1920390363))
			aClass654_Sub1Array7452
			    [(anInt7451 += -755338105) * -219410121 - 1]
			    = class654_sub1;
		    else
			aClass654_Sub1Array7424
			    [(anInt7450 += 744803731) * 1319065755 - 1]
			    = class654_sub1;
		}
	    }
	    if (!bool) {
		for (int i_296_ = 0; i_296_ < anInt7465 * -67005741;
		     i_296_++) {
		    method9287(aClass654_Sub1_Sub5Array7454[i_296_],
			       (byte) 36);
		    if ((aClass654_Sub1_Sub5Array7454[i_296_].anInt10851
			 * 621186375) != -1
			&& !method9328(aClass654_Sub1_Sub5Array7454[i_296_],
				       bool, is, i, i_293_)) {
			if (aClass654_Sub1_Sub5Array7454[i_296_]
				.method16850(-1920390363))
			    aClass654_Sub1Array7452
				[(anInt7451 += -755338105) * -219410121 - 1]
				= aClass654_Sub1_Sub5Array7454[i_296_];
			else
			    aClass654_Sub1Array7424
				[(anInt7450 += 744803731) * 1319065755 - 1]
				= aClass654_Sub1_Sub5Array7454[i_296_];
		    }
		}
	    }
	}
	if (anInt7450 * 1319065755 > 0) {
	    method9312(aClass654_Sub1Array7424, 0, 1319065755 * anInt7450 - 1);
	    for (int i_297_ = 0; i_297_ < anInt7450 * 1319065755; i_297_++)
		method9234(aClass654_Sub1Array7424[i_297_],
			   aClass534_Sub21Array7469);
	}
	if (aBool7443)
	    aClass185_7453.method3654(0, null);
	if (0 == (i_294_ & 0x2)) {
	    for (int i_298_ = 0; i_298_ < -1381097937 * anInt7418; i_298_++) {
		if (i_298_ >= i && null != is) {
		    int i_299_ = aBoolArrayArray7481.length;
		    if (-474621401 * anInt7419 + aBoolArrayArray7481.length
			> anInt7435 * 1183912005)
			i_299_ -= (anInt7419 * -474621401
				   + aBoolArrayArray7481.length
				   - 1183912005 * anInt7435);
		    int i_300_ = aBoolArrayArray7481[0].length;
		    if (anInt7430 * 1375305047 + aBoolArrayArray7481[0].length
			> anInt7470 * 60330541)
			i_300_ -= (1375305047 * anInt7430
				   + aBoolArrayArray7481[0].length
				   - 60330541 * anInt7470);
		    boolean[][] bools = aBoolArrayArray7479;
		    if (aBool7476) {
			for (int i_301_ = anInt7425 * 951958497;
			     i_301_ < i_299_; i_301_++) {
			    int i_302_ = (i_301_ + anInt7419 * -474621401
					  - anInt7425 * 951958497);
			    for (int i_303_ = anInt7463 * -856129721;
				 i_303_ < i_300_; i_303_++) {
				bools[i_301_][i_303_] = false;
				if (aBoolArrayArray7481[i_301_][i_303_]) {
				    int i_304_
					= (1375305047 * anInt7430 + i_303_
					   - -856129721 * anInt7463);
				    for (int i_305_ = i_298_; i_305_ >= 0;
					 i_305_--) {
					if ((null
					     != (aClass568ArrayArrayArray7431
						 [i_305_][i_302_][i_304_]))
					    && (i_298_
						== (aClass568ArrayArrayArray7431
						    [i_305_][i_302_][i_304_]
						    .aByte7599))) {
					    if ((i_305_ >= i
						 && (is[i_305_][i_302_][i_304_]
						     == i_293_))
						|| (aClass552_7427.method9062
						    (i_298_, i_302_, i_304_)))
						bools[i_301_][i_303_] = false;
					    else
						bools[i_301_][i_303_] = true;
					    break;
					}
				    }
				}
			    }
			}
		    }
		    aClass151Array7432[i_298_].method2506
			(anInt7455 * -380604831, anInt7459 * -1709472547,
			 -1213435377 * anInt7461, aBoolArrayArray7479, false,
			 i_294_);
		} else {
		    int i_306_ = aBoolArrayArray7481.length;
		    if (anInt7419 * -474621401 + aBoolArrayArray7481.length
			> anInt7435 * 1183912005)
			i_306_ -= (anInt7419 * -474621401
				   + aBoolArrayArray7481.length
				   - anInt7435 * 1183912005);
		    int i_307_ = aBoolArrayArray7481[0].length;
		    if (anInt7430 * 1375305047 + aBoolArrayArray7481[0].length
			> 60330541 * anInt7470)
			i_307_ -= (aBoolArrayArray7481[0].length
				   + 1375305047 * anInt7430
				   - anInt7470 * 60330541);
		    boolean[][] bools = aBoolArrayArray7479;
		    if (aBool7476) {
			for (int i_308_ = anInt7425 * 951958497;
			     i_308_ < i_306_; i_308_++) {
			    int i_309_ = (i_308_ + -474621401 * anInt7419
					  - 951958497 * anInt7425);
			    for (int i_310_ = anInt7463 * -856129721;
				 i_310_ < i_307_; i_310_++) {
				if (aBoolArrayArray7481[i_308_][i_310_]
				    && !(aClass552_7427.method9062
					 (i_298_, i_309_,
					  (i_310_ + 1375305047 * anInt7430
					   - -856129721 * anInt7463))))
				    bools[i_308_][i_310_] = true;
				else
				    bools[i_308_][i_310_] = false;
			    }
			}
		    }
		    aClass151Array7432[i_298_].method2506
			(anInt7455 * -380604831, -1709472547 * anInt7459,
			 -1213435377 * anInt7461, aBoolArrayArray7479, true,
			 i_294_);
		}
	    }
	}
	if (-219410121 * anInt7451 > 0) {
	    method9236(aClass654_Sub1Array7452, 0, -219410121 * anInt7451 - 1);
	    for (int i_311_ = 0; i_311_ < -219410121 * anInt7451; i_311_++)
		method9234(aClass654_Sub1Array7452[i_311_],
			   aClass534_Sub21Array7469);
	}
    }
    
    public void method9292() {
	for (int i = 0; i < 1183912005 * anInt7435; i++) {
	    for (int i_312_ = 0; i_312_ < anInt7470 * 60330541; i_312_++) {
		if (null == aClass568ArrayArrayArray7431[0][i][i_312_])
		    aClass568ArrayArrayArray7431[0][i][i_312_]
			= new Class568(0);
	    }
	}
    }
    
    public void method9293(int i, int i_313_) {
	Class568 class568 = aClass568ArrayArrayArray7431[0][i][i_313_];
	for (int i_314_ = 0; i_314_ < 3; i_314_++) {
	    Class568 class568_315_
		= (aClass568ArrayArrayArray7431[i_314_][i][i_313_]
		   = aClass568ArrayArrayArray7431[i_314_ + 1][i][i_313_]);
	    if (null != class568_315_) {
		for (Class559 class559 = class568_315_.aClass559_7604;
		     class559 != null; class559 = class559.aClass559_7497) {
		    Class654_Sub1_Sub5 class654_sub1_sub5
			= class559.aClass654_Sub1_Sub5_7500;
		    if (class654_sub1_sub5.aShort11900 == i
			&& class654_sub1_sub5.aShort11901 == i_313_)
			class654_sub1_sub5.aByte10854--;
		}
		if (null != class568_315_.aClass654_Sub1_Sub2_7607)
		    class568_315_.aClass654_Sub1_Sub2_7607.aByte10854--;
		if (null != class568_315_.aClass654_Sub1_Sub1_7598)
		    class568_315_.aClass654_Sub1_Sub1_7598.aByte10854--;
		if (null != class568_315_.aClass654_Sub1_Sub1_7597)
		    class568_315_.aClass654_Sub1_Sub1_7597.aByte10854--;
		if (null != class568_315_.aClass654_Sub1_Sub3_7600)
		    class568_315_.aClass654_Sub1_Sub3_7600.aByte10854--;
		if (class568_315_.aClass654_Sub1_Sub3_7601 != null)
		    class568_315_.aClass654_Sub1_Sub3_7601.aByte10854--;
	    }
	}
	if (aClass568ArrayArrayArray7431[0][i][i_313_] == null) {
	    aClass568ArrayArrayArray7431[0][i][i_313_] = new Class568(0);
	    aClass568ArrayArrayArray7431[0][i][i_313_].aByte7599 = (byte) 1;
	}
	aClass568ArrayArrayArray7431[0][i][i_313_].aClass568_7605 = class568;
	aClass568ArrayArrayArray7431[3][i][i_313_] = null;
    }
    
    public void method9294(int i, int i_316_) {
	Class568 class568 = aClass568ArrayArrayArray7431[0][i][i_316_];
	for (int i_317_ = 0; i_317_ < 3; i_317_++) {
	    Class568 class568_318_
		= (aClass568ArrayArrayArray7431[i_317_][i][i_316_]
		   = aClass568ArrayArrayArray7431[i_317_ + 1][i][i_316_]);
	    if (null != class568_318_) {
		for (Class559 class559 = class568_318_.aClass559_7604;
		     class559 != null; class559 = class559.aClass559_7497) {
		    Class654_Sub1_Sub5 class654_sub1_sub5
			= class559.aClass654_Sub1_Sub5_7500;
		    if (class654_sub1_sub5.aShort11900 == i
			&& class654_sub1_sub5.aShort11901 == i_316_)
			class654_sub1_sub5.aByte10854--;
		}
		if (null != class568_318_.aClass654_Sub1_Sub2_7607)
		    class568_318_.aClass654_Sub1_Sub2_7607.aByte10854--;
		if (null != class568_318_.aClass654_Sub1_Sub1_7598)
		    class568_318_.aClass654_Sub1_Sub1_7598.aByte10854--;
		if (null != class568_318_.aClass654_Sub1_Sub1_7597)
		    class568_318_.aClass654_Sub1_Sub1_7597.aByte10854--;
		if (null != class568_318_.aClass654_Sub1_Sub3_7600)
		    class568_318_.aClass654_Sub1_Sub3_7600.aByte10854--;
		if (class568_318_.aClass654_Sub1_Sub3_7601 != null)
		    class568_318_.aClass654_Sub1_Sub3_7601.aByte10854--;
	    }
	}
	if (aClass568ArrayArrayArray7431[0][i][i_316_] == null) {
	    aClass568ArrayArrayArray7431[0][i][i_316_] = new Class568(0);
	    aClass568ArrayArrayArray7431[0][i][i_316_].aByte7599 = (byte) 1;
	}
	aClass568ArrayArrayArray7431[0][i][i_316_].aClass568_7605 = class568;
	aClass568ArrayArrayArray7431[3][i][i_316_] = null;
    }
    
    public void method9295(Class561 class561) {
	if (1942714221 * anInt7472 < 65165) {
	    Class534_Sub21 class534_sub21 = class561.aClass534_Sub21_7541;
	    aClass561Array7439[1942714221 * anInt7472] = class561;
	    aBoolArray7475[anInt7472 * 1942714221] = false;
	    anInt7472 += -328586651;
	    int i = class561.anInt7544 * 551684827;
	    if (class561.aBool7543)
		i = 0;
	    int i_319_ = 551684827 * class561.anInt7544;
	    if (class561.aBool7542)
		i_319_ = anInt7418 * -1381097937 - 1;
	    for (int i_320_ = i; i_320_ <= i_319_; i_320_++) {
		int i_321_ = 0;
		int i_322_ = ((class534_sub21.method16199((byte) -113)
			       - class534_sub21.method16233(-1053005281)
			       + anInt7421 * -1978691487)
			      >> 941710601 * anInt7422);
		if (i_322_ < 0) {
		    i_321_ -= i_322_;
		    i_322_ = 0;
		}
		int i_323_ = ((class534_sub21.method16199((byte) -19)
			       + class534_sub21.method16233(-648969891)
			       - -1978691487 * anInt7421)
			      >> anInt7422 * 941710601);
		if (i_323_ >= 60330541 * anInt7470)
		    i_323_ = anInt7470 * 60330541 - 1;
		for (int i_324_ = i_322_; i_324_ <= i_323_; i_324_++) {
		    int i_325_ = class561.aShortArray7531[i_321_++];
		    int i_326_ = (((class534_sub21.method16197(368560942)
				    - class534_sub21.method16233(-1558983710)
				    + -1978691487 * anInt7421)
				   >> 941710601 * anInt7422)
				  + (i_325_ >>> 8));
		    int i_327_ = i_326_ + (i_325_ & 0xff) - 1;
		    if (i_326_ < 0)
			i_326_ = 0;
		    if (i_327_ >= 1183912005 * anInt7435)
			i_327_ = anInt7435 * 1183912005 - 1;
		    for (int i_328_ = i_326_; i_328_ <= i_327_; i_328_++) {
			long l
			    = aLongArrayArrayArray7473[i_320_][i_328_][i_324_];
			if ((l & 0xffffL) == 0L)
			    aLongArrayArrayArray7473[i_320_][i_328_][i_324_]
				= l | (long) (anInt7472 * 1942714221);
			else if ((l & 0xffff0000L) == 0L)
			    aLongArrayArrayArray7473[i_320_][i_328_][i_324_]
				= l | (long) (1942714221 * anInt7472) << 16;
			else if (0L == (l & 0xffff00000000L))
			    aLongArrayArrayArray7473[i_320_][i_328_][i_324_]
				= l | (long) (anInt7472 * 1942714221) << 32;
			else if ((l & ~0xffffffffffffL) == 0L)
			    aLongArrayArrayArray7473[i_320_][i_328_][i_324_]
				= l | (long) (anInt7472 * 1942714221) << 48;
		    }
		}
	    }
	    if (-1 != class561.anInt7546 * -1446450775) {
		List list
		    = (List) aHashMap7429.get(Integer.valueOf(-1446450775
							      * (class561
								 .anInt7546)));
		if (list == null) {
		    list = new ArrayList();
		    aHashMap7429.put(Integer.valueOf(-1446450775
						     * class561.anInt7546),
				     list);
		}
		list.add(class561);
		Class534_Sub21 class534_sub21_329_
		    = ((Class534_Sub21)
		       aHashMap7477.get(Integer.valueOf(-1446450775
							* (class561
							   .anInt7546))));
		if (class534_sub21_329_ != null) {
		    class561.aClass534_Sub21_7541.method16207
			(class534_sub21_329_.method16201((byte) 77),
			 1622605085);
		    class561.aClass534_Sub21_7541.method16235
			(class534_sub21_329_.method16230((byte) -26),
			 class534_sub21_329_.method16206((byte) 59),
			 (byte) 115);
		}
	    }
	}
    }
    
    public int method9296(int i, int i_330_) {
	return (null != aByteArrayArray7480
		? aByteArrayArray7480[i][i_330_] & 0xff : 0);
    }
    
    public void method9297(int i) {
	/* empty */
    }
    
    public void method9298(int i, int i_331_, int i_332_) {
	boolean bool = (null != aClass568ArrayArrayArray7431[0][i_331_][i_332_]
			&& (aClass568ArrayArrayArray7431[0][i_331_][i_332_]
			    .aClass568_7605) != null);
	for (int i_333_ = i; i_333_ >= 0; i_333_--) {
	    if (aClass568ArrayArrayArray7431[i_333_][i_331_][i_332_] == null) {
		Class568 class568
		    = (aClass568ArrayArrayArray7431[i_333_][i_331_][i_332_]
		       = new Class568(i_333_));
		if (bool)
		    class568.aByte7599++;
	    }
	}
    }
    
    public HashMap method9299() {
	aHashMap7477.clear();
	Iterator iterator = aHashMap7429.entrySet().iterator();
	while (iterator.hasNext()) {
	    Map.Entry entry = (Map.Entry) iterator.next();
	    aHashMap7477.put(entry.getKey(),
			     (((Class561) ((List) entry.getValue()).get(0))
			      .aClass534_Sub21_7541));
	}
	return aHashMap7477;
    }
    
    void method9300(Class654_Sub1 class654_sub1,
		    Class534_Sub21[] class534_sub21s) {
	if (aBool7443) {
	    int i = class654_sub1.method16856(class534_sub21s, 308999563);
	    aClass185_7453.method3654(i, class534_sub21s);
	}
	if (aClass151Array7436 == aClass151Array7432) {
	    boolean bool = false;
	    boolean bool_334_ = false;
	    Class438 class438 = class654_sub1.method10807().aClass438_4885;
	    int i;
	    int i_335_;
	    if (class654_sub1 instanceof Class654_Sub1_Sub5) {
		i = ((Class654_Sub1_Sub5) class654_sub1).aShort11900;
		i_335_ = ((Class654_Sub1_Sub5) class654_sub1).aShort11901;
	    } else {
		i = (int) class438.aFloat4864 >> anInt7422 * 941710601;
		i_335_ = (int) class438.aFloat4865 >> 941710601 * anInt7422;
	    }
	    i = Math.min(1183912005 * anInt7435 - 1, Math.max(0, i));
	    i_335_ = Math.min(anInt7470 * 60330541 - 1, Math.max(0, i_335_));
	    Class166 class166 = new Class166();
	    class166.anInt1763 = method9305(i, i_335_, -845342674) * 929497037;
	    class166.anInt1762
		= method9283(i, i_335_, (byte) -95) * 1063224577;
	    class166.anInt1764
		= method9331(i, i_335_, 1136503272) * -1101271945;
	    class166.anInt1765
		= method9238(i, i_335_, (byte) -58) * -1961234339;
	    class166.anInt1766
		= method9239(i, i_335_, 604886291) * -1338809385;
	    class166.anInt1767
		= method9310(i, i_335_, (short) -18684) * -2041662327;
	    aClass185_7453.method3364
		(aClass151Array7434[0].method2498((int) class438.aFloat4864,
						  (int) class438.aFloat4865,
						  -2143346464),
		 class166);
	}
	Class550 class550
	    = class654_sub1.method16853(aClass185_7453, 308999563);
	if (class550 != null) {
	    if (class550.aBool7305) {
		class550.aClass654_Sub1_7303 = class654_sub1;
		aClass557_7478.method9402(class550, (byte) 98);
	    } else
		Class42.method1063(class550, 1262399261);
	}
    }
    
    public void method9301() {
	for (int i = 0; i < anInt7418 * -1381097937; i++) {
	    for (int i_336_ = 0; i_336_ < 1183912005 * anInt7435; i_336_++) {
		for (int i_337_ = 0; i_337_ < 60330541 * anInt7470; i_337_++) {
		    Class568 class568
			= aClass568ArrayArrayArray7431[i][i_336_][i_337_];
		    if (null != class568) {
			Class654_Sub1_Sub1 class654_sub1_sub1
			    = class568.aClass654_Sub1_Sub1_7598;
			Class654_Sub1_Sub1 class654_sub1_sub1_338_
			    = class568.aClass654_Sub1_Sub1_7597;
			if (class654_sub1_sub1 != null
			    && class654_sub1_sub1.method16848((byte) 20)) {
			    method9267(class654_sub1_sub1, i, i_336_, i_337_,
				       1, 1);
			    if (null != class654_sub1_sub1_338_
				&& class654_sub1_sub1_338_
				       .method16848((byte) 34)) {
				method9267(class654_sub1_sub1_338_, i, i_336_,
					   i_337_, 1, 1);
				class654_sub1_sub1_338_.method16851
				    (aClass185_7453, class654_sub1_sub1, 0, 0,
				     0, false, 308999563);
				class654_sub1_sub1_338_
				    .method16852(1863389464);
			    }
			    class654_sub1_sub1.method16852(1617913550);
			}
			for (Class559 class559 = class568.aClass559_7604;
			     class559 != null;
			     class559 = class559.aClass559_7497) {
			    Class654_Sub1_Sub5 class654_sub1_sub5
				= class559.aClass654_Sub1_Sub5_7500;
			    if (class654_sub1_sub5 != null
				&& class654_sub1_sub5.method16848((byte) 15)) {
				method9267(class654_sub1_sub5, i, i_336_,
					   i_337_,
					   (class654_sub1_sub5.aShort11896
					    - class654_sub1_sub5.aShort11900
					    + 1),
					   (class654_sub1_sub5.aShort11898
					    - class654_sub1_sub5.aShort11901
					    + 1));
				class654_sub1_sub5.method16852(1368651373);
			    }
			}
			Class654_Sub1_Sub2 class654_sub1_sub2
			    = class568.aClass654_Sub1_Sub2_7607;
			if (class654_sub1_sub2 != null
			    && class654_sub1_sub2.method16848((byte) 87)) {
			    method9325(class654_sub1_sub2, i, i_336_, i_337_,
				       -2072382502);
			    class654_sub1_sub2.method16852(1396300831);
			}
		    }
		}
	    }
	}
    }
    
    public void method9302(int i, Class151 class151) {
	aClass151Array7432[i] = class151;
    }
    
    public void method9303(int i, Class151 class151) {
	aClass151Array7432[i] = class151;
    }
    
    public int method9304(int i, int i_339_) {
	return (null != aByteArrayArray7474
		? aByteArrayArray7474[i][i_339_] & 0xff : 0);
    }
    
    public int method9305(int i, int i_340_, int i_341_) {
	return (anIntArrayArray7437 != null
		? anIntArrayArray7437[i][i_340_] & 0xffffff : 0);
    }
    
    public int method9306(int i, int i_342_) {
	return (anIntArrayArray7437 != null
		? anIntArrayArray7437[i][i_342_] & 0xffffff : 0);
    }
    
    public int method9307(int i, int i_343_) {
	return (anIntArrayArray7437 != null
		? anIntArrayArray7437[i][i_343_] & 0xffffff : 0);
    }
    
    public int method9308(int i, int i_344_) {
	return (anIntArrayArray7437 != null
		? anIntArrayArray7437[i][i_344_] & 0xffffff : 0);
    }
    
    public void method9309(int i, Class151 class151, int i_345_) {
	aClass151Array7432[i] = class151;
    }
    
    public int method9310(int i, int i_346_, short i_347_) {
	return (aByteArrayArray7423 != null
		? aByteArrayArray7423[i][i_346_] & 0xff : 0);
    }
    
    public Class654_Sub1_Sub5 method9311(int i, int i_348_, int i_349_,
					 Interface64 interface64) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_348_][i_349_];
	if (class568 == null)
	    return null;
	for (Class559 class559 = class568.aClass559_7604; null != class559;
	     class559 = class559.aClass559_7497) {
	    Class654_Sub1_Sub5 class654_sub1_sub5
		= class559.aClass654_Sub1_Sub5_7500;
	    if ((null == interface64
		 || interface64.method432(class654_sub1_sub5, (byte) -88))
		&& i_348_ == class654_sub1_sub5.aShort11900
		&& class654_sub1_sub5.aShort11901 == i_349_) {
		method9337(class654_sub1_sub5, false, -2063681178);
		return class654_sub1_sub5;
	    }
	}
	return null;
    }
    
    void method9312(Class654_Sub1[] class654_sub1s, int i, int i_350_) {
	if (i < i_350_) {
	    int i_351_ = (i_350_ + i) / 2;
	    int i_352_ = i;
	    Class654_Sub1 class654_sub1 = class654_sub1s[i_351_];
	    class654_sub1s[i_351_] = class654_sub1s[i_350_];
	    class654_sub1s[i_350_] = class654_sub1;
	    int i_353_ = class654_sub1.anInt10851 * 621186375;
	    for (int i_354_ = i; i_354_ < i_350_; i_354_++) {
		if (class654_sub1s[i_354_].anInt10851 * 621186375
		    < i_353_ + (i_354_ & 0x1)) {
		    Class654_Sub1 class654_sub1_355_ = class654_sub1s[i_354_];
		    class654_sub1s[i_354_] = class654_sub1s[i_352_];
		    class654_sub1s[i_352_++] = class654_sub1_355_;
		}
	    }
	    class654_sub1s[i_350_] = class654_sub1s[i_352_];
	    class654_sub1s[i_352_] = class654_sub1;
	    method9312(class654_sub1s, i, i_352_ - 1);
	    method9312(class654_sub1s, i_352_ + 1, i_350_);
	}
    }
    
    public int method9313(int i, int i_356_) {
	return (null != aByteArrayArray7474
		? aByteArrayArray7474[i][i_356_] & 0xff : 0);
    }
    
    public int method9314(int i, int i_357_) {
	return (null != aByteArrayArray7474
		? aByteArrayArray7474[i][i_357_] & 0xff : 0);
    }
    
    public int method9315(int i, int i_358_) {
	return (aByteArrayArray7441 != null
		? aByteArrayArray7441[i][i_358_] & 0xff : 0);
    }
    
    public Class654_Sub1_Sub1 method9316(int i, int i_359_, int i_360_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_359_][i_360_];
	if (class568 == null)
	    return null;
	return class568.aClass654_Sub1_Sub1_7597;
    }
    
    public Class654_Sub1_Sub4 method9317(int i, int i_361_, int i_362_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_361_][i_362_];
	if (class568 == null)
	    return null;
	return class568.aClass654_Sub1_Sub4_7602;
    }
    
    public void method9318(int i, int i_363_, int i_364_, int i_365_,
			   int i_366_, int i_367_, int i_368_, int i_369_) {
	if (anIntArrayArray7437 != null)
	    anIntArrayArray7437[i][i_363_] = ~0xffffff | i_364_;
	if (aShortArrayArray7438 != null)
	    aShortArrayArray7438[i][i_363_] = (short) i_365_;
	if (null != aByteArrayArray7480)
	    aByteArrayArray7480[i][i_363_] = (byte) i_366_;
	if (aByteArrayArray7474 != null)
	    aByteArrayArray7474[i][i_363_] = (byte) i_367_;
	if (null != aByteArrayArray7441)
	    aByteArrayArray7441[i][i_363_] = (byte) i_368_;
	if (aByteArrayArray7423 != null)
	    aByteArrayArray7423[i][i_363_] = (byte) i_369_;
    }
    
    public void method9319(int i, int i_370_, int i_371_, int i_372_,
			   int i_373_, int i_374_, int i_375_, int i_376_) {
	if (anIntArrayArray7437 != null)
	    anIntArrayArray7437[i][i_370_] = ~0xffffff | i_371_;
	if (aShortArrayArray7438 != null)
	    aShortArrayArray7438[i][i_370_] = (short) i_372_;
	if (null != aByteArrayArray7480)
	    aByteArrayArray7480[i][i_370_] = (byte) i_373_;
	if (aByteArrayArray7474 != null)
	    aByteArrayArray7474[i][i_370_] = (byte) i_374_;
	if (null != aByteArrayArray7441)
	    aByteArrayArray7441[i][i_370_] = (byte) i_375_;
	if (aByteArrayArray7423 != null)
	    aByteArrayArray7423[i][i_370_] = (byte) i_376_;
    }
    
    public void method9320(int i, int i_377_, int i_378_, int i_379_,
			   int i_380_, int i_381_, int i_382_, int i_383_) {
	if (anIntArrayArray7437 != null)
	    anIntArrayArray7437[i][i_377_] = ~0xffffff | i_378_;
	if (aShortArrayArray7438 != null)
	    aShortArrayArray7438[i][i_377_] = (short) i_379_;
	if (null != aByteArrayArray7480)
	    aByteArrayArray7480[i][i_377_] = (byte) i_380_;
	if (aByteArrayArray7474 != null)
	    aByteArrayArray7474[i][i_377_] = (byte) i_381_;
	if (null != aByteArrayArray7441)
	    aByteArrayArray7441[i][i_377_] = (byte) i_382_;
	if (aByteArrayArray7423 != null)
	    aByteArrayArray7423[i][i_377_] = (byte) i_383_;
    }
    
    public void method9321(int i, int i_384_, int i_385_,
			   Class654_Sub1_Sub2 class654_sub1_sub2) {
	Class568 class568 = method9256(i, i_384_, i_385_, 768451766);
	if (null != class568) {
	    class568.aClass654_Sub1_Sub2_7607 = class654_sub1_sub2;
	    int i_386_ = aClass151Array7432 == aClass151Array7436 ? 1 : 0;
	    if (class654_sub1_sub2.method16849(-504468630)) {
		if (class654_sub1_sub2.method16850(-1920390363)) {
		    class654_sub1_sub2.aClass654_Sub1_10850
			= aClass654_Sub1Array7448[i_386_];
		    aClass654_Sub1Array7448[i_386_] = class654_sub1_sub2;
		} else {
		    class654_sub1_sub2.aClass654_Sub1_10850
			= aClass654_Sub1Array7447[i_386_];
		    aClass654_Sub1Array7447[i_386_] = class654_sub1_sub2;
		}
	    } else {
		class654_sub1_sub2.aClass654_Sub1_10850
		    = aClass654_Sub1Array7449[i_386_];
		aClass654_Sub1Array7449[i_386_] = class654_sub1_sub2;
	    }
	}
    }
    
    public Class556(Class185 class185, int i, int i_387_, int i_388_,
		    int i_389_, int i_390_, boolean bool, boolean bool_391_) {
	anInt7444 = 10083;
	anInt7445 = 5098;
	anInt7446 = 5078;
	aFloatArray7468 = new float[3];
	aClass534_Sub21Array7469 = new Class534_Sub21[8];
	anInt7464 = 65165;
	aHashMap7429 = new HashMap();
	aHashMap7477 = new HashMap();
	aClass185_7453 = class185;
	aBool7443 = aClass185_7453.method3344() > 0;
	anInt7422 = i * 2015052089;
	anInt7420 = -1848530245 * (1 << anInt7422 * 941710601);
	anInt7421 = (anInt7420 * -1935734157 >> 1) * 1088361377;
	anInt7418 = i_387_ * -1525290801;
	anInt7435 = 795574925 * i_388_;
	anInt7470 = i_389_ * -1843163739;
	anInt7461 = -564511505 * i_390_;
	aClass27_7466 = new Class27();
	aClass552_7427 = new Class552(this);
	aClass568ArrayArrayArray7433
	    = (new Class568[i_387_][1183912005 * anInt7435]
	       [anInt7470 * 60330541]);
	aClass151Array7434 = new Class151[i_387_];
	if (bool) {
	    anIntArrayArray7437
		= new int[anInt7435 * 1183912005][anInt7470 * 60330541];
	    aByteArrayArray7480
		= new byte[1183912005 * anInt7435][60330541 * anInt7470];
	    aShortArrayArray7438
		= new short[anInt7435 * 1183912005][anInt7470 * 60330541];
	    aByteArrayArray7474
		= new byte[1183912005 * anInt7435][anInt7470 * 60330541];
	    aByteArrayArray7441
		= new byte[anInt7435 * 1183912005][anInt7470 * 60330541];
	    aByteArrayArray7423
		= new byte[anInt7435 * 1183912005][anInt7470 * 60330541];
	    aClass568ArrayArrayArray7460
		= (new Class568[1][1183912005 * anInt7435]
		   [anInt7470 * 60330541]);
	    aClass151Array7436 = new Class151[1];
	}
	if (bool_391_) {
	    aLongArrayArrayArray7473 = new long[i_387_][i_388_][i_389_];
	    aClass561Array7439 = new Class561[65165];
	    aBoolArray7475 = new boolean[65165];
	    anInt7472 = 0;
	}
	method9228(false, (byte) -67);
	aClass654_Sub1Array7447 = new Class654_Sub1[2];
	aClass654_Sub1Array7448 = new Class654_Sub1[2];
	aClass654_Sub1Array7449 = new Class654_Sub1[2];
	aClass654_Sub1Array7424 = new Class654_Sub1[10083];
	anInt7450 = 0;
	aClass654_Sub1Array7452 = new Class654_Sub1[5098];
	anInt7451 = 0;
	aClass654_Sub1_Sub5Array7454 = new Class654_Sub1_Sub5[5078];
	anInt7465 = 0;
	aBoolArrayArray7481
	    = (new boolean
	       [anInt7461 * -1213435377 + anInt7461 * -1213435377 + 1]
	       [anInt7461 * -1213435377 + anInt7461 * -1213435377 + 1]);
	aBoolArrayArray7479
	    = (new boolean
	       [anInt7461 * -1213435377 + anInt7461 * -1213435377 + 2]
	       [anInt7461 * -1213435377 + anInt7461 * -1213435377 + 2]);
	anIntArray7442
	    = new int[anInt7461 * -1213435377 + anInt7461 * -1213435377 + 2];
	aClass557_7478 = new Class557(false);
    }
    
    public void method9322(int i, int i_392_, int i_393_, int i_394_,
			   Class654_Sub1_Sub4 class654_sub1_sub4) {
	Class568 class568 = method9256(i, i_392_, i_393_, 768451766);
	if (null != class568) {
	    class654_sub1_sub4.method10809
		(new Class438((float) (-1978691487 * anInt7421
				       + (i_392_ << 941710601 * anInt7422)),
			      (float) i_394_,
			      (float) ((i_393_ << anInt7422 * 941710601)
				       + -1978691487 * anInt7421)));
	    class568.aClass654_Sub1_Sub4_7602 = class654_sub1_sub4;
	    int i_395_ = aClass151Array7432 == aClass151Array7436 ? 1 : 0;
	    if (class654_sub1_sub4.method16849(213912810)) {
		if (class654_sub1_sub4.method16850(-1920390363)) {
		    class654_sub1_sub4.aClass654_Sub1_10850
			= aClass654_Sub1Array7448[i_395_];
		    aClass654_Sub1Array7448[i_395_] = class654_sub1_sub4;
		} else {
		    class654_sub1_sub4.aClass654_Sub1_10850
			= aClass654_Sub1Array7447[i_395_];
		    aClass654_Sub1Array7447[i_395_] = class654_sub1_sub4;
		}
	    } else {
		class654_sub1_sub4.aClass654_Sub1_10850
		    = aClass654_Sub1Array7449[i_395_];
		aClass654_Sub1Array7449[i_395_] = class654_sub1_sub4;
	    }
	}
    }
    
    public void method9323(int i, int i_396_, int i_397_, int i_398_,
			   Class654_Sub1_Sub4 class654_sub1_sub4) {
	Class568 class568 = method9256(i, i_396_, i_397_, 768451766);
	if (null != class568) {
	    class654_sub1_sub4.method10809
		(new Class438((float) (-1978691487 * anInt7421
				       + (i_396_ << 941710601 * anInt7422)),
			      (float) i_398_,
			      (float) ((i_397_ << anInt7422 * 941710601)
				       + -1978691487 * anInt7421)));
	    class568.aClass654_Sub1_Sub4_7602 = class654_sub1_sub4;
	    int i_399_ = aClass151Array7432 == aClass151Array7436 ? 1 : 0;
	    if (class654_sub1_sub4.method16849(627896813)) {
		if (class654_sub1_sub4.method16850(-1920390363)) {
		    class654_sub1_sub4.aClass654_Sub1_10850
			= aClass654_Sub1Array7448[i_399_];
		    aClass654_Sub1Array7448[i_399_] = class654_sub1_sub4;
		} else {
		    class654_sub1_sub4.aClass654_Sub1_10850
			= aClass654_Sub1Array7447[i_399_];
		    aClass654_Sub1Array7447[i_399_] = class654_sub1_sub4;
		}
	    } else {
		class654_sub1_sub4.aClass654_Sub1_10850
		    = aClass654_Sub1Array7449[i_399_];
		aClass654_Sub1Array7449[i_399_] = class654_sub1_sub4;
	    }
	}
    }
    
    public void method9324(int i, int i_400_, int i_401_,
			   Class654_Sub1_Sub1 class654_sub1_sub1,
			   Class654_Sub1_Sub1 class654_sub1_sub1_402_) {
	Class568 class568 = method9256(i, i_400_, i_401_, 768451766);
	if (class568 != null) {
	    class568.aClass654_Sub1_Sub1_7598 = class654_sub1_sub1;
	    class568.aClass654_Sub1_Sub1_7597 = class654_sub1_sub1_402_;
	    int i_403_ = aClass151Array7432 == aClass151Array7436 ? 1 : 0;
	    if (class654_sub1_sub1.method16849(426620030)) {
		if (class654_sub1_sub1.method16850(-1920390363)) {
		    class654_sub1_sub1.aClass654_Sub1_10850
			= aClass654_Sub1Array7448[i_403_];
		    aClass654_Sub1Array7448[i_403_] = class654_sub1_sub1;
		} else {
		    class654_sub1_sub1.aClass654_Sub1_10850
			= aClass654_Sub1Array7447[i_403_];
		    aClass654_Sub1Array7447[i_403_] = class654_sub1_sub1;
		}
	    } else {
		class654_sub1_sub1.aClass654_Sub1_10850
		    = aClass654_Sub1Array7449[i_403_];
		aClass654_Sub1Array7449[i_403_] = class654_sub1_sub1;
	    }
	    if (class654_sub1_sub1_402_ != null) {
		if (class654_sub1_sub1_402_.method16849(1615190815)) {
		    if (class654_sub1_sub1_402_.method16850(-1920390363)) {
			class654_sub1_sub1_402_.aClass654_Sub1_10850
			    = aClass654_Sub1Array7448[i_403_];
			aClass654_Sub1Array7448[i_403_]
			    = class654_sub1_sub1_402_;
		    } else {
			class654_sub1_sub1_402_.aClass654_Sub1_10850
			    = aClass654_Sub1Array7447[i_403_];
			aClass654_Sub1Array7447[i_403_]
			    = class654_sub1_sub1_402_;
		    }
		} else {
		    class654_sub1_sub1_402_.aClass654_Sub1_10850
			= aClass654_Sub1Array7449[i_403_];
		    aClass654_Sub1Array7449[i_403_] = class654_sub1_sub1_402_;
		}
	    }
	}
    }
    
    void method9325(Class654_Sub1 class654_sub1, int i, int i_404_, int i_405_,
		    int i_406_) {
	if (i_404_ < anInt7435 * 1183912005) {
	    Class568 class568
		= aClass568ArrayArrayArray7431[i][1 + i_404_][i_405_];
	    if (class568 != null && null != class568.aClass654_Sub1_Sub2_7607
		&& class568.aClass654_Sub1_Sub2_7607.method16848((byte) 54)) {
		int i_407_
		    = ((aClass151Array7432[i].method2491(i_404_ + 1, i_405_,
							 -1009143955)
			+ aClass151Array7432[i].method2491(1 + i_404_ + 1,
							   i_405_, 1938606878)
			+ aClass151Array7432[i]
			      .method2491(1 + i_404_, i_405_ + 1, -249798907)
			+ aClass151Array7432[i].method2491(1 + (i_404_ + 1),
							   i_405_ + 1,
							   2009798981)) / 4
		       - (aClass151Array7432[i].method2491(i_404_, i_405_,
							   -1244496269)
			  + aClass151Array7432[i]
				.method2491(i_404_ + 1, i_405_, -265781467)
			  + aClass151Array7432[i]
				.method2491(i_404_, i_405_ + 1, 378631483)
			  + aClass151Array7432[i].method2491(i_404_ + 1,
							     i_405_ + 1,
							     -694832651)) / 4);
		class654_sub1.method16851(aClass185_7453,
					  class568.aClass654_Sub1_Sub2_7607,
					  -1935734157 * anInt7420, i_407_, 0,
					  true, 308999563);
	    }
	}
	if (i_405_ < anInt7435 * 1183912005) {
	    Class568 class568
		= aClass568ArrayArrayArray7431[i][i_404_][1 + i_405_];
	    if (null != class568 && null != class568.aClass654_Sub1_Sub2_7607
		&& class568.aClass654_Sub1_Sub2_7607.method16848((byte) 25)) {
		int i_408_
		    = ((aClass151Array7432[i].method2491(i_404_, i_405_,
							 2096478972)
			+ aClass151Array7432[i]
			      .method2491(i_404_ + 1, 1 + i_405_, -1486237303)
			+ aClass151Array7432[i]
			      .method2491(i_404_, i_405_ + 1 + 1, 765961141)
			+ aClass151Array7432[i].method2491(i_404_ + 1,
							   1 + (1 + i_405_),
							   -682273503)) / 4
		       - (aClass151Array7432[i].method2491(i_404_, i_405_,
							   876519706)
			  + aClass151Array7432[i].method2491(i_404_ + 1,
							     i_405_, 841282877)
			  + aClass151Array7432[i]
				.method2491(i_404_, 1 + i_405_, -866826010)
			  + aClass151Array7432[i].method2491(i_404_ + 1,
							     1 + i_405_,
							     1513579109)) / 4);
		class654_sub1.method16851(aClass185_7453,
					  class568.aClass654_Sub1_Sub2_7607, 0,
					  i_408_, -1935734157 * anInt7420,
					  true, 308999563);
	    }
	}
	if (i_404_ < anInt7435 * 1183912005 && i_405_ < 60330541 * anInt7470) {
	    Class568 class568
		= aClass568ArrayArrayArray7431[i][1 + i_404_][i_405_ + 1];
	    if (class568 != null && class568.aClass654_Sub1_Sub2_7607 != null
		&& class568.aClass654_Sub1_Sub2_7607.method16848((byte) 116)) {
		int i_409_
		    = ((aClass151Array7432[i].method2491(i_404_ + 1,
							 i_405_ + 1, 267743273)
			+ aClass151Array7432[i].method2491(1 + (1 + i_404_),
							   1 + i_405_,
							   166041858)
			+ aClass151Array7432[i].method2491(i_404_ + 1,
							   1 + (1 + i_405_),
							   -1935103081)
			+ aClass151Array7432[i].method2491(1 + (i_404_ + 1),
							   i_405_ + 1 + 1,
							   977969706)) / 4
		       - (aClass151Array7432[i].method2491(i_404_, i_405_,
							   -2031188775)
			  + aClass151Array7432[i]
				.method2491(i_404_ + 1, i_405_, -1428909715)
			  + aClass151Array7432[i]
				.method2491(i_404_, i_405_ + 1, -1398885155)
			  + aClass151Array7432[i].method2491(i_404_ + 1,
							     1 + i_405_,
							     -2781269)) / 4);
		class654_sub1.method16851(aClass185_7453,
					  class568.aClass654_Sub1_Sub2_7607,
					  -1935734157 * anInt7420, i_409_,
					  anInt7420 * -1935734157, true,
					  308999563);
	    }
	}
	if (i_404_ < anInt7435 * 1183912005 && i_405_ > 0) {
	    Class568 class568
		= aClass568ArrayArrayArray7431[i][i_404_ + 1][i_405_ - 1];
	    if (class568 != null && class568.aClass654_Sub1_Sub2_7607 != null
		&& class568.aClass654_Sub1_Sub2_7607.method16848((byte) 110)) {
		int i_410_
		    = ((aClass151Array7432[i]
			    .method2491(i_404_ + 1, i_405_ - 1, -1764576472)
			+ aClass151Array7432[i].method2491(i_404_ + 1 + 1,
							   i_405_ - 1,
							   686342879)
			+ aClass151Array7432[i].method2491(1 + i_404_,
							   i_405_ + 1 - 1,
							   -396957151)
			+ aClass151Array7432[i].method2491(1 + (1 + i_404_),
							   i_405_ + 1 - 1,
							   661381874)) / 4
		       - ((aClass151Array7432[i].method2491(i_404_, i_405_,
							    -2091327387)
			   + aClass151Array7432[i]
				 .method2491(1 + i_404_, i_405_, -1635038326)
			   + aClass151Array7432[i]
				 .method2491(i_404_, 1 + i_405_, 84715287)
			   + aClass151Array7432[i].method2491(i_404_ + 1,
							      i_405_ + 1,
							      -1637131558))
			  / 4));
		class654_sub1.method16851(aClass185_7453,
					  class568.aClass654_Sub1_Sub2_7607,
					  -1935734157 * anInt7420, i_410_,
					  -(anInt7420 * -1935734157), true,
					  308999563);
	    }
	}
    }
    
    public void method9326(int i, int i_411_, int i_412_,
			   Class654_Sub1_Sub3 class654_sub1_sub3,
			   Class654_Sub1_Sub3 class654_sub1_sub3_413_) {
	Class568 class568 = method9256(i, i_411_, i_412_, 768451766);
	if (null != class568) {
	    class568.aClass654_Sub1_Sub3_7600 = class654_sub1_sub3;
	    class568.aClass654_Sub1_Sub3_7601 = class654_sub1_sub3_413_;
	    int i_414_ = aClass151Array7436 == aClass151Array7432 ? 1 : 0;
	    if (class654_sub1_sub3.method16849(585141837)) {
		if (class654_sub1_sub3.method16850(-1920390363)) {
		    class654_sub1_sub3.aClass654_Sub1_10850
			= aClass654_Sub1Array7448[i_414_];
		    aClass654_Sub1Array7448[i_414_] = class654_sub1_sub3;
		} else {
		    class654_sub1_sub3.aClass654_Sub1_10850
			= aClass654_Sub1Array7447[i_414_];
		    aClass654_Sub1Array7447[i_414_] = class654_sub1_sub3;
		}
	    } else {
		class654_sub1_sub3.aClass654_Sub1_10850
		    = aClass654_Sub1Array7449[i_414_];
		aClass654_Sub1Array7449[i_414_] = class654_sub1_sub3;
	    }
	    if (null != class654_sub1_sub3_413_) {
		if (class654_sub1_sub3_413_.method16849(-621681793)) {
		    if (class654_sub1_sub3_413_.method16850(-1920390363)) {
			class654_sub1_sub3_413_.aClass654_Sub1_10850
			    = aClass654_Sub1Array7448[i_414_];
			aClass654_Sub1Array7448[i_414_]
			    = class654_sub1_sub3_413_;
		    } else {
			class654_sub1_sub3_413_.aClass654_Sub1_10850
			    = aClass654_Sub1Array7447[i_414_];
			aClass654_Sub1Array7447[i_414_]
			    = class654_sub1_sub3_413_;
		    }
		} else {
		    class654_sub1_sub3_413_.aClass654_Sub1_10850
			= aClass654_Sub1Array7449[i_414_];
		    aClass654_Sub1Array7449[i_414_] = class654_sub1_sub3_413_;
		}
	    }
	}
    }
    
    public boolean method9327(Class654_Sub1_Sub5 class654_sub1_sub5,
			      boolean bool) {
	boolean bool_415_ = aClass151Array7436 == aClass151Array7432;
	int i = 0;
	short i_416_ = 0;
	byte i_417_ = 0;
	class654_sub1_sub5.method18487(-1833807797);
	short i_418_ = 0;
	int i_419_ = Math.min(1183912005 * anInt7435 - 1,
			      Math.max(0, class654_sub1_sub5.aShort11900));
	int i_420_ = Math.min(1183912005 * anInt7435 - 1,
			      Math.max(0, class654_sub1_sub5.aShort11896));
	int i_421_ = Math.min(anInt7470 * 60330541 - 1,
			      Math.max(0, class654_sub1_sub5.aShort11901));
	int i_422_ = Math.min(anInt7470 * 60330541 - 1,
			      Math.max(0, class654_sub1_sub5.aShort11898));
	for (int i_423_ = i_419_; i_423_ <= i_420_; i_423_++) {
	    for (int i_424_ = i_421_; i_424_ <= i_422_; i_424_++) {
		Class568 class568 = method9232(class654_sub1_sub5.aByte10854,
					       i_423_, i_424_, (byte) 0);
		if (null != class568) {
		    Class559 class559
			= Class197.method3822(class654_sub1_sub5, 1244811404);
		    Class559 class559_425_ = class568.aClass559_7604;
		    if (class559_425_ == null)
			class568.aClass559_7604 = class559;
		    else {
			for (/**/; null != class559_425_.aClass559_7497;
			     class559_425_ = class559_425_.aClass559_7497) {
			    /* empty */
			}
			class559_425_.aClass559_7497 = class559;
		    }
		    if (bool_415_ && (anIntArrayArray7437[i_423_][i_424_]
				      & ~0xffffff) != 0) {
			i = anIntArrayArray7437[i_423_][i_424_];
			i_416_ = aShortArrayArray7438[i_423_][i_424_];
			i_417_ = aByteArrayArray7480[i_423_][i_424_];
		    }
		    if (!bool && class568.aClass654_Sub1_Sub2_7607 != null
			&& (class568.aClass654_Sub1_Sub2_7607.aShort11864
			    > i_418_))
			i_418_ = class568.aClass654_Sub1_Sub2_7607.aShort11864;
		}
	    }
	}
	if (bool_415_ && 0 != (i & ~0xffffff)) {
	    for (int i_426_ = i_419_; i_426_ <= i_420_; i_426_++) {
		for (int i_427_ = i_421_; i_427_ <= i_422_; i_427_++) {
		    if ((anIntArrayArray7437[i_426_][i_427_] & ~0xffffff)
			== 0) {
			anIntArrayArray7437[i_426_][i_427_] = i;
			aShortArrayArray7438[i_426_][i_427_] = i_416_;
			aByteArrayArray7480[i_426_][i_427_] = i_417_;
		    }
		}
	    }
	}
	if (bool) {
	    aClass654_Sub1_Sub5Array7454
		[(anInt7465 += -2053860005) * -67005741 - 1]
		= class654_sub1_sub5;
	    class654_sub1_sub5.aClass556_10855 = this;
	} else {
	    int i_428_ = aClass151Array7432 == aClass151Array7436 ? 1 : 0;
	    if (class654_sub1_sub5.method16849(-676876585)) {
		if (class654_sub1_sub5.method16850(-1920390363)) {
		    class654_sub1_sub5.aClass654_Sub1_10850
			= aClass654_Sub1Array7448[i_428_];
		    aClass654_Sub1Array7448[i_428_] = class654_sub1_sub5;
		} else {
		    class654_sub1_sub5.aClass654_Sub1_10850
			= aClass654_Sub1Array7447[i_428_];
		    aClass654_Sub1Array7447[i_428_] = class654_sub1_sub5;
		}
	    } else {
		class654_sub1_sub5.aClass654_Sub1_10850
		    = aClass654_Sub1Array7449[i_428_];
		aClass654_Sub1Array7449[i_428_] = class654_sub1_sub5;
	    }
	}
	if (bool) {
	    Class438 class438
		= Class438.method6994(class654_sub1_sub5.method10807()
				      .aClass438_4885);
	    class438.aFloat4863 -= (float) i_418_;
	    class654_sub1_sub5.method10809(class438);
	    class438.method7074();
	}
	return true;
    }
    
    boolean method9328(Class654_Sub1 class654_sub1, boolean bool,
		       byte[][][] is, int i, byte i_429_) {
	if (!aBool7476)
	    return false;
	if (class654_sub1 instanceof Class654_Sub1_Sub5) {
	    int i_430_ = ((Class654_Sub1_Sub5) class654_sub1).aShort11896;
	    int i_431_ = ((Class654_Sub1_Sub5) class654_sub1).aShort11898;
	    int i_432_ = ((Class654_Sub1_Sub5) class654_sub1).aShort11900;
	    int i_433_ = ((Class654_Sub1_Sub5) class654_sub1).aShort11901;
	    for (int i_434_ = i_432_; i_434_ <= i_430_; i_434_++) {
		for (int i_435_ = i_433_; i_435_ <= i_431_; i_435_++) {
		    if (class654_sub1.aByte10853 < anInt7418 * -1381097937
			&& i_434_ >= -474621401 * anInt7419
			&& i_434_ < anInt7456 * 2106025629
			&& i_435_ >= anInt7430 * 1375305047
			&& i_435_ < anInt7467 * -860025685) {
			if ((is != null && class654_sub1.aByte10854 >= i
			     && i_429_ == (is[class654_sub1.aByte10854][i_434_]
					   [i_435_]))
			    || !class654_sub1.method16858((byte) -53)
			    || class654_sub1.method16857(aClass185_7453,
							 -2042236519)) {
			    if (!bool && i_434_ >= -380604831 * anInt7455 - 16
				&& i_434_ <= -380604831 * anInt7455 + 16
				&& i_435_ >= anInt7459 * -1709472547 - 16
				&& i_435_ <= 16 + anInt7459 * -1709472547)
				class654_sub1.method16868(aClass185_7453,
							  -993952189);
			    return true;
			}
			return false;
		    }
		}
	    }
	    return true;
	}
	Class438 class438 = class654_sub1.method10807().aClass438_4885;
	int i_436_ = (int) class438.aFloat4864 >> 941710601 * anInt7422;
	int i_437_ = (int) class438.aFloat4865 >> 941710601 * anInt7422;
	if (class654_sub1.aByte10853 < anInt7418 * -1381097937
	    && i_436_ >= anInt7419 * -474621401
	    && i_436_ < anInt7456 * 2106025629
	    && i_437_ >= 1375305047 * anInt7430
	    && i_437_ < -860025685 * anInt7467) {
	    if ((is != null && class654_sub1.aByte10854 >= i
		 && i_429_ == is[class654_sub1.aByte10854][i_436_][i_437_])
		|| !class654_sub1.method16858((byte) -113)
		|| class654_sub1.method16857(aClass185_7453, -1949503846)) {
		if (!bool && i_436_ >= -380604831 * anInt7455 - 16
		    && i_436_ <= -380604831 * anInt7455 + 16
		    && i_437_ >= anInt7459 * -1709472547 - 16
		    && i_437_ <= anInt7459 * -1709472547 + 16)
		    class654_sub1.method16868(aClass185_7453, -993952189);
		return true;
	    }
	    return false;
	}
	return true;
    }
    
    public Class654_Sub1_Sub4 method9329(int i, int i_438_, int i_439_,
					 int i_440_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_438_][i_439_];
	if (class568 == null)
	    return null;
	return class568.aClass654_Sub1_Sub4_7602;
    }
    
    public void method9330() {
	for (int i = 0; i < anInt7465 * -67005741; i++) {
	    Class654_Sub1_Sub5 class654_sub1_sub5
		= aClass654_Sub1_Sub5Array7454[i];
	    method9337(class654_sub1_sub5, true, -2104672463);
	    aClass654_Sub1_Sub5Array7454[i] = null;
	}
	anInt7465 = 0;
    }
    
    public int method9331(int i, int i_441_, int i_442_) {
	return (null != aByteArrayArray7480
		? aByteArrayArray7480[i][i_441_] & 0xff : 0);
    }
    
    public void method9332() {
	for (int i = 0; i < anInt7465 * -67005741; i++) {
	    Class654_Sub1_Sub5 class654_sub1_sub5
		= aClass654_Sub1_Sub5Array7454[i];
	    method9337(class654_sub1_sub5, true, -2067874158);
	    aClass654_Sub1_Sub5Array7454[i] = null;
	}
	anInt7465 = 0;
    }
    
    public int method9333(int i, int i_443_) {
	return (aByteArrayArray7441 != null
		? aByteArrayArray7441[i][i_443_] & 0xff : 0);
    }
    
    public Class654_Sub1_Sub1 method9334(int i, int i_444_, int i_445_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_444_][i_445_];
	if (class568 != null) {
	    method9257(class568.aClass654_Sub1_Sub1_7598, -1916752553);
	    if (null != class568.aClass654_Sub1_Sub1_7598) {
		Class654_Sub1_Sub1 class654_sub1_sub1
		    = class568.aClass654_Sub1_Sub1_7598;
		class568.aClass654_Sub1_Sub1_7598 = null;
		return class654_sub1_sub1;
	    }
	}
	return null;
    }
    
    public Class654_Sub1_Sub1 method9335(int i, int i_446_, int i_447_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_446_][i_447_];
	if (class568 != null) {
	    method9257(class568.aClass654_Sub1_Sub1_7598, -1648050029);
	    if (null != class568.aClass654_Sub1_Sub1_7598) {
		Class654_Sub1_Sub1 class654_sub1_sub1
		    = class568.aClass654_Sub1_Sub1_7598;
		class568.aClass654_Sub1_Sub1_7598 = null;
		return class654_sub1_sub1;
	    }
	}
	return null;
    }
    
    public Class654_Sub1_Sub1 method9336(int i, int i_448_, int i_449_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_448_][i_449_];
	if (class568 != null) {
	    method9257(class568.aClass654_Sub1_Sub1_7598, -973399829);
	    if (null != class568.aClass654_Sub1_Sub1_7598) {
		Class654_Sub1_Sub1 class654_sub1_sub1
		    = class568.aClass654_Sub1_Sub1_7598;
		class568.aClass654_Sub1_Sub1_7598 = null;
		return class654_sub1_sub1;
	    }
	}
	return null;
    }
    
    void method9337(Class654_Sub1_Sub5 class654_sub1_sub5, boolean bool,
		    int i) {
	int i_450_ = Math.min(1183912005 * anInt7435 - 1,
			      Math.max(0, class654_sub1_sub5.aShort11900));
	int i_451_ = Math.min(anInt7435 * 1183912005 - 1,
			      Math.max(0, class654_sub1_sub5.aShort11896));
	int i_452_ = Math.min(60330541 * anInt7470 - 1,
			      Math.max(0, class654_sub1_sub5.aShort11901));
	int i_453_ = Math.min(anInt7470 * 60330541 - 1,
			      Math.max(0, class654_sub1_sub5.aShort11898));
	for (int i_454_ = i_450_; i_454_ <= i_451_; i_454_++) {
	    for (int i_455_ = i_452_; i_455_ <= i_453_; i_455_++) {
		Class568 class568
		    = (aClass568ArrayArrayArray7431
		       [class654_sub1_sub5.aByte10854][i_454_][i_455_]);
		if (class568 != null) {
		    Class559 class559 = class568.aClass559_7604;
		    Class559 class559_456_ = null;
		    for (/**/; null != class559;
			 class559 = class559.aClass559_7497) {
			if (class559.aClass654_Sub1_Sub5_7500
			    == class654_sub1_sub5) {
			    if (class559_456_ != null)
				class559_456_.aClass559_7497
				    = class559.aClass559_7497;
			    else
				class568.aClass559_7604
				    = class559.aClass559_7497;
			    class559.method9418(-1724878778);
			    break;
			}
			class559_456_ = class559;
		    }
		}
	    }
	}
	if (!bool)
	    method9257(class654_sub1_sub5, -1259423686);
    }
    
    public Class654_Sub1_Sub3 method9338(int i, int i_457_, int i_458_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_457_][i_458_];
	if (null != class568) {
	    method9257(class568.aClass654_Sub1_Sub3_7601, -828787830);
	    if (class568.aClass654_Sub1_Sub3_7601 != null) {
		Class654_Sub1_Sub3 class654_sub1_sub3
		    = class568.aClass654_Sub1_Sub3_7601;
		class568.aClass654_Sub1_Sub3_7601 = null;
		return class654_sub1_sub3;
	    }
	}
	return null;
    }
    
    void method9339(Class654_Sub1 class654_sub1) {
	Class438 class438 = class654_sub1.method10807().aClass438_4885;
	aClass185_7453.method3367((float) (int) class438.aFloat4864,
				  (float) ((int) class438.aFloat4863
					   + (class654_sub1
						  .method16876(-1231893078)
					      >> 1)),
				  (float) (int) class438.aFloat4865,
				  aFloatArray7468);
	class654_sub1.anInt10851 = 1350765687 * (int) aFloatArray7468[2];
    }
    
    public Class654_Sub1_Sub2 method9340(int i, int i_459_, int i_460_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_459_][i_460_];
	if (class568 == null)
	    return null;
	method9257(class568.aClass654_Sub1_Sub2_7607, -1360819413);
	if (null != class568.aClass654_Sub1_Sub2_7607) {
	    Class654_Sub1_Sub2 class654_sub1_sub2
		= class568.aClass654_Sub1_Sub2_7607;
	    class568.aClass654_Sub1_Sub2_7607 = null;
	    return class654_sub1_sub2;
	}
	return null;
    }
    
    public Class654_Sub1_Sub2 method9341(int i, int i_461_, int i_462_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_461_][i_462_];
	if (class568 == null)
	    return null;
	method9257(class568.aClass654_Sub1_Sub2_7607, -689327242);
	if (null != class568.aClass654_Sub1_Sub2_7607) {
	    Class654_Sub1_Sub2 class654_sub1_sub2
		= class568.aClass654_Sub1_Sub2_7607;
	    class568.aClass654_Sub1_Sub2_7607 = null;
	    return class654_sub1_sub2;
	}
	return null;
    }
    
    public void method9342(int i, int i_463_) {
	/* empty */
    }
    
    public Class654_Sub1_Sub1 method9343(int i, int i_464_, int i_465_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_464_][i_465_];
	if (null != class568) {
	    method9257(class568.aClass654_Sub1_Sub1_7597, -1868112107);
	    if (null != class568.aClass654_Sub1_Sub1_7597) {
		Class654_Sub1_Sub1 class654_sub1_sub1
		    = class568.aClass654_Sub1_Sub1_7597;
		class568.aClass654_Sub1_Sub1_7597 = null;
		return class654_sub1_sub1;
	    }
	}
	return null;
    }
    
    public void method9344(int i, int i_466_, int i_467_) {
	boolean bool = (null != aClass568ArrayArrayArray7431[0][i_466_][i_467_]
			&& (aClass568ArrayArrayArray7431[0][i_466_][i_467_]
			    .aClass568_7605) != null);
	for (int i_468_ = i; i_468_ >= 0; i_468_--) {
	    if (aClass568ArrayArrayArray7431[i_468_][i_466_][i_467_] == null) {
		Class568 class568
		    = (aClass568ArrayArrayArray7431[i_468_][i_466_][i_467_]
		       = new Class568(i_468_));
		if (bool)
		    class568.aByte7599++;
	    }
	}
    }
    
    void method9345(Class654_Sub1 class654_sub1) {
	if (class654_sub1 != null) {
	    class654_sub1.method10840();
	    for (int i = 0; i < 2; i++) {
		Class654_Sub1 class654_sub1_469_ = null;
		for (Class654_Sub1 class654_sub1_470_
			 = aClass654_Sub1Array7447[i];
		     null != class654_sub1_470_;
		     class654_sub1_470_
			 = class654_sub1_470_.aClass654_Sub1_10850) {
		    if (class654_sub1 == class654_sub1_470_) {
			if (null != class654_sub1_469_)
			    class654_sub1_469_.aClass654_Sub1_10850
				= class654_sub1_470_.aClass654_Sub1_10850;
			else
			    aClass654_Sub1Array7447[i]
				= class654_sub1_470_.aClass654_Sub1_10850;
			return;
		    }
		    class654_sub1_469_ = class654_sub1_470_;
		}
		class654_sub1_469_ = null;
		for (Class654_Sub1 class654_sub1_471_
			 = aClass654_Sub1Array7448[i];
		     null != class654_sub1_471_;
		     class654_sub1_471_
			 = class654_sub1_471_.aClass654_Sub1_10850) {
		    if (class654_sub1 == class654_sub1_471_) {
			if (null != class654_sub1_469_)
			    class654_sub1_469_.aClass654_Sub1_10850
				= class654_sub1_471_.aClass654_Sub1_10850;
			else
			    aClass654_Sub1Array7448[i]
				= class654_sub1_471_.aClass654_Sub1_10850;
			return;
		    }
		    class654_sub1_469_ = class654_sub1_471_;
		}
		class654_sub1_469_ = null;
		for (Class654_Sub1 class654_sub1_472_
			 = aClass654_Sub1Array7449[i];
		     class654_sub1_472_ != null;
		     class654_sub1_472_
			 = class654_sub1_472_.aClass654_Sub1_10850) {
		    if (class654_sub1_472_ == class654_sub1) {
			if (null != class654_sub1_469_)
			    class654_sub1_469_.aClass654_Sub1_10850
				= class654_sub1_472_.aClass654_Sub1_10850;
			else
			    aClass654_Sub1Array7449[i]
				= class654_sub1_472_.aClass654_Sub1_10850;
			return;
		    }
		    class654_sub1_469_ = class654_sub1_472_;
		}
	    }
	}
    }
    
    public void method9346() {
	for (int i = 0; i < 1183912005 * anInt7435; i++) {
	    for (int i_473_ = 0; i_473_ < anInt7470 * 60330541; i_473_++) {
		if (null == aClass568ArrayArrayArray7431[0][i][i_473_])
		    aClass568ArrayArrayArray7431[0][i][i_473_]
			= new Class568(0);
	    }
	}
    }
    
    public Class654_Sub1_Sub1 method9347(int i, int i_474_, int i_475_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_474_][i_475_];
	if (class568 == null)
	    return null;
	return class568.aClass654_Sub1_Sub1_7597;
    }
    
    void method9348(Class654_Sub1_Sub5 class654_sub1_sub5, boolean bool) {
	int i = Math.min(1183912005 * anInt7435 - 1,
			 Math.max(0, class654_sub1_sub5.aShort11900));
	int i_476_ = Math.min(anInt7435 * 1183912005 - 1,
			      Math.max(0, class654_sub1_sub5.aShort11896));
	int i_477_ = Math.min(60330541 * anInt7470 - 1,
			      Math.max(0, class654_sub1_sub5.aShort11901));
	int i_478_ = Math.min(anInt7470 * 60330541 - 1,
			      Math.max(0, class654_sub1_sub5.aShort11898));
	for (int i_479_ = i; i_479_ <= i_476_; i_479_++) {
	    for (int i_480_ = i_477_; i_480_ <= i_478_; i_480_++) {
		Class568 class568
		    = (aClass568ArrayArrayArray7431
		       [class654_sub1_sub5.aByte10854][i_479_][i_480_]);
		if (class568 != null) {
		    Class559 class559 = class568.aClass559_7604;
		    Class559 class559_481_ = null;
		    for (/**/; null != class559;
			 class559 = class559.aClass559_7497) {
			if (class559.aClass654_Sub1_Sub5_7500
			    == class654_sub1_sub5) {
			    if (class559_481_ != null)
				class559_481_.aClass559_7497
				    = class559.aClass559_7497;
			    else
				class568.aClass559_7604
				    = class559.aClass559_7497;
			    class559.method9418(-912262510);
			    break;
			}
			class559_481_ = class559;
		    }
		}
	    }
	}
	if (!bool)
	    method9257(class654_sub1_sub5, -1476763604);
    }
    
    public Class654_Sub1_Sub3 method9349(int i, int i_482_, int i_483_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_482_][i_483_];
	if (class568 == null)
	    return null;
	return class568.aClass654_Sub1_Sub3_7600;
    }
    
    public Class654_Sub1_Sub3 method9350(int i, int i_484_, int i_485_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_484_][i_485_];
	if (class568 == null)
	    return null;
	return class568.aClass654_Sub1_Sub3_7600;
    }
    
    public Class654_Sub1_Sub3 method9351(int i, int i_486_, int i_487_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_486_][i_487_];
	if (class568 == null)
	    return null;
	return class568.aClass654_Sub1_Sub3_7600;
    }
    
    public void method9352(int i, int i_488_, int i_489_, int i_490_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_488_][i_489_];
	if (null != class568) {
	    Class654_Sub1_Sub3 class654_sub1_sub3
		= class568.aClass654_Sub1_Sub3_7600;
	    Class654_Sub1_Sub3 class654_sub1_sub3_491_
		= class568.aClass654_Sub1_Sub3_7601;
	    if (class654_sub1_sub3 != null) {
		class654_sub1_sub3.aShort11869
		    = (short) (class654_sub1_sub3.aShort11869 * i_490_
			       / (16 << anInt7422 * 941710601 - 7));
		class654_sub1_sub3.aShort11867
		    = (short) (i_490_ * class654_sub1_sub3.aShort11867
			       / (16 << anInt7422 * 941710601 - 7));
	    }
	    if (class654_sub1_sub3_491_ != null) {
		class654_sub1_sub3_491_.aShort11869
		    = (short) (i_490_ * class654_sub1_sub3_491_.aShort11869
			       / (16 << anInt7422 * 941710601 - 7));
		class654_sub1_sub3_491_.aShort11867
		    = (short) (class654_sub1_sub3_491_.aShort11867 * i_490_
			       / (16 << anInt7422 * 941710601 - 7));
	    }
	}
    }
    
    public Class654_Sub1_Sub4 method9353(int i, int i_492_, int i_493_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_492_][i_493_];
	if (null == class568)
	    return null;
	Class654_Sub1_Sub4 class654_sub1_sub4
	    = class568.aClass654_Sub1_Sub4_7602;
	class568.aClass654_Sub1_Sub4_7602 = null;
	method9257(class654_sub1_sub4, -1306853659);
	return class654_sub1_sub4;
    }
    
    public Class654_Sub1_Sub5 method9354(int i, int i_494_, int i_495_,
					 Interface64 interface64) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_494_][i_495_];
	if (class568 == null)
	    return null;
	for (Class559 class559 = class568.aClass559_7604; class559 != null;
	     class559 = class559.aClass559_7497) {
	    Class654_Sub1_Sub5 class654_sub1_sub5
		= class559.aClass654_Sub1_Sub5_7500;
	    if ((null == interface64
		 || interface64.method432(class654_sub1_sub5, (byte) -12))
		&& i_494_ == class654_sub1_sub5.aShort11900
		&& i_495_ == class654_sub1_sub5.aShort11901)
		return class654_sub1_sub5;
	}
	return null;
    }
    
    public Class654_Sub1_Sub5 method9355(int i, int i_496_, int i_497_,
					 Interface64 interface64) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_496_][i_497_];
	if (class568 == null)
	    return null;
	for (Class559 class559 = class568.aClass559_7604; class559 != null;
	     class559 = class559.aClass559_7497) {
	    Class654_Sub1_Sub5 class654_sub1_sub5
		= class559.aClass654_Sub1_Sub5_7500;
	    if ((null == interface64
		 || interface64.method432(class654_sub1_sub5, (byte) -111))
		&& i_496_ == class654_sub1_sub5.aShort11900
		&& i_497_ == class654_sub1_sub5.aShort11901)
		return class654_sub1_sub5;
	}
	return null;
    }
    
    public Class654_Sub1_Sub2 method9356(int i, int i_498_, int i_499_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_498_][i_499_];
	if (class568 == null || null == class568.aClass654_Sub1_Sub2_7607)
	    return null;
	return class568.aClass654_Sub1_Sub2_7607;
    }
    
    public Class654_Sub1_Sub3 method9357(int i, int i_500_, int i_501_,
					 byte i_502_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_500_][i_501_];
	if (class568 == null)
	    return null;
	return class568.aClass654_Sub1_Sub3_7600;
    }
    
    void method9358(Class654_Sub1 class654_sub1, int i, int i_503_, int i_504_,
		    int i_505_, int i_506_) {
	boolean bool = true;
	int i_507_ = i_503_;
	int i_508_ = i_503_ + i_505_;
	int i_509_ = i_504_ - 1;
	int i_510_ = i_504_ + i_506_;
	for (int i_511_ = i; i_511_ <= i + 1; i_511_++) {
	    if (i_511_ != -1381097937 * anInt7418) {
		for (int i_512_ = i_507_; i_512_ <= i_508_; i_512_++) {
		    if (i_512_ >= 0 && i_512_ < anInt7435 * 1183912005) {
			for (int i_513_ = i_509_; i_513_ <= i_510_; i_513_++) {
			    if (i_513_ >= 0 && i_513_ < 60330541 * anInt7470
				&& (!bool || i_512_ >= i_508_
				    || i_513_ >= i_510_
				    || i_513_ < i_504_ && i_512_ != i_503_)) {
				Class568 class568
				    = (aClass568ArrayArrayArray7431[i_511_]
				       [i_512_][i_513_]);
				if (null != class568) {
				    int i_514_
					= (((aClass151Array7432[i_511_]
						 .method2491
					     (i_512_, i_513_, -1570057226))
					    + (aClass151Array7432[i_511_]
						   .method2491
					       (1 + i_512_, i_513_,
						-887838560))
					    + (aClass151Array7432[i_511_]
						   .method2491
					       (i_512_, 1 + i_513_,
						-1371225138))
					    + (aClass151Array7432[i_511_]
						   .method2491
					       (i_512_ + 1, i_513_ + 1,
						2094457818))) / 4
					   - ((aClass151Array7432[i].method2491
					       (i_503_, i_504_, 330855242))
					      + (aClass151Array7432[i]
						     .method2491
						 (1 + i_503_, i_504_,
						  553518868))
					      + (aClass151Array7432[i]
						     .method2491
						 (i_503_, i_504_ + 1,
						  1465067783))
					      + (aClass151Array7432[i]
						     .method2491
						 (1 + i_503_, 1 + i_504_,
						  -1561584041))) / 4);
				    Class654_Sub1_Sub1 class654_sub1_sub1
					= class568.aClass654_Sub1_Sub1_7598;
				    Class654_Sub1_Sub1 class654_sub1_sub1_515_
					= class568.aClass654_Sub1_Sub1_7597;
				    if (class654_sub1_sub1 != null
					&& class654_sub1_sub1
					       .method16848((byte) 112))
					class654_sub1.method16851
					    (aClass185_7453,
					     class654_sub1_sub1,
					     (((i_512_ - i_503_)
					       * (anInt7420 * -1935734157))
					      + (-1978691487 * anInt7421
						 * (1 - i_505_))),
					     i_514_,
					     ((-1978691487 * anInt7421
					       * (1 - i_506_))
					      + ((i_513_ - i_504_)
						 * (anInt7420 * -1935734157))),
					     bool, 308999563);
				    if (null != class654_sub1_sub1_515_
					&& class654_sub1_sub1_515_
					       .method16848((byte) 46))
					class654_sub1.method16851
					    (aClass185_7453,
					     class654_sub1_sub1_515_,
					     ((i_512_ - i_503_) * (-1935734157
								   * anInt7420)
					      + (-1978691487 * anInt7421
						 * (1 - i_505_))),
					     i_514_,
					     ((anInt7421 * -1978691487
					       * (1 - i_506_))
					      + ((i_513_ - i_504_)
						 * (-1935734157 * anInt7420))),
					     bool, 308999563);
				    for (Class559 class559
					     = class568.aClass559_7604;
					 class559 != null;
					 class559 = class559.aClass559_7497) {
					Class654_Sub1_Sub5 class654_sub1_sub5
					    = (class559
					       .aClass654_Sub1_Sub5_7500);
					if (null != class654_sub1_sub5
					    && class654_sub1_sub5
						   .method16848((byte) 20)
					    && ((class654_sub1_sub5.aShort11900
						 == i_512_)
						|| i_512_ == i_507_)
					    && ((class654_sub1_sub5.aShort11901
						 == i_513_)
						|| i_509_ == i_513_)) {
					    int i_516_ = ((class654_sub1_sub5
							   .aShort11896)
							  - (class654_sub1_sub5
							     .aShort11900)
							  + 1);
					    int i_517_ = ((class654_sub1_sub5
							   .aShort11898)
							  - (class654_sub1_sub5
							     .aShort11901)
							  + 1);
					    class654_sub1.method16851
						(aClass185_7453,
						 class654_sub1_sub5,
						 ((((class654_sub1_sub5
						     .aShort11900)
						    - i_503_)
						   * (anInt7420 * -1935734157))
						  + ((i_516_ - i_505_)
						     * (anInt7421
							* -1978691487))),
						 i_514_,
						 ((((class654_sub1_sub5
						     .aShort11901)
						    - i_504_)
						   * (-1935734157 * anInt7420))
						  + (anInt7421 * -1978691487
						     * (i_517_ - i_506_))),
						 bool, 308999563);
					}
				    }
				}
			    }
			}
		    }
		}
		i_507_--;
		bool = false;
	    }
	}
    }
    
    void method9359(Class654_Sub1 class654_sub1, int i, int i_518_, int i_519_,
		    int i_520_, int i_521_) {
	boolean bool = true;
	int i_522_ = i_518_;
	int i_523_ = i_518_ + i_520_;
	int i_524_ = i_519_ - 1;
	int i_525_ = i_519_ + i_521_;
	for (int i_526_ = i; i_526_ <= i + 1; i_526_++) {
	    if (i_526_ != -1381097937 * anInt7418) {
		for (int i_527_ = i_522_; i_527_ <= i_523_; i_527_++) {
		    if (i_527_ >= 0 && i_527_ < anInt7435 * 1183912005) {
			for (int i_528_ = i_524_; i_528_ <= i_525_; i_528_++) {
			    if (i_528_ >= 0 && i_528_ < 60330541 * anInt7470
				&& (!bool || i_527_ >= i_523_
				    || i_528_ >= i_525_
				    || i_528_ < i_519_ && i_527_ != i_518_)) {
				Class568 class568
				    = (aClass568ArrayArrayArray7431[i_526_]
				       [i_527_][i_528_]);
				if (null != class568) {
				    int i_529_
					= (((aClass151Array7432[i_526_]
						 .method2491
					     (i_527_, i_528_, 874653581))
					    + (aClass151Array7432[i_526_]
						   .method2491
					       (1 + i_527_, i_528_,
						1698221023))
					    + (aClass151Array7432[i_526_]
						   .method2491
					       (i_527_, 1 + i_528_,
						-102015199))
					    + (aClass151Array7432[i_526_]
						   .method2491
					       (i_527_ + 1, i_528_ + 1,
						199513710))) / 4
					   - ((aClass151Array7432[i].method2491
					       (i_518_, i_519_, 414018712))
					      + (aClass151Array7432[i]
						     .method2491
						 (1 + i_518_, i_519_,
						  -2117383012))
					      + (aClass151Array7432[i]
						     .method2491
						 (i_518_, i_519_ + 1,
						  1601090848))
					      + (aClass151Array7432[i]
						     .method2491
						 (1 + i_518_, 1 + i_519_,
						  -2136011481))) / 4);
				    Class654_Sub1_Sub1 class654_sub1_sub1
					= class568.aClass654_Sub1_Sub1_7598;
				    Class654_Sub1_Sub1 class654_sub1_sub1_530_
					= class568.aClass654_Sub1_Sub1_7597;
				    if (class654_sub1_sub1 != null
					&& class654_sub1_sub1
					       .method16848((byte) 112))
					class654_sub1.method16851
					    (aClass185_7453,
					     class654_sub1_sub1,
					     (((i_527_ - i_518_)
					       * (anInt7420 * -1935734157))
					      + (-1978691487 * anInt7421
						 * (1 - i_520_))),
					     i_529_,
					     ((-1978691487 * anInt7421
					       * (1 - i_521_))
					      + ((i_528_ - i_519_)
						 * (anInt7420 * -1935734157))),
					     bool, 308999563);
				    if (null != class654_sub1_sub1_530_
					&& class654_sub1_sub1_530_
					       .method16848((byte) 91))
					class654_sub1.method16851
					    (aClass185_7453,
					     class654_sub1_sub1_530_,
					     ((i_527_ - i_518_) * (-1935734157
								   * anInt7420)
					      + (-1978691487 * anInt7421
						 * (1 - i_520_))),
					     i_529_,
					     ((anInt7421 * -1978691487
					       * (1 - i_521_))
					      + ((i_528_ - i_519_)
						 * (-1935734157 * anInt7420))),
					     bool, 308999563);
				    for (Class559 class559
					     = class568.aClass559_7604;
					 class559 != null;
					 class559 = class559.aClass559_7497) {
					Class654_Sub1_Sub5 class654_sub1_sub5
					    = (class559
					       .aClass654_Sub1_Sub5_7500);
					if (null != class654_sub1_sub5
					    && class654_sub1_sub5
						   .method16848((byte) 84)
					    && ((class654_sub1_sub5.aShort11900
						 == i_527_)
						|| i_527_ == i_522_)
					    && ((class654_sub1_sub5.aShort11901
						 == i_528_)
						|| i_524_ == i_528_)) {
					    int i_531_ = ((class654_sub1_sub5
							   .aShort11896)
							  - (class654_sub1_sub5
							     .aShort11900)
							  + 1);
					    int i_532_ = ((class654_sub1_sub5
							   .aShort11898)
							  - (class654_sub1_sub5
							     .aShort11901)
							  + 1);
					    class654_sub1.method16851
						(aClass185_7453,
						 class654_sub1_sub5,
						 ((((class654_sub1_sub5
						     .aShort11900)
						    - i_518_)
						   * (anInt7420 * -1935734157))
						  + ((i_531_ - i_520_)
						     * (anInt7421
							* -1978691487))),
						 i_529_,
						 ((((class654_sub1_sub5
						     .aShort11901)
						    - i_519_)
						   * (-1935734157 * anInt7420))
						  + (anInt7421 * -1978691487
						     * (i_532_ - i_521_))),
						 bool, 308999563);
					}
				    }
				}
			    }
			}
		    }
		}
		i_522_--;
		bool = false;
	    }
	}
    }
    
    void method9360(Class654_Sub1 class654_sub1, int i, int i_533_, int i_534_,
		    int i_535_, int i_536_) {
	boolean bool = true;
	int i_537_ = i_533_;
	int i_538_ = i_533_ + i_535_;
	int i_539_ = i_534_ - 1;
	int i_540_ = i_534_ + i_536_;
	for (int i_541_ = i; i_541_ <= i + 1; i_541_++) {
	    if (i_541_ != -1381097937 * anInt7418) {
		for (int i_542_ = i_537_; i_542_ <= i_538_; i_542_++) {
		    if (i_542_ >= 0 && i_542_ < anInt7435 * 1183912005) {
			for (int i_543_ = i_539_; i_543_ <= i_540_; i_543_++) {
			    if (i_543_ >= 0 && i_543_ < 60330541 * anInt7470
				&& (!bool || i_542_ >= i_538_
				    || i_543_ >= i_540_
				    || i_543_ < i_534_ && i_542_ != i_533_)) {
				Class568 class568
				    = (aClass568ArrayArrayArray7431[i_541_]
				       [i_542_][i_543_]);
				if (null != class568) {
				    int i_544_
					= (((aClass151Array7432[i_541_]
						 .method2491
					     (i_542_, i_543_, 2064674583))
					    + (aClass151Array7432[i_541_]
						   .method2491
					       (1 + i_542_, i_543_,
						1646036029))
					    + (aClass151Array7432[i_541_]
						   .method2491
					       (i_542_, 1 + i_543_,
						-430479402))
					    + (aClass151Array7432[i_541_]
						   .method2491
					       (i_542_ + 1, i_543_ + 1,
						1488940503))) / 4
					   - ((aClass151Array7432[i].method2491
					       (i_533_, i_534_, -878094716))
					      + (aClass151Array7432[i]
						     .method2491
						 (1 + i_533_, i_534_,
						  -1169855234))
					      + (aClass151Array7432[i]
						     .method2491
						 (i_533_, i_534_ + 1,
						  807046116))
					      + (aClass151Array7432[i]
						     .method2491
						 (1 + i_533_, 1 + i_534_,
						  1591298085))) / 4);
				    Class654_Sub1_Sub1 class654_sub1_sub1
					= class568.aClass654_Sub1_Sub1_7598;
				    Class654_Sub1_Sub1 class654_sub1_sub1_545_
					= class568.aClass654_Sub1_Sub1_7597;
				    if (class654_sub1_sub1 != null
					&& class654_sub1_sub1
					       .method16848((byte) 73))
					class654_sub1.method16851
					    (aClass185_7453,
					     class654_sub1_sub1,
					     (((i_542_ - i_533_)
					       * (anInt7420 * -1935734157))
					      + (-1978691487 * anInt7421
						 * (1 - i_535_))),
					     i_544_,
					     ((-1978691487 * anInt7421
					       * (1 - i_536_))
					      + ((i_543_ - i_534_)
						 * (anInt7420 * -1935734157))),
					     bool, 308999563);
				    if (null != class654_sub1_sub1_545_
					&& class654_sub1_sub1_545_
					       .method16848((byte) 8))
					class654_sub1.method16851
					    (aClass185_7453,
					     class654_sub1_sub1_545_,
					     ((i_542_ - i_533_) * (-1935734157
								   * anInt7420)
					      + (-1978691487 * anInt7421
						 * (1 - i_535_))),
					     i_544_,
					     ((anInt7421 * -1978691487
					       * (1 - i_536_))
					      + ((i_543_ - i_534_)
						 * (-1935734157 * anInt7420))),
					     bool, 308999563);
				    for (Class559 class559
					     = class568.aClass559_7604;
					 class559 != null;
					 class559 = class559.aClass559_7497) {
					Class654_Sub1_Sub5 class654_sub1_sub5
					    = (class559
					       .aClass654_Sub1_Sub5_7500);
					if (null != class654_sub1_sub5
					    && class654_sub1_sub5
						   .method16848((byte) 26)
					    && ((class654_sub1_sub5.aShort11900
						 == i_542_)
						|| i_542_ == i_537_)
					    && ((class654_sub1_sub5.aShort11901
						 == i_543_)
						|| i_539_ == i_543_)) {
					    int i_546_ = ((class654_sub1_sub5
							   .aShort11896)
							  - (class654_sub1_sub5
							     .aShort11900)
							  + 1);
					    int i_547_ = ((class654_sub1_sub5
							   .aShort11898)
							  - (class654_sub1_sub5
							     .aShort11901)
							  + 1);
					    class654_sub1.method16851
						(aClass185_7453,
						 class654_sub1_sub5,
						 ((((class654_sub1_sub5
						     .aShort11900)
						    - i_533_)
						   * (anInt7420 * -1935734157))
						  + ((i_546_ - i_535_)
						     * (anInt7421
							* -1978691487))),
						 i_544_,
						 ((((class654_sub1_sub5
						     .aShort11901)
						    - i_534_)
						   * (-1935734157 * anInt7420))
						  + (anInt7421 * -1978691487
						     * (i_547_ - i_536_))),
						 bool, 308999563);
					}
				    }
				}
			    }
			}
		    }
		}
		i_537_--;
		bool = false;
	    }
	}
    }
    
    void method9361(Class654_Sub1 class654_sub1, int i, int i_548_, int i_549_,
		    int i_550_, int i_551_) {
	boolean bool = true;
	int i_552_ = i_548_;
	int i_553_ = i_548_ + i_550_;
	int i_554_ = i_549_ - 1;
	int i_555_ = i_549_ + i_551_;
	for (int i_556_ = i; i_556_ <= i + 1; i_556_++) {
	    if (i_556_ != -1381097937 * anInt7418) {
		for (int i_557_ = i_552_; i_557_ <= i_553_; i_557_++) {
		    if (i_557_ >= 0 && i_557_ < anInt7435 * 1183912005) {
			for (int i_558_ = i_554_; i_558_ <= i_555_; i_558_++) {
			    if (i_558_ >= 0 && i_558_ < 60330541 * anInt7470
				&& (!bool || i_557_ >= i_553_
				    || i_558_ >= i_555_
				    || i_558_ < i_549_ && i_557_ != i_548_)) {
				Class568 class568
				    = (aClass568ArrayArrayArray7431[i_556_]
				       [i_557_][i_558_]);
				if (null != class568) {
				    int i_559_
					= (((aClass151Array7432[i_556_]
						 .method2491
					     (i_557_, i_558_, -883634391))
					    + (aClass151Array7432[i_556_]
						   .method2491
					       (1 + i_557_, i_558_,
						1098095216))
					    + (aClass151Array7432[i_556_]
						   .method2491
					       (i_557_, 1 + i_558_,
						-1382205505))
					    + (aClass151Array7432[i_556_]
						   .method2491
					       (i_557_ + 1, i_558_ + 1,
						-1060943351))) / 4
					   - ((aClass151Array7432[i].method2491
					       (i_548_, i_549_, -1848912863))
					      + (aClass151Array7432[i]
						     .method2491
						 (1 + i_548_, i_549_,
						  -1203432593))
					      + (aClass151Array7432[i]
						     .method2491
						 (i_548_, i_549_ + 1,
						  -1855656536))
					      + (aClass151Array7432[i]
						     .method2491
						 (1 + i_548_, 1 + i_549_,
						  155613640))) / 4);
				    Class654_Sub1_Sub1 class654_sub1_sub1
					= class568.aClass654_Sub1_Sub1_7598;
				    Class654_Sub1_Sub1 class654_sub1_sub1_560_
					= class568.aClass654_Sub1_Sub1_7597;
				    if (class654_sub1_sub1 != null
					&& class654_sub1_sub1
					       .method16848((byte) 57))
					class654_sub1.method16851
					    (aClass185_7453,
					     class654_sub1_sub1,
					     (((i_557_ - i_548_)
					       * (anInt7420 * -1935734157))
					      + (-1978691487 * anInt7421
						 * (1 - i_550_))),
					     i_559_,
					     ((-1978691487 * anInt7421
					       * (1 - i_551_))
					      + ((i_558_ - i_549_)
						 * (anInt7420 * -1935734157))),
					     bool, 308999563);
				    if (null != class654_sub1_sub1_560_
					&& class654_sub1_sub1_560_
					       .method16848((byte) 86))
					class654_sub1.method16851
					    (aClass185_7453,
					     class654_sub1_sub1_560_,
					     ((i_557_ - i_548_) * (-1935734157
								   * anInt7420)
					      + (-1978691487 * anInt7421
						 * (1 - i_550_))),
					     i_559_,
					     ((anInt7421 * -1978691487
					       * (1 - i_551_))
					      + ((i_558_ - i_549_)
						 * (-1935734157 * anInt7420))),
					     bool, 308999563);
				    for (Class559 class559
					     = class568.aClass559_7604;
					 class559 != null;
					 class559 = class559.aClass559_7497) {
					Class654_Sub1_Sub5 class654_sub1_sub5
					    = (class559
					       .aClass654_Sub1_Sub5_7500);
					if (null != class654_sub1_sub5
					    && class654_sub1_sub5
						   .method16848((byte) 16)
					    && ((class654_sub1_sub5.aShort11900
						 == i_557_)
						|| i_557_ == i_552_)
					    && ((class654_sub1_sub5.aShort11901
						 == i_558_)
						|| i_554_ == i_558_)) {
					    int i_561_ = ((class654_sub1_sub5
							   .aShort11896)
							  - (class654_sub1_sub5
							     .aShort11900)
							  + 1);
					    int i_562_ = ((class654_sub1_sub5
							   .aShort11898)
							  - (class654_sub1_sub5
							     .aShort11901)
							  + 1);
					    class654_sub1.method16851
						(aClass185_7453,
						 class654_sub1_sub5,
						 ((((class654_sub1_sub5
						     .aShort11900)
						    - i_548_)
						   * (anInt7420 * -1935734157))
						  + ((i_561_ - i_550_)
						     * (anInt7421
							* -1978691487))),
						 i_559_,
						 ((((class654_sub1_sub5
						     .aShort11901)
						    - i_549_)
						   * (-1935734157 * anInt7420))
						  + (anInt7421 * -1978691487
						     * (i_562_ - i_551_))),
						 bool, 308999563);
					}
				    }
				}
			    }
			}
		    }
		}
		i_552_--;
		bool = false;
	    }
	}
    }
    
    public void method9362
	(int i, int i_563_, int i_564_, int i_565_, byte[][][] is,
	 int[] is_566_, int[] is_567_, int[] is_568_, int[] is_569_,
	 int[] is_570_, int i_571_, byte i_572_, int i_573_, int i_574_,
	 boolean bool, boolean bool_575_, int i_576_, boolean bool_577_) {
	aClass552_7427.aBool7310 = true;
	aBool7476 = bool_575_;
	anInt7455 = (i_563_ >> anInt7422 * 941710601) * 1113634209;
	anInt7459 = 23163765 * (i_565_ >> anInt7422 * 941710601);
	anInt7457 = i_563_ * -1749494531;
	anInt7458 = i_565_ * -1680182669;
	anInt7462 = 973969553 * i_564_;
	anInt7419 = -273223625 * anInt7455 - 562864601 * anInt7461;
	if (anInt7419 * -474621401 < 0) {
	    anInt7425 = -(-1556834553 * anInt7419);
	    anInt7419 = 0;
	} else
	    anInt7425 = 0;
	anInt7430 = -1674151189 * anInt7459 - anInt7461 * 425788937;
	if (anInt7430 * 1375305047 < 0) {
	    anInt7463 = -(anInt7430 * 1723447665);
	    anInt7430 = 0;
	} else
	    anInt7463 = 0;
	anInt7456 = anInt7455 * -1718617707 + anInt7461 * 1700611483;
	if (anInt7456 * 2106025629 > 1183912005 * anInt7435)
	    anInt7456 = anInt7435 * -444609591;
	anInt7467 = anInt7461 * 1281599533 + anInt7459 * -1334631273;
	if (-860025685 * anInt7467 > anInt7470 * 60330541)
	    anInt7467 = anInt7470 * -1909674873;
	boolean[][] bools = aBoolArrayArray7481;
	boolean[][] bools_578_ = aBoolArrayArray7479;
	if (aBool7476) {
	    for (int i_579_ = 0;
		 (i_579_
		  < 2 + (-1213435377 * anInt7461 + anInt7461 * -1213435377));
		 i_579_++) {
		int i_580_ = 0;
		int i_581_ = 0;
		for (int i_582_ = 0;
		     i_582_ < 2 + (-1213435377 * anInt7461
				   + anInt7461 * -1213435377);
		     i_582_++) {
		    if (i_582_ > 1)
			anIntArray7442[i_582_ - 2] = i_580_;
		    i_580_ = i_581_;
		    int i_583_ = (-380604831 * anInt7455
				  - -1213435377 * anInt7461 + i_579_);
		    int i_584_ = i_582_ + (anInt7459 * -1709472547
					   - anInt7461 * -1213435377);
		    if (i_583_ >= 0 && i_584_ >= 0
			&& i_583_ < 1183912005 * anInt7435
			&& i_584_ < 60330541 * anInt7470) {
			int i_585_ = i_583_ << anInt7422 * 941710601;
			int i_586_ = i_584_ << anInt7422 * 941710601;
			int i_587_
			    = (aClass151Array7434
				   [aClass151Array7434.length - 1]
				   .method2491(i_583_, i_584_, -768558761)
			       - (1000 << 941710601 * anInt7422 - 7));
			int i_588_
			    = ((aClass151Array7436 != null
				? (aClass151Array7436[0]
				       .method2491(i_583_, i_584_, 1225092139)
				   + -1935734157 * anInt7420)
				: (aClass151Array7434[0]
				       .method2491(i_583_, i_584_, 1806386609)
				   + -1935734157 * anInt7420))
			       + (1000 << 941710601 * anInt7422 - 7));
			i_581_ = aClass185_7453.method3308(i_585_, i_587_,
							   i_586_, i_585_,
							   i_588_, i_586_);
			aBoolArrayArray7479[i_579_][i_582_] = 0 == i_581_;
		    } else {
			i_581_ = -1;
			aBoolArrayArray7479[i_579_][i_582_] = false;
		    }
		    if (i_579_ > 0 && i_582_ > 0) {
			int i_589_
			    = (anIntArray7442[i_582_ - 1]
			       & anIntArray7442[i_582_] & i_580_ & i_581_);
			aBoolArrayArray7481[i_579_ - 1][i_582_ - 1]
			    = i_589_ == 0;
		    }
		}
		anIntArray7442[(anInt7461 * -1213435377
				+ -1213435377 * anInt7461)]
		    = i_580_;
		anIntArray7442[(anInt7461 * -1213435377
				+ -1213435377 * anInt7461 + 1)]
		    = i_581_;
	    }
	    if (!bool_577_)
		aClass552_7427.aBool7310 = false;
	    else {
		aClass552_7427.anIntArray7328 = is_566_;
		aClass552_7427.anIntArray7329 = is_567_;
		aClass552_7427.anIntArray7330 = is_568_;
		aClass552_7427.anIntArray7331 = is_569_;
		aClass552_7427.anIntArray7336 = is_570_;
		aClass552_7427.method9058(aClass185_7453, i_571_);
	    }
	} else {
	    if (null == aBoolArrayArray7482)
		aBoolArrayArray7482
		    = (new boolean
		       [-1213435377 * anInt7461 + -1213435377 * anInt7461 + 2]
		       [(-1213435377 * anInt7461 + -1213435377 * anInt7461
			 + 2)]);
	    for (int i_590_ = 0; i_590_ < aBoolArrayArray7482.length;
		 i_590_++) {
		for (int i_591_ = 0; i_591_ < aBoolArrayArray7482[0].length;
		     i_591_++)
		    aBoolArrayArray7482[i_590_][i_591_] = true;
	    }
	    aBoolArrayArray7479 = aBoolArrayArray7482;
	    aBoolArrayArray7481 = aBoolArrayArray7482;
	    anInt7419 = 0;
	    anInt7430 = 0;
	    anInt7456 = -444609591 * anInt7435;
	    anInt7467 = -1909674873 * anInt7470;
	    aClass552_7427.aBool7310 = false;
	}
	Class636.method10551(this, aClass185_7453, -441101126);
	if (!aClass557_7478.aBool7483) {
	    Iterator iterator = aClass557_7478.aList7484.iterator();
	    while (iterator.hasNext()) {
		Class550 class550 = (Class550) iterator.next();
		iterator.remove();
		Class42.method1063(class550, 1262399261);
	    }
	}
	if (aBool7443) {
	    for (int i_592_ = 0; i_592_ < anInt7472 * 1942714221; i_592_++)
		aClass561Array7439[i_592_].method9436(i, bool, 1350763384);
	}
	if (null != aClass568ArrayArrayArray7460) {
	    method9228(true, (byte) -5);
	    aClass185_7453.method3363(-1, new Class166(1583160, 40, 127, 63, 0,
						       0, 0));
	    method9269(true, is, i_571_, i_572_, i_576_);
	    aClass185_7453.method3529();
	    method9228(false, (byte) -34);
	}
	method9269(false, is, i_571_, i_572_, i_576_);
	if (!aBool7476) {
	    aBoolArrayArray7481 = bools;
	    aBoolArrayArray7479 = bools_578_;
	}
    }
    
    void method9363(boolean bool, byte[][][] is, int i, byte i_593_,
		    int i_594_) {
	int i_595_ = bool ? 1 : 0;
	anInt7450 = 0;
	anInt7451 = 0;
	anInt7428 += 964746483;
	if ((i_594_ & 0x2) == 0) {
	    for (Class654_Sub1 class654_sub1 = aClass654_Sub1Array7447[i_595_];
		 class654_sub1 != null;
		 class654_sub1 = class654_sub1.aClass654_Sub1_10850) {
		method9287(class654_sub1, (byte) 111);
		if (class654_sub1.anInt10851 * 621186375 != -1
		    && !method9328(class654_sub1, bool, is, i, i_593_))
		    aClass654_Sub1Array7424
			[(anInt7450 += 744803731) * 1319065755 - 1]
			= class654_sub1;
	    }
	}
	if ((i_594_ & 0x1) == 0) {
	    for (Class654_Sub1 class654_sub1 = aClass654_Sub1Array7448[i_595_];
		 class654_sub1 != null;
		 class654_sub1 = class654_sub1.aClass654_Sub1_10850) {
		method9287(class654_sub1, (byte) 53);
		if (-1 != class654_sub1.anInt10851 * 621186375
		    && !method9328(class654_sub1, bool, is, i, i_593_))
		    aClass654_Sub1Array7452
			[(anInt7451 += -755338105) * -219410121 - 1]
			= class654_sub1;
	    }
	    for (Class654_Sub1 class654_sub1 = aClass654_Sub1Array7449[i_595_];
		 null != class654_sub1;
		 class654_sub1 = class654_sub1.aClass654_Sub1_10850) {
		method9287(class654_sub1, (byte) 10);
		if (-1 != class654_sub1.anInt10851 * 621186375
		    && !method9328(class654_sub1, bool, is, i, i_593_)) {
		    if (class654_sub1.method16850(-1920390363))
			aClass654_Sub1Array7452
			    [(anInt7451 += -755338105) * -219410121 - 1]
			    = class654_sub1;
		    else
			aClass654_Sub1Array7424
			    [(anInt7450 += 744803731) * 1319065755 - 1]
			    = class654_sub1;
		}
	    }
	    if (!bool) {
		for (int i_596_ = 0; i_596_ < anInt7465 * -67005741;
		     i_596_++) {
		    method9287(aClass654_Sub1_Sub5Array7454[i_596_],
			       (byte) 32);
		    if ((aClass654_Sub1_Sub5Array7454[i_596_].anInt10851
			 * 621186375) != -1
			&& !method9328(aClass654_Sub1_Sub5Array7454[i_596_],
				       bool, is, i, i_593_)) {
			if (aClass654_Sub1_Sub5Array7454[i_596_]
				.method16850(-1920390363))
			    aClass654_Sub1Array7452
				[(anInt7451 += -755338105) * -219410121 - 1]
				= aClass654_Sub1_Sub5Array7454[i_596_];
			else
			    aClass654_Sub1Array7424
				[(anInt7450 += 744803731) * 1319065755 - 1]
				= aClass654_Sub1_Sub5Array7454[i_596_];
		    }
		}
	    }
	}
	if (anInt7450 * 1319065755 > 0) {
	    method9312(aClass654_Sub1Array7424, 0, 1319065755 * anInt7450 - 1);
	    for (int i_597_ = 0; i_597_ < anInt7450 * 1319065755; i_597_++)
		method9234(aClass654_Sub1Array7424[i_597_],
			   aClass534_Sub21Array7469);
	}
	if (aBool7443)
	    aClass185_7453.method3654(0, null);
	if (0 == (i_594_ & 0x2)) {
	    for (int i_598_ = 0; i_598_ < -1381097937 * anInt7418; i_598_++) {
		if (i_598_ >= i && null != is) {
		    int i_599_ = aBoolArrayArray7481.length;
		    if (-474621401 * anInt7419 + aBoolArrayArray7481.length
			> anInt7435 * 1183912005)
			i_599_ -= (anInt7419 * -474621401
				   + aBoolArrayArray7481.length
				   - 1183912005 * anInt7435);
		    int i_600_ = aBoolArrayArray7481[0].length;
		    if (anInt7430 * 1375305047 + aBoolArrayArray7481[0].length
			> anInt7470 * 60330541)
			i_600_ -= (1375305047 * anInt7430
				   + aBoolArrayArray7481[0].length
				   - 60330541 * anInt7470);
		    boolean[][] bools = aBoolArrayArray7479;
		    if (aBool7476) {
			for (int i_601_ = anInt7425 * 951958497;
			     i_601_ < i_599_; i_601_++) {
			    int i_602_ = (i_601_ + anInt7419 * -474621401
					  - anInt7425 * 951958497);
			    for (int i_603_ = anInt7463 * -856129721;
				 i_603_ < i_600_; i_603_++) {
				bools[i_601_][i_603_] = false;
				if (aBoolArrayArray7481[i_601_][i_603_]) {
				    int i_604_
					= (1375305047 * anInt7430 + i_603_
					   - -856129721 * anInt7463);
				    for (int i_605_ = i_598_; i_605_ >= 0;
					 i_605_--) {
					if ((null
					     != (aClass568ArrayArrayArray7431
						 [i_605_][i_602_][i_604_]))
					    && (i_598_
						== (aClass568ArrayArrayArray7431
						    [i_605_][i_602_][i_604_]
						    .aByte7599))) {
					    if ((i_605_ >= i
						 && (is[i_605_][i_602_][i_604_]
						     == i_593_))
						|| (aClass552_7427.method9062
						    (i_598_, i_602_, i_604_)))
						bools[i_601_][i_603_] = false;
					    else
						bools[i_601_][i_603_] = true;
					    break;
					}
				    }
				}
			    }
			}
		    }
		    aClass151Array7432[i_598_].method2506
			(anInt7455 * -380604831, anInt7459 * -1709472547,
			 -1213435377 * anInt7461, aBoolArrayArray7479, false,
			 i_594_);
		} else {
		    int i_606_ = aBoolArrayArray7481.length;
		    if (anInt7419 * -474621401 + aBoolArrayArray7481.length
			> anInt7435 * 1183912005)
			i_606_ -= (anInt7419 * -474621401
				   + aBoolArrayArray7481.length
				   - anInt7435 * 1183912005);
		    int i_607_ = aBoolArrayArray7481[0].length;
		    if (anInt7430 * 1375305047 + aBoolArrayArray7481[0].length
			> 60330541 * anInt7470)
			i_607_ -= (aBoolArrayArray7481[0].length
				   + 1375305047 * anInt7430
				   - anInt7470 * 60330541);
		    boolean[][] bools = aBoolArrayArray7479;
		    if (aBool7476) {
			for (int i_608_ = anInt7425 * 951958497;
			     i_608_ < i_606_; i_608_++) {
			    int i_609_ = (i_608_ + -474621401 * anInt7419
					  - 951958497 * anInt7425);
			    for (int i_610_ = anInt7463 * -856129721;
				 i_610_ < i_607_; i_610_++) {
				if (aBoolArrayArray7481[i_608_][i_610_]
				    && !(aClass552_7427.method9062
					 (i_598_, i_609_,
					  (i_610_ + 1375305047 * anInt7430
					   - -856129721 * anInt7463))))
				    bools[i_608_][i_610_] = true;
				else
				    bools[i_608_][i_610_] = false;
			    }
			}
		    }
		    aClass151Array7432[i_598_].method2506
			(anInt7455 * -380604831, -1709472547 * anInt7459,
			 -1213435377 * anInt7461, aBoolArrayArray7479, true,
			 i_594_);
		}
	    }
	}
	if (-219410121 * anInt7451 > 0) {
	    method9236(aClass654_Sub1Array7452, 0, -219410121 * anInt7451 - 1);
	    for (int i_611_ = 0; i_611_ < -219410121 * anInt7451; i_611_++)
		method9234(aClass654_Sub1Array7452[i_611_],
			   aClass534_Sub21Array7469);
	}
    }
    
    public int method9364(int i, int i_612_) {
	return (null != aByteArrayArray7480
		? aByteArrayArray7480[i][i_612_] & 0xff : 0);
    }
    
    void method9365(Class654_Sub1 class654_sub1) {
	Class438 class438 = class654_sub1.method10807().aClass438_4885;
	aClass185_7453.method3367((float) (int) class438.aFloat4864,
				  (float) ((int) class438.aFloat4863
					   + (class654_sub1
						  .method16876(-2138327445)
					      >> 1)),
				  (float) (int) class438.aFloat4865,
				  aFloatArray7468);
	class654_sub1.anInt10851 = 1350765687 * (int) aFloatArray7468[2];
    }
    
    void method9366(Class654_Sub1 class654_sub1) {
	Class438 class438 = class654_sub1.method10807().aClass438_4885;
	aClass185_7453.method3367((float) (int) class438.aFloat4864,
				  (float) ((int) class438.aFloat4863
					   + (class654_sub1
						  .method16876(-846974272)
					      >> 1)),
				  (float) (int) class438.aFloat4865,
				  aFloatArray7468);
	class654_sub1.anInt10851 = 1350765687 * (int) aFloatArray7468[2];
    }
    
    public void method9367(int i, int i_613_, int i_614_, int i_615_) {
	boolean bool = (null != aClass568ArrayArrayArray7431[0][i_613_][i_614_]
			&& (aClass568ArrayArrayArray7431[0][i_613_][i_614_]
			    .aClass568_7605) != null);
	for (int i_616_ = i; i_616_ >= 0; i_616_--) {
	    if (aClass568ArrayArrayArray7431[i_616_][i_613_][i_614_] == null) {
		Class568 class568
		    = (aClass568ArrayArrayArray7431[i_616_][i_613_][i_614_]
		       = new Class568(i_616_));
		if (bool)
		    class568.aByte7599++;
	    }
	}
    }
    
    public int method9368(int i, int i_617_) {
	return (aByteArrayArray7423 != null
		? aByteArrayArray7423[i][i_617_] & 0xff : 0);
    }
    
    boolean method9369(Class654_Sub1 class654_sub1, boolean bool,
		       byte[][][] is, int i, byte i_618_) {
	if (!aBool7476)
	    return false;
	if (class654_sub1 instanceof Class654_Sub1_Sub5) {
	    int i_619_ = ((Class654_Sub1_Sub5) class654_sub1).aShort11896;
	    int i_620_ = ((Class654_Sub1_Sub5) class654_sub1).aShort11898;
	    int i_621_ = ((Class654_Sub1_Sub5) class654_sub1).aShort11900;
	    int i_622_ = ((Class654_Sub1_Sub5) class654_sub1).aShort11901;
	    for (int i_623_ = i_621_; i_623_ <= i_619_; i_623_++) {
		for (int i_624_ = i_622_; i_624_ <= i_620_; i_624_++) {
		    if (class654_sub1.aByte10853 < anInt7418 * -1381097937
			&& i_623_ >= -474621401 * anInt7419
			&& i_623_ < anInt7456 * 2106025629
			&& i_624_ >= anInt7430 * 1375305047
			&& i_624_ < anInt7467 * -860025685) {
			if ((is != null && class654_sub1.aByte10854 >= i
			     && i_618_ == (is[class654_sub1.aByte10854][i_623_]
					   [i_624_]))
			    || !class654_sub1.method16858((byte) 40)
			    || class654_sub1.method16857(aClass185_7453,
							 -2121294381)) {
			    if (!bool && i_623_ >= -380604831 * anInt7455 - 16
				&& i_623_ <= -380604831 * anInt7455 + 16
				&& i_624_ >= anInt7459 * -1709472547 - 16
				&& i_624_ <= 16 + anInt7459 * -1709472547)
				class654_sub1.method16868(aClass185_7453,
							  -993952189);
			    return true;
			}
			return false;
		    }
		}
	    }
	    return true;
	}
	Class438 class438 = class654_sub1.method10807().aClass438_4885;
	int i_625_ = (int) class438.aFloat4864 >> 941710601 * anInt7422;
	int i_626_ = (int) class438.aFloat4865 >> 941710601 * anInt7422;
	if (class654_sub1.aByte10853 < anInt7418 * -1381097937
	    && i_625_ >= anInt7419 * -474621401
	    && i_625_ < anInt7456 * 2106025629
	    && i_626_ >= 1375305047 * anInt7430
	    && i_626_ < -860025685 * anInt7467) {
	    if ((is != null && class654_sub1.aByte10854 >= i
		 && i_618_ == is[class654_sub1.aByte10854][i_625_][i_626_])
		|| !class654_sub1.method16858((byte) -125)
		|| class654_sub1.method16857(aClass185_7453, -2023132783)) {
		if (!bool && i_625_ >= -380604831 * anInt7455 - 16
		    && i_625_ <= -380604831 * anInt7455 + 16
		    && i_626_ >= anInt7459 * -1709472547 - 16
		    && i_626_ <= anInt7459 * -1709472547 + 16)
		    class654_sub1.method16868(aClass185_7453, -993952189);
		return true;
	    }
	    return false;
	}
	return true;
    }
    
    public void method9370(Class534_Sub18_Sub16 class534_sub18_sub16, int i,
			   int i_627_, int i_628_, boolean[] bools) {
	if (aClass151Array7432 != aClass151Array7436) {
	    int i_629_
		= aClass151Array7434[i].method2498(i_627_, i_628_, 382329037);
	    for (int i_630_ = 0; i_630_ <= i; i_630_++) {
		if (bools == null || bools[i_630_]) {
		    Class151 class151 = aClass151Array7434[i_630_];
		    if (null != class151)
			class151.method2503
			    (class534_sub18_sub16, i_627_,
			     i_629_ - class151.method2498(i_627_, i_628_,
							  -1845371350),
			     i_628_, 0, false);
		}
	    }
	}
    }
    
    public boolean method9371(Class534_Sub18_Sub16 class534_sub18_sub16, int i,
			      int i_631_, int i_632_, boolean[] bools) {
	boolean bool = false;
	if (aClass151Array7432 != aClass151Array7436) {
	    int i_633_
		= aClass151Array7434[i].method2498(i_631_, i_632_, 1157319826);
	    int i_634_ = 0;
	    for (/**/; i_634_ <= i; i_634_++) {
		Class151 class151 = aClass151Array7434[i_634_];
		if (class151 != null) {
		    int i_635_ = i_633_ - class151.method2498(i_631_, i_632_,
							      1538685332);
		    if (null != bools) {
			bools[i_634_]
			    = class151.method2504(class534_sub18_sub16, i_631_,
						  i_635_, i_632_, 0, false);
			if (!bools[i_634_])
			    continue;
		    }
		    class151.method2502(class534_sub18_sub16, i_631_, i_635_,
					i_632_, 0, false);
		    bool = true;
		}
	    }
	}
	return bool;
    }
    
    public boolean method9372(Class534_Sub18_Sub16 class534_sub18_sub16, int i,
			      int i_636_, int i_637_, boolean[] bools) {
	boolean bool = false;
	if (aClass151Array7432 != aClass151Array7436) {
	    int i_638_
		= aClass151Array7434[i].method2498(i_636_, i_637_, 1832261379);
	    int i_639_ = 0;
	    for (/**/; i_639_ <= i; i_639_++) {
		Class151 class151 = aClass151Array7434[i_639_];
		if (class151 != null) {
		    int i_640_ = i_638_ - class151.method2498(i_636_, i_637_,
							      -354517206);
		    if (null != bools) {
			bools[i_639_]
			    = class151.method2504(class534_sub18_sub16, i_636_,
						  i_640_, i_637_, 0, false);
			if (!bools[i_639_])
			    continue;
		    }
		    class151.method2502(class534_sub18_sub16, i_636_, i_640_,
					i_637_, 0, false);
		    bool = true;
		}
	    }
	}
	return bool;
    }
    
    public void method9373(Class534_Sub18_Sub16 class534_sub18_sub16, int i,
			   int i_641_, int i_642_, boolean[] bools) {
	if (aClass151Array7432 != aClass151Array7436) {
	    int i_643_
		= aClass151Array7434[i].method2498(i_641_, i_642_, 1469086490);
	    for (int i_644_ = 0; i_644_ <= i; i_644_++) {
		if (bools == null || bools[i_644_]) {
		    Class151 class151 = aClass151Array7434[i_644_];
		    if (null != class151)
			class151.method2503
			    (class534_sub18_sub16, i_641_,
			     i_643_ - class151.method2498(i_641_, i_642_,
							  -1042817560),
			     i_642_, 0, false);
		}
	    }
	}
    }
    
    public Class654_Sub1_Sub1 method9374(int i, int i_645_, int i_646_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_645_][i_646_];
	if (class568 != null) {
	    method9257(class568.aClass654_Sub1_Sub1_7598, -1241993570);
	    if (null != class568.aClass654_Sub1_Sub1_7598) {
		Class654_Sub1_Sub1 class654_sub1_sub1
		    = class568.aClass654_Sub1_Sub1_7598;
		class568.aClass654_Sub1_Sub1_7598 = null;
		return class654_sub1_sub1;
	    }
	}
	return null;
    }
    
    void method9375(int i, int i_647_) {
	Class534_Sub18_Sub16 class534_sub18_sub16 = null;
	for (int i_648_ = i; i_648_ < i_647_; i_648_++) {
	    Class151 class151 = aClass151Array7434[i_648_];
	    if (null != class151) {
		for (int i_649_ = 0; i_649_ < anInt7470 * 60330541; i_649_++) {
		    for (int i_650_ = 0; i_650_ < 1183912005 * anInt7435;
			 i_650_++) {
			class534_sub18_sub16
			    = class151.method2526(i_650_, i_649_,
						  class534_sub18_sub16);
			if (null != class534_sub18_sub16) {
			    int i_651_ = i_650_ << anInt7422 * 941710601;
			    int i_652_ = i_649_ << anInt7422 * 941710601;
			    for (int i_653_ = i_648_ - 1; i_653_ >= 0;
				 i_653_--) {
				Class151 class151_654_
				    = aClass151Array7434[i_653_];
				if (class151_654_ != null) {
				    int i_655_
					= (class151.method2491(i_650_, i_649_,
							       -1461522440)
					   - (class151_654_.method2491
					      (i_650_, i_649_, -1133283963)));
				    int i_656_
					= (class151.method2491(1 + i_650_,
							       i_649_,
							       -1531699166)
					   - (class151_654_.method2491
					      (1 + i_650_, i_649_,
					       293394366)));
				    int i_657_
					= (class151.method2491(i_650_ + 1,
							       i_649_ + 1,
							       -1230909055)
					   - (class151_654_.method2491
					      (i_650_ + 1, i_649_ + 1,
					       120925238)));
				    int i_658_
					= (class151.method2491(i_650_,
							       1 + i_649_,
							       -1262736349)
					   - (class151_654_.method2491
					      (i_650_, 1 + i_649_,
					       1829463425)));
				    class151_654_.method2502
					(class534_sub18_sub16, i_651_,
					 ((i_657_ + (i_656_ + i_655_) + i_658_)
					  / 4),
					 i_652_, 0, false);
				}
			    }
			}
		    }
		}
	    }
	}
    }
    
    public void method9376
	(int i, int i_659_, int i_660_, Class654_Sub1_Sub1 class654_sub1_sub1,
	 Class654_Sub1_Sub1 class654_sub1_sub1_661_, int i_662_) {
	Class568 class568 = method9256(i, i_659_, i_660_, 768451766);
	if (class568 != null) {
	    class568.aClass654_Sub1_Sub1_7598 = class654_sub1_sub1;
	    class568.aClass654_Sub1_Sub1_7597 = class654_sub1_sub1_661_;
	    int i_663_ = aClass151Array7432 == aClass151Array7436 ? 1 : 0;
	    if (class654_sub1_sub1.method16849(-376083885)) {
		if (class654_sub1_sub1.method16850(-1920390363)) {
		    class654_sub1_sub1.aClass654_Sub1_10850
			= aClass654_Sub1Array7448[i_663_];
		    aClass654_Sub1Array7448[i_663_] = class654_sub1_sub1;
		} else {
		    class654_sub1_sub1.aClass654_Sub1_10850
			= aClass654_Sub1Array7447[i_663_];
		    aClass654_Sub1Array7447[i_663_] = class654_sub1_sub1;
		}
	    } else {
		class654_sub1_sub1.aClass654_Sub1_10850
		    = aClass654_Sub1Array7449[i_663_];
		aClass654_Sub1Array7449[i_663_] = class654_sub1_sub1;
	    }
	    if (class654_sub1_sub1_661_ != null) {
		if (class654_sub1_sub1_661_.method16849(1456068924)) {
		    if (class654_sub1_sub1_661_.method16850(-1920390363)) {
			class654_sub1_sub1_661_.aClass654_Sub1_10850
			    = aClass654_Sub1Array7448[i_663_];
			aClass654_Sub1Array7448[i_663_]
			    = class654_sub1_sub1_661_;
		    } else {
			class654_sub1_sub1_661_.aClass654_Sub1_10850
			    = aClass654_Sub1Array7447[i_663_];
			aClass654_Sub1Array7447[i_663_]
			    = class654_sub1_sub1_661_;
		    }
		} else {
		    class654_sub1_sub1_661_.aClass654_Sub1_10850
			= aClass654_Sub1Array7449[i_663_];
		    aClass654_Sub1Array7449[i_663_] = class654_sub1_sub1_661_;
		}
	    }
	}
    }
    
    void method9377(int i, int i_664_) {
	Class534_Sub18_Sub16 class534_sub18_sub16 = null;
	for (int i_665_ = i; i_665_ < i_664_; i_665_++) {
	    Class151 class151 = aClass151Array7434[i_665_];
	    if (null != class151) {
		for (int i_666_ = 0; i_666_ < anInt7470 * 60330541; i_666_++) {
		    for (int i_667_ = 0; i_667_ < 1183912005 * anInt7435;
			 i_667_++) {
			class534_sub18_sub16
			    = class151.method2526(i_667_, i_666_,
						  class534_sub18_sub16);
			if (null != class534_sub18_sub16) {
			    int i_668_ = i_667_ << anInt7422 * 941710601;
			    int i_669_ = i_666_ << anInt7422 * 941710601;
			    for (int i_670_ = i_665_ - 1; i_670_ >= 0;
				 i_670_--) {
				Class151 class151_671_
				    = aClass151Array7434[i_670_];
				if (class151_671_ != null) {
				    int i_672_
					= (class151.method2491(i_667_, i_666_,
							       -987978273)
					   - (class151_671_.method2491
					      (i_667_, i_666_, 1532396782)));
				    int i_673_
					= (class151.method2491(1 + i_667_,
							       i_666_,
							       -1334217186)
					   - (class151_671_.method2491
					      (1 + i_667_, i_666_,
					       -734237901)));
				    int i_674_
					= (class151.method2491(i_667_ + 1,
							       i_666_ + 1,
							       632343050)
					   - (class151_671_.method2491
					      (i_667_ + 1, i_666_ + 1,
					       289990480)));
				    int i_675_
					= (class151.method2491(i_667_,
							       1 + i_666_,
							       -1149572457)
					   - (class151_671_.method2491
					      (i_667_, 1 + i_666_,
					       -282494203)));
				    class151_671_.method2502
					(class534_sub18_sub16, i_668_,
					 ((i_674_ + (i_673_ + i_672_) + i_675_)
					  / 4),
					 i_669_, 0, false);
				}
			    }
			}
		    }
		}
	    }
	}
    }
    
    void method9378(int i, int i_676_) {
	Class534_Sub18_Sub16 class534_sub18_sub16 = null;
	for (int i_677_ = i; i_677_ < i_676_; i_677_++) {
	    Class151 class151 = aClass151Array7434[i_677_];
	    if (null != class151) {
		for (int i_678_ = 0; i_678_ < anInt7470 * 60330541; i_678_++) {
		    for (int i_679_ = 0; i_679_ < 1183912005 * anInt7435;
			 i_679_++) {
			class534_sub18_sub16
			    = class151.method2526(i_679_, i_678_,
						  class534_sub18_sub16);
			if (null != class534_sub18_sub16) {
			    int i_680_ = i_679_ << anInt7422 * 941710601;
			    int i_681_ = i_678_ << anInt7422 * 941710601;
			    for (int i_682_ = i_677_ - 1; i_682_ >= 0;
				 i_682_--) {
				Class151 class151_683_
				    = aClass151Array7434[i_682_];
				if (class151_683_ != null) {
				    int i_684_
					= (class151.method2491(i_679_, i_678_,
							       -645826370)
					   - (class151_683_.method2491
					      (i_679_, i_678_, -254217189)));
				    int i_685_
					= (class151.method2491(1 + i_679_,
							       i_678_,
							       -889534255)
					   - (class151_683_.method2491
					      (1 + i_679_, i_678_,
					       1894318137)));
				    int i_686_
					= (class151.method2491(i_679_ + 1,
							       i_678_ + 1,
							       -2086821182)
					   - (class151_683_.method2491
					      (i_679_ + 1, i_678_ + 1,
					       1291791797)));
				    int i_687_
					= (class151.method2491(i_679_,
							       1 + i_678_,
							       1710143355)
					   - (class151_683_.method2491
					      (i_679_, 1 + i_678_,
					       -2144392862)));
				    class151_683_.method2502
					(class534_sub18_sub16, i_680_,
					 ((i_686_ + (i_685_ + i_684_) + i_687_)
					  / 4),
					 i_681_, 0, false);
				}
			    }
			}
		    }
		}
	    }
	}
    }
    
    public void method9379() {
	method9277(1, -1381097937 * anInt7418, -31783848);
    }
    
    public void method9380(Class561 class561) {
	if (1942714221 * anInt7472 < 65165) {
	    Class534_Sub21 class534_sub21 = class561.aClass534_Sub21_7541;
	    aClass561Array7439[1942714221 * anInt7472] = class561;
	    aBoolArray7475[anInt7472 * 1942714221] = false;
	    anInt7472 += -328586651;
	    int i = class561.anInt7544 * 551684827;
	    if (class561.aBool7543)
		i = 0;
	    int i_688_ = 551684827 * class561.anInt7544;
	    if (class561.aBool7542)
		i_688_ = anInt7418 * -1381097937 - 1;
	    for (int i_689_ = i; i_689_ <= i_688_; i_689_++) {
		int i_690_ = 0;
		int i_691_ = ((class534_sub21.method16199((byte) -10)
			       - class534_sub21.method16233(-337404484)
			       + anInt7421 * -1978691487)
			      >> 941710601 * anInt7422);
		if (i_691_ < 0) {
		    i_690_ -= i_691_;
		    i_691_ = 0;
		}
		int i_692_ = ((class534_sub21.method16199((byte) -21)
			       + class534_sub21.method16233(-1568551603)
			       - -1978691487 * anInt7421)
			      >> anInt7422 * 941710601);
		if (i_692_ >= 60330541 * anInt7470)
		    i_692_ = anInt7470 * 60330541 - 1;
		for (int i_693_ = i_691_; i_693_ <= i_692_; i_693_++) {
		    int i_694_ = class561.aShortArray7531[i_690_++];
		    int i_695_ = (((class534_sub21.method16197(913263644)
				    - class534_sub21.method16233(-1900627304)
				    + -1978691487 * anInt7421)
				   >> 941710601 * anInt7422)
				  + (i_694_ >>> 8));
		    int i_696_ = i_695_ + (i_694_ & 0xff) - 1;
		    if (i_695_ < 0)
			i_695_ = 0;
		    if (i_696_ >= 1183912005 * anInt7435)
			i_696_ = anInt7435 * 1183912005 - 1;
		    for (int i_697_ = i_695_; i_697_ <= i_696_; i_697_++) {
			long l
			    = aLongArrayArrayArray7473[i_689_][i_697_][i_693_];
			if ((l & 0xffffL) == 0L)
			    aLongArrayArrayArray7473[i_689_][i_697_][i_693_]
				= l | (long) (anInt7472 * 1942714221);
			else if ((l & 0xffff0000L) == 0L)
			    aLongArrayArrayArray7473[i_689_][i_697_][i_693_]
				= l | (long) (1942714221 * anInt7472) << 16;
			else if (0L == (l & 0xffff00000000L))
			    aLongArrayArrayArray7473[i_689_][i_697_][i_693_]
				= l | (long) (anInt7472 * 1942714221) << 32;
			else if ((l & ~0xffffffffffffL) == 0L)
			    aLongArrayArrayArray7473[i_689_][i_697_][i_693_]
				= l | (long) (anInt7472 * 1942714221) << 48;
		    }
		}
	    }
	    if (-1 != class561.anInt7546 * -1446450775) {
		List list
		    = (List) aHashMap7429.get(Integer.valueOf(-1446450775
							      * (class561
								 .anInt7546)));
		if (list == null) {
		    list = new ArrayList();
		    aHashMap7429.put(Integer.valueOf(-1446450775
						     * class561.anInt7546),
				     list);
		}
		list.add(class561);
		Class534_Sub21 class534_sub21_698_
		    = ((Class534_Sub21)
		       aHashMap7477.get(Integer.valueOf(-1446450775
							* (class561
							   .anInt7546))));
		if (class534_sub21_698_ != null) {
		    class561.aClass534_Sub21_7541.method16207
			(class534_sub21_698_.method16201((byte) -3),
			 750498360);
		    class561.aClass534_Sub21_7541.method16235
			(class534_sub21_698_.method16230((byte) 3),
			 class534_sub21_698_.method16206((byte) -6),
			 (byte) 78);
		}
	    }
	}
    }
    
    public void method9381(Class561 class561) {
	if (1942714221 * anInt7472 < 65165) {
	    Class534_Sub21 class534_sub21 = class561.aClass534_Sub21_7541;
	    aClass561Array7439[1942714221 * anInt7472] = class561;
	    aBoolArray7475[anInt7472 * 1942714221] = false;
	    anInt7472 += -328586651;
	    int i = class561.anInt7544 * 551684827;
	    if (class561.aBool7543)
		i = 0;
	    int i_699_ = 551684827 * class561.anInt7544;
	    if (class561.aBool7542)
		i_699_ = anInt7418 * -1381097937 - 1;
	    for (int i_700_ = i; i_700_ <= i_699_; i_700_++) {
		int i_701_ = 0;
		int i_702_ = ((class534_sub21.method16199((byte) -80)
			       - class534_sub21.method16233(-208627394)
			       + anInt7421 * -1978691487)
			      >> 941710601 * anInt7422);
		if (i_702_ < 0) {
		    i_701_ -= i_702_;
		    i_702_ = 0;
		}
		int i_703_ = ((class534_sub21.method16199((byte) -53)
			       + class534_sub21.method16233(-771021907)
			       - -1978691487 * anInt7421)
			      >> anInt7422 * 941710601);
		if (i_703_ >= 60330541 * anInt7470)
		    i_703_ = anInt7470 * 60330541 - 1;
		for (int i_704_ = i_702_; i_704_ <= i_703_; i_704_++) {
		    int i_705_ = class561.aShortArray7531[i_701_++];
		    int i_706_ = (((class534_sub21.method16197(1658489310)
				    - class534_sub21.method16233(-835718778)
				    + -1978691487 * anInt7421)
				   >> 941710601 * anInt7422)
				  + (i_705_ >>> 8));
		    int i_707_ = i_706_ + (i_705_ & 0xff) - 1;
		    if (i_706_ < 0)
			i_706_ = 0;
		    if (i_707_ >= 1183912005 * anInt7435)
			i_707_ = anInt7435 * 1183912005 - 1;
		    for (int i_708_ = i_706_; i_708_ <= i_707_; i_708_++) {
			long l
			    = aLongArrayArrayArray7473[i_700_][i_708_][i_704_];
			if ((l & 0xffffL) == 0L)
			    aLongArrayArrayArray7473[i_700_][i_708_][i_704_]
				= l | (long) (anInt7472 * 1942714221);
			else if ((l & 0xffff0000L) == 0L)
			    aLongArrayArrayArray7473[i_700_][i_708_][i_704_]
				= l | (long) (1942714221 * anInt7472) << 16;
			else if (0L == (l & 0xffff00000000L))
			    aLongArrayArrayArray7473[i_700_][i_708_][i_704_]
				= l | (long) (anInt7472 * 1942714221) << 32;
			else if ((l & ~0xffffffffffffL) == 0L)
			    aLongArrayArrayArray7473[i_700_][i_708_][i_704_]
				= l | (long) (anInt7472 * 1942714221) << 48;
		    }
		}
	    }
	    if (-1 != class561.anInt7546 * -1446450775) {
		List list
		    = (List) aHashMap7429.get(Integer.valueOf(-1446450775
							      * (class561
								 .anInt7546)));
		if (list == null) {
		    list = new ArrayList();
		    aHashMap7429.put(Integer.valueOf(-1446450775
						     * class561.anInt7546),
				     list);
		}
		list.add(class561);
		Class534_Sub21 class534_sub21_709_
		    = ((Class534_Sub21)
		       aHashMap7477.get(Integer.valueOf(-1446450775
							* (class561
							   .anInt7546))));
		if (class534_sub21_709_ != null) {
		    class561.aClass534_Sub21_7541.method16207
			(class534_sub21_709_.method16201((byte) 100),
			 1195698296);
		    class561.aClass534_Sub21_7541.method16235
			(class534_sub21_709_.method16230((byte) 45),
			 class534_sub21_709_.method16206((byte) 3), (byte) 19);
		}
	    }
	}
    }
    
    public void method9382(Class561 class561) {
	if (1942714221 * anInt7472 < 65165) {
	    Class534_Sub21 class534_sub21 = class561.aClass534_Sub21_7541;
	    aClass561Array7439[1942714221 * anInt7472] = class561;
	    aBoolArray7475[anInt7472 * 1942714221] = false;
	    anInt7472 += -328586651;
	    int i = class561.anInt7544 * 551684827;
	    if (class561.aBool7543)
		i = 0;
	    int i_710_ = 551684827 * class561.anInt7544;
	    if (class561.aBool7542)
		i_710_ = anInt7418 * -1381097937 - 1;
	    for (int i_711_ = i; i_711_ <= i_710_; i_711_++) {
		int i_712_ = 0;
		int i_713_ = ((class534_sub21.method16199((byte) -38)
			       - class534_sub21.method16233(111166047)
			       + anInt7421 * -1978691487)
			      >> 941710601 * anInt7422);
		if (i_713_ < 0) {
		    i_712_ -= i_713_;
		    i_713_ = 0;
		}
		int i_714_ = ((class534_sub21.method16199((byte) -37)
			       + class534_sub21.method16233(-1459351141)
			       - -1978691487 * anInt7421)
			      >> anInt7422 * 941710601);
		if (i_714_ >= 60330541 * anInt7470)
		    i_714_ = anInt7470 * 60330541 - 1;
		for (int i_715_ = i_713_; i_715_ <= i_714_; i_715_++) {
		    int i_716_ = class561.aShortArray7531[i_712_++];
		    int i_717_ = (((class534_sub21.method16197(-1758813966)
				    - class534_sub21.method16233(-356511940)
				    + -1978691487 * anInt7421)
				   >> 941710601 * anInt7422)
				  + (i_716_ >>> 8));
		    int i_718_ = i_717_ + (i_716_ & 0xff) - 1;
		    if (i_717_ < 0)
			i_717_ = 0;
		    if (i_718_ >= 1183912005 * anInt7435)
			i_718_ = anInt7435 * 1183912005 - 1;
		    for (int i_719_ = i_717_; i_719_ <= i_718_; i_719_++) {
			long l
			    = aLongArrayArrayArray7473[i_711_][i_719_][i_715_];
			if ((l & 0xffffL) == 0L)
			    aLongArrayArrayArray7473[i_711_][i_719_][i_715_]
				= l | (long) (anInt7472 * 1942714221);
			else if ((l & 0xffff0000L) == 0L)
			    aLongArrayArrayArray7473[i_711_][i_719_][i_715_]
				= l | (long) (1942714221 * anInt7472) << 16;
			else if (0L == (l & 0xffff00000000L))
			    aLongArrayArrayArray7473[i_711_][i_719_][i_715_]
				= l | (long) (anInt7472 * 1942714221) << 32;
			else if ((l & ~0xffffffffffffL) == 0L)
			    aLongArrayArrayArray7473[i_711_][i_719_][i_715_]
				= l | (long) (anInt7472 * 1942714221) << 48;
		    }
		}
	    }
	    if (-1 != class561.anInt7546 * -1446450775) {
		List list
		    = (List) aHashMap7429.get(Integer.valueOf(-1446450775
							      * (class561
								 .anInt7546)));
		if (list == null) {
		    list = new ArrayList();
		    aHashMap7429.put(Integer.valueOf(-1446450775
						     * class561.anInt7546),
				     list);
		}
		list.add(class561);
		Class534_Sub21 class534_sub21_720_
		    = ((Class534_Sub21)
		       aHashMap7477.get(Integer.valueOf(-1446450775
							* (class561
							   .anInt7546))));
		if (class534_sub21_720_ != null) {
		    class561.aClass534_Sub21_7541.method16207
			(class534_sub21_720_.method16201((byte) 38),
			 221691003);
		    class561.aClass534_Sub21_7541.method16235
			(class534_sub21_720_.method16230((byte) -21),
			 class534_sub21_720_.method16206((byte) 76),
			 (byte) 48);
		}
	    }
	}
    }
    
    public void method9383(int i, int i_721_, int i_722_) {
	boolean bool = (null != aClass568ArrayArrayArray7431[0][i_721_][i_722_]
			&& (aClass568ArrayArrayArray7431[0][i_721_][i_722_]
			    .aClass568_7605) != null);
	for (int i_723_ = i; i_723_ >= 0; i_723_--) {
	    if (aClass568ArrayArrayArray7431[i_723_][i_721_][i_722_] == null) {
		Class568 class568
		    = (aClass568ArrayArrayArray7431[i_723_][i_721_][i_722_]
		       = new Class568(i_723_));
		if (bool)
		    class568.aByte7599++;
	    }
	}
    }
    
    public void method9384() {
	for (int i = 0; i < anInt7472 * 1942714221; i++) {
	    if (!aBoolArray7475[i]) {
		Class561 class561 = aClass561Array7439[i];
		Class534_Sub21 class534_sub21 = class561.aClass534_Sub21_7541;
		int i_724_ = class561.anInt7544 * 551684827;
		int i_725_ = (class534_sub21.method16233(-929958175)
			      - anInt7421 * -1978691487);
		int i_726_ = (2 * i_725_ >> 941710601 * anInt7422) + 1;
		int i_727_ = 0;
		int[] is = new int[i_726_ * i_726_];
		int i_728_ = (class534_sub21.method16197(1396628012) - i_725_
			      >> 941710601 * anInt7422);
		int i_729_ = (class534_sub21.method16199((byte) -65) - i_725_
			      >> 941710601 * anInt7422);
		int i_730_ = (class534_sub21.method16199((byte) -110) + i_725_
			      >> 941710601 * anInt7422);
		if (i_729_ < 0) {
		    i_727_ -= i_729_;
		    i_729_ = 0;
		}
		if (i_730_ >= 60330541 * anInt7470)
		    i_730_ = 60330541 * anInt7470 - 1;
		for (int i_731_ = i_729_; i_731_ <= i_730_; i_731_++) {
		    int i_732_ = class561.aShortArray7531[i_727_];
		    int i_733_ = i_732_ >>> 8;
		    int i_734_ = i_733_ + i_726_ * i_727_;
		    int i_735_ = i_728_ + (i_732_ >>> 8);
		    int i_736_ = i_735_ + (i_732_ & 0xff) - 1;
		    if (i_735_ < 0) {
			i_734_ -= i_735_;
			i_735_ = 0;
		    }
		    if (i_736_ >= 1183912005 * anInt7435)
			i_736_ = anInt7435 * 1183912005 - 1;
		    for (int i_737_ = i_735_; i_737_ <= i_736_; i_737_++) {
			int i_738_ = 1;
			Class654_Sub1_Sub5 class654_sub1_sub5
			    = method9262(i_724_, i_737_, i_731_, null,
					 (byte) -17);
			if (null != class654_sub1_sub5
			    && 0 != class654_sub1_sub5.aByte11899) {
			    if (class654_sub1_sub5.aByte11899 == 1) {
				boolean bool = i_737_ - 1 >= i_735_;
				boolean bool_739_ = i_737_ + 1 <= i_736_;
				if (!bool && 1 + i_731_ <= i_730_) {
				    int i_740_
					= class561.aShortArray7531[1 + i_727_];
				    int i_741_ = i_728_ + (i_740_ >>> 8);
				    int i_742_ = i_741_ + (i_740_ & 0xff);
				    bool = i_737_ > i_741_ && i_737_ < i_742_;
				}
				if (!bool_739_ && i_731_ - 1 >= i_729_) {
				    int i_743_
					= class561.aShortArray7531[i_727_ - 1];
				    int i_744_ = i_728_ + (i_743_ >>> 8);
				    int i_745_ = i_744_ + (i_743_ & 0xff);
				    bool_739_
					= i_737_ > i_744_ && i_737_ < i_745_;
				}
				if (bool && !bool_739_)
				    i_738_ = 4;
				else if (bool_739_ && !bool)
				    i_738_ = 2;
			    } else {
				boolean bool = i_737_ - 1 >= i_735_;
				boolean bool_746_ = i_737_ + 1 <= i_736_;
				if (!bool && i_731_ - 1 >= i_729_) {
				    int i_747_
					= class561.aShortArray7531[i_727_ - 1];
				    int i_748_ = (i_747_ >>> 8) + i_728_;
				    int i_749_ = i_748_ + (i_747_ & 0xff);
				    bool = i_737_ > i_748_ && i_737_ < i_749_;
				}
				if (!bool_746_ && i_731_ + 1 <= i_730_) {
				    int i_750_
					= class561.aShortArray7531[1 + i_727_];
				    int i_751_ = i_728_ + (i_750_ >>> 8);
				    int i_752_ = i_751_ + (i_750_ & 0xff);
				    bool_746_
					= i_737_ > i_751_ && i_737_ < i_752_;
				}
				if (bool && !bool_746_)
				    i_738_ = 3;
				else if (bool_746_ && !bool)
				    i_738_ = 5;
			    }
			}
			is[i_734_++] = i_738_;
		    }
		    i_727_++;
		}
		aBoolArray7475[i] = true;
		if (aBool7440)
		    aClass151Array7432[i_724_].method2530(class534_sub21, is);
	    }
	}
    }
    
    public void method9385(int i, int i_753_, int i_754_) {
	List list = (List) aHashMap7429.get(Integer.valueOf(i));
	if (list != null) {
	    Iterator iterator = list.iterator();
	    while (iterator.hasNext()) {
		Class561 class561 = (Class561) iterator.next();
		class561.aClass534_Sub21_7541.method16235(i_753_, i_754_,
							  (byte) 56);
	    }
	}
    }
    
    public void method9386(int i, int i_755_, int i_756_) {
	List list = (List) aHashMap7429.get(Integer.valueOf(i));
	if (list != null) {
	    Iterator iterator = list.iterator();
	    while (iterator.hasNext()) {
		Class561 class561 = (Class561) iterator.next();
		class561.aClass534_Sub21_7541.method16235(i_755_, i_756_,
							  (byte) 95);
	    }
	}
    }
    
    public Class654_Sub1_Sub3 method9387(int i, int i_757_, int i_758_,
					 int i_759_) {
	Class568 class568 = aClass568ArrayArrayArray7431[i][i_757_][i_758_];
	if (null != class568) {
	    method9257(class568.aClass654_Sub1_Sub3_7600, -1974750911);
	    if (null != class568.aClass654_Sub1_Sub3_7600) {
		Class654_Sub1_Sub3 class654_sub1_sub3
		    = class568.aClass654_Sub1_Sub3_7600;
		class568.aClass654_Sub1_Sub3_7600 = null;
		return class654_sub1_sub3;
	    }
	}
	return null;
    }
    
    public HashMap method9388() {
	aHashMap7477.clear();
	Iterator iterator = aHashMap7429.entrySet().iterator();
	while (iterator.hasNext()) {
	    Map.Entry entry = (Map.Entry) iterator.next();
	    aHashMap7477.put(entry.getKey(),
			     (((Class561) ((List) entry.getValue()).get(0))
			      .aClass534_Sub21_7541));
	}
	return aHashMap7477;
    }
    
    public HashMap method9389() {
	aHashMap7477.clear();
	Iterator iterator = aHashMap7429.entrySet().iterator();
	while (iterator.hasNext()) {
	    Map.Entry entry = (Map.Entry) iterator.next();
	    aHashMap7477.put(entry.getKey(),
			     (((Class561) ((List) entry.getValue()).get(0))
			      .aClass534_Sub21_7541));
	}
	return aHashMap7477;
    }
    
    public void method9390(HashMap hashmap) {
	aHashMap7477 = hashmap;
    }
    
    public void method9391(HashMap hashmap) {
	aHashMap7477 = hashmap;
    }
    
    public void method9392(HashMap hashmap) {
	aHashMap7477 = hashmap;
    }
    
    public void method9393(HashMap hashmap) {
	aHashMap7477 = hashmap;
    }
    
    static final void method9394(Class247 class247, Class243 class243,
				 Class669 class669, int i) {
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	int[] is = Class546.method8989(string, class669, -219410121);
	if (is != null)
	    string = string.substring(0, string.length() - 1);
	class247.anObjectArray2582
	    = Class99.method1859(string, class669, 819716396);
	class247.anIntArray2588 = is;
	class247.aBool2561 = true;
    }
    
    static final void method9395(Class669 class669, int i) {
	int i_760_ = (class669.anIntArray8595
		      [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_760_, 1457054679);
	Class592.method9887(class247, class669, (byte) 110);
    }
    
    static String method9396(long l) {
	if (l <= 0L || l >= 6582952005840035281L)
	    return null;
	if (l % 37L == 0L)
	    return null;
	int i = 0;
	for (long l_761_ = l; 0L != l_761_; l_761_ /= 37L)
	    i++;
	StringBuilder stringbuilder = new StringBuilder(i);
	while (0L != l) {
	    long l_762_ = l;
	    l /= 37L;
	    char c = Class687.aCharArray8710[(int) (l_762_ - l * 37L)];
	    if (c == '_') {
		int i_763_ = stringbuilder.length() - 1;
		stringbuilder.setCharAt
		    (i_763_,
		     Character.toUpperCase(stringbuilder.charAt(i_763_)));
		c = '\u00a0';
	    }
	    stringbuilder.append(c);
	}
	stringbuilder.reverse();
	stringbuilder
	    .setCharAt(0, Character.toUpperCase(stringbuilder.charAt(0)));
	return stringbuilder.toString();
    }
    
    static final void method9397(Class669 class669, int i) {
	int i_764_ = (class669.anIntArray8595
		      [(class669.anInt8600 -= 308999563) * 2088438307]);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = i_764_ & 0x3fff;
    }
    
    public static void method9398(Class534 class534, Class534 class534_765_,
				  int i) {
	if (null != class534.aClass534_7157)
	    class534.method8892((byte) 1);
	class534.aClass534_7157 = class534_765_.aClass534_7157;
	class534.aClass534_7159 = class534_765_;
	class534.aClass534_7157.aClass534_7159 = class534;
	class534.aClass534_7159.aClass534_7157 = class534;
    }
    
    static final void method9399(Class669 class669, int i) {
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	Class100 class100 = Class201.method3864(2095398292);
	Class534_Sub19 class534_sub19
	    = Class346.method6128(Class404.aClass404_4220,
				  class100.aClass13_1183, 1341391005);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16506(0, 594702648);
	int i_766_
	    = class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811 * 31645619;
	class534_sub19.aClass534_Sub40_Sub1_10513.method16713(string,
							      -878325066);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16507
	    (-1791556207 * class669.aClass395_8601.anInt4103, 2107375332);
	class669.aClass395_8601.aClass534_Sub18_Sub12_4104.method18357
	    (class534_sub19.aClass534_Sub40_Sub1_10513,
	     class669.aClass395_8601.anIntArray4102, 2092425168);
	class534_sub19.aClass534_Sub40_Sub1_10513.method16528
	    ((class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811 * 31645619
	      - i_766_),
	     2007520410);
	class100.method1863(class534_sub19, (byte) 35);
    }
    
    static void method9400(Class669 class669, int i) {
	class669.anInt8594 -= -1374580330;
	String string
	    = ((String)
	       class669.anObjectArray8593[class669.anInt8594 * 1485266147]);
	String string_767_ = (String) (class669.anObjectArray8593
				       [class669.anInt8594 * 1485266147 + 1]);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class492.method8092(string, string_767_, -1518532947);
    }
    
    static final void method9401(Class669 class669, int i) {
	int i_768_ = (class669.anIntArray8595
		      [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_768_, 1615236132);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_768_ >> 16];
	Class641.method10600(class247, class243, class669, (byte) 74);
    }
}
