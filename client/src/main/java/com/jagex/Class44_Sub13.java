/* Class44_Sub13 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class44_Sub13 extends Class44
{
    public static final int anInt11008 = 256;
    
    public void method1098() {
	super.method1080(65280);
	((Class601) anInterface6_326).method9968((byte) 103);
    }
    
    public Class44_Sub13(Class675 class675, Class672 class672, boolean bool,
			 Class472 class472, Class472 class472_0_) {
	super(class675, class672, class472, Class649.aClass649_8384, 256,
	      new Class601_Sub1(bool, class472_0_, class672, class675));
    }
    
    public void method17349(int i, int i_1_) {
	((Class601) anInterface6_326).method9954(i, 1753185446);
    }
    
    public void method1080(int i) {
	super.method1080(65280);
	((Class601) anInterface6_326).method9968((byte) 66);
    }
    
    public void method1078() {
	super.method1080(65280);
	((Class601) anInterface6_326).method9968((byte) 120);
    }
    
    public void method1085(int i) {
	super.method1085(1981288662);
	((Class601) anInterface6_326).method9956(238507345);
    }
    
    public void method1077() {
	super.method1080(65280);
	((Class601) anInterface6_326).method9968((byte) 90);
    }
    
    public void method1092(int i) {
	super.method1081(i, (short) 11740);
	((Class601) anInterface6_326).method9965(i, (byte) 59);
    }
    
    public void method1076() {
	super.method1080(65280);
	((Class601) anInterface6_326).method9968((byte) 35);
    }
    
    public void method17350(int i) {
	((Class601) anInterface6_326).method9954(i, -965068939);
    }
    
    public void method1084(int i) {
	super.method1081(i, (short) 11272);
	((Class601) anInterface6_326).method9965(i, (byte) 24);
    }
    
    public void method1081(int i, short i_2_) {
	super.method1081(i, (short) 2298);
	((Class601) anInterface6_326).method9965(i, (byte) 107);
    }
    
    public void method1087() {
	super.method1085(1978775286);
	((Class601) anInterface6_326).method9956(-1041793543);
    }
    
    public void method1088() {
	super.method1085(2137146053);
	((Class601) anInterface6_326).method9956(-1907125508);
    }
    
    public void method1089() {
	super.method1085(2113179573);
	((Class601) anInterface6_326).method9956(-634883697);
    }
    
    public void method1090() {
	super.method1085(1934067123);
	((Class601) anInterface6_326).method9956(-248587642);
    }
    
    public void method1091() {
	super.method1085(2095388709);
	((Class601) anInterface6_326).method9956(-1361852942);
    }
    
    public void method17351(int i) {
	((Class601) anInterface6_326).method9954(i, 1393379236);
    }
    
    public void method17352(int i) {
	((Class601) anInterface6_326).method9954(i, -433315552);
    }
    
    public void method17353(boolean bool, byte i) {
	((Class601) anInterface6_326).method9955(bool, (byte) 14);
    }
    
    public void method17354(int i) {
	((Class601) anInterface6_326).method9954(i, -698618383);
    }
    
    public void method17355(int i) {
	((Class601) anInterface6_326).method9954(i, 495643385);
    }
}
