/* Class713 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.awt.Frame;

public final class Class713
{
    public static boolean aBool8884 = true;
    
    Class713() throws Throwable {
	throw new Error();
    }
    
    public static Frame method14425(Class14 class14, int i, int i_0_, int i_1_,
				    int i_2_) {
	if (0 == i_1_) {
	    Class6[] class6s = Class644.method10680(class14, 1723434021);
	    if (class6s == null)
		return null;
	    boolean bool = false;
	    for (int i_3_ = 0; i_3_ < class6s.length; i_3_++) {
		if (class6s[i_3_].anInt52 * -1991181227 == i
		    && -778390023 * class6s[i_3_].anInt53 == i_0_
		    && (0 == i_2_
			|| i_2_ == class6s[i_3_].anInt55 * 1793071357)
		    && (!bool || 198554383 * class6s[i_3_].anInt54 > i_1_)) {
		    i_1_ = 198554383 * class6s[i_3_].anInt54;
		    bool = true;
		}
	    }
	    if (!bool)
		return null;
	}
	Frame frame = new Frame("Jagex Full Screen");
	frame.setResizable(false);
	class14.method653(frame, i, i_0_, i_1_, i_2_, (byte) -64);
	return frame;
    }
    
    public static Class6[] method14426(Class14 class14) {
	int[] is = class14.method652(2088438307);
	Class6[] class6s = new Class6[is.length >> 2];
	for (int i = 0; i < class6s.length; i++) {
	    Class6 class6 = new Class6();
	    class6s[i] = class6;
	    class6.anInt52 = is[i << 2] * 1773936893;
	    class6.anInt53 = 1510603849 * is[(i << 2) + 1];
	    class6.anInt54 = is[2 + (i << 2)] * 1739643887;
	    class6.anInt55 = 791594069 * is[3 + (i << 2)];
	}
	return class6s;
    }
    
    public static Frame method14427(Class14 class14, int i, int i_4_, int i_5_,
				    int i_6_) {
	if (0 == i_5_) {
	    Class6[] class6s = Class644.method10680(class14, 1341015449);
	    if (class6s == null)
		return null;
	    boolean bool = false;
	    for (int i_7_ = 0; i_7_ < class6s.length; i_7_++) {
		if (class6s[i_7_].anInt52 * -1991181227 == i
		    && -778390023 * class6s[i_7_].anInt53 == i_4_
		    && (0 == i_6_
			|| i_6_ == class6s[i_7_].anInt55 * 1793071357)
		    && (!bool || 198554383 * class6s[i_7_].anInt54 > i_5_)) {
		    i_5_ = 198554383 * class6s[i_7_].anInt54;
		    bool = true;
		}
	    }
	    if (!bool)
		return null;
	}
	Frame frame = new Frame("Jagex Full Screen");
	frame.setResizable(false);
	class14.method653(frame, i, i_4_, i_5_, i_6_, (byte) -71);
	return frame;
    }
    
    public static Frame method14428(Class14 class14, int i, int i_8_, int i_9_,
				    int i_10_) {
	if (0 == i_9_) {
	    Class6[] class6s = Class644.method10680(class14, -1630001958);
	    if (class6s == null)
		return null;
	    boolean bool = false;
	    for (int i_11_ = 0; i_11_ < class6s.length; i_11_++) {
		if (class6s[i_11_].anInt52 * -1991181227 == i
		    && -778390023 * class6s[i_11_].anInt53 == i_8_
		    && (0 == i_10_
			|| i_10_ == class6s[i_11_].anInt55 * 1793071357)
		    && (!bool || 198554383 * class6s[i_11_].anInt54 > i_9_)) {
		    i_9_ = 198554383 * class6s[i_11_].anInt54;
		    bool = true;
		}
	    }
	    if (!bool)
		return null;
	}
	Frame frame = new Frame("Jagex Full Screen");
	frame.setResizable(false);
	class14.method653(frame, i, i_8_, i_9_, i_10_, (byte) -112);
	return frame;
    }
    
    public static Frame method14429(Class14 class14, int i, int i_12_,
				    int i_13_, int i_14_) {
	if (0 == i_13_) {
	    Class6[] class6s = Class644.method10680(class14, 1859634892);
	    if (class6s == null)
		return null;
	    boolean bool = false;
	    for (int i_15_ = 0; i_15_ < class6s.length; i_15_++) {
		if (class6s[i_15_].anInt52 * -1991181227 == i
		    && -778390023 * class6s[i_15_].anInt53 == i_12_
		    && (0 == i_14_
			|| i_14_ == class6s[i_15_].anInt55 * 1793071357)
		    && (!bool || 198554383 * class6s[i_15_].anInt54 > i_13_)) {
		    i_13_ = 198554383 * class6s[i_15_].anInt54;
		    bool = true;
		}
	    }
	    if (!bool)
		return null;
	}
	Frame frame = new Frame("Jagex Full Screen");
	frame.setResizable(false);
	class14.method653(frame, i, i_12_, i_13_, i_14_, (byte) -91);
	return frame;
    }
    
    public static Frame method14430(Class14 class14, int i, int i_16_,
				    int i_17_, int i_18_) {
	if (0 == i_17_) {
	    Class6[] class6s = Class644.method10680(class14, 358817042);
	    if (class6s == null)
		return null;
	    boolean bool = false;
	    for (int i_19_ = 0; i_19_ < class6s.length; i_19_++) {
		if (class6s[i_19_].anInt52 * -1991181227 == i
		    && -778390023 * class6s[i_19_].anInt53 == i_16_
		    && (0 == i_18_
			|| i_18_ == class6s[i_19_].anInt55 * 1793071357)
		    && (!bool || 198554383 * class6s[i_19_].anInt54 > i_17_)) {
		    i_17_ = 198554383 * class6s[i_19_].anInt54;
		    bool = true;
		}
	    }
	    if (!bool)
		return null;
	}
	Frame frame = new Frame("Jagex Full Screen");
	frame.setResizable(false);
	class14.method653(frame, i, i_16_, i_17_, i_18_, (byte) -102);
	return frame;
    }
    
    public static Frame method14431(Class14 class14, int i, int i_20_,
				    int i_21_, int i_22_) {
	if (0 == i_21_) {
	    Class6[] class6s = Class644.method10680(class14, 1554810182);
	    if (class6s == null)
		return null;
	    boolean bool = false;
	    for (int i_23_ = 0; i_23_ < class6s.length; i_23_++) {
		if (class6s[i_23_].anInt52 * -1991181227 == i
		    && -778390023 * class6s[i_23_].anInt53 == i_20_
		    && (0 == i_22_
			|| i_22_ == class6s[i_23_].anInt55 * 1793071357)
		    && (!bool || 198554383 * class6s[i_23_].anInt54 > i_21_)) {
		    i_21_ = 198554383 * class6s[i_23_].anInt54;
		    bool = true;
		}
	    }
	    if (!bool)
		return null;
	}
	Frame frame = new Frame("Jagex Full Screen");
	frame.setResizable(false);
	class14.method653(frame, i, i_20_, i_21_, i_22_, (byte) -93);
	return frame;
    }
    
    public static Class6[] method14432(Class14 class14) {
	int[] is = class14.method652(2088438307);
	Class6[] class6s = new Class6[is.length >> 2];
	for (int i = 0; i < class6s.length; i++) {
	    Class6 class6 = new Class6();
	    class6s[i] = class6;
	    class6.anInt52 = is[i << 2] * 1773936893;
	    class6.anInt53 = 1510603849 * is[(i << 2) + 1];
	    class6.anInt54 = is[2 + (i << 2)] * 1739643887;
	    class6.anInt55 = 791594069 * is[3 + (i << 2)];
	}
	return class6s;
    }
    
    public static Frame method14433(Class14 class14, int i, int i_24_,
				    int i_25_, int i_26_) {
	if (0 == i_25_) {
	    Class6[] class6s = Class644.method10680(class14, 1866322908);
	    if (class6s == null)
		return null;
	    boolean bool = false;
	    for (int i_27_ = 0; i_27_ < class6s.length; i_27_++) {
		if (class6s[i_27_].anInt52 * -1991181227 == i
		    && -778390023 * class6s[i_27_].anInt53 == i_24_
		    && (0 == i_26_
			|| i_26_ == class6s[i_27_].anInt55 * 1793071357)
		    && (!bool || 198554383 * class6s[i_27_].anInt54 > i_25_)) {
		    i_25_ = 198554383 * class6s[i_27_].anInt54;
		    bool = true;
		}
	    }
	    if (!bool)
		return null;
	}
	Frame frame = new Frame("Jagex Full Screen");
	frame.setResizable(false);
	class14.method653(frame, i, i_24_, i_25_, i_26_, (byte) -57);
	return frame;
    }
    
    public static void method14434(Class14 class14, Frame frame) {
	class14.method654((byte) 2);
	frame.setVisible(false);
	frame.dispose();
    }
    
    public static void method14435(Class14 class14, Frame frame) {
	class14.method654((byte) -32);
	frame.setVisible(false);
	frame.dispose();
    }
    
    public static void method14436(Class14 class14, Frame frame) {
	class14.method654((byte) -26);
	frame.setVisible(false);
	frame.dispose();
    }
}
