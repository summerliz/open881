/* Class677 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class677
{
    int anInt8652;
    Class597 aClass597_8653;
    static int anInt8654;
    
    public Class654_Sub1_Sub4_Sub1 method11135() {
	Class534_Sub7 class534_sub7
	    = ((Class534_Sub7)
	       (client.aClass9_11209.method579
		((long) (aClass597_8653.anInt7860 * -217302955 << 28
			 | aClass597_8653.anInt7861 * -1166289421 << 14
			 | aClass597_8653.anInt7859 * -424199969))));
	if (null == class534_sub7)
	    return null;
	Class597 class597 = client.aClass512_11100.method8416((byte) 43);
	int i = (-424199969 * aClass597_8653.anInt7859
		 - -424199969 * class597.anInt7859);
	int i_0_ = (-1166289421 * aClass597_8653.anInt7861
		    - class597.anInt7861 * -1166289421);
	if (i >= 0 && i_0_ >= 0
	    && i < client.aClass512_11100.method8417(1844631781)
	    && i_0_ < client.aClass512_11100.method8418(-1533611049)
	    && client.aClass512_11100.method8424((byte) 24) != null) {
	    for (Class534_Sub23 class534_sub23
		     = ((Class534_Sub23)
			class534_sub7.aClass700_10418.method14135((byte) -1));
		 null != class534_sub23;
		 class534_sub23
		     = (Class534_Sub23) class534_sub7.aClass700_10418
					    .method14139(1510968253)) {
		if (-1202920859 * anInt8652
		    == -400233975 * class534_sub23.anInt10548)
		    return ((Class654_Sub1_Sub4_Sub1)
			    (client.aClass512_11100.method8424((byte) 79)
				 .method9329
			     (aClass597_8653.anInt7860 * -217302955, i, i_0_,
			      -1702533605)));
	    }
	}
	return null;
    }
    
    public Class654_Sub1_Sub4_Sub1 method11136(byte i) {
	Class534_Sub7 class534_sub7
	    = ((Class534_Sub7)
	       (client.aClass9_11209.method579
		((long) (aClass597_8653.anInt7860 * -217302955 << 28
			 | aClass597_8653.anInt7861 * -1166289421 << 14
			 | aClass597_8653.anInt7859 * -424199969))));
	if (null == class534_sub7)
	    return null;
	Class597 class597 = client.aClass512_11100.method8416((byte) 71);
	int i_1_ = (-424199969 * aClass597_8653.anInt7859
		    - -424199969 * class597.anInt7859);
	int i_2_ = (-1166289421 * aClass597_8653.anInt7861
		    - class597.anInt7861 * -1166289421);
	if (i_1_ >= 0 && i_2_ >= 0
	    && i_1_ < client.aClass512_11100.method8417(-308585335)
	    && i_2_ < client.aClass512_11100.method8418(-1533611049)
	    && client.aClass512_11100.method8424((byte) 113) != null) {
	    for (Class534_Sub23 class534_sub23
		     = ((Class534_Sub23)
			class534_sub7.aClass700_10418.method14135((byte) -1));
		 null != class534_sub23;
		 class534_sub23
		     = (Class534_Sub23) class534_sub7.aClass700_10418
					    .method14139(1510502991)) {
		if (-1202920859 * anInt8652
		    == -400233975 * class534_sub23.anInt10548)
		    return ((Class654_Sub1_Sub4_Sub1)
			    (client.aClass512_11100.method8424((byte) 95)
				 .method9329
			     (aClass597_8653.anInt7860 * -217302955, i_1_,
			      i_2_, -1702533605)));
	    }
	}
	return null;
    }
    
    public Class654_Sub1_Sub4_Sub1 method11137() {
	Class534_Sub7 class534_sub7
	    = ((Class534_Sub7)
	       (client.aClass9_11209.method579
		((long) (aClass597_8653.anInt7860 * -217302955 << 28
			 | aClass597_8653.anInt7861 * -1166289421 << 14
			 | aClass597_8653.anInt7859 * -424199969))));
	if (null == class534_sub7)
	    return null;
	Class597 class597 = client.aClass512_11100.method8416((byte) 94);
	int i = (-424199969 * aClass597_8653.anInt7859
		 - -424199969 * class597.anInt7859);
	int i_3_ = (-1166289421 * aClass597_8653.anInt7861
		    - class597.anInt7861 * -1166289421);
	if (i >= 0 && i_3_ >= 0
	    && i < client.aClass512_11100.method8417(480382713)
	    && i_3_ < client.aClass512_11100.method8418(-1533611049)
	    && client.aClass512_11100.method8424((byte) 24) != null) {
	    for (Class534_Sub23 class534_sub23
		     = ((Class534_Sub23)
			class534_sub7.aClass700_10418.method14135((byte) -1));
		 null != class534_sub23;
		 class534_sub23
		     = (Class534_Sub23) class534_sub7.aClass700_10418
					    .method14139(730483858)) {
		if (-1202920859 * anInt8652
		    == -400233975 * class534_sub23.anInt10548)
		    return ((Class654_Sub1_Sub4_Sub1)
			    (client.aClass512_11100.method8424((byte) 99)
				 .method9329
			     (aClass597_8653.anInt7860 * -217302955, i, i_3_,
			      -1702533605)));
	    }
	}
	return null;
    }
    
    public Class677(Class597 class597, int i) {
	aClass597_8653 = class597;
	anInt8652 = 774766445 * i;
    }
    
    public Class654_Sub1_Sub4_Sub1 method11138() {
	Class534_Sub7 class534_sub7
	    = ((Class534_Sub7)
	       (client.aClass9_11209.method579
		((long) (aClass597_8653.anInt7860 * -217302955 << 28
			 | aClass597_8653.anInt7861 * -1166289421 << 14
			 | aClass597_8653.anInt7859 * -424199969))));
	if (null == class534_sub7)
	    return null;
	Class597 class597 = client.aClass512_11100.method8416((byte) 80);
	int i = (-424199969 * aClass597_8653.anInt7859
		 - -424199969 * class597.anInt7859);
	int i_4_ = (-1166289421 * aClass597_8653.anInt7861
		    - class597.anInt7861 * -1166289421);
	if (i >= 0 && i_4_ >= 0
	    && i < client.aClass512_11100.method8417(906508804)
	    && i_4_ < client.aClass512_11100.method8418(-1533611049)
	    && client.aClass512_11100.method8424((byte) 95) != null) {
	    for (Class534_Sub23 class534_sub23
		     = ((Class534_Sub23)
			class534_sub7.aClass700_10418.method14135((byte) -1));
		 null != class534_sub23;
		 class534_sub23
		     = (Class534_Sub23) class534_sub7.aClass700_10418
					    .method14139(1967274027)) {
		if (-1202920859 * anInt8652
		    == -400233975 * class534_sub23.anInt10548)
		    return ((Class654_Sub1_Sub4_Sub1)
			    (client.aClass512_11100.method8424((byte) 39)
				 .method9329
			     (aClass597_8653.anInt7860 * -217302955, i, i_4_,
			      -1702533605)));
	    }
	}
	return null;
    }
    
    public Class654_Sub1_Sub4_Sub1 method11139() {
	Class534_Sub7 class534_sub7
	    = ((Class534_Sub7)
	       (client.aClass9_11209.method579
		((long) (aClass597_8653.anInt7860 * -217302955 << 28
			 | aClass597_8653.anInt7861 * -1166289421 << 14
			 | aClass597_8653.anInt7859 * -424199969))));
	if (null == class534_sub7)
	    return null;
	Class597 class597 = client.aClass512_11100.method8416((byte) 40);
	int i = (-424199969 * aClass597_8653.anInt7859
		 - -424199969 * class597.anInt7859);
	int i_5_ = (-1166289421 * aClass597_8653.anInt7861
		    - class597.anInt7861 * -1166289421);
	if (i >= 0 && i_5_ >= 0
	    && i < client.aClass512_11100.method8417(-305602425)
	    && i_5_ < client.aClass512_11100.method8418(-1533611049)
	    && client.aClass512_11100.method8424((byte) 22) != null) {
	    for (Class534_Sub23 class534_sub23
		     = ((Class534_Sub23)
			class534_sub7.aClass700_10418.method14135((byte) -1));
		 null != class534_sub23;
		 class534_sub23
		     = (Class534_Sub23) class534_sub7.aClass700_10418
					    .method14139(1249058661)) {
		if (-1202920859 * anInt8652
		    == -400233975 * class534_sub23.anInt10548)
		    return ((Class654_Sub1_Sub4_Sub1)
			    (client.aClass512_11100.method8424((byte) 47)
				 .method9329
			     (aClass597_8653.anInt7860 * -217302955, i, i_5_,
			      -1702533605)));
	    }
	}
	return null;
    }
    
    public Class654_Sub1_Sub4_Sub1 method11140() {
	Class534_Sub7 class534_sub7
	    = ((Class534_Sub7)
	       (client.aClass9_11209.method579
		((long) (aClass597_8653.anInt7860 * -217302955 << 28
			 | aClass597_8653.anInt7861 * -1166289421 << 14
			 | aClass597_8653.anInt7859 * -424199969))));
	if (null == class534_sub7)
	    return null;
	Class597 class597 = client.aClass512_11100.method8416((byte) 84);
	int i = (-424199969 * aClass597_8653.anInt7859
		 - -424199969 * class597.anInt7859);
	int i_6_ = (-1166289421 * aClass597_8653.anInt7861
		    - class597.anInt7861 * -1166289421);
	if (i >= 0 && i_6_ >= 0
	    && i < client.aClass512_11100.method8417(52385475)
	    && i_6_ < client.aClass512_11100.method8418(-1533611049)
	    && client.aClass512_11100.method8424((byte) 56) != null) {
	    for (Class534_Sub23 class534_sub23
		     = ((Class534_Sub23)
			class534_sub7.aClass700_10418.method14135((byte) -1));
		 null != class534_sub23;
		 class534_sub23
		     = (Class534_Sub23) class534_sub7.aClass700_10418
					    .method14139(860157035)) {
		if (-1202920859 * anInt8652
		    == -400233975 * class534_sub23.anInt10548)
		    return ((Class654_Sub1_Sub4_Sub1)
			    (client.aClass512_11100.method8424((byte) 45)
				 .method9329
			     (aClass597_8653.anInt7860 * -217302955, i, i_6_,
			      -1702533605)));
	    }
	}
	return null;
    }
    
    static final void method11141(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 0;
    }
    
    static boolean method11142(Class603 class603, int i) {
	return Class479.method7919(class603, null, 941710601);
    }
    
    static final void method11143(Class669 class669, int i) {
	int i_7_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_7_, 1525848603);
	Class243 class243 = Class44_Sub11.aClass243Array11006[i_7_ >> 16];
	client.method17837(class247, class243, class669, (byte) -79);
    }
}
