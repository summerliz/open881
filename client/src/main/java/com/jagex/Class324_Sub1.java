/* Class324_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class324_Sub1 extends Class324
{
    public void method5796(int i) {
	aClass185_Sub1_3429.method14668(0);
	aClass185_Sub1_3429.method14672(Class378.aClass378_3916,
					Class378.aClass378_3916);
	aClass185_Sub1_3429.method14673(0, Class373.aClass373_3891);
	aClass185_Sub1_3429.method14599(0, Class373.aClass373_3891);
	aClass185_Sub1_3429.method14673(1, Class373.aClass373_3890);
	aClass185_Sub1_3429.method14599(1, Class373.aClass373_3890);
	method15695();
    }
    
    public void method5794(Class433 class433) {
	aClass185_Sub1_3429.method14894(class433,
					aClass185_Sub1_3429.aClass433_9142,
					aClass185_Sub1_3429.aClass433_9202);
    }
    
    public void method5812(boolean bool) {
	aClass185_Sub1_3429.method14668(0);
	aClass185_Sub1_3429.method14673(0, Class373.aClass373_3891);
	aClass185_Sub1_3429.method14599(0, Class373.aClass373_3891);
	aClass185_Sub1_3429.method14673(1, Class373.aClass373_3890);
	aClass185_Sub1_3429.method14599(1, Class373.aClass373_3890);
	aClass185_Sub1_3429.method14672(Class378.aClass378_3916,
					Class378.aClass378_3916);
	method15695();
    }
    
    public void method5813(int i) {
	aClass185_Sub1_3429.method14668(0);
	aClass185_Sub1_3429.method14672(Class378.aClass378_3916,
					Class378.aClass378_3916);
	aClass185_Sub1_3429.method14673(0, Class373.aClass373_3891);
	aClass185_Sub1_3429.method14599(0, Class373.aClass373_3891);
	aClass185_Sub1_3429.method14673(1, Class373.aClass373_3890);
	aClass185_Sub1_3429.method14599(1, Class373.aClass373_3890);
	method15695();
    }
    
    public void method5804(boolean bool) {
	aClass185_Sub1_3429.method14668(0);
	aClass185_Sub1_3429.method14673(0, Class373.aClass373_3891);
	aClass185_Sub1_3429.method14599(0, Class373.aClass373_3891);
	aClass185_Sub1_3429.method14673(1, Class373.aClass373_3890);
	aClass185_Sub1_3429.method14599(1, Class373.aClass373_3890);
	aClass185_Sub1_3429.method14672(Class378.aClass378_3916,
					Class378.aClass378_3916);
	method15695();
    }
    
    public void method5810(int i) {
	aClass185_Sub1_3429.method14668(0);
	aClass185_Sub1_3429.method14672(Class378.aClass378_3916,
					Class378.aClass378_3920);
	aClass185_Sub1_3429.method14673(0, Class373.aClass373_3891);
	aClass185_Sub1_3429.method14599(0, Class373.aClass373_3890);
	aClass185_Sub1_3429.method14673(1, Class373.aClass373_3890);
	aClass185_Sub1_3429.method14599(1, Class373.aClass373_3890);
	method15695();
    }
    
    public void method5805(int i) {
	aClass185_Sub1_3429.method14668(0);
	aClass185_Sub1_3429.method14672(Class378.aClass378_3916,
					Class378.aClass378_3920);
	aClass185_Sub1_3429.method14673(0, Class373.aClass373_3891);
	aClass185_Sub1_3429.method14599(0, Class373.aClass373_3890);
	aClass185_Sub1_3429.method14673(1, Class373.aClass373_3890);
	aClass185_Sub1_3429.method14599(1, Class373.aClass373_3890);
	method15695();
    }
    
    public void method5800() {
	method5796(0);
    }
    
    public void method5801(Class433 class433) {
	aClass185_Sub1_3429.method14894(class433,
					aClass185_Sub1_3429.aClass433_9142,
					aClass185_Sub1_3429.aClass433_9202);
    }
    
    public void method5797(int i) {
	aClass185_Sub1_3429.method14668(0);
	aClass185_Sub1_3429.method14672(Class378.aClass378_3916,
					Class378.aClass378_3920);
	aClass185_Sub1_3429.method14673(0, Class373.aClass373_3891);
	aClass185_Sub1_3429.method14599(0, Class373.aClass373_3890);
	aClass185_Sub1_3429.method14673(1, Class373.aClass373_3890);
	aClass185_Sub1_3429.method14599(1, Class373.aClass373_3890);
	method15695();
    }
    
    void method15695() {
	aClass185_Sub1_3429.method14669(anInterface38_3432);
	aClass185_Sub1_3429.method14679().method6842(aClass433_3430);
	aClass185_Sub1_3429.method14871(Class364.aClass364_3724);
	aClass185_Sub1_3429.method14710(Class374.aClass374_3898, anInt3447,
					anInt3428, anInt3449, anInt3450);
    }
    
    public void method5803(boolean bool) {
	aClass185_Sub1_3429.method14668(0);
	aClass185_Sub1_3429.method14673(0, Class373.aClass373_3891);
	aClass185_Sub1_3429.method14599(0, Class373.aClass373_3891);
	aClass185_Sub1_3429.method14673(1, Class373.aClass373_3890);
	aClass185_Sub1_3429.method14599(1, Class373.aClass373_3890);
	aClass185_Sub1_3429.method14672(Class378.aClass378_3916,
					Class378.aClass378_3916);
	method15695();
    }
    
    public void method5799() {
	method5796(0);
    }
    
    public void method5802(Class433 class433) {
	aClass185_Sub1_3429.method14894(class433,
					aClass185_Sub1_3429.aClass433_9142,
					aClass185_Sub1_3429.aClass433_9202);
    }
    
    public void method5806(int i) {
	aClass185_Sub1_3429.method14668(0);
	aClass185_Sub1_3429.method14672(Class378.aClass378_3916,
					Class378.aClass378_3920);
	aClass185_Sub1_3429.method14673(0, Class373.aClass373_3891);
	aClass185_Sub1_3429.method14599(0, Class373.aClass373_3890);
	aClass185_Sub1_3429.method14673(1, Class373.aClass373_3890);
	aClass185_Sub1_3429.method14599(1, Class373.aClass373_3890);
	method15695();
    }
    
    public void method5807(int i) {
	aClass185_Sub1_3429.method14668(0);
	aClass185_Sub1_3429.method14672(Class378.aClass378_3916,
					Class378.aClass378_3920);
	aClass185_Sub1_3429.method14673(0, Class373.aClass373_3891);
	aClass185_Sub1_3429.method14599(0, Class373.aClass373_3890);
	aClass185_Sub1_3429.method14673(1, Class373.aClass373_3890);
	aClass185_Sub1_3429.method14599(1, Class373.aClass373_3890);
	method15695();
    }
    
    public void method5808(int i) {
	aClass185_Sub1_3429.method14668(0);
	aClass185_Sub1_3429.method14672(Class378.aClass378_3916,
					Class378.aClass378_3920);
	aClass185_Sub1_3429.method14673(0, Class373.aClass373_3891);
	aClass185_Sub1_3429.method14599(0, Class373.aClass373_3890);
	aClass185_Sub1_3429.method14673(1, Class373.aClass373_3890);
	aClass185_Sub1_3429.method14599(1, Class373.aClass373_3890);
	method15695();
    }
    
    public void method5809() {
	method5796(0);
    }
    
    public void method5795() {
	method5796(0);
    }
    
    public void method5792() {
	method5796(0);
    }
    
    public Class324_Sub1(Class185_Sub1 class185_sub1) {
	super(class185_sub1);
    }
    
    public void method5814(int i) {
	aClass185_Sub1_3429.method14668(0);
	aClass185_Sub1_3429.method14672(Class378.aClass378_3916,
					Class378.aClass378_3916);
	aClass185_Sub1_3429.method14673(0, Class373.aClass373_3891);
	aClass185_Sub1_3429.method14599(0, Class373.aClass373_3891);
	aClass185_Sub1_3429.method14673(1, Class373.aClass373_3890);
	aClass185_Sub1_3429.method14599(1, Class373.aClass373_3890);
	method15695();
    }
    
    public void method5815(int i) {
	aClass185_Sub1_3429.method14668(0);
	aClass185_Sub1_3429.method14672(Class378.aClass378_3916,
					Class378.aClass378_3916);
	aClass185_Sub1_3429.method14673(0, Class373.aClass373_3891);
	aClass185_Sub1_3429.method14599(0, Class373.aClass373_3891);
	aClass185_Sub1_3429.method14673(1, Class373.aClass373_3890);
	aClass185_Sub1_3429.method14599(1, Class373.aClass373_3890);
	method15695();
    }
}
