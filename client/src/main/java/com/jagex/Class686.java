/* Class686 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class686 implements Interface76
{
    public static Class686 aClass686_8699;
    public static Class686 aClass686_8700;
    public static Class686 aClass686_8701;
    public static Class686 aClass686_8702;
    int anInt8703;
    public static Class686 aClass686_8704;
    public static Class686 aClass686_8705;
    public static Class686 aClass686_8706;
    public int anInt8707;
    public static Class686 aClass686_8708 = new Class686(7, 0);
    
    public Class686 method13962() {
	switch (-1846013213 * anInt8707) {
	case 0:
	    return aClass686_8702;
	case 3:
	    return aClass686_8708;
	case 2:
	    return aClass686_8706;
	case 6:
	    return aClass686_8700;
	case 7:
	    return aClass686_8701;
	case 5:
	    return aClass686_8705;
	case 1:
	    return aClass686_8704;
	case 4:
	    return aClass686_8699;
	default:
	    throw new IllegalStateException();
	}
    }
    
    public int method93() {
	return anInt8703 * 603705973;
    }
    
    public Class686 method13963(int i) {
	switch (-1846013213 * anInt8707) {
	case 0:
	    return aClass686_8702;
	case 3:
	    return aClass686_8708;
	case 2:
	    return aClass686_8706;
	case 6:
	    return aClass686_8700;
	case 7:
	    return aClass686_8701;
	case 5:
	    return aClass686_8705;
	case 1:
	    return aClass686_8704;
	case 4:
	    return aClass686_8699;
	default:
	    throw new IllegalStateException();
	}
    }
    
    Class686(int i, int i_0_) {
	anInt8707 = i * 41140939;
	anInt8703 = i_0_ * 68283869;
    }
    
    public int method22() {
	return anInt8703 * 603705973;
    }
    
    public int method53() {
	return anInt8703 * 603705973;
    }
    
    public static Class686[] method13964() {
	return new Class686[] { aClass686_8701, aClass686_8704, aClass686_8702,
				aClass686_8699, aClass686_8700, aClass686_8706,
				aClass686_8708, aClass686_8705 };
    }
    
    static {
	aClass686_8700 = new Class686(1, 1);
	aClass686_8705 = new Class686(2, 2);
	aClass686_8699 = new Class686(0, 3);
	aClass686_8701 = new Class686(3, 4);
	aClass686_8704 = new Class686(6, 5);
	aClass686_8706 = new Class686(5, 6);
	aClass686_8702 = new Class686(4, 7);
    }
    
    public Class686 method13965() {
	switch (-1846013213 * anInt8707) {
	case 0:
	    return aClass686_8702;
	case 3:
	    return aClass686_8708;
	case 2:
	    return aClass686_8706;
	case 6:
	    return aClass686_8700;
	case 7:
	    return aClass686_8701;
	case 5:
	    return aClass686_8705;
	case 1:
	    return aClass686_8704;
	case 4:
	    return aClass686_8699;
	default:
	    throw new IllegalStateException();
	}
    }
    
    static void method13966(Class669 class669, int i) {
	class669.anInt8600 -= 1235998252;
	int i_1_ = class669.anIntArray8595[class669.anInt8600 * 2088438307];
	int i_2_
	    = class669.anIntArray8595[2088438307 * class669.anInt8600 + 1];
	int i_3_
	    = class669.anIntArray8595[2 + 2088438307 * class669.anInt8600];
	int i_4_
	    = class669.anIntArray8595[3 + class669.anInt8600 * 2088438307];
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class34.method921(i_1_, i_2_, i_3_, 0 != i_4_, (byte) 17);
    }
    
    static void method13967(int i, boolean bool, int i_5_) {
	Class534_Sub5 class534_sub5 = Class269.method5023(i, bool, (byte) -17);
	if (null != class534_sub5) {
	    for (int i_6_ = 0; i_6_ < class534_sub5.anIntArray10414.length;
		 i_6_++) {
		class534_sub5.anIntArray10414[i_6_] = -1;
		class534_sub5.anIntArray10415[i_6_] = 0;
	    }
	    class534_sub5.aClass8Array10416 = null;
	}
    }
    
    static void method13968(int i) {
	Class534_Sub5.aClass9_10411.method578((byte) -14);
    }
    
    public static void method13969(Class583 class583, int i, int i_7_,
				   Interface62 interface62, int i_8_) {
	Class669 class669 = Class567.method9559((byte) -79);
	class669.anInterface62_8603 = interface62;
	Class244.method4484(class583, i, i_7_, class669, 1252957814);
	class669.anInterface62_8603 = null;
    }
    
    static final void method13970(Class247 class247, Class243 class243,
				  Class669 class669, int i) {
	String string
	    = (String) (class669.anObjectArray8593
			[(class669.anInt8594 -= 1460193483) * 1485266147]);
	if (Class546.method8989(string, class669, -219410121) != null)
	    string = string.substring(0, string.length() - 1);
	class247.anObjectArray2493
	    = Class99.method1859(string, class669, -1513457478);
	class247.aBool2561 = true;
    }
    
    static final void method13971(Class669 class669, byte i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = -1246244543 * Class633.anInt8278;
    }
    
    static final void method13972(Class669 class669, byte i) {
	class669.anInt8600 -= 617999126;
	Class347_Sub3.method15868((class669.anIntArray8595
				   [class669.anInt8600 * 2088438307]),
				  (class669.anIntArray8595
				   [1 + class669.anInt8600 * 2088438307]),
				  0, 1021820546);
    }
}
