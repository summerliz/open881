/* Class554_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Class554_Sub1 extends Class554
{
    static int anInt10665;
    static int anInt10666;
    static boolean aBool10667;
    static int anInt10668;
    public static int anInt10669;
    public static int anInt10670;
    public static int anInt10671;
    static int anInt10672;
    static int anInt10673;
    static boolean aBool10674;
    static boolean aBool10675;
    static boolean aBool10676;
    static boolean aBool10677;
    static Class171[][] aClass171ArrayArray10678;
    static Class16[][] aClass16ArrayArray10679;
    static int anInt10680;
    static HashMap aHashMap10681;
    static HashMap aHashMap10682;
    static int anInt10683 = 0;
    static int anInt10684;
    public static boolean aBool10685;
    public static Class9 aClass9_10686;
    public static Class9 aClass9_10687;
    static Class710 aClass710_10688;
    public static boolean aBool10689;
    public static boolean aBool10690;
    static int anInt10691;
    static int anInt10692;
    public static boolean aBool10693;
    static boolean aBool10694;
    
    static void method16319(Class185 class185, Class534_Sub29 class534_sub29,
			    Class272 class272, int i, int i_0_, int i_1_,
			    int i_2_) {
	int i_3_ = i - 5;
	int i_4_ = i_0_ + 2;
	if (class272.anInt2976 * 1313711519 != 0)
	    class185.method3292(i_3_, i_4_, 10 + i_2_,
				1 + (i_0_ + i_1_ - i_4_),
				class272.anInt2976 * 1313711519, -2056707568);
	if (class272.anInt3009 * -2045123201 != 0)
	    class185.method3425(i_3_, i_4_, i_2_ + 10,
				1 + (i_1_ + i_0_ - i_4_),
				class272.anInt3009 * -2045123201, -378133539);
	int i_5_ = class272.anInt2970 * -1512587879;
	if (class534_sub29.aBool10656 && -1 != 261637687 * class272.anInt2995)
	    i_5_ = 261637687 * class272.anInt2995;
	Class582.aClass171_7771.method2844(class272.aString2969, i, i_0_, i_2_,
					   i_1_, ~0xffffff | i_5_,
					   (aClass622_7353.anInt8145
					    * 335392643),
					   1, 0, 0, null, null, null, 0, 0,
					   202025040);
    }
    
    static {
	anInt10666 = 752461685;
	anInt10692 = -401515070;
	anInt10684 = -1577514632;
	anInt10669 = 1887617585;
	anInt10670 = 1557660430;
	anInt10671 = 0;
	anInt10672 = 1421361991;
	anInt10673 = -2017528667;
	aBool10674 = false;
	anInt10668 = -2119321151;
	anInt10680 = -177934039;
	aBool10677 = false;
	aClass171ArrayArray10678 = new Class171[3][5];
	aClass16ArrayArray10679 = new Class16[3][5];
	aHashMap10682 = new HashMap();
	aHashMap10681 = new HashMap();
	anInt10665 = anInt10669 * -1926359745;
	anInt10691 = anInt10670 * -1435674875;
	aBool10694 = false;
	aBool10685 = false;
	aClass9_10686 = new Class9(8);
	aClass9_10687 = new Class9(8);
	aClass710_10688 = new Class710(new Class700());
	aBool10689 = false;
	aBool10690 = false;
	aBool10693 = false;
	aBool10667 = false;
	aBool10676 = false;
	aBool10675 = false;
    }
    
    static final void method16320(Class185 class185, Class177 class177) {
	if (anInt10671 * -1857977261 != 100
	    && aClass534_Sub18_Sub9_7354 != null) {
	    Class250.method4604((byte) -26);
	    Class250.method4604((byte) -111);
	    if (-1857977261 * anInt10671 < 10) {
		for (int i = 0; i < aClass171ArrayArray10678.length; i++) {
		    for (int i_6_ = 0;
			 i_6_ < aClass171ArrayArray10678[i].length; i_6_++) {
			Class464.aClass472_5113.method7670
			    (aClass622_7353.anIntArrayArray8146[i][i_6_],
			     (byte) -94);
			Class606.aClass472_7988.method7670
			    (aClass622_7353.anIntArrayArray8146[i][i_6_],
			     (byte) -33);
		    }
		}
		if (!aClass472_7365.method7705((aClass534_Sub18_Sub9_7354
						.aString11765),
					       -2031341115)) {
		    anInt10671 = (Class74.aClass472_800.method7667
				  (aClass534_Sub18_Sub9_7354.aString11765,
				   -520981513)) / 10 * -1532796965;
		    return;
		}
		anInt10671 = 1851899534;
	    }
	    if (10 == -1857977261 * anInt10671) {
		anInt7368 = (-192382841 * aClass534_Sub18_Sub9_7354.anInt11763
			     >> 6 << 6);
		anInt7369 = (aClass534_Sub18_Sub9_7354.anInt11771 * -1004159431
			     >> 6 << 6);
		anInt7370
		    = 64 + ((aClass534_Sub18_Sub9_7354.anInt11770 * 36458189
			     >> 6 << 6)
			    - anInt7368);
		anInt7371 = (189160645 * aClass534_Sub18_Sub9_7354.anInt11772
			     >> 6 << 6) - anInt7369 + 64;
		int[] is = new int[3];
		int i = -1;
		int i_7_ = -1;
		Class438 class438
		    = (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419.method10807
		       ().aClass438_4885);
		Class597 class597
		    = client.aClass512_11100.method8416((byte) 126);
		if (aClass534_Sub18_Sub9_7354.method18260
		    (Class322.aClass654_Sub1_Sub5_Sub1_Sub2_3419.aByte10854,
		     (class597.anInt7859 * -424199969
		      + ((int) class438.aFloat4864 >> 9)),
		     (((int) class438.aFloat4865 >> 9)
		      + -1166289421 * class597.anInt7861),
		     is, -257932934)) {
		    i = is[1] - anInt7368;
		    i_7_ = is[2] - anInt7369;
		}
		if (!aBool10677 && i >= 0 && i < anInt7370 && i_7_ >= 0
		    && i_7_ < anInt7371) {
		    i += (int) (Math.random() * 10.0) - 5;
		    i_7_ += (int) (Math.random() * 10.0) - 5;
		    Class151.anInt1705 = 704227181 * i;
		    Class328.anInt3479 = i_7_ * 980626489;
		} else if (-1404917313 * anInt10668 != -1
			   && -1 != -2064661273 * anInt10680) {
		    aClass534_Sub18_Sub9_7354.method18263((anInt10668
							   * -1404917313),
							  (anInt10680
							   * -2064661273),
							  is, 1716777905);
		    if (is != null) {
			Class151.anInt1705 = (is[1] - anInt7368) * 704227181;
			Class328.anInt3479 = 980626489 * (is[2] - anInt7369);
		    }
		    anInt10680 = -177934039;
		    anInt10668 = -2119321151;
		    aBool10677 = false;
		} else {
		    aClass534_Sub18_Sub9_7354.method18263
			((aClass534_Sub18_Sub9_7354.anInt11769 * -1320299993
			  >> 14) & 0x3fff,
			 (-1320299993 * aClass534_Sub18_Sub9_7354.anInt11769
			  & 0x3fff),
			 is, 1320260487);
		    Class151.anInt1705 = 704227181 * (is[1] - anInt7368);
		    Class328.anInt3479 = (is[2] - anInt7369) * 980626489;
		}
		if (25 == aClass534_Sub18_Sub9_7354.anInt11768 * 646871815) {
		    aFloat7364 = 2.0F;
		    aFloat7409 = 2.0F;
		} else if (37 == (aClass534_Sub18_Sub9_7354.anInt11768
				  * 646871815)) {
		    aFloat7364 = 3.0F;
		    aFloat7409 = 3.0F;
		} else if (50 == (aClass534_Sub18_Sub9_7354.anInt11768
				  * 646871815)) {
		    aFloat7364 = 4.0F;
		    aFloat7409 = 4.0F;
		} else if (75 == (aClass534_Sub18_Sub9_7354.anInt11768
				  * 646871815)) {
		    aFloat7364 = 6.0F;
		    aFloat7409 = 6.0F;
		} else if (aClass534_Sub18_Sub9_7354.anInt11768 * 646871815
			   == 100) {
		    aFloat7364 = 8.0F;
		    aFloat7409 = 8.0F;
		} else if (aClass534_Sub18_Sub9_7354.anInt11768 * 646871815
			   == 200) {
		    aFloat7364 = 16.0F;
		    aFloat7409 = 16.0F;
		} else {
		    aFloat7364 = 8.0F;
		    aFloat7409 = 8.0F;
		}
		anInt7394 = (int) aFloat7409 >> 1;
		aByteArrayArrayArray7388
		    = Class200_Sub18.method15632(anInt7394, -1270047487);
		Class612.method10108(65280);
		method9109();
		Class615.aClass700_8055 = new Class700();
		anInt7358 += (int) (Math.random() * 5.0) - 2;
		if (anInt7358 < -8)
		    anInt7358 = -8;
		if (anInt7358 > 8)
		    anInt7358 = 8;
		anInt7359 += (int) (Math.random() * 5.0) - 2;
		if (anInt7359 < -16)
		    anInt7359 = -16;
		if (anInt7359 > 16)
		    anInt7359 = 16;
		method9116(class177, anInt7358 >> 2 << 10, anInt7359 >> 1);
		aClass44_Sub9_7349.method17322(1024, 256, 2042262591);
		aClass44_Sub18_7416.method17359(256, 256, 2146642220);
		aClass44_Sub13_7348.method1079(4096, 1379809429);
		Class84.aClass44_Sub11_840.method1079(256, -2116643552);
		anInt10671 = -591168228;
	    } else if (anInt10671 * -1857977261 == 20) {
		if (aBool7355) {
		    if (method9119(class185, anInt7358, anInt7359, aBool7355))
			anInt10671 = -1773504684;
		} else {
		    Class184.method3220(true, (byte) -121);
		    method9119(class185, anInt7358, anInt7359, aBool7355);
		    anInt10671 = -1773504684;
		    Class184.method3220(true, (byte) -12);
		    Class563.method9513(-469937648);
		}
	    } else if (anInt10671 * -1857977261 == 60) {
		if (aClass472_7365.method7685((aClass534_Sub18_Sub9_7354
					       .aString11765),
					      (short) -753)) {
		    if (!aClass472_7365.method7705((aClass534_Sub18_Sub9_7354
						    .aString11765),
						   -1239231793))
			return;
		    aClass523_7366
			= Class369.method6378(aClass472_7365,
					      (aClass534_Sub18_Sub9_7354
					       .aString11765),
					      client.aBool11157, (byte) -126);
		} else
		    aClass523_7366 = new Class523(0);
		method9123();
		anInt10671 = 78394850;
		Class184.method3220(true, (byte) -68);
		Class563.method9513(1516263396);
	    } else if (-1857977261 * anInt10671 >= 70) {
		for (int i = 0; i < 3; i++) {
		    for (int i_8_ = 0; i_8_ < 5; i_8_++) {
			if (null == aClass171ArrayArray10678[i][i_8_]
			    || aClass16ArrayArray10679[i][i_8_] == null) {
			    aClass171ArrayArray10678[i][i_8_]
				= (Class171) (Class351.aClass406_3620
						  .method6650
					      (client.anInterface52_11081,
					       (aClass622_7353
						.anIntArrayArray8146[i][i_8_]),
					       true, true, (byte) 0));
			    aClass16ArrayArray10679[i][i_8_]
				= (Class351.aClass406_3620.method6666
				   (client.anInterface52_11081,
				    (aClass622_7353.anIntArrayArray8146[i]
				     [i_8_]),
				    (byte) 52));
			    if (null != aClass171ArrayArray10678[i][i_8_]
				&& null != aClass16ArrayArray10679[i][i_8_])
				anInt10671 += -303423599;
			    else
				return;
			}
		    }
		}
		anInt10671 = 1339126156;
		System.gc();
	    }
	}
    }
    
    public static int method16321() {
	if (2.0 == (double) aFloat7409)
	    return 25;
	if ((double) aFloat7409 == 3.0)
	    return 37;
	if ((double) aFloat7409 == 4.0)
	    return 50;
	if ((double) aFloat7409 == 6.0)
	    return 75;
	if (8.0 == (double) aFloat7409)
	    return 100;
	return 200;
    }
    
    public static boolean method16322(int i, boolean bool) {
	if (i == anInt10683 * -363511917)
	    aBool10675 = bool;
	else if (i == 1272099037 * anInt10666)
	    aBool10667 = bool;
	else if (-1565977311 * anInt10692 == i)
	    aBool10676 = bool;
	else
	    return false;
	return true;
    }
    
    static void method16323() {
	Class615.aClass700_8055 = null;
	anInt10671 = 0;
	anInt7399 = 0;
	Class237.aClass247_2377 = null;
	method9117();
	aClass700_7413.method14152(-1239740258);
	aClass523_7366 = null;
	aClass203_7392.method3877(1938352823);
	aClass203_7393.method3877(481939723);
	Class204.aClass163_2205 = null;
	anInt10672 = 1421361991;
	anInt10673 = -2017528667;
	if (null != aClass44_Sub9_7349) {
	    aClass44_Sub9_7349.method1080(65280);
	    aClass44_Sub9_7349.method17322(128, 64, 1936862797);
	}
	if (null != aClass44_Sub18_7416)
	    aClass44_Sub18_7416.method17359(64, 64, 2099092474);
	if (aClass44_Sub13_7348 != null)
	    aClass44_Sub13_7348.method1079(256, 1032452626);
	Class84.aClass44_Sub11_840.method1079(64, 470427657);
    }
    
    static void method16324() {
	Class615.aClass700_8055 = null;
	anInt10671 = 0;
	anInt7399 = 0;
	Class237.aClass247_2377 = null;
	method9117();
	aClass700_7413.method14152(-1504399344);
	aClass523_7366 = null;
	aClass203_7392.method3877(-1152920793);
	aClass203_7393.method3877(-462138896);
	Class204.aClass163_2205 = null;
	anInt10672 = 1421361991;
	anInt10673 = -2017528667;
	if (null != aClass44_Sub9_7349) {
	    aClass44_Sub9_7349.method1080(65280);
	    aClass44_Sub9_7349.method17322(128, 64, 1630997597);
	}
	if (null != aClass44_Sub18_7416)
	    aClass44_Sub18_7416.method17359(64, 64, 2059421698);
	if (aClass44_Sub13_7348 != null)
	    aClass44_Sub13_7348.method1079(256, -1524035882);
	Class84.aClass44_Sub11_840.method1079(64, 1858727535);
    }
    
    static void method16325() {
	Class615.aClass700_8055 = null;
	anInt10671 = 0;
	anInt7399 = 0;
	Class237.aClass247_2377 = null;
	method9117();
	aClass700_7413.method14152(-1128442162);
	aClass523_7366 = null;
	aClass203_7392.method3877(1988907314);
	aClass203_7393.method3877(-1671233343);
	Class204.aClass163_2205 = null;
	anInt10672 = 1421361991;
	anInt10673 = -2017528667;
	if (null != aClass44_Sub9_7349) {
	    aClass44_Sub9_7349.method1080(65280);
	    aClass44_Sub9_7349.method17322(128, 64, 1699640609);
	}
	if (null != aClass44_Sub18_7416)
	    aClass44_Sub18_7416.method17359(64, 64, 2064425700);
	if (aClass44_Sub13_7348 != null)
	    aClass44_Sub13_7348.method1079(256, 360807699);
	Class84.aClass44_Sub11_840.method1079(64, 1672646942);
    }
    
    public static void method16326(int i) {
	if (i == 25)
	    aFloat7409 = 2.0F;
	else if (37 == i)
	    aFloat7409 = 3.0F;
	else if (50 == i)
	    aFloat7409 = 4.0F;
	else if (75 == i)
	    aFloat7409 = 6.0F;
	else if (i == 100)
	    aFloat7409 = 8.0F;
	else if (i == 200)
	    aFloat7409 = 16.0F;
	anInt10673 = -2017528667;
	anInt10673 = -2017528667;
    }
    
    static void method16327() {
	Class176.method2929(-2024655746);
	aClass534_Sub18_Sub9_7354 = null;
	Class519.aClass534_Sub18_Sub9_7057 = null;
	aClass9_10686.method578((byte) 99);
	aClass9_10687.method578((byte) 0);
	for (int i = 0; i < 3; i++) {
	    for (int i_9_ = 0; i_9_ < 5; i_9_++) {
		aClass171ArrayArray10678[i][i_9_] = null;
		aClass16ArrayArray10679[i][i_9_] = null;
	    }
	}
    }
    
    static void method16328() {
	if (204700261 * Class151.anInt1705 < 0) {
	    Class151.anInt1705 = 0;
	    anInt10672 = 1421361991;
	    anInt10673 = -2017528667;
	}
	if (204700261 * Class151.anInt1705 > anInt7370) {
	    Class151.anInt1705 = 704227181 * anInt7370;
	    anInt10672 = 1421361991;
	    anInt10673 = -2017528667;
	}
	if (-1636630007 * Class328.anInt3479 < 0) {
	    Class328.anInt3479 = 0;
	    anInt10672 = 1421361991;
	    anInt10673 = -2017528667;
	}
	if (-1636630007 * Class328.anInt3479 > anInt7371) {
	    Class328.anInt3479 = anInt7371 * 980626489;
	    anInt10672 = 1421361991;
	    anInt10673 = -2017528667;
	}
    }
    
    static boolean method16329(Class272 class272) {
	if (class272 == null)
	    return false;
	if (null != class272.anIntArray2981) {
	    class272 = class272.method5052(anInterface18_7352,
					   anInterface20_7351, 1120623436);
	    if (class272 == null)
		return false;
	}
	if (!class272.aBool2978)
	    return false;
	if (!class272.method5063(anInterface18_7352, anInterface20_7351,
				 (short) 12415))
	    return false;
	if (aClass9_10686.method579((long) (1390042623 * class272.anInt2966))
	    != null)
	    return false;
	if (aClass9_10687.method579((long) (class272.anInt2977 * -15128681))
	    != null)
	    return false;
	if (class272.aString2969 != null) {
	    if (638004337 * class272.anInt2985 == 0 && aBool10689)
		return false;
	    if (class272.anInt2985 * 638004337 == 1 && aBool10690)
		return false;
	    if (2 == class272.anInt2985 * 638004337 && aBool10693)
		return false;
	}
	return true;
    }
    
    public static void method16330() {
	Class98.method1837(true, -1682978959);
    }
    
    static void method16331(Class185 class185, Class534_Sub29 class534_sub29,
			    Class272 class272) {
	Class163 class163 = class272.method5058(class185, (byte) 10);
	if (class163 != null) {
	    int i = class163.method2647();
	    if (class163.method2649() > i)
		i = class163.method2649();
	    int i_10_ = 10;
	    int i_11_ = 2139882933 * class534_sub29.anInt10654;
	    int i_12_ = class534_sub29.anInt10651 * 917865515;
	    int i_13_ = 0;
	    int i_14_ = 0;
	    if (null != class272.aString2969) {
		i_14_ = Class236.aClass16_2373.method737(class272.aString2969,
							 (1771907305
							  * (Class706_Sub4
							     .anInt10994)),
							 0, null, 2112849964);
		i_13_ = Class236.aClass16_2373.method747(class272.aString2969,
							 ((Class706_Sub4
							   .anInt10994)
							  * 1771907305),
							 null, 580667308);
	    }
	    int i_15_ = 2139882933 * class534_sub29.anInt10654 + i / 2;
	    int i_16_ = class534_sub29.anInt10651 * 917865515;
	    if (i_11_ < anInt7410 + i) {
		i_11_ = anInt7410;
		i_15_ = 5 + (i_10_ + (anInt7410 + i / 2) + i_13_ / 2);
	    } else if (i_11_ > anInt7412 - i) {
		i_11_ = anInt7412 - i;
		i_15_ = anInt7412 - i / 2 - i_10_ - i_13_ / 2 - 5;
	    }
	    if (i_12_ < anInt7411 + i) {
		i_12_ = anInt7411;
		i_16_ = i / 2 + (anInt7411 + i_10_);
	    } else if (i_12_ > anInt7378 - i) {
		i_12_ = anInt7378 - i;
		i_16_ = anInt7378 - i / 2 - i_10_ - i_14_;
	    }
	    int i_17_
		= ((int) (Math.atan2((double) (i_11_
					       - (class534_sub29.anInt10654
						  * 2139882933)),
				     (double) (i_12_
					       - (class534_sub29.anInt10651
						  * 917865515)))
			  / 3.141592653589793 * 32767.0)
		   & 0xffff);
	    class163.method2665((float) i / 2.0F + (float) i_11_,
				(float) i / 2.0F + (float) i_12_, 4096, i_17_);
	    int i_18_ = -2;
	    int i_19_ = -2;
	    int i_20_ = -2;
	    int i_21_ = -2;
	    if (class272.aString2969 != null) {
		i_18_ = i_15_ - i_13_ / 2 - 5;
		i_19_ = i_16_;
		i_20_ = i_13_ + i_18_ + 10;
		i_21_ = i_19_ + i_14_ + 3;
		if (1313711519 * class272.anInt2976 != 0)
		    class185.method3292(i_18_, i_19_, i_20_ - i_18_,
					i_21_ - i_19_,
					1313711519 * class272.anInt2976,
					-2093094712);
		if (0 != class272.anInt3009 * -2045123201)
		    class185.method3425(i_18_, i_19_, i_20_ - i_18_,
					i_21_ - i_19_,
					-2045123201 * class272.anInt3009,
					-166439740);
		Class582.aClass171_7771.method2844
		    (class272.aString2969, i_15_, i_16_, i_13_, i_14_,
		     ~0xffffff | class272.anInt2970 * -1512587879,
		     335392643 * aClass622_7353.anInt8145, 1, 0, 0, null, null,
		     null, 0, 0, 202025040);
	    }
	    if (1747122653 * class272.anInt2967 != -1
		|| class272.aString2969 != null) {
		Class534_Sub20 class534_sub20
		    = new Class534_Sub20(class534_sub29);
		class534_sub20.anInt10520 = -773567207 * (i_11_ - i / 2);
		class534_sub20.anInt10522 = (i_11_ + i / 2) * -1946346005;
		class534_sub20.anInt10519 = (i_12_ - i) * -1190860309;
		class534_sub20.anInt10518 = i_12_ * -1010377381;
		class534_sub20.anInt10521 = i_18_ * 869904827;
		class534_sub20.anInt10517 = -536426739 * i_20_;
		class534_sub20.anInt10523 = -2069055837 * i_19_;
		class534_sub20.anInt10524 = -1973105707 * i_21_;
		Class615.aClass700_8055.method14131(class534_sub20,
						    (short) 789);
	    }
	}
    }
    
    static void method16332() {
	Class176.method2929(-2024655746);
	aClass534_Sub18_Sub9_7354 = null;
	Class519.aClass534_Sub18_Sub9_7057 = null;
	aClass9_10686.method578((byte) 17);
	aClass9_10687.method578((byte) 27);
	for (int i = 0; i < 3; i++) {
	    for (int i_22_ = 0; i_22_ < 5; i_22_++) {
		aClass171ArrayArray10678[i][i_22_] = null;
		aClass16ArrayArray10679[i][i_22_] = null;
	    }
	}
    }
    
    static void method16333(Class185 class185, Class700 class700, int i,
			    int i_23_) {
	Class615.aClass700_8055.method14152(-2062559676);
	if (!aBool10685) {
	    for (Class534_Sub29 class534_sub29
		     = (Class534_Sub29) class700.method14135((byte) -1);
		 class534_sub29 != null;
		 class534_sub29
		     = (Class534_Sub29) class700.method14139(658061088)) {
		Class272 class272
		    = ((Class272)
		       aClass44_Sub9_7349.method91((class534_sub29.anInt10652
						    * 1592054281),
						   -997765420));
		if (Class231.method4211(class272, -1488345619)) {
		    boolean bool
			= Class291.method5297(class185, class534_sub29,
					      class272, i, i_23_, 1164456508);
		    if (bool)
			Class595.method9915(class185, class534_sub29, class272,
					    416132176);
		}
	    }
	}
    }
    
    static boolean method16334(Class272 class272) {
	if (class272 == null)
	    return false;
	if (null != class272.anIntArray2981) {
	    class272 = class272.method5052(anInterface18_7352,
					   anInterface20_7351, -1494084314);
	    if (class272 == null)
		return false;
	}
	if (!class272.aBool2978)
	    return false;
	if (!class272.method5063(anInterface18_7352, anInterface20_7351,
				 (short) 13939))
	    return false;
	if (aClass9_10686.method579((long) (1390042623 * class272.anInt2966))
	    != null)
	    return false;
	if (aClass9_10687.method579((long) (class272.anInt2977 * -15128681))
	    != null)
	    return false;
	if (class272.aString2969 != null) {
	    if (638004337 * class272.anInt2985 == 0 && aBool10689)
		return false;
	    if (class272.anInt2985 * 638004337 == 1 && aBool10690)
		return false;
	    if (2 == class272.anInt2985 * 638004337 && aBool10693)
		return false;
	}
	return true;
    }
    
    static void method16335(Class185 class185, int i, int i_24_, int i_25_,
			    int i_26_) {
	class185.method3373(i, i_24_, i + i_25_, i_24_ + i_26_);
	if (-1857977261 * anInt10671 < 100) {
	    int i_27_ = 20;
	    int i_28_ = i + i_25_ / 2;
	    int i_29_ = i_24_ + i_26_ / 2 - 18 - i_27_;
	    class185.method3298(i, i_24_, i_25_, i_26_, -16777216, 0);
	    class185.method3297(i_28_ - 152, i_29_, 304, 34,
				client.aColorArray11072
				    [client.anInt11021 * -859311063].getRGB(),
				0);
	    class185.method3298(i_28_ - 150, 2 + i_29_,
				-1278964487 * anInt10671, 30,
				client.aColorArray11071
				    [client.anInt11021 * -859311063].getRGB(),
				0);
	    Class231.aClass171_2325.method2830
		(Class58.aClass58_620.method1245(Class539.aClass672_7171,
						 (byte) -61),
		 i_28_, i_29_ + i_27_,
		 client.aColorArray11073[-859311063 * client.anInt11021]
		     .getRGB(),
		 -1, 65535);
	} else {
	    int i_30_ = (Class151.anInt1705 * 204700261
			 - (int) ((float) i_25_ / aFloat7364));
	    int i_31_ = (Class328.anInt3479 * -1636630007
			 + (int) ((float) i_26_ / aFloat7364));
	    int i_32_ = (204700261 * Class151.anInt1705
			 + (int) ((float) i_25_ / aFloat7364));
	    int i_33_ = (-1636630007 * Class328.anInt3479
			 - (int) ((float) i_26_ / aFloat7364));
	    Class662.anInt8553
		= (204700261 * Class151.anInt1705
		   - (int) ((float) i_25_ / aFloat7364)) * 1062791895;
	    Class347.anInt3590
		= (Class328.anInt3479 * -1636630007
		   - (int) ((float) i_26_ / aFloat7364)) * -304732319;
	    Class550.anInt7307
		= (int) ((float) (i_25_ * 2) / aFloat7364) * 2055893919;
	    Class567.anInt7595
		= -1977075073 * (int) ((float) (i_26_ * 2) / aFloat7364);
	    aClass203_7375 = aClass203_7392;
	    method9124(i_30_ + anInt7368, anInt7369 + i_31_, i_32_ + anInt7368,
		       anInt7369 + i_33_, i, i_24_, i_25_ + i,
		       1 + (i_26_ + i_24_));
	    method9179(class185, !aBool10667, !aBool10676, client.aBool11157,
		       false);
	    Class700 class700 = method9132(class185);
	    Class347_Sub3.method15869(class185, class700, 0, 0, (byte) -121);
	    if (client.aBool11262) {
		int i_34_ = i + i_25_ - 5;
		int i_35_ = i_24_ + i_26_ - 8;
		Class539_Sub1.aClass171_10327.method2829
		    (new StringBuilder().append("Fps: ").append
			 (Class498.anInt5554 * 1720947399).append
			 (" (").append
			 (Class498.anInt5555 * -482787859).append
			 (" ms)").toString(),
		     i_34_, i_35_, 16776960, -1, -838817863);
		i_35_ -= 15;
		Runtime runtime = Runtime.getRuntime();
		int i_36_
		    = (int) ((runtime.totalMemory() - runtime.freeMemory())
			     / 1024L);
		int i_37_ = 16776960;
		if (i_36_ > 65536)
		    i_37_ = 16711680;
		Class539_Sub1.aClass171_10327.method2829(new StringBuilder
							     ().append
							     ("Mem:").append
							     (i_36_).append
							     ("k").toString(),
							 i_34_, i_35_, i_37_,
							 -1, -1884723386);
		i_35_ -= 15;
	    }
	    aClass203_7392.method3876(5, (byte) 0);
	}
    }
    
    static void method16336() {
	Class176.method2929(-2024655746);
	aClass534_Sub18_Sub9_7354 = null;
	Class519.aClass534_Sub18_Sub9_7057 = null;
	aClass9_10686.method578((byte) 26);
	aClass9_10687.method578((byte) -46);
	for (int i = 0; i < 3; i++) {
	    for (int i_38_ = 0; i_38_ < 5; i_38_++) {
		aClass171ArrayArray10678[i][i_38_] = null;
		aClass16ArrayArray10679[i][i_38_] = null;
	    }
	}
    }
    
    public static void method16337(int i) {
	if (i < 1)
	    anInt10665 = -1926359745 * anInt10669;
	else
	    anInt10665 = i * 1856193797;
    }
    
    static void method16338(int i) {
	int i_39_;
	if (i == 0)
	    i_39_ = 0;
	else if (1 == i)
	    i_39_ = 1;
	else if (i == 2)
	    i_39_ = 2;
	else
	    return;
	int i_40_;
	if ((double) aFloat7409 == 2.0)
	    i_40_ = 0;
	else if ((double) aFloat7409 == 3.0)
	    i_40_ = 1;
	else if ((double) aFloat7409 == 4.0)
	    i_40_ = 2;
	else if ((double) aFloat7409 == 6.0)
	    i_40_ = 3;
	else if ((double) aFloat7409 >= 8.0)
	    i_40_ = 4;
	else
	    return;
	Class582.aClass171_7771 = aClass171ArrayArray10678[i_39_][i_40_];
	Class236.aClass16_2373 = aClass16ArrayArray10679[i_39_][i_40_];
    }
    
    public static void method16339(int i, int i_41_) {
	Class151.anInt1705 = (i - anInt7368) * 704227181;
	Class328.anInt3479 = 980626489 * (i_41_ - anInt7369);
	anInt10672 = 1421361991;
	anInt10673 = -2017528667;
	Class612.method10108(65280);
    }
    
    Class554_Sub1() throws Throwable {
	throw new Error();
    }
    
    static void method16340(Class185 class185, int i, int i_42_, int i_43_,
			    int i_44_) {
	class185.method3373(i, i_42_, i + i_43_, i_42_ + i_44_);
	if (-1857977261 * anInt10671 < 100) {
	    int i_45_ = 20;
	    int i_46_ = i + i_43_ / 2;
	    int i_47_ = i_42_ + i_44_ / 2 - 18 - i_45_;
	    class185.method3298(i, i_42_, i_43_, i_44_, -16777216, 0);
	    class185.method3297(i_46_ - 152, i_47_, 304, 34,
				client.aColorArray11072
				    [client.anInt11021 * -859311063].getRGB(),
				0);
	    class185.method3298(i_46_ - 150, 2 + i_47_,
				-1278964487 * anInt10671, 30,
				client.aColorArray11071
				    [client.anInt11021 * -859311063].getRGB(),
				0);
	    Class231.aClass171_2325.method2830
		(Class58.aClass58_620.method1245(Class539.aClass672_7171,
						 (byte) -55),
		 i_46_, i_47_ + i_45_,
		 client.aColorArray11073[-859311063 * client.anInt11021]
		     .getRGB(),
		 -1, 65535);
	} else {
	    int i_48_ = (Class151.anInt1705 * 204700261
			 - (int) ((float) i_43_ / aFloat7364));
	    int i_49_ = (Class328.anInt3479 * -1636630007
			 + (int) ((float) i_44_ / aFloat7364));
	    int i_50_ = (204700261 * Class151.anInt1705
			 + (int) ((float) i_43_ / aFloat7364));
	    int i_51_ = (-1636630007 * Class328.anInt3479
			 - (int) ((float) i_44_ / aFloat7364));
	    Class662.anInt8553
		= (204700261 * Class151.anInt1705
		   - (int) ((float) i_43_ / aFloat7364)) * 1062791895;
	    Class347.anInt3590
		= (Class328.anInt3479 * -1636630007
		   - (int) ((float) i_44_ / aFloat7364)) * -304732319;
	    Class550.anInt7307
		= (int) ((float) (i_43_ * 2) / aFloat7364) * 2055893919;
	    Class567.anInt7595
		= -1977075073 * (int) ((float) (i_44_ * 2) / aFloat7364);
	    aClass203_7375 = aClass203_7392;
	    method9124(i_48_ + anInt7368, anInt7369 + i_49_, i_50_ + anInt7368,
		       anInt7369 + i_51_, i, i_42_, i_43_ + i,
		       1 + (i_44_ + i_42_));
	    method9179(class185, !aBool10667, !aBool10676, client.aBool11157,
		       false);
	    Class700 class700 = method9132(class185);
	    Class347_Sub3.method15869(class185, class700, 0, 0, (byte) -99);
	    if (client.aBool11262) {
		int i_52_ = i + i_43_ - 5;
		int i_53_ = i_42_ + i_44_ - 8;
		Class539_Sub1.aClass171_10327.method2829
		    (new StringBuilder().append("Fps: ").append
			 (Class498.anInt5554 * 1720947399).append
			 (" (").append
			 (Class498.anInt5555 * -482787859).append
			 (" ms)").toString(),
		     i_52_, i_53_, 16776960, -1, 1444187527);
		i_53_ -= 15;
		Runtime runtime = Runtime.getRuntime();
		int i_54_
		    = (int) ((runtime.totalMemory() - runtime.freeMemory())
			     / 1024L);
		int i_55_ = 16776960;
		if (i_54_ > 65536)
		    i_55_ = 16711680;
		Class539_Sub1.aClass171_10327.method2829(new StringBuilder
							     ().append
							     ("Mem:").append
							     (i_54_).append
							     ("k").toString(),
							 i_52_, i_53_, i_55_,
							 -1, -1756491187);
		i_53_ -= 15;
	    }
	    aClass203_7392.method3876(5, (byte) 0);
	}
    }
    
    static final void method16341(Class185 class185, int i, int i_56_,
				  int i_57_, int i_58_) {
	class185.method3373(i, i_56_, i_57_ + i, i_58_ + i_56_);
	class185.method3292(i, i_56_, i_57_, i_58_, -16777216, -1834018784);
	if (anInt10671 * -1857977261 >= 100) {
	    float f = (float) anInt7371 / (float) anInt7370;
	    int i_59_ = i_57_;
	    int i_60_ = i_58_;
	    if (f < 1.0F)
		i_60_ = (int) (f * (float) i_57_);
	    else
		i_59_ = (int) ((float) i_58_ / f);
	    i += (i_57_ - i_59_) / 2;
	    i_56_ += (i_58_ - i_60_) / 2;
	    if (Class204.aClass163_2205 == null
		|| Class204.aClass163_2205.method2647() != i_59_
		|| Class204.aClass163_2205.method2649() != i_60_) {
		aClass203_7375 = aClass203_7393;
		method9124(anInt7368, anInt7369 + anInt7371,
			   anInt7370 + anInt7368, anInt7369, i, i_56_,
			   i_59_ + i, i_60_ + i_56_);
		method9179(class185, false, false, client.aBool11157, true);
		class185.method3581();
		Class204.aClass163_2205
		    = class185.method3319(i, i_56_, i_59_, i_60_, true);
	    }
	    Class204.aClass163_2205.method2656(i, i_56_);
	    int i_61_ = Class550.anInt7307 * -87431073 * i_59_ / anInt7370;
	    int i_62_ = 114918783 * Class567.anInt7595 * i_60_ / anInt7371;
	    int i_63_
		= i + i_59_ * (Class662.anInt8553 * 313632999) / anInt7370;
	    int i_64_
		= (i_60_ + i_56_
		   - i_60_ * (-1356415839 * Class347.anInt3590) / anInt7371
		   - i_62_);
	    int i_65_ = -1996554240;
	    if (client.aClass675_11016 == Class675.aClass675_8635)
		i_65_ = -1996488705;
	    class185.method3298(i_63_, i_64_, i_61_, i_62_, i_65_, 1);
	    class185.method3297(i_63_, i_64_, i_61_, i_62_, i_65_, 0);
	    for (Class534_Sub29 class534_sub29
		     = (Class534_Sub29) aClass700_7413.method14135((byte) -1);
		 class534_sub29 != null;
		 class534_sub29 = ((Class534_Sub29)
				   aClass700_7413.method14139(1360286976))) {
		Class272 class272
		    = ((Class272)
		       aClass44_Sub9_7349.method91((class534_sub29.anInt10652
						    * 1592054281),
						   -897059972));
		if (Class231.method4211(class272, -1488345619)) {
		    Class25 class25
			= ((Class25)
			   (aHashMap10682.get
			    (Integer.valueOf(1592054281
					     * class534_sub29.anInt10652))));
		    if (class25 == null)
			class25
			    = (Class25) (aHashMap10681.get
					 (Integer.valueOf(class272.anInt2977
							  * -15128681)));
		    if (null != class25) {
			int i_66_;
			if (class25.anInt231 * -959968621
			    > 2135626771 * anInt10691 / 2)
			    i_66_ = ((anInt10691 * -876019987
				      - 21137517 * class25.anInt231)
				     / (anInt10691 * 2135626771));
			else
			    i_66_
				= 21137517 * class25.anInt231 / (2135626771
								 * anInt10691);
			int i_67_ = i + i_59_ * (class534_sub29.anInt10655
						 * 1412374331) / anInt7370;
			int i_68_
			    = ((i_60_
				* (anInt7371
				   - -958626707 * class534_sub29.anInt10653)
				/ anInt7371)
			       + i_56_);
			class185.method3292(i_67_ - 2, i_68_ - 2, 4, 4,
					    i_66_ << 24 | 0xffff00,
					    -2040716056);
		    }
		}
	    }
	    aClass203_7393.method3876(5, (byte) 0);
	}
    }
    
    static final void method16342(int i, int i_69_) {
	if (aFloat7364 < aFloat7409) {
	    aFloat7364 += (double) aFloat7364 / 30.0;
	    if (aFloat7364 > aFloat7409)
		aFloat7364 = aFloat7409;
	    Class612.method10108(65280);
	    anInt7394 = (int) aFloat7409 >> 1;
	    aByteArrayArrayArray7388
		= Class200_Sub18.method15632(anInt7394, -613799221);
	} else if (aFloat7364 > aFloat7409) {
	    aFloat7364 -= (double) aFloat7364 / 30.0;
	    if (aFloat7364 < aFloat7409)
		aFloat7364 = aFloat7409;
	    Class612.method10108(65280);
	    anInt7394 = (int) aFloat7409 >> 1;
	    aByteArrayArrayArray7388
		= Class200_Sub18.method15632(anInt7394, -127567289);
	}
	if (-1 != -620394103 * anInt10672 && -1 != anInt10673 * -128963373) {
	    int i_70_
		= anInt10672 * -620394103 - 204700261 * Class151.anInt1705;
	    if (i_70_ != 0)
		i_70_ /= Math.min(anInt10684 * -1048197681, Math.abs(i_70_));
	    int i_71_
		= -128963373 * anInt10673 - Class328.anInt3479 * -1636630007;
	    if (0 != i_71_)
		i_71_ /= Math.min(-1048197681 * anInt10684, Math.abs(i_71_));
	    Class151.anInt1705
		= (i_70_ + 204700261 * Class151.anInt1705) * 704227181;
	    Class328.anInt3479
		= (-1636630007 * Class328.anInt3479 + i_71_) * 980626489;
	    if (i_70_ == 0 && i_71_ == 0) {
		anInt10672 = 1421361991;
		anInt10673 = -2017528667;
	    }
	    Class612.method10108(65280);
	}
	Iterator iterator = aHashMap10682.entrySet().iterator();
	while (iterator.hasNext()) {
	    Class25 class25
		= (Class25) ((Map.Entry) iterator.next()).getValue();
	    class25.anInt231 -= -1425890405;
	    if (0 == -959968621 * class25.anInt231) {
		if (-132330391 * class25.anInt232 <= 1 && !aBool10694)
		    iterator.remove();
		else {
		    class25.anInt232 -= 1869972441;
		    class25.anInt231 = anInt10691 * -1184677759;
		}
	    }
	}
	iterator = aHashMap10681.entrySet().iterator();
	while (iterator.hasNext()) {
	    Class25 class25
		= (Class25) ((Map.Entry) iterator.next()).getValue();
	    class25.anInt231 -= -1425890405;
	    if (-959968621 * class25.anInt231 == 0) {
		if (class25.anInt232 * -132330391 <= 1 && !aBool10694)
		    iterator.remove();
		else {
		    class25.anInt232 -= 1869972441;
		    class25.anInt231 = anInt10691 * -1184677759;
		}
	    }
	}
	if (aBool10674 && Class615.aClass700_8055 != null) {
	    for (Class534_Sub20 class534_sub20
		     = ((Class534_Sub20)
			Class615.aClass700_8055.method14135((byte) -1));
		 null != class534_sub20;
		 class534_sub20
		     = ((Class534_Sub20)
			Class615.aClass700_8055.method14139(1454257091))) {
		Class272 class272
		    = ((Class272)
		       aClass44_Sub9_7349.method91((class534_sub20
						    .aClass534_Sub29_10525
						    .anInt10652) * 1592054281,
						   -903890884));
		if (class534_sub20.method16192(i, i_69_, (byte) 1)) {
		    if (class272.aStringArray2980 != null) {
			if (null != class272.aStringArray2980[4])
			    Class112.method2016
				(class272.aStringArray2980[4],
				 class272.aString2964, -1, 1012, -1,
				 (long) ((class534_sub20.aClass534_Sub29_10525
					  .anInt10652)
					 * 1592054281),
				 -15128681 * class272.anInt2977, 0, true,
				 false,
				 (long) (1592054281 * (class534_sub20
						       .aClass534_Sub29_10525
						       .anInt10652)),
				 false, (short) 3259);
			if (class272.aStringArray2980[3] != null)
			    Class112.method2016
				(class272.aStringArray2980[3],
				 class272.aString2964, -1, 1011, -1,
				 (long) (1592054281 * (class534_sub20
						       .aClass534_Sub29_10525
						       .anInt10652)),
				 class272.anInt2977 * -15128681, 0, true,
				 false,
				 (long) (1592054281 * (class534_sub20
						       .aClass534_Sub29_10525
						       .anInt10652)),
				 false, (short) 2000);
			if (class272.aStringArray2980[2] != null)
			    Class112.method2016
				(class272.aStringArray2980[2],
				 class272.aString2964, -1, 1010, -1,
				 (long) (1592054281 * (class534_sub20
						       .aClass534_Sub29_10525
						       .anInt10652)),
				 -15128681 * class272.anInt2977, 0, true,
				 false,
				 (long) ((class534_sub20.aClass534_Sub29_10525
					  .anInt10652)
					 * 1592054281),
				 false, (short) 19726);
			if (null != class272.aStringArray2980[1])
			    Class112.method2016(class272.aStringArray2980[1],
						class272.aString2964, -1, 1009,
						-1,
						(long) ((class534_sub20
							 .aClass534_Sub29_10525
							 .anInt10652)
							* 1592054281),
						class272.anInt2977 * -15128681,
						0, true, false,
						(long) ((class534_sub20
							 .aClass534_Sub29_10525
							 .anInt10652)
							* 1592054281),
						false, (short) 27952);
			if (class272.aStringArray2980[0] != null)
			    Class112.method2016
				(class272.aStringArray2980[0],
				 class272.aString2964, -1, 1008, -1,
				 (long) (1592054281 * (class534_sub20
						       .aClass534_Sub29_10525
						       .anInt10652)),
				 -15128681 * class272.anInt2977, 0, true,
				 false,
				 (long) (1592054281 * (class534_sub20
						       .aClass534_Sub29_10525
						       .anInt10652)),
				 false, (short) 3016);
		    }
		    if (!class534_sub20.aClass534_Sub29_10525.aBool10656) {
			class534_sub20.aClass534_Sub29_10525.aBool10656 = true;
			Class680.method13862(Class583.aClass583_7787,
					     (1592054281
					      * (class534_sub20
						 .aClass534_Sub29_10525
						 .anInt10652)),
					     -15128681 * class272.anInt2977,
					     (byte) 91);
		    }
		    if (class534_sub20.aClass534_Sub29_10525.aBool10656)
			Class680.method13862(Class583.aClass583_7779,
					     (class534_sub20
					      .aClass534_Sub29_10525
					      .anInt10652) * 1592054281,
					     class272.anInt2977 * -15128681,
					     (byte) 10);
		} else if (class534_sub20.aClass534_Sub29_10525.aBool10656) {
		    class534_sub20.aClass534_Sub29_10525.aBool10656 = false;
		    Class680.method13862(Class583.aClass583_7778,
					 1592054281 * (class534_sub20
						       .aClass534_Sub29_10525
						       .anInt10652),
					 -15128681 * class272.anInt2977,
					 (byte) 46);
		}
	    }
	}
    }
    
    static final void method16343(int i, int i_72_) {
	if (aFloat7364 < aFloat7409) {
	    aFloat7364 += (double) aFloat7364 / 30.0;
	    if (aFloat7364 > aFloat7409)
		aFloat7364 = aFloat7409;
	    Class612.method10108(65280);
	    anInt7394 = (int) aFloat7409 >> 1;
	    aByteArrayArrayArray7388
		= Class200_Sub18.method15632(anInt7394, 513547750);
	} else if (aFloat7364 > aFloat7409) {
	    aFloat7364 -= (double) aFloat7364 / 30.0;
	    if (aFloat7364 < aFloat7409)
		aFloat7364 = aFloat7409;
	    Class612.method10108(65280);
	    anInt7394 = (int) aFloat7409 >> 1;
	    aByteArrayArrayArray7388
		= Class200_Sub18.method15632(anInt7394, 616847665);
	}
	if (-1 != -620394103 * anInt10672 && -1 != anInt10673 * -128963373) {
	    int i_73_
		= anInt10672 * -620394103 - 204700261 * Class151.anInt1705;
	    if (i_73_ != 0)
		i_73_ /= Math.min(anInt10684 * -1048197681, Math.abs(i_73_));
	    int i_74_
		= -128963373 * anInt10673 - Class328.anInt3479 * -1636630007;
	    if (0 != i_74_)
		i_74_ /= Math.min(-1048197681 * anInt10684, Math.abs(i_74_));
	    Class151.anInt1705
		= (i_73_ + 204700261 * Class151.anInt1705) * 704227181;
	    Class328.anInt3479
		= (-1636630007 * Class328.anInt3479 + i_74_) * 980626489;
	    if (i_73_ == 0 && i_74_ == 0) {
		anInt10672 = 1421361991;
		anInt10673 = -2017528667;
	    }
	    Class612.method10108(65280);
	}
	Iterator iterator = aHashMap10682.entrySet().iterator();
	while (iterator.hasNext()) {
	    Class25 class25
		= (Class25) ((Map.Entry) iterator.next()).getValue();
	    class25.anInt231 -= -1425890405;
	    if (0 == -959968621 * class25.anInt231) {
		if (-132330391 * class25.anInt232 <= 1 && !aBool10694)
		    iterator.remove();
		else {
		    class25.anInt232 -= 1869972441;
		    class25.anInt231 = anInt10691 * -1184677759;
		}
	    }
	}
	iterator = aHashMap10681.entrySet().iterator();
	while (iterator.hasNext()) {
	    Class25 class25
		= (Class25) ((Map.Entry) iterator.next()).getValue();
	    class25.anInt231 -= -1425890405;
	    if (-959968621 * class25.anInt231 == 0) {
		if (class25.anInt232 * -132330391 <= 1 && !aBool10694)
		    iterator.remove();
		else {
		    class25.anInt232 -= 1869972441;
		    class25.anInt231 = anInt10691 * -1184677759;
		}
	    }
	}
	if (aBool10674 && Class615.aClass700_8055 != null) {
	    for (Class534_Sub20 class534_sub20
		     = ((Class534_Sub20)
			Class615.aClass700_8055.method14135((byte) -1));
		 null != class534_sub20;
		 class534_sub20
		     = ((Class534_Sub20)
			Class615.aClass700_8055.method14139(1926759784))) {
		Class272 class272
		    = ((Class272)
		       aClass44_Sub9_7349.method91((class534_sub20
						    .aClass534_Sub29_10525
						    .anInt10652) * 1592054281,
						   1217214815));
		if (class534_sub20.method16192(i, i_72_, (byte) 1)) {
		    if (class272.aStringArray2980 != null) {
			if (null != class272.aStringArray2980[4])
			    Class112.method2016
				(class272.aStringArray2980[4],
				 class272.aString2964, -1, 1012, -1,
				 (long) ((class534_sub20.aClass534_Sub29_10525
					  .anInt10652)
					 * 1592054281),
				 -15128681 * class272.anInt2977, 0, true,
				 false,
				 (long) (1592054281 * (class534_sub20
						       .aClass534_Sub29_10525
						       .anInt10652)),
				 false, (short) 5370);
			if (class272.aStringArray2980[3] != null)
			    Class112.method2016
				(class272.aStringArray2980[3],
				 class272.aString2964, -1, 1011, -1,
				 (long) (1592054281 * (class534_sub20
						       .aClass534_Sub29_10525
						       .anInt10652)),
				 class272.anInt2977 * -15128681, 0, true,
				 false,
				 (long) (1592054281 * (class534_sub20
						       .aClass534_Sub29_10525
						       .anInt10652)),
				 false, (short) 18019);
			if (class272.aStringArray2980[2] != null)
			    Class112.method2016
				(class272.aStringArray2980[2],
				 class272.aString2964, -1, 1010, -1,
				 (long) (1592054281 * (class534_sub20
						       .aClass534_Sub29_10525
						       .anInt10652)),
				 -15128681 * class272.anInt2977, 0, true,
				 false,
				 (long) ((class534_sub20.aClass534_Sub29_10525
					  .anInt10652)
					 * 1592054281),
				 false, (short) 29865);
			if (null != class272.aStringArray2980[1])
			    Class112.method2016(class272.aStringArray2980[1],
						class272.aString2964, -1, 1009,
						-1,
						(long) ((class534_sub20
							 .aClass534_Sub29_10525
							 .anInt10652)
							* 1592054281),
						class272.anInt2977 * -15128681,
						0, true, false,
						(long) ((class534_sub20
							 .aClass534_Sub29_10525
							 .anInt10652)
							* 1592054281),
						false, (short) 26069);
			if (class272.aStringArray2980[0] != null)
			    Class112.method2016
				(class272.aStringArray2980[0],
				 class272.aString2964, -1, 1008, -1,
				 (long) (1592054281 * (class534_sub20
						       .aClass534_Sub29_10525
						       .anInt10652)),
				 -15128681 * class272.anInt2977, 0, true,
				 false,
				 (long) (1592054281 * (class534_sub20
						       .aClass534_Sub29_10525
						       .anInt10652)),
				 false, (short) 22186);
		    }
		    if (!class534_sub20.aClass534_Sub29_10525.aBool10656) {
			class534_sub20.aClass534_Sub29_10525.aBool10656 = true;
			Class680.method13862(Class583.aClass583_7787,
					     (1592054281
					      * (class534_sub20
						 .aClass534_Sub29_10525
						 .anInt10652)),
					     -15128681 * class272.anInt2977,
					     (byte) 122);
		    }
		    if (class534_sub20.aClass534_Sub29_10525.aBool10656)
			Class680.method13862(Class583.aClass583_7779,
					     (class534_sub20
					      .aClass534_Sub29_10525
					      .anInt10652) * 1592054281,
					     class272.anInt2977 * -15128681,
					     (byte) 52);
		} else if (class534_sub20.aClass534_Sub29_10525.aBool10656) {
		    class534_sub20.aClass534_Sub29_10525.aBool10656 = false;
		    Class680.method13862(Class583.aClass583_7778,
					 1592054281 * (class534_sub20
						       .aClass534_Sub29_10525
						       .anInt10652),
					 -15128681 * class272.anInt2977,
					 (byte) 51);
		}
	    }
	}
    }
    
    static void method16344(int i, int i_75_, int i_76_) {
	if (i == 1008)
	    Class680.method13862(Class583.aClass583_7773, i_75_, i_76_,
				 (byte) 100);
	else if (1009 == i)
	    Class680.method13862(Class583.aClass583_7783, i_75_, i_76_,
				 (byte) 11);
	else if (1010 == i)
	    Class680.method13862(Class583.aClass583_7774, i_75_, i_76_,
				 (byte) 50);
	else if (i == 1011)
	    Class680.method13862(Class583.aClass583_7772, i_75_, i_76_,
				 (byte) 22);
	else if (1012 == i)
	    Class680.method13862(Class583.aClass583_7792, i_75_, i_76_,
				 (byte) 118);
    }
    
    static void method16345(int i, int i_77_, int i_78_) {
	if (i == 1008)
	    Class680.method13862(Class583.aClass583_7773, i_77_, i_78_,
				 (byte) 78);
	else if (1009 == i)
	    Class680.method13862(Class583.aClass583_7783, i_77_, i_78_,
				 (byte) 90);
	else if (1010 == i)
	    Class680.method13862(Class583.aClass583_7774, i_77_, i_78_,
				 (byte) 12);
	else if (i == 1011)
	    Class680.method13862(Class583.aClass583_7772, i_77_, i_78_,
				 (byte) 26);
	else if (1012 == i)
	    Class680.method13862(Class583.aClass583_7792, i_77_, i_78_,
				 (byte) 97);
    }
    
    static void method16346(int i, int i_79_, int i_80_) {
	if (i == 1008)
	    Class680.method13862(Class583.aClass583_7773, i_79_, i_80_,
				 (byte) 65);
	else if (1009 == i)
	    Class680.method13862(Class583.aClass583_7783, i_79_, i_80_,
				 (byte) 108);
	else if (1010 == i)
	    Class680.method13862(Class583.aClass583_7774, i_79_, i_80_,
				 (byte) 67);
	else if (i == 1011)
	    Class680.method13862(Class583.aClass583_7772, i_79_, i_80_,
				 (byte) 127);
	else if (1012 == i)
	    Class680.method13862(Class583.aClass583_7792, i_79_, i_80_,
				 (byte) 16);
    }
    
    public static void method16347(int i) {
	if (i < 1)
	    anInt10691 = anInt10670 * -1435674875;
	else
	    anInt10691 = i * 1444927003;
    }
    
    public static void method16348(int i, int i_81_, int i_82_, boolean bool) {
	Class534_Sub18_Sub9 class534_sub18_sub9 = aClass534_Sub18_Sub9_7354;
	method9118(i);
	aBool7355 = false;
	if (aClass534_Sub18_Sub9_7354 != class534_sub18_sub9)
	    Class176.method2929(-2024655746);
	anInt10668 = i_81_ * 2119321151;
	anInt10680 = i_82_ * 177934039;
	aBool10677 = bool;
    }
    
    public static void method16349(int i, int i_83_, int i_84_, boolean bool) {
	Class534_Sub18_Sub9 class534_sub18_sub9 = aClass534_Sub18_Sub9_7354;
	method9118(i);
	aBool7355 = false;
	if (aClass534_Sub18_Sub9_7354 != class534_sub18_sub9)
	    Class176.method2929(-2024655746);
	anInt10668 = i_83_ * 2119321151;
	anInt10680 = i_84_ * 177934039;
	aBool10677 = bool;
    }
    
    public static void method16350() {
	aHashMap10682.clear();
	aHashMap10681.clear();
    }
    
    public static Class534_Sub18_Sub9 method16351() {
	return aClass534_Sub18_Sub9_7354;
    }
    
    static void method16352(int i) {
	Class151.anInt1705 = 704227181 * i;
	anInt10672 = 1421361991;
	anInt10673 = -2017528667;
	Class612.method10108(65280);
    }
    
    static void method16353() {
	Class176.method2929(-2024655746);
	aClass534_Sub18_Sub9_7354 = null;
	Class519.aClass534_Sub18_Sub9_7057 = null;
	aClass9_10686.method578((byte) -25);
	aClass9_10687.method578((byte) -13);
	for (int i = 0; i < 3; i++) {
	    for (int i_85_ = 0; i_85_ < 5; i_85_++) {
		aClass171ArrayArray10678[i][i_85_] = null;
		aClass16ArrayArray10679[i][i_85_] = null;
	    }
	}
    }
    
    static void method16354(int i) {
	Class151.anInt1705 = 704227181 * i;
	anInt10672 = 1421361991;
	anInt10673 = -2017528667;
	Class612.method10108(65280);
    }
    
    public static void method16355(int i, int i_86_, int i_87_, boolean bool) {
	Class534_Sub18_Sub9 class534_sub18_sub9 = aClass534_Sub18_Sub9_7354;
	method9118(i);
	aBool7355 = false;
	if (aClass534_Sub18_Sub9_7354 != class534_sub18_sub9)
	    Class176.method2929(-2024655746);
	anInt10668 = i_86_ * 2119321151;
	anInt10680 = i_87_ * 177934039;
	aBool10677 = bool;
    }
    
    static void method16356(Class185 class185, Class534_Sub29 class534_sub29,
			    Class272 class272, int i, int i_88_, int i_89_,
			    int i_90_) {
	int i_91_ = i - 5;
	int i_92_ = i_88_ + 2;
	if (class272.anInt2976 * 1313711519 != 0)
	    class185.method3292(i_91_, i_92_, 10 + i_90_,
				1 + (i_88_ + i_89_ - i_92_),
				class272.anInt2976 * 1313711519, -1864954665);
	if (class272.anInt3009 * -2045123201 != 0)
	    class185.method3425(i_91_, i_92_, i_90_ + 10,
				1 + (i_89_ + i_88_ - i_92_),
				class272.anInt3009 * -2045123201, -73667214);
	int i_93_ = class272.anInt2970 * -1512587879;
	if (class534_sub29.aBool10656 && -1 != 261637687 * class272.anInt2995)
	    i_93_ = 261637687 * class272.anInt2995;
	Class582.aClass171_7771.method2844(class272.aString2969, i, i_88_,
					   i_90_, i_89_, ~0xffffff | i_93_,
					   (aClass622_7353.anInt8145
					    * 335392643),
					   1, 0, 0, null, null, null, 0, 0,
					   202025040);
    }
    
    public static void method16357(int i) {
	if (i == 25)
	    aFloat7409 = 2.0F;
	else if (37 == i)
	    aFloat7409 = 3.0F;
	else if (50 == i)
	    aFloat7409 = 4.0F;
	else if (75 == i)
	    aFloat7409 = 6.0F;
	else if (i == 100)
	    aFloat7409 = 8.0F;
	else if (i == 200)
	    aFloat7409 = 16.0F;
	anInt10673 = -2017528667;
	anInt10673 = -2017528667;
    }
    
    static void method16358(int i, int i_94_, int i_95_, int i_96_) {
	float f = (float) anInt7371 / (float) anInt7370;
	int i_97_ = i_95_;
	int i_98_ = i_96_;
	if (f < 1.0F)
	    i_98_ = (int) ((float) i_95_ * f);
	else
	    i_97_ = (int) ((float) i_96_ / f);
	i -= (i_95_ - i_97_) / 2;
	i_94_ -= (i_96_ - i_98_) / 2;
	Class151.anInt1705 = 704227181 * (i * anInt7370 / i_97_);
	Class328.anInt3479
	    = 980626489 * (anInt7371 - anInt7371 * i_94_ / i_98_);
	anInt10672 = 1421361991;
	anInt10673 = -2017528667;
	Class612.method10108(65280);
    }
    
    static void method16359(int i, int i_99_, int i_100_, int i_101_) {
	float f = (float) anInt7371 / (float) anInt7370;
	int i_102_ = i_100_;
	int i_103_ = i_101_;
	if (f < 1.0F)
	    i_103_ = (int) ((float) i_100_ * f);
	else
	    i_102_ = (int) ((float) i_101_ / f);
	i -= (i_100_ - i_102_) / 2;
	i_99_ -= (i_101_ - i_103_) / 2;
	Class151.anInt1705 = 704227181 * (i * anInt7370 / i_102_);
	Class328.anInt3479
	    = 980626489 * (anInt7371 - anInt7371 * i_99_ / i_103_);
	anInt10672 = 1421361991;
	anInt10673 = -2017528667;
	Class612.method10108(65280);
    }
    
    public static int method16360() {
	if (2.0 == (double) aFloat7409)
	    return 25;
	if ((double) aFloat7409 == 3.0)
	    return 37;
	if ((double) aFloat7409 == 4.0)
	    return 50;
	if ((double) aFloat7409 == 6.0)
	    return 75;
	if (8.0 == (double) aFloat7409)
	    return 100;
	return 200;
    }
    
    public static int method16361() {
	if (2.0 == (double) aFloat7409)
	    return 25;
	if ((double) aFloat7409 == 3.0)
	    return 37;
	if ((double) aFloat7409 == 4.0)
	    return 50;
	if ((double) aFloat7409 == 6.0)
	    return 75;
	if (8.0 == (double) aFloat7409)
	    return 100;
	return 200;
    }
    
    public static void method16362() {
	aHashMap10682.clear();
	aHashMap10681.clear();
    }
    
    public static void method16363(int i) {
	Class25 class25 = (Class25) aHashMap10681.get(Integer.valueOf(i));
	if (class25 == null)
	    class25 = new Class25();
	class25.anInt232 = anInt10665 * -1551336507;
	class25.anInt231 = -1184677759 * anInt10691;
	aHashMap10681.put(Integer.valueOf(i), class25);
    }
    
    static void method16364() {
	if (204700261 * Class151.anInt1705 < 0) {
	    Class151.anInt1705 = 0;
	    anInt10672 = 1421361991;
	    anInt10673 = -2017528667;
	}
	if (204700261 * Class151.anInt1705 > anInt7370) {
	    Class151.anInt1705 = 704227181 * anInt7370;
	    anInt10672 = 1421361991;
	    anInt10673 = -2017528667;
	}
	if (-1636630007 * Class328.anInt3479 < 0) {
	    Class328.anInt3479 = 0;
	    anInt10672 = 1421361991;
	    anInt10673 = -2017528667;
	}
	if (-1636630007 * Class328.anInt3479 > anInt7371) {
	    Class328.anInt3479 = anInt7371 * 980626489;
	    anInt10672 = 1421361991;
	    anInt10673 = -2017528667;
	}
    }
    
    public static void method16365(int i) {
	if (i < 1)
	    anInt10691 = anInt10670 * -1435674875;
	else
	    anInt10691 = i * 1444927003;
    }
    
    static void method16366() {
	Class176.method2929(-2024655746);
	aClass534_Sub18_Sub9_7354 = null;
	Class519.aClass534_Sub18_Sub9_7057 = null;
	aClass9_10686.method578((byte) -6);
	aClass9_10687.method578((byte) -65);
	for (int i = 0; i < 3; i++) {
	    for (int i_104_ = 0; i_104_ < 5; i_104_++) {
		aClass171ArrayArray10678[i][i_104_] = null;
		aClass16ArrayArray10679[i][i_104_] = null;
	    }
	}
    }
    
    public static void method16367(int i) {
	if (i < 1)
	    anInt10691 = anInt10670 * -1435674875;
	else
	    anInt10691 = i * 1444927003;
    }
    
    static void method16368(Class185 class185, Class534_Sub29 class534_sub29,
			    Class272 class272) {
	Class163 class163 = class272.method5058(class185, (byte) 10);
	if (class163 != null) {
	    int i = class163.method2647();
	    if (class163.method2649() > i)
		i = class163.method2649();
	    int i_105_ = 10;
	    int i_106_ = 2139882933 * class534_sub29.anInt10654;
	    int i_107_ = class534_sub29.anInt10651 * 917865515;
	    int i_108_ = 0;
	    int i_109_ = 0;
	    if (null != class272.aString2969) {
		i_109_ = Class236.aClass16_2373.method737(class272.aString2969,
							  (1771907305
							   * (Class706_Sub4
							      .anInt10994)),
							  0, null, 2112849964);
		i_108_ = Class236.aClass16_2373.method747(class272.aString2969,
							  ((Class706_Sub4
							    .anInt10994)
							   * 1771907305),
							  null, -1240725718);
	    }
	    int i_110_ = 2139882933 * class534_sub29.anInt10654 + i / 2;
	    int i_111_ = class534_sub29.anInt10651 * 917865515;
	    if (i_106_ < anInt7410 + i) {
		i_106_ = anInt7410;
		i_110_ = 5 + (i_105_ + (anInt7410 + i / 2) + i_108_ / 2);
	    } else if (i_106_ > anInt7412 - i) {
		i_106_ = anInt7412 - i;
		i_110_ = anInt7412 - i / 2 - i_105_ - i_108_ / 2 - 5;
	    }
	    if (i_107_ < anInt7411 + i) {
		i_107_ = anInt7411;
		i_111_ = i / 2 + (anInt7411 + i_105_);
	    } else if (i_107_ > anInt7378 - i) {
		i_107_ = anInt7378 - i;
		i_111_ = anInt7378 - i / 2 - i_105_ - i_109_;
	    }
	    int i_112_
		= ((int) (Math.atan2((double) (i_106_
					       - (class534_sub29.anInt10654
						  * 2139882933)),
				     (double) (i_107_
					       - (class534_sub29.anInt10651
						  * 917865515)))
			  / 3.141592653589793 * 32767.0)
		   & 0xffff);
	    class163.method2665((float) i / 2.0F + (float) i_106_,
				(float) i / 2.0F + (float) i_107_, 4096,
				i_112_);
	    int i_113_ = -2;
	    int i_114_ = -2;
	    int i_115_ = -2;
	    int i_116_ = -2;
	    if (class272.aString2969 != null) {
		i_113_ = i_110_ - i_108_ / 2 - 5;
		i_114_ = i_111_;
		i_115_ = i_108_ + i_113_ + 10;
		i_116_ = i_114_ + i_109_ + 3;
		if (1313711519 * class272.anInt2976 != 0)
		    class185.method3292(i_113_, i_114_, i_115_ - i_113_,
					i_116_ - i_114_,
					1313711519 * class272.anInt2976,
					-2012383459);
		if (0 != class272.anInt3009 * -2045123201)
		    class185.method3425(i_113_, i_114_, i_115_ - i_113_,
					i_116_ - i_114_,
					-2045123201 * class272.anInt3009,
					-1365545602);
		Class582.aClass171_7771.method2844
		    (class272.aString2969, i_110_, i_111_, i_108_, i_109_,
		     ~0xffffff | class272.anInt2970 * -1512587879,
		     335392643 * aClass622_7353.anInt8145, 1, 0, 0, null, null,
		     null, 0, 0, 202025040);
	    }
	    if (1747122653 * class272.anInt2967 != -1
		|| class272.aString2969 != null) {
		Class534_Sub20 class534_sub20
		    = new Class534_Sub20(class534_sub29);
		class534_sub20.anInt10520 = -773567207 * (i_106_ - i / 2);
		class534_sub20.anInt10522 = (i_106_ + i / 2) * -1946346005;
		class534_sub20.anInt10519 = (i_107_ - i) * -1190860309;
		class534_sub20.anInt10518 = i_107_ * -1010377381;
		class534_sub20.anInt10521 = i_113_ * 869904827;
		class534_sub20.anInt10517 = -536426739 * i_115_;
		class534_sub20.anInt10523 = -2069055837 * i_114_;
		class534_sub20.anInt10524 = -1973105707 * i_116_;
		Class615.aClass700_8055.method14131(class534_sub20,
						    (short) 789);
	    }
	}
    }
    
    public static void method16369(boolean bool) {
	aBool10694 = bool;
    }
    
    static final void method16370(Class185 class185, int i, int i_117_,
				  int i_118_, int i_119_) {
	class185.method3373(i, i_117_, i_118_ + i, i_119_ + i_117_);
	class185.method3292(i, i_117_, i_118_, i_119_, -16777216, -1752720559);
	if (anInt10671 * -1857977261 >= 100) {
	    float f = (float) anInt7371 / (float) anInt7370;
	    int i_120_ = i_118_;
	    int i_121_ = i_119_;
	    if (f < 1.0F)
		i_121_ = (int) (f * (float) i_118_);
	    else
		i_120_ = (int) ((float) i_119_ / f);
	    i += (i_118_ - i_120_) / 2;
	    i_117_ += (i_119_ - i_121_) / 2;
	    if (Class204.aClass163_2205 == null
		|| Class204.aClass163_2205.method2647() != i_120_
		|| Class204.aClass163_2205.method2649() != i_121_) {
		aClass203_7375 = aClass203_7393;
		method9124(anInt7368, anInt7369 + anInt7371,
			   anInt7370 + anInt7368, anInt7369, i, i_117_,
			   i_120_ + i, i_121_ + i_117_);
		method9179(class185, false, false, client.aBool11157, true);
		class185.method3581();
		Class204.aClass163_2205
		    = class185.method3319(i, i_117_, i_120_, i_121_, true);
	    }
	    Class204.aClass163_2205.method2656(i, i_117_);
	    int i_122_ = Class550.anInt7307 * -87431073 * i_120_ / anInt7370;
	    int i_123_ = 114918783 * Class567.anInt7595 * i_121_ / anInt7371;
	    int i_124_
		= i + i_120_ * (Class662.anInt8553 * 313632999) / anInt7370;
	    int i_125_
		= (i_121_ + i_117_
		   - i_121_ * (-1356415839 * Class347.anInt3590) / anInt7371
		   - i_123_);
	    int i_126_ = -1996554240;
	    if (client.aClass675_11016 == Class675.aClass675_8635)
		i_126_ = -1996488705;
	    class185.method3298(i_124_, i_125_, i_122_, i_123_, i_126_, 1);
	    class185.method3297(i_124_, i_125_, i_122_, i_123_, i_126_, 0);
	    for (Class534_Sub29 class534_sub29
		     = (Class534_Sub29) aClass700_7413.method14135((byte) -1);
		 class534_sub29 != null;
		 class534_sub29 = ((Class534_Sub29)
				   aClass700_7413.method14139(695483354))) {
		Class272 class272
		    = ((Class272)
		       aClass44_Sub9_7349.method91((class534_sub29.anInt10652
						    * 1592054281),
						   1133480630));
		if (Class231.method4211(class272, -1488345619)) {
		    Class25 class25
			= ((Class25)
			   (aHashMap10682.get
			    (Integer.valueOf(1592054281
					     * class534_sub29.anInt10652))));
		    if (class25 == null)
			class25
			    = (Class25) (aHashMap10681.get
					 (Integer.valueOf(class272.anInt2977
							  * -15128681)));
		    if (null != class25) {
			int i_127_;
			if (class25.anInt231 * -959968621
			    > 2135626771 * anInt10691 / 2)
			    i_127_ = ((anInt10691 * -876019987
				       - 21137517 * class25.anInt231)
				      / (anInt10691 * 2135626771));
			else
			    i_127_
				= 21137517 * class25.anInt231 / (2135626771
								 * anInt10691);
			int i_128_ = i + i_120_ * (class534_sub29.anInt10655
						   * 1412374331) / anInt7370;
			int i_129_
			    = ((i_121_
				* (anInt7371
				   - -958626707 * class534_sub29.anInt10653)
				/ anInt7371)
			       + i_117_);
			class185.method3292(i_128_ - 2, i_129_ - 2, 4, 4,
					    i_127_ << 24 | 0xffff00,
					    -1993540173);
		    }
		}
	    }
	    aClass203_7393.method3876(5, (byte) 0);
	}
    }
    
    static void method16371(int i) {
	Class151.anInt1705 = 704227181 * i;
	anInt10672 = 1421361991;
	anInt10673 = -2017528667;
	Class612.method10108(65280);
    }
    
    public static int method16372(int i, int i_130_, int i_131_) {
	if (anInt10671 * -1857977261 < 100)
	    return -2;
	int i_132_ = -2;
	int i_133_ = 2147483647;
	int i_134_ = i_130_ - anInt7368;
	int i_135_ = i_131_ - anInt7369;
	for (Class534_Sub29 class534_sub29
		 = (Class534_Sub29) aClass700_7413.method14135((byte) -1);
	     null != class534_sub29;
	     class534_sub29
		 = (Class534_Sub29) aClass700_7413.method14139(1063435394)) {
	    if (1592054281 * class534_sub29.anInt10652 == i) {
		int i_136_ = 1412374331 * class534_sub29.anInt10655;
		int i_137_ = -958626707 * class534_sub29.anInt10653;
		int i_138_ = anInt7368 + i_136_ << 14 | i_137_ + anInt7369;
		int i_139_ = ((i_135_ - i_137_) * (i_135_ - i_137_)
			      + (i_134_ - i_136_) * (i_134_ - i_136_));
		if (i_132_ < 0 || i_139_ < i_133_) {
		    i_132_ = i_138_;
		    i_133_ = i_139_;
		}
	    }
	}
	return i_132_;
    }
    
    public static Class534_Sub29 method16373() {
	if (null == aClass700_7413 || null == aClass710_10688)
	    return null;
	aClass710_10688.method14307(aClass700_7413, -871417193);
	Class534_Sub29 class534_sub29
	    = (Class534_Sub29) aClass710_10688.method14308((byte) 31);
	if (null == class534_sub29)
	    return null;
	Class272 class272
	    = (Class272) aClass44_Sub9_7349.method91((class534_sub29.anInt10652
						      * 1592054281),
						     -328943913);
	if (null == class272 || !class272.aBool2971
	    || !class272.method5063(anInterface18_7352, anInterface20_7351,
				    (short) 9921))
	    return Class324_Sub2.method15700(270262198);
	return class534_sub29;
    }
    
    public static Class534_Sub29 method16374() {
	if (aClass700_7413 == null || null == aClass710_10688)
	    return null;
	for (Class534_Sub29 class534_sub29
		 = (Class534_Sub29) aClass710_10688.next();
	     class534_sub29 != null;
	     class534_sub29 = (Class534_Sub29) aClass710_10688.next()) {
	    Class272 class272
		= ((Class272)
		   aClass44_Sub9_7349.method91((class534_sub29.anInt10652
						* 1592054281),
					       1329452562));
	    if (class272 != null && class272.aBool2971
		&& class272.method5063(anInterface18_7352, anInterface20_7351,
				       (short) 882))
		return class534_sub29;
	}
	return null;
    }
    
    public static Class534_Sub29 method16375() {
	if (aClass700_7413 == null || null == aClass710_10688)
	    return null;
	for (Class534_Sub29 class534_sub29
		 = (Class534_Sub29) aClass710_10688.next();
	     class534_sub29 != null;
	     class534_sub29 = (Class534_Sub29) aClass710_10688.next()) {
	    Class272 class272
		= ((Class272)
		   aClass44_Sub9_7349.method91((class534_sub29.anInt10652
						* 1592054281),
					       284579464));
	    if (class272 != null && class272.aBool2971
		&& class272.method5063(anInterface18_7352, anInterface20_7351,
				       (short) 5802))
		return class534_sub29;
	}
	return null;
    }
    
    public static void method16376(int i, int i_140_) {
	anInt10672 = (i - anInt7368) * -1421361991;
	anInt10673 = 2017528667 * (i_140_ - anInt7369);
    }
    
    static void method16377(int i) {
	Class328.anInt3479 = 980626489 * i;
	anInt10673 = -2017528667;
	anInt10673 = -2017528667;
	Class612.method10108(65280);
    }
    
    public static void method16378(int i, int i_141_) {
	Class151.anInt1705 = (i - anInt7368) * 704227181;
	Class328.anInt3479 = 980626489 * (i_141_ - anInt7369);
	anInt10672 = 1421361991;
	anInt10673 = -2017528667;
	Class612.method10108(65280);
    }
    
    public static void method16379(int i) {
	if (i < 1)
	    anInt10691 = anInt10670 * -1435674875;
	else
	    anInt10691 = i * 1444927003;
    }
    
    public static void method16380(int i, int i_142_) {
	anInt10672 = (i - anInt7368) * -1421361991;
	anInt10673 = 2017528667 * (i_142_ - anInt7369);
    }
    
    public static boolean method16381(int i, boolean bool) {
	if (i == anInt10683 * -363511917)
	    aBool10675 = bool;
	else if (i == 1272099037 * anInt10666)
	    aBool10667 = bool;
	else if (-1565977311 * anInt10692 == i)
	    aBool10676 = bool;
	else
	    return false;
	return true;
    }
    
    public static boolean method16382(int i, boolean bool) {
	if (i == anInt10683 * -363511917)
	    aBool10675 = bool;
	else if (i == 1272099037 * anInt10666)
	    aBool10667 = bool;
	else if (-1565977311 * anInt10692 == i)
	    aBool10676 = bool;
	else
	    return false;
	return true;
    }
    
    public static boolean method16383(int i, boolean bool) {
	if (i == anInt10683 * -363511917)
	    aBool10675 = bool;
	else if (i == 1272099037 * anInt10666)
	    aBool10667 = bool;
	else if (-1565977311 * anInt10692 == i)
	    aBool10676 = bool;
	else
	    return false;
	return true;
    }
    
    public static boolean method16384(int i, boolean bool) {
	if (i == anInt10683 * -363511917)
	    aBool10675 = bool;
	else if (i == 1272099037 * anInt10666)
	    aBool10667 = bool;
	else if (-1565977311 * anInt10692 == i)
	    aBool10676 = bool;
	else
	    return false;
	return true;
    }
    
    public static int method16385(int i) {
	if (-363511917 * anInt10683 == i)
	    return aBool10675 ? 1 : 0;
	if (i == 1272099037 * anInt10666)
	    return aBool10667 ? 1 : 0;
	if (i == anInt10692 * -1565977311)
	    return aBool10676 ? 1 : 0;
	return -1;
    }
    
    public static int method16386(int i) {
	if (-363511917 * anInt10683 == i)
	    return aBool10675 ? 1 : 0;
	if (i == 1272099037 * anInt10666)
	    return aBool10667 ? 1 : 0;
	if (i == anInt10692 * -1565977311)
	    return aBool10676 ? 1 : 0;
	return -1;
    }
}
