/* Class534_Sub37_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class534_Sub37_Sub3 extends Class534_Sub37
{
    Class683 aClass683_11885;
    
    public boolean method16500() {
	Interface62 interface62 = aClass683_11885.method13916((byte) 112);
	if (interface62 != null) {
	    Class686.method13969(Class583.aClass583_7788,
				 1225863589 * anInt10803, -1, interface62,
				 1321037813);
	    return true;
	}
	return false;
    }
    
    public boolean method16499(byte i) {
	Interface62 interface62 = aClass683_11885.method13916((byte) 26);
	if (interface62 != null) {
	    Class686.method13969(Class583.aClass583_7788,
				 1225863589 * anInt10803, -1, interface62,
				 -932988495);
	    return true;
	}
	return false;
    }
    
    public Class534_Sub37_Sub3(int i, int i_0_, Class683 class683) {
	super(i, i_0_);
	aClass683_11885 = class683;
    }
}
