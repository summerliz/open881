/* Class200_Sub23 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class200_Sub23 extends Class200
{
    int anInt10025;
    static final int anInt10026 = 1;
    int anInt10027;
    int anInt10028;
    int anInt10029;
    int anInt10030;
    static final int anInt10031 = 0;
    int anInt10032;
    int anInt10033;
    int anInt10034;
    int anInt10035;
    int anInt10036;
    int anInt10037;
    int anInt10038;
    int anInt10039;
    public static Class534_Sub28 aClass534_Sub28_10040;
    public static Class44_Sub14 aClass44_Sub14_10041;
    
    boolean method3848() {
	Class684 class684
	    = ((Class684)
	       Class55.aClass44_Sub4_447.method91(anInt10036 * -514047535,
						  649340253));
	boolean bool = class684.method13932(-1683357056);
	Class205 class205
	    = ((Class205)
	       Class200_Sub12.aClass44_Sub1_9934
		   .method91(-811043807 * class684.anInt8688, -1146568168));
	bool &= class205.method3913(-1452128371);
	return bool;
    }
    
    public void method3845(int i) {
	int i_0_;
	int i_1_;
	int i_2_;
	if (anInt10029 * 1755412759 >= 0) {
	    i_0_ = 256 + 1123167744 * anInt10029;
	    i_1_ = 653642240 * anInt10028 + 256;
	    i_2_ = -402800937 * anInt10027;
	} else {
	    Class654_Sub1_Sub5_Sub1 class654_sub1_sub5_sub1
		= Class65.aClass192Array712[-2057162939 * anInt10030]
		      .method3775(2088438307);
	    Class438 class438
		= class654_sub1_sub5_sub1.method10807().aClass438_4885;
	    i_0_ = (int) class438.aFloat4864;
	    i_1_ = (int) class438.aFloat4865;
	    i_2_ = class654_sub1_sub5_sub1.aByte10854;
	}
	int i_3_;
	int i_4_;
	if (anInt10028 * -225215771 >= 0) {
	    i_3_ = 256 + anInt10033 * 1204475392;
	    i_4_ = 256 + anInt10034 * -1312693760;
	} else {
	    Class654_Sub1_Sub5_Sub1 class654_sub1_sub5_sub1
		= Class65.aClass192Array712[anInt10032 * -1734843405]
		      .method3775(2088438307);
	    Class438 class438
		= class654_sub1_sub5_sub1.method10807().aClass438_4885;
	    i_3_ = (int) class438.aFloat4864;
	    i_4_ = (int) class438.aFloat4865;
	    if (i_2_ < 0)
		i_2_ = class654_sub1_sub5_sub1.aByte10854;
	}
	int i_5_ = anInt10039 * -79068723 << 2;
	Class654_Sub1_Sub5_Sub6 class654_sub1_sub5_sub6
	    = (new Class654_Sub1_Sub5_Sub6
	       (client.aClass512_11100.method8424((byte) 119),
		anInt10036 * -514047535, i_2_, i_2_, i_0_, i_1_,
		420167909 * anInt10038 << 2, client.anInt11101,
		client.anInt11101 + anInt10037 * 1300119697,
		anInt10025 * 2088163855, i_5_, anInt10030 * -2057162939 + 1,
		-1734843405 * anInt10032 + 1, anInt10035 * -715241479 << 2,
		false, 0, 0));
	class654_sub1_sub5_sub6.method18799(i_3_, i_4_,
					    anInt10035 * -715241479 << 2,
					    (1300119697 * anInt10037
					     + client.anInt11101),
					    16711935);
	client.aClass700_11210.method14131
	    (new Class534_Sub18_Sub4(class654_sub1_sub5_sub6), (short) 789);
    }
    
    Class200_Sub23(Class534_Sub40 class534_sub40, int i, int i_6_) {
	super(class534_sub40);
	if (0 == i) {
	    int i_7_ = class534_sub40.method16533(-258848859);
	    anInt10029 = 864243367 * (i_7_ >>> 16);
	    anInt10028 = (i_7_ & 0xffff) * 95728365;
	    anInt10030 = -1502874509;
	} else {
	    anInt10029 = -864243367;
	    anInt10028 = -95728365;
	    anInt10030 = class534_sub40.method16529((byte) 1) * 1502874509;
	}
	if (0 == i_6_) {
	    int i_8_ = class534_sub40.method16533(-258848859);
	    anInt10033 = 1580801859 * (i_8_ >>> 16);
	    anInt10034 = -894824431 * (i_8_ & 0xffff);
	    anInt10032 = 1182536389;
	} else {
	    anInt10033 = -1580801859;
	    anInt10034 = 894824431;
	    anInt10032 = class534_sub40.method16529((byte) 1) * -1182536389;
	}
	if (i == 0 && 0 == i_6_)
	    anInt10027 = class534_sub40.method16527(-212162260) * -1646258969;
	else
	    anInt10027 = 1646258969;
	anInt10036 = class534_sub40.method16529((byte) 1) * 1968944945;
	anInt10038 = class534_sub40.method16527(-2064407541) * -1540457235;
	anInt10035 = class534_sub40.method16527(531855458) * 816564809;
	anInt10037 = class534_sub40.method16531(1382344996) * -106069903;
	anInt10025 = class534_sub40.method16529((byte) 1) * -970271505;
	anInt10039 = class534_sub40.method16527(-407211236) * 761746181;
    }
    
    public void method3846() {
	int i;
	int i_9_;
	int i_10_;
	if (anInt10029 * 1755412759 >= 0) {
	    i = 256 + 1123167744 * anInt10029;
	    i_9_ = 653642240 * anInt10028 + 256;
	    i_10_ = -402800937 * anInt10027;
	} else {
	    Class654_Sub1_Sub5_Sub1 class654_sub1_sub5_sub1
		= Class65.aClass192Array712[-2057162939 * anInt10030]
		      .method3775(2088438307);
	    Class438 class438
		= class654_sub1_sub5_sub1.method10807().aClass438_4885;
	    i = (int) class438.aFloat4864;
	    i_9_ = (int) class438.aFloat4865;
	    i_10_ = class654_sub1_sub5_sub1.aByte10854;
	}
	int i_11_;
	int i_12_;
	if (anInt10028 * -225215771 >= 0) {
	    i_11_ = 256 + anInt10033 * 1204475392;
	    i_12_ = 256 + anInt10034 * -1312693760;
	} else {
	    Class654_Sub1_Sub5_Sub1 class654_sub1_sub5_sub1
		= Class65.aClass192Array712[anInt10032 * -1734843405]
		      .method3775(2088438307);
	    Class438 class438
		= class654_sub1_sub5_sub1.method10807().aClass438_4885;
	    i_11_ = (int) class438.aFloat4864;
	    i_12_ = (int) class438.aFloat4865;
	    if (i_10_ < 0)
		i_10_ = class654_sub1_sub5_sub1.aByte10854;
	}
	int i_13_ = anInt10039 * -79068723 << 2;
	Class654_Sub1_Sub5_Sub6 class654_sub1_sub5_sub6
	    = (new Class654_Sub1_Sub5_Sub6
	       (client.aClass512_11100.method8424((byte) 30),
		anInt10036 * -514047535, i_10_, i_10_, i, i_9_,
		420167909 * anInt10038 << 2, client.anInt11101,
		client.anInt11101 + anInt10037 * 1300119697,
		anInt10025 * 2088163855, i_13_, anInt10030 * -2057162939 + 1,
		-1734843405 * anInt10032 + 1, anInt10035 * -715241479 << 2,
		false, 0, 0));
	class654_sub1_sub5_sub6.method18799(i_11_, i_12_,
					    anInt10035 * -715241479 << 2,
					    (1300119697 * anInt10037
					     + client.anInt11101),
					    16711935);
	client.aClass700_11210.method14131
	    (new Class534_Sub18_Sub4(class654_sub1_sub5_sub6), (short) 789);
    }
    
    boolean method3844(int i) {
	Class684 class684
	    = ((Class684)
	       Class55.aClass44_Sub4_447.method91(anInt10036 * -514047535,
						  1130689918));
	boolean bool = class684.method13932(-1683357056);
	Class205 class205
	    = ((Class205)
	       Class200_Sub12.aClass44_Sub1_9934
		   .method91(-811043807 * class684.anInt8688, -336295057));
	bool &= class205.method3913(-652506218);
	return bool;
    }
    
    public void method3847() {
	int i;
	int i_14_;
	int i_15_;
	if (anInt10029 * 1755412759 >= 0) {
	    i = 256 + 1123167744 * anInt10029;
	    i_14_ = 653642240 * anInt10028 + 256;
	    i_15_ = -402800937 * anInt10027;
	} else {
	    Class654_Sub1_Sub5_Sub1 class654_sub1_sub5_sub1
		= Class65.aClass192Array712[-2057162939 * anInt10030]
		      .method3775(2088438307);
	    Class438 class438
		= class654_sub1_sub5_sub1.method10807().aClass438_4885;
	    i = (int) class438.aFloat4864;
	    i_14_ = (int) class438.aFloat4865;
	    i_15_ = class654_sub1_sub5_sub1.aByte10854;
	}
	int i_16_;
	int i_17_;
	if (anInt10028 * -225215771 >= 0) {
	    i_16_ = 256 + anInt10033 * 1204475392;
	    i_17_ = 256 + anInt10034 * -1312693760;
	} else {
	    Class654_Sub1_Sub5_Sub1 class654_sub1_sub5_sub1
		= Class65.aClass192Array712[anInt10032 * -1734843405]
		      .method3775(2088438307);
	    Class438 class438
		= class654_sub1_sub5_sub1.method10807().aClass438_4885;
	    i_16_ = (int) class438.aFloat4864;
	    i_17_ = (int) class438.aFloat4865;
	    if (i_15_ < 0)
		i_15_ = class654_sub1_sub5_sub1.aByte10854;
	}
	int i_18_ = anInt10039 * -79068723 << 2;
	Class654_Sub1_Sub5_Sub6 class654_sub1_sub5_sub6
	    = (new Class654_Sub1_Sub5_Sub6
	       (client.aClass512_11100.method8424((byte) 57),
		anInt10036 * -514047535, i_15_, i_15_, i, i_14_,
		420167909 * anInt10038 << 2, client.anInt11101,
		client.anInt11101 + anInt10037 * 1300119697,
		anInt10025 * 2088163855, i_18_, anInt10030 * -2057162939 + 1,
		-1734843405 * anInt10032 + 1, anInt10035 * -715241479 << 2,
		false, 0, 0));
	class654_sub1_sub5_sub6.method18799(i_16_, i_17_,
					    anInt10035 * -715241479 << 2,
					    (1300119697 * anInt10037
					     + client.anInt11101),
					    16711935);
	client.aClass700_11210.method14131
	    (new Class534_Sub18_Sub4(class654_sub1_sub5_sub6), (short) 789);
    }
    
    boolean method3849() {
	Class684 class684
	    = ((Class684)
	       Class55.aClass44_Sub4_447.method91(anInt10036 * -514047535,
						  -1282981671));
	boolean bool = class684.method13932(-1683357056);
	Class205 class205
	    = ((Class205)
	       Class200_Sub12.aClass44_Sub1_9934
		   .method91(-811043807 * class684.anInt8688, -1446759337));
	bool &= class205.method3913(1778931440);
	return bool;
    }
    
    static void method15658(Class433 class433, boolean bool, float f,
			    float f_19_, float f_20_, float f_21_, int i,
			    int i_22_, int i_23_) {
	int i_24_ = client.aClass512_11100.method8456((byte) -96);
	int i_25_ = client.aClass512_11100.method8443(-814167901);
	class433.method6857(f, f_19_, f_20_, f_21_, (float) i_25_,
			    (float) i_24_, (float) i, (float) i_22_);
    }
    
    public static int method15659(int i, int i_26_, boolean bool, int i_27_) {
	int i_28_ = 0;
	Class534_Sub5 class534_sub5 = Class269.method5023(i, bool, (byte) 18);
	if (class534_sub5 == null)
	    return 0;
	for (int i_29_ = 0; i_29_ < class534_sub5.anIntArray10414.length;
	     i_29_++) {
	    if (class534_sub5.anIntArray10414[i_29_] >= 0
		&& (((Class15)
		     Class531.aClass44_Sub7_7135.method91((class534_sub5
							   .anIntArray10414
							   [i_29_]),
							  -318265928)).anInt113
		    * 1160623023) == i_26_)
		i_28_ += Class19.method798(i, i_29_, bool, (byte) -20);
	}
	return i_28_;
    }
    
    public static boolean method15660(int i, int i_30_) {
	return 1 == i || i == 3 || i == 5;
    }
    
    static final void method15661(Class669 class669, short i) {
	String string = null;
	if (Class376.aClass43_3911 != null)
	    string = Class376.aClass43_3911.method1071((byte) 6);
	if (null == string)
	    string = "";
	class669.anObjectArray8593
	    [(class669.anInt8594 += 1460193483) * 1485266147 - 1]
	    = string;
    }
    
    public static final int method15662(int i, int i_31_, int i_32_,
					byte i_33_) {
	if (i_32_ > 243)
	    i_31_ >>= 4;
	else if (i_32_ > 217)
	    i_31_ >>= 3;
	else if (i_32_ > 192)
	    i_31_ >>= 2;
	else if (i_32_ > 179)
	    i_31_ >>= 1;
	return (i_31_ >> 5 << 7) + ((i & 0xff) >> 2 << 10) + (i_32_ >> 1);
    }
}
