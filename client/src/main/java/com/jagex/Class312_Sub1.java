/* Class312_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class312_Sub1 extends Class312
{
    Class163 aClass163_9996;
    Class163 aClass163_9997;
    Class163 aClass163_9998;
    Class163 aClass163_9999;
    Class163 aClass163_10000;
    Class163 aClass163_10001;
    
    void method5675(boolean bool, int i, int i_0_) {
	int i_1_ = i + aClass163_9998.method22();
	int i_2_ = (i + aClass394_3366.anInt4097 * -1607607997
		    - aClass163_9999.method22());
	int i_3_ = i_0_ + aClass163_10000.method2692();
	int i_4_ = (aClass394_3366.anInt4098 * -228886179 + i_0_
		    - aClass163_9996.method2692());
	int i_5_ = i_2_ - i_1_;
	int i_6_ = i_4_ - i_3_;
	int i_7_ = method5674((byte) 40) * i_5_ / 10000;
	int[] is = new int[4];
	Class254.aClass185_2683.method3360(is);
	Class254.aClass185_2683.method3373(i_1_, i_3_, i_7_ + i_1_, i_4_);
	method15644(i_1_, i_3_, i_5_, i_6_, 1656420121);
	Class254.aClass185_2683.method3373(i_7_ + i_1_, i_3_, i_2_, i_4_);
	aClass163_9997.method2662(i_1_, i_3_, i_5_, i_6_);
	Class254.aClass185_2683.method3373(is[0], is[1], is[2], is[3]);
    }
    
    public void method201(short i) {
	super.method201((short) -10352);
	Class394_Sub1 class394_sub1 = (Class394_Sub1) aClass394_3366;
	aClass163_10001
	    = Class243.method4480(aClass472_3367,
				  1333388775 * class394_sub1.anInt10143,
				  1574244493);
	aClass163_9997
	    = Class243.method4480(aClass472_3367,
				  class394_sub1.anInt10142 * -2081665769,
				  -1090376174);
	aClass163_9998
	    = Class243.method4480(aClass472_3367,
				  class394_sub1.anInt10147 * 845449995,
				  -1242783559);
	aClass163_9999
	    = Class243.method4480(aClass472_3367,
				  class394_sub1.anInt10145 * -773478983,
				  1761712212);
	aClass163_10000
	    = Class243.method4480(aClass472_3367,
				  class394_sub1.anInt10146 * -1674677163,
				  1916235048);
	aClass163_9996
	    = Class243.method4480(aClass472_3367,
				  class394_sub1.anInt10144 * 2123496001,
				  -110045395);
    }
    
    void method5672(boolean bool, int i, int i_8_, short i_9_) {
	if (bool) {
	    int[] is = new int[4];
	    Class254.aClass185_2683.method3360(is);
	    Class254.aClass185_2683.method3373
		(i, i_8_, -1607607997 * aClass394_3366.anInt4097 + i,
		 aClass394_3366.anInt4098 * -228886179 + i_8_);
	    int i_10_ = aClass163_9998.method22();
	    int i_11_ = aClass163_9998.method2692();
	    int i_12_ = aClass163_9999.method22();
	    int i_13_ = aClass163_9999.method2692();
	    aClass163_9998.method2656(i, i_8_ + ((-228886179
						  * aClass394_3366.anInt4098)
						 - i_11_) / 2);
	    aClass163_9999.method2656((aClass394_3366.anInt4097 * -1607607997
				       + i - i_12_),
				      (aClass394_3366.anInt4098 * -228886179
				       - i_13_) / 2 + i_8_);
	    Class254.aClass185_2683.method3373
		(i, i_8_, i + -1607607997 * aClass394_3366.anInt4097,
		 i_8_ + aClass163_10000.method2692());
	    aClass163_10000.method2662(i_10_ + i, i_8_,
				       (-1607607997 * aClass394_3366.anInt4097
					- i_10_ - i_12_),
				       aClass394_3366.anInt4098 * -228886179);
	    int i_14_ = aClass163_9996.method2692();
	    Class254.aClass185_2683.method3373
		(i, -228886179 * aClass394_3366.anInt4098 + i_8_ - i_14_,
		 aClass394_3366.anInt4097 * -1607607997 + i,
		 -228886179 * aClass394_3366.anInt4098 + i_8_);
	    aClass163_9996.method2662(i_10_ + i,
				      (-228886179 * aClass394_3366.anInt4098
				       + i_8_ - i_14_),
				      (aClass394_3366.anInt4097 * -1607607997
				       - i_10_ - i_12_),
				      -228886179 * aClass394_3366.anInt4098);
	    Class254.aClass185_2683.method3373(is[0], is[1], is[2], is[3]);
	}
    }
    
    void method5678(boolean bool, int i, int i_15_) {
	int i_16_ = i + aClass163_9998.method22();
	int i_17_ = (i + aClass394_3366.anInt4097 * -1607607997
		     - aClass163_9999.method22());
	int i_18_ = i_15_ + aClass163_10000.method2692();
	int i_19_ = (aClass394_3366.anInt4098 * -228886179 + i_15_
		     - aClass163_9996.method2692());
	int i_20_ = i_17_ - i_16_;
	int i_21_ = i_19_ - i_18_;
	int i_22_ = method5674((byte) 64) * i_20_ / 10000;
	int[] is = new int[4];
	Class254.aClass185_2683.method3360(is);
	Class254.aClass185_2683.method3373(i_16_, i_18_, i_22_ + i_16_, i_19_);
	method15644(i_16_, i_18_, i_20_, i_21_, 661489476);
	Class254.aClass185_2683.method3373(i_22_ + i_16_, i_18_, i_17_, i_19_);
	aClass163_9997.method2662(i_16_, i_18_, i_20_, i_21_);
	Class254.aClass185_2683.method3373(is[0], is[1], is[2], is[3]);
    }
    
    public void method203() {
	super.method201((short) -31962);
	Class394_Sub1 class394_sub1 = (Class394_Sub1) aClass394_3366;
	aClass163_10001
	    = Class243.method4480(aClass472_3367,
				  1333388775 * class394_sub1.anInt10143,
				  -831081666);
	aClass163_9997
	    = Class243.method4480(aClass472_3367,
				  class394_sub1.anInt10142 * -2081665769,
				  427126496);
	aClass163_9998
	    = Class243.method4480(aClass472_3367,
				  class394_sub1.anInt10147 * 845449995,
				  879208737);
	aClass163_9999
	    = Class243.method4480(aClass472_3367,
				  class394_sub1.anInt10145 * -773478983,
				  1630396811);
	aClass163_10000
	    = Class243.method4480(aClass472_3367,
				  class394_sub1.anInt10146 * -1674677163,
				  997023227);
	aClass163_9996
	    = Class243.method4480(aClass472_3367,
				  class394_sub1.anInt10144 * 2123496001,
				  1331586502);
    }
    
    public boolean method208() {
	if (!super.method199(-1455130856))
	    return false;
	Class394_Sub1 class394_sub1 = (Class394_Sub1) aClass394_3366;
	if (!aClass472_3367.method7670(class394_sub1.anInt10143 * 1333388775,
				       (byte) -21))
	    return false;
	if (!aClass472_3367.method7670(class394_sub1.anInt10142 * -2081665769,
				       (byte) -42))
	    return false;
	if (!aClass472_3367.method7670(845449995 * class394_sub1.anInt10147,
				       (byte) -21))
	    return false;
	if (!aClass472_3367.method7670(class394_sub1.anInt10145 * -773478983,
				       (byte) -97))
	    return false;
	if (!aClass472_3367.method7670(-1674677163 * class394_sub1.anInt10146,
				       (byte) -79))
	    return false;
	if (!aClass472_3367.method7670(2123496001 * class394_sub1.anInt10144,
				       (byte) -45))
	    return false;
	return true;
    }
    
    public boolean method199(int i) {
	if (!super.method199(425966225))
	    return false;
	Class394_Sub1 class394_sub1 = (Class394_Sub1) aClass394_3366;
	if (!aClass472_3367.method7670(class394_sub1.anInt10143 * 1333388775,
				       (byte) -69))
	    return false;
	if (!aClass472_3367.method7670(class394_sub1.anInt10142 * -2081665769,
				       (byte) -94))
	    return false;
	if (!aClass472_3367.method7670(845449995 * class394_sub1.anInt10147,
				       (byte) 0))
	    return false;
	if (!aClass472_3367.method7670(class394_sub1.anInt10145 * -773478983,
				       (byte) -60))
	    return false;
	if (!aClass472_3367.method7670(-1674677163 * class394_sub1.anInt10146,
				       (byte) -46))
	    return false;
	if (!aClass472_3367.method7670(2123496001 * class394_sub1.anInt10144,
				       (byte) -20))
	    return false;
	return true;
    }
    
    public boolean method204() {
	if (!super.method199(725372492))
	    return false;
	Class394_Sub1 class394_sub1 = (Class394_Sub1) aClass394_3366;
	if (!aClass472_3367.method7670(class394_sub1.anInt10143 * 1333388775,
				       (byte) -85))
	    return false;
	if (!aClass472_3367.method7670(class394_sub1.anInt10142 * -2081665769,
				       (byte) -43))
	    return false;
	if (!aClass472_3367.method7670(845449995 * class394_sub1.anInt10147,
				       (byte) -70))
	    return false;
	if (!aClass472_3367.method7670(class394_sub1.anInt10145 * -773478983,
				       (byte) -92))
	    return false;
	if (!aClass472_3367.method7670(-1674677163 * class394_sub1.anInt10146,
				       (byte) -127))
	    return false;
	if (!aClass472_3367.method7670(2123496001 * class394_sub1.anInt10144,
				       (byte) -4))
	    return false;
	return true;
    }
    
    void method15644(int i, int i_23_, int i_24_, int i_25_, int i_26_) {
	aClass163_10001.method2662(i, i_23_, i_24_, i_25_);
    }
    
    public boolean method200() {
	if (!super.method199(-1499277777))
	    return false;
	Class394_Sub1 class394_sub1 = (Class394_Sub1) aClass394_3366;
	if (!aClass472_3367.method7670(class394_sub1.anInt10143 * 1333388775,
				       (byte) -89))
	    return false;
	if (!aClass472_3367.method7670(class394_sub1.anInt10142 * -2081665769,
				       (byte) -89))
	    return false;
	if (!aClass472_3367.method7670(845449995 * class394_sub1.anInt10147,
				       (byte) -98))
	    return false;
	if (!aClass472_3367.method7670(class394_sub1.anInt10145 * -773478983,
				       (byte) -29))
	    return false;
	if (!aClass472_3367.method7670(-1674677163 * class394_sub1.anInt10146,
				       (byte) -65))
	    return false;
	if (!aClass472_3367.method7670(2123496001 * class394_sub1.anInt10144,
				       (byte) -46))
	    return false;
	return true;
    }
    
    public boolean method207() {
	if (!super.method199(-916274637))
	    return false;
	Class394_Sub1 class394_sub1 = (Class394_Sub1) aClass394_3366;
	if (!aClass472_3367.method7670(class394_sub1.anInt10143 * 1333388775,
				       (byte) -73))
	    return false;
	if (!aClass472_3367.method7670(class394_sub1.anInt10142 * -2081665769,
				       (byte) -6))
	    return false;
	if (!aClass472_3367.method7670(845449995 * class394_sub1.anInt10147,
				       (byte) -21))
	    return false;
	if (!aClass472_3367.method7670(class394_sub1.anInt10145 * -773478983,
				       (byte) -103))
	    return false;
	if (!aClass472_3367.method7670(-1674677163 * class394_sub1.anInt10146,
				       (byte) -92))
	    return false;
	if (!aClass472_3367.method7670(2123496001 * class394_sub1.anInt10144,
				       (byte) -13))
	    return false;
	return true;
    }
    
    public boolean method206() {
	if (!super.method199(-244054500))
	    return false;
	Class394_Sub1 class394_sub1 = (Class394_Sub1) aClass394_3366;
	if (!aClass472_3367.method7670(class394_sub1.anInt10143 * 1333388775,
				       (byte) -39))
	    return false;
	if (!aClass472_3367.method7670(class394_sub1.anInt10142 * -2081665769,
				       (byte) -22))
	    return false;
	if (!aClass472_3367.method7670(845449995 * class394_sub1.anInt10147,
				       (byte) -37))
	    return false;
	if (!aClass472_3367.method7670(class394_sub1.anInt10145 * -773478983,
				       (byte) -60))
	    return false;
	if (!aClass472_3367.method7670(-1674677163 * class394_sub1.anInt10146,
				       (byte) -46))
	    return false;
	if (!aClass472_3367.method7670(2123496001 * class394_sub1.anInt10144,
				       (byte) -61))
	    return false;
	return true;
    }
    
    void method5676(boolean bool, int i, int i_27_) {
	if (bool) {
	    int[] is = new int[4];
	    Class254.aClass185_2683.method3360(is);
	    Class254.aClass185_2683.method3373
		(i, i_27_, -1607607997 * aClass394_3366.anInt4097 + i,
		 aClass394_3366.anInt4098 * -228886179 + i_27_);
	    int i_28_ = aClass163_9998.method22();
	    int i_29_ = aClass163_9998.method2692();
	    int i_30_ = aClass163_9999.method22();
	    int i_31_ = aClass163_9999.method2692();
	    aClass163_9998.method2656(i, i_27_ + ((-228886179
						   * aClass394_3366.anInt4098)
						  - i_29_) / 2);
	    aClass163_9999.method2656((aClass394_3366.anInt4097 * -1607607997
				       + i - i_30_),
				      (aClass394_3366.anInt4098 * -228886179
				       - i_31_) / 2 + i_27_);
	    Class254.aClass185_2683.method3373
		(i, i_27_, i + -1607607997 * aClass394_3366.anInt4097,
		 i_27_ + aClass163_10000.method2692());
	    aClass163_10000.method2662(i_28_ + i, i_27_,
				       (-1607607997 * aClass394_3366.anInt4097
					- i_28_ - i_30_),
				       aClass394_3366.anInt4098 * -228886179);
	    int i_32_ = aClass163_9996.method2692();
	    Class254.aClass185_2683.method3373
		(i, -228886179 * aClass394_3366.anInt4098 + i_27_ - i_32_,
		 aClass394_3366.anInt4097 * -1607607997 + i,
		 -228886179 * aClass394_3366.anInt4098 + i_27_);
	    aClass163_9996.method2662(i_28_ + i,
				      (-228886179 * aClass394_3366.anInt4098
				       + i_27_ - i_32_),
				      (aClass394_3366.anInt4097 * -1607607997
				       - i_28_ - i_30_),
				      -228886179 * aClass394_3366.anInt4098);
	    Class254.aClass185_2683.method3373(is[0], is[1], is[2], is[3]);
	}
    }
    
    void method5671(boolean bool, int i, int i_33_) {
	if (bool) {
	    int[] is = new int[4];
	    Class254.aClass185_2683.method3360(is);
	    Class254.aClass185_2683.method3373
		(i, i_33_, -1607607997 * aClass394_3366.anInt4097 + i,
		 aClass394_3366.anInt4098 * -228886179 + i_33_);
	    int i_34_ = aClass163_9998.method22();
	    int i_35_ = aClass163_9998.method2692();
	    int i_36_ = aClass163_9999.method22();
	    int i_37_ = aClass163_9999.method2692();
	    aClass163_9998.method2656(i, i_33_ + ((-228886179
						   * aClass394_3366.anInt4098)
						  - i_35_) / 2);
	    aClass163_9999.method2656((aClass394_3366.anInt4097 * -1607607997
				       + i - i_36_),
				      (aClass394_3366.anInt4098 * -228886179
				       - i_37_) / 2 + i_33_);
	    Class254.aClass185_2683.method3373
		(i, i_33_, i + -1607607997 * aClass394_3366.anInt4097,
		 i_33_ + aClass163_10000.method2692());
	    aClass163_10000.method2662(i_34_ + i, i_33_,
				       (-1607607997 * aClass394_3366.anInt4097
					- i_34_ - i_36_),
				       aClass394_3366.anInt4098 * -228886179);
	    int i_38_ = aClass163_9996.method2692();
	    Class254.aClass185_2683.method3373
		(i, -228886179 * aClass394_3366.anInt4098 + i_33_ - i_38_,
		 aClass394_3366.anInt4097 * -1607607997 + i,
		 -228886179 * aClass394_3366.anInt4098 + i_33_);
	    aClass163_9996.method2662(i_34_ + i,
				      (-228886179 * aClass394_3366.anInt4098
				       + i_33_ - i_38_),
				      (aClass394_3366.anInt4097 * -1607607997
				       - i_34_ - i_36_),
				      -228886179 * aClass394_3366.anInt4098);
	    Class254.aClass185_2683.method3373(is[0], is[1], is[2], is[3]);
	}
    }
    
    void method5677(boolean bool, int i, int i_39_) {
	int i_40_ = i + aClass163_9998.method22();
	int i_41_ = (i + aClass394_3366.anInt4097 * -1607607997
		     - aClass163_9999.method22());
	int i_42_ = i_39_ + aClass163_10000.method2692();
	int i_43_ = (aClass394_3366.anInt4098 * -228886179 + i_39_
		     - aClass163_9996.method2692());
	int i_44_ = i_41_ - i_40_;
	int i_45_ = i_43_ - i_42_;
	int i_46_ = method5674((byte) 21) * i_44_ / 10000;
	int[] is = new int[4];
	Class254.aClass185_2683.method3360(is);
	Class254.aClass185_2683.method3373(i_40_, i_42_, i_46_ + i_40_, i_43_);
	method15644(i_40_, i_42_, i_44_, i_45_, 295516132);
	Class254.aClass185_2683.method3373(i_46_ + i_40_, i_42_, i_41_, i_43_);
	aClass163_9997.method2662(i_40_, i_42_, i_44_, i_45_);
	Class254.aClass185_2683.method3373(is[0], is[1], is[2], is[3]);
    }
    
    Class312_Sub1(Class472 class472, Class472 class472_47_,
		  Class394_Sub1 class394_sub1) {
	super(class472, class472_47_, (Class394) class394_sub1);
    }
    
    void method5673(boolean bool, int i, int i_48_, byte i_49_) {
	int i_50_ = i + aClass163_9998.method22();
	int i_51_ = (i + aClass394_3366.anInt4097 * -1607607997
		     - aClass163_9999.method22());
	int i_52_ = i_48_ + aClass163_10000.method2692();
	int i_53_ = (aClass394_3366.anInt4098 * -228886179 + i_48_
		     - aClass163_9996.method2692());
	int i_54_ = i_51_ - i_50_;
	int i_55_ = i_53_ - i_52_;
	int i_56_ = method5674((byte) 122) * i_54_ / 10000;
	int[] is = new int[4];
	Class254.aClass185_2683.method3360(is);
	Class254.aClass185_2683.method3373(i_50_, i_52_, i_56_ + i_50_, i_53_);
	method15644(i_50_, i_52_, i_54_, i_55_, 963857384);
	Class254.aClass185_2683.method3373(i_56_ + i_50_, i_52_, i_51_, i_53_);
	aClass163_9997.method2662(i_50_, i_52_, i_54_, i_55_);
	Class254.aClass185_2683.method3373(is[0], is[1], is[2], is[3]);
    }
    
    void method15645(int i, int i_57_, int i_58_, int i_59_) {
	aClass163_10001.method2662(i, i_57_, i_58_, i_59_);
    }
    
    static Class534_Sub18_Sub9 method15646(Class472 class472, int i,
					   int i_60_) {
	Class534_Sub40 class534_sub40
	    = new Class534_Sub40(class472.method7743(0, i, -590984005));
	return Class415.method6745(class534_sub40, i, (byte) -84);
    }
}
