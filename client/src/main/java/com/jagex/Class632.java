/* Class632 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class632
{
    public int anInt8223;
    public int anInt8224;
    public int anInt8225;
    public int[] anIntArray8226;
    static final int anInt8227 = 4;
    public int anInt8228;
    static final int anInt8229 = 4;
    static final int anInt8230 = 4;
    static final int anInt8231 = 7;
    public int anInt8232;
    public int[] anIntArray8233;
    public int anInt8234;
    public int anInt8235;
    public boolean aBool8236;
    public short aShort8237;
    public byte aByte8238;
    public boolean aBool8239;
    public short[][] aShortArrayArray8240;
    public int anInt8241;
    public short[][] aShortArrayArray8242;
    public boolean aBool8243;
    public boolean aBool8244;
    public int anInt8245;
    public int anInt8246;
    public int anInt8247;
    public int anInt8248;
    public int anInt8249;
    public int anInt8250;
    public int anInt8251;
    public int anInt8252;
    public int anInt8253;
    public short[][][] aShortArrayArrayArray8254;
    public int anInt8255;
    public int anInt8256;
    public int anInt8257 = 1081061908;
    public int anInt8258;
    public int anInt8259;
    public int anInt8260;
    public int anInt8261;
    public short[][][] aShortArrayArrayArray8262;
    public int anInt8263;
    public int anInt8264;
    public int anInt8265;
    public int anInt8266;
    public int anInt8267;
    public int anInt8268;
    public static Class538 aClass538_8269;
    public static Class44_Sub2 aClass44_Sub2_8270;
    
    void method10470(Class534_Sub40 class534_sub40) {
	boolean bool = false;
	for (;;) {
	    int i = class534_sub40.method16527(-328348556);
	    if (i == 0)
		break;
	    if (1 == i) {
		if (null == anIntArray8233) {
		    anInt8257 = 1081061908;
		    anIntArray8233 = new int[4];
		    anIntArray8226 = new int[4];
		}
		for (int i_0_ = 0; i_0_ < anIntArray8233.length; i_0_++) {
		    anIntArray8233[i_0_]
			= class534_sub40.method16530((byte) -3);
		    anIntArray8226[i_0_]
			= class534_sub40.method16530((byte) -106);
		}
		bool = true;
	    } else if (2 == i)
		anInt8259 = class534_sub40.method16550((byte) 30) * 1894341301;
	    else if (i == 3) {
		anInt8257 = class534_sub40.method16527(-778692505) * 270265477;
		anIntArray8233 = new int[521612365 * anInt8257];
		anIntArray8226 = new int[anInt8257 * 521612365];
	    } else if (i == 4)
		aBool8236 = false;
	    else if (5 == i)
		anInt8225
		    = class534_sub40.method16531(1895500442) * 1207442911;
	    else if (i == 6)
		anInt8235
		    = class534_sub40.method16531(1583761772) * -341323945;
	    else if (i == 7) {
		aShortArrayArray8240 = new short[10][4];
		aShortArrayArrayArray8254 = new short[10][4][];
		for (int i_1_ = 0; i_1_ < 10; i_1_++) {
		    for (int i_2_ = 0; i_2_ < 4; i_2_++) {
			int i_3_ = class534_sub40.method16529((byte) 1);
			if (i_3_ == 65535)
			    i_3_ = -1;
			aShortArrayArray8240[i_1_][i_2_] = (short) i_3_;
			int i_4_ = class534_sub40.method16529((byte) 1);
			aShortArrayArrayArray8254[i_1_][i_2_]
			    = new short[i_4_];
			for (int i_5_ = 0; i_5_ < i_4_; i_5_++) {
			    int i_6_ = class534_sub40.method16529((byte) 1);
			    if (65535 == i_6_)
				i_6_ = -1;
			    aShortArrayArrayArray8254[i_1_][i_2_][i_5_]
				= (short) i_6_;
			}
		    }
		}
	    } else if (i == 8)
		aBool8244 = false;
	    else if (i == 9)
		anInt8245
		    = class534_sub40.method16527(-2021202415) * -1107026483;
	    else if (i == 10)
		aBool8239 = false;
	    else if (i == 11)
		anInt8247
		    = class534_sub40.method16527(-2133488050) * -1439769685;
	    else if (12 == i) {
		anInt8234 = class534_sub40.method16529((byte) 1) * -2010386519;
		anInt8249 = class534_sub40.method16529((byte) 1) * 1786054219;
	    } else if (13 == i)
		anInt8256 = class534_sub40.method16527(322224485) * -406371079;
	    else if (14 == i)
		anInt8228 = class534_sub40.method16527(-44534520) * -861101745;
	    else if (i == 15)
		anInt8255
		    = class534_sub40.method16527(1584269971) * 1653182275;
	    else if (i == 16)
		aBool8243 = true;
	    else if (17 == i)
		anInt8251
		    = class534_sub40.method16533(-258848859) * -1859976553;
	    else if (18 == i)
		anInt8252
		    = class534_sub40.method16533(-258848859) * 1496757365;
	    else if (i == 19)
		anInt8253
		    = class534_sub40.method16533(-258848859) * -655924695;
	    else if (20 == i) {
		aShort8237 = (short) class534_sub40.method16529((byte) 1);
		aByte8238 = (byte) class534_sub40.method16527(1024645071);
	    } else if (21 == i)
		anInt8267
		    = class534_sub40.method16527(1417255231) * 2083591901;
	    else if (i == 22) {
		anInt8241
		    = class534_sub40.method16550((byte) -98) * 1794367753;
		anInt8250 = class534_sub40.method16550((byte) -8) * -939984419;
		anInt8263
		    = class534_sub40.method16550((byte) -59) * -934776929;
		anInt8246 = class534_sub40.method16550((byte) 58) * 456770393;
		anInt8258 = class534_sub40.method16550((byte) 64) * 2016267899;
		anInt8248
		    = class534_sub40.method16550((byte) -21) * 1319900403;
		anInt8260 = class534_sub40.method16586((byte) 1) * -660974931;
		anInt8261 = class534_sub40.method16586((byte) 1) * 770233465;
		anInt8224 = class534_sub40.method16550((byte) 17) * 1450685037;
		anInt8232 = class534_sub40.method16550((byte) -54) * 511676973;
		anInt8264
		    = class534_sub40.method16550((byte) -60) * -1583595061;
		anInt8265 = class534_sub40.method16550((byte) 42) * 990826513;
		anInt8266 = class534_sub40.method16550((byte) 13) * 1174984071;
		anInt8223 = class534_sub40.method16550((byte) 76) * -33359515;
		anInt8268
		    = class534_sub40.method16550((byte) -12) * -882413499;
	    } else if (i == 23) {
		aShortArrayArray8242 = new short[10][4];
		aShortArrayArrayArray8262 = new short[10][4][];
		for (int i_7_ = 0; i_7_ < 10; i_7_++) {
		    for (int i_8_ = 0; i_8_ < 4; i_8_++) {
			int i_9_ = class534_sub40.method16529((byte) 1);
			if (i_9_ == 65535)
			    i_9_ = -1;
			aShortArrayArray8242[i_7_][i_8_] = (short) i_9_;
			int i_10_ = class534_sub40.method16529((byte) 1);
			aShortArrayArrayArray8262[i_7_][i_8_]
			    = new short[i_10_];
			for (int i_11_ = 0; i_11_ < i_10_; i_11_++) {
			    int i_12_ = class534_sub40.method16529((byte) 1);
			    if (65535 == i_12_)
				i_12_ = -1;
			    aShortArrayArrayArray8262[i_7_][i_8_][i_11_]
				= (short) i_12_;
			}
		    }
		}
	    }
	}
	if (!bool) {
	    if (anIntArray8233 == null) {
		anInt8257 = 1081061908;
		anIntArray8233 = new int[4];
		anIntArray8226 = new int[4];
	    }
	    for (int i = 0; i < anIntArray8233.length; i++) {
		anIntArray8233[i] = 0;
		anIntArray8226[i] = i * 20;
	    }
	}
    }
    
    void method10471(Class534_Sub40 class534_sub40) {
	boolean bool = false;
	for (;;) {
	    int i = class534_sub40.method16527(661486114);
	    if (i == 0)
		break;
	    if (1 == i) {
		if (null == anIntArray8233) {
		    anInt8257 = 1081061908;
		    anIntArray8233 = new int[4];
		    anIntArray8226 = new int[4];
		}
		for (int i_13_ = 0; i_13_ < anIntArray8233.length; i_13_++) {
		    anIntArray8233[i_13_]
			= class534_sub40.method16530((byte) 0);
		    anIntArray8226[i_13_]
			= class534_sub40.method16530((byte) -90);
		}
		bool = true;
	    } else if (2 == i)
		anInt8259 = class534_sub40.method16550((byte) 63) * 1894341301;
	    else if (i == 3) {
		anInt8257 = class534_sub40.method16527(378571605) * 270265477;
		anIntArray8233 = new int[521612365 * anInt8257];
		anIntArray8226 = new int[anInt8257 * 521612365];
	    } else if (i == 4)
		aBool8236 = false;
	    else if (5 == i)
		anInt8225
		    = class534_sub40.method16531(1966267523) * 1207442911;
	    else if (i == 6)
		anInt8235 = class534_sub40.method16531(820053629) * -341323945;
	    else if (i == 7) {
		aShortArrayArray8240 = new short[10][4];
		aShortArrayArrayArray8254 = new short[10][4][];
		for (int i_14_ = 0; i_14_ < 10; i_14_++) {
		    for (int i_15_ = 0; i_15_ < 4; i_15_++) {
			int i_16_ = class534_sub40.method16529((byte) 1);
			if (i_16_ == 65535)
			    i_16_ = -1;
			aShortArrayArray8240[i_14_][i_15_] = (short) i_16_;
			int i_17_ = class534_sub40.method16529((byte) 1);
			aShortArrayArrayArray8254[i_14_][i_15_]
			    = new short[i_17_];
			for (int i_18_ = 0; i_18_ < i_17_; i_18_++) {
			    int i_19_ = class534_sub40.method16529((byte) 1);
			    if (65535 == i_19_)
				i_19_ = -1;
			    aShortArrayArrayArray8254[i_14_][i_15_][i_18_]
				= (short) i_19_;
			}
		    }
		}
	    } else if (i == 8)
		aBool8244 = false;
	    else if (i == 9)
		anInt8245
		    = class534_sub40.method16527(-1144064236) * -1107026483;
	    else if (i == 10)
		aBool8239 = false;
	    else if (i == 11)
		anInt8247
		    = class534_sub40.method16527(592788707) * -1439769685;
	    else if (12 == i) {
		anInt8234 = class534_sub40.method16529((byte) 1) * -2010386519;
		anInt8249 = class534_sub40.method16529((byte) 1) * 1786054219;
	    } else if (13 == i)
		anInt8256 = class534_sub40.method16527(353731998) * -406371079;
	    else if (14 == i)
		anInt8228
		    = class534_sub40.method16527(1668279522) * -861101745;
	    else if (i == 15)
		anInt8255
		    = class534_sub40.method16527(-1035554324) * 1653182275;
	    else if (i == 16)
		aBool8243 = true;
	    else if (17 == i)
		anInt8251
		    = class534_sub40.method16533(-258848859) * -1859976553;
	    else if (18 == i)
		anInt8252
		    = class534_sub40.method16533(-258848859) * 1496757365;
	    else if (i == 19)
		anInt8253
		    = class534_sub40.method16533(-258848859) * -655924695;
	    else if (20 == i) {
		aShort8237 = (short) class534_sub40.method16529((byte) 1);
		aByte8238 = (byte) class534_sub40.method16527(618585862);
	    } else if (21 == i)
		anInt8267
		    = class534_sub40.method16527(-373335120) * 2083591901;
	    else if (i == 22) {
		anInt8241
		    = class534_sub40.method16550((byte) 113) * 1794367753;
		anInt8250 = class534_sub40.method16550((byte) 20) * -939984419;
		anInt8263 = class534_sub40.method16550((byte) 90) * -934776929;
		anInt8246 = class534_sub40.method16550((byte) -45) * 456770393;
		anInt8258 = class534_sub40.method16550((byte) 24) * 2016267899;
		anInt8248
		    = class534_sub40.method16550((byte) -58) * 1319900403;
		anInt8260 = class534_sub40.method16586((byte) 1) * -660974931;
		anInt8261 = class534_sub40.method16586((byte) 1) * 770233465;
		anInt8224
		    = class534_sub40.method16550((byte) -78) * 1450685037;
		anInt8232 = class534_sub40.method16550((byte) -91) * 511676973;
		anInt8264
		    = class534_sub40.method16550((byte) -78) * -1583595061;
		anInt8265
		    = class534_sub40.method16550((byte) -118) * 990826513;
		anInt8266
		    = class534_sub40.method16550((byte) -45) * 1174984071;
		anInt8223 = class534_sub40.method16550((byte) 44) * -33359515;
		anInt8268 = class534_sub40.method16550((byte) 64) * -882413499;
	    } else if (i == 23) {
		aShortArrayArray8242 = new short[10][4];
		aShortArrayArrayArray8262 = new short[10][4][];
		for (int i_20_ = 0; i_20_ < 10; i_20_++) {
		    for (int i_21_ = 0; i_21_ < 4; i_21_++) {
			int i_22_ = class534_sub40.method16529((byte) 1);
			if (i_22_ == 65535)
			    i_22_ = -1;
			aShortArrayArray8242[i_20_][i_21_] = (short) i_22_;
			int i_23_ = class534_sub40.method16529((byte) 1);
			aShortArrayArrayArray8262[i_20_][i_21_]
			    = new short[i_23_];
			for (int i_24_ = 0; i_24_ < i_23_; i_24_++) {
			    int i_25_ = class534_sub40.method16529((byte) 1);
			    if (65535 == i_25_)
				i_25_ = -1;
			    aShortArrayArrayArray8262[i_20_][i_21_][i_24_]
				= (short) i_25_;
			}
		    }
		}
	    }
	}
	if (!bool) {
	    if (anIntArray8233 == null) {
		anInt8257 = 1081061908;
		anIntArray8233 = new int[4];
		anIntArray8226 = new int[4];
	    }
	    for (int i = 0; i < anIntArray8233.length; i++) {
		anIntArray8233[i] = 0;
		anIntArray8226[i] = i * 20;
	    }
	}
    }
    
    void method10472(Class534_Sub40 class534_sub40, int i) {
	boolean bool = false;
	for (;;) {
	    int i_26_ = class534_sub40.method16527(799347192);
	    if (i_26_ == 0)
		break;
	    if (1 == i_26_) {
		if (null == anIntArray8233) {
		    anInt8257 = 1081061908;
		    anIntArray8233 = new int[4];
		    anIntArray8226 = new int[4];
		}
		for (int i_27_ = 0; i_27_ < anIntArray8233.length; i_27_++) {
		    anIntArray8233[i_27_]
			= class534_sub40.method16530((byte) -22);
		    anIntArray8226[i_27_]
			= class534_sub40.method16530((byte) -102);
		}
		bool = true;
	    } else if (2 == i_26_)
		anInt8259 = class534_sub40.method16550((byte) 3) * 1894341301;
	    else if (i_26_ == 3) {
		anInt8257 = class534_sub40.method16527(-962880302) * 270265477;
		anIntArray8233 = new int[521612365 * anInt8257];
		anIntArray8226 = new int[anInt8257 * 521612365];
	    } else if (i_26_ == 4)
		aBool8236 = false;
	    else if (5 == i_26_)
		anInt8225
		    = class534_sub40.method16531(1337956299) * 1207442911;
	    else if (i_26_ == 6)
		anInt8235
		    = class534_sub40.method16531(1415266419) * -341323945;
	    else if (i_26_ == 7) {
		aShortArrayArray8240 = new short[10][4];
		aShortArrayArrayArray8254 = new short[10][4][];
		for (int i_28_ = 0; i_28_ < 10; i_28_++) {
		    for (int i_29_ = 0; i_29_ < 4; i_29_++) {
			int i_30_ = class534_sub40.method16529((byte) 1);
			if (i_30_ == 65535)
			    i_30_ = -1;
			aShortArrayArray8240[i_28_][i_29_] = (short) i_30_;
			int i_31_ = class534_sub40.method16529((byte) 1);
			aShortArrayArrayArray8254[i_28_][i_29_]
			    = new short[i_31_];
			for (int i_32_ = 0; i_32_ < i_31_; i_32_++) {
			    int i_33_ = class534_sub40.method16529((byte) 1);
			    if (65535 == i_33_)
				i_33_ = -1;
			    aShortArrayArrayArray8254[i_28_][i_29_][i_32_]
				= (short) i_33_;
			}
		    }
		}
	    } else if (i_26_ == 8)
		aBool8244 = false;
	    else if (i_26_ == 9)
		anInt8245
		    = class534_sub40.method16527(-2028878711) * -1107026483;
	    else if (i_26_ == 10)
		aBool8239 = false;
	    else if (i_26_ == 11)
		anInt8247
		    = class534_sub40.method16527(-1420388941) * -1439769685;
	    else if (12 == i_26_) {
		anInt8234 = class534_sub40.method16529((byte) 1) * -2010386519;
		anInt8249 = class534_sub40.method16529((byte) 1) * 1786054219;
	    } else if (13 == i_26_)
		anInt8256
		    = class534_sub40.method16527(-1967197720) * -406371079;
	    else if (14 == i_26_)
		anInt8228
		    = class534_sub40.method16527(-449580711) * -861101745;
	    else if (i_26_ == 15)
		anInt8255
		    = class534_sub40.method16527(-426167639) * 1653182275;
	    else if (i_26_ == 16)
		aBool8243 = true;
	    else if (17 == i_26_)
		anInt8251
		    = class534_sub40.method16533(-258848859) * -1859976553;
	    else if (18 == i_26_)
		anInt8252
		    = class534_sub40.method16533(-258848859) * 1496757365;
	    else if (i_26_ == 19)
		anInt8253
		    = class534_sub40.method16533(-258848859) * -655924695;
	    else if (20 == i_26_) {
		aShort8237 = (short) class534_sub40.method16529((byte) 1);
		aByte8238 = (byte) class534_sub40.method16527(-460200428);
	    } else if (21 == i_26_)
		anInt8267
		    = class534_sub40.method16527(-1879968240) * 2083591901;
	    else if (i_26_ == 22) {
		anInt8241 = class534_sub40.method16550((byte) 26) * 1794367753;
		anInt8250
		    = class534_sub40.method16550((byte) -42) * -939984419;
		anInt8263 = class534_sub40.method16550((byte) 35) * -934776929;
		anInt8246 = class534_sub40.method16550((byte) -8) * 456770393;
		anInt8258
		    = class534_sub40.method16550((byte) -79) * 2016267899;
		anInt8248 = class534_sub40.method16550((byte) 46) * 1319900403;
		anInt8260 = class534_sub40.method16586((byte) 1) * -660974931;
		anInt8261 = class534_sub40.method16586((byte) 1) * 770233465;
		anInt8224
		    = class534_sub40.method16550((byte) -26) * 1450685037;
		anInt8232 = class534_sub40.method16550((byte) 103) * 511676973;
		anInt8264
		    = class534_sub40.method16550((byte) 46) * -1583595061;
		anInt8265 = class534_sub40.method16550((byte) 13) * 990826513;
		anInt8266
		    = class534_sub40.method16550((byte) -34) * 1174984071;
		anInt8223 = class534_sub40.method16550((byte) -10) * -33359515;
		anInt8268 = class534_sub40.method16550((byte) 2) * -882413499;
	    } else if (i_26_ == 23) {
		aShortArrayArray8242 = new short[10][4];
		aShortArrayArrayArray8262 = new short[10][4][];
		for (int i_34_ = 0; i_34_ < 10; i_34_++) {
		    for (int i_35_ = 0; i_35_ < 4; i_35_++) {
			int i_36_ = class534_sub40.method16529((byte) 1);
			if (i_36_ == 65535)
			    i_36_ = -1;
			aShortArrayArray8242[i_34_][i_35_] = (short) i_36_;
			int i_37_ = class534_sub40.method16529((byte) 1);
			aShortArrayArrayArray8262[i_34_][i_35_]
			    = new short[i_37_];
			for (int i_38_ = 0; i_38_ < i_37_; i_38_++) {
			    int i_39_ = class534_sub40.method16529((byte) 1);
			    if (65535 == i_39_)
				i_39_ = -1;
			    aShortArrayArrayArray8262[i_34_][i_35_][i_38_]
				= (short) i_39_;
			}
		    }
		}
	    }
	}
	if (!bool) {
	    if (anIntArray8233 == null) {
		anInt8257 = 1081061908;
		anIntArray8233 = new int[4];
		anIntArray8226 = new int[4];
	    }
	    for (int i_40_ = 0; i_40_ < anIntArray8233.length; i_40_++) {
		anIntArray8233[i_40_] = 0;
		anIntArray8226[i_40_] = i_40_ * 20;
	    }
	}
    }
    
    public Class632(Class472 class472) {
	anIntArray8233 = null;
	anIntArray8226 = null;
	anInt8228 = 850560316;
	anInt8256 = -1625484316;
	anInt8255 = -1312625963;
	anInt8259 = -1894341301;
	anInt8225 = -1207442911;
	anInt8235 = 341323945;
	aBool8236 = true;
	aShort8237 = (short) -1;
	aByte8238 = (byte) 0;
	anInt8267 = -2094207404;
	aBool8244 = true;
	anInt8245 = 2080914330;
	aBool8239 = true;
	anInt8247 = -24341759;
	anInt8234 = 2010386519;
	anInt8249 = -1786054219;
	anInt8251 = 1945987328;
	anInt8252 = 466176907;
	anInt8253 = -294130816;
	anInt8241 = -1794367753;
	anInt8250 = 939984419;
	anInt8263 = 934776929;
	anInt8246 = -456770393;
	anInt8258 = -2016267899;
	anInt8248 = -1319900403;
	anInt8224 = -1450685037;
	anInt8232 = -511676973;
	anInt8264 = 1583595061;
	anInt8265 = -990826513;
	anInt8266 = -1174984071;
	anInt8223 = 33359515;
	anInt8268 = 882413499;
	byte[] is = class472.method7738((Class617.aClass617_8093.anInt8096
					 * -448671533),
					(byte) -119);
	method10472(new Class534_Sub40(is), 225165556);
    }
    
    static final void method10473(Class669 class669, byte i) {
	class669.anObjectArray8611[(class669.anIntArray8591
				    [class669.anInt8613 * 662605117])]
	    = (class669.anObjectArray8593
	       [(class669.anInt8594 -= 1460193483) * 1485266147]);
    }
}
