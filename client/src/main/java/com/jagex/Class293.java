/* Class293 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class293
{
    public static Class293 aClass293_3124;
    public static Class293 aClass293_3125;
    public int anInt3126;
    static Class293 aClass293_3127;
    static Class293 aClass293_3128;
    static Class293 aClass293_3129;
    static Class293 aClass293_3130;
    public static Class293 aClass293_3131 = new Class293(0, false);
    boolean aBool3132;
    
    public boolean method5308() {
	return aBool3132;
    }
    
    public boolean method5309(int i) {
	return aBool3132;
    }
    
    static {
	aClass293_3125 = new Class293(1, false);
	aClass293_3128 = new Class293(2, true);
	aClass293_3124 = new Class293(3, false);
	aClass293_3127 = new Class293(4, true);
	aClass293_3129 = new Class293(5, true);
	aClass293_3130 = new Class293(6, true);
    }
    
    Class293(int i, boolean bool) {
	anInt3126 = i * -1942521291;
	aBool3132 = bool;
    }
    
    public static Class293 method5310(int i) {
	if (-2097182691 * aClass293_3131.anInt3126 == i)
	    return aClass293_3131;
	if (-2097182691 * aClass293_3125.anInt3126 == i)
	    return aClass293_3125;
	if (i == -2097182691 * aClass293_3128.anInt3126)
	    return aClass293_3128;
	if (i == -2097182691 * aClass293_3124.anInt3126)
	    return aClass293_3124;
	if (i == aClass293_3127.anInt3126 * -2097182691)
	    return aClass293_3127;
	if (aClass293_3129.anInt3126 * -2097182691 == i)
	    return aClass293_3129;
	if (i == -2097182691 * aClass293_3130.anInt3126)
	    return aClass293_3130;
	return null;
    }
    
    public boolean method5311() {
	return aBool3132;
    }
    
    public boolean method5312() {
	return aBool3132;
    }
    
    static final void method5313(Class669 class669, int i) {
	class669.anInt8596 -= -2111195934;
	if (class669.aLongArray8587[1572578961 * class669.anInt8596]
	    < class669.aLongArray8587[class669.anInt8596 * 1572578961 + 1])
	    class669.anInt8613
		+= (class669.anIntArray8591[662605117 * class669.anInt8613]
		    * -793595371);
    }
    
    static final void method5314(Class669 class669, byte i) {
	class669.anInt8594 -= -1374580330;
	String string
	    = ((String)
	       class669.anObjectArray8593[class669.anInt8594 * 1485266147]);
	String string_0_ = (String) (class669.anObjectArray8593
				     [class669.anInt8594 * 1485266147 + 1]);
	if (0 != 365872613 * client.anInt11194
	    || ((!client.aBool11223 || client.aBool11196)
		&& !client.aBool11076)) {
	    Class100 class100 = Class201.method3864(2095398292);
	    Class534_Sub19 class534_sub19
		= Class346.method6128(Class404.aClass404_4236,
				      class100.aClass13_1183, 1341391005);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16507(0,
								  1330050423);
	    int i_1_ = (class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
			* 31645619);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16713(string,
								  1279917996);
	    Class188.method3753(class534_sub19.aClass534_Sub40_Sub1_10513,
				string_0_, (byte) 34);
	    class534_sub19.aClass534_Sub40_Sub1_10513.method16733
		((class534_sub19.aClass534_Sub40_Sub1_10513.anInt10811
		  * 31645619) - i_1_,
		 2037140115);
	    class100.method1863(class534_sub19, (byte) 115);
	}
    }
    
    static final void method5315(Class669 class669, int i) {
	int i_2_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class534_Sub18_Sub9 class534_sub18_sub9 = Class554.method9114(i_2_);
	if (null == class534_sub18_sub9)
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= 0;
	else
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= -1320299993 * class534_sub18_sub9.anInt11769;
    }
    
    static final void method5316(Class669 class669, short i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub25_10750
		  .method17102((byte) -49) == 1 ? 1 : 0;
    }
}
