/* Class278 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public abstract class Class278 implements Interface36
{
    protected String aString3050;
    protected String aString3051;
    protected String aString3052 = null;
    
    abstract void method5166(int i, float f, float f_0_, float f_1_,
			     float f_2_);
    
    String method5167(byte i) {
	return aString3052;
    }
    
    abstract void method5168(int i, float f, float f_3_, float f_4_);
    
    abstract void method5169(Class534_Sub12_Sub1 class534_sub12_sub1, float f);
    
    abstract void method5170(Class534_Sub12_Sub1 class534_sub12_sub1, float f);
    
    abstract void method5171(Class534_Sub12_Sub1 class534_sub12_sub1, float f,
			     float f_5_, float f_6_);
    
    abstract void method5172(Class534_Sub12_Sub1 class534_sub12_sub1, float f,
			     float f_7_, float f_8_, float f_9_);
    
    abstract void method5173(int i, int i_10_, Interface43 interface43);
    
    abstract void method5174(Class534_Sub12_Sub1 class534_sub12_sub1,
			     Class433 class433);
    
    abstract void method5175(Class534_Sub12_Sub1 class534_sub12_sub1,
			     Class433 class433);
    
    abstract void method5176(Class534_Sub12_Sub1 class534_sub12_sub1, int i,
			     Interface43 interface43);
    
    abstract void method5177(int i, float f, float f_11_, float f_12_);
    
    abstract void method5178(int i, float f, float f_13_, float f_14_,
			     float f_15_);
    
    String method5179() {
	return aString3052;
    }
    
    abstract void method5180(int i, Class433 class433);
    
    abstract void method5181(int i, Class433 class433);
    
    public abstract boolean method5182();
    
    abstract void method5183(int i, int i_16_, Interface43 interface43);
    
    abstract void method5184(int i, Class433 class433);
    
    public abstract boolean method5185();
    
    public abstract boolean method5186();
    
    abstract void method5187(int i, Class433 class433);
    
    abstract void method5188(Class534_Sub12_Sub1 class534_sub12_sub1, int i,
			     Interface43 interface43);
    
    abstract void method5189(Class534_Sub12_Sub1 class534_sub12_sub1, float f);
    
    abstract void method5190(Class534_Sub12_Sub1 class534_sub12_sub1, float f,
			     float f_17_);
    
    abstract void method5191(Class534_Sub12_Sub1 class534_sub12_sub1, float f,
			     float f_18_);
    
    abstract void method5192(Class534_Sub12_Sub1 class534_sub12_sub1, float f,
			     float f_19_, float f_20_, float f_21_);
    
    abstract void method5193(Class534_Sub12_Sub1 class534_sub12_sub1,
			     Class433 class433);
    
    abstract void method5194(int i, float[] fs, int i_22_);
    
    abstract void method5195(Class534_Sub12_Sub1 class534_sub12_sub1,
			     float[] fs, int i);
    
    abstract void method5196(int i, float f, float f_23_, float f_24_,
			     float f_25_);
    
    abstract void method5197(int i, float f, float f_26_, float f_27_,
			     float f_28_);
    
    abstract void method5198(int i, float f, float f_29_, float f_30_,
			     float f_31_);
    
    abstract void method5199(int i, float[] fs, int i_32_);
    
    abstract void method5200(int i, float[] fs, int i_33_);
    
    abstract void method5201(Class534_Sub12_Sub1 class534_sub12_sub1,
			     float[] fs, int i);
    
    abstract void method5202(int i, Class433 class433);
    
    abstract void method5203(int i, Class433 class433);
    
    abstract void method5204(int i, Class433 class433);
    
    abstract void method5205(int i, Class433 class433);
    
    abstract void method5206(Class534_Sub12_Sub1 class534_sub12_sub1,
			     Class433 class433);
    
    abstract void method5207(int i, Class433 class433);
    
    abstract void method5208(int i, int i_34_, Interface43 interface43);
    
    abstract void method5209(int i, float f, float f_35_, float f_36_);
    
    abstract void method5210(Class534_Sub12_Sub1 class534_sub12_sub1, float f,
			     float f_37_, float f_38_);
    
    abstract void method5211(Class534_Sub12_Sub1 class534_sub12_sub1, float f,
			     float f_39_, float f_40_);
    
    abstract void method5212(Class534_Sub12_Sub1 class534_sub12_sub1, float f,
			     float f_41_);
    
    abstract void method5213(Class534_Sub12_Sub1 class534_sub12_sub1,
			     Class433 class433);
    
    abstract void method5214(Class534_Sub12_Sub1 class534_sub12_sub1,
			     Class433 class433);
    
    abstract void method5215(Class534_Sub12_Sub1 class534_sub12_sub1,
			     float[] fs, int i);
    
    abstract void method5216(int i, float[] fs, int i_42_);
    
    abstract void method5217(Class534_Sub12_Sub1 class534_sub12_sub1,
			     float[] fs, int i);
    
    Class278() {
	/* empty */
    }
    
    abstract void method5218(Class534_Sub12_Sub1 class534_sub12_sub1, int i,
			     Interface43 interface43);
    
    abstract void method5219(Class534_Sub12_Sub1 class534_sub12_sub1, int i,
			     Interface43 interface43);
    
    abstract void method5220(Class534_Sub12_Sub1 class534_sub12_sub1,
			     Class433 class433);
    
    abstract void method5221(Class534_Sub12_Sub1 class534_sub12_sub1, float f);
    
    abstract void method5222(int i, float f, float f_43_, float f_44_);
    
    abstract void method5223(int i, float f, float f_45_, float f_46_);
    
    public static void method5224(boolean bool, int i) {
	for (Class534_Sub16 class534_sub16
		 = ((Class534_Sub16)
		    Class534_Sub16.aClass700_10468.method14135((byte) -1));
	     null != class534_sub16;
	     class534_sub16 = (Class534_Sub16) Class534_Sub16
						   .aClass700_10468
						   .method14139(1360876077)) {
	    if (class534_sub16.aClass491_10492 != null) {
		class534_sub16.aClass491_10492.method8014(150, 1962988118);
		Class171_Sub4.aClass232_9944
		    .method4234(class534_sub16.aClass491_10492, 759125504);
		class534_sub16.aClass491_10492 = null;
	    }
	    if (null != class534_sub16.aClass491_10494) {
		class534_sub16.aClass491_10494.method8014(150, 1962988118);
		Class171_Sub4.aClass232_9944
		    .method4234(class534_sub16.aClass491_10494, -2015207183);
		class534_sub16.aClass491_10494 = null;
	    }
	    class534_sub16.method8892((byte) 1);
	}
	if (bool) {
	    for (Class534_Sub16 class534_sub16
		     = ((Class534_Sub16)
			Class534_Sub16.aClass700_10469.method14135((byte) -1));
		 null != class534_sub16;
		 class534_sub16
		     = (Class534_Sub16) Class534_Sub16.aClass700_10469
					    .method14139(1451480084)) {
		if (null != class534_sub16.aClass491_10492) {
		    class534_sub16.aClass491_10492.method8014(150, 1962988118);
		    Class171_Sub4.aClass232_9944.method4234((class534_sub16
							     .aClass491_10492),
							    -2138045131);
		    class534_sub16.aClass491_10492 = null;
		}
		class534_sub16.method8892((byte) 1);
	    }
	    for (Class534_Sub16 class534_sub16
		     = ((Class534_Sub16)
			Class534_Sub16.aClass9_10470.method583(-1594708768));
		 null != class534_sub16;
		 class534_sub16
		     = ((Class534_Sub16)
			Class534_Sub16.aClass9_10470.method584((byte) -88))) {
		if (null != class534_sub16.aClass491_10492) {
		    class534_sub16.aClass491_10492.method8014(150, 1962988118);
		    Class171_Sub4.aClass232_9944.method4234((class534_sub16
							     .aClass491_10492),
							    -1934970728);
		    class534_sub16.aClass491_10492 = null;
		}
		class534_sub16.method8892((byte) 1);
	    }
	}
    }
    
    static final void method5225(Class669 class669, byte i) {
	int i_47_ = (class669.anIntArray8595
		     [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class247 class247 = Class112.method2017(i_47_, -132355223);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = -881188269 * class247.anInt2468;
    }
}
