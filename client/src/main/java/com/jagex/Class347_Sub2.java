/* Class347_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class347_Sub2 extends Class347
{
    int anInt10160;
    Class438 aClass438_10161 = new Class438(Float.NaN, Float.NaN, Float.NaN);
    Class438 aClass438_10162 = new Class438(Float.NaN, Float.NaN, Float.NaN);
    Class438 aClass438_10163 = new Class438();
    boolean aBool10164 = false;
    
    public void method6142(Class287 class287, int i, int i_0_) {
	class287.anInt3082
	    = -1803517691 * ((int) aClass438_10161.aFloat4864 - i);
	class287.anInt3083 = -1390317707 * (int) -aClass438_10161.aFloat4863;
	class287.anInt3081
	    = 1552704101 * ((int) aClass438_10161.aFloat4865 - i_0_);
    }
    
    public void method15787(Class534_Sub36 class534_sub36, int i) {
	aClass438_10162.aFloat4864
	    = (float) (599803149 * class534_sub36.anInt10797);
	aClass438_10162.aFloat4863
	    = (float) (-1203728391 * class534_sub36.anInt10798);
	aClass438_10162.aFloat4865
	    = (float) (class534_sub36.anInt10799 * 33298755);
	if (Float.isNaN(aClass438_10161.aFloat4864)) {
	    aClass438_10161.method6992(aClass438_10162);
	    aClass438_10163.method6999();
	}
	anInt10160 = class534_sub36.anInt10796 * 1592336477;
    }
    
    public void method6136(float f, int[][][] is, Class468 class468, int i,
			   int i_1_, byte i_2_) {
	if (aBool10164)
	    method15791(is, class468, i, i_1_, -1663786923);
	aClass298_3589.method5352(true, f, aClass438_10161,
				  aClass298_3589.method5391((byte) 89),
				  aClass438_10162, aClass438_10163,
				  (byte) -50);
    }
    
    public boolean method6131(int i) {
	return !Float.isNaN(aClass438_10161.aFloat4864);
    }
    
    public Class438 method6137(int i) {
	return Class438.method6994(aClass438_10161);
    }
    
    public double[] method6130(int i) {
	double[] ds = new double[3];
	Class438 class438 = method6137(-252620383);
	ds[0] = (double) class438.aFloat4864;
	ds[1] = (double) class438.aFloat4863;
	ds[2] = (double) class438.aFloat4865;
	return ds;
    }
    
    public void method6139(float f, int[][][] is, Class468 class468, int i,
			   int i_3_) {
	if (aBool10164)
	    method15791(is, class468, i, i_3_, -1663786923);
	aClass298_3589.method5352(true, f, aClass438_10161,
				  aClass298_3589.method5391((byte) 3),
				  aClass438_10162, aClass438_10163,
				  (byte) -31);
    }
    
    public float method6134(int i) {
	return 0.0F;
    }
    
    public void method15788(boolean bool) {
	aBool10164 = bool;
    }
    
    public void method6148(Class534_Sub40 class534_sub40, byte i) {
	aClass438_10162.method7080(class534_sub40);
    }
    
    public void method15789(boolean bool, int i) {
	aBool10164 = bool;
    }
    
    public Class438 method6145() {
	return Class438.method6994(aClass438_10161);
    }
    
    public Class347_Sub2(Class298 class298) {
	super(class298);
    }
    
    public void method15790(Class534_Sub36 class534_sub36) {
	aClass438_10162.aFloat4864
	    = (float) (599803149 * class534_sub36.anInt10797);
	aClass438_10162.aFloat4863
	    = (float) (-1203728391 * class534_sub36.anInt10798);
	aClass438_10162.aFloat4865
	    = (float) (class534_sub36.anInt10799 * 33298755);
	if (Float.isNaN(aClass438_10161.aFloat4864)) {
	    aClass438_10161.method6992(aClass438_10162);
	    aClass438_10163.method6999();
	}
	anInt10160 = class534_sub36.anInt10796 * 1592336477;
    }
    
    public void method6149(Class287 class287, int i, int i_4_, short i_5_) {
	class287.anInt3082
	    = -1803517691 * ((int) aClass438_10161.aFloat4864 - i);
	class287.anInt3083 = -1390317707 * (int) -aClass438_10161.aFloat4863;
	class287.anInt3081
	    = 1552704101 * ((int) aClass438_10161.aFloat4865 - i_4_);
    }
    
    public void method6138(float f, int[][][] is, Class468 class468, int i,
			   int i_6_) {
	if (aBool10164)
	    method15791(is, class468, i, i_6_, -1663786923);
	aClass298_3589.method5352(true, f, aClass438_10161,
				  aClass298_3589.method5391((byte) 34),
				  aClass438_10162, aClass438_10163,
				  (byte) -44);
    }
    
    public Class534_Sub36 method6133(byte i) {
	return new Class534_Sub36(540202667 * anInt10160,
				  (int) aClass438_10161.aFloat4864,
				  (int) aClass438_10161.aFloat4863,
				  (int) aClass438_10161.aFloat4865);
    }
    
    public boolean method6140() {
	return !Float.isNaN(aClass438_10161.aFloat4864);
    }
    
    public boolean method6141() {
	return !Float.isNaN(aClass438_10161.aFloat4864);
    }
    
    public boolean method6147() {
	return !Float.isNaN(aClass438_10161.aFloat4864);
    }
    
    public Class438 method6143() {
	return Class438.method6994(aClass438_10161);
    }
    
    public Class438 method6129() {
	return Class438.method6994(aClass438_10161);
    }
    
    public void method6132(float f, int[][][] is, Class468 class468, int i,
			   int i_7_) {
	if (aBool10164)
	    method15791(is, class468, i, i_7_, -1663786923);
	aClass298_3589.method5352(true, f, aClass438_10161,
				  aClass298_3589.method5391((byte) 96),
				  aClass438_10162, aClass438_10163,
				  (byte) -102);
    }
    
    public Class438 method6146() {
	return Class438.method6994(aClass438_10161);
    }
    
    public double[] method6144() {
	double[] ds = new double[3];
	Class438 class438 = method6137(-2005119331);
	ds[0] = (double) class438.aFloat4864;
	ds[1] = (double) class438.aFloat4863;
	ds[2] = (double) class438.aFloat4865;
	return ds;
    }
    
    public float method6153() {
	return 0.0F;
    }
    
    void method15791(int[][][] is, Class468 class468, int i, int i_8_,
		     int i_9_) {
	if (method6131(-1285270037)
	    && (aClass298_3589.method5400(-694692752)
		|| aClass298_3589.method5485((byte) 40))
	    && aClass298_3589.method5485((byte) 117) && null != is) {
	    int i_10_ = (int) aClass438_10162.aFloat4864 - i >> 9;
	    int i_11_ = (int) aClass438_10162.aFloat4865 - i_8_ >> 9;
	    if (i_10_ >= 0 && i_11_ >= 0 && i_10_ + 1 < is[0].length
		&& i_11_ + 1 < is[0][0].length) {
		int i_12_ = 540202667 * anInt10160;
		if (class468.method7612(i_10_, i_11_, (byte) 0))
		    i_12_ = 1 + anInt10160 * 540202667;
		long l = (long) aClass438_10162.aFloat4864 % 512L;
		long l_13_ = (long) aClass438_10162.aFloat4865 % 512L;
		long l_14_ = ((512L - l) * (long) is[i_12_][i_10_][i_11_]
			      * (512L - l_13_));
		l_14_
		    += (512L - l_13_) * (l
					 * (long) is[i_12_][1 + i_10_][i_11_]);
		l_14_
		    += (long) is[i_12_][i_10_][1 + i_11_] * (512L - l) * l_13_;
		l_14_ += l_13_ * ((long) is[i_12_][1 + i_10_][1 + i_11_] * l);
		l_14_ /= 262144L;
		l_14_ -= 512L;
		float f = (float) -l_14_ - aClass438_10162.aFloat4863;
		if (f > 0.0F)
		    aClass438_10162.aFloat4863 = (float) -l_14_;
	    }
	}
    }
    
    public Class534_Sub36 method6135() {
	return new Class534_Sub36(540202667 * anInt10160,
				  (int) aClass438_10161.aFloat4864,
				  (int) aClass438_10161.aFloat4863,
				  (int) aClass438_10161.aFloat4865);
    }
    
    public void method6151(Class534_Sub40 class534_sub40) {
	aClass438_10162.method7080(class534_sub40);
    }
    
    public void method6152(Class534_Sub40 class534_sub40) {
	aClass438_10162.method7080(class534_sub40);
    }
    
    public void method15792(boolean bool) {
	aBool10164 = bool;
    }
    
    public void method15793(boolean bool) {
	aBool10164 = bool;
    }
    
    void method15794(int[][][] is, Class468 class468, int i, int i_15_) {
	if (method6131(-2134540698)
	    && (aClass298_3589.method5400(1229340960)
		|| aClass298_3589.method5485((byte) 43))
	    && aClass298_3589.method5485((byte) 75) && null != is) {
	    int i_16_ = (int) aClass438_10162.aFloat4864 - i >> 9;
	    int i_17_ = (int) aClass438_10162.aFloat4865 - i_15_ >> 9;
	    if (i_16_ >= 0 && i_17_ >= 0 && i_16_ + 1 < is[0].length
		&& i_17_ + 1 < is[0][0].length) {
		int i_18_ = 540202667 * anInt10160;
		if (class468.method7612(i_16_, i_17_, (byte) 0))
		    i_18_ = 1 + anInt10160 * 540202667;
		long l = (long) aClass438_10162.aFloat4864 % 512L;
		long l_19_ = (long) aClass438_10162.aFloat4865 % 512L;
		long l_20_ = ((512L - l) * (long) is[i_18_][i_16_][i_17_]
			      * (512L - l_19_));
		l_20_
		    += (512L - l_19_) * (l
					 * (long) is[i_18_][1 + i_16_][i_17_]);
		l_20_
		    += (long) is[i_18_][i_16_][1 + i_17_] * (512L - l) * l_19_;
		l_20_ += l_19_ * ((long) is[i_18_][1 + i_16_][1 + i_17_] * l);
		l_20_ /= 262144L;
		l_20_ -= 512L;
		float f = (float) -l_20_ - aClass438_10162.aFloat4863;
		if (f > 0.0F)
		    aClass438_10162.aFloat4863 = (float) -l_20_;
	    }
	}
    }
    
    public void method15795(boolean bool) {
	aBool10164 = bool;
    }
    
    public void method6150(Class287 class287, int i, int i_21_) {
	class287.anInt3082
	    = -1803517691 * ((int) aClass438_10161.aFloat4864 - i);
	class287.anInt3083 = -1390317707 * (int) -aClass438_10161.aFloat4863;
	class287.anInt3081
	    = 1552704101 * ((int) aClass438_10161.aFloat4865 - i_21_);
    }
    
    void method15796(int[][][] is, Class468 class468, int i, int i_22_) {
	if (method6131(-1126966756)
	    && (aClass298_3589.method5400(-1167591845)
		|| aClass298_3589.method5485((byte) 96))
	    && aClass298_3589.method5485((byte) 94) && null != is) {
	    int i_23_ = (int) aClass438_10162.aFloat4864 - i >> 9;
	    int i_24_ = (int) aClass438_10162.aFloat4865 - i_22_ >> 9;
	    if (i_23_ >= 0 && i_24_ >= 0 && i_23_ + 1 < is[0].length
		&& i_24_ + 1 < is[0][0].length) {
		int i_25_ = 540202667 * anInt10160;
		if (class468.method7612(i_23_, i_24_, (byte) 0))
		    i_25_ = 1 + anInt10160 * 540202667;
		long l = (long) aClass438_10162.aFloat4864 % 512L;
		long l_26_ = (long) aClass438_10162.aFloat4865 % 512L;
		long l_27_ = ((512L - l) * (long) is[i_25_][i_23_][i_24_]
			      * (512L - l_26_));
		l_27_
		    += (512L - l_26_) * (l
					 * (long) is[i_25_][1 + i_23_][i_24_]);
		l_27_
		    += (long) is[i_25_][i_23_][1 + i_24_] * (512L - l) * l_26_;
		l_27_ += l_26_ * ((long) is[i_25_][1 + i_23_][1 + i_24_] * l);
		l_27_ /= 262144L;
		l_27_ -= 512L;
		float f = (float) -l_27_ - aClass438_10162.aFloat4863;
		if (f > 0.0F)
		    aClass438_10162.aFloat4863 = (float) -l_27_;
	    }
	}
    }
    
    void method15797(int[][][] is, Class468 class468, int i, int i_28_) {
	if (method6131(-280544811)
	    && (aClass298_3589.method5400(-627881539)
		|| aClass298_3589.method5485((byte) 42))
	    && aClass298_3589.method5485((byte) 36) && null != is) {
	    int i_29_ = (int) aClass438_10162.aFloat4864 - i >> 9;
	    int i_30_ = (int) aClass438_10162.aFloat4865 - i_28_ >> 9;
	    if (i_29_ >= 0 && i_30_ >= 0 && i_29_ + 1 < is[0].length
		&& i_30_ + 1 < is[0][0].length) {
		int i_31_ = 540202667 * anInt10160;
		if (class468.method7612(i_29_, i_30_, (byte) 0))
		    i_31_ = 1 + anInt10160 * 540202667;
		long l = (long) aClass438_10162.aFloat4864 % 512L;
		long l_32_ = (long) aClass438_10162.aFloat4865 % 512L;
		long l_33_ = ((512L - l) * (long) is[i_31_][i_29_][i_30_]
			      * (512L - l_32_));
		l_33_
		    += (512L - l_32_) * (l
					 * (long) is[i_31_][1 + i_29_][i_30_]);
		l_33_
		    += (long) is[i_31_][i_29_][1 + i_30_] * (512L - l) * l_32_;
		l_33_ += l_32_ * ((long) is[i_31_][1 + i_29_][1 + i_30_] * l);
		l_33_ /= 262144L;
		l_33_ -= 512L;
		float f = (float) -l_33_ - aClass438_10162.aFloat4863;
		if (f > 0.0F)
		    aClass438_10162.aFloat4863 = (float) -l_33_;
	    }
	}
    }
    
    void method15798(int[][][] is, Class468 class468, int i, int i_34_) {
	if (method6131(-1260270450)
	    && (aClass298_3589.method5400(-1503397244)
		|| aClass298_3589.method5485((byte) 117))
	    && aClass298_3589.method5485((byte) 86) && null != is) {
	    int i_35_ = (int) aClass438_10162.aFloat4864 - i >> 9;
	    int i_36_ = (int) aClass438_10162.aFloat4865 - i_34_ >> 9;
	    if (i_35_ >= 0 && i_36_ >= 0 && i_35_ + 1 < is[0].length
		&& i_36_ + 1 < is[0][0].length) {
		int i_37_ = 540202667 * anInt10160;
		if (class468.method7612(i_35_, i_36_, (byte) 0))
		    i_37_ = 1 + anInt10160 * 540202667;
		long l = (long) aClass438_10162.aFloat4864 % 512L;
		long l_38_ = (long) aClass438_10162.aFloat4865 % 512L;
		long l_39_ = ((512L - l) * (long) is[i_37_][i_35_][i_36_]
			      * (512L - l_38_));
		l_39_
		    += (512L - l_38_) * (l
					 * (long) is[i_37_][1 + i_35_][i_36_]);
		l_39_
		    += (long) is[i_37_][i_35_][1 + i_36_] * (512L - l) * l_38_;
		l_39_ += l_38_ * ((long) is[i_37_][1 + i_35_][1 + i_36_] * l);
		l_39_ /= 262144L;
		l_39_ -= 512L;
		float f = (float) -l_39_ - aClass438_10162.aFloat4863;
		if (f > 0.0F)
		    aClass438_10162.aFloat4863 = (float) -l_39_;
	    }
	}
    }
    
    void method15799(int[][][] is, Class468 class468, int i, int i_40_) {
	if (method6131(-2044999321)
	    && (aClass298_3589.method5400(882949931)
		|| aClass298_3589.method5485((byte) 112))
	    && aClass298_3589.method5485((byte) 60) && null != is) {
	    int i_41_ = (int) aClass438_10162.aFloat4864 - i >> 9;
	    int i_42_ = (int) aClass438_10162.aFloat4865 - i_40_ >> 9;
	    if (i_41_ >= 0 && i_42_ >= 0 && i_41_ + 1 < is[0].length
		&& i_42_ + 1 < is[0][0].length) {
		int i_43_ = 540202667 * anInt10160;
		if (class468.method7612(i_41_, i_42_, (byte) 0))
		    i_43_ = 1 + anInt10160 * 540202667;
		long l = (long) aClass438_10162.aFloat4864 % 512L;
		long l_44_ = (long) aClass438_10162.aFloat4865 % 512L;
		long l_45_ = ((512L - l) * (long) is[i_43_][i_41_][i_42_]
			      * (512L - l_44_));
		l_45_
		    += (512L - l_44_) * (l
					 * (long) is[i_43_][1 + i_41_][i_42_]);
		l_45_
		    += (long) is[i_43_][i_41_][1 + i_42_] * (512L - l) * l_44_;
		l_45_ += l_44_ * ((long) is[i_43_][1 + i_41_][1 + i_42_] * l);
		l_45_ /= 262144L;
		l_45_ -= 512L;
		float f = (float) -l_45_ - aClass438_10162.aFloat4863;
		if (f > 0.0F)
		    aClass438_10162.aFloat4863 = (float) -l_45_;
	    }
	}
    }
    
    public void method15800(Class534_Sub36 class534_sub36) {
	aClass438_10162.aFloat4864
	    = (float) (599803149 * class534_sub36.anInt10797);
	aClass438_10162.aFloat4863
	    = (float) (-1203728391 * class534_sub36.anInt10798);
	aClass438_10162.aFloat4865
	    = (float) (class534_sub36.anInt10799 * 33298755);
	if (Float.isNaN(aClass438_10161.aFloat4864)) {
	    aClass438_10161.method6992(aClass438_10162);
	    aClass438_10163.method6999();
	}
	anInt10160 = class534_sub36.anInt10796 * 1592336477;
    }
    
    public float method6154() {
	return 0.0F;
    }
}
