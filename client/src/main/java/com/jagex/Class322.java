/* Class322 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class322 implements Interface32
{
    Class393 aClass393_3413;
    Class472 aClass472_3414;
    Class472 aClass472_3415;
    Class171 aClass171_3416;
    long aLong3417 = 2139800476556346893L;
    int anInt3418;
    public static Class654_Sub1_Sub5_Sub1_Sub2 aClass654_Sub1_Sub5_Sub1_Sub2_3419;
    static Class16 aClass16_3420;
    
    public boolean method200() {
	boolean bool = true;
	if (!aClass472_3414.method7670(aClass393_3413.anInt4090 * -513306215,
				       (byte) -116))
	    bool = false;
	if (!aClass472_3415.method7670(-513306215 * aClass393_3413.anInt4090,
				       (byte) -18))
	    bool = false;
	return bool;
    }
    
    public void method205(boolean bool, byte i) {
	int i_0_
	    = (aClass393_3413.aClass401_4087
		   .method6586(0, -321474631 * client.anInt11047, -1269354195)
	       + 1897659751 * aClass393_3413.anInt4088);
	int i_1_
	    = (aClass393_3413.aClass391_4084
		   .method6544(0, 43072843 * client.anInt11192, (byte) 115)
	       + aClass393_3413.anInt4089 * 882406353);
	String string;
	switch (-1781625265 * aClass393_3413.anInt4092) {
	case 0:
	    string = Class277.aClass300_3049.method5544((byte) -13);
	    break;
	default:
	    string = "";
	    break;
	case 2:
	    string = Class277.aClass300_3049.method5526((byte) -86);
	    break;
	case 1:
	    string
		= new StringBuilder().append("").append
		      (Class277.aClass300_3049.method5525(-1787726803)).append
		      ("%").toString();
	}
	int i_2_ = Class277.aClass300_3049.method5525(1095536512);
	if (aLong3417 * -8092573913604323525L < 0L || i_2_ == 0
	    || i_2_ != 1198604243 * anInt3418) {
	    aLong3417
		= Class250.method4604((byte) -99) * -2139800476556346893L;
	    anInt3418 = i_2_ * 614887515;
	}
	if (aClass393_3413.anInt4092 * -1781625265 != 1
	    && (Class250.method4604((byte) -31)
		- -8092573913604323525L * aLong3417) > 10000L)
	    string
		= new StringBuilder().append(string).append(" (").append
		      (Class277.aClass300_3049.method5529((byte) 75).anInt3148
		       * -1262216053)
		      .append
		      (")").toString();
	aClass171_3416.method2830(string, i_0_, i_1_,
				  1112579143 * aClass393_3413.anInt4083, -1,
				  65535);
    }
    
    Class322(Class472 class472, Class472 class472_3_, Class393 class393) {
	aClass472_3414 = class472;
	aClass472_3415 = class472_3_;
	aClass393_3413 = class393;
    }
    
    public void method201(short i) {
	Class16 class16
	    = Class20.method804(aClass472_3415,
				aClass393_3413.anInt4090 * -513306215,
				Class351.aClass406_3620, -103210469);
	aClass171_3416
	    = (Class254.aClass185_2683.method3325
	       (class16,
		Class178.method2942(aClass472_3414,
				    aClass393_3413.anInt4090 * -513306215),
		true));
    }
    
    public void method202(boolean bool) {
	int i
	    = (aClass393_3413.aClass401_4087
		   .method6586(0, -321474631 * client.anInt11047, -1932350247)
	       + 1897659751 * aClass393_3413.anInt4088);
	int i_4_ = (aClass393_3413.aClass391_4084
			.method6544(0, 43072843 * client.anInt11192, (byte) 48)
		    + aClass393_3413.anInt4089 * 882406353);
	String string;
	switch (-1781625265 * aClass393_3413.anInt4092) {
	case 0:
	    string = Class277.aClass300_3049.method5544((byte) -28);
	    break;
	default:
	    string = "";
	    break;
	case 2:
	    string = Class277.aClass300_3049.method5526((byte) -57);
	    break;
	case 1:
	    string
		= new StringBuilder().append("").append
		      (Class277.aClass300_3049.method5525(-794386242)).append
		      ("%").toString();
	}
	int i_5_ = Class277.aClass300_3049.method5525(1884587276);
	if (aLong3417 * -8092573913604323525L < 0L || i_5_ == 0
	    || i_5_ != 1198604243 * anInt3418) {
	    aLong3417
		= Class250.method4604((byte) -111) * -2139800476556346893L;
	    anInt3418 = i_5_ * 614887515;
	}
	if (aClass393_3413.anInt4092 * -1781625265 != 1
	    && (Class250.method4604((byte) -21)
		- -8092573913604323525L * aLong3417) > 10000L)
	    string
		= new StringBuilder().append(string).append(" (").append
		      (Class277.aClass300_3049.method5529((byte) 53).anInt3148
		       * -1262216053)
		      .append
		      (")").toString();
	aClass171_3416.method2830(string, i, i_4_,
				  1112579143 * aClass393_3413.anInt4083, -1,
				  65535);
    }
    
    public void method203() {
	Class16 class16
	    = Class20.method804(aClass472_3415,
				aClass393_3413.anInt4090 * -513306215,
				Class351.aClass406_3620, -103210469);
	aClass171_3416
	    = (Class254.aClass185_2683.method3325
	       (class16,
		Class178.method2942(aClass472_3414,
				    aClass393_3413.anInt4090 * -513306215),
		true));
    }
    
    public boolean method199(int i) {
	boolean bool = true;
	if (!aClass472_3414.method7670(aClass393_3413.anInt4090 * -513306215,
				       (byte) -123))
	    bool = false;
	if (!aClass472_3415.method7670(-513306215 * aClass393_3413.anInt4090,
				       (byte) -83))
	    bool = false;
	return bool;
    }
    
    public boolean method204() {
	boolean bool = true;
	if (!aClass472_3414.method7670(aClass393_3413.anInt4090 * -513306215,
				       (byte) -91))
	    bool = false;
	if (!aClass472_3415.method7670(-513306215 * aClass393_3413.anInt4090,
				       (byte) -27))
	    bool = false;
	return bool;
    }
    
    public boolean method208() {
	boolean bool = true;
	if (!aClass472_3414.method7670(aClass393_3413.anInt4090 * -513306215,
				       (byte) -55))
	    bool = false;
	if (!aClass472_3415.method7670(-513306215 * aClass393_3413.anInt4090,
				       (byte) -7))
	    bool = false;
	return bool;
    }
    
    public boolean method207() {
	boolean bool = true;
	if (!aClass472_3414.method7670(aClass393_3413.anInt4090 * -513306215,
				       (byte) -120))
	    bool = false;
	if (!aClass472_3415.method7670(-513306215 * aClass393_3413.anInt4090,
				       (byte) -26))
	    bool = false;
	return bool;
    }
    
    public boolean method206() {
	boolean bool = true;
	if (!aClass472_3414.method7670(aClass393_3413.anInt4090 * -513306215,
				       (byte) -4))
	    bool = false;
	if (!aClass472_3415.method7670(-513306215 * aClass393_3413.anInt4090,
				       (byte) -31))
	    bool = false;
	return bool;
    }
    
    public static int method5778(int i, byte i_6_) {
	Class534_Sub18_Sub14 class534_sub18_sub14
	    = (Class534_Sub18_Sub14) Class274.aClass9_3036.method579((long) i);
	if (null == class534_sub18_sub14)
	    return -1;
	if (Class274.aClass696_3038.aClass534_Sub18_8785
	    == class534_sub18_sub14.aClass534_Sub18_10508)
	    return -1;
	return (((Class534_Sub18_Sub14)
		 class534_sub18_sub14.aClass534_Sub18_10508).anInt11808
		* -759944081);
    }
    
    public static Class550 method5779(boolean bool, int i) {
	synchronized (Class550.aStack7304) {
	    Class550 class550;
	    if (Class550.aStack7304.isEmpty())
		class550 = new Class550();
	    else
		class550 = (Class550) Class550.aStack7304.pop();
	    class550.aBool7305 = bool;
	    Class550 class550_7_ = class550;
	    return class550_7_;
	}
    }
    
    static final void method5780(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 1;
    }
    
    static final void method5781(Class669 class669, int i) {
	Class534_Sub36 class534_sub36
	    = Class200_Sub19.method15634(((Class534_Sub36)
					  (class669.anObjectArray8593
					   [((class669.anInt8594 -= 1460193483)
					     * 1485266147)])),
					 -1663700774);
	class669.anInt8600 -= 1235998252;
	class534_sub36.anInt10796
	    += (class669.anIntArray8595[class669.anInt8600 * 2088438307]
		* 1166548959);
	class534_sub36.anInt10797
	    += -1224142395 * (class669.anIntArray8595
			      [1 + 2088438307 * class669.anInt8600]);
	class534_sub36.anInt10798
	    += -1544617399 * (class669.anIntArray8595
			      [class669.anInt8600 * 2088438307 + 2]);
	class534_sub36.anInt10799
	    += (class669.anIntArray8595[2088438307 * class669.anInt8600 + 3]
		* 646265707);
	class669.anObjectArray8593
	    [(class669.anInt8594 += 1460193483) * 1485266147 - 1]
	    = class534_sub36;
    }
}
