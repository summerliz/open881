/* Class534_Sub18_Sub6 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class534_Sub18_Sub6 extends Class534_Sub18
{
    static Class696 aClass696_11659 = new Class696();
    int anInt11660;
    int anInt11661;
    static final int anInt11662 = 21;
    static final int anInt11663 = 23;
    static final int anInt11664 = 4;
    static final int anInt11665 = 3;
    int anInt11666;
    static final int anInt11667 = 5;
    static final int anInt11668 = 6;
    static Class696 aClass696_11669 = new Class696();
    static final int anInt11670 = 8;
    static final int anInt11671 = 13;
    static final int anInt11672 = 7;
    static final int anInt11673 = 11;
    static final int anInt11674 = 12;
    static final int anInt11675 = 10;
    static final int anInt11676 = 14;
    String aString11677;
    static final int anInt11678 = 17;
    static final int anInt11679 = 20;
    static final int anInt11680 = 15;
    static final int anInt11681 = 22;
    static final int anInt11682 = 9;
    static final int anInt11683 = 2;
    static final int anInt11684 = 1;
    static Class9 aClass9_11685 = new Class9(16);
    static final long aLong11686 = 9223372036854775807L;
    static final long aLong11687 = -9223372036854775808L;
    static final long aLong11688 = 500L;
    static boolean aBool11689 = false;
    
    public static void method18119(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(3, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    static void method18120(int i, int i_0_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(5, (long) i);
	class534_sub18_sub6.method18121(-593518159);
	class534_sub18_sub6.anInt11666 = 517206857 * i_0_;
    }
    
    void method18121(int i) {
	aLong10509 = 84410810002887935L * (aLong10509 * -753566336061658369L
					   | ~0x7fffffffffffffffL);
	if (method18159(-134804279) == 0L)
	    aClass696_11659.method14076(this, (byte) 18);
    }
    
    int method18122(int i) {
	return (int) (aLong7158 * 8258869577519436579L >>> 56 & 0xffL);
    }
    
    long method18123(int i) {
	return 8258869577519436579L * aLong7158 & 0xffffffffffffffL;
    }
    
    void method18124() {
	aLong10509
	    = (-753566336061658369L * aLong10509 & ~0x7fffffffffffffffL
	       | Class250.method4604((byte) -111) + 500L) * 84410810002887935L;
	aClass696_11669.method14076(this, (byte) 89);
    }
    
    static Class534_Sub18_Sub6 method18125(int i, long l) {
	aBool11689 = false;
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = ((Class534_Sub18_Sub6)
	       aClass9_11685.method579((long) i << 56 | l));
	if (null == class534_sub18_sub6) {
	    class534_sub18_sub6 = new Class534_Sub18_Sub6(i, l);
	    aClass9_11685.method581(class534_sub18_sub6,
				    (8258869577519436579L
				     * class534_sub18_sub6.aLong7158));
	    aBool11689 = true;
	}
	return class534_sub18_sub6;
    }
    
    Class534_Sub18_Sub6(int i, long l) {
	aLong7158 = ((long) i << 56 | l) * 936217890172187787L;
    }
    
    static Class534_Sub18_Sub6 method18126(int i, long l) {
	aBool11689 = false;
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = ((Class534_Sub18_Sub6)
	       aClass9_11685.method579((long) i << 56 | l));
	if (null == class534_sub18_sub6) {
	    class534_sub18_sub6 = new Class534_Sub18_Sub6(i, l);
	    aClass9_11685.method581(class534_sub18_sub6,
				    (8258869577519436579L
				     * class534_sub18_sub6.aLong7158));
	    aBool11689 = true;
	}
	return class534_sub18_sub6;
    }
    
    static void method18127(int i, boolean bool) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(23, (long) i);
	class534_sub18_sub6.method18121(361652371);
	class534_sub18_sub6.anInt11666 = 517206857 * (bool ? 1 : 0);
    }
    
    static void method18128() {
	aClass9_11685.method578((byte) 12);
	aClass696_11669.method14075(958896847);
	aClass696_11659.method14075(958896847);
    }
    
    static void method18129() {
	aClass9_11685.method578((byte) -53);
	aClass696_11669.method14075(958896847);
	aClass696_11659.method14075(958896847);
    }
    
    static Class534_Sub18_Sub6 method18130() {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = (Class534_Sub18_Sub6) aClass696_11659.method14078(1221951837);
	if (null != class534_sub18_sub6) {
	    class534_sub18_sub6.method8892((byte) 1);
	    class534_sub18_sub6.method16180(-421776830);
	    return class534_sub18_sub6;
	}
	for (;;) {
	    class534_sub18_sub6 = ((Class534_Sub18_Sub6)
				   aClass696_11669.method14078(1221951837));
	    if (class534_sub18_sub6 == null)
		return null;
	    if (class534_sub18_sub6.method18159(-134804279)
		> Class250.method4604((byte) -96))
		return null;
	    class534_sub18_sub6.method8892((byte) 1);
	    class534_sub18_sub6.method16180(-421776830);
	    if (0L != (class534_sub18_sub6.aLong10509 * -753566336061658369L
		       & ~0x7fffffffffffffffL))
		return class534_sub18_sub6;
	}
    }
    
    static void method18131(int i, int i_1_, int i_2_, int i_3_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(4, (long) i);
	class534_sub18_sub6.method18121(-466731628);
	class534_sub18_sub6.anInt11666 = 517206857 * i_1_;
	class534_sub18_sub6.anInt11660 = i_2_ * -1621355885;
	class534_sub18_sub6.anInt11661 = i_3_ * -105177451;
    }
    
    static void method18132(int i, int i_4_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(6, (long) i);
	class534_sub18_sub6.method18121(-1957282316);
	class534_sub18_sub6.anInt11666 = i_4_ * 517206857;
    }
    
    public static void method18133(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(13, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    static void method18134(int i, int i_5_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(7, (long) i);
	class534_sub18_sub6.method18121(1145738347);
	class534_sub18_sub6.anInt11666 = i_5_ * 517206857;
    }
    
    public static void method18135(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(3, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    void method18136() {
	aLong10509 = 84410810002887935L * (aLong10509 * -753566336061658369L
					   | ~0x7fffffffffffffffL);
	if (method18159(-134804279) == 0L)
	    aClass696_11659.method14076(this, (byte) 11);
    }
    
    public static void method18137(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(23, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    static void method18138(int i, int i_6_, int i_7_, int i_8_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(4, (long) i);
	class534_sub18_sub6.method18121(425761885);
	class534_sub18_sub6.anInt11666 = 517206857 * i_6_;
	class534_sub18_sub6.anInt11660 = i_7_ * -1621355885;
	class534_sub18_sub6.anInt11661 = i_8_ * -105177451;
    }
    
    public static void method18139(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(4, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    public static void method18140(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(3, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    public static void method18141(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(5, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    public static void method18142(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(12, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    public static void method18143(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(7, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    public static void method18144(int i, int i_9_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(17, (long) i_9_ << 32 | (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    public static void method18145(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(7, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    public static void method18146(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(7, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    public static void method18147(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(7, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    public static void method18148(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(7, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    public static void method18149(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(8, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    public static void method18150(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(8, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    public static void method18151(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(10, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    public static void method18152(Class150 class150) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(1,
				  (long) (class150.anInt1694 * -1270946121));
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    public static void method18153(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(11, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    static void method18154(int i, int i_10_, int i_11_, int i_12_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(8, (long) i);
	class534_sub18_sub6.method18121(568957232);
	class534_sub18_sub6.anInt11666 = 517206857 * i_10_;
	class534_sub18_sub6.anInt11660 = i_11_ * -1621355885;
	class534_sub18_sub6.anInt11661 = i_12_ * -105177451;
    }
    
    static void method18155(int i, int i_13_, int i_14_, int i_15_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(10, (long) i);
	class534_sub18_sub6.method18121(-353504674);
	class534_sub18_sub6.anInt11666 = i_13_ * 517206857;
	class534_sub18_sub6.anInt11660 = i_14_ * -1621355885;
	class534_sub18_sub6.anInt11661 = -105177451 * i_15_;
    }
    
    long method18156() {
	return aLong10509 * -753566336061658369L & 0x7fffffffffffffffL;
    }
    
    public static void method18157(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(13, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    public static void method18158(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(13, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    long method18159(int i) {
	return aLong10509 * -753566336061658369L & 0x7fffffffffffffffL;
    }
    
    public static void method18160(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(15, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    public static void method18161(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(15, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    public static void method18162(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(21, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    static void method18163(int i, int i_16_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(6, (long) i);
	class534_sub18_sub6.method18121(61712317);
	class534_sub18_sub6.anInt11666 = i_16_ * 517206857;
    }
    
    static void method18164() {
	Class534_Sub18_Sub6 class534_sub18_sub6 = Class447.method7308(14, 0L);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    static void method18165() {
	Class534_Sub18_Sub6 class534_sub18_sub6 = Class447.method7308(14, 0L);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    void method18166() {
	aLong10509 = 84410810002887935L * (aLong10509 * -753566336061658369L
					   | ~0x7fffffffffffffffL);
	if (method18159(-134804279) == 0L)
	    aClass696_11659.method14076(this, (byte) 97);
    }
    
    static void method18167() {
	Class534_Sub18_Sub6 class534_sub18_sub6 = Class447.method7308(14, 0L);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    static void method18168() {
	Class534_Sub18_Sub6 class534_sub18_sub6 = Class447.method7308(14, 0L);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    public static void method18169(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(6, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    public static void method18170(int i, int i_17_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(20, (long) i_17_ << 32 | (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    public static void method18171(int i, int i_18_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(20, (long) i_18_ << 32 | (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    static void method18172(int i, int i_19_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(1, (long) i);
	class534_sub18_sub6.method18121(-1872097216);
	class534_sub18_sub6.anInt11666 = 517206857 * i_19_;
    }
    
    static void method18173(int i, int i_20_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(1, (long) i);
	class534_sub18_sub6.method18121(257949934);
	class534_sub18_sub6.anInt11666 = 517206857 * i_20_;
    }
    
    public static void method18174(Class150 class150) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(1,
				  (long) (class150.anInt1694 * -1270946121));
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    static void method18175(int i, int i_21_) {
	Class318 class318
	    = (Class318) Class84.aClass44_Sub11_840.method91(i, 1473470550);
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(1, (long) (class318.aClass150_3392.anInt1694
					     * -1270946121));
	try {
	    if (aBool11689)
		class534_sub18_sub6.anInt11666
		    = (Class77.aClass155_Sub1_819
			   .method120(class318.aClass150_3392, (byte) -80)
		       * 517206857);
	    class534_sub18_sub6.anInt11666
		= class318.method5748((class534_sub18_sub6.anInt11666
				       * -1479053575),
				      i_21_, (byte) -7) * 517206857;
	    class534_sub18_sub6.method18121(-196864156);
	} catch (Exception_Sub6 exception_sub6) {
	    Class262.method4824(new StringBuilder().append("").append(i)
				    .toString(),
				exception_sub6, (byte) 51);
	}
    }
    
    static void method18176(int i, String string) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(2, (long) i);
	class534_sub18_sub6.method18121(1170998470);
	class534_sub18_sub6.aString11677 = string;
    }
    
    static void method18177(int i, String string) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(3, (long) i);
	class534_sub18_sub6.method18121(877628367);
	class534_sub18_sub6.aString11677 = string;
    }
    
    static void method18178(int i, String string) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(3, (long) i);
	class534_sub18_sub6.method18121(-629845670);
	class534_sub18_sub6.aString11677 = string;
    }
    
    public static void method18179(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(22, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    static void method18180(int i, int i_22_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(6, (long) i);
	class534_sub18_sub6.method18121(632110875);
	class534_sub18_sub6.anInt11666 = i_22_ * 517206857;
    }
    
    static void method18181(int i, int i_23_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(13, (long) i);
	class534_sub18_sub6.method18121(-1482387910);
	class534_sub18_sub6.anInt11666 = i_23_ * 517206857;
    }
    
    void method18182(int i) {
	aLong10509
	    = (-753566336061658369L * aLong10509 & ~0x7fffffffffffffffL
	       | Class250.method4604((byte) -85) + 500L) * 84410810002887935L;
	aClass696_11669.method14076(this, (byte) 76);
    }
    
    static void method18183(int i, int i_24_, int i_25_, int i_26_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(4, (long) i);
	class534_sub18_sub6.method18121(-1422269843);
	class534_sub18_sub6.anInt11666 = 517206857 * i_24_;
	class534_sub18_sub6.anInt11660 = i_25_ * -1621355885;
	class534_sub18_sub6.anInt11661 = i_26_ * -105177451;
    }
    
    static void method18184(int i, int i_27_) {
	Class534_Sub18_Sub6 class534_sub18_sub6 = Class447.method7308(14, 0L);
	class534_sub18_sub6.method18121(337325428);
	class534_sub18_sub6.anInt11666 = 517206857 * i;
	class534_sub18_sub6.anInt11660 = -1621355885 * i_27_;
    }
    
    static void method18185(int i, int i_28_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(5, (long) i);
	class534_sub18_sub6.method18121(774326607);
	class534_sub18_sub6.anInt11666 = 517206857 * i_28_;
    }
    
    static void method18186(int i, int i_29_, int i_30_, int i_31_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(17, (long) i_29_ << 32 | (long) i);
	class534_sub18_sub6.method18121(-906927526);
	class534_sub18_sub6.anInt11666 = 517206857 * i_30_;
	class534_sub18_sub6.anInt11660 = i_31_ * -1621355885;
    }
    
    static void method18187(int i, int i_32_) {
	Class318 class318
	    = (Class318) Class84.aClass44_Sub11_840.method91(i, -299796957);
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(1, (long) (class318.aClass150_3392.anInt1694
					     * -1270946121));
	try {
	    if (aBool11689)
		class534_sub18_sub6.anInt11666
		    = (Class77.aClass155_Sub1_819
			   .method120(class318.aClass150_3392, (byte) -64)
		       * 517206857);
	    class534_sub18_sub6.anInt11666
		= class318.method5748((class534_sub18_sub6.anInt11666
				       * -1479053575),
				      i_32_, (byte) -2) * 517206857;
	    class534_sub18_sub6.method18121(-2061452205);
	} catch (Exception_Sub6 exception_sub6) {
	    Class262.method4824(new StringBuilder().append("").append(i)
				    .toString(),
				exception_sub6, (byte) -23);
	}
    }
    
    static void method18188(int i, int i_33_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(6, (long) i);
	class534_sub18_sub6.method18121(520587951);
	class534_sub18_sub6.anInt11666 = i_33_ * 517206857;
    }
    
    static void method18189(int i, int i_34_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(6, (long) i);
	class534_sub18_sub6.method18121(840096687);
	class534_sub18_sub6.anInt11666 = i_34_ * 517206857;
    }
    
    static void method18190() {
	Class534_Sub18_Sub6 class534_sub18_sub6 = Class447.method7308(14, 0L);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    static void method18191(int i, int i_35_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(15, (long) i);
	class534_sub18_sub6.method18121(783729415);
	class534_sub18_sub6.anInt11666 = i_35_ * 517206857;
    }
    
    static void method18192(int i, int i_36_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(7, (long) i);
	class534_sub18_sub6.method18121(575299069);
	class534_sub18_sub6.anInt11666 = i_36_ * 517206857;
    }
    
    static void method18193(int i, int i_37_, int i_38_, int i_39_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(8, (long) i);
	class534_sub18_sub6.method18121(-1583483535);
	class534_sub18_sub6.anInt11666 = 517206857 * i_37_;
	class534_sub18_sub6.anInt11660 = i_38_ * -1621355885;
	class534_sub18_sub6.anInt11661 = i_39_ * -105177451;
    }
    
    public static void method18194(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(12, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    static void method18195(int i, int i_40_, int i_41_, int i_42_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(8, (long) i);
	class534_sub18_sub6.method18121(332134342);
	class534_sub18_sub6.anInt11666 = 517206857 * i_40_;
	class534_sub18_sub6.anInt11660 = i_41_ * -1621355885;
	class534_sub18_sub6.anInt11661 = i_42_ * -105177451;
    }
    
    static void method18196(int i, int i_43_, int i_44_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(9, (long) i);
	class534_sub18_sub6.method18121(179003570);
	class534_sub18_sub6.anInt11666 = i_43_ * 517206857;
	class534_sub18_sub6.anInt11660 = -1621355885 * i_44_;
    }
    
    static void method18197(int i, int i_45_, int i_46_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(9, (long) i);
	class534_sub18_sub6.method18121(522683027);
	class534_sub18_sub6.anInt11666 = i_45_ * 517206857;
	class534_sub18_sub6.anInt11660 = -1621355885 * i_46_;
    }
    
    static void method18198(int i, int i_47_, int i_48_, int i_49_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(10, (long) i);
	class534_sub18_sub6.method18121(-91455860);
	class534_sub18_sub6.anInt11666 = i_47_ * 517206857;
	class534_sub18_sub6.anInt11660 = i_48_ * -1621355885;
	class534_sub18_sub6.anInt11661 = -105177451 * i_49_;
    }
    
    static void method18199(int i, int i_50_, int i_51_, int i_52_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(10, (long) i);
	class534_sub18_sub6.method18121(-1562783931);
	class534_sub18_sub6.anInt11666 = i_50_ * 517206857;
	class534_sub18_sub6.anInt11660 = i_51_ * -1621355885;
	class534_sub18_sub6.anInt11661 = -105177451 * i_52_;
    }
    
    static void method18200(int i, int i_53_, int i_54_, int i_55_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(10, (long) i);
	class534_sub18_sub6.method18121(-745768671);
	class534_sub18_sub6.anInt11666 = i_53_ * 517206857;
	class534_sub18_sub6.anInt11660 = i_54_ * -1621355885;
	class534_sub18_sub6.anInt11661 = -105177451 * i_55_;
    }
    
    static Class534_Sub18_Sub6 method18201(int i, long l) {
	aBool11689 = false;
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = ((Class534_Sub18_Sub6)
	       aClass9_11685.method579((long) i << 56 | l));
	if (null == class534_sub18_sub6) {
	    class534_sub18_sub6 = new Class534_Sub18_Sub6(i, l);
	    aClass9_11685.method581(class534_sub18_sub6,
				    (8258869577519436579L
				     * class534_sub18_sub6.aLong7158));
	    aBool11689 = true;
	}
	return class534_sub18_sub6;
    }
    
    static void method18202(int i, int i_56_, int i_57_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(11, (long) i);
	class534_sub18_sub6.method18121(431238004);
	class534_sub18_sub6.anInt11666 = i_56_ * 517206857;
	class534_sub18_sub6.anInt11660 = -1621355885 * i_57_;
    }
    
    public static void method18203(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(15, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    static void method18204(int i, int i_58_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(13, (long) i);
	class534_sub18_sub6.method18121(142314207);
	class534_sub18_sub6.anInt11666 = i_58_ * 517206857;
    }
    
    public static void method18205(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(7, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    static void method18206(int i, int i_59_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(15, (long) i);
	class534_sub18_sub6.method18121(-762356431);
	class534_sub18_sub6.anInt11666 = i_59_ * 517206857;
    }
    
    public static void method18207(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(10, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    static void method18208(int i, boolean bool) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(22, (long) i);
	class534_sub18_sub6.method18121(79345552);
	class534_sub18_sub6.anInt11666 = (bool ? 1 : 0) * 517206857;
    }
    
    static void method18209(int i, boolean bool) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(22, (long) i);
	class534_sub18_sub6.method18121(1176507073);
	class534_sub18_sub6.anInt11666 = (bool ? 1 : 0) * 517206857;
    }
    
    static Class534_Sub18_Sub6 method18210(int i, long l) {
	aBool11689 = false;
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = ((Class534_Sub18_Sub6)
	       aClass9_11685.method579((long) i << 56 | l));
	if (null == class534_sub18_sub6) {
	    class534_sub18_sub6 = new Class534_Sub18_Sub6(i, l);
	    aClass9_11685.method581(class534_sub18_sub6,
				    (8258869577519436579L
				     * class534_sub18_sub6.aLong7158));
	    aBool11689 = true;
	}
	return class534_sub18_sub6;
    }
    
    static void method18211(int i, int i_60_) {
	Class534_Sub18_Sub6 class534_sub18_sub6 = Class447.method7308(14, 0L);
	class534_sub18_sub6.method18121(-1094512045);
	class534_sub18_sub6.anInt11666 = 517206857 * i;
	class534_sub18_sub6.anInt11660 = -1621355885 * i_60_;
    }
    
    static void method18212(int i, int i_61_) {
	Class534_Sub18_Sub6 class534_sub18_sub6 = Class447.method7308(14, 0L);
	class534_sub18_sub6.method18121(-1408114766);
	class534_sub18_sub6.anInt11666 = 517206857 * i;
	class534_sub18_sub6.anInt11660 = -1621355885 * i_61_;
    }
    
    static void method18213(int i, int i_62_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(15, (long) i);
	class534_sub18_sub6.method18121(-2125895363);
	class534_sub18_sub6.anInt11666 = i_62_ * 517206857;
    }
    
    static void method18214(int i, int i_63_, int i_64_, int i_65_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(17, (long) i_63_ << 32 | (long) i);
	class534_sub18_sub6.method18121(-1918866577);
	class534_sub18_sub6.anInt11666 = 517206857 * i_64_;
	class534_sub18_sub6.anInt11660 = i_65_ * -1621355885;
    }
    
    static void method18215(int i, int i_66_, int i_67_, int i_68_) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(17, (long) i_66_ << 32 | (long) i);
	class534_sub18_sub6.method18121(-236774397);
	class534_sub18_sub6.anInt11666 = 517206857 * i_67_;
	class534_sub18_sub6.anInt11660 = i_68_ * -1621355885;
    }
    
    public static void method18216(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(5, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    public static void method18217(Class150 class150) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(2,
				  (long) (class150.anInt1694 * -1270946121));
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    public static void method18218(int i) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(23, (long) i);
	class534_sub18_sub6.method18182(-1052637456);
    }
    
    int method18219() {
	return (int) (aLong7158 * 8258869577519436579L >>> 56 & 0xffL);
    }
    
    int method18220() {
	return (int) (aLong7158 * 8258869577519436579L >>> 56 & 0xffL);
    }
    
    long method18221() {
	return aLong10509 * -753566336061658369L & 0x7fffffffffffffffL;
    }
    
    long method18222() {
	return aLong10509 * -753566336061658369L & 0x7fffffffffffffffL;
    }
    
    static void method18223(int i, boolean bool) {
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(23, (long) i);
	class534_sub18_sub6.method18121(309143664);
	class534_sub18_sub6.anInt11666 = 517206857 * (bool ? 1 : 0);
    }
}
