/* Class44_Sub7 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class44_Sub7 extends Class44
{
    public void method17305(boolean bool) {
	((Class12) anInterface6_326).method619(bool, (byte) -14);
	super.method1080(65280);
    }
    
    public Class163 method17306(Class185 class185, int i, int i_0_, int i_1_,
				int i_2_, int i_3_, Class631 class631,
				int i_4_) {
	return ((Class12) anInterface6_326).method617(class185, i, i_0_, i_1_,
						      i_2_, i_3_, class631,
						      1582971671);
    }
    
    public Class163 method17307
	(Class185 class185, Class185 class185_5_, int i, int i_6_, int i_7_,
	 int i_8_, boolean bool, boolean bool_9_, int i_10_, Class171 class171,
	 Class631 class631, Class632 class632, int i_11_) {
	return ((Class12) anInterface6_326).method618(class185, class185_5_, i,
						      i_6_, i_7_, i_8_, bool,
						      bool_9_, i_10_, class171,
						      class631, class632, this,
						      241444018);
    }
    
    public void method17308(boolean bool, int i) {
	((Class12) anInterface6_326).method619(bool, (byte) -103);
	super.method1080(65280);
    }
    
    public void method17309(int i, byte i_12_) {
	((Class12) anInterface6_326).method620(i, 2088438307);
    }
    
    public void method1098() {
	super.method1080(65280);
	((Class12) anInterface6_326).method621(1763094119);
    }
    
    public void method17310() {
	((Class12) anInterface6_326).method623(-1675692882);
    }
    
    public void method1081(int i, short i_13_) {
	super.method1081(i, (short) 15819);
	((Class12) anInterface6_326).method622(i, (byte) -41);
    }
    
    public void method1085(int i) {
	super.method1085(2049951820);
	((Class12) anInterface6_326).method624((byte) -106);
    }
    
    public void method1077() {
	super.method1080(65280);
	((Class12) anInterface6_326).method621(1836134868);
    }
    
    public void method1078() {
	super.method1080(65280);
	((Class12) anInterface6_326).method621(1439055349);
    }
    
    public void method1076() {
	super.method1080(65280);
	((Class12) anInterface6_326).method621(1747591459);
    }
    
    public Class163 method17311
	(Class185 class185, Class185 class185_14_, int i, int i_15_, int i_16_,
	 int i_17_, boolean bool, boolean bool_18_, int i_19_,
	 Class171 class171, Class631 class631, Class632 class632) {
	return ((Class12) anInterface6_326).method618(class185, class185_14_,
						      i, i_15_, i_16_, i_17_,
						      bool, bool_18_, i_19_,
						      class171, class631,
						      class632, this,
						      241444018);
    }
    
    public Class163 method17312
	(Class185 class185, Class185 class185_20_, int i, int i_21_, int i_22_,
	 int i_23_, boolean bool, boolean bool_24_, int i_25_,
	 Class171 class171, Class631 class631, Class632 class632) {
	return ((Class12) anInterface6_326).method618(class185, class185_20_,
						      i, i_21_, i_22_, i_23_,
						      bool, bool_24_, i_25_,
						      class171, class631,
						      class632, this,
						      241444018);
    }
    
    public void method1092(int i) {
	super.method1081(i, (short) 30027);
	((Class12) anInterface6_326).method622(i, (byte) -6);
    }
    
    public void method1087() {
	super.method1085(1947086404);
	((Class12) anInterface6_326).method624((byte) -53);
    }
    
    public void method1088() {
	super.method1085(2112276442);
	((Class12) anInterface6_326).method624((byte) -84);
    }
    
    public void method1089() {
	super.method1085(1957700822);
	((Class12) anInterface6_326).method624((byte) -112);
    }
    
    public void method1084(int i) {
	super.method1081(i, (short) 727);
	((Class12) anInterface6_326).method622(i, (byte) -26);
    }
    
    public Class44_Sub7(Class675 class675, Class672 class672, boolean bool,
			Interface14 interface14, Class472 class472,
			Class472 class472_26_) {
	super(class675, class672, class472, Class649.aClass649_8388, 64,
	      new Class12_Sub1(class675, class672, bool, class472_26_,
			       interface14));
    }
    
    public Class163 method17313(Class185 class185, int i, int i_27_, int i_28_,
				int i_29_, int i_30_, Class631 class631) {
	return ((Class12) anInterface6_326).method617(class185, i, i_27_,
						      i_28_, i_29_, i_30_,
						      class631, 1706033255);
    }
    
    public void method1091() {
	super.method1085(2054465809);
	((Class12) anInterface6_326).method624((byte) -93);
    }
    
    public Class163 method17314(Class185 class185, int i, int i_31_, int i_32_,
				int i_33_, int i_34_, Class631 class631) {
	return ((Class12) anInterface6_326).method617(class185, i, i_31_,
						      i_32_, i_33_, i_34_,
						      class631, 863026948);
    }
    
    public Class163 method17315(Class185 class185, int i, int i_35_, int i_36_,
				int i_37_, int i_38_, Class631 class631) {
	return ((Class12) anInterface6_326).method617(class185, i, i_35_,
						      i_36_, i_37_, i_38_,
						      class631, -317522142);
    }
    
    public void method17316(int i) {
	((Class12) anInterface6_326).method623(-1412288401);
    }
    
    public void method1090() {
	super.method1085(1967097888);
	((Class12) anInterface6_326).method624((byte) -36);
    }
    
    public void method17317(int i) {
	((Class12) anInterface6_326).method620(i, 2088438307);
    }
    
    public void method17318() {
	((Class12) anInterface6_326).method623(-1820060792);
    }
    
    public void method1080(int i) {
	super.method1080(65280);
	((Class12) anInterface6_326).method621(1903337007);
    }
    
    public void method17319() {
	((Class12) anInterface6_326).method623(-1984031283);
    }
    
    public void method17320() {
	((Class12) anInterface6_326).method623(-1483698013);
    }
}
