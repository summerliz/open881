/* Class696 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;
import java.util.Iterator;

public class Class696 implements Iterable
{
    public Class534_Sub18 aClass534_Sub18_8785 = new Class534_Sub18();
    Class534_Sub18 aClass534_Sub18_8786;
    
    public int method14074() {
	int i = 0;
	for (Class534_Sub18 class534_sub18
		 = aClass534_Sub18_8785.aClass534_Sub18_10510;
	     class534_sub18 != aClass534_Sub18_8785;
	     class534_sub18 = class534_sub18.aClass534_Sub18_10510)
	    i++;
	return i;
    }
    
    public void method14075(int i) {
	while (aClass534_Sub18_8785
	       != aClass534_Sub18_8785.aClass534_Sub18_10510)
	    aClass534_Sub18_8785.aClass534_Sub18_10510.method16180(-421776830);
    }
    
    public void method14076(Class534_Sub18 class534_sub18, byte i) {
	if (null != class534_sub18.aClass534_Sub18_10508)
	    class534_sub18.method16180(-421776830);
	class534_sub18.aClass534_Sub18_10508
	    = aClass534_Sub18_8785.aClass534_Sub18_10508;
	class534_sub18.aClass534_Sub18_10510 = aClass534_Sub18_8785;
	class534_sub18.aClass534_Sub18_10508.aClass534_Sub18_10510
	    = class534_sub18;
	class534_sub18.aClass534_Sub18_10510.aClass534_Sub18_10508
	    = class534_sub18;
    }
    
    public Class534_Sub18 method14077(byte i) {
	Class534_Sub18 class534_sub18
	    = aClass534_Sub18_8785.aClass534_Sub18_10510;
	if (class534_sub18 == aClass534_Sub18_8785)
	    return null;
	class534_sub18.method16180(-421776830);
	return class534_sub18;
    }
    
    public Class534_Sub18 method14078(int i) {
	return method14079(null, (byte) -69);
    }
    
    Class534_Sub18 method14079(Class534_Sub18 class534_sub18, byte i) {
	Class534_Sub18 class534_sub18_0_;
	if (null == class534_sub18)
	    class534_sub18_0_ = aClass534_Sub18_8785.aClass534_Sub18_10510;
	else
	    class534_sub18_0_ = class534_sub18;
	if (class534_sub18_0_ == aClass534_Sub18_8785) {
	    aClass534_Sub18_8786 = null;
	    return null;
	}
	aClass534_Sub18_8786 = class534_sub18_0_.aClass534_Sub18_10510;
	return class534_sub18_0_;
    }
    
    public Class534_Sub18 method14080(byte i) {
	Class534_Sub18 class534_sub18 = aClass534_Sub18_8786;
	if (class534_sub18 == aClass534_Sub18_8785) {
	    aClass534_Sub18_8786 = null;
	    return null;
	}
	aClass534_Sub18_8786 = class534_sub18.aClass534_Sub18_10510;
	return class534_sub18;
    }
    
    public int method14081(byte i) {
	int i_1_ = 0;
	for (Class534_Sub18 class534_sub18
		 = aClass534_Sub18_8785.aClass534_Sub18_10510;
	     class534_sub18 != aClass534_Sub18_8785;
	     class534_sub18 = class534_sub18.aClass534_Sub18_10510)
	    i_1_++;
	return i_1_;
    }
    
    public Iterator iterator() {
	return new Class694(this);
    }
    
    public Iterator method14082() {
	return new Class694(this);
    }
    
    public Iterator method14083() {
	return new Class694(this);
    }
    
    public void method14084() {
	while (aClass534_Sub18_8785
	       != aClass534_Sub18_8785.aClass534_Sub18_10510)
	    aClass534_Sub18_8785.aClass534_Sub18_10510.method16180(-421776830);
    }
    
    public void method14085() {
	while (aClass534_Sub18_8785
	       != aClass534_Sub18_8785.aClass534_Sub18_10510)
	    aClass534_Sub18_8785.aClass534_Sub18_10510.method16180(-421776830);
    }
    
    public int method14086() {
	int i = 0;
	for (Class534_Sub18 class534_sub18
		 = aClass534_Sub18_8785.aClass534_Sub18_10510;
	     class534_sub18 != aClass534_Sub18_8785;
	     class534_sub18 = class534_sub18.aClass534_Sub18_10510)
	    i++;
	return i;
    }
    
    public void method14087() {
	while (aClass534_Sub18_8785
	       != aClass534_Sub18_8785.aClass534_Sub18_10510)
	    aClass534_Sub18_8785.aClass534_Sub18_10510.method16180(-421776830);
    }
    
    public void method14088(Class534_Sub18 class534_sub18) {
	if (null != class534_sub18.aClass534_Sub18_10508)
	    class534_sub18.method16180(-421776830);
	class534_sub18.aClass534_Sub18_10508
	    = aClass534_Sub18_8785.aClass534_Sub18_10508;
	class534_sub18.aClass534_Sub18_10510 = aClass534_Sub18_8785;
	class534_sub18.aClass534_Sub18_10508.aClass534_Sub18_10510
	    = class534_sub18;
	class534_sub18.aClass534_Sub18_10510.aClass534_Sub18_10508
	    = class534_sub18;
    }
    
    public void method14089() {
	while (aClass534_Sub18_8785
	       != aClass534_Sub18_8785.aClass534_Sub18_10510)
	    aClass534_Sub18_8785.aClass534_Sub18_10510.method16180(-421776830);
    }
    
    public static void method14090(Class534_Sub18 class534_sub18,
				   Class534_Sub18 class534_sub18_2_) {
	if (class534_sub18.aClass534_Sub18_10508 != null)
	    class534_sub18.method16180(-421776830);
	class534_sub18.aClass534_Sub18_10508
	    = class534_sub18_2_.aClass534_Sub18_10508;
	class534_sub18.aClass534_Sub18_10510 = class534_sub18_2_;
	class534_sub18.aClass534_Sub18_10508.aClass534_Sub18_10510
	    = class534_sub18;
	class534_sub18.aClass534_Sub18_10510.aClass534_Sub18_10508
	    = class534_sub18;
    }
    
    Class534_Sub18 method14091(Class534_Sub18 class534_sub18) {
	Class534_Sub18 class534_sub18_3_;
	if (null == class534_sub18)
	    class534_sub18_3_ = aClass534_Sub18_8785.aClass534_Sub18_10510;
	else
	    class534_sub18_3_ = class534_sub18;
	if (class534_sub18_3_ == aClass534_Sub18_8785) {
	    aClass534_Sub18_8786 = null;
	    return null;
	}
	aClass534_Sub18_8786 = class534_sub18_3_.aClass534_Sub18_10510;
	return class534_sub18_3_;
    }
    
    public Class534_Sub18 method14092() {
	Class534_Sub18 class534_sub18
	    = aClass534_Sub18_8785.aClass534_Sub18_10510;
	if (class534_sub18 == aClass534_Sub18_8785)
	    return null;
	class534_sub18.method16180(-421776830);
	return class534_sub18;
    }
    
    public Class534_Sub18 method14093() {
	Class534_Sub18 class534_sub18
	    = aClass534_Sub18_8785.aClass534_Sub18_10510;
	if (class534_sub18 == aClass534_Sub18_8785)
	    return null;
	class534_sub18.method16180(-421776830);
	return class534_sub18;
    }
    
    public Class696() {
	aClass534_Sub18_8785.aClass534_Sub18_10510 = aClass534_Sub18_8785;
	aClass534_Sub18_8785.aClass534_Sub18_10508 = aClass534_Sub18_8785;
    }
    
    public Class534_Sub18 method14094() {
	return method14079(null, (byte) 76);
    }
    
    public Class534_Sub18 method14095() {
	Class534_Sub18 class534_sub18
	    = aClass534_Sub18_8785.aClass534_Sub18_10510;
	if (class534_sub18 == aClass534_Sub18_8785)
	    return null;
	class534_sub18.method16180(-421776830);
	return class534_sub18;
    }
    
    Class534_Sub18 method14096(Class534_Sub18 class534_sub18) {
	Class534_Sub18 class534_sub18_4_;
	if (null == class534_sub18)
	    class534_sub18_4_ = aClass534_Sub18_8785.aClass534_Sub18_10510;
	else
	    class534_sub18_4_ = class534_sub18;
	if (class534_sub18_4_ == aClass534_Sub18_8785) {
	    aClass534_Sub18_8786 = null;
	    return null;
	}
	aClass534_Sub18_8786 = class534_sub18_4_.aClass534_Sub18_10510;
	return class534_sub18_4_;
    }
    
    public Class534_Sub18 method14097() {
	Class534_Sub18 class534_sub18 = aClass534_Sub18_8786;
	if (class534_sub18 == aClass534_Sub18_8785) {
	    aClass534_Sub18_8786 = null;
	    return null;
	}
	aClass534_Sub18_8786 = class534_sub18.aClass534_Sub18_10510;
	return class534_sub18;
    }
    
    public Class534_Sub18 method14098() {
	Class534_Sub18 class534_sub18 = aClass534_Sub18_8786;
	if (class534_sub18 == aClass534_Sub18_8785) {
	    aClass534_Sub18_8786 = null;
	    return null;
	}
	aClass534_Sub18_8786 = class534_sub18.aClass534_Sub18_10510;
	return class534_sub18;
    }
    
    public int method14099() {
	int i = 0;
	for (Class534_Sub18 class534_sub18
		 = aClass534_Sub18_8785.aClass534_Sub18_10510;
	     class534_sub18 != aClass534_Sub18_8785;
	     class534_sub18 = class534_sub18.aClass534_Sub18_10510)
	    i++;
	return i;
    }
    
    public static void method14100(Class534_Sub18 class534_sub18,
				   Class534_Sub18 class534_sub18_5_) {
	if (class534_sub18.aClass534_Sub18_10508 != null)
	    class534_sub18.method16180(-421776830);
	class534_sub18.aClass534_Sub18_10508
	    = class534_sub18_5_.aClass534_Sub18_10508;
	class534_sub18.aClass534_Sub18_10510 = class534_sub18_5_;
	class534_sub18.aClass534_Sub18_10508.aClass534_Sub18_10510
	    = class534_sub18;
	class534_sub18.aClass534_Sub18_10510.aClass534_Sub18_10508
	    = class534_sub18;
    }
    
    public int method14101() {
	int i = 0;
	for (Class534_Sub18 class534_sub18
		 = aClass534_Sub18_8785.aClass534_Sub18_10510;
	     class534_sub18 != aClass534_Sub18_8785;
	     class534_sub18 = class534_sub18.aClass534_Sub18_10510)
	    i++;
	return i;
    }
    
    public void method14102(Class534_Sub18 class534_sub18) {
	if (null != class534_sub18.aClass534_Sub18_10508)
	    class534_sub18.method16180(-421776830);
	class534_sub18.aClass534_Sub18_10508
	    = aClass534_Sub18_8785.aClass534_Sub18_10508;
	class534_sub18.aClass534_Sub18_10510 = aClass534_Sub18_8785;
	class534_sub18.aClass534_Sub18_10508.aClass534_Sub18_10510
	    = class534_sub18;
	class534_sub18.aClass534_Sub18_10510.aClass534_Sub18_10508
	    = class534_sub18;
    }
    
    static void method14103(int i, int i_6_, int i_7_) {
	Class318 class318
	    = (Class318) Class84.aClass44_Sub11_840.method91(i, 903495669);
	Class534_Sub18_Sub6 class534_sub18_sub6
	    = Class447.method7308(1, (long) (class318.aClass150_3392.anInt1694
					     * -1270946121));
	try {
	    if (Class534_Sub18_Sub6.aBool11689)
		class534_sub18_sub6.anInt11666
		    = (Class77.aClass155_Sub1_819
			   .method120(class318.aClass150_3392, (byte) -26)
		       * 517206857);
	    class534_sub18_sub6.anInt11666
		= class318.method5748((class534_sub18_sub6.anInt11666
				       * -1479053575),
				      i_6_, (byte) 56) * 517206857;
	    class534_sub18_sub6.method18121(793314969);
	} catch (Exception_Sub6 exception_sub6) {
	    Class262.method4824(new StringBuilder().append("").append(i)
				    .toString(),
				exception_sub6, (byte) -24);
	}
    }
}
