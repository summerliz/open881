/* Class547 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public abstract class Class547
{
    public static final int anInt7193 = 86;
    static final int anInt7194 = 1;
    static final int anInt7195 = 2;
    static final int anInt7196 = 3;
    static final int anInt7197 = 11;
    static final int anInt7198 = 49;
    static final int anInt7199 = 6;
    static final int anInt7200 = 7;
    static final int anInt7201 = 8;
    static final int anInt7202 = 9;
    static final int anInt7203 = 10;
    static final int anInt7204 = 72;
    static final int anInt7205 = 12;
    static final int anInt7206 = 83;
    static final int anInt7207 = 17;
    public static final int anInt7208 = 101;
    static final int anInt7209 = 19;
    static final int anInt7210 = 27;
    static final int anInt7211 = 21;
    static final int anInt7212 = 34;
    static final int anInt7213 = 23;
    static final int anInt7214 = 74;
    static final int anInt7215 = 22;
    static final int anInt7216 = 37;
    static final int anInt7217 = 16;
    static final int anInt7218 = 32;
    static final int anInt7219 = 33;
    static final int anInt7220 = 59;
    static final int anInt7221 = 35;
    public static final int anInt7222 = 105;
    static final int anInt7223 = 20;
    static final int anInt7224 = 38;
    static final int anInt7225 = 39;
    static final int anInt7226 = 40;
    static final int anInt7227 = 4;
    static final int anInt7228 = 42;
    static final int anInt7229 = 51;
    static final int anInt7230 = 129;
    static final int anInt7231 = 43;
    static final int anInt7232 = 50;
    static final int anInt7233 = 13;
    static final int anInt7234 = 52;
    static final int anInt7235 = 53;
    static final int anInt7236 = 54;
    static final int anInt7237 = 140;
    static final int anInt7238 = 56;
    static final int anInt7239 = 57;
    static final int anInt7240 = 58;
    static final int anInt7241 = 64;
    static final int anInt7242 = 65;
    public static final int anInt7243 = 66;
    public static final int anInt7244 = 67;
    static final int anInt7245 = 68;
    static final int anInt7246 = 55;
    public static final int anInt7247 = 85;
    static final int anInt7248 = 71;
    static final int anInt7249 = 25;
    static final int anInt7250 = 73;
    static final int anInt7251 = 135;
    public static final int anInt7252 = 81;
    public static final int anInt7253 = 82;
    static final int anInt7254 = 70;
    public static final int anInt7255 = 84;
    static final int anInt7256 = 18;
    static final int anInt7257 = 26;
    static final int anInt7258 = 48;
    static final int anInt7259 = 5;
    static final int anInt7260 = 89;
    static final int anInt7261 = 87;
    static final int anInt7262 = 88;
    public static final int anInt7263 = 28;
    static final int anInt7264 = 90;
    static final int anInt7265 = 91;
    public static final int anInt7266 = 96;
    public static final int anInt7267 = 97;
    public static final int anInt7268 = 98;
    public static final int anInt7269 = 99;
    static final int anInt7270 = 100;
    static final int anInt7271 = 136;
    public static final int anInt7272 = 102;
    public static final int anInt7273 = 103;
    public static final int anInt7274 = 104;
    static final int anInt7275 = 162;
    static final int anInt7276 = 128;
    static final int anInt7277 = 69;
    static final int anInt7278 = 130;
    static final int anInt7279 = 131;
    static final int anInt7280 = 132;
    static final int anInt7281 = 133;
    static final int anInt7282 = 134;
    static final int anInt7283 = 161;
    static final int anInt7284 = 24;
    static final int anInt7285 = 137;
    static final int anInt7286 = 138;
    static final int anInt7287 = 41;
    public static final int anInt7288 = 80;
    static final int anInt7289 = 141;
    static final int anInt7290 = 142;
    static final int anInt7291 = 143;
    static final int anInt7292 = 150;
    static final int anInt7293 = 151;
    static final int anInt7294 = 152;
    static final int anInt7295 = 153;
    static final int anInt7296 = 160;
    static final int anInt7297 = 139;
    static final int anInt7298 = 36;
    
    public abstract Interface63 method8994();
    
    Class547() {
	/* empty */
    }
    
    public abstract boolean method8995(int i, byte i_0_);
    
    public abstract Interface63 method8996(int i);
    
    public abstract void method8997(byte i);
    
    public abstract void method8998();
    
    public abstract Interface63 method8999();
    
    public abstract Interface63 method9000();
    
    public abstract Interface63 method9001();
    
    public abstract void method9002(int i);
    
    public abstract boolean method9003(int i);
    
    public abstract boolean method9004(int i);
    
    public abstract void method9005();
    
    public abstract void method9006();
    
    public abstract void method9007();
    
    public abstract boolean method9008(int i);
    
    public abstract void method9009();
    
    static final void method9010(Class669 class669, int i) {
	Class590.method9879((String) (class669.anObjectArray8593
				      [((class669.anInt8594 -= 1460193483)
					* 1485266147)]),
			    ((class669.anIntArray8595
			      [(class669.anInt8600 -= 308999563) * 2088438307])
			     == 1),
			    2134192560);
    }
    
    static final void method9011(Class669 class669, int i) {
	class669.anInt8600 -= 617999126;
	int i_1_ = class669.anIntArray8595[class669.anInt8600 * 2088438307];
	int i_2_
	    = class669.anIntArray8595[class669.anInt8600 * 2088438307 + 1];
	Class273.method5102(3, i_1_, i_2_, "", (byte) 42);
    }
    
    static final void method9012(Class669 class669, byte i) {
	class669.anInt8600 -= 617999126;
	int i_3_ = class669.anIntArray8595[class669.anInt8600 * 2088438307];
	int i_4_
	    = class669.anIntArray8595[1 + class669.anInt8600 * 2088438307];
	Class273.method5102(1, i_3_, i_4_, "", (byte) 63);
    }
    
    static final void method9013(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = class669.aClass534_Sub26_8606.aByte10582;
    }
}
