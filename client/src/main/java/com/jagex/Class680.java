/* Class680 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class680 implements Interface19
{
    Class352 aClass352_8667;
    static int anInt8668;
    public static Class472 aClass472_8669;
    
    public void method113(Class150 class150, long l) {
	throw new UnsupportedOperationException();
    }
    
    public int method120(Class150 class150, byte i) {
	Integer integer
	    = aClass352_8667.method6194(((client.aClass675_11016.anInt8642
					  * -1082924039) << 16
					 | class150.anInt1694 * -1270946121),
					(byte) 13);
	if (integer == null)
	    return ((Integer) class150.method2478(-499047090)).intValue();
	return integer.intValue();
    }
    
    public long method127(Class150 class150, byte i) {
	Long var_long
	    = aClass352_8667.method6195(((client.aClass675_11016.anInt8642
					  * -1082924039) << 16
					 | class150.anInt1694 * -1270946121),
					(byte) 125);
	if (var_long == null)
	    return ((Long) class150.method2478(-152364081)).longValue();
	return var_long.longValue();
    }
    
    public Object method124(Class150 class150, int i) {
	if (class150.aClass493_1696 != Class493.aClass493_5496)
	    throw new IllegalArgumentException("");
	return aClass352_8667.method6253((-1082924039 * (client.aClass675_11016
							 .anInt8642) << 16
					  | -1270946121 * class150.anInt1694),
					 (byte) 19);
    }
    
    public int method123(Class318 class318) {
	return class318.method5750(method120(class318.aClass150_3392,
					     (byte) -109),
				   1477976921);
    }
    
    public void method114(Class150 class150, int i, int i_0_) {
	throw new UnsupportedOperationException();
    }
    
    public void method116(Class150 class150, long l) {
	throw new UnsupportedOperationException();
    }
    
    public void method118(Class150 class150, Object object, byte i) {
	throw new UnsupportedOperationException();
    }
    
    public void method140(Class318 class318, int i) {
	throw new UnsupportedOperationException();
    }
    
    public int method117(Class150 class150) {
	Integer integer
	    = aClass352_8667.method6194(((client.aClass675_11016.anInt8642
					  * -1082924039) << 16
					 | class150.anInt1694 * -1270946121),
					(byte) 13);
	if (integer == null)
	    return ((Integer) class150.method2478(-1629737171)).intValue();
	return integer.intValue();
    }
    
    public int method136(Class150 class150) {
	Integer integer
	    = aClass352_8667.method6194(((client.aClass675_11016.anInt8642
					  * -1082924039) << 16
					 | class150.anInt1694 * -1270946121),
					(byte) 13);
	if (integer == null)
	    return ((Integer) class150.method2478(1738837690)).intValue();
	return integer.intValue();
    }
    
    public Object method134(Class150 class150) {
	if (class150.aClass493_1696 != Class493.aClass493_5496)
	    throw new IllegalArgumentException("");
	return aClass352_8667.method6253((-1082924039 * (client.aClass675_11016
							 .anInt8642) << 16
					  | -1270946121 * class150.anInt1694),
					 (byte) 80);
    }
    
    public void method115(Class150 class150, long l) {
	throw new UnsupportedOperationException();
    }
    
    public int method125(Class318 class318) {
	return class318.method5750(method120(class318.aClass150_3392,
					     (byte) -19),
				   1356913538);
    }
    
    public long method131(Class150 class150) {
	Long var_long
	    = aClass352_8667.method6195(((client.aClass675_11016.anInt8642
					  * -1082924039) << 16
					 | class150.anInt1694 * -1270946121),
					(byte) 125);
	if (var_long == null)
	    return ((Long) class150.method2478(1613239308)).longValue();
	return var_long.longValue();
    }
    
    public void method121(Class150 class150, int i) {
	throw new UnsupportedOperationException();
    }
    
    public void method128(Class150 class150, int i) {
	throw new UnsupportedOperationException();
    }
    
    public int method126(Class318 class318) {
	return class318.method5750(method120(class318.aClass150_3392,
					     (byte) -115),
				   1077722378);
    }
    
    public long method133(Class150 class150) {
	Long var_long
	    = aClass352_8667.method6195(((client.aClass675_11016.anInt8642
					  * -1082924039) << 16
					 | class150.anInt1694 * -1270946121),
					(byte) 126);
	if (var_long == null)
	    return ((Long) class150.method2478(594347997)).longValue();
	return var_long.longValue();
    }
    
    public void method122(Class318 class318, int i, byte i_1_) {
	throw new UnsupportedOperationException();
    }
    
    public long method132(Class150 class150) {
	Long var_long
	    = aClass352_8667.method6195(((client.aClass675_11016.anInt8642
					  * -1082924039) << 16
					 | class150.anInt1694 * -1270946121),
					(byte) 125);
	if (var_long == null)
	    return ((Long) class150.method2478(-1529313628)).longValue();
	return var_long.longValue();
    }
    
    Class680(Class352 class352) {
	aClass352_8667 = class352;
    }
    
    public long method129(Class150 class150) {
	Long var_long
	    = aClass352_8667.method6195(((client.aClass675_11016.anInt8642
					  * -1082924039) << 16
					 | class150.anInt1694 * -1270946121),
					(byte) 125);
	if (var_long == null)
	    return ((Long) class150.method2478(-1702848974)).longValue();
	return var_long.longValue();
    }
    
    public int method135(Class318 class318) {
	return class318.method5750(method120(class318.aClass150_3392,
					     (byte) -42),
				   1686813396);
    }
    
    public int method119(Class318 class318, int i) {
	return class318.method5750(method120(class318.aClass150_3392,
					     (byte) -58),
				   2135706233);
    }
    
    public Object method137(Class150 class150) {
	if (class150.aClass493_1696 != Class493.aClass493_5496)
	    throw new IllegalArgumentException("");
	return aClass352_8667.method6253((-1082924039 * (client.aClass675_11016
							 .anInt8642) << 16
					  | -1270946121 * class150.anInt1694),
					 (byte) 105);
    }
    
    public void method138(Class150 class150, Object object) {
	throw new UnsupportedOperationException();
    }
    
    public void method139(Class318 class318, int i) {
	throw new UnsupportedOperationException();
    }
    
    public void method130(Class150 class150, long l) {
	throw new UnsupportedOperationException();
    }
    
    public static void method13862(Class583 class583, int i, int i_2_,
				   byte i_3_) {
	Class669 class669 = Class567.method9559((byte) -64);
	Class244.method4484(class583, i, i_2_, class669, 2109286662);
    }
    
    static final void method13863(Class669 class669, byte i) {
	class669.aLongArray8586[(class669.anIntArray8591
				 [662605117 * class669.anInt8613])]
	    = (class669.aLongArray8587
	       [(class669.anInt8596 -= 1091885681) * 1572578961]);
    }
    
    static final void method13864(Class669 class669, int i)
	throws Exception_Sub2 {
	int i_4_ = (class669.anIntArray8595
		    [(class669.anInt8600 -= 308999563) * 2088438307]);
	Class599.aClass298_Sub1_7871.method5355(Class217.method4114(i_4_,
								    608978046),
						true, 1779542450);
	client.aBool11147 = true;
    }
}
