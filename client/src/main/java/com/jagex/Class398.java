/* Class398 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class398 implements Interface47
{
    public int anInt4121;
    
    public Class397 method349() {
	return Class397.aClass397_4112;
    }
    
    public Class397 method348(int i) {
	return Class397.aClass397_4112;
    }
    
    public static Class398 method6576(Class534_Sub40 class534_sub40) {
	int i = class534_sub40.method16533(-258848859);
	return new Class398(i);
    }
    
    Class398(int i) {
	anInt4121 = 686099959 * i;
    }
    
    public Class397 method350() {
	return Class397.aClass397_4112;
    }
    
    public Class397 method351() {
	return Class397.aClass397_4112;
    }
    
    static final void method6577(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = 0;
    }
    
    static String method6578(int i, Class672 class672, Class632 class632,
			     int i_0_) {
	if (i < 100000)
	    return new StringBuilder().append
		       (Class308.method5655(class632.anInt8251 * -1811112665,
					    (byte) 1))
		       .append
		       (i).append
		       (Class15.aString184).toString();
	if (i < 10000000)
	    return new StringBuilder().append
		       (Class308.method5655(1015708637 * class632.anInt8252,
					    (byte) 1))
		       .append
		       (i / 1000).append
		       (Class58.aClass58_604.method1245(class672, (byte) -20))
		       .append
		       (Class15.aString184).toString();
	return new StringBuilder().append
		   (Class308.method5655(1348295193 * class632.anInt8253,
					(byte) 1))
		   .append
		   (i / 1000000).append
		   (Class58.aClass58_602.method1245(class672, (byte) -77))
		   .append
		   (Class15.aString184).toString();
    }
    
    static final void method6579(Class669 class669, byte i) {
	if (0 == client.anInt11171 * -1050164879)
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= -1;
	else
	    class669.anIntArray8595
		[(class669.anInt8600 += 308999563) * 2088438307 - 1]
		= client.anInt11329 * 2103713403;
    }
    
    static Class328[] method6580(byte i) {
	return (new Class328[]
		{ Class328.aClass328_3473, Class328.aClass328_3464,
		  Class328.aClass328_3463, Class328.aClass328_3470,
		  Class328.aClass328_3466, Class328.aClass328_3475,
		  Class328.aClass328_3462, Class328.aClass328_3468,
		  Class328.aClass328_3467, Class328.aClass328_3476,
		  Class328.aClass328_3472, Class328.aClass328_3469,
		  Class328.aClass328_3474, Class328.aClass328_3465,
		  Class328.aClass328_3461 });
    }
}
