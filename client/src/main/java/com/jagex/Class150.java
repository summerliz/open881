/* Class150 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public abstract class Class150 implements Interface12
{
    public int anInt1694;
    public Class453 aClass453_1695;
    public Class493 aClass493_1696;
    public Class455 aClass455_1697 = Class455.aClass455_4961;
    public boolean aBool1698 = true;
    static int anInt1699;
    
    public Object method2476() {
	return aClass453_1695.method7396(this, (byte) 30);
    }
    
    public void method80(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-1308596559);
	    if (0 == i)
		break;
	    Class154 class154
		= ((Class154)
		   Class448.method7319(Class177.method2933(1926919461), i,
				       2088438307));
	    if (null != class154) {
		switch (class154.anInt1738 * 1108078243) {
		case 3:
		    aBool1698 = false;
		    break;
		case 6: {
		    int i_0_ = class534_sub40.method16527(-534649853);
		    aClass493_1696
			= ((Class493)
			   Class448.method7319(Class493.method8108((byte) 7),
					       i_0_, 2088438307));
		    if (aClass493_1696 == null)
			throw new IllegalStateException("");
		    break;
		}
		case 2:
		    aClass455_1697
			= ((Class455)
			   Class448.method7319(Class635
						   .method10535(-2146862355),
					       class534_sub40
						   .method16527(1779972431),
					       2088438307));
		    break;
		case 4:
		    break;
		case 1:
		    class534_sub40.method16523(-1582899738);
		    break;
		case 0:
		    Class448.method7319(Class497.method8141((byte) 9),
					class534_sub40
					    .method16527(-1730663677),
					2088438307);
		    break;
		default:
		    throw new IllegalStateException("");
		}
	    } else
		method2486(class534_sub40, i, (byte) 58);
	}
    }
    
    public void method79(Class534_Sub40 class534_sub40, byte i) {
	for (;;) {
	    int i_1_ = class534_sub40.method16527(1973789150);
	    if (0 == i_1_)
		break;
	    Class154 class154
		= ((Class154)
		   Class448.method7319(Class177.method2933(621267555), i_1_,
				       2088438307));
	    if (null != class154) {
		switch (class154.anInt1738 * 1108078243) {
		case 3:
		    aBool1698 = false;
		    break;
		case 6: {
		    int i_2_ = class534_sub40.method16527(2130380509);
		    aClass493_1696
			= ((Class493)
			   Class448.method7319(Class493.method8108((byte) -1),
					       i_2_, 2088438307));
		    if (aClass493_1696 == null)
			throw new IllegalStateException("");
		    break;
		}
		case 2:
		    aClass455_1697
			= ((Class455)
			   Class448.method7319(Class635.method10535(-53642523),
					       class534_sub40
						   .method16527(-379663306),
					       2088438307));
		    break;
		case 4:
		    break;
		case 1:
		    class534_sub40.method16523(-1368700180);
		    break;
		case 0:
		    Class448.method7319(Class497.method8141((byte) 9),
					class534_sub40.method16527(803852621),
					2088438307);
		    break;
		default:
		    throw new IllegalStateException("");
		}
	    } else
		method2486(class534_sub40, i_1_, (byte) 87);
	}
    }
    
    boolean method2477(int i) {
	if (null == aClass453_1695 || null == aClass493_1696)
	    return false;
	return true;
    }
    
    public Object method2478(int i) {
	return aClass453_1695.method7396(this, (byte) 58);
    }
    
    public Object method2479() {
	return aClass453_1695.method7396(this, (byte) 82);
    }
    
    public void method78(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-2113844080);
	    if (0 == i)
		break;
	    Class154 class154
		= ((Class154)
		   Class448.method7319(Class177.method2933(1773177399), i,
				       2088438307));
	    if (null != class154) {
		switch (class154.anInt1738 * 1108078243) {
		case 3:
		    aBool1698 = false;
		    break;
		case 6: {
		    int i_3_ = class534_sub40.method16527(-824821022);
		    aClass493_1696
			= ((Class493)
			   Class448.method7319(Class493.method8108((byte) 19),
					       i_3_, 2088438307));
		    if (aClass493_1696 == null)
			throw new IllegalStateException("");
		    break;
		}
		case 2:
		    aClass455_1697
			= ((Class455)
			   Class448.method7319(Class635
						   .method10535(-1125985659),
					       class534_sub40
						   .method16527(-1316664115),
					       2088438307));
		    break;
		case 4:
		    break;
		case 1:
		    class534_sub40.method16523(-2136308234);
		    break;
		case 0:
		    Class448.method7319(Class497.method8141((byte) 9),
					class534_sub40.method16527(-913494898),
					2088438307);
		    break;
		default:
		    throw new IllegalStateException("");
		}
	    } else
		method2486(class534_sub40, i, (byte) 111);
	}
    }
    
    public void method81(Class534_Sub40 class534_sub40) {
	for (;;) {
	    int i = class534_sub40.method16527(-1456549248);
	    if (0 == i)
		break;
	    Class154 class154
		= ((Class154)
		   Class448.method7319(Class177.method2933(1918605673), i,
				       2088438307));
	    if (null != class154) {
		switch (class154.anInt1738 * 1108078243) {
		case 3:
		    aBool1698 = false;
		    break;
		case 6: {
		    int i_4_ = class534_sub40.method16527(-223844828);
		    aClass493_1696
			= ((Class493)
			   Class448.method7319(Class493.method8108((byte) 7),
					       i_4_, 2088438307));
		    if (aClass493_1696 == null)
			throw new IllegalStateException("");
		    break;
		}
		case 2:
		    aClass455_1697
			= ((Class455)
			   Class448.method7319(Class635
						   .method10535(-342233026),
					       class534_sub40
						   .method16527(-11149412),
					       2088438307));
		    break;
		case 4:
		    break;
		case 1:
		    class534_sub40.method16523(-1626235688);
		    break;
		case 0:
		    Class448.method7319(Class497.method8141((byte) 9),
					class534_sub40.method16527(-888004417),
					2088438307);
		    break;
		default:
		    throw new IllegalStateException("");
		}
	    } else
		method2486(class534_sub40, i, (byte) 75);
	}
    }
    
    abstract void method2480(Class534_Sub40 class534_sub40, int i);
    
    abstract void method2481(Class534_Sub40 class534_sub40, int i);
    
    boolean method2482() {
	if (null == aClass453_1695 || null == aClass493_1696)
	    return false;
	return true;
    }
    
    boolean method2483() {
	if (null == aClass453_1695 || null == aClass493_1696)
	    return false;
	return true;
    }
    
    Class150(Class453 class453, int i) {
	aClass453_1695 = class453;
	anInt1694 = -1851992313 * i;
    }
    
    boolean method2484() {
	if (null == aClass453_1695 || null == aClass493_1696)
	    return false;
	return true;
    }
    
    public Object method2485() {
	return aClass453_1695.method7396(this, (byte) 48);
    }
    
    abstract void method2486(Class534_Sub40 class534_sub40, int i, byte i_5_);
    
    public Object method2487() {
	return aClass453_1695.method7396(this, (byte) 32);
    }
    
    boolean method2488() {
	if (null == aClass453_1695 || null == aClass493_1696)
	    return false;
	return true;
    }
    
    static final void method2489(Class669 class669, byte i) {
	Class292.method5306(0, 0,
			    client.aClass247_11226.anInt2468 * -881188269,
			    -1279656873 * client.aClass247_11226.anInt2469,
			    false, (byte) 110);
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = client.anInt11234 * -610622155;
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = client.anInt11190 * 608745565;
    }
    
    public static void method2490(int i, String string, boolean bool,
				  byte i_6_) {
	if (0 == i) {
	    Class254.aClass185_2683
		= Class321.method5777(0, Class464.aCanvas5111,
				      Class656.aClass177_8524,
				      Class534.anInterface25_7160,
				      Class334.aClass402_3513,
				      Class269.aClass396_2956,
				      Class48.aClass387_363,
				      Class295.aClass472_3161,
				      Class44_Sub6.aClass534_Sub35_10989
					  .aClass690_Sub27_10757
					  .method17119((byte) 108) * 2,
				      2143842374);
	    if (null != string) {
		Class254.aClass185_2683.method3340(1, 0);
		Class16 class16
		    = Class288.method5276(Class606.aClass472_7988,
					  -849564261 * Class67.anInt716, 0,
					  Class351.aClass406_3620, -554933684);
		Class171 class171
		    = (Class254.aClass185_2683.method3325
		       (class16,
			Class178.method2939(Class464.aClass472_5113,
					    -849564261 * Class67.anInt716, 0),
			true));
		Class292.method5305(-841564954);
		Class689.method14015(string, true, Class254.aClass185_2683,
				     class171, class16, (byte) -113);
	    }
	} else {
	    Class185 class185 = null;
	    if (null != string) {
		class185 = Class321.method5777(0, Class464.aCanvas5111,
					       Class656.aClass177_8524,
					       Class534.anInterface25_7160,
					       Class334.aClass402_3513,
					       Class269.aClass396_2956,
					       Class48.aClass387_363,
					       Class295.aClass472_3161, 0,
					       2142421551);
		class185.method3340(1, 0);
		Class16 class16
		    = Class711.method14414(Class606.aClass472_7988,
					   Class67.anInt716 * -849564261, 0,
					   1799843746);
		Class171 class171
		    = (class185.method3325
		       (class16,
			Class178.method2939(Class464.aClass472_5113,
					    -849564261 * Class67.anInt716, 0),
			true));
		Class292.method5305(-841564954);
		Class689.method14015(string, true, class185, class171, class16,
				     (byte) -115);
		try {
		    class185.method3236(-568376843);
		    class185 = null;
		} catch (Throwable throwable) {
		    /* empty */
		}
	    }
	    try {
		try {
		    Class305.method5605(2048081638);
		    System.gc();
		    Class254.aClass185_2683
			= Class321.method5777(i, Class464.aCanvas5111,
					      Class656.aClass177_8524,
					      Class534.anInterface25_7160,
					      Class334.aClass402_3513,
					      Class269.aClass396_2956,
					      Class48.aClass387_363,
					      Class295.aClass472_3161,
					      Class44_Sub6
						  .aClass534_Sub35_10989
						  .aClass690_Sub27_10757
						  .method17119((byte) 114) * 2,
					      2144306196);
		    Class351.aClass406_3620
			= new Class406(Class254.aClass185_2683,
				       Class464.aClass472_5113,
				       Class606.aClass472_7988,
				       Class97.method1832((byte) -44));
		    if (Class254.aClass185_2683.method3240()) {
			boolean bool_7_ = true;
			try {
			    bool_7_ = -686202593 * (Class200_Sub23
						    .aClass534_Sub28_10040
						    .anInt10635) > 256;
			} catch (Throwable throwable) {
			    /* empty */
			}
			Class534_Sub2 class534_sub2;
			if (bool_7_)
			    class534_sub2 = Class254.aClass185_2683
						.method3312(146800640);
			else
			    class534_sub2 = Class254.aClass185_2683
						.method3312(104857600);
			Class254.aClass185_2683.method3538(class534_sub2);
		    }
		} catch (Throwable throwable) {
		    switch (i) {
		    case 1:
			Class325.method5818(Class26.aClass26_236, (byte) -81);
			break;
		    case 3:
			Class325.method5818(Class26.aClass26_246, (byte) -44);
			break;
		    }
		    if (throwable instanceof RuntimeException_Sub4) {
			Class44_Sub6.aClass534_Sub35_10989
			    .aClass690_Sub27_10781.method14020(0, -873186265);
			Class44_Sub6.aClass534_Sub35_10989
			    .aClass690_Sub27_10757.method14020(0, 150576017);
		    }
		    int i_8_
			= Class44_Sub6.aClass534_Sub35_10989
			      .aClass690_Sub7_10733.method16935(-1807368365);
		    Class44_Sub6.aClass534_Sub35_10989.method16438
			((Class44_Sub6.aClass534_Sub35_10989
			  .aClass690_Sub7_10733),
			 0, 54625654);
		    if (class185 != null) {
			try {
			    class185.method3236(-568376843);
			    class185 = null;
			} catch (Throwable throwable_9_) {
			    /* empty */
			}
		    }
		    Class305.method5605(251930862);
		    System.gc();
		    method2490(i_8_, string, bool, (byte) 62);
		    if (class185 != null) {
			try {
			    class185.method3236(-568376843);
			} catch (Throwable throwable_10_) {
			    /* empty */
			}
		    }
		    return;
		}
		if (class185 != null) {
		    try {
			class185.method3236(-568376843);
		    } catch (Throwable throwable) {
			/* empty */
		    }
		}
	    } catch (Exception object) {
		if (class185 != null) {
		    try {
			class185.method3236(-568376843);
		    } catch (Throwable throwable) {
			/* empty */
		    }
		}
		throw object;
	    }
	}
	if (bool)
	    Class44_Sub6.aClass534_Sub35_10989.method16439
		(Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub7_10733,
		 !bool, (byte) 86);
	Class44_Sub6.aClass534_Sub35_10989.method16438((Class44_Sub6
							.aClass534_Sub35_10989
							.aClass690_Sub7_10733),
						       i, -96783067);
	if (!bool)
	    Class44_Sub6.aClass534_Sub35_10989.method16439
		(Class44_Sub6.aClass534_Sub35_10989.aClass690_Sub7_10733,
		 !bool, (byte) 44);
	Class690_Sub37.method17195(1681212841);
	Class254.aClass185_2683.method3328(20000);
	Class254.aClass185_2683.method3342(32);
	client.aClass512_11100.method8439((byte) 8);
	if (Class254.aClass185_2683.method3534())
	    Class172.method2902(Class44_Sub6.aClass534_Sub35_10989
				    .aClass690_Sub19_10741
				    .method17053((byte) -46) == 1,
				-1789490349);
	client.aClass512_11100.method8437(true, 409582091);
	client.aClass512_11100.method8501((byte) -70).method10157(823968051);
	Class606.method10051(-441261296);
	client.aBool11048 = false;
	client.aBool11049 = false;
	client.aBool11059 = true;
	Class585.aClass6Array7799 = null;
	Class254.aClass185_2683.method3284(0.0F, 1.0F);
    }
}
