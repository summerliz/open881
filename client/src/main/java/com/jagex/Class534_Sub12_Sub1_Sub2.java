/* Class534_Sub12_Sub1_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public final class Class534_Sub12_Sub1_Sub2 extends Class534_Sub12_Sub1
{
    int[] anIntArray12108;
    int[] anIntArray12109 = null;
    Class269_Sub2 aClass269_Sub2_12110;
    
    public boolean method18245(int i) {
	return true;
    }
    
    public boolean method18235(int i) {
	return true;
    }
    
    final int method18733() {
	return anIntArray12109[aClass269_Sub2_12110.method4921(574919555)];
    }
    
    final int method18734() {
	return anIntArray12108[aClass269_Sub2_12110.method4921(295766526)];
    }
    
    public final int method18240(int i) {
	int i_0_ = anIntArray12109[i];
	int i_1_ = anIntArray12108[i];
	if (i_0_ != -1)
	    return i_0_;
	if (i_1_ != -1)
	    return i_1_ | 0x10000;
	return -1;
    }
    
    final int method18735() {
	return anIntArray12108[aClass269_Sub2_12110.method4921(1011710526)];
    }
    
    Class534_Sub12_Sub1_Sub2(Class269_Sub2 class269_sub2, Class266 class266) {
	super(class266);
	anIntArray12108 = null;
	aClass269_Sub2_12110 = class269_sub2;
	anIntArray12109 = class266.anIntArray2938;
	anIntArray12108 = class266.anIntArray2936;
    }
    
    final int method18736() {
	return anIntArray12108[aClass269_Sub2_12110.method4921(1069815809)];
    }
    
    final int method18737() {
	return anIntArray12109[aClass269_Sub2_12110.method4921(933017422)];
    }
    
    public final int method18251(int i) {
	int i_2_ = anIntArray12109[i];
	int i_3_ = anIntArray12108[i];
	if (i_2_ != -1)
	    return i_2_;
	if (i_3_ != -1)
	    return i_3_ | 0x10000;
	return -1;
    }
    
    final int method18738() {
	return anIntArray12109[aClass269_Sub2_12110.method4921(904722877)];
    }
    
    final int method18739() {
	return anIntArray12108[aClass269_Sub2_12110.method4921(467649452)];
    }
}
