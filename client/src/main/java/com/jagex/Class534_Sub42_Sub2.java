/* Class534_Sub42_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public final class Class534_Sub42_Sub2 extends Class534_Sub42
{
    int anInt11876;
    static Class534_Sub42_Sub2[] aClass534_Sub42_Sub2Array11877
	= new Class534_Sub42_Sub2[0];
    int anInt11878;
    int anInt11879;
    long aLong11880;
    int anInt11881;
    static String aString11882;
    
    Class534_Sub42_Sub2() {
	/* empty */
    }
    
    public int method16798(byte i) {
	return anInt11876 * -2076408251;
    }
    
    public int method16799(byte i) {
	return -224222521 * anInt11878;
    }
    
    public int method16800(int i) {
	return 420516991 * anInt11879;
    }
    
    public long method16802(int i) {
	return aLong11880 * -2484580550736137123L;
    }
    
    public int method16817(int i) {
	return anInt11881 * -507795847;
    }
    
    public void method16803(byte i) {
	synchronized (aClass534_Sub42_Sub2Array11877) {
	    if (Class534_Sub18_Sub3.anInt11375 * -1194496119
		< 1900344283 * Class155.anInt1743 - 1)
		aClass534_Sub42_Sub2Array11877[((Class534_Sub18_Sub3.anInt11375
						 += -1383614791) * -1194496119
						- 1)]
		    = this;
	}
    }
    
    public int method16809() {
	return 420516991 * anInt11879;
    }
    
    static void method18454(int i) {
	Class155.anInt1743 = 1517932627 * i;
	aClass534_Sub42_Sub2Array11877 = new Class534_Sub42_Sub2[i];
	Class534_Sub18_Sub3.anInt11375 = 0;
    }
    
    static void method18455(int i) {
	Class155.anInt1743 = 1517932627 * i;
	aClass534_Sub42_Sub2Array11877 = new Class534_Sub42_Sub2[i];
	Class534_Sub18_Sub3.anInt11375 = 0;
    }
    
    static Class534_Sub42_Sub2 method18456(int i, int i_0_, int i_1_, long l,
					   int i_2_) {
	synchronized (aClass534_Sub42_Sub2Array11877) {
	    Class534_Sub42_Sub2 class534_sub42_sub2;
	    if (-1194496119 * Class534_Sub18_Sub3.anInt11375 == 0)
		class534_sub42_sub2 = new Class534_Sub42_Sub2();
	    else
		class534_sub42_sub2 = (aClass534_Sub42_Sub2Array11877
				       [(Class534_Sub18_Sub3.anInt11375
					 -= -1383614791) * -1194496119]);
	    class534_sub42_sub2.anInt11876 = i * -1091003251;
	    class534_sub42_sub2.anInt11878 = -397312265 * i_0_;
	    class534_sub42_sub2.anInt11879 = i_1_ * -374658177;
	    class534_sub42_sub2.aLong11880 = 2565003256494833653L * l;
	    class534_sub42_sub2.anInt11881 = 674620361 * i_2_;
	    Class534_Sub42_Sub2 class534_sub42_sub2_3_ = class534_sub42_sub2;
	    return class534_sub42_sub2_3_;
	}
    }
    
    public int method16804() {
	return anInt11876 * -2076408251;
    }
    
    public int method16805() {
	return anInt11876 * -2076408251;
    }
    
    public int method16818() {
	return anInt11881 * -507795847;
    }
    
    public int method16807() {
	return 420516991 * anInt11879;
    }
    
    public int method16806() {
	return 420516991 * anInt11879;
    }
    
    public long method16813() {
	return aLong11880 * -2484580550736137123L;
    }
    
    public void method16811() {
	synchronized (aClass534_Sub42_Sub2Array11877) {
	    if (Class534_Sub18_Sub3.anInt11375 * -1194496119
		< 1900344283 * Class155.anInt1743 - 1)
		aClass534_Sub42_Sub2Array11877[((Class534_Sub18_Sub3.anInt11375
						 += -1383614791) * -1194496119
						- 1)]
		    = this;
	}
    }
    
    public void method16812() {
	synchronized (aClass534_Sub42_Sub2Array11877) {
	    if (Class534_Sub18_Sub3.anInt11375 * -1194496119
		< 1900344283 * Class155.anInt1743 - 1)
		aClass534_Sub42_Sub2Array11877[((Class534_Sub18_Sub3.anInt11375
						 += -1383614791) * -1194496119
						- 1)]
		    = this;
	}
    }
    
    public void method16810() {
	synchronized (aClass534_Sub42_Sub2Array11877) {
	    if (Class534_Sub18_Sub3.anInt11375 * -1194496119
		< 1900344283 * Class155.anInt1743 - 1)
		aClass534_Sub42_Sub2Array11877[((Class534_Sub18_Sub3.anInt11375
						 += -1383614791) * -1194496119
						- 1)]
		    = this;
	}
    }
    
    public void method16801() {
	synchronized (aClass534_Sub42_Sub2Array11877) {
	    if (Class534_Sub18_Sub3.anInt11375 * -1194496119
		< 1900344283 * Class155.anInt1743 - 1)
		aClass534_Sub42_Sub2Array11877[((Class534_Sub18_Sub3.anInt11375
						 += -1383614791) * -1194496119
						- 1)]
		    = this;
	}
    }
    
    public int method16808() {
	return -224222521 * anInt11878;
    }
    
    static void method18457(Class534_Sub36 class534_sub36, byte i) {
	Class51.aClass298_Sub1_412 = new Class298_Sub1(client.aClass55_11304);
	Class51.aClass298_Sub1_412.method5353(Class308.aClass308_3351,
					      (byte) 7);
	try {
	    Class347_Sub2 class347_sub2
		= ((Class347_Sub2)
		   Class51.aClass298_Sub1_412.method5355((Class305
							  .aClass305_3266),
							 false, 1439017449));
	    Class706_Sub3 class706_sub3
		= ((Class706_Sub3)
		   Class51.aClass298_Sub1_412
		       .method5455(Class293.aClass293_3124, false, (byte) 55));
	    class347_sub2.method15787(class534_sub36, -1062631237);
	    class706_sub3.method17278(new Class443(0.0F, 0.0F, 0.0F),
				      (byte) 1);
	    Class51.aClass298_Sub1_412.method5410
		(Class438.method6996(99999.0F, 99999.0F, 99999.0F),
		 -2058647403);
	    Class51.aClass298_Sub1_412.method5454
		(Class438.method6996(Float.POSITIVE_INFINITY,
				     Float.POSITIVE_INFINITY,
				     Float.POSITIVE_INFINITY),
		 -1127503483);
	    Class51.aClass298_Sub1_412.method5364
		(Class438.method6996(99999.0F, 99999.0F, 99999.0F),
		 1504345188);
	    Class51.aClass298_Sub1_412.method5470
		(Class438.method6996(Float.POSITIVE_INFINITY,
				     Float.POSITIVE_INFINITY,
				     Float.POSITIVE_INFINITY),
		 -881375624);
	} catch (Exception_Sub2 exception_sub2) {
	    /* empty */
	}
	Class51.anInt413
	    = Class81.aClass563_834.method9493(-1595825620) * 742277525;
	RuntimeException_Sub4.anInt12126
	    = Class81.aClass563_834.method9477(1747184896) * -1144821879;
	Class51.aBool414 = true;
    }
}
