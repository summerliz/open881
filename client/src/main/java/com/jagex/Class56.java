/* Class56 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package com.jagex;

public class Class56
{
    public int anInt448;
    Class472 aClass472_449;
    public int anInt450 = 0;
    static final int anInt451 = 1;
    Class203 aClass203_452;
    static final int anInt453 = 32768;
    Class472 aClass472_454;
    Interface72 anInterface72_455;
    
    public Class534_Sub18_Sub12 method1217(int i) {
	Class534_Sub18_Sub12 class534_sub18_sub12
	    = (Class534_Sub18_Sub12) aClass203_452.method3871((long) i);
	if (class534_sub18_sub12 != null)
	    return class534_sub18_sub12;
	byte[] is;
	if (i >= 32768)
	    is = aClass472_449.method7743(1, i & 0x7fff, -559163568);
	else
	    is = aClass472_454.method7743(1, i, -879651748);
	class534_sub18_sub12 = new Class534_Sub18_Sub12();
	class534_sub18_sub12.aClass56_11797 = this;
	if (is != null)
	    class534_sub18_sub12.method18355(new Class534_Sub40(is),
					     -1877591417);
	if (i >= 32768)
	    class534_sub18_sub12.method18363((byte) 95);
	aClass203_452.method3893(class534_sub18_sub12, (long) i);
	return class534_sub18_sub12;
    }
    
    public Class534_Sub18_Sub12 method1218(int i, int i_0_) {
	Class534_Sub18_Sub12 class534_sub18_sub12
	    = (Class534_Sub18_Sub12) aClass203_452.method3871((long) i);
	if (class534_sub18_sub12 != null)
	    return class534_sub18_sub12;
	byte[] is;
	if (i >= 32768)
	    is = aClass472_449.method7743(1, i & 0x7fff, -1116803889);
	else
	    is = aClass472_454.method7743(1, i, -945409675);
	class534_sub18_sub12 = new Class534_Sub18_Sub12();
	class534_sub18_sub12.aClass56_11797 = this;
	if (is != null)
	    class534_sub18_sub12.method18355(new Class534_Sub40(is),
					     839154170);
	if (i >= 32768)
	    class534_sub18_sub12.method18363((byte) 14);
	aClass203_452.method3893(class534_sub18_sub12, (long) i);
	return class534_sub18_sub12;
    }
    
    String method1219(Class640 class640, int[] is, long l) {
	if (null != anInterface72_455) {
	    String string = anInterface72_455.method487(class640, is, l);
	    if (null != string)
		return string;
	}
	return Long.toString(l);
    }
    
    public Class534_Sub18_Sub12 method1220(int i) {
	Class534_Sub18_Sub12 class534_sub18_sub12
	    = (Class534_Sub18_Sub12) aClass203_452.method3871((long) i);
	if (class534_sub18_sub12 != null)
	    return class534_sub18_sub12;
	byte[] is;
	if (i >= 32768)
	    is = aClass472_449.method7743(1, i & 0x7fff, -1871075777);
	else
	    is = aClass472_454.method7743(1, i, -845103283);
	class534_sub18_sub12 = new Class534_Sub18_Sub12();
	class534_sub18_sub12.aClass56_11797 = this;
	if (is != null)
	    class534_sub18_sub12.method18355(new Class534_Sub40(is),
					     2040510876);
	if (i >= 32768)
	    class534_sub18_sub12.method18363((byte) 20);
	aClass203_452.method3893(class534_sub18_sub12, (long) i);
	return class534_sub18_sub12;
    }
    
    public Class56(Class672 class672, Class472 class472, Class472 class472_1_,
		   Interface72 interface72) {
	anInt448 = 0;
	aClass203_452 = new Class203(64);
	anInterface72_455 = null;
	aClass472_454 = class472;
	aClass472_449 = class472_1_;
	anInterface72_455 = interface72;
	if (null != aClass472_454)
	    anInt450 = aClass472_454.method7726(1, (byte) 20) * 873941565;
	if (null != aClass472_449)
	    anInt448 = aClass472_449.method7726(1, (byte) 53) * -536968583;
    }
    
    public Class534_Sub18_Sub12 method1221(int i) {
	Class534_Sub18_Sub12 class534_sub18_sub12
	    = (Class534_Sub18_Sub12) aClass203_452.method3871((long) i);
	if (class534_sub18_sub12 != null)
	    return class534_sub18_sub12;
	byte[] is;
	if (i >= 32768)
	    is = aClass472_449.method7743(1, i & 0x7fff, -1340048852);
	else
	    is = aClass472_454.method7743(1, i, -1564427278);
	class534_sub18_sub12 = new Class534_Sub18_Sub12();
	class534_sub18_sub12.aClass56_11797 = this;
	if (is != null)
	    class534_sub18_sub12.method18355(new Class534_Sub40(is),
					     -1042027890);
	if (i >= 32768)
	    class534_sub18_sub12.method18363((byte) 111);
	aClass203_452.method3893(class534_sub18_sub12, (long) i);
	return class534_sub18_sub12;
    }
    
    public Class534_Sub18_Sub12 method1222(int i) {
	Class534_Sub18_Sub12 class534_sub18_sub12
	    = (Class534_Sub18_Sub12) aClass203_452.method3871((long) i);
	if (class534_sub18_sub12 != null)
	    return class534_sub18_sub12;
	byte[] is;
	if (i >= 32768)
	    is = aClass472_449.method7743(1, i & 0x7fff, -1903244399);
	else
	    is = aClass472_454.method7743(1, i, -960716132);
	class534_sub18_sub12 = new Class534_Sub18_Sub12();
	class534_sub18_sub12.aClass56_11797 = this;
	if (is != null)
	    class534_sub18_sub12.method18355(new Class534_Sub40(is),
					     -2039339142);
	if (i >= 32768)
	    class534_sub18_sub12.method18363((byte) 107);
	aClass203_452.method3893(class534_sub18_sub12, (long) i);
	return class534_sub18_sub12;
    }
    
    String method1223(Class640 class640, int[] is, long l) {
	if (null != anInterface72_455) {
	    String string = anInterface72_455.method487(class640, is, l);
	    if (null != string)
		return string;
	}
	return Long.toString(l);
    }
    
    String method1224(Class640 class640, int[] is, long l) {
	if (null != anInterface72_455) {
	    String string = anInterface72_455.method487(class640, is, l);
	    if (null != string)
		return string;
	}
	return Long.toString(l);
    }
    
    public static Class534_Sub19 method1225(int i) {
	Class534_Sub19 class534_sub19 = Class670.method11042(278897385);
	class534_sub19.aClass404_10512 = null;
	class534_sub19.anInt10516 = 0;
	class534_sub19.aClass534_Sub40_Sub1_10513
	    = new Class534_Sub40_Sub1(5000);
	return class534_sub19;
    }
    
    static void method1226(int i) {
	Class114.anInt1386
	    = -218904694 + (Class322.aClass16_3420.anInt189 * -858559641
			    + -2123874781 * Class322.aClass16_3420.anInt190);
	Class114.anInt1384
	    = (Class67.aClass16_720.anInt190 * -309101603
	       + 376293017 * Class67.aClass16_720.anInt189 + -1626528650);
	Class200_Sub11.aStringArray9930 = new String[500];
	for (int i_2_ = 0; i_2_ < Class200_Sub11.aStringArray9930.length;
	     i_2_++)
	    Class200_Sub11.aStringArray9930[i_2_] = "";
	Class73.method1567(Class58.aClass58_631.method1245((Class539
							    .aClass672_7171),
							   (byte) -101),
			   -2063140164);
    }
    
    static final void method1227(Class669 class669, int i) {
	class669.anIntArray8595[((class669.anInt8600 += 308999563) * 2088438307
				 - 1)]
	    = class669.aClass352_8602.aBool3641 ? 1 : 0;
    }
}
