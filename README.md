# Open881

## Running

Place the 881 cache inside `server/data/cache/`.

To start the server while developing, run `./gradlew server:run --args='1 false false false false'`  
To start the client, run `./gradlew client:run`

## Credits

- Founded by Castiel
