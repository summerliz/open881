package com.rs.game.player.content;

import com.rs.Settings;

public enum News {

	
	NEWS1("Loot beams",
			"Optimize the loot beam settings to your preferences by opening the loot settings menu!",
			Settings.GAME_UPDATES, "11/6/2017", true),
	NEWS2("Revision Update", "Fully converted Matrix 880 to 881.",
					Settings.TECHNICAL, "11/6/2017", true),
	NEWS3("Improved Looting",
			"With a single click, bring up the new, customisable loot window and grab everything in the surrounding area.",
			Settings.GAME_UPDATES, "25/5/2017", true);


	private String title, message, date;
	private int category;
	private boolean pinned;

	News(String title, String message, int category, String date, boolean pinned) {
		this.title = title;
		this.message = message;
		this.category = category;
		this.date = date;
		this.pinned = pinned;
	}

	public String getTitle() {
		return title;
	}

	public String getMessage() {
		return message;
	}

	public int getCategory() {
		return category;
	}

	public String getDate() {
		return date;
	}

	public boolean isPinned() {
		return pinned;
	}
}